#pragma once 

//***************************************************
#include "../Src/Common/CommonPrivate.h"
#include "../Src/Common/StringHelper.h"
#include "../Src/Common/IRender.h"
#include "../Src/Common/IGame.h"
#include "../Src/Common/IScripting.h"

#include "../Src/Core/inc/coredll.h"
#include "../Src/Core/inc/SteamWorks_API.h"
#include "../Src/Core/inc/CvarManager.h"
#include "../Src/Core/inc/Config.h"
#include "../Src/Core/inc/DynLib.h"

//***************************************************
#include "../Src/Engine/inc/Frustum.h"
#include "../Src/Engine/inc/Engine.h"
#include "../Src/Engine/inc/ALSystem.h"
#include "../Src/Engine/inc/LoadingScreen.h"
#include "../Src/Engine/inc/Scene.h"
#include "../Src/Engine/inc/LightsInc.h"
#include "../Src/Engine/inc/Cache.h"
#include "../Src/Engine/inc/GUI.h"
//***************************************************
#include "../Src/Engine/inc/Object.h"
#include "../Src/Engine/inc/ObjectMesh.h"
#include "../Src/Engine/inc/AnimatedMesh.h"
#include "../Src/Engine/inc/ObjectSky.h"

#include "../Src/Engine/inc/Camera.h"
#include "../Src/Engine/inc/CameraFixed.h"
#include "../Src/Engine/inc/CameraFPS.h"
#include "../Src/Engine/inc/CameraFree.h"
//***************************************************
// Render
#include "../Src/Engine/inc/RenderPipeline.h"

//***************************************************
//Scripting
#include "../Src/Engine/inc/WrappedScriptFunctions.h"

//***************************************************
//Physics
#include "../Src/Engine/inc/PhysSystem.h"
//***************************************************

//End user API
#include "../Src/Engine/API/RenderAPI.h"
#include "../Src/Engine/API/WindowAPI.h"
#include "../Src/Engine/API/EngineAPI.h"
#include "../Src/Engine/API/EditorAPI.h"