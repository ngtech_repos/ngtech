/* Copyright (C) 2009-2017, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include "Hub.h"
/*
Class description
*/
class NGMaxClassDesc : public ClassDesc
{
public:
	/*
	*/
	int IsPublic()
	{
		return 1;
	}

	/*
	*/
	void *Create(int loading = 0)
	{
		return new NGMaxDeveloperHub;
	}

	/*
	*/
	const TCHAR *ClassName()
	{
		return _T("NGMaxDeveloperHub");
	}

	/*
	*/
	SClass_ID SuperClassID()
	{
		return SCENE_EXPORT_CLASS_ID;
	}

	/*
	*/
	Class_ID ClassID()
	{
		return Class_ID(0x1f527516, 0x2fc1118d);
	}

	/*
	*/
	const TCHAR *Category()
	{
		return _T("");
	}
};
extern NGMaxClassDesc _gNGMaxDesc;