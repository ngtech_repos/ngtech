/* Copyright (C) 2009-2017, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma comment(lib, "MaxCore.lib")
#pragma comment(lib, "maxutil.lib")
#pragma comment(lib, "geom.lib")
#pragma comment(lib, "mesh.lib")
#pragma comment(lib, "comctl32.lib")
#include "ClassDesc.h"

/**
_name must be with dll.
For Example: YourExportLibrary.dll
*/
HMODULE NGMaxDeveloperHub::LoadPluginLibrary(const wchar_t*_name)
{
	int pluginsDirCount = m_interface->GetPlugInEntryCount();

	HMODULE hLib = nullptr;
	if (!m_interface)
		return nullptr;

	for (int n = 0; n < pluginsDirCount; n++)
	{
		std::wstring pluginDirectory = m_interface->GetPlugInDir(n);
		pluginDirectory += L"\\";
		std::wstring libraryPath = pluginDirectory + _name;
		hLib = LoadLibrary(libraryPath.c_str());
		if (hLib)
			break;
	}

	return hLib;
}

/**
*/
int NGMaxDeveloperHub::DoExport(const TCHAR *name, ExpInterface *ei, Interface *i, BOOL suppressPromts, DWORD options)
{
	if (!ei || !i)
	{
		return IMPEXP_FAIL;
	}

	m_interface = i;

	auto hLib = (HINSTANCE)LoadPluginLibrary(L"NGMax-developer");

	auto ptr = (int(__cdecl *)(const TCHAR*, ExpInterface*, Interface*, BOOL, DWORD))GetProcAddress(hLib, "GetDoExport");

	if (!hLib)
		MessageBoxA(0, "Fatal error, can't load plugin dll", "ERROR", 0);

	if (ptr == nullptr)
	{
		FreeLibrary(hLib);
		return IMPEXP_FAIL;
	}

	ptr(name, ei, i, suppressPromts, options);

	FreeLibrary(hLib);

	return IMPEXP_SUCCESS;
}

/**
*/
BOOL NGMaxDeveloperHub::SupportsOptions(int ext, DWORD options)
{
	auto hLib = (HINSTANCE)LoadPluginLibrary(L"NGMax-developer");

	auto ptr = (BOOL(__cdecl *)(int, DWORD))GetProcAddress(hLib, "GetSupportsOptions");

	if (!hLib)
		MessageBoxA(0, "Fatal error, can't load plugin dll", "ERROR", 0);

	if (ptr == nullptr)
	{
		FreeLibrary(hLib);
		return IMPEXP_FAIL;
	}

	auto status = ptr(ext, options);

	FreeLibrary(hLib);
	return status;
}

/*
Plugin entry
*/
HINSTANCE hInstance;

/*
*/
BOOL WINAPI DllMain(HINSTANCE hinstDLL, ULONG fdwReason, LPVOID lpvReserved)
{
	static int controlsInit = FALSE;
	hInstance = hinstDLL;
	fdwReason = fdwReason;
	lpvReserved = lpvReserved;
	if (!controlsInit)
	{
		controlsInit = TRUE;
		InitCommonControls();
	}
	return TRUE;
}

/*
Number of plugins
*/
__declspec(dllexport) int LibNumberClasses()
{
	return 1;
}

/*
LibClassDesc
*/
__declspec(dllexport) ClassDesc *LibClassDesc(int i)
{
	return (i == 0) ? &_gNGMaxDesc : 0;
}

/*
LibDesc
*/
__declspec(dllexport) const TCHAR *LibDescription()
{
	return _T("NGMax export plugin");
}

/*
LibVersion
*/
__declspec(dllexport) ULONG LibVersion()
{
	return VERSION_3DSMAX;
}

/*
*/
__declspec(dllexport) ULONG CanAutoDefer()
{
	return 1;
}