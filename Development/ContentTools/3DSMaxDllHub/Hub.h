/* Copyright (C) 2009-2017, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include <max.h>
#include <stdmat.h>
#include <vector>
#include <Windows.h>

/**
NGMax
*/
class NGMaxDeveloperHub : public SceneExport
{
public:
	/*
	*/
	NGMaxDeveloperHub() {
		m_interface = nullptr;
	}

	/*
	*/
	virtual ~NGMaxDeveloperHub()
	{
	}

	/*
	*/
	int ExtCount()
	{
		return 1;
	}

	/*
	*/
	const TCHAR* Ext(int i)
	{
		if (i == 0)
			return _T("NGGF");

		return _T("");
	}

	/*
	*/
	const TCHAR* LongDesc()
	{
		return _T("NGMax mesh exporter");
	}

	/*
	*/
	const TCHAR* ShortDesc()
	{
		return _T("NGMax");
	}

	/*
	*/
	const TCHAR* AuthorName()
	{
		return _T("NG Games");
	}

	/*
	*/
	const TCHAR* CopyrightMessage()
	{
		return _T("Copyright (C) 2006-2015");
	}

	/*
	*/
	const TCHAR* OtherMessage1()
	{
		return _T("DEVELOPER-HUB_NULL");
	}

	/*
	*/
	const TCHAR* OtherMessage2()
	{
		return _T("DEVELOPER-HUB_NULL");
	}

	/*
	*/
	unsigned int Version()
	{
		return 0.1;
	}

	/*
	*/
	void ShowAbout(HWND hWnd)
	{
		MessageBoxA(hWnd, "About", "NGTech DEVELOPER-HUB", MB_OK);
	}

	/*
	*/
	BOOL SupportsOptions(int ext, DWORD options);

	/*
	*/
	int DoExport(const TCHAR *name, ExpInterface *ei, Interface *i, BOOL suppressPromts = FALSE, DWORD options = 0);

	HMODULE LoadPluginLibrary(const wchar_t*);
private:
	Interface* m_interface;
};