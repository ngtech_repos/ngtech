// Copy a file
#include <fstream>      // std::ifstream, std::ofstream

#include "../API/NGTechEngineAPI.h"
#include <stdmat.h>

using namespace NGTech;

#include "ConstructMaterial.h"

void ConstructMaterial(MaterialData& data)
{
	std::ofstream outfile(data.name.c_str());

	if (outfile.is_open())
	{
		outfile << "trans" << (data.AlphaValue == ALPHA_NONE) << endl;
		outfile << "diffTexture" << data.diffTexture.c_str() << endl;
		outfile << "normalTexture" << data.normalTexture.c_str() << endl;
		outfile << "specTexture" << data.specTexture.c_str() << endl;
		outfile << "glowTexture" << data.glowTexture.c_str() << endl;
		outfile << "reflTexture" << data.reflTexture.c_str() << endl;
		outfile << "refrTexture" << data.refrTexture.c_str() << endl;

		outfile << "opacityTexture" << data.opacityTexture.c_str() << endl;
		outfile << "filtercolorTexture" << data.filtercolorTexture.c_str() << endl;

		outfile << "displacementTexture" << data.displacementTexture.c_str() << endl;

		outfile << "specularLevelTexture" << data.specularLevelTexture.c_str() << endl;
		outfile << "glossinessTexture" << data.glossinessTexture.c_str() << endl;

		outfile.close();
	}
	else std::cout << "Unable to open file";
}

void ConstructMaterialList(const std::string &path,
	const std::vector<MaterialData> &materials,
	const MaterialData& withoutMaterials)
{
	std::ofstream outfile(path.c_str());

	for (auto i = 0; i < materials.size(); i++)
	{
		for (auto k = 0; k < materials[i].subsets.size(); k++)
		{
			outfile << materials[i].subsets[k].c_str() << " = " << materials[i].name.c_str() << endl;
		}
	}
	for (auto k = 0; k < withoutMaterials.subsets.size(); k++)
	{
		outfile << withoutMaterials.subsets[k].c_str() << " = " << "defmat.mat" << endl;
	}

	outfile.close();
}

