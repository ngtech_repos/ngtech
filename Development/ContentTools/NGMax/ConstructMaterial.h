#pragma once

#include <string>
#include <vector>

//material class
struct MaterialData
{
	std::string name;

	int AlphaValue;

	std::string diffTexture;
	std::string normalTexture;
	std::string specTexture;
	std::string glowTexture;
	std::string reflTexture;
	std::string refrTexture;

	std::string opacityTexture;
	std::string filtercolorTexture;

	std::string displacementTexture;

	std::string specularLevelTexture;
	std::string glossinessTexture;


	std::vector<std::string> subsets;
};

void ConstructMaterial(MaterialData& data);
void ConstructMaterialList(const std::string &path, const std::vector<MaterialData> &materials, const MaterialData& withoutMaterials);