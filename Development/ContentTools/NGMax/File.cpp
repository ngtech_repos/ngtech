/* Copyright (C) 2009-2017, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//***************************************************************************
#include <iostream>
#include <fstream>
#include <stdarg.h>
//***************************************************************************
#include "string.h"//memset for gcc
//***************************************************************************
#include "File.h"
//***************************************************************************
#if PLATFORM_OS == PLATFORM_OS_WINDOWS
#include  <io.h>
#include  <stdio.h>
#include  <stdlib.h>
#else
#include <unistd.h>
#endif
//***************************************************************************

/**
*/
File::File(const String & _name, FILE_MODE _mode, bool _notSearch)
	:mName(_name), mCurrentPos(0), mSize(0), mFile(nullptr)
{
	useSearch = true;
	memoryBuffer.clear();
	_OpenFile(_name, _mode, _notSearch);
}

/**
*/
File::~File() {
	if (mFile) fclose(mFile);
	mSize = 0;
	mCurrentPos = 0;
}

/**
*/
bool File::IsDataExist(bool _warn) {
	/*bool status = GetVFS()->isDataExist(mName);
	if ((!status) && _warn)
		Warning("File %s :is not exist", mName.c_str());
	return status;*/
	return false;
}

/**
*/
bool File::IsDataExist(const char* _name, bool _warn) {
	//bool status = GetVFS()->isDataExist(_name);
	////if ((!status) && _warn)
	//	//Warning("File %s :is not exist", _name);

	//return status;
	return false;
}

/**
*/
char* File::LoadFile()
{
	if ((!IsDataExist()) || (!mFile))
		return nullptr;

	//PROFILER_START(File::LoadFile);

	// obtain file size:
	fseek(mFile, 0, SEEK_END);
	mSize = ftell(mFile);
	rewind(mFile);

	// allocate memory to contain the whole file:
	memoryBuffer.resize(mSize);
	fseek(mFile, 0, SEEK_SET);
	fread(&memoryBuffer[0], sizeof(char), mSize, mFile);
	memoryBuffer.push_back('\0');

#if HEAVY_DEBUG
	Warning(memoryBuffer.data());
#endif

	//PROFILER_END();

	return memoryBuffer.data();
}

/**
*/
size_t File::FTell() {
	if (mFile)
		return ftell(mFile);
	return -1;
}

/**
*/
size_t File::FSeek(long offset, int mode)
{
	if (mFile)
		return fseek(mFile, offset, mode);
	return 1;
}

/**
*/
void File::Read(void *buf, int size, int count)
{
	if (mFile)
		fread(buf, size, count, mFile);

	if (!memoryBuffer.empty())
	{
		memcpy((char*)buf, (char*)(&memoryBuffer[0]) + mCurrentPos, size*count);
		mCurrentPos += size*count;
	}
}

/**
*/
bool File::IsEof() {
	if (mFile)
		return feof(mFile) != 0;

	if (!memoryBuffer.empty())
		return (mCurrentPos > mSize);

	return true;
}

/**
*/
void File::Write(void *buf, int size, int count)
{
	if (mFile)
	{
		fwrite(buf, size, count, mFile);
	}

	if (!memoryBuffer.empty())
	{
		memcpy((char*)(&memoryBuffer[0]) + mCurrentPos, (char*)buf, size*count);
		mCurrentPos += size*count;
	}
}

/**
*/
size_t File::Size()
{
	if (mSize == 0)
	{
		//DebugM("[FileSystem] loading file with zero size.All is correctly? Filename: %s", mName.c_str());
		LoadFile();
	}

	return mSize;
}

/**
*/
String File::GetLine()
{
	if (!mFile) return "";
	if (IsEof()) return "";

	String output = "";
	unsigned char h = fgetc(mFile);

	while ((h != '\n') && (!feof(mFile))) {
		output += h;
		h = fgetc(mFile);
	}
	return output;
}

/**
*/
String File::GetFileExt() {
	if (mName.size() == 0)
		return "UNKNOWN_EXT";

	int i = (int)mName.size() - 1;
	String buf;

	while ((mName[i] != '.') && (i > 0)) {
		buf = mName[i] + buf;
		i--;
	}
	return buf;
}

/**
*/
void File::WriteString(const String &text) {
	if (mFile)
		fprintf(mFile, "%s\n", text.c_str());
}

/**
*/
String File::CutFileExt() {
	if (mName.size() == 0)
		return "UNKNOWN_EXT";

	String buf;

	int i = 0;
	while ((mName[i] != '.') && (i < (int)mName.size())) {
		buf += mName[i];
		i++;
	}
	return buf;
}

/**
*/
void File::_OpenFile(const String&path, int _mode, bool _notSearch)
{
	//useSearch = !_notSearch;
	//if (_notSearch)
	{
		if (_mode == READ_TEXT)
			mFile = fopen(path.c_str(), "rt");
		else if (_mode == WRITE_TEXT)
			mFile = fopen(path.c_str(), "wt");
		else if (_mode == APPEND_TEXT)
			mFile = fopen(path.c_str(), "a+t");
		else if (_mode == READ_BINARY)
			mFile = fopen(path.c_str(), "rb");
		else if (_mode == WRITE_BINARY)
			mFile = fopen(path.c_str(), "wb");
		else if (_mode == APPEND_BINARY)
			mFile = fopen(path.c_str(), "a+b");
	}
	/*else {
		if (!IsDataExist())
			return;
		if (_mode == READ_TEXT)
			mFile = fopen(GetDataPath(), "rt");
		else if (_mode == READ_BIN)
			mFile = fopen(GetDataPath(), "rb");
		else if (_mode == WRITE_BIN)
			mFile = fopen(GetDataPath(), "wb");
	}*/
}

/**
*/
void File::ScanF(const char *format, ...)
{
	if (mFile)
	{
		va_list ap;
		va_start(ap, format);
		vfscanf(mFile, format, ap);
		va_end(ap);
	}
}

/**
*/
int File::FClose()
{
	if (mFile)
		return fclose(mFile);

	return 1;
}

/**
*/
bool File::HasAccess()
{
	/*return !FileUtil::fileExist(mName);*/
	return false;
}