/* Copyright (C) 2009-2017, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include <vector>

class File
{
public:
	enum FILE_MODE {
		READ_TEXT,
		WRITE_TEXT,
		APPEND_TEXT,

		READ_BINARY,
		WRITE_BINARY,
		APPEND_BINARY,
	};
public:
	explicit File(const String &_name, FILE_MODE _mode = WRITE_BINARY, bool _notSearch = false);
	~File();

	//Check what file exist,or write in log(if warn=true)
	bool IsDataExist(bool _warn = true);
	bool IsEof();

	/*Allocation memory in memorybuffer, and loading*/
	char* LoadFile();

	const char* GetDataPath() {
		return mName.c_str();
	}

	size_t Size();
	size_t FTell();
	size_t FSeek(long offset, int mode);

	String GetLine();

	String GetFileExt();
	String CutFileExt();

	//Revert 0 if all is normal
	int FClose();
	void Read(void *buf, int size, int count);
	void WriteString(const String &text);
	void Write(void *buf, int size, int count);
	void ScanF(const char * format, ...);

	__forceinline FILE* GetLowLevelFile() { return mFile; }
	__forceinline String GetName() { return mName; }
	__forceinline bool IsValid() { return HasAccess() && IsDataExist() && (mFile != nullptr); }
	bool HasAccess();

	static bool IsDataExist(const char* _name, bool _warn = true);
private:
	void _OpenFile(const String&path, int _mode, bool _notSearch);
private:
	bool useSearch;
	FILE *mFile;
	std::vector<char>memoryBuffer;
	String mName;
	size_t mSize;
	size_t mCurrentPos;
};