/* Copyright (C) 2009-2017, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include <max.h>

#include "NGMaxClassDesc.h"

using namespace NGMax;

extern NGMaxClassDesc *_gNGMaxDesc;
void RunCreationPlugin();

/**
*/
extern "C"
{
	//---------------------------------------------------------------------------
	__declspec(dllexport)
		__forceinline	int __cdecl GetDoExport(const TCHAR *name, ExpInterface *ei, Interface *i, BOOL suppressPromts, DWORD options)
	{
		return PluginActions::DoExport(name, ei, i, suppressPromts, options);
	}

	//---------------------------------------------------------------------------
	__declspec(dllexport)
		__forceinline	const TCHAR* __cdecl GetExt(int i)
	{
		return PluginActions::DoExt(i);
	}

	//---------------------------------------------------------------------------
	__declspec(dllexport)
		__forceinline	const TCHAR* __cdecl GetLongDesc()
	{
		return PluginActions::DoLongDesc();
	}

	//---------------------------------------------------------------------------
	__declspec(dllexport)
		__forceinline	const TCHAR* __cdecl GetShortDesc()
	{
		return PluginActions::DoShortDesc();
	}

	//---------------------------------------------------------------------------
	__declspec(dllexport)
		__forceinline	const TCHAR* __cdecl GetAuthorName()
	{
		return PluginActions::DoAuthorName();
	}

	//---------------------------------------------------------------------------
	__declspec(dllexport)
		__forceinline	const TCHAR* __cdecl GetCopyrightMessage()
	{
		return PluginActions::DoCopyrightMessage();
	}

	//---------------------------------------------------------------------------
	__declspec(dllexport)
		__forceinline	const TCHAR* __cdecl GetOtherMessage1()
	{
		return PluginActions::DoOtherMessage1();
	}

	//---------------------------------------------------------------------------
	__declspec(dllexport)
		__forceinline	const TCHAR* __cdecl GetOtherMessage2()
	{
		return PluginActions::DoOtherMessage2();
	}

	//---------------------------------------------------------------------------
	__declspec(dllexport)
		__forceinline	unsigned int __cdecl GetPluginVersion()
	{
		return PluginActions::DoVersion();
	}

	//---------------------------------------------------------------------------
	__declspec(dllexport)
		__forceinline	void __cdecl GetShowAbout(HWND _h)
	{
		PluginActions::DoShowAbout(_h);
	}

	//---------------------------------------------------------------------------
	__declspec(dllexport)
		__forceinline	BOOL __cdecl GetSupportsOptions(int ext, DWORD options)
	{
		return PluginActions::DoSupportsOptions(ext, options);
	}
}