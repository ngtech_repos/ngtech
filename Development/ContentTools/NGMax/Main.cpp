/* Copyright (C) 2009-2017, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include <max.h>
#include <stdmat.h>
#include <vector>

#pragma comment(lib, "MaxCore.lib")
#pragma comment(lib, "maxutil.lib")
#pragma comment(lib, "geom.lib")
#pragma comment(lib, "mesh.lib")
#pragma comment(lib, "comctl32.lib")

#include "NGMaxClassDesc.h"
#include "NGEnumProc.h"

using namespace NGMax;

//======================================================================================================
/**
Plugin entry
*/
NGMaxClassDesc*_gNGMaxDesc;
HINSTANCE hInstance;
int controlsInit = FALSE;
//======================================================================================================

void RunCreationPlugin()
{
	_gNGMaxDesc = new NGMaxClassDesc();
}

/*
*/
BOOL WINAPI DllMain(HINSTANCE hinstDLL, ULONG fdwReason, LPVOID lpvReserved)
{
	hInstance = hinstDLL;
	if (!controlsInit)
	{
		controlsInit = TRUE;
		InitCommonControls();
	}

#if (!defined(_MIXED_BUILD)) && (defined(NDEBUG))
	RunCreationPlugin();
#endif

	return TRUE;
}

/*
Number of plugins
*/
__declspec(dllexport) int LibNumberClasses()
{
	return 1;
}

/*
LibClassDesc
*/
__declspec(dllexport) ClassDesc *LibClassDesc(int i)
{
	return (i == 0) ? _gNGMaxDesc : 0;
}

/*
LibDesc
*/
__declspec(dllexport) const TCHAR *LibDescription()
{
	return _T("NGMax export plugin");
}

/*
LibVersion
*/
__declspec(dllexport) ULONG LibVersion()
{
	return VERSION_3DSMAX;
}

/*
*/
__declspec(dllexport) ULONG CanAutoDefer()
{
	return 1;
}