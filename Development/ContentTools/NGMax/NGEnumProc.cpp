/* Copyright (C) 2009-2017, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include <max.h>
#include <stdmat.h>
#include <vector>

#pragma comment(lib, "MaxCore.lib")
#pragma comment(lib, "maxutil.lib")
#pragma comment(lib, "geom.lib")
#pragma comment(lib, "mesh.lib")
#pragma comment(lib, "comctl32.lib")

#include "NGMaxClassDesc.h"
#include "NGEnumProc.h"

/**
*/
void ConstructMaterial(const char* materialName, const char* textureFile)
{
}

namespace NGMax
{
	/**
	*/
	void NGEnumProc::Export(const char *name)
	{
		ReadModel();
		SaveXSMSH(name);
		SaveMaterials(name);
		SaveMaterialList(name);
	}

	/**
	*/
	int NGEnumProc::callback(INode *node)
	{
		if (selected && node->Selected() == FALSE)
		{
			return TREE_CONTINUE;
		}
		nodes.push_back(node);
		return TREE_CONTINUE;
	}

	/**
	*/
	void NGEnumProc::ReadModel()
	{
		try
		{
			//Process model
			for (auto n = 0; n < nodes.size(); n++)
			{
				Subset subset;
				subset.node = nodes[n];;

				int d;
				subset.object = NODE2OBJ(subset.node, d);
				if (!subset.object)
					continue;

				auto obj = subset.object;

				//name
				subset.name = getstring(subset.node->GetName());

				//process geometry
				Matrix3 tm = subset.node->GetObjTMAfterWSM(iface->GetTime());
				Matrix3 nm = tm;
				nm.NoScale();
				nm.NoTrans();

				//get bBox
				Box3 box;
				obj->mesh.buildBoundingBox();
				box = obj->mesh.getBoundingBox(&tm);
				subset.min = box.Min();
				subset.max = box.Max();

				//build normals
				obj->mesh.buildNormals();

				//additional texcoord
				int mp = -1;
				for (int i = 2; i < MAX_MESHMAPS - 1; i++)
				{
					if (!obj->mesh.mapSupport(i))
					{
						mp = i - 1;
						break;
					}
				}

				if (mp > 0)
				{
					unwrappedTexcoord = true;
				}

				for (int i = 0; i < obj->mesh.numFaces; i++)
				{
					Face *f = &obj->mesh.faces[i];
					if (!f)
						continue;

					TVFace *tf = &obj->mesh.tvFace[i];
					if (!tf)
						continue;

					//Nick:BUG �� �������� ��� ���� ���������
					//BUG �� ��� ���� ���������� ������� �����
					TVFace *tfu = nullptr;
					if (unwrappedTexcoord)
					{
						tfu = &obj->mesh.mapFaces(mp)[i];
					}

					for (int j = 0; j < 3; j++)
					{
						Vertex vertex;

						Point3 v = tm * obj->mesh.verts[f->v[j]];
						vertex.position.x = v.x;
						vertex.position.y = v.y;
						vertex.position.z = v.z;

						vertex.texcoord.x = obj->mesh.tVerts[tf->t[j]].x;
						vertex.texcoord.y = obj->mesh.tVerts[tf->t[j]].y;

						if (unwrappedTexcoord)
						{
							vertex.unwrappedTexcoord.x = obj->mesh.mapVerts(mp)[tfu->t[j]].x;
							vertex.unwrappedTexcoord.y = obj->mesh.mapVerts(mp)[tfu->t[j]].y;
						}

						//read normal
						bool specifiedNormal = true;

						RVertex *rv = obj->mesh.getRVertPtr(f->v[j]);
						int nnormals = 0;

						if (rv->rFlags & SPECIFIED_NORMAL)
						{
							Point3 normal = nm * rv->rn.getNormal();
							vertex.normal.x = normal.x;
							vertex.normal.y = normal.y;
							vertex.normal.z = normal.z;
						}
						else
						{
							if ((nnormals = rv->rFlags & NORCT_MASK) && f->smGroup)
							{
								if (nnormals == 1)
								{
									Point3 normal = nm * rv->rn.getNormal();
									vertex.normal.x = normal.x;
									vertex.normal.y = normal.y;
									vertex.normal.z = normal.z;
								}
								else
								{
									for (int l = 0; l < nnormals; l++)
									{
										if (rv->ern[l].getSmGroup() & f->smGroup)
										{
											Point3 normal = nm * rv->ern[l].getNormal();
											vertex.normal.x = normal.x;
											vertex.normal.y = normal.y;
											vertex.normal.z = normal.z;
										}
									}
								}
							}
							else
							{
								specifiedNormal = false;
								Point3 normal = nm * obj->mesh.getFaceNormal(i);
								vertex.normal.x = normal.x;
								vertex.normal.y = normal.y;
								vertex.normal.z = normal.z;
							}
						}

						//make indices
						int index = -1;
						for (int k = 0; k < subset.vertices.size(); k++)
						{
							if ((subset.vertices[k].position == vertex.position) &&
								(subset.vertices[k].texcoord == vertex.texcoord))
							{
								if (unwrappedTexcoord && subset.vertices[k].unwrappedTexcoord == vertex.unwrappedTexcoord)
								{
									if (specifiedNormal)
									{
										if (subset.vertices[k].normal == vertex.normal)
											index = k;
									}
									else
									{
										index = k;
										subset.vertices[k].normal += vertex.normal;
									}
								}

								if (!unwrappedTexcoord)
								{
									if (specifiedNormal)
									{
										if (subset.vertices[k].normal == vertex.normal)
											index = k;
									}
									else
									{
										index = k;
										subset.vertices[k].normal += vertex.normal;
									}
								}
							}
						}

						if (index > -1)
						{
							subset.indices.push_back(index);
						}
						else
						{
							subset.indices.push_back(subset.vertices.size());
							subset.vertices.push_back(vertex);
						}
					}
				}
				//normalize normals
				for (auto k = 0; k < subset.vertices.size(); k++)
				{
					subset.vertices[k].normal = subset.vertices[k].normal.Normalize();
				}
				subsets.push_back(subset);
			}

			if (subsets.empty())
				return;

			//Get position
			Point3 smin = subsets[0].min;
			Point3 smax = subsets[0].max;

			for (auto s = 1; s < subsets.size(); s++)
			{
				smin.x = min(smin.x, subsets[s].min.x);
				smin.y = min(smin.y, subsets[s].min.y);
				smin.z = min(smin.z, subsets[s].min.z);

				smax.x = max(smax.x, subsets[s].max.x);
				smax.y = max(smax.y, subsets[s].max.y);
				smax.z = max(smax.z, subsets[s].max.z);
			}

			this->pos = (smin + smax);
			this->pos.x = this->pos.x * 0.5;
			this->pos.y = this->pos.y * 0.5;
			this->pos.z = this->pos.z * 0.5;
		}
		catch (std::exception &e)
		{
			MessageBoxA(0, e.what(), "Failed exporting model in function [ReadModel]", 0);
		}
	}

	/**
	*/
	void NGEnumProc::SaveXSMSH(const String &name)
	{
		try
		{
			//Save mesh
			File *file = new File(name, File::WRITE_BINARY);

			//header
			unsigned int header;
			if (unwrappedTexcoord)
				header = MESH_HEADER_UNWRAPPED;
			else
				header = MESH_HEADER;

			file->Write(&header, sizeof(unsigned int), 1);

			//num_subsets
			unsigned int numSubsets = subsets.size();
			file->Write(&numSubsets, sizeof(unsigned int), 1);

			//subsets
			for (int s = 0; s < subsets.size(); s++)
			{
				Subset *st = &subsets[s];
				WriteName(st->name, file);

				//num vertices
				unsigned int numVertices = st->vertices.size();
				file->Write(&numVertices, sizeof(unsigned int), 1);

				//process vertices
				for (unsigned int v = 0; v < st->vertices.size(); v++)
				{
					Point3 vpositon = (st->vertices[v].position - pos) * zySwap;
					Point3 vnormal = (st->vertices[v].normal) * zySwap;

					file->Write(&st->vertices[v].position.x, sizeof(float), 1);
					file->Write(&st->vertices[v].position.z, sizeof(float), 1);
					file->Write(&st->vertices[v].position.y, sizeof(float), 1);
					file->Write(&st->vertices[v].normal.x, sizeof(float), 1);
					file->Write(&st->vertices[v].normal.z, sizeof(float), 1);
					file->Write(&st->vertices[v].normal.y, sizeof(float), 1);
					file->Write(&st->vertices[v].texcoord.x, sizeof(float), 1);
					file->Write(&st->vertices[v].texcoord.y, sizeof(float), 1);

					if ((unwrappedTexcoord) && (st->vertices[v].unwrappedTexcoord))
					{
						file->Write(&st->vertices[v].unwrappedTexcoord.x, sizeof(float), 1);
						file->Write(&st->vertices[v].unwrappedTexcoord.y, sizeof(float), 1);
					}
				}

				//number of indices
				unsigned int numIndices = st->indices.size();
				file->Write(&numIndices, sizeof(unsigned int), 1);

				//process indices
				for (unsigned int i = 0; i < numIndices / 3; i++)
				{
					file->Write(&st->indices[i * 3 + 0], sizeof(unsigned int), 1);
					file->Write(&st->indices[i * 3 + 2], sizeof(unsigned int), 1);
					file->Write(&st->indices[i * 3 + 1], sizeof(unsigned int), 1);
				}
			}
			delete file;
		}
		catch (std::exception &e)
		{
			MessageBoxA(0, e.what(), "Failed saving file. In function [SaveXSMSH]", 0);
		}
	}

	/**
	*/
	void NGEnumProc::SaveMaterials(const String &_name)
	{
		try
		{
			//materials
			for (auto s = 0; s < subsets.size(); s++)
			{
				Subset *st = &subsets[s];

				//Save material
				Mtl *m = st->node->GetMtl();

				// �� ���������� ��������� ��� �� ���������
				if (!m)
					continue;

				Texmap *tmap = m->GetSubTexmap(ID_DI);

#pragma message("��� ���")
				//http://www.hugi.scene.org/online/coding/hugi%2030%20-%20coding%20rand0m%20own%20exporter%20plug-in%20for%203ds%20max.htm
				//
				if (tmap && tmap->ClassID() == Class_ID(BMTEX_CLASS_ID, 0))
				{
					BitmapTex *bmt = (BitmapTex*)tmap;

					String textureName = _strlwr((char*)getstring(bmt->GetMapName()).c_str());
					ReplaceSpaces(textureName);

					auto textureFile = ExtractFileName(textureName);

					String materialName = textureFile.CutFileExt() + ".mat";
					//TODO:Check on existing of file
					{}

					//find existing
					bool found = false;
					for (int k = 0; k < materials.size(); k++)
					{
						if (materials[k].name == materialName)
						{
							materials[k].subsets.push_back(subsets[s].name);
							found = true;
						}
					}

					if (!found)
					{
						MaterialData material;
						material.name = materialName;
						material.subsets.push_back(subsets[s].name);
						materials.push_back(material);

						ConstructMaterial(materialName.c_str(), textureName.c_str());
					}

					OutputDebugStringA(materialName);
				}
				else
				{
					withoutMaterials.subsets.push_back(subsets[s].name);
				}
			}
		}
		catch (std::exception &e)
		{
			MessageBoxA(0, e.what(), "Failed exporting materials in function [SaveMaterials]", 0);
		}
	}

	/**
	*/
	void NGEnumProc::SaveMaterialList(const String &name)
	{
		String path = name;
		path = path.CutFileExt();
		path = path + ".matls";

		FILE *fmtr = fopen(path, "wt");
		for (auto i = 0; i < materials.size(); i++)
		{
			for (auto k = 0; k < materials[i].subsets.size(); k++)
			{
				fprintf(fmtr, "%s = %s\n", materials[i].subsets[k].c_str(), materials[i].name.c_str());
			}
		}
		for (auto k = 0; k < withoutMaterials.subsets.size(); k++)
		{
			fprintf(fmtr, "%s = %s\n", withoutMaterials.subsets[k].c_str(), "defmat.mat");
		}
		fclose(fmtr);
	}

	/**
	*/
	TriObject *NGEnumProc::NODE2OBJ(INode *node, int &deleteIt)
	{
		deleteIt = false;
		Object *obj = node->EvalWorldState(iface->GetTime()).obj;

		if (obj->CanConvertToType(Class_ID(TRIOBJ_CLASS_ID, 0)))
		{
			TriObject *tri = (TriObject *)obj->ConvertToType(iface->GetTime(),
				Class_ID(TRIOBJ_CLASS_ID, 0));
			if (obj != tri) deleteIt = true;
			return tri;
		}
		else
		{
			return NULL;
		}
	}
}