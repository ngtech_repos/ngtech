/* Copyright (C) 2009-2017, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include "String.h"
#include "File.h"
#include "../Shared/Shared.h"
using namespace Shared;

namespace NGMax
{
	/**
	NGmax ENUM PROC
	*/
	class NGEnumProc : public ITreeEnumProc
	{
	public:
		NGEnumProc(Interface *i)
		{
			iface = i;
		}

		/*
		*/
		int callback(INode *node);
		void Export(const char *name);
		void ReadModel();
		void SaveXSMSH(const String &name);
		void SaveMaterials(const String &name);
		void SaveMaterialList(const String &name);

		/*
		*/
		TriObject *NODE2OBJ(INode *node, int &deleteIt);

		Interface *iface;
		bool selected;

		std::vector<INode*> nodes;
		std::vector<Subset> subsets;
		std::vector<MaterialData> materials;
		MaterialData withoutMaterials;
		Point3 pos;

		bool unwrappedTexcoord;
	};
}