/* Copyright (C) 2009-2017, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include <max.h>
#include <stdmat.h>
#include <vector>

#include "String.h"
#include "../Shared/Shared.h"
using namespace Shared;

#include "NGEnumProc.h"
#include "NGMax.h"

using namespace NGMax;
using namespace NGTech;

namespace NGMax
{
	/**
	*/
	NGMaxImpl::NGMaxImpl() { }

	/**
	*/
	int NGMaxImpl::DoExport(const TCHAR *name, ExpInterface *ei, Interface *i, BOOL suppressPromts, DWORD options)
	{
		return PluginActions::DoExport(name, ei, i, suppressPromts, options);
	}

	int PluginActions::DoExport(const TCHAR *name, ExpInterface *ei, Interface *i, BOOL suppressPromts, DWORD options)
	{
		NGEnumProc xProc(i);
		xProc.selected = (options & SCENE_EXPORT_SELECTED) ? TRUE : FALSE;

		ei->theScene->EnumTree(&xProc);
#ifdef _UNICODE
		auto str = getstring(name);
		xProc.Export(str.c_str());
#else
		xProc.Export(name);
#endif

		MessageBoxA(0, "Mesh was succesfully exported!", "Success!", MB_OK);
		return 1;
	}

	const TCHAR* PluginActions::DoExt(int i)
	{
		if (i == 0)
			return _T("NGGF");

		return _T("");
	}

	const TCHAR* PluginActions::DoLongDesc()
	{
		return _T("NGMax mesh exporter");
	}

	const TCHAR* PluginActions::DoShortDesc()
	{
		return _T("NGMax");
	}

	const TCHAR* PluginActions::DoAuthorName()
	{
		return _T("NG Games");
	}

	const TCHAR* PluginActions::DoCopyrightMessage()
	{
		return _T("Copyright (C) 2006-2015");
	}

	const TCHAR* PluginActions::DoOtherMessage1()
	{
		return _T("");
	}

	const TCHAR* PluginActions::DoOtherMessage2()
	{
		return _T("");
	}

	void PluginActions::DoShowAbout(HWND _h)
	{
		MessageBoxA(_h, "About", "NGTech Engine mesh exporter", MB_OK);
	}

	BOOL PluginActions::DoSupportsOptions(int ext, DWORD options)
	{
		return (options == SCENE_EXPORT_SELECTED) ? TRUE : FALSE;
	}
}