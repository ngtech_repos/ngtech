/* Copyright (C) 2009-2017, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include "../Src/Common/Singleton.h"
using namespace NGTech;

namespace NGMax
{
	/**
	Plugin Actions - used for Developers Hub
	*/
	struct PluginActions
	{
		/*
		*/
		static int DoExport(const TCHAR *name, ExpInterface *ei, Interface *i, BOOL suppressPromts = FALSE, DWORD options = 0);
		static const TCHAR* DoExt(int i);
		static const TCHAR* DoLongDesc();
		static const TCHAR* DoShortDesc();
		static const TCHAR* DoAuthorName();
		static const TCHAR* DoCopyrightMessage();
		static const TCHAR* DoOtherMessage1();
		static const TCHAR* DoOtherMessage2();
		static int DoVersion() { return 0.1; }
		static void DoShowAbout(HWND hWnd);
		static BOOL DoSupportsOptions(int ext, DWORD options);
	};

	/**
	NGMax
	*/
	class NGMaxImpl : public SceneExport
	{
	public:
		/*
		*/
		NGMaxImpl();

		/*
		*/
		virtual ~NGMaxImpl()
		{
		}

		/*
		*/
		int ExtCount()
		{
			return 1;
		}

		/*
		*/
		const TCHAR* Ext(int i)
		{
			return PluginActions::DoExt(i);
		}

		/*
		*/
		const TCHAR* LongDesc()
		{
			return PluginActions::DoLongDesc();
		}

		/*
		*/
		const TCHAR* ShortDesc()
		{
			return PluginActions::DoShortDesc();
		}

		/*
		*/
		const TCHAR* AuthorName()
		{
			return PluginActions::DoAuthorName();
		}

		/*
		*/
		const TCHAR* CopyrightMessage()
		{
			return PluginActions::DoCopyrightMessage();
		}

		/*
		*/
		const TCHAR* OtherMessage1()
		{
			return PluginActions::DoOtherMessage1();
		}

		/*
		*/
		const TCHAR* OtherMessage2()
		{
			return PluginActions::DoOtherMessage1();
		}

		/*
		*/
		unsigned int Version()
		{
			return PluginActions::DoVersion();
		}

		/*
		*/
		void ShowAbout(HWND hWnd)
		{
			PluginActions::DoShowAbout(hWnd);
		}

		/*
		*/
		BOOL SupportsOptions(int ext, DWORD options)
		{
			return PluginActions::DoSupportsOptions(ext, options);
		}

		/*
		*/
		int DoExport(const TCHAR *name, ExpInterface *ei, Interface *i, BOOL suppressPromts = FALSE, DWORD options = 0);
	};
}