/* Copyright (C) 2009-2017, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//**************************************
#include <string>
//**************************************

/**
std::string
*/
class String : public std::string
{
public:
	/**
	String constructor
	*/
	String();

	/**
	String constructor
	*/
	String(const char *str);

	/**
	String constructor
	*/
	String(const std::string &str);

	/**
	String constructor
	*/
	String(int i);

	/*
	String constructor
	*/
	String(unsigned int i);

	/**
	String constructor
	*/
	String(float i);

	/**
	String constructor
	*/
	String(double i);

	/**
	String constructor
	*/
	String(bool i);

	/**
	Gets word from String. Words are separated by space
	*/
	String GetWord(int n);

	/**
	Gets quoted word from String. Words are separated by space and quoted word can contain spaces
	*/
	String GetQuotedWord(int n);

	/**
	Gets words count. Words are separated by space
	*/
	int GetWordCount();

	/**
	Cuts file ext from file name
	*/
	String CutFileExt();

	/**
	Gets file ext from file name
	*/
	String GetFileExt();

	/**
	Converts String to int
	*/
	int ToInt();

	/**
	Converts String to float
	*/
	float ToFloat();

	/**
	Converts String to double
	*/
	double ToDouble();

	/**
	Converts String to bool
	*/
	bool ToBool();

	/**
	strstr analog
	*/
	bool Strstr(const String &line);

	/**
	sprintf analog
	*/
	void Printf(const String &format, ...);

	/**
	�������� � �����
	*/
	void Tail(size_t length);

	/**
	Converts String to const char *
	*/
	operator const char*() const;
};

std::string getstring(const wchar_t* wstr);