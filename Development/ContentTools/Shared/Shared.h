#pragma once

#include "../NGMax/File.h"

namespace Shared
{
	//name buffer
#define NAME_SIZE 64
	static char nameBuffer[NAME_SIZE];

	//write name to file
	static void WriteName(const String &name, File *file)
	{
		memset(nameBuffer, 0, sizeof(nameBuffer));
		strcpy(nameBuffer, name.c_str());
		file->Write(nameBuffer, sizeof(nameBuffer), 1);
	}

	//replaces ' ' with '_'
	static void ReplaceSpaces(String &str)
	{
		char buffer[1024];
		strcpy(buffer, str.c_str());

		int i = 0;
		while (i < strlen(buffer))
		{
			if (buffer[i] == ' ')
				buffer[i] = '_';
			i++;
		}

		str.assign(buffer);
	}

	/**
	*/
	static String ExtractFileName(String& filepath)
	{
		if (filepath.empty()) return "";

		int i = filepath.size();
		String buf;
		buf.clear();
		while ((filepath[i] != '\\') && (i > 0))
		{
			buf = filepath[i--] + buf;
		}
		return buf;
	}

	/*
	Mesh
	*/

	//header
#define MESH_HEADER ('x' | 's' << 8 | 'm' << 16 | 's' << 24)
#define MESH_HEADER_UNWRAPPED ('x' | 's' << 8 | 'm' << 16 | 'u' << 24)

	//
	static Matrix3 zySwap = Matrix3(Point3(1, 0, 0), Point3(0, 0, 1), Point3(0, 1, 0), Point3(0, 0, 0));

	//vertex class
	struct Vertex
	{
		Point3 position;
		Point3 normal;
		Point2 texcoord;
		Point2 unwrappedTexcoord;
	};

	//subset class
	struct Subset
	{
		std::vector<Vertex> vertices;
		std::vector<unsigned int> indices;
		String name;

		INode *node;
		TriObject *object;

		Point3 min;
		Point3 max;
	};

	//material class
	struct MaterialData
	{
		String name;
		String texture;

		std::vector<String> subsets;
	};
}