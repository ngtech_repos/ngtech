/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#ifndef _BUILD_DEFINES_H
#define _BUILD_DEFINES_H

/**
*/
#ifndef ENGINE_VERSION_NUMBER
#define ENGINE_VERSION_NUMBER 0.7.3
#endif

#ifndef ENGINE_VERSION_STRING
#define ENGINE_VERSION_STRING "0.7.3"
#endif

// For android - in currently moment, we will write all dialogs
#if (defined (_DEBUG) || defined (_MIXED)) && (!IS_OS_ANDROID)

#ifndef _ENGINE_DEBUG_
#define _ENGINE_DEBUG_ 1
#endif

#else

#ifndef ENGINE_RELEASE
#define ENGINE_RELEASE 1
#endif

#ifndef _FINALRELEASE
#define _FINALRELEASE 1
#endif

#endif

//***************************************************************************
// Platform Configs
#if (!IS_OS_WINDOWS)

#define HAVE_NVAPI 0
#define HAVE_AMDADL 0
#define USE_OWN_MINIDUMP 0
#define USE_STEAMWORKS 0
#define USE_PROFILER 0

#ifndef MT_INSTRUMENTED_BUILD
#define MT_INSTRUMENTED_BUILD USE_PROFILER
#endif

#endif

//***************************************************************************
#ifndef HAVE_NVAPI
#define HAVE_NVAPI 1
#endif

//***************************************************************************
#ifndef HAVE_AMDADL
#define HAVE_AMDADL 1
#endif

//***************************************************************************
#ifndef USE_OWN_MINIDUMP
#define USE_OWN_MINIDUMP 1
#endif
#endif

//***************************************************************************
#ifndef USE_STEAMWORKS
//#define USE_STEAMWORKS 1
#endif

//***************************************************************************
#if ((!(ENGINE_RELEASE)) && (!(_FINALRELEASE) || (EMULATE_DEVELOPER_FINAL_RELEASE)) && !defined(NGTECH_STATIC_LIBS))
#ifndef USE_PROFILER
#define USE_PROFILER 1
#endif

//#pragma message ("Profiling is enabled")

#else
#ifndef USE_PROFILER
#define USE_PROFILER 0
#endif

//#pragma message ("Profiling is disabled")

//#ifndef DROP_EDITOR
//#define DROP_EDITOR 1
//#endif

#endif

//***************************************************************************
#ifndef USE_BROFILER
#define USE_BROFILER USE_PROFILER

#ifndef MT_INSTRUMENTED_BUILD
#define MT_INSTRUMENTED_BUILD USE_PROFILER
#endif

#ifndef MT_ENABLE_BROFILER_SUPPORT
#define MT_ENABLE_BROFILER_SUPPORT USE_BROFILER
#endif

#endif
//***************************************************************************

// ENABLE_PVD - PhysX Visual Debugger
#ifndef ENABLE_PVD
//#define ENABLE_PVD 1
#endif

//***************************************************************************

#ifndef _DEMO_BUILD
//#define _DEMO_BUILD 1
#endif

#ifndef PROJECT_TITLE
#define PROJECT_TITLE "NGTech Engine"
#endif