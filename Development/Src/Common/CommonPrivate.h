/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//STL and types
#include <assert.h>
//***************************************************************************
#include "../Platform/inc/PlatformPrivate.h"
//***************************************************************************

// disable: warning C4275: non dll-interface class '***' used as base for dll-interface clas '***'
#pragma warning (disable : 4275)

#include "NonCopyable.h"
#include "EngineState.h"
#include "Threadable.h"
#include "Statistic.h"
#include "StringHelper.h"
#include "FileHelper.h"
#include "MathLib.h"
#include "IRender.h"
#include "I_MeshFormat.h"
#include "I_MeshFormatSkinned.h"
#include "IGame.h"
#include "IResource.h"
#include "DebugLayer.h"
#include "SceneMatrixes.h"
#include "BSphere.h"
#include "BBox.h"
#include "orderedarray.h"
#include "orderedmultiarray.h"
#include "NList.h"
#include "mcplist.h"
#include "SharedStructures.h"
#include "ScriptManager.h"