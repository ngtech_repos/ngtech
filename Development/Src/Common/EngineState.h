/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#ifndef ENGINE_STATE_H
#define ENGINE_STATE_H 1

#pragma once

namespace NGTech
{
	enum EngineState
	{
		STATE_GAME = 1 << 0, // 1
		STATE_EDITOR = 1 << 1, // 2
		STATE_EDITOR_RUNNING = STATE_EDITOR | STATE_GAME, //3

		STATE_PHYSICS_PAUSED = 1 << 2, // 4
		STATE_AUDIO_PAUSED = 1 << 3, // 8

		STATE_PAUSED = STATE_PHYSICS_PAUSED | STATE_AUDIO_PAUSED, //12+
		STATE_LOADING = STATE_PAUSED + 1,//13
		STATE_EDITOR_PAUSED = STATE_EDITOR | STATE_PAUSED //14 +
	};

	void SetEngineState(EngineState _state);
	EngineState GetEngineState();
}

#endif