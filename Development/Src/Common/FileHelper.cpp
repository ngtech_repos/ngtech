/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "CommonPrivate.h"
//***************************************************************************
#if defined(_WIN32)
#include <io.h>//_access
#include <direct.h> //_mkdir
#else
#include <unistd.h>
#include <sys/stat.h>
#endif
//***************************************************************************
#include "FileHelper.h"
#include "UString.h"
//***************************************************************************

namespace NGTech {
	/**
	*/
	void FileUtil::WriteString(FILE *file, const String &text) {
		fprintf(file, "%s\n", text.c_str());
	}

	/**
	*/
	const char* FileUtil::GetExt(const char *filepath) {
		const char *str = filepath;
		size_t len = strlen(filepath);
		for (const char* p = str + len - 1; p >= str; --p)
		{
			switch (*p)
			{
			case ':':
			case '/':
			case '\\':
				// we've reached a path separator - it means there's no extension in this name
				return "";
			case '.':
				// there's an extension in this file name
				return p + 1;
			}
		}
		return "";
	}

	/**
	*/
	bool FileUtil::FileExist(const String &path) {
		return DirectoryIsExist(path) != -1;
	}

	/**
	*/
	int FileUtil::DirectoryIsExist(const String& _path)
	{
		UString path(_path);
#ifdef _UNICODE
		return _waccess(path.asWStr_c_str(), 00);
#else
		return access(path.asUTF8_c_str(), 00);
#endif
	}

	/**
	*/
	bool FileUtil::CreateDirectoryIfNotExist(const String & _path)
	{
		if (DirectoryIsExist(_path) == -1)
		{
			UtilCreateDirectory(_path);
			return true;
		}
		return false;
	}

	/**
	*/
	void FileUtil::UtilCreateDirectory(const String & _path)
	{
		UString path(_path);
#if PLATFORM_OS == PLATFORM_OS_WINDOWS
#ifdef _UNICODE
		_wmkdir(path.asWStr_c_str());
#else
		_mkdir(path.asUTF8_c_str());
#endif
#else
#ifdef _UNICODE
		wmkdir(path.asWStr_c_str(), 0777);
#else
		mkdir(path.asUTF8_c_str(), 0777);
#endif
#endif
	}
}