/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#ifndef FILE_HELPER_H
#define FILE_HELPER_H 1

//**************************************
#include "StringHelper.h"
//**************************************
#include <stdio.h>
#include <string.h>

namespace NGTech {
	//!@todo CHECK ON DEPRECATED!
	//---------------------------------------------------------------------------
	//Desc: some important File functions
	//---------------------------------------------------------------------------
	struct FileUtil {
		static void WriteString(FILE *file, const String &text);

		/**
		! Extract extension from full specified file path
		! Returns pointer to the extension (without .) or pointer to an empty 0-terminated string
		*/
		static const char* GetExt(const char *filepath);
		static bool FileExist(const String &path);

		/**
		! add a backslash if needed
		*/
		static ENGINE_INLINE String AddSlash(const String &path)
		{
			if (path.empty() || path[path.length() - 1] == '/')
				return path;
			if (path[path.length() - 1] == '\\')
				return path.substr(0, path.length() - 1) + "/";
			return path + "/";
		}

		/**
		! Replace extension for given file.
		*/
		static ENGINE_INLINE void RemoveExtension(String &filepath)
		{
			const char *str = filepath.c_str();
			for (const char* p = str + filepath.length() - 1; p >= str; --p)
			{
				switch (*p)
				{
				case ':':
				case '/':
				case '\\':
					// we've reached a path separator - it means there's no extension in this name
					return;
				case '.':
					// there's an extension in this file name
					filepath = filepath.substr(0, p - str);
					return;
				}
			}
			// it seems the file name is a pure name, without path or extension
		}

		/**
		! Extract file name with extension from full specified file path.
		*/
		static ENGINE_INLINE String GetFileName(const String &filepath)
		{
			String file = filepath;
			RemoveExtension(file);
			return GetFile(file);
		}

		/**
		! Extract file name with extension from full specified file path.
		*/
		static ENGINE_INLINE String GetFile(const String &filepath)
		{
			const char *str = filepath.c_str();
			for (const char* p = str + filepath.length() - 1; p >= str; --p)
			{
				switch (*p)
				{
				case ':':
				case '/':
				case '\\':
					return filepath.substr(p - str + 1);
				}
			}
			return filepath;
		}

		/**
		*/
		static ENGINE_INLINE const char* GetFile(const char* filepath)
		{
			const size_t len = strlen(filepath);
			for (const char* p = filepath + len - 1; p >= filepath; --p)
			{
				switch (*p)
				{
				case ':':
				case '/':
				case '\\':
					return p + 1;
				}
			}
			return filepath;
		}

		/**
		! Replace extension for given file.
		*/
		static ENGINE_INLINE String ReplaceExtension(const String& filepath, const char* ext)
		{
			String str = filepath;
			if (ext != 0)
			{
				RemoveExtension(str);
				if (ext[0] != 0 && ext[0] != '.')
				{
					str += ".";
				}
				str += ext;
			}
			return str;
		}

		/**
		! Replace extension for given file.
		*/
		static ENGINE_INLINE String ReplaceExtension(const char* filepath, const char* ext)
		{
			return ReplaceExtension(String(filepath), ext);
		}

		/**
		! Makes a fully specified file path from path and file name.
		*/
		static ENGINE_INLINE String Make(const String &path, const String &file)
		{
			return AddSlash(path) + file;
		}

		/**
		! Makes a fully specified file path from path and file name.
		*/
		static ENGINE_INLINE String Make(const String &dir, const String &filename, const String &ext)
		{
			String path = ReplaceExtension(filename, ext.c_str());
			path = AddSlash(dir) + path;
			return path;
		}

		/**
		! Makes a fully specified file path from path and file name.
		*/
		static ENGINE_INLINE String Make(const String &dir, const String &filename, const char* ext)
		{
			return Make(dir, filename, String(ext));
		}

		/**
		! Check existing directory
		! Returns -1 if not exist
		*/
		static int DirectoryIsExist(const String& _path);

		/**
		! Check existing directory and create if not exist
		! Returns true if successfully
		*/
		static bool CreateDirectoryIfNotExist(const String& _path);

		/**
		! Creates directory using _mkdir
		*/
		static void UtilCreateDirectory(const String& _path);
	};
};

#endif // FILE_HELPER_H