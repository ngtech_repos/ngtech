/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/

#pragma once

//***************************************************************************

namespace NGTech {
	class Vec3;
	//---------------------------------------------------------------------------
	//Desc: Engine`s main sound system. Created one time
	//---------------------------------------------------------------------------
	struct I_Audio {
	public:
		virtual ~I_Audio() {}
		virtual void Initialise() = 0;
		virtual void Update() = 0;
		virtual void SetListener(Vec3 pos, Vec3 dir) = 0;
	protected:
		virtual String GetVendor() = 0;
		virtual String GetRenderer() = 0;
		virtual String GetVersion() = 0;
		virtual String GetExtensions() = 0;
	};
};