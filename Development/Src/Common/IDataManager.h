/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include "Singleton.h"

namespace NGTech
{
	struct IDataStream
	{
		virtual ~IDataStream() { }

		virtual bool Eof() { return false; }
		virtual size_t Size() { return 0; }
		virtual void Readline(String& _source, unsigned int _delim = '\n') {}
		virtual size_t Read(void* _buf, size_t _count) { return 0; }
	};

	struct DataManager
	{
		/** Get data stream from specified resource name.
		@param _name Resource name (usually file name).
		*/
		virtual IDataStream* GetData(const String& _name) { return nullptr; }

		/** Free data stream.
		@param _data Data stream.
		*/
		virtual void FreeData(IDataStream* _data) {}

		/** Is data with specified name exist.
		@param _name Resource name.
		*/
		virtual bool IsDataExist(const String& _name) { return false; }

		/** Get all data names with names that matches pattern.
		@param _pattern Pattern to match (for example "*.layout").
		*/
		virtual const VectorString& GetDataListNames(const String& _pattern) = 0;

		/** Get full path to data.
		@param _name Resource name.
		@return Return full path to specified data.
		For example getDataPath("My.layout") might return "C:\path\to\project\data\My.layout"
		*/
		virtual const String& GetDataPath(const String& _name) = 0;
	};
} // namespace NGTech