/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

namespace NGTech {
	struct ICallback {
		virtual void Body() = 0;
	};

	struct IGame
	{
		virtual ~IGame() {}
	protected:
		IGame() :rc(false), ec(false), render_callback(nullptr), events_callback(nullptr) {}
	public:
		virtual void Initialise() {}
		virtual void StartGamePlay() {}
		virtual void Update() {}
		virtual void Render() {}

		virtual const char* ResourceDirectory() = 0;

		ENGINE_INLINE void RunRenderCallback() { if (render_callback) render_callback->Body(); };
		ENGINE_INLINE void RunEventsCallback() { if (events_callback) events_callback->Body(); };

		//---------------------------------------------------------------------------
		//Desc:    sets render callback
		//Params:  callback - pointer to render function
		//Returns: -
		//---------------------------------------------------------------------------
		ENGINE_INLINE void SetRenderCallback(ICallback* _rc) { ASSERT(_rc, "_rc is null"); render_callback = _rc; rc = true; };
		//---------------------------------------------------------------------------
		//Desc:    sets events callback
		//Params:  callback - pointer to events function
		//Returns: -
		//---------------------------------------------------------------------------
		ENGINE_INLINE void SetEventsCallback(ICallback* _ec) { ASSERT(_ec, "_ec is null"); events_callback = _ec; ec = true; };

		bool rc = false, ec = false;
		ICallback* render_callback = nullptr;
		ICallback* events_callback = nullptr;
	};
}