/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include "CommonPrivate.h"

#include "IPostProcessor.h"

/*Std::cout*/
#include <iostream>

namespace NGTech
{
	// For Search
	static IPostProcessor * IPostProcessor_ptr;

	IPostProcessor* SearchPostProcess(const char* _name) {
		IPostProcessor*ptr = IPostProcessor_ptr;

		if (!ptr)
		{
			PlatformError("requested post", "Search");
			return nullptr;
		}

		while ((ptr->GetPrevent() != nullptr) && (ptr->GetName() != _name)) {
			PlatformError("requested post-process", "Search");
			ptr = ptr->GetPrevent();
			if (!ptr)
			{
				PlatformError("requested post-process not found", "Search");
				break;
			}
		}

		return ptr;
	}

	/**
	*/
	IPostProcessor::IPostProcessor() :
		active(false),
		EarlyZEnabled(false),
		IPostProcessor_prevent_ptr(nullptr),
		IPostProcessor_next_ptr(nullptr),
		render(nullptr),
		name(nullptr)
	{
		IPostProcessor_prevent_ptr = IPostProcessor_ptr;
		IPostProcessor_ptr = this;
	}

	/**
	*/
	IPostProcessor::IPostProcessor(bool _active, const char*_name) :
		active(_active),
		name(_name),
		render(nullptr),
		EarlyZEnabled(false),
		IPostProcessor_next_ptr(nullptr),
		IPostProcessor_prevent_ptr(nullptr)
	{
		IPostProcessor_prevent_ptr = IPostProcessor_ptr;
		IPostProcessor_ptr = this;
	}

	/**
	*/
	IPostProcessor::~IPostProcessor()
	{
	}
}