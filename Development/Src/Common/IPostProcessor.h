/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

namespace NGTech
{
	struct I_RenderTarget;
	struct I_RenderLowLevel;

	/**
	Interface for post-processors.
	*/
	struct IPostProcessor
	{
	public:
		/**
		*/
		IPostProcessor();

		/**
		*/
		IPostProcessor(bool _active, const char*_name);

		/**
		*/
		virtual ~IPostProcessor();

		/**
		*/
		virtual bool Create() { return false; }

		/**
		Used for cleaning, EarlyZ and etc
		*/
		virtual bool EarlyZPass() { return false; }
		virtual void PreRender0() {}
		virtual void PreRender1() {}

		/**
		Deferred Passes, will called in RenderPipeline Manager
		\params: RenderPipeline, CVARManager, Scene Manager
		*/
		virtual bool DeferredPass1() { return false; }
		virtual bool DeferredPass2(void* _cvars, void* _scene) { return false; }
		/**
		*/
		virtual bool ForwardPass1() { return false; }
		virtual bool ForwardPass2() { return false; }

		/**
		*/
		virtual I_RenderTarget* GetOutputRT() const { return nullptr; }

		/**
		*/
		virtual void Execute() {}
		virtual void ReSize(unsigned int x, unsigned int y) {}

		/**
		*/
		ENGINE_INLINE const char* GetName() const {
			return name;
		}

		/**
		*/
		ENGINE_INLINE void SetActive(bool _active) {
			this->active = _active;
		}

		ENGINE_INLINE bool IsActive() const {
			return active;
		}

		/**
		*/
		ENGINE_INLINE void SetEarlyZEnabled(bool _active) {
			this->EarlyZEnabled = _active;
		}

		ENGINE_INLINE bool IsEarlyZEnabled() const {
			return EarlyZEnabled;
		}

		/**
		*/
		ENGINE_INLINE IPostProcessor* GetPrevent() const {
			return IPostProcessor_prevent_ptr;
		}
		ENGINE_INLINE IPostProcessor* GetNext() const {
			return IPostProcessor_prevent_ptr;
		}

	protected:
		const char*				name = nullptr;
		bool					active = false;
		bool					EarlyZEnabled = false;;
		I_RenderLowLevel*		render = nullptr;
		Vec2 size = Vec2(1980, 1080);
		bool mInited = false;

		IPostProcessor * IPostProcessor_prevent_ptr = nullptr;
		IPostProcessor* IPostProcessor_next_ptr = nullptr;
	};

	// For Search
	IPostProcessor* SearchPostProcess(const char* _name);
}