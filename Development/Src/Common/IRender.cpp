#include "CommonPrivate.h"

//**************************************
#include "RenderQueueWrapper.h"
//**************************************

namespace NGTech
{
	I_RenderLowLevel::I_RenderLowLevel()
		:RenderQueue(new RenderQueueWrapper())
	{
		AddRef();

		defFilter = Filter::UNKNOWN;
		defAniso = Aniso::ANISO_X0;
	}

	void I_RenderLowLevel::DrawCommand(const CommandForDraw& _command) {
		ASSERT(RenderQueue, "Not exist RenderQueue");
		RenderQueue->obj.enqueue(_command);
	}

	I_RenderLowLevel::~I_RenderLowLevel() {
		SAFE_rESET(RenderQueue);
	}
}