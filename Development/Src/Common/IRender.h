/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#ifndef I_RENDER_H
#define I_RENDER_H

//**************************************
#include "StringHelper.h"
#include "IRenderCommons.h"
#include "IPostProcessor.h"
#include "IResource.h"
//**************************************

namespace NGTech
{
	class Mat4;
	class Vec2;
	class Vec3;
	class Vec4;
	struct I_DisplayList;
	struct I_OcclusionQuery;
	struct I_RenderTarget;
	struct I_VBManager;
	struct I_Shader;
	struct I_Texture;
	struct I_ModelHelper;
	struct I_Drawable;

	struct I_Texture : public IResource
	{
		virtual void SetWrap(WrapType wrap) = 0;
		virtual void SetFilter(Filter filter) = 0;
		virtual void SetAniso(Aniso aniso) = 0;

		virtual void Set() = 0;
		virtual void UnSet() = 0;

		virtual void Set(unsigned int tex_unit) = 0;
		virtual void Unset(unsigned int tex_unit) = 0;

		virtual void Copy(int face = -1) = 0;

		virtual void SetMinMipLevel(unsigned int level) = 0;
		virtual	unsigned int GetMinMipLevel() const = 0;

		virtual void SetMaxMipLevel(unsigned int level) = 0;
		virtual	unsigned int GetMaxMipLevel() const = 0;

		ENGINE_INLINE
			virtual String GetResourceType() const override { return "Texture"; }
	protected:
		/**
		!@ Destructor, don't call me directly, used only for shared_ptr, using Release
		*/
		virtual ~I_Texture() {}
	};

	struct I_RenderLowLevel :public RefCount
	{
		Filter defFilter = Filter::UNKNOWN;
		Aniso defAniso = Aniso::ANISO_X0;

		/**
		*/
		I_RenderLowLevel();
		/**
		*/
		virtual String GetRenderName() = 0;
		/**
		*/
		virtual I_OcclusionQuery* GetOQ() = 0;

		virtual I_Texture* TextureCreate(const std::shared_ptr<TextureParamsTransfer>&_Tpp) = 0;

		/**
		*/
		virtual I_Texture* TextureCreate2DUtil(const String &path, const std::shared_ptr<TextureParamsTransfer>&_Tpp) = 0;
		/**
		*/
		virtual I_Texture* TextureCreateCubeUtil(const String &path) = 0;
		virtual I_Texture* TextureCreateCubeFromFileUtil(const String &path) = 0;
		/**
		*/
		virtual I_Texture *TextureCreate2DUtil(unsigned int width, unsigned int height, const Format& format, const String& path) = 0;
		/**
		*/
		virtual I_Texture *TextureCreate3DUtil(unsigned int width, unsigned int height, unsigned int depth, const Format& format, const String& path) = 0;
		/**
		*/
		virtual I_Texture *TextureCreateCubeUtil(unsigned int width, unsigned int height, const Format& format, const String& path) = 0;
		/**
		*/
		virtual I_Shader *ShaderCreate(const String &path, const String &defines = "", bool allowShaderCache = true) = 0;
		/**
		*/
		virtual I_RenderTarget*CreateFBO(unsigned int x, unsigned int y) = 0;
		/**
		*/
		virtual I_VBManager *CreateIBO(void *data, unsigned int numElements, DataType dataType) = 0;
		/**
		*/
		virtual I_VBManager *CreateVBO(void *data, unsigned int numElements, unsigned int elemSize, DataType dataType, TypeDraw drawType) = 0;

		/**
		HELPERS
		*/
		virtual I_ModelHelper* GetModelHelper() = 0;
		/**
		*/
		virtual void EnableWireframeMode() = 0;
		virtual void DisableWireframeMode() = 0;

		/**
		*/
		virtual void Initialise() = 0;
		/**
		*/
		virtual void Reshape(unsigned int width, unsigned int height, Mat4&proj) = 0;
		/**
		*/
		virtual void Reshape(unsigned int width, unsigned int height) = 0;
		/**
		*/
		virtual void GetViewport(int *viewport) = 0;
		/**
		*/
		virtual void ClearColor(Vec3 color) = 0;

		virtual void ColorMask(bool r, bool g, bool b, bool a) = 0;
		/**
		*/
		virtual void SetViewport(unsigned int w, unsigned int h) = 0;
		virtual void SetViewport(unsigned int x, unsigned int y, unsigned int w, unsigned int h) = 0;
		/**
		*/
		virtual void BeginFrame() = 0;
		virtual void FlushGPU() = 0;
		virtual void EndFrame() = 0;

		/**
		2D/3D-mode React(triangle) creation
		*/
		virtual I_Drawable* CreateReact() = 0;

		/**
		Blending
		*/
		virtual void BlendFunc(BlendParam src, BlendParam dst) = 0;
		virtual void EnableBlending() = 0;
		virtual void EnableBlending(BlendParam src, BlendParam dst) = 0;
		virtual void DisableBlending() = 0;

		/**
		Depth-Buffer
		*/
		virtual void DepthFunc(CompareType type) = 0;
		virtual void EnableDepth(CompareType type) = 0;
		virtual void EnableDepth() = 0;
		virtual void DisableDepth() = 0;
		virtual void DepthMask(bool mask) = 0;

		/**
		Z-Buffer
		*/
		virtual void PolygonOffsetFill(float a, float b) = 0;
		virtual void EnablePolygonOffsetFill(float a, float b) = 0;
		virtual void EnablePolygonOffsetFill() = 0;
		virtual void DisablePolygonOffsetFill() = 0;

		/**
		Culling
		*/
		virtual void SetPolygonFront(CullType type) = 0;
		virtual void SetPolygonCull(CullFace face) = 0;
		virtual void EnableCulling(CullType type) = 0;
		virtual void EnableCulling(CullFace face) = 0;
		virtual void EnableCulling() = 0;
		virtual void DisableCulling() = 0;

		/**
		Stencil test
		*/
		virtual void DisableStencilTest() = 0;
		virtual void EnableStencilTest() = 0;

		/**
		Clip-plains
		*/
		virtual void ClipPlane(const Vec4 &plain, int plainNum) = 0;
		virtual void EnableClipPlane(int plainNum) = 0;
		virtual void EnableClipPlane(const Vec4 &plain, int plainNum) = 0;
		virtual void DisableClipPlane(int plainNum) = 0;

		/**
		�������� ������� �� ��������� � �������
		*/
		virtual void DrawCommand(const CommandForDraw& _command);

		/**
		������������ �������. ����� ������� ������� � ��� �����������
		*/
		virtual void ProcessQueue() {}

		/**
		*/
		virtual void WriteScreenshot(const char* path) const = 0;

		/**
		Compute shader support check
		*/
		ENGINE_INLINE virtual bool IsComputeSupported() {
			return false;
		}

		/**/
		virtual void Clear(RenderBuffer buffers) = 0;
		virtual void ClearDepthStencilColor() = 0;
		virtual void ClearDepthColor() = 0;
		virtual void ClearDepthOnly() = 0;
		virtual void ClearColorOnly() = 0;

	protected:
		/**
		!@ Destructor, don't call me directly, used only for shared_ptr, using Release
		*/
		virtual ~I_RenderLowLevel();

		struct RenderQueueWrapper;
		std::unique_ptr<RenderQueueWrapper> RenderQueue;
	};

	struct I_Shader : public IResource
	{
		I_Shader() {
			defines.clear();
		}

		/**
		*/
		virtual void Enable() = 0;

		/**
		*/
		virtual void Disable() = 0;

		/**
		Recompile shader
		*/
		virtual void Recompile(const String& _shaderSpecificDefs, const String& _sceneSpecificDefs = "") = 0;

		/**
		*/
		virtual void DispatchCompute(unsigned int num_groups_x, unsigned int num_groups_y, unsigned int num_groups_z) = 0;

		/**
		*/
		virtual void CommitChanges() = 0;

		/**
		*/
		virtual void AddDefines(const String &_defines) = 0;

		/**
		*/
		virtual void SendMat4(const String &name, const Mat4 &value) = 0;
		virtual void SendVec4(const String &name, const Vec4 &value) = 0;
		virtual void SendVec3(const String &name, const Vec3 &value) = 0;
		virtual void SendVec2(const String &name, const Vec2 &value) = 0;
		virtual void SendFloat(const String &name, float value) = 0;
		virtual void SendInt(const String &name, int value) = 0;

		ENGINE_INLINE virtual String GetResourceType() const override { return "Shader"; }
	protected:
		/**
		!@ Destructor, don't call me directly, used only for shared_ptr, using Release
		*/
		virtual ~I_Shader() {}
	public:
		String defines = StringHelper::EMPTY_STRING;
	};

	struct I_VBManager
	{
		/**
		*/
		virtual void Bind() const = 0;
		/**
		*/
		virtual void UnBind() const = 0;
		/**
		*/
		virtual void BindIndex(unsigned int idx) const = 0;
		/**
		*/
		virtual void UnbindIndex(unsigned int idx) const = 0;
		/**
		*/
		virtual void Allocate(const void *data, size_t size, const TypeDraw& usage) = 0;
		/**
		*/
		virtual void FillBuffer(size_t offset) = 0;

	protected:
		/**
		*/
		virtual ~I_VBManager() {}

		struct locked_data
		{
			void* ptr = nullptr;
			unsigned int flags = 0;
		} data_locked;
	};

	struct I_OcclusionQuery :public RefCount
	{
		I_OcclusionQuery()
		{
			AddRef();
		}

		/**
		Begins rendering to query
		*/
		virtual void BeginQuery() = 0;

		/**
		End rendering to query
		*/
		virtual void EndQuery() = 0;

		/**
		Draw only if one sample went through the tests,
		\we don't need to get the query result which prevent the rendering pipeline to stall.
		*/
		virtual void BeginConditionalRender() {}
		virtual void EndConditionalRender() {}

	protected:
		/**
		!@ Destructor, don't call me directly, used only for shared_ptr, using Release
		*/
		virtual ~I_OcclusionQuery() {}
	};

	struct I_RenderTarget :public RefCount
	{
		I_RenderTarget()
		{
			AddRef();
		}

		/**
		*/
		virtual void Set() = 0;
		virtual void Unset() = 0;

		virtual void ReadBuffer(unsigned int _texIndex) = 0;
		virtual void WriteBuffer(unsigned int _texIndex) = 0;

		/**
		*/
		virtual void Clear() = 0;
		virtual void ClearDepthOnly() = 0;
		virtual void ClearDepthAndStencil() = 0;

		/**
		*/
		virtual void ClearColorOnly() = 0;

		/**
		ColorAttachment
		*/
		virtual bool AttachColorTexture(unsigned int _index) = 0;
		virtual bool AttachColorTexture(unsigned int _index, const Format& _format) = 0;
		virtual void BindColorTexture(unsigned int _index) = 0;
		virtual void BindColorTexture(unsigned int _as, unsigned int _index) = 0;

		/**
		DepthAttachment
		*/
		virtual bool AttachDepthTexture() = 0;
		virtual void BindDepthTexture(unsigned int _index) = 0;
		virtual void BindDepthTexture() = 0;

		virtual bool Validate() = 0;
		virtual void UnBindActiveTexture(unsigned int tex_unit) = 0;

		virtual void ReattachColor(unsigned int target, int face, int level) = 0;
		virtual void ReattachColor(unsigned int target, int level) = 0;

		virtual void ReattachDepth(int face, int level) = 0;
		virtual void ReattachDepth(int level) = 0;
		virtual void ReattachDepthStencil(int face, int level) = 0;
		virtual void ReattachDepthStencil(int level) = 0;

		/* !@ ����� ������ ������ BlitDepth*/
		/* !@ Get blit depth from current frame-buffer to main frame-buffer */
		virtual void Resolve(I_RenderTarget* to, unsigned int bitmask) {}

		virtual unsigned int GetUID() const = 0;

		const Vec2& GetSize() const { return mFBOSize; }
	protected:
		/**
		!@ Destructor, don't call me directly, used only for shared_ptr, using Release
		*/
		virtual ~I_RenderTarget() {}

		/**
		width-x
		height-y
		*/
		Vec2 mFBOSize = Vec2(1920, 1080);
	};

	struct I_ModelHelper :public RefCount
	{
		I_ModelHelper()
		{
			AddRef();
		}

		virtual void LoadToRender(void* _vertices, unsigned int* _indices, unsigned int sizeofVertex, unsigned int _numVertices, unsigned int _numIndices) = 0;
		virtual void Draw() const = 0;
		virtual void DrawInstanced(unsigned int instance_count, unsigned int base_instance) const = 0;

		// This method for dynamic meshes
		virtual void UpdateAndDraw(unsigned int) const = 0;
		// This method for dynamic meshes
		virtual void UpdateAndDrawInstanced(unsigned int _offset, unsigned int instance_count, unsigned int base_instance) const = 0;

		virtual I_VBManager * GetVB() const = 0;
		virtual I_VBManager * GetIB() const = 0;
	protected:
		virtual ~I_ModelHelper() {}
	};

	struct I_Drawable :public RefCount
	{
		I_Drawable()
		{
			AddRef();
		}

		virtual void Draw() = 0;
	protected:
		/**
		!@ Destructor, don't call me directly, used only for shared_ptr, using Release
		*/
		virtual ~I_Drawable() {}
	};
}

#endif