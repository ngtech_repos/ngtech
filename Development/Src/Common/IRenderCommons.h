/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

namespace NGTech
{
	//Nick:Note: enum class described in there http://www.cprogramming.com/c++11/c++11-nullptr-strongly-typed-enum-class.html
	enum class ShaderType
	{
		SHT_VERTEX = 0,
		SHT_FRAGMENT,
		SHT_GEOMETRY,
		SHT_COMPUTE,
		SHT_TES,
		SHT_TCS,
		SHT_COUNT
	};

	enum class DataType
	{
		DT_FLOAT = 0,
		DT_DOUBLE,
		DT_SHORT,
		DT_UNSIGNED_BYTE,
		DT_UNSIGNED_INT,
		DT_UNSIGNED_SHORT,
		DT_COUNT
	};

	enum class TypeDraw
	{
		TD_UNKNOWN = 0,
		TD_STATIC_DRAW,
		TD_STREAM_DRAW,
		TD_DYNAMIC_DRAW,
		TD_DYNAMIC_COPY
	};

	enum class DrawMode
	{
		DM_TRINAGLES = 0,
		DM_PATCHES,
		DM_LINES,
		DM_POINTS,
		DM_COUNT
	};

	/**
	Texture edge wrap enum
	*/
	enum class WrapType
	{
		ZERO = 0,
		REPEAT,
		CLAMP,
		CLAMP_TO_EDGE,
	};

	//---------------------------------------------------------------------------
	//Desc: Texture anisotropy enum
	//---------------------------------------------------------------------------
	enum class Aniso {
		ANISO_X0 = 0,
		ANISO_X1 = 1,
		ANISO_X2 = 2,
		ANISO_X4 = 4,
		ANISO_X8 = 8,
		ANISO_X16 = 16,
		ANISO_NUM
	};

	/**
	Texture filter enum
	*/
	enum class Filter {
		UNKNOWN = 0,
		NEAREST,
		LINEAR,
		NEAREST_MIPMAP_NEAREST,
		LINEAR_MIPMAP_NEAREST,
		LINEAR_MIPMAP_LINEAR,
		FILTER_NUM
	};

	/**
	Texture src format enum
	*/
	enum class Format
	{
		GLFMT_UNKNOWN = 0,
		GLFMT_R8G8B8,//1
		GLFMT_A8R8G8B8,//2
		GLFMT_sA8R8G8B8,//3

		GLFMT_D24S8,//4
		GLFMT_D32F,//5

		GLFMT_B16G16R16,//6
		GLFMT_A16B16G16R16,//7

		GLFMT_R16F,//8
		GLFMT_G16R16F,//9
		GLFMT_B16G16R16F,//10
		GLFMT_A16B16G16R16F,//11

		GLFMT_R32F,//12
		GLFMT_G32R32F,//13
		GLFMT_B32G32R32F,//14
		GLFMT_A32B32G32R32F,//15
		GLFMT_SRGB,//16
		GLFMT_SRGBA,//17

		GLFMT_DXT1, // 18
		GLFMT_DXT5, // 19
		GLFMT_NUM
	};

	/**
	Texture target enum
	*/
	enum class TextureTarget {
		TEXTURE_ZERO = 0,
		TEXTURE_2D,
		TEXTURE_3D,
		TEXTURE_CUBE,
		TEXTURE_NUM
	};

	/**
	Desc: Blending type enum
	*/
	enum class BlendParam {
		BLEND_NONE = 0,
		ONE,
		ZERO,
		SRC_COLOR,
		DST_COLOR,
		SRC_ALPHA,
		DST_ALPHA,
		ONE_MINUS_SRC_COLOR,
		ONE_MINUS_DST_COLOR,
		ONE_MINUS_SRC_ALPHA,
		ONE_MINUS_DST_ALPHA,
		BLEND_NUM
	};

	/**
	Desc: Depth function enum
	*/
	enum class CompareType {
		COMP_NONE = 0,
		NEVER,
		LESS,
		EQUAL,
		LEQUAL,
		GREATER,
		NOTEQUAL,
		GEQUAL,
		ALWAYS,
		COMPARE_NUM
	};

	/**
	Desc: Cull type enum
	*/
	enum class CullType
	{
		CULL_NONE = 0,
		CULL_CCW,
		CULL_CW,
		CULL_FRONT,
		NUM_FRONT_MODES
	};

	/**
	Desc: Cull face
	*/
	enum class CullFace {
		CULL_NONE = 0,
		CULL_FRONT_FACE,
		CULL_BACK_FACE,
		NUM_CULL_MODES,
	};

	/**
	Desc: Buffers enum
	*/
	enum class RenderBuffer {
		INVALID = 0,
		COLOR_BUFFER,
		DEPTH_BUFFER,
		STENCIL_BUFFER,
		COLOR_AND_DEPTH,
		ALL_BUFFERS,
		NUM_BUFFERS
	};

	enum RenderLayer
	{
		LAYER_0 = 0, // SkyBox and etc
		LAYER_1, // Default layer
		LAYER_2, LAYER_3,
		LAYER_4, LAYER_5, LAYER_6,
		LAYER_7,
		USER_LAYER_8, USER_LAYER_9, USER_LAYER_10,
		USER_LAYER_11, USER_LAYER_12, USER_LAYER_13,
		USER_LAYER_14
	};

	/*������������ ��� ����, ��� �� ���������� ������ �� ������ � ������, ������ ������ �����*/
	struct TextureParamsTransfer final
	{
		bool generateNormal = false;
		uint32_t normalLevel = 0;
		void* data = nullptr;
		String path = StringHelper::EMPTY_STRING;

		enum class Type {
			TEXTURE_2D = 0,
			TEXTURE_3D,
			TEXTURE_CUBE
		} m_Type = Type::TEXTURE_2D;

		TextureParamsTransfer() {}
		TextureParamsTransfer(const TextureParamsTransfer&_obj) { _Swap(_obj); }

		TextureParamsTransfer& operator=(const TextureParamsTransfer&_obj) {
			_Swap(_obj);
			return *this;
		}

		bool operator==(const TextureParamsTransfer&_obj)
		{
			return
				(this->generateNormal == _obj.generateNormal)
				&& (this->normalLevel == _obj.normalLevel)
				&& (this->data == _obj.data)
				&& (this->path == _obj.path)
				&& (this->m_Type == _obj.m_Type);
		}

		void _Swap(const TextureParamsTransfer&_obj)
		{
			if (this != &_obj)
			{
				generateNormal = _obj.generateNormal;
				normalLevel = _obj.normalLevel;
				data = _obj.data;
				path = _obj.path;
				m_Type = _obj.m_Type;
			}
		}
	};

	// ��������� �������������� ������� ��� ���������
	struct CommandForDraw final
	{
		enum class Type
		{
			INVALID = 0,
			ELEMENTS,
			ARRAYS
		} m_Type = Type::INVALID;

		void	 *m_Indices = nullptr;
		uint32_t  m_IndexCount = 0;
		uint32_t  m_VertexCount = 0;
		uint32_t  m_InstanceCount = 1;
		uint32_t  m_BaseInstance = 0;
		String	  m_Name = "Empty command";
		DrawMode  m_Mode = DrawMode::DM_TRINAGLES;
	};
}