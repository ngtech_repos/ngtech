/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/

#pragma once

namespace NGTech
{
	/**
	Used for Resource system
	*/
	//!@todo RefCountSafe
	struct IResource : public RefCount/*RefCountSafe*/
	{
		virtual ~IResource() {}

		ENGINE_INLINE virtual void Load() {}

		ENGINE_INLINE virtual bool OnlyLoad() { return false; }
		ENGINE_INLINE virtual void PushToRender() {  }

		ENGINE_INLINE bool IsLoaded() const { return mLoaded; }
		ENGINE_INLINE void SetLoaded(bool _loaded) { MT::ScopedGuard guard(mutex); mLoaded = _loaded; }

		ENGINE_INLINE virtual String GetResourceType() const { return "DummyResType"; }

		ENGINE_INLINE const String& GetPath() const { return m_LoadingPath; }

		ENGINE_INLINE void SetPath(const String &_v) { MT::ScopedGuard guard(mutex); m_LoadingPath = _v; }

	protected:
		ENGINE_INLINE IResource() {
			AddRef();
		}

		ENGINE_INLINE IResource(const IResource&_other) {
			_Swap(_other);
		}

		ENGINE_INLINE const IResource & operator = (const IResource & _other) {
			_Swap(_other);
			// �� ���������� ������ ���������� *this
			return *this;
		}

	private:
		ENGINE_INLINE void _Swap(const IResource&_other) {
			if (this != &_other) // ������ �� ������������� ����������������
			{
				mLoaded = _other.mLoaded;
				m_LoadingPath = _other.m_LoadingPath;
			}
		}

	private:
		bool mLoaded = false;
		String m_LoadingPath = StringHelper::EMPTY_STRING;

	protected:
		MT::Mutex mutex;
	};
}