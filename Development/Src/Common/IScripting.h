/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

// for std::cout
#include <iostream>

namespace NGTech
{
	class ScriptInterface;
	enum class ScriptType
	{
		INTEGRATED_SCRIPT = 0,
		LUA_SCRIPT
	};
	/**
	*/
	struct IScriptInterp :public Noncopyable
	{
	public:

	public:
		ENGINE_INLINE IScriptInterp()
			:m_IsInited(false),
			m_Type(ScriptType::INTEGRATED_SCRIPT)
		{}

		virtual ~IScriptInterp() {}

		virtual bool Initialise() { return false; }

		ENGINE_INLINE bool IsInited() {
			return m_IsInited;
		}

		ENGINE_INLINE void SetInited(bool _s) {
			m_IsInited = _s;
		}

		int RunScript(ScriptInterface* _script);

		ENGINE_INLINE ScriptType GetType() { return m_Type; }

		virtual void* GetScriptState() { return nullptr; }
	protected:
		bool m_IsInited = false;
		ScriptType m_Type = ScriptType::INTEGRATED_SCRIPT;
	};
}