/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#ifndef LIST_H
#define LIST_H

namespace NGTech
{
	// List
	//
	// A very simple dynamic list used instead of the stl vector.
	template <class T>
	class NList
	{
	public:
		NList() :
			entries(NULL),
			numEntries(0),
			listSize(0)
		{
		}

		~NList()
		{
			if (entries)
				free(entries);
			entries = NULL;
		}

		ENGINE_INLINE T& operator[](size_t index) const
		{
			return entries[index];
		}

		ENGINE_INLINE operator void*()
		{
			return (void*)entries;
		}

		ENGINE_INLINE operator const void*()
		{
			return (const void*)entries;
		}

		// adds one new element to end of list
		ENGINE_INLINE size_t push_back(const T &newEntry)
		{
			if ((listSize - numEntries) < 1)
			{
				if (!ChangeSpace(numEntries + 1))
					return (size_t)-1;
			}
			memcpy(&entries[numEntries], &newEntry, sizeof(T));
			size_t firstEntryIndex = numEntries;
			numEntries++;
			return firstEntryIndex;
		}

		// adds count elements to end of list
		ENGINE_INLINE size_t push_back(size_t count, const T &newEntries)
		{
			if ((listSize - numEntries) < count)
			{
				if (!ChangeSpace(numEntries + count))
					return -1;
			}
			memcpy(&entries[numEntries], &newEntries, count * sizeof(T));
			size_t firstEntryIndex = numEntries;
			numEntries += count;
			return firstEntryIndex;
		}

		// gets number of currently used elements (not the actual list-size!!)
		ENGINE_INLINE size_t size() const
		{
			return numEntries;
		}

		// changes size of list
		ENGINE_INLINE bool resize(size_t count)
		{
			if (listSize < count)
			{
				if (!ChangeSpace(count))
					return false;
			}
			else
			{
				numEntries = count;
				if (!ChangeSpace(count))
					return false;
			}
			return true;
		}

		// resets number of currently used elements (not the actual list-size!!)
		ENGINE_INLINE void clear()
		{
			numEntries = 0;
		}

		// frees elements of list
		ENGINE_INLINE void erase()
		{
			numEntries = 0;
			listSize = 0;
			if (entries)
				free(entries);
			entries = NULL;
		}

		// performs a qsort on elements, based on passed compare-function
		typedef int(*_compareF)(const void*, const void*);
		ENGINE_INLINE void Sort(_compareF ___compare)
		{
			std::stable_sort(begin(), end(), ___compare);
		}

		// performs a qsort on elements, based on passed compare-function
		ENGINE_INLINE const T& begin()
		{
			return entries[0];
		}

		// performs a qsort on elements, based on passed compare-function
		ENGINE_INLINE const T& end()
		{
			return entries[numEntries];
		}

	protected:
		// change memory-size of list
		ENGINE_INLINE bool ChangeSpace(size_t newSize)
		{
			T *newEntries = (T*)realloc(entries, sizeof(T)*newSize);
			if ((!newEntries) && (newSize > 0))
				return false;
			entries = newEntries;
			listSize = newSize;
			return true;
		}

		T *entries; // elements of list
		size_t numEntries; // number of currently used elements
		size_t listSize; // overall size of list (memory-size)
	};
}
#endif