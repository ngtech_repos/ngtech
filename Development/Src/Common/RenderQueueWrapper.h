#pragma once

#include "IRender.h"

//**************************************
#include "../Platform/inc/concurrentqueue/concurrentqueue.h"
#include "../Platform/inc/concurrentqueue/blockingconcurrentqueue.h"
//**************************************

namespace NGTech
{
	struct I_RenderLowLevel::RenderQueueWrapper
	{
		moodycamel::ConcurrentQueue<CommandForDraw> obj;
	};
}