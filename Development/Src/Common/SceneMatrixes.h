/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

namespace NGTech
{
	struct I_Texture;
	/**
	*/
	struct SceneMatrixes
	{
		SceneMatrixes();

		Mat4 matWorld;
		Mat4 matView;
		Mat4 matProj;
		Mat4 matProjInv;
		Mat4 matWorldInv;
		Mat4 matMVP;

		Mat4 matViewportTransform;
		Mat4 matSpotTransform;

		Vec3 matSceneAmbient;
		Vec3 CamViewPosition;
		Vec3 CamViewDirection;

		Vec2 CamClipplanes;

		/**/
		float matTime;

		I_Texture *matShadowMap = nullptr;
		I_Texture *matViewportMap = nullptr;
		I_Texture *matSpotMap = nullptr;

		/*
		Used for result lighting
		*/
		enum DrawMode
		{
			FINAL_LIGHTING = 0,
			DIFFUSE_VIS,
			NORMALS_VISG,
			POSITIONS_VIS,
			ROUGHNESS_VIS,
			SPECULAR_VIS,
			SPECULARPOWER_VIS,
			DEPTH_VIS,
			BRDF_VIS,
			COUNT_VIS
		} debug_drawmode = FINAL_LIGHTING;

		/*
		Used for gamma correction
		*/
		enum GammaCorrectionMode
		{
			simpleGammaCorrection = 0,
			linearToneMapping,
			simpleReinhardToneMapping,
			lumaBasedReinhardToneMapping,
			whitePreservingLumaBasedReinhardToneMapping,
			RomBinDaHouseToneMapping,
			Uncharted2ToneMapping,
			filmicToneMapping,
			GAMMACORRECTION_MAX
		} u_gammaMode = simpleGammaCorrection;

		ENGINE_INLINE void ComputeMVPWorld(const Mat4& objTrans, const Vec3& _cameraPos, const Vec3& _cameraDir, const Vec2& _camClipplanes)
		{
			//don't worry,must be proj*view(see NV docs)
			matMVP = matProj * matView;

			matWorld = objTrans;
			matWorldInv = Mat4::inverse(matWorld);
			CamViewPosition = _cameraPos;
			CamViewDirection = _cameraDir;

			CamClipplanes = _camClipplanes;
		}

		ENGINE_INLINE void UpdateUniforms(I_Shader* ptr)
		{
			if (!ptr) {
				PlatformError("Invalid pointer on shader");
				return;
			}

			// Vec3
			ptr->SendVec3("auto_globalAmbient", matSceneAmbient);

			// Camera
			ptr->SendVec3("auto_globalViewPosition", CamViewPosition);
			ptr->SendVec3("auto_globalViewDirection", CamViewDirection);

			// Vec2
			ptr->SendVec2("auto_CameraClipPlanes", CamClipplanes);
			ptr->SendVec2("texCoordScales", Vec2(matProjInv.GetScale()));

			// Mat4
			ptr->SendMat4("auto_globalWorld", matWorld);
			ptr->SendMat4("auto_globalView", matView);
			ptr->SendMat4("auto_globalViewInv", Mat4::inverse(matView));
			ptr->SendMat4("auto_globalProj", matProj);
			ptr->SendMat4("auto_globalProjInv", matProjInv);
			ptr->SendMat4("auto_globalWorldInv", matWorldInv);
			ptr->SendMat4("auto_globalMVP", matMVP);
			ptr->SendMat4("auto_globalMVPInv", Mat4::inverse(matMVP));
			ptr->SendMat4("auto_globalViewportTransform", matViewportTransform);
			ptr->SendMat4("auto_globalSpotTransform", matSpotTransform);

			// float
			ptr->SendFloat("auto_globalTime", matTime);

			// int
			ptr->SendInt("debug_drawmode", debug_drawmode);
			ptr->SendInt("u_gammaMode", u_gammaMode);
		}
	};
	extern SceneMatrixes matrixes;
}