/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "CommonPrivate.h"

#include "ScriptInterface.h"
#include "ScriptManager.h"

// for std::cout
#include <iostream>

namespace NGTech
{
	ScriptInterface::ScriptInterface(const String& _name)
		:ScriptInterface()
	{
		m_name = _name;
		_AttachScript();
	}

	void ScriptInterface::_AttachScript()
	{
		ASSERT("[ScriptInterface::_AttachScript] Not implemented");
	}
}