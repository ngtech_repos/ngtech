/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include <string>
#include <memory>

namespace NGTech
{
	/*!
	\brief Script action class

	Used only for Script
	*/
	struct ScriptAction {
		virtual ~ScriptAction() {}
		ENGINE_INLINE virtual void Action() {}
	};

	/*!
	\brief Main class of scripts

	This class used for C-implementation of script action
	*/
	class ScriptInterface
	{
	public:
		enum STATUS
		{
			INVALID_FILE = -2,
			UNKNOWN_ERROR = -1,
			NO_ERRORS = 0
		};
	public:
		ScriptInterface(const String& _name);
		virtual ~ScriptInterface()
		{
			DestroyEvent();
			_Destroy();
		}

		ENGINE_INLINE virtual void CreateScriptEvent(void) {}

		ENGINE_INLINE virtual STATUS Loaded(void) { return UNKNOWN_ERROR; }
		ENGINE_INLINE virtual STATUS LoadedGUI(void) { return UNKNOWN_ERROR; }
		/*Updates functions, will called in different sub-systems*/
		ENGINE_INLINE virtual STATUS Update(void) { return UNKNOWN_ERROR; }
		ENGINE_INLINE virtual STATUS UpdateInput(void) { return UNKNOWN_ERROR; }
		ENGINE_INLINE virtual STATUS UpdateGUI(void) { return UNKNOWN_ERROR; }
		ENGINE_INLINE virtual STATUS UpdatePhysics(void) { return UNKNOWN_ERROR; }
		ENGINE_INLINE virtual STATUS UpdateRender(void) { return UNKNOWN_ERROR; }
		ENGINE_INLINE virtual STATUS UpdateSceneManager(void) { return UNKNOWN_ERROR; }
		ENGINE_INLINE virtual STATUS UpdateAI(void) { return UNKNOWN_ERROR; }

		ENGINE_INLINE virtual STATUS DestroyEvent(void) { return UNKNOWN_ERROR; }
		ENGINE_INLINE virtual STATUS ReloadEvent(void) { return UNKNOWN_ERROR; }

		/*Current state of this script*/
		ENGINE_INLINE bool IsActive(void) { return m_active; }
		ENGINE_INLINE void SetActive(bool _active) { m_active = _active; }

		ENGINE_INLINE const String GetName(void) { return m_name; }
	protected:
		/**/
		ENGINE_INLINE ScriptInterface() {
			m_active = false;
			m_name.clear();
			callbacks.ResetAllCallbacks();
		}

		/**/
		ENGINE_INLINE void _Destroy() { SetActive(false); callbacks.ResetAllCallbacks(); }
		/**/
		virtual void _AttachScript();
	private:
		/**/
		struct ScriptCallbacks
		{
			std::shared_ptr<ScriptAction> updateInput;
			std::shared_ptr<ScriptAction> updateGUI;
			std::shared_ptr<ScriptAction> updatePhysics;
			std::shared_ptr<ScriptAction> updateRender;
			std::shared_ptr<ScriptAction> updateSceneManager;
			std::shared_ptr<ScriptAction> updateAI;
			std::shared_ptr<ScriptAction> loaded;
			std::shared_ptr<ScriptAction> loadedGUI;

			ENGINE_INLINE void ResetAllCallbacks()
			{
				SAFE_rESET(updateInput);
				SAFE_rESET(updateGUI);
				SAFE_rESET(updatePhysics);
				SAFE_rESET(updateRender);
				SAFE_rESET(updateSceneManager);
				SAFE_rESET(updateAI);
				SAFE_rESET(loaded);
				SAFE_rESET(loadedGUI);
			}
		};
	protected:
		bool			m_active;
		String			m_name;
		ScriptCallbacks callbacks;
	};
}