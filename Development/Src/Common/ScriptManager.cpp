/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "CommonPrivate.h"

#include "ScriptInterface.h"
#include "ScriptManager.h"

// for std::cout
#include <iostream>
// for shared ptr
#include <memory>

namespace NGTech
{
	ScriptManager::ScriptManager(void)
	{
		this->m_scripts.clear();
	}

	ScriptManager::~ScriptManager(void)
	{
		_Destroy();
	}

	void ScriptManager::_Destroy(void)
	{
		for (auto & script : m_scripts)
		{
			SAFE_rESET(script);
		}
	}
	void ScriptManager::ResetScript(void)
	{
		for (const auto & script : m_scripts)
		{
			if (script)
				script->ReloadEvent();
		}
	}

	bool ScriptManager::AttachScript(ScriptInterface * script)
	{
		return AttachScript(std::shared_ptr<ScriptInterface>(script));
	}

	bool ScriptManager::AttachScript(const std::shared_ptr<ScriptInterface>& scriptToAttach)
	{
		if (!scriptToAttach)
			return false;

		for (const auto & script : m_scripts)
		{
			if (script == nullptr)
				continue;

			if (scriptToAttach->GetName() == script->GetName())
			{
#ifdef _ENGINE_DEBUG_
				std::cout << "Script " << scriptToAttach->GetName() << " already exist \n";
#endif
				return false;
			}
		}
		m_scripts.push_back(scriptToAttach);

		return true;
	}

	ScriptInterface* ScriptManager::GetScriptByName(const String & _name)
	{
		for (const auto & script : m_scripts)
		{
			if (script->GetName() == _name)
				return script.get();
		}
#ifdef _ENGINE_DEBUG_
		std::cout << "Script " << _name << " not found \n";
#endif
		return nullptr;
	}

	void ScriptManager::Loaded(void) const
	{
		for (const auto & script : m_scripts)
		{
			if (script)
				script->Loaded();
		}
	}
	void ScriptManager::LoadedGUI(void) const
	{
		for (const auto & script : m_scripts)
		{
			if (script)
				script->LoadedGUI();
		}
	}

	void ScriptManager::UpdateAI(void) const
	{
		for (const auto & script : m_scripts)
		{
			if (script)
				script->UpdateAI();
		}
	}

	void ScriptManager::UpdateSceneManager(void) const
	{
		for (const auto & script : m_scripts)
		{
			if (script)
				script->UpdateSceneManager();
		}
	}
	void ScriptManager::UpdateRender(void) const
	{
		for (const auto & script : m_scripts)
		{
			if (script)
				script->UpdateRender();
		}
	}
	void ScriptManager::UpdatePhysics(void) const
	{
		for (const auto & script : m_scripts)
		{
			if (script)
				script->UpdatePhysics();
		}
	}
	void ScriptManager::UpdateGUI(void) const
	{
		for (const auto & script : m_scripts)
		{
			if (script)
				script->UpdateGUI();
		}
	}
	void ScriptManager::UpdateInput(void) const
	{
		for (const auto & script : m_scripts)
		{
			if (script)
				script->UpdateInput();
		}
	}
	void ScriptManager::Update(void) const
	{
		for (const auto & script : m_scripts)
		{
			if (script)
				script->Update();
		}
	}
}