/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include <memory>
#include <vector>

namespace NGTech
{
	class ScriptInterface;
	/**
	*/
	class ScriptManager :public Noncopyable
	{
	public:
		ScriptManager(void);
		virtual ~ScriptManager(void);

		void ResetScript(void);

		bool AttachScript(ScriptInterface* script);
		bool AttachScript(const std::shared_ptr<ScriptInterface>& script);

		ScriptInterface* GetScriptByName(const String&_name);

		void Loaded(void) const;
		void LoadedGUI(void) const;
		/*Updates functions, will called in different sub-systems*/
		void Update(void) const;
		void UpdateInput(void) const;
		void UpdateGUI(void) const;
		void UpdatePhysics(void)const;
		void UpdateRender(void) const;
		void UpdateSceneManager(void) const;
		void UpdateAI(void) const;
	private:
		void _Destroy(void);
	private:
		std::vector<std::shared_ptr<ScriptInterface>> m_scripts;
	};
}