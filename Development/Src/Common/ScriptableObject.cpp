/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "CommonPrivate.h"

#include "ScriptableObject.h"
#include "ScriptInterface.h"

namespace NGTech
{
	ScriptableObject::~ScriptableObject()
	{
		for (auto & script : m_scripts) {
			SAFE_rESET(script);
		}
	}

	void ScriptableObject::AttachScript(ScriptInterface*script) {
		AttachScript(std::shared_ptr<ScriptInterface>(script));
	}

	void ScriptableObject::AttachScript(const std::shared_ptr<ScriptInterface>& _sc)
	{
		m_scripts.push_back(_sc);
		Script_Loaded();
	}

	void ScriptableObject::Script_Loaded(void) {
		for (auto & script : m_scripts)
		{
			script->Loaded();
		}
	}

	void ScriptableObject::Script_LoadedGUI(void) {
		for (auto & script : m_scripts)
		{
			script->LoadedGUI();
		}
	}

	/*Updates functions, will called in different sub-systems*/
	void ScriptableObject::Script_Update(void) {
		for (auto & script : m_scripts)
		{
			script->Update();
		}
	}

	void ScriptableObject::Script_UpdateInput(void) {
		for (auto & script : m_scripts)
		{
			script->UpdateInput();
		}
	}

	void ScriptableObject::Script_UpdateGUI(void) {
		for (auto & script : m_scripts)
		{
			script->UpdateGUI();
		}
	}

	void ScriptableObject::Script_UpdatePhysics(void) {
		for (auto & script : m_scripts)
		{
			script->UpdatePhysics();
		}
	}

	void ScriptableObject::Script_UpdateRender(void) {
		for (auto & script : m_scripts)
		{
			script->UpdateRender();
		}
	}
	void ScriptableObject::Script_UpdateSceneManager(void) {
		for (auto & script : m_scripts)
		{
			script->UpdateSceneManager();
		}
	}

	void ScriptableObject::Script_UpdateAI(void) {
		for (auto & script : m_scripts)
		{
			script->UpdateAI();
		}
	}

	void ScriptableObject::Script_DestroyEvent(void) {
		for (auto & script : m_scripts)
		{
			script->DestroyEvent();
		}
	}

	void ScriptableObject::Script_ReloadEvent(void) {
		for (auto & script : m_scripts)
		{
			script->ReloadEvent();
		}
	}
}