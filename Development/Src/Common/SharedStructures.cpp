#include "CommonPrivate.h"

#include "SharedStructures.h"

namespace NGTech {
	/**/
	const std::vector<EditorVar> EditorVar::EMPTY_VARS;

	EditorVar::EditorVar()
	{
		Browsable = true;
		HasSlider = false;
		SliderMax = 100;
		SliderMin = 0;
		SliderFactor = 1;
		ActualData = nullptr;
		OlderData = nullptr;

		type = UNKNOWN;
	}

	// -------------------Default types-------------------
	/**/
	template<>
	EditorVar::EditorVar(String newName, bool* var, String category, String desc, bool browsable) : EditorVar() { name = std::move(newName); ActualData = var; OlderData = var; type = BOOL; this->category = std::move(category); this->desc = std::move(desc); Browsable = browsable; }

	/**/
	template<>
	EditorVar::EditorVar(String newName, int* var, String category, String desc, bool browsable) : EditorVar() { name = std::move(newName);	ActualData = var; OlderData = var; type = INT; this->category = std::move(category); this->desc = std::move(desc); Browsable = browsable; }

	/**/
	template<>
	EditorVar::EditorVar(String newName, unsigned int* var, String category, String desc, bool browsable) : EditorVar() { name = std::move(newName); ActualData = var; OlderData = var; type = UNINT; this->category = std::move(category); this->desc = std::move(desc); Browsable = browsable; }

	/**/
	template<>
	EditorVar::EditorVar(String newName, float* var, String category, String desc, bool browsable) : EditorVar() { name = std::move(newName);	ActualData = var; OlderData = var; type = FLOAT; this->category = std::move(category); this->desc = std::move(desc); Browsable = browsable; }

	/**/
	template<>
	EditorVar::EditorVar(String newName, double* var, String category, String desc, bool browsable) : EditorVar() { name = std::move(newName);	ActualData = var; OlderData = var; type = DOUBLE; this->category = std::move(category); this->desc = std::move(desc); Browsable = browsable; }

	/**/
	template<>
	EditorVar::EditorVar(String newName, String* var, String category, String desc, bool browsable) : EditorVar() { name = std::move(newName); ActualData = var; OlderData = var; type = STRING; this->category = std::move(category); this->desc = std::move(desc); Browsable = browsable; }

	/**/
	template<>
	EditorVar::EditorVar(String newName, NList<String> * var, String category, String desc, bool browsable) : EditorVar() { name = std::move(newName); ActualData = var; OlderData = var; type = STRINGARRAY; this->category = std::move(category); this->desc = std::move(desc); Browsable = browsable; }

	// -------------------Math and Color-------------------
	/**/
	template<>
	EditorVar::EditorVar(String newName, Vec2* var, String category, String desc, bool browsable) : EditorVar() { name = std::move(newName); ActualData = var; OlderData = var; type = FLOAT2; this->category = std::move(category); this->desc = std::move(desc); Browsable = browsable; }

	/**/
	template<>
	EditorVar::EditorVar(String newName, Vec3* var, String category, String desc, bool browsable) : EditorVar() { name = std::move(newName); ActualData = var; OlderData = var; type = FLOAT3; this->category = std::move(category); this->desc = std::move(desc); Browsable = browsable; }

	/**/
	template<>
	EditorVar::EditorVar(String newName, Vec4* var, String category, String desc, bool browsable) : EditorVar() { name = std::move(newName); ActualData = var; OlderData = var; type = FLOAT4; this->category = std::move(category); this->desc = std::move(desc); Browsable = browsable; }

	/**/
	template<>
	EditorVar::EditorVar(String newName, Color* var, String category, String desc, bool browsable) : EditorVar() { name = std::move(newName); ActualData = var; OlderData = var; type = COLOR; this->category = std::move(category); this->desc = std::move(desc); Browsable = browsable; }

	// -------------------Scripts, Materials-------------------
	//!@todo Scripts, Materials
	/**/
	template<>
	EditorVar::EditorVar(String newName, I_Texture* var, String category, String desc, bool browsable) : EditorVar() { name = std::move(newName); ActualData = var; OlderData = var; type = TEXTURE; this->category = std::move(category); this->desc = std::move(desc); Browsable = browsable; }

	// ������� ������������ ��������� ��������� - �������� ��� �������
	//template <class T, class Func>
	//struct AssignmentHook {
	//	AssignmentHook(T&_olderData, T&_actualData, Func _f) { if (_olderData != _actualData) _f(_var); }
	//	void Compare() { if (_olderData != _actualData) _f(_var); }

	//private:
	//	T& _olderData;
	//	T& _actualData;
	//};
}