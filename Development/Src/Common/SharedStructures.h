/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

namespace NGTech
{
	/**
	 \brief Generic type container, used by Editor and Engine to serialize
	 any and every class parameter for editing and saving
	*/
	class EditorVar
	{
	public:
		static const std::vector<EditorVar> EMPTY_VARS;

		enum Type {
			UNKNOWN,
			BOOL,
			INT,
			UNINT,
			FLOAT,
			DOUBLE,
			FLOAT2,
			FLOAT3,
			FLOAT4,
			MATRIX,

			FILENAME,
			TEXTURE,
			MATERIAL,
			SHADER,
			SCRIPT,

			STRING,
			COLOR,
			STRINGARRAY,
			VOIDPTR,
		};

		/// Overloaded Set() calls
		EditorVar();

		/*Template based*/
		template<typename T>
		ENGINE_INLINE
			EditorVar(String newName, T * var, String category = "General", String desc = "", bool browsable = true);

		template<typename T>
		ENGINE_INLINE
			EditorVar(String newName, T * var, Type _type, String category = "General", String desc = "", bool browsable = true)
		{
			name = std::move(newName);
			ActualData = var;
			OlderData = var;
			type = _type;
			this->category = std::move(category);
			this->desc = std::move(desc);
			Browsable = browsable;
		}

		template<typename T>
		ENGINE_INLINE
			EditorVar(String newName, T * var, Type _type, String category, String desc, bool browsable, bool _HasSlider, int _SliderMax, int _SliderMin, int _SliderFactor)
		{
			name = std::move(newName);
			ActualData = var;
			OlderData = var;
			type = _type;
			this->category = std::move(category);
			this->desc = std::move(desc);
			Browsable = browsable;
			HasSlider = _HasSlider;
			SliderMax = _SliderMax;
			SliderMin = _SliderMin;
			SliderFactor = _SliderFactor;
		}

		ENGINE_INLINE bool IsBrowsable() { return Browsable; }
		ENGINE_INLINE Type GetType() { return type; }
		ENGINE_INLINE bool IsHasSlider() { return HasSlider; }
		ENGINE_INLINE String GetName() { return name; }
		ENGINE_INLINE String GetDesc() { return desc; }
		ENGINE_INLINE String GetCategory() { return category; }
		ENGINE_INLINE int GetSliderMax() { return SliderMax; }
		ENGINE_INLINE int GetSliderMin() { return SliderMin; }
		ENGINE_INLINE int GetSliderFactor() { return SliderFactor; }

		template<typename T>
		ENGINE_INLINE void SetData(T _var)
		{
			// Setter
			// ���������� 2 �������(!), �� ���������
			if (&(T*)OlderData == &(T*)ActualData)
				return;

			// !todo: Implement setter function(see std::function or raw pointer on function)
			/*if (ExistSetter) {
				return;
			}*/

			_SwapDataWithoutSetter(_var);
		}

		ENGINE_INLINE void* GetData() {
			return ActualData;
		}
	private:
		template<typename T>
		ENGINE_INLINE void _SwapDataWithoutSetter(T _var)
		{
			OlderData = ActualData;
			*((T*)ActualData) = _var;
		}
	private:
		/// Var display type
		Type		type = UNKNOWN;
		/// Var display name
		String		name = StringHelper::EMPTY_STRING;
		/// Description shown in pane
		String		desc = StringHelper::EMPTY_STRING;
		/// Category within browser
		String		category = StringHelper::EMPTY_STRING;
		/// Data, of type 'type'
		void*		ActualData = nullptr;
		void*		OlderData = nullptr;

		/// Whether to display this variable in the editor
		bool Browsable = true;
		bool HasSlider = false;

		int SliderMax = 100;
		int SliderMin = 0;
		int SliderFactor = 1;
	};
}