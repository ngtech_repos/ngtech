/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

namespace NGTech
{
	// 28.07.2017 - stable. Works perefectly.
	// Thread safe and with instanceRef
	template<typename T> // Singleton policy class
	class Singleton
	{
	protected:
		Singleton() = default;
		Singleton(const Singleton&) = delete;
		Singleton& operator=(const Singleton&) = delete;
		virtual ~Singleton() = default;
	public:
		template<typename... Args>
		static T& getInstance(Args... args) // Singleton
		{
#if DEBUG_SINGLETON
			std::cout << "getInstance called" << std::endl;
#endif

			//we pack our arguments in a T&() function...
			//the bind is there to avoid some gcc bug
			static auto onceFunction = std::bind(createInstanceInternal<Args...>, args...);
			//and we apply it once...
			return apply(onceFunction);
		}

	private:

		//This method has one instance per T
		//so the static reference should be initialized only once
		//so the function passed in is called only the first time
		static T& apply(const std::function<T&()>& function)
		{
			static T& instanceRef = function();
			return instanceRef;
		}

		//Internal creation function. We have to make sure it is called only once...
		template<typename... Args>
		static T& createInstanceInternal(Args... args)
		{
			static T instance{ std::forward<Args>(args)... };
			return instance;
		}
	};
} // namespace NGTech