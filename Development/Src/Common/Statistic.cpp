/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "CommonPrivate.h"

namespace NGTech
{
	Statistic::Statistic()
	{
		/**
		Audio
		*/
		audioDelta = 0;
		/**
		Physics
		*/
		physicsDelta = 0;

		/**
		GUI
		*/
		guiDelta = 0;

		/**
		Render
		*/
		renderDelta = 0;
		drawCalls = 0;
		maxDrawCalls = 0;

		renderStateChanges = 0;
		maxRenderStateChanges = 0;

		gpuMemoryWrite = 0;
		maxGPUMemoryWrite = 0;

		gpuMemoryRead = 0;
		maxGPUMemoryRead = 0;
		/**
		Scene
		*/
		lightsCount = 0;
		entityCount = 0;
		camsCount = 0;
		lastFPS = 0;
		/**/
		drawCallsTmp = 0;
		renderStateChangesTmp = 0;
		gpuMemoryWriteTmp = 0;
		gpuMemoryReadTmp = 0;
	}

	/**
	Render
	*/
	void Statistic::SetRenderDelta()
	{
		static Timer timer;
		static unsigned long last_time = timer.GetMilliseconds();
		unsigned long now_time = timer.GetMilliseconds();
		unsigned long time = now_time - last_time;

		renderDelta = time;
		last_time = timer.GetMilliseconds();
	}

	/**
	GUI
	*/
	void Statistic::SetGUIDelta()
	{
		static Timer timer;
		static unsigned long last_time = timer.GetMilliseconds();
		unsigned long now_time = timer.GetMilliseconds();
		unsigned long time = now_time - last_time;

		guiDelta = time;
		last_time = timer.GetMilliseconds();
	}
}