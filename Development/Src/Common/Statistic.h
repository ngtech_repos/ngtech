/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include "NonCopyable.h"
// For clang and gcc std::fmax and std::fmin
//#include <cmath>
#include "MathLib.h"

namespace NGTech
{
	struct Statistic :public Noncopyable
	{
	public:
		Statistic();

		// Physics
		ENGINE_INLINE void SetPhysicsDelta(unsigned long _physicsDelta) {
			physicsDelta = _physicsDelta;
		}
		ENGINE_INLINE void SetPhysicsDeltaZero() {
			physicsDelta = 0;
		}
		ENGINE_INLINE unsigned long GetPhysicsDelta() {
			return physicsDelta;
		}

		// Audio
		ENGINE_INLINE void SetAudioDelta(unsigned long _audioDelta) {
			audioDelta = _audioDelta;
		}
		ENGINE_INLINE void SetAudioDeltaZero() {
			audioDelta = 0;
		}
		ENGINE_INLINE unsigned long GetAudioDelta() {
			return audioDelta;
		}

		// GUI
		void SetGUIDelta();

		ENGINE_INLINE void SetGUIDeltaZero() {
			guiDelta = 0;
		}
		ENGINE_INLINE unsigned long GetGUIDelta() {
			return guiDelta;
		}

		// Render
		void SetRenderDelta();

		ENGINE_INLINE unsigned long GetRenderDelta() {
			return renderDelta;
		}

		ENGINE_INLINE void RecordMemoryWrite(size_t numBytes) {
			gpuMemoryWriteTmp += numBytes;
		}

		ENGINE_INLINE void RecordMemoryRead(size_t numBytes) {
			gpuMemoryReadTmp += numBytes;
		}

		ENGINE_INLINE void RecordDrawCalls() {
			drawCallsTmp++;
		}

		ENGINE_INLINE unsigned int GetDrawCalls() {
			return drawCalls;
		}

		ENGINE_INLINE unsigned int GetMaxDrawCalls() {
			return maxDrawCalls;
		}

		ENGINE_INLINE unsigned int GetRenderStateChanges() {
			return renderStateChanges;
		}

		ENGINE_INLINE unsigned int GetMaxRenderStateChanges() {
			return maxRenderStateChanges;
		}

		ENGINE_INLINE unsigned int GetLastFPS() { return lastFPS; }
		ENGINE_INLINE void SetLastFPS(unsigned int _lastFPS) { lastFPS = _lastFPS; }

		ENGINE_INLINE float GetGPUMemoryRead() {
			return gpuMemoryRead;
		}

		ENGINE_INLINE float GetMaxGPUMemoryRead() {
			return maxGPUMemoryRead;
		}

		ENGINE_INLINE float GetGPUMemoryWrite() {
			return gpuMemoryWrite;
		}

		ENGINE_INLINE float GetMaxGPUMemoryWrite() {
			return maxGPUMemoryWrite;
		}

		// Scene

		ENGINE_INLINE unsigned int GetLightsCount() {
			return lightsCount;
		}

		ENGINE_INLINE unsigned int GetEntityCount() {
			return entityCount;
		}

		ENGINE_INLINE unsigned int GetCamerasCount() {
			return camsCount;
		}

		ENGINE_INLINE void SetCamerasCount(unsigned int _count) {
			camsCount = _count;
		}

		ENGINE_INLINE void SetLightsCount(unsigned int _count) {
			lightsCount = _count;
		}

		ENGINE_INLINE void SetEntityCount(unsigned int _count) {
			entityCount = _count;
		}

		ENGINE_INLINE void AddCamerasCount() {
			camsCount++;
		}

		ENGINE_INLINE void AddLightsCount() {
			lightsCount++;
		}

		ENGINE_INLINE void AddEntityCount() {
			entityCount++;
		}

		ENGINE_INLINE void ReduceLightsCount() {
			lightsCount--;
		}

		ENGINE_INLINE void ReduceCamerasCount() {
			camsCount--;
		}

		ENGINE_INLINE void ReduceEntityCount() {
			entityCount--;
		}

		ENGINE_INLINE void PrepareToNewFrame() {
			_ResetGPUStats();
		}

		ENGINE_INLINE void Reset() {
			/*Rendering*/
			drawCalls = 0;
			drawCallsTmp = 0;
			maxDrawCalls = 0;

			renderStateChanges = 0;
			renderStateChangesTmp = 0;
			maxRenderStateChanges = 0;

			lightsCount = 0;
			entityCount = 0;
			camsCount = 0;

			gpuMemoryWrite = 0;
			gpuMemoryWriteTmp = 0;
			maxGPUMemoryWrite = 0;

			gpuMemoryRead = 0;
			gpuMemoryReadTmp = 0;
			maxGPUMemoryRead = 0;
		}

	private:
		ENGINE_INLINE void _ResetDrawCallsCountZero() {
			drawCalls = drawCallsTmp;
			maxDrawCalls = Math::Max<unsigned int>(maxDrawCalls, drawCalls);
			drawCallsTmp = 0;
		}

		ENGINE_INLINE void _ResetRenderStateChangesCountZero() {
			renderStateChanges = renderStateChangesTmp;
			maxRenderStateChanges = Math::Max<unsigned int>(maxRenderStateChanges, renderStateChanges);
			renderStateChangesTmp = 0;
		}

		ENGINE_INLINE void _ResetGPUStats()
		{
			_ResetDrawCallsCountZero();
			_ResetRenderStateChangesCountZero();

			gpuMemoryWrite = Math::Max<size_t>(gpuMemoryWriteTmp, gpuMemoryWrite);
			maxGPUMemoryWrite = Math::Max<size_t>(maxGPUMemoryWrite, gpuMemoryWrite);
			gpuMemoryWriteTmp = 0;

			gpuMemoryRead = Math::Max<size_t>(gpuMemoryReadTmp, gpuMemoryRead);
			maxGPUMemoryRead = Math::Max<size_t>(maxGPUMemoryRead, gpuMemoryRead);
			gpuMemoryReadTmp = 0;
		}
	private:
		/*Rendering*/
		unsigned int drawCalls = 0, drawCallsTmp = 0;
		unsigned int maxDrawCalls = 0;

		/*!todo Not implemented
		 *
		 *
		 TODO:Not implemented*/
		unsigned int renderStateChanges = 0, renderStateChangesTmp = 0;
		unsigned int maxRenderStateChanges = 0;

		/*Scene*/
		unsigned int lightsCount = 0;
		unsigned int entityCount = 0;
		unsigned int camsCount = 0;
		unsigned int lastFPS = 0;

		/*Delta updates for subsystems*/
		unsigned long physicsDelta = 0, audioDelta = 0, renderDelta = 0, guiDelta = 0;

		/*Rendering*/
		size_t gpuMemoryWrite = 0, gpuMemoryWriteTmp = 0;
		size_t maxGPUMemoryWrite = 0;

		/*!todo Not implemented
		 *
		 *
		 TODO:Not implemented*/
		size_t gpuMemoryRead = 0, gpuMemoryReadTmp = 0;
		size_t maxGPUMemoryRead = 0;
	};
}