/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//**************************************
#include "StringHelper.h"
#include <stdlib.h>
#include <stdio.h>
#include <cstdlib>
#include <stdarg.h>
#include <algorithm>
#include <string.h>
//**************************************

namespace NGTech {
	/**/
	const String StringHelper::EMPTY_STRING("");

	/**
	*/
	int StringHelper::getWordsNumber(const String &input) {
		int n = 0;
		size_t pos = 0;

		if (input[pos] == ' ') pos = input.find_first_not_of(' ', pos);
		while (pos < input.length()) {
			n++;
			pos = input.find_first_of(' ', pos);
			if (pos < input.length()) pos = input.find_first_not_of(' ', pos);
		}
		return n;
	}

	/**
	*/
	String StringHelper::getWord(const String &input, unsigned int n) {
		String output;
		unsigned int k = 0;
		size_t pos = 0, last = 0;

		if ((input[pos] == ' ') || (input[pos] == '\x9')) {
			pos = output.find_first_not_of(" \x9", pos);
		}

		while (pos < input.length()) {
			k++;
			last = pos;
			pos = input.find_first_of(' ', pos);
			if (k == n) {
				output = input.substr(last, pos - last);
				return output;
			}
			if (pos < input.length()) {
				pos = input.find_first_not_of(' ', pos);
			}
		}
		return "";
	}

	/**
	*/
	void StringHelper::getPath(String& out, const String& filepath)
	{
		size_t pos = filepath.find_last_of("\\/");

		if (pos != String::npos)
#if PLATFORM_OS == PLATFORM_OS_WINDOWS
			out = filepath.substr(0, pos) + '\\';
#else
			out = filepath.substr(0, pos) + '/';
#endif
		else
			out = "";
	}

	/**
	*/
	void StringHelper::getExtension(String& out, const String& str)
	{
		size_t pos = str.find_last_of('.');

		if (pos == String::npos)
			out = "";
		else
		{
			out = str.substr(pos + 1, str.length());
			toLower(out, out);
		}
	}

	/**
	*/
	void StringHelper::toLower(String& out, const String& str)
	{
		out.resize(str.size());

		for (size_t i = 0; i < str.size(); ++i)
		{
			if (str[i] >= 'A' && str[i] <= 'Z')
				out[i] = str[i] + 32;
			else
				out[i] = str[i];
		}
	}

	/**
	*/
	void StringHelper::getFile(String& out, const String& filepath)
	{
		size_t pos = filepath.find_last_of("\\/");

		if (pos != String::npos)
			out = filepath.substr(pos + 1);
		else
			out = filepath;
	}

	/**
	*/
	int StringHelper::toInt(const String &input) {
		return (atoi(input.c_str()));
	}

	/**
	*/
	float StringHelper::toFloat(const String &input) {
		return (float)(atof(input.c_str()));
	}

	/**
	*/
	double StringHelper::toDouble(const String &input) {
		return (double)(atof(input.c_str()));
	}

	/**
	*/
	int StringHelper::getInt(const String &input, unsigned int n) {
		String temp = getWord(input, n);
		return toInt(temp);
	}

	/**
	*/
	float StringHelper::getFloat(const String &input, unsigned int n) {
		String temp = getWord(input, n);
		return toFloat(temp);
	}

	/**
	*/
	double StringHelper::getDouble(const String &input, unsigned int n) {
		String temp = getWord(input, n);
		return toDouble(temp);
	}

	/**
	*/
	String StringHelper::toUpper(const String &input) {
		String buf = input;
		std::transform(buf.begin(), buf.end(), buf.begin(), ::toupper);
		return buf;
	}

	/**
	*/
	String StringHelper::fromInt(int i) {
		char buf[32];
		sprintf(buf, "%d", i);
		return String(buf);
	}

	/**
	*/
	String StringHelper::fromFloat(float i) {
		char buf[32];
		sprintf(buf, "%f", i);
		return String(buf);
	}

	/**
	*/
	String StringHelper::fromDouble(double i) {
		char buf[32];
		sprintf(buf, "%f", i);
		return String(buf);
	}

	/**
	*/
	String StringHelper::fromBool(bool i) {
		if (i) {
			return "TRUE";
		}
		else {
			return "FALSE";
		}
	}

	/**
	*/
	void StringHelper::Copy(char *dest, const char *src, int destsize) {
		if (!src) {
			return;
		}
		if (destsize < 1) {
			return;
		}

		strncpy(dest, src, destsize - 1);
		dest[destsize - 1] = 0;
	}

	/**
	*/
	String StringHelper::StickString(const char* text, ...) {
		char           msg[8000];

		va_list         argptr;
		va_start(argptr, text);
		vsprintf(msg, text, argptr);
		va_end(argptr);

		return msg;
	}

	/**
	*/
	String StringHelper::CutExt(const String& fullname)
	{
		size_t lastindex = fullname.find_last_of('.');
		String rawname = fullname.substr(0, lastindex);
		return rawname;
	}

	String StringHelper::GetString(const wchar_t * wstr)
	{
		std::wstring ws(wstr);
		String str(ws.begin(), ws.end());
		return str;
	}

	//replaces ' ' with '_'
	void StringHelper::ReplaceSpaces(String &str)
	{
		char buffer[1024];
		strcpy(buffer, str.c_str());

		int i = 0;
		while (i < strlen(buffer))
		{
			if (buffer[i] == ' ')
				buffer[i] = '_';
			i++;
		}

		str.assign(buffer);
	}
}