/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#ifndef STRING_HELPER_H
#define STRING_HELPER_H
//**************************************
#include <string>
#include <sstream>
//**************************************
#include "../Platform/inc/SystemUtils.h"
//**************************************

namespace NGTech {
	//---------------------------------------------------------------------------
	//Desc: some important String functions
	//---------------------------------------------------------------------------
	struct StringHelper
	{
		static int getWordsNumber(const String &input);
		static String getWord(const String &input, unsigned int n);

		static int toInt(const String &input);
		static float toFloat(const String &input);
		static double toDouble(const String &input);

		static int getInt(const String &input, unsigned int n);
		static float getFloat(const String &input, unsigned int n);
		static double getDouble(const String &input, unsigned int n);

		/**
		*/
		static void toLower(String& out, const String& str);
		static String toUpper(const String &input);

		static String fromInt(int i);
		static String fromDouble(double i);
		static String fromFloat(float i);
		static String fromBool(bool i);

		/**
		*/
		static void getPath(String& out, const String& filepath);

		/**
		*/
		static void getExtension(String& out, const String& str);
		static void getFile(String& out, const String& filepath);

		static String CutExt(const String& fullname);

		static String GetString(const wchar_t* wstr);
		static void ReplaceSpaces(String &str);

		/**
		*/
		static void Copy(char *dest, const char *src, int destsize);

		static String StickString(const char*, ...);

		template < typename T > static ENGINE_INLINE String to_string(const T& n)
		{
#if (__clang__) || (__GNUG__) || (__GNUC__)
			std::ostringstream ss;
			ss << n;
			return ss.str();
#else
			return std::to_string(n);
#endif
		}

		static const String EMPTY_STRING;
	};
}

#endif //STRING_HELPER_H