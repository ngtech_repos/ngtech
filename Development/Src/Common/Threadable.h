/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

namespace NGTech {
	struct ThreadTask;

	class Threadable :public Noncopyable
	{
	protected:
		Threadable() { m_ScriptTasks.clear(); }
		virtual ~Threadable() {}

		virtual void _InitThreadTasks() {}
		virtual void _DestroyThreadTasks()
		{
			for (auto & script : m_ScriptTasks)
			{
				script.reset();
			}
		}

		virtual void _UpdateJobs() {}

	protected:
		std::vector<std::shared_ptr<ThreadTask>>	m_ScriptTasks;
	};
}