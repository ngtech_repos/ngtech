/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include <string>
#include "ConfigIO.OU.inc"

namespace NGTech
{
#if !defined INI_BUFFERSIZE
#define INI_BUFFERSIZE  512
#endif
	/*!
	@brief Config file

	This is class implemented ini classes support.
	*/
	class CORE_API Config
	{
	public:
		Config(const String& filename) : iniFilename(filename)
		{ }

		/**
		Get value from config as boolean
		@param[in] _section Section name
		@param[in] _key Key name in section
		@param[in] _defValue Default value
		*/
		ENGINE_INLINE bool GetBool(const String& _section, const String& _key, bool _defValue = false) {
			return ini_getbool(_section.c_str(), _key.c_str(), int(_defValue), iniFilename.c_str()) != 0;
		}

		/**
		Get value from config as long
		@param[in] _section Section name
		@param[in] _key Key name in section
		@param[in] _defValue Default value
		*/
		ENGINE_INLINE long GetLong(const String& _section, const String& _key, long _defValue = 0) {
			return ini_getl(_section.c_str(), _key.c_str(), _defValue, iniFilename.c_str());
		}

		/**
		Get value from config as int
		@param[in] _section Section name
		@param[in] _key Key name in section
		@param[in] _defValue Default value
		*/
		ENGINE_INLINE int GetInt(const String& _section, const String& _key, int _defValue = 0) {
			return int(this->GetLong(_section, _key, long(_defValue)));
		}

		/**
		Get value from config as String
		@param[in] _section Section name
		@param[in] _key Key name in section
		@param[in] _defValue Default value
		*/
		ENGINE_INLINE String GetString(const String& _section, const String& _key, const String& _defValue = "") {
			char buffer[INI_BUFFERSIZE];
			ini_gets(_section.c_str(), _key.c_str(), _defValue.c_str(), buffer, INI_BUFFERSIZE, iniFilename.c_str());
			return buffer;
		}

		/**
		Get section as String
		@param[in] _idx Key id as int
		*/
		ENGINE_INLINE String GetSection(int _idx) {
			char buffer[INI_BUFFERSIZE];
			ini_getsection(_idx, buffer, INI_BUFFERSIZE, iniFilename.c_str());
			return buffer;
		}

		/**
		Get key as String
		@param[in] _section Section name
		@param[in] _idx Key id as int in Section
		*/
		ENGINE_INLINE String GetKey(const String& _section, int _idx) {
			char buffer[INI_BUFFERSIZE];
			ini_getkey(_section.c_str(), _idx, buffer, INI_BUFFERSIZE, iniFilename.c_str());
			return buffer;
		}

		/**
		Get key as float
		@param[in] _section Section name
		@param[in] _idx Key id as int in Section
		*/
		ENGINE_INLINE float GetFloat(const String& _section, const String& _key, float DefValue = 0) {
			return ini_getf(_section.c_str(), _key.c_str(), DefValue, iniFilename.c_str());
		}

		/**
		Put long value with key in section
		@param[in] _section Section name
		@param[in] _key Key name
		@param[in] _value Value as long
		*/
		ENGINE_INLINE bool Put(const String& _section, const String& _key, long _value) {
			return ini_putl(_section.c_str(), _key.c_str(), _value, iniFilename.c_str()) != 0;
		}

		/**
		Put int value with key in section
		@param[in] _section Section name
		@param[in] _key Key name
		@param[in] _value Value as int
		*/
		ENGINE_INLINE bool Put(const String& _section, const String& _key, int _value) {
			return ini_putl(_section.c_str(), _key.c_str(), (long)_value, iniFilename.c_str()) != 0;
		}

		/**
		Put boolean value with key in section
		@param[in] _section Section name
		@param[in] _key Key name
		@param[in] _value Value as boolean
		*/
		ENGINE_INLINE bool Put(const String& _section, const String& _key, bool _value) {
			return ini_putl(_section.c_str(), _key.c_str(), (long)_value, iniFilename.c_str()) != 0;
		}

		/**
		Put String value with key in section
		@param[in] _section Section name
		@param[in] _key Key name
		@param[in] _value Value as string
		*/
		ENGINE_INLINE bool Put(const String& _section, const String& _key, const String& _value) {
			return ini_puts(_section.c_str(), _key.c_str(), _value.c_str(), iniFilename.c_str()) != 0;
		}

		/**
		Put const char* value with key in section
		@param[in] _section Section name
		@param[in] _key Key name
		@param[in] _value Value as const char*
		*/
		ENGINE_INLINE bool Put(const String& _section, const String& _key, const char* _value) {
			return ini_puts(_section.c_str(), _key.c_str(), _value, iniFilename.c_str()) != 0;
		}

		/**
		Put float value with key in section
		@param[in] _section Section name
		@param[in] _key Key name
		@param[in] _value Value as float
		*/
		ENGINE_INLINE bool Put(const String& _section, const String& _key, float _value) {
			return ini_putf(_section.c_str(), _key.c_str(), _value, iniFilename.c_str()) != 0;
		}

		/**
		Delete key from section
		@param[in] _section Section name
		@param[in] _key Key name
		*/
		ENGINE_INLINE bool Del(const String& _section, const String& _key) {
			return ini_puts(_section.c_str(), _key.c_str(), 0, iniFilename.c_str()) != 0;
		}

		/**
		Delete section
		@param[in] _section Section name
		*/
		ENGINE_INLINE bool Del(const String& _section) {
			return ini_puts(_section.c_str(), 0, 0, iniFilename.c_str()) != 0;
		}

	private:
		int   ini_getbool(const char *Section, const char *Key, int DefValue, const char *Filename);
		long  ini_getl(const char *Section, const char *Key, long DefValue, const char *Filename);
		size_t   ini_gets(const char *Section, const char *Key, const char *DefValue, char *Buffer, int BufferSize, const char *Filename);
		size_t   ini_getsection(int idx, char *Buffer, int BufferSize, const char *Filename);
		size_t   ini_getkey(const char *Section, int idx, char *Buffer, int BufferSize, const char *Filename);

		float ini_getf(const char *Section, const char *Key, float DefValue, const char *Filename);

		int   ini_putl(const char *Section, const char *Key, long Value, const char *Filename);
		int   ini_puts(const char *Section, const char *Key, const char *Value, const char *Filename);
		int   ini_putf(const char *Section, const char *Key, float Value, const char *Filename);

		typedef int(*INI_CALLBACK)(const char *Section, const char *Key, const char *Value, const void *UserData);
		size_t ini_browse(INI_CALLBACK Callback, const void *UserData, const char *Filename);

	private:
		String iniFilename;
	};
}