/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

// Core API macro
#include "coredll.h"

namespace NGTech {
	/*
	Forward declarations
	*/
	struct I_Window;
	struct I_RenderLowLevel;
	struct I_Texture;
	struct I_Audio;
	struct IGame;
	struct IScriptInterp;
	struct Statistic;
#if USE_STEAMWORKS
	struct SteamWorksMgr;
#endif
	class Log;
	class PlatformManager;
	class ScriptManager;
	class CVARManager;
	class FileSystem;
	class PhysSystem;
	class GUI;
	class RenderPipeline;
	class SceneManager;
	class SystemInfo;
	class MeshLoader;
	class SkinnedMeshLoader;
	struct DebugLayer;
	struct Cache;

	enum class RenderDrv
	{
		OpenGLDrv = 0,
		NULLDrv,
		NUM_DRV
	};

	/*!
		\brief Main core class. Only for using core and render.

		This is class contains only base methods and will replaced on Engine.
	*/
	struct CORE_API CoreManager
	{
		CoreManager();
		virtual ~CoreManager();

		virtual void Quit() {}
		virtual void Initialise(void* _hwnd) {}
		virtual void MainLoop() {}
		virtual void LoadEngineModule(const char* _name) {}
		virtual void LoadEngineModuleEditor(const char*_name) {}

		/**
		*/
		ENGINE_INLINE void SetRender(I_RenderLowLevel*_r) { ASSERT(_r, "Invalid pointer on render"); iRender = _r; }

		/**
		*/
		ENGINE_INLINE bool IsEditor() const { return mIsEditor; }

		/**
		*/
		ENGINE_INLINE void RunEditor(bool _ed) { mIsEditor = _ed; }

		/**
		*/
		ENGINE_INLINE bool IsRunning() const { return running; }

		/**
		*/
		ENGINE_INLINE TimeDelta GetElapsedTimeFromStart() const { return elapsedTimeFromStart; }

		/**
		Command Line Utils
		\param[in] argc Arguments count
		\param[in] argv Arguments varible
		*/
		void ParseCommandLine(int argc, const char * const * argv);

		/**
		Registrator of inheritor of IScriptInterp
		\param[in] _interp IScriptInterp pointer
		*/
		void RegisterLuaBindings(IScriptInterp*_interp);

		/**
		Registrator of inheritor of IScriptInterp
		\param[in] _interp IScriptInterp shared pointer
		*/
		void RegisterLuaBindings(const std::shared_ptr<IScriptInterp> &_interp);

		/**
		Getter container of IScriptInterp
		\returns std::vector<std::shared_ptr<IScriptInterp>>&
		*/
		ENGINE_INLINE const std::vector<std::shared_ptr<IScriptInterp>>& GetLuaBindings() const { return m_LuaBindings; }
	protected:
		/**
		LuaBindings
		*/
		void InitAllLuaBindings();
		void DestroyAllLuaBindings();
	private:
		/**
		*/
		void _PreInitFast();
		/**
		*/
		void _InitUserDir(const char* dir = "../userData/");
		/**
		*/
		bool _InitPlatformCode();
		/**
		*/
	public:
		/**
		*/
		I_Window* iWindow = nullptr;
		/**
		*/
		I_RenderLowLevel*iRender = nullptr;
		/**
		*/
		std::shared_ptr<CVARManager> cvars = nullptr;
		/**
		*/
		std::shared_ptr<Log> log = nullptr;
		/**
		*/
		FileSystem*vfs = nullptr;
		/**
		*/
		I_Audio *alSystem = nullptr;
		/**
		*/
		PhysSystem *physSystem = nullptr;
		/**
		*/
		std::shared_ptr<SystemInfo> info = nullptr;
		/**
		*/
		std::shared_ptr<Cache> cache = nullptr;
		/**
		*/
		GUI*gui = nullptr;
		/**
		*/
		RenderPipeline*rpipeline = nullptr;
		/**
		*/
		SceneManager* scene = nullptr;
		/**
		*/
		std::shared_ptr<IGame> game = nullptr;
		/**
		*/
		MeshLoader *meshLoader = nullptr;
		/**
		*/
		SkinnedMeshLoader *skinnedMeshLoader = nullptr;
		/**
		*/
		DebugLayer* debug = nullptr;
#if USE_STEAMWORKS
		/**
		*/
		std::shared_ptr<SteamWorksMgr> steamworks = nullptr;
#endif
		/**
		*/
		PlatformManager* platformM = nullptr;
		/**
		*/
		std::shared_ptr<ScriptManager> scriptManager = nullptr;

		/**
		*/
		std::vector<std::shared_ptr<IScriptInterp>> m_LuaBindings;
		/**
		*/
		bool mIsEditor = false;
	protected:
		/**
		*/
		bool running = false;
		/**
		*/
		TimeDelta elapsedTimeFromStart = Math::ZERODELTA;
	};

	/**
	For Using in Render and another libs,what not will link with Engine.dll
	After creation Engine pointer on Core will pointer on Engine
	 */
	CORE_API CoreManager* GetCore();
	/**
	*/
	CORE_API void SetCore(CoreManager* _core);
	/**
	*/
	CORE_API FileSystem* GetVFS();
	/**
	*/
	CORE_API I_Window* GetWindow();
	/**
	*/
	CORE_API const std::shared_ptr<CVARManager>& GetCvars();
	/**
	*/
	CORE_API I_RenderLowLevel* GetRender();
	/**
	*/
	CORE_API I_Audio* GetAudio();
	/**
	*/
	CORE_API PhysSystem* GetPhysics();
	/**
	*/
	CORE_API const std::shared_ptr<Cache>& GetCache();
	/**
	*/
	CORE_API GUI* GetGUI();
	/**
	*/
	CORE_API RenderPipeline* GetRenderPipeline();
	/**
	*/
	CORE_API SceneManager* GetScene();
	/**
	*/
	CORE_API const std::shared_ptr<IGame>& GetGame();
	/**
	*/
	CORE_API SkinnedMeshLoader* GetSkinnedMeshLoader();
	/**
	*/
	CORE_API MeshLoader* GetMeshLoader();
	/**
	*/
	CORE_API DebugLayer* GetDebug();
	/**
	*/
#if USE_STEAMWORKS
	CORE_API const std::shared_ptr<SteamWorksMgr>& GetSteamworksMgr();
#endif
	/**
	*/
#ifndef DROP_EDITOR
	CORE_API const std::shared_ptr<Statistic>& GetStatistic();
#endif
	/**
	*/
	CORE_API const std::shared_ptr<ScriptManager>& GetScriptManager();
}