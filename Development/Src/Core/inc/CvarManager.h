/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//**************************************
#include "../../Common/NonCopyable.h"
//**************************************

namespace NGTech {
	class Config;
	class CORE_API CVARManager :public Noncopyable
	{
	public:
		explicit CVARManager(const std::shared_ptr<Config>& c);
		CVARManager();
		virtual ~CVARManager();

		bool SaveConfig();
		ENGINE_INLINE bool GetCanBeSaved() { return canBeSaved; }
		ENGINE_INLINE void SetCanBeSaved(bool _v) { canBeSaved = _v; }
		void ParseCommandLine(int argc, const char * const * argv);
	private:
		void _ValidateConfig();
		void _ParseSystemCFG();
	public:
		//Render
		int r_width, r_height, r_bpp, r_zdepth, r_shadowtype, r_shadowsize, r_parallax;
		bool r_fullscreen, r_specular, r_hdr, r_wireframe, r_reflections, r_ShadersAllowCache;

		/**
		������������� ����� ������������ � ��������
		*/
		enum AA_MODE
		{
			AA_NONE = -1,
			AA_FXAA_VERYLOW,
			AA_FXAA_LOW,
			AA_FXAA_MEDIUM,
			AA_FXAA_HIGH,
			AA_FXAA_VERYHIGH,
			AA_FXAA_ULTRA,
		};
		AA_MODE r_aa_mode;

		// Window
		bool w_withoutBorder;
		// Client
		int cl_fov;
		// Client gamma value, will be unsigned
		float cl_gamma;
		// Debug
		bool d_DisplayInfo;
		// System
		String sys_game_dll, sys_game_folder;
		// Physics
		const static Vec3 m_DefaultGravity;
	private:
		std::shared_ptr<Config> cfg;
		std::shared_ptr<Config> systemCfg;
		bool canBeSaved;
	};
}