/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include "coredll.h"
#include "../../Common/IDataManager.h"

namespace NGTech
{
	class CORE_API DataStream :
		public IDataStream
	{
	public:
		DataStream();
		DataStream(std::ifstream* _stream);
		virtual ~DataStream();

		virtual bool Eof() override;
		virtual size_t Size() override;
		virtual void Readline(String& _source, unsigned int _delim) override;
		virtual size_t Read(void* _buf, size_t _count) override;

	protected:
		std::ifstream* mStream = nullptr;;
		size_t mSize = (size_t)-1;
	};
} // namespace NGTech