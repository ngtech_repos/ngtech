/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#if PLATFORM_OS == PLATFORM_OS_WINDOWS
#    define DYNLIB_HANDLE hInstance
#    define DYNLIB_LOAD( a ) LoadLibraryA( a )
#    define DYNLIB_GETSYM( a, b ) GetProcAddress( a, b )
#    define DYNLIB_UNLOAD( a ) !FreeLibrary( a )

struct HINSTANCE__;
typedef struct HINSTANCE__* hInstance;

#elif (PLATFORM_OS == PLATFORM_OS_LINUX) || (PLATFORM_OS == PLATFORM_OS_ANDROID)
#    define DYNLIB_HANDLE void*
#    define DYNLIB_LOAD( a ) dlopen( a, RTLD_LAZY )
#    define DYNLIB_GETSYM( a, b ) dlsym( a, b )
#    define DYNLIB_UNLOAD( a ) dlclose( a )

#elif PLATFORM_OS == PLATFORM_OS_MACOSX
#    define DYNLIB_HANDLE CFBundleRef
#    define DYNLIB_LOAD( a ) mac_loadExeBundle( a )
#    define DYNLIB_GETSYM( a, b ) mac_getBundleSym( a, b )
#    define DYNLIB_UNLOAD( a ) mac_unloadExeBundle( a )
#endif

namespace NGTech
{
	//-----------------------------------------------------------------------------
	// Cross-platform dynamic library system
	//-----------------------------------------------------------------------------
	class CORE_API DynLib
	{
	public:
		///
		DynLib(const String& name);
		///
		~DynLib();

		/// Load the library
		void Load();
		/// Unload the library
		void Unload();
		/// Get the name of the library
		const String& GetName(void) const { return mName; }
		/// Returns the address (handle) of the given symbol from the loaded library.
		void* GetSymbol(const String& strName) const throw();
	protected:
		String mName;
		/// Gets the last loading error
		String GetError(void);
	protected:
		/// Handle to the loaded library.
		DYNLIB_HANDLE m_hInst;
	};
}