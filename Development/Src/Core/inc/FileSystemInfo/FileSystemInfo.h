/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
/*!
	@file
	@author		Albert Semenov
	@date		09/2009
	@module
	*/
#ifndef __FILE_SYSTEM_INFO_H__
#define __FILE_SYSTEM_INFO_H__

#if PLATFORM_OS == PLATFORM_OS_WINDOWS
#include <windows.h>
#include <io.h>
#else
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fnmatch.h>
#endif

#include <algorithm>
#include <string>
#include <vector>
#include <locale>

namespace NGTech
{
	struct FileInfo
	{
		FileInfo(const std::wstring& _name, bool _folder) : name(_name), folder(_folder) { }
		std::wstring name;
		bool folder;
	};
	typedef std::vector<FileInfo> VectorFileInfo;

	ENGINE_INLINE bool isAbsolutePath(const wchar_t* path)
	{
#if PLATFORM_OS == PLATFORM_OS_WINDOWS
		if (IsCharAlphaW(path[0]) && path[1] == ':')
			return true;
#endif
		return path[0] == '/' || path[0] == '\\';
	}

	ENGINE_INLINE void toLower(std::wstring& _input)
	{
		std::transform(
			_input.begin(), _input.end(),
			_input.begin(),
			towlower);
	}

	ENGINE_INLINE bool sortFiles(const FileInfo& left, const FileInfo& right)
	{
		if (left.folder < right.folder)
			return true;
		if (left.folder > right.folder)
			return false;

		auto name = left.name;
		toLower(name);
		name = right.name;
		toLower(name);

		return (left.name) < (right.name);
	}

	ENGINE_INLINE bool endWith(const std::wstring& _source, const std::wstring& _value)
	{
		size_t count = _value.size();
		if (_source.size() < count)
			return false;
		size_t offset = _source.size() - count;
		for (size_t index = 0; index < count; ++index)
		{
			if (_source[index + offset] != _value[index])
				return false;
		}
		return true;
	}

	ENGINE_INLINE std::wstring concatenatePath(const std::wstring& _base, const std::wstring& _name)
	{
		if (_base.empty() || isAbsolutePath(_name.c_str()))
		{
			return _name;
		}
		else
		{
			if (endWith(_base, L"\\") || endWith(_base, L"/"))
				return _base + _name;

#if PLATFORM_OS == PLATFORM_OS_WINDOWS
			return _base + L'\\' + _name;
#else
			return _base + L'/' + _name;
#endif
		}
	}

	ENGINE_INLINE bool isReservedDir(const wchar_t* _fn)
	{
		// if "."
		return (_fn[0] == '.' && _fn[1] == 0);
	}

	ENGINE_INLINE bool isParentDir(const wchar_t* _fn)
	{
		// if ".."
		return (_fn[0] == '.' && _fn[1] == '.' && _fn[2] == 0);
	}

	ENGINE_INLINE void getSystemFileList(VectorFileInfo& _result, const std::wstring& _folder, const std::wstring& _mask, bool _sorted = true)
	{
#if PLATFORM_OS == PLATFORM_OS_WINDOWS
		//FIXME add optional parameter?
		bool ms_IgnoreHidden = true;

		intptr_t lHandle;
		int res;
		struct _wfinddata_t tagData;

		// pattern can contain a directory name, separate it from mask
		size_t pos = _mask.find_last_of(L"/\\");
		std::wstring directory;
		if (pos != _mask.npos)
			directory = _mask.substr(0, pos);

		std::wstring full_mask = concatenatePath(_folder, _mask);

		lHandle = _wfindfirst(full_mask.c_str(), &tagData);
		res = 0;
		while (lHandle != -1 && res != -1)
		{
			if ((!ms_IgnoreHidden || (tagData.attrib & _A_HIDDEN) == 0) &&
				!isReservedDir(tagData.name))
			{
				_result.push_back(FileInfo(concatenatePath(directory, tagData.name), (tagData.attrib & _A_SUBDIR) != 0));
			}
			res = _wfindnext(lHandle, &tagData);
		}
		// Close if we found any files
		if (lHandle != -1)
			_findclose(lHandle);
#else
		DIR* dir = opendir(UString(_folder).asUTF8_c_str());
		struct dirent* dp;

		if (dir == NULL)
		{
			/* opendir() failed */
			Error((String("[VFS] Can't open ") + UString(_folder).asUTF8_c_str()).c_str(), false);
			return;
		}

		rewinddir(dir);

		while ((dp = readdir(dir)) != NULL)
		{
			if ((fnmatch(UString(_mask).asUTF8_c_str(), dp->d_name, 0) == 0)
				&& !isReservedDir(UString(dp->d_name).asWStr_c_str()))
			{
				struct stat fInfo;
				String path = UString(_folder).asUTF8() + "/" + dp->d_name;
				if (stat(path.c_str(), &fInfo) == -1)
					perror("[VFS-Linux ERROR] ");
				_result.push_back(FileInfo(UString(dp->d_name).asWStr(), (S_ISDIR(fInfo.st_mode))));
			}
		}

		closedir(dir);
#endif

		if (_sorted)
		{
			std::stable_sort(_result.begin(), _result.end(), sortFiles);
		}
	}

	ENGINE_INLINE std::wstring getSystemCurrentFolder()
	{
#if PLATFORM_OS == PLATFORM_OS_WINDOWS
		wchar_t buff[MAX_PATH + 1];
		::GetCurrentDirectoryW(MAX_PATH, buff);
		return buff;
#else
#   ifndef PATH_MAX
#       define PATH_MAX 256
#   endif
		char buff[PATH_MAX + 1];
		return getcwd(buff, PATH_MAX) ? UString(buff).asWStr() : std::wstring();
#endif
	}

	typedef std::vector<std::wstring> VectorWString;
	ENGINE_INLINE void scanFolder(VectorWString& _result, const std::wstring& _folder, bool _recursive, const std::wstring& _mask, bool _fullpath)
	{
		std::wstring folder = _folder;
		if (!folder.empty() && *folder.rbegin() != '/' && *folder.rbegin() != '\\') folder += L"/";

		VectorFileInfo result;
		getSystemFileList(result, folder, _mask);

		for (VectorFileInfo::const_iterator item = result.begin(); item != result.end(); ++item)
		{
			if (item->folder) continue;

			if (_fullpath)
				_result.push_back(folder + item->name);
			else
				_result.push_back(item->name);
		}

		if (_recursive)
		{
			getSystemFileList(result, folder, L"*");

			for (VectorFileInfo::const_iterator item = result.begin(); item != result.end(); ++item)
			{
				if (!item->folder
					|| item->name == L".."
					|| item->name == L".") continue;
				scanFolder(_result, folder + item->name, _recursive, _mask, _fullpath);
			}
		}
	}
}

#endif // __FILE_SYSTEM_INFO_H__