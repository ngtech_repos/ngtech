/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//***************************************************************************
#include "IncludesAndLibs.h"
//***************************************************************************
#include "../../Common/StringHelper.h"
//***************************************************************************
#include "dds.h"
//***************************************************************************

namespace NGTech {
	/**
	Image format
	*/
	enum class ILFormats {
		UNKNOWN = 0,
		COLOUR_INDEX = 0x1900,
		COLOR_INDEX = 0x1900,
		ALPHA = 0x1906,
		RGB = 0x1907,
		RGBA = 0x1908,
		BGR = 0x80E0,
		BGRA = 0x80E1,
		LUMINANCE = 0x1909,
		LUMINANCE_ALPHA = 0x190A
	};

	struct ImageData
	{
		unsigned int Width = 0;
		unsigned int Height = 0;
		unsigned int Depth = 1;
		unsigned int Bpp = 1;
		unsigned int MipLevels = 0;

		ILFormats Format;
		NGTech::Format DDSFormat;

		std::vector<unsigned char> data;

		~ImageData() {
			Clear();
		}

		void Clear() {
			data.clear();
		}
	};

	/**
	Image class
	*/
	struct CORE_API Image
	{
		~Image() {
			if (data) {
				data->Clear();
				delete data;
			}
		}

		static ImageData *Load(const String &path);

		static void GenerateNormalMap(ImageData * data, int k);
		static void ToGreyScale(ImageData * data);

		unsigned char *GetData() { MT::ScopedGuard guard(mutex); return data->data.data(); }

		unsigned int GetWidth() { MT::ScopedGuard guard(mutex); return data->Width; }
		unsigned int GetHeight() { MT::ScopedGuard guard(mutex); return data->Height; }
		unsigned int GetDepth() { MT::ScopedGuard guard(mutex); return data->Depth; }
		unsigned int GetBPP() { MT::ScopedGuard guard(mutex); return data->Bpp; }
		ILFormats	 GetFormat() { MT::ScopedGuard guard(mutex); return data->Format; }

		bool Is3D() { MT::ScopedGuard guard(mutex); return data->Depth > 1; }
	private:
		MT::Mutex  mutex;
		ImageData *data = new ImageData();
	};
}