/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include "coredll.h"
//***************************************************************************
#include "spdlog/spdlog.h"
//***************************************************************************
#include "../../Common/StringHelper.h"
//***************************************************************************

namespace NGTech {
	//---------------------------------------------------------------------------
	//Desc: Log file
	//---------------------------------------------------------------------------
	class CORE_API Log
	{
	public:
		class CORE_API ListenerCallback
		{
		public:
			virtual void PushToLogCallback(const String& text) = 0;
		protected:
			ListenerCallback();
		protected:
			String lastStr;
			String prevLastStr;
			friend Log;
		};
		static std::vector<ListenerCallback*> callbacks;
	public:
		Log(const char*_name = "../Logs/EngineLog", bool _createBackup = true);
		~Log();
		static void WriteInfo(const String& text);
		static void WriteWarning(const String& text);
		static void WriteDebug(const String& text);
		static void ProfilerWrite(const String& text);
		static void ErrorWrite(const String& text);
		static void WriteHeader(const String& text);
		void AddListenerCallback(ListenerCallback*);
	private:
		void _CreateBackupFile() const;
		void _WriteHeader() const;
		void _PutHiddenHeader() const;
		static void _CallbackActions(const String& text);
	private:
		friend ListenerCallback;
		const char* mFileName;
		mutable char m_sBackupFilename[MAX_FILENAME_SIZE];	// can be with path

		std::shared_ptr<logger> console;
		std::shared_ptr<logger> file_logger;
	};
};