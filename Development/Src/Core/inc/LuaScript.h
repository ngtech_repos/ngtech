/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include "Script.h"

struct lua_State;

namespace NGTech
{
	class lua_chunk;
	/*!
	\brief Class for Lua-scripts

	This class based for C-implementation of script, and calls action-functions from LUA
	*/
	class CORE_API LuaScript :public Script
	{
	public:
		static LuaScript* CreateActive(const String &_filename);
		static LuaScript* CreateInActive(const String &_filename);

		static LuaScript* RunGameEntryPoint(const String &_fromFile);

		virtual ~LuaScript();

		virtual STATUS Loaded(void) override;
		virtual STATUS LoadedGUI(void) override;
		/*Updates functions, will called in different sub-systems*/
		virtual STATUS Update(void) override;
		virtual STATUS UpdateInput(void) override;
		virtual STATUS UpdateGUI(void) override;
		virtual STATUS UpdatePhysics(void) override;
		virtual STATUS UpdateRender(void) override;
		virtual STATUS UpdateSceneManager(void) override;
		virtual STATUS UpdateAI(void) override;

		virtual STATUS DestroyEvent(void) override;
		virtual STATUS ReloadEvent(void) override;

		STATUS CallSpecificFunction(const String& _name, const String& _file);
	protected:
		LuaScript(const String &_filename);
	private:
		LuaScript();

		virtual void _AttachScript() override;

		int				_CompileFile(const String& _name);
		void			_CreateChunk(lua_State* L, char* fileContent);
		lua_State*		_GetLuaState() const;
	private:
		std::shared_ptr<lua_chunk>	m_chunk;
	};
}