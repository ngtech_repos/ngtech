/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//**************************************
#include "../../Common/StringHelper.h"
#include "../../Common/IResource.h"
//**************************************
#include "../../Common/BBox.h"
#include "../../Common/BSphere.h"
//**************************************

namespace NGTech {
	struct Vertex {
		Vec3 position;
		Vec2 texcoord;
		Vec3 normal;
		Vec3 tangent;
		Vec3 binormal;
		Vec2 lmTexcoord;
	};

	struct Subset {
		String name;

		Subset() {
			mModelHelper = nullptr;
			vertices = nullptr;
			indices = nullptr;

			name.clear();
			numVertices = 0;
			numIndices = 0;
		}

		~Subset()
		{
			SAFE_RELEASE_ADV(mModelHelper, name.c_str());

			SAFE_DELETE_ARRAY(vertices);
			SAFE_DELETE_ARRAY(indices);
		}

		ENGINE_INLINE void ClearVertex() { if (!vertices) return; memset(vertices, 0, sizeof(Vertex)*numVertices); numVertices = 0; } //-V104
		ENGINE_INLINE void ClearIndices() { if (!indices) return; memset(indices, 0, sizeof(unsigned int)*numIndices); numIndices = 0; } //-V104

		uint32_t numVertices = 0;
		uint32_t numIndices = 0;

		Vertex* vertices = nullptr;// will allocated as Vertex[size]
		unsigned int* indices = nullptr;// will allocated as indices[size]

		I_ModelHelper* mModelHelper = nullptr;

		BBox bBox;
		BSphere bSphere;
	};

	class CORE_API Mesh final :public IResource
	{
	public:
		explicit Mesh(unsigned int _numSubsets);
		explicit Mesh(const String &path, bool _load);
		explicit Mesh(const Mesh &_other) { _Swap(_other); }

		virtual ~Mesh();

		void Save(const String &path);

		void DrawAllSubsets();

		ENGINE_INLINE void DrawSubsetShared(unsigned int s)
		{
			if (this->RefCounter() > 1)
				this->_DrawSubsetInstanced(s);
			else
				this->_DrawSubset(s);
		}

		ENGINE_INLINE unsigned int GetNumSubsets() { return numSubsets; }

		/**
		Desc: Gets UID, UID from 1
		*/
		unsigned int GetSubset(String name);
		const String& GetSubsetName(unsigned int s) const;

		/**
		*/
		ENGINE_INLINE const BBox &GetBBox(unsigned int subset = -1) const
		{
			// ��� ��� ������� �����
			if (subset > 0)
				return subsets[subset]->bBox; //-V108

			return bBox;
		}

		ENGINE_INLINE const BSphere &GetBSphere(unsigned int subset = -1) const
		{
			// ��� ��� ������� �����
			if (subset > 0)
				return subsets[subset]->bSphere; //-V108

			return bSphere;
		}

		ENGINE_INLINE const Vec3& GetMax() const { return GetBBox().maxes; }
		ENGINE_INLINE const Vec3& GetMin() const { return GetBBox().mins; }
		ENGINE_INLINE const Vec3& GetCenter() const { return GetBSphere().center; }
		ENGINE_INLINE float GetRadius() const { return GetBSphere().radius; }

		ENGINE_INLINE Vec3 GetMax(unsigned int s) const { return GetBBox(s).maxes; }

		ENGINE_INLINE const Vec3& GetMin(unsigned int s) const { return GetBBox(s).mins; }
		ENGINE_INLINE const Vec3& GetCenter(unsigned int s) const { return GetBSphere(s).center; }
		ENGINE_INLINE float GetRadius(unsigned int s) const { return GetBSphere(s).radius; }

		ENGINE_INLINE Subset* GetSubset(unsigned int s) {
			ASSERT(this->subsets, "Invalid pointer");
			ASSERT(this->subsets[s], "Invalid index in pointer"); //-V108
			return this->subsets[s]; //-V108
		}

		void ClearVertex();
		void ClearIndices();
		void AddIndex(int _value);
		void AddVertexArray(const void *v, int num_vertex);

		virtual void Load() override;
		virtual bool OnlyLoad() override;

		ENGINE_INLINE virtual String GetResourceType() const override { return "StaticMesh"; }

		ENGINE_INLINE const Mesh& operator = (const Mesh& _other) {
			_Swap(_other);
			return *this;
		}
	private:
		Mesh() = delete;
		/**
		Loads Mesh from file
		*/
		void _Load(const String &path);

		void CalculateBoundings();

		void CalculateTBN();

		virtual void PushToRender() override;

		void _DrawSubset(unsigned int s);
		void _DrawSubsetInstanced(unsigned int _s);

		ENGINE_INLINE void _Swap(const Mesh&_other) {
			if (this != &_other)
			{
				this->numSubsets = _other.numSubsets;
				this->subsets = _other.subsets;
				this->bBox = _other.bBox;
				this->bSphere = _other.bSphere;
				this->lmTexcoords = _other.lmTexcoords;
			}
		}
	private:
		uint32_t numSubsets = 1;
		Subset **subsets = nullptr;
		bool lmTexcoords = false;

		BBox bBox;
		BSphere bSphere;

		friend class RenderPipeline;
		friend class ObjectMesh;
		friend class MeshFormatNGGFStatic;
	};
}