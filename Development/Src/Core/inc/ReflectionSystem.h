/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#ifndef DROP_EDITOR
//***************************************************************************
#include <functional>
//***************************************************************************
#include "coredll.h"
//***************************************************************************
#include "EditableObject.h"
//***************************************************************************

namespace NGTech
{
#if REFLECTION_USE_DEBUG_GLOBAL_CONTAINER
	/*GLOBAL CONTAINER*/
	struct ENGINE_API ReflectionCreatorsContainer
	{
	public:
		typedef std::function<void *()> Creator;
		typedef std::map<String, Creator> Creators;
		Creators _creators;
	};
	ENGINE_API extern ReflectionCreatorsContainer* _g_ReflectionCreatorsContainer;
#endif

	/**/
	template<typename T>
	class ReflectionSystem
	{
	public:
		typedef std::function<T *()> Creator;
		typedef std::map<String, Creator> Creators;
		Creators _creators;
	public:
		virtual ~ReflectionSystem() {}
	public:
		ENGINE_INLINE virtual void Register(const String &className, const Creator &creator) {
			if (_creators.find(className) == _creators.end())// ���� �� ����
			{
				DebugM("Registration: %s", className.c_str()); //-V111
				_creators[className.c_str()] = creator;
#if REFLECTION_USE_DEBUG_GLOBAL_CONTAINER
				_g_ReflectionCreatorsContainer->_creators[className.c_str()] = creator;
#endif
				return;
			}
			Warning("[ReflectionSystem] Already registered for :%s ABORT!! ", className.c_str()); //-V111
		}
#if REFLECTION_USE_DEBUG_GLOBAL_CONTAINER
		ENGINE_INLINE virtual void RegisterGlobal(const String &className, const ReflectionCreatorsContainer::Creator &creator) {
			if (_g_ReflectionCreatorsContainer->_creators.find(className) == _g_ReflectionCreatorsContainer->_creators.end())// ���� �� ����
			{
				_g_ReflectionCreatorsContainer->_creators[className.c_str()] = creator;
				return;
			}
			Warning("[ReflectionSystem] Already registered IN GLOBALS for :%s ABORT!! ", className.c_str());
		}
#endif

		ENGINE_INLINE virtual T *Create(const String &className) {
			Debug(__FUNCTION__);
			if (className.empty()) return nullptr;
			try {
				auto it = _creators.find(className);
				if (it == _creators.end()) // ���� ����� ���������
				{
					Warning("[ReflectionSystem] Requested name:%s not available IN GLOBALS  for creation by name ", className.c_str()); //-V111
					return nullptr;
				}

				auto func = it->second; //std::function<T *()>
				if (!func)
				{
					Warning("[ReflectionSystem] Requested name:%s not available IN GLOBALS for callable ", className.c_str()); //-V111
					return nullptr;
				}
				return func(); // call this function
			}
			catch (const std::bad_function_call& e) {
				Warning("[ReflectionSystem] bad_function_call: %s", e.what()); //-V111
			}
			catch (std::exception&e) {
				Warning("[ReflectionSystem] exception: %s", e.what());
			}
			return nullptr;
		}
#if REFLECTION_USE_DEBUG_GLOBAL_CONTAINER
		ENGINE_INLINE virtual void *CreateFromGlobals(const String &className) {
			Debug(__FUNCTION__);
			if (className.empty()) return nullptr;
			try {
				auto it = _g_ReflectionCreatorsContainer->_creators.find(className);
				if (it == _g_ReflectionCreatorsContainer->_creators.end()) // ���� ����� ���������
				{
					Warning("[ReflectionSystem] Requested name:%s not available IN GLOBALS  for creation by name ", className.c_str());
					return nullptr;
				}

				auto func = it->second; //std::function<T *()>
				if (!func)
				{
					Warning("[ReflectionSystem] Requested name:%s not available IN GLOBALS for callable ", className.c_str());
					return nullptr;
				}
				return func(); // call this function
			}
			catch (const std::bad_function_call& e) {
				Warning("[ReflectionSystem] bad_function_call: %s", e.what());
			}
			catch (std::exception&e) {
				Warning("[ReflectionSystem] exception: %s", e.what());
			}
			return nullptr;
		}
#endif
	};

	/**/
	struct EditableObject;
	/**/
	class ENGINE_API ReflectionSystemForEditableObject :public ReflectionSystem<EditableObject>
	{
	public:
		virtual ~ReflectionSystemForEditableObject() {}
	public:
		ENGINE_INLINE virtual void Register(const String &className, const Creator &creator) override {
			if (_creators.find(className) == _creators.end())// ���� �� ����
			{
				DebugM("Registration: %s", className.c_str());
				_creators[className.c_str()] = creator;
#if REFLECTION_USE_DEBUG_GLOBAL_CONTAINER
				_g_ReflectionCreatorsContainer->_creators[className.c_str()] = creator;
#endif
				return;
			}
			Warning("[ReflectionSystemForEditableObjects] Already registered for :%s ABORT!! ", className.c_str());
		}
		ENGINE_INLINE virtual EditableObject *Create(const String &className) override {
			Debug(__FUNCTION__);
			if (className.empty()) return nullptr;
			try {
				auto it = _creators.find(className);
				if (it == _creators.end()) // ���� ����� ���������
				{
					Warning("[ReflectionSystemForEditableObjects] Requested name:%s not available for creation by name ", className.c_str());
					return nullptr;
				}

				auto func = it->second; //std::function<T *()>
				if (!func)
				{
					Warning("[ReflectionSystemForEditableObjects] Requested name:%s not available for callable ", className.c_str());
					return nullptr;
				}
				return it->second(); // call this function
			}
			catch (const std::bad_function_call& e) {
				Warning("[ReflectionSystemForEditableObjects] bad_function_call: %s", e.what());
			}
			catch (std::exception&e) {
				Warning("[ReflectionSystemForEditableObjects] exception: %s", e.what());
			}
			return nullptr;
		}

#if REFLECTION_USE_DEBUG_GLOBAL_CONTAINER
		ENGINE_INLINE virtual void *CreateFromGlobals(const String &className) override {
			Debug(__FUNCTION__);
			if (className.empty()) return nullptr;
			try {
				auto it = _g_ReflectionCreatorsContainer->_creators.find(className);
				if (it == _g_ReflectionCreatorsContainer->_creators.end()) // ���� ����� ���������
				{
					Warning("[ReflectionSystemForEditableObject] Requested name:%s not available IN GLOBALS  for creation by name ", className.c_str());
					return nullptr;
				}

				auto func = it->second; //std::function<T *()>
				if (!func)
				{
					Warning("[ReflectionSystemForEditableObject] Requested name:%s not available IN GLOBALS for callable ", className.c_str());
					return nullptr;
				}
				return func(); // call this function
			}
			catch (const std::bad_function_call& e) {
				Warning("[ReflectionSystemForEditableObject] bad_function_call: %s", e.what());
			}
			catch (std::exception&e) {
				Warning("[ReflectionSystemForEditableObject] exception: %s", e.what());
			}
			return nullptr;
		}
#endif
	};
}

#endif