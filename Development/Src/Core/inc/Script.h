/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include <string>
#include <memory>
#include "../Common/ScriptInterface.h"

namespace NGTech
{
	/*!
	\brief Main class of scripts

	This class used for C-implementation of script action
	*/
	class CORE_API Script :public ScriptInterface
	{
	public:
		Script(String _name);
		virtual ~Script()
		{
			DestroyEvent();
			_Destroy();
		}
	protected:
		/**/
		ENGINE_INLINE Script() {
			m_active = false;
			m_name.clear();
			callbacks.ResetAllCallbacks();
		}

		/**/
		virtual void _AttachScript();
	};
}