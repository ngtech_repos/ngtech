/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

namespace NGTech
{
	//!@todo REFACTOR THIS AS MESH CLASS
	/**
	Skinned mesh class
	*/
	class CORE_API SkinnedMesh final :public IResource
	{
	public:
		struct Weight
		{
			int bone;
			float weight;
			Vec3 position;
			Vec3 normal;
			Vec3 tangent;
			Vec3 binormal;
		};

		struct Bone
		{
			String name;
			Mat4 transform;
			Mat4 rotation;
			int parent;
		};

		struct Frame
		{
			Vec3 position;
			Quat rotation;
		};

		struct Vertex
		{
			Vec3 position;
			Vec2 texcoord;
			Vec3 normal;
			Vec3 tangent;
			Vec3 binormal;
			int numWeights = 0;
			Weight *weights = nullptr;
		};

		struct Subset
		{
			String name;

			ENGINE_INLINE Subset()
			{
				mModelHelper = nullptr;
				indices = nullptr;
				vertices = nullptr;
				numVertices = 0;
				numIndices = 0;
				visible = true;
				name.clear();
			}

			~Subset()
			{
				//!@todo CHECK THIS, MEMORY LEAK
				// ������ �� �������� ������ ������� �������� ������
				/*SAFE_DELETE_ARRAY(vertices);
				SAFE_DELETE_ARRAY(indices);
				SAFE_RELEASE(mModelHelper);*/

				SAFE_RELEASE_ADV(mModelHelper, name.c_str());
				//!@todo CHECK THIS, MEMORY LEAK
				//SAFE_DELETE_ARRAY(vertices);
				//SAFE_DELETE_ARRAY(indices);
			}

			uint32_t numVertices = 0;
			uint32_t numIndices = 0;

			Vertex* vertices = nullptr;// will allocated as Vertex[size]
			unsigned int* indices = nullptr;// will allocated as indices[size]

			I_ModelHelper* mModelHelper = nullptr;

			BBox bBox;
			BSphere bSphere;

			bool visible;
		};
	public:
		/**
		Creates new SkinnedSkinnedMesh from file
		*/
		SkinnedMesh(const String &path, bool _load);
		SkinnedMesh(const SkinnedMesh&_other);

		/**
		destroys object
		*/
		virtual ~SkinnedMesh();

		/**
		Loads new SkinnedSkinnedMesh from file
		*/
		virtual void Load() override;
		virtual bool OnlyLoad() override;
		virtual void PushToRender() override;

		/**
		draws object subset
		*/
		void DrawSubsetShared(unsigned int s)
		{
			if (this->RefCounter() > 1)
				this->_DrawSubsetInstanced(s);
			else
				this->_DrawSubset(s);
		}

		/**
		gets number of subsets
		*/
		ENGINE_INLINE uint32_t GetNumSubsets() {
			return numSubsets;
		}

		ENGINE_INLINE virtual String GetResourceType() const override { return "AnimatedMesh"; }

		void SetNumSubsets(unsigned int _num) { numSubsets = _num; }

		/**
		get subset number by number
		*/
		unsigned int GetSubset(String name);
		const String& GetSubsetName(unsigned int);

		/**
		*/
		ENGINE_INLINE const BBox &GetBBox(size_t subset = -1)
		{
			// ��� ��� ������� �����
			if (subset > 0)
				return subsets[subset]->bBox;
			return bBox;
		}

		/**
		*/
		ENGINE_INLINE const BSphere &GetBSphere(size_t subset = -1)
		{
			// ��� ��� ������� �����
			if (subset > 0)
				return subsets[subset]->bSphere;
			return bSphere;
		}

		ENGINE_INLINE Subset* GetSubset(unsigned int s) {
			ASSERT(this->subsets, "Invalid pointer");
			ASSERT(this->subsets[s], "Invalid index"); //-V108
			return this->subsets[s]; //-V108
		}

		/**
		must be int
		*/
		void SetFrame(int frame, int from = -1, int to = -1);

		/*
		*/
		void CalculateBoundings();

		/*
		*/
		void CalculateTBN();

		const SkinnedMesh & operator = (const SkinnedMesh & _other) {
			_Swap(_other);
			// �� ���������� ������ ���������� *this
			return *this;
		}

	public:
		BBox bBox;
		BSphere bSphere;
	private:
		/**
		*/
		void _CreateVBO();
		void _DrawSubset(unsigned int subset);
		void _DrawSubsetInstanced(unsigned int subset);

		void _Load(const String &path);
		void _Swap(const SkinnedMesh&_other) {
			if (this != &_other) // ������ �� ������������� ����������������
			{
				bBox = _other.bBox;
				bSphere = _other.bSphere;

				numBones = _other.numBones;

				//!@todo ��� ���� �����������������
				bones = _other.bones;

				numFrames = _other.numFrames;
				frames = _other.frames;

				//!@todo ��� ���� �����������������
				numSubsets = _other.numSubsets;

				visible = _other.visible;

				//!@todo ��� ���� �����������������
				subsets = _other.subsets;
			}
		}
	private:
		unsigned int numBones = 0;
		Bone *bones = nullptr;

		unsigned int numFrames = 0;
		Frame **frames = nullptr;

		uint32_t numSubsets = 1;

		bool visible = true;
		Subset **subsets = nullptr;
	private:
		friend class SkinnedMeshLoader;
		friend class MeshFormatNGGFSkinned;
		friend class SceneNodeSkinnedMesh;
	};
}
