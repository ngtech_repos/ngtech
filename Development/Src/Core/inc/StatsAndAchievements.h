/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#if USE_STEAMWORKS
//**************************************
class ISteamUser;
class ISteamUserStats;
//**************************************
#include "steamworks/steam/steam_api.h"
#include "steamworks/steam/ISteamUserStats.h"
//**************************************

namespace NGTech
{
	/**
	*/
	class StatsAndAchievements
	{
	public:
		StatsAndAchievements();
		/**
		��������� ������������ ������
		*/
		bool SetAchievement(const char* API_NAME);
		/**
		������� � ������������ ������
		*/
		bool DeleteAchievement(const char* API_NAME);
		/**
		������������� ����������
		*/
		void SetStat(const char* _name, int _value);
		/**
		�������� ����������
		*/
		void GetStat(const char*_name, int &_value);
	private:
		// Steam UserStats interface
		ISteamUserStats *m_pSteamUserStats;

		// Did we get the stats from Steam?
		bool m_bRequestedStats;
		bool m_bStatsValid;

		// Should we store stats this frame?
		bool m_bStoreStats;
	};
}
#endif