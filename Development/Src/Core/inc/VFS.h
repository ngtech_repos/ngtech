/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
/*!
	@file
	@author		George Evmenov
	@date		07/2009
	*/

#ifndef __DATA_MANAGER_H__
#define __DATA_MANAGER_H__

#include "../../Common/IDataManager.h"

namespace NGTech
{
	class CORE_API FileSystem :
		public DataManager
	{
	public:
		FileSystem();

		void Initialise();
		void Shutdown();

		/** @see DataManager::getData(const String& _name) */
		virtual IDataStream* GetData(const String& _name) override;

		/** @see DataManager::isDataExist(const String& _name) */
		virtual bool IsDataExist(const String& _name) override;

		/** @see DataManager::getDataListNames(const String& _pattern) */
		virtual const VectorString& GetDataListNames(const String& _pattern) override;

		/** @see DataManager::getDataPath(const String& _name) */
		virtual const String& GetDataPath(const String& _name) override;

		/*internal:*/
		void AddResourceLocation(const String& _name, bool _recursive);

		virtual void FreeData(IDataStream* _data) override;
	private:
		struct ArchiveInfo
		{
			std::wstring name;
			bool recursive;
		};
		typedef std::vector<ArchiveInfo> VectorArchiveInfo;
		VectorArchiveInfo mPaths;

		bool mIsInitialise;
	};

	/*
	Saves binary file - async operation(all's data must be copy)
	*/
	bool CORE_API saveBinary(const char* _file, unsigned int const Format, std::vector<unsigned char> Data, int const Size);
	bool CORE_API saveBinary(const char* _file, std::vector<unsigned char> Data);
	/**/
	bool CORE_API loadBinary(const char* _file, unsigned int & Format, std::vector<unsigned char> & Data, int & Size);
	bool CORE_API loadBinary(const char* _file, std::vector<unsigned char> & Data, int & Size);
	/**/
	bool CORE_API deleteBinary(const char* _file);
}

#endif