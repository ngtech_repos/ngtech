/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include <fstream>

namespace NGTech
{
	enum class FILE_MODE {
		READ_BIN,
		READ_TEXT,
		WRITE_BIN,
		WRITE_TEXT,
	};

	class CORE_API VFile
	{
	public:
		explicit VFile(const String &_name, FILE_MODE _mode = FILE_MODE::READ_BIN, bool _notSearch = false);
		~VFile();

		//Check what file exist,or write in log(if warn=true)
		bool IsDataExist(bool _warn = true);
		bool IsEof();

		/*Allocation memory in memory buffer, and loading*/
		char* LoadFile();

		const char* GetDataPath();

		size_t Size();
		size_t FTell();

		size_t FSeek(long offset, int mode);

		String GetLine();

		//Revert 0 if all is normal
		int Close();
		void Read(void *buf, int size, int count);
		size_t ReadAsSizeT(void *buf, int size, int count);

		void WriteString(const String &text);
		void Write(void *buf, int size, int count);
		void ScanF(const char * format, ...);

		ENGINE_INLINE FILE* GetLowLevelFile() { return mFile; }
		ENGINE_INLINE const char* GetName() { return mName.c_str(); }
		ENGINE_INLINE bool IsValid() { return HasAccess() && IsDataExist() && (mFile != nullptr); }
		bool HasAccess();

		static bool IsDataExist(const char* _name, bool _warn = true);
	private:
		VFile() {}
		void _OpenFile(const String&path, FILE_MODE _mode, bool _notSearch);

		void _FileOpen(const String&path, FILE_MODE _mode);
	private:
		VFile(const VFile&) = delete;
		VFile& operator=(const VFile&) = delete;
	private:
		bool useSearch;

		FILE *mFile;
		std::fstream fileStream;

		std::vector<char>memoryBuffer;
		String mName;
		size_t mSize;
		size_t mCurrentPos;
	};

	class CORE_API VFileStream
	{
	public:
		explicit VFileStream(const String &_name, FILE_MODE _mode = FILE_MODE::READ_BIN, bool _notSearch = false, bool _stillOpened = false);
		~VFileStream();

		//Check what file exist,or write in log(if warn=true)
		bool IsDataExist(bool _warn = true);

		bool IsEof();

		/*Allocation memory in memory buffer, and loading*/
		char* LoadFile();

		const char* GetDataPath();

		size_t Size();
		size_t FTell();

		size_t Seek(long offset);
		/*
		If file not opened -> will opened, but not will closed
		*/
		String GetLine();

		String GetFileExt();
		String CutFileExt();

		//Revert 0 if all is normal
		int Close();
		void Read(void *buf, int size, int count);
		void WriteString(const String &text);
		void Write(void *buf, int size, int count);
		void ScanF(const char * format, ...);

		ENGINE_INLINE const char* GetName() { return mName.c_str(); }
		ENGINE_INLINE bool IsValid() { if (mode < FILE_MODE::WRITE_BIN) return !fileStream.fail(); return !fileOStream.fail(); }

		static bool IsDataExist(const char* _name, bool _warn = true);
	private:
		VFileStream();
		bool _OpenFileStream(const String&path, FILE_MODE _mode, bool _notSearch, bool _stillOpened = false);
		bool _StreamOpen(const String&path, FILE_MODE _mode);
	private:
		VFileStream(const VFileStream&) = delete;
		VFileStream& operator=(const VFileStream& other) = delete;
	private:
		bool useSearch;
		bool stillOpened;

		std::ifstream fileStream;
		std::ofstream fileOStream;
		FILE_MODE mode;

		std::vector<char>memoryBuffer;
		String mName;
		size_t mSize;
		size_t mCurrentPos;
	};
}