/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#ifndef _DDS_H_
#define _DDS_H_

namespace NGTech
{
	struct CORE_API DDS_Image_Info
	{
		unsigned int		Width;
		unsigned int		Height;
		unsigned int		Depth;
		Format				Format;
		unsigned int		MipLevels;
		unsigned int		DataSize;
		void*				Data;
	};

	bool CORE_API LoadFromDDS(const char* file, DDS_Image_Info* outinfo);
	bool CORE_API SaveToDDS(const char* file, const DDS_Image_Info* info);

	namespace ddsShared
	{
		unsigned int CORE_API GetImageSize(unsigned int width, unsigned int height, unsigned int bytes, unsigned int miplevels);
		unsigned int CORE_API GetCompressedImageSize(unsigned int width, unsigned int height, unsigned int miplevels, const Format& format);
		unsigned int CORE_API GetCompressedImageSize(unsigned int width, unsigned int height, unsigned int depth, unsigned int miplevels, const Format& format);
		unsigned int CORE_API GetCompressedLevelSize(unsigned int width, unsigned int height, unsigned int level, const Format& format);
		unsigned int CORE_API NextPow2(unsigned int x);
	}
}

#endif