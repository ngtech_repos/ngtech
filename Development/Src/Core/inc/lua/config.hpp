/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
// Copyright (c) 2003 Daniel Wallin and Arvid Norberg

// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
// ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
// PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
// SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
// ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
// OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef LUABIND_CONFIG_HPP_INCLUDED
#define LUABIND_CONFIG_HPP_INCLUDED

#include "../../Platform/inc/platformdetect.h"

// the maximum number of arguments of functions that's
// registered. Must at least be 2
#ifndef LUABIND_MAX_ARITY
#define LUABIND_MAX_ARITY 100
#elif LUABIND_MAX_ARITY <= 1
#undef LUABIND_MAX_ARITY
#define LUABIND_MAX_ARITY 2
#endif

// the maximum number of classes one class
// can derive from
// max bases must at least be 1
#ifndef LUABIND_MAX_BASES
#define LUABIND_MAX_BASES 100
#elif LUABIND_MAX_BASES <= 0
#undef LUABIND_MAX_BASES
#define LUABIND_MAX_BASES 1
#endif

#ifndef LUABIND_DYNAMIC_LINK
#define LUABIND_DYNAMIC_LINK
#endif

#ifdef LUABIND_DYNAMIC_LINK
# if defined (WIN32) &&(!defined NGTECH_STATIC_LIBS)
#  ifdef CORE_EXPORTS
#   define LUABIND_API CROSSPLATFORM_EXPORT_FROM_DLL
#  else
#   define LUABIND_API CROSSPLATFORM_IMPORT_FROM_DLL
#  endif
# elif defined (__CYGWIN__)
#  ifdef LUABIND_BUILDING
#   define LUABIND_API __attribute__ ((dllexport))
#  else
#   define LUABIND_API __attribute__ ((dllimport))
#  endif
# else
#  if defined(_GNUC_) && _GNUC_ >=4
#   define LUABIND_API __attribute__ ((visibility("default")))
#  endif
# endif
#endif

#ifndef LUABIND_API
# define LUABIND_API
#endif

// This switches between using tag arguments / structure specialization for code size tests
#define LUABIND_NO_INTERNAL_TAG_ARGUMENTS

namespace luabind {
	LUABIND_API void disable_super_deprecation();

	namespace detail {
		const int max_argument_count = 100;
		const int max_hierarchy_depth = 100;
	}

	const int no_match = -(detail::max_argument_count*detail::max_hierarchy_depth + 1);
} // namespace luabind

#endif // LUABIND_CONFIG_HPP_INCLUDED