/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
// Copyright Daniel Wallin 2008. Use, modification and distribution is
// subject to the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef LUABIND_DETAIL_CONSTRUCTOR_081018_HPP
#define LUABIND_DETAIL_CONSTRUCTOR_081018_HPP

#include "../get_main_thread.hpp"
#include "../lua_argument_proxy.hpp"
#include "../wrapper_base.hpp"
#include "inheritance.hpp"

namespace luabind {
	namespace detail {
		inline void inject_backref(lua_State*, void*, void*)
		{}

		template <class T>
		void inject_backref(lua_State* L, T* p, wrap_base*)
		{
			weak_ref(get_main_thread(L), L, 1).swap(wrap_access::ref(*p));
		}

		template< class T, class Pointer, class Signature, class Arguments, class ArgumentIndices >
		struct construct_aux_helper;

		template< class T, class Pointer, class Signature, typename... Arguments, unsigned int... ArgumentIndices >
		struct construct_aux_helper< T, Pointer, Signature, meta::type_list< Arguments... >, meta::index_list< ArgumentIndices... > >
		{
			typedef pointer_holder<Pointer, T> holder_type;
			typedef meta::type_list< Arguments... > ArgumentList;

			void operator()(argument const& self_, Arguments... args) const
			{
				object_rep* self = touserdata<object_rep>(self_);

				std::unique_ptr<T> instance(new T(args...));
				inject_backref(self_.interpreter(), instance.get(), instance.get());

				void* naked_ptr = instance.get();
				Pointer ptr(instance.release());

				void* storage = self->allocate(sizeof(holder_type));

				self->set_instance(new (storage) holder_type(std::move(ptr), registered_class<T>::id, naked_ptr));
			}
		};

		template< class T, class Pointer, class Signature >
		struct construct :
			public construct_aux_helper <
			T,
			Pointer,
			Signature, typename meta::sub_range< Signature, 2, meta::size<Signature>::value >::type,
			typename meta::make_index_range<0, meta::size<Signature>::value - 2>::type >
		{
		};
	}	// namespace detail
}	// namespace luabind

# endif // LUABIND_DETAIL_CONSTRUCTOR_081018_HPP