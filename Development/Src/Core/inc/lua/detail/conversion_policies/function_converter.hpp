/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
// Copyright Christian Neum�ller 2013. Use, modification and distribution is
// subject to the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef LUABIND_FUNCTION_CONVERTER_HPP_INCLUDED
#define LUABIND_FUNCTION_CONVERTER_HPP_INCLUDED
#include <functional>
#include "../deduce_signature.hpp"
#include "../conversion_policies/conversion_base.hpp"
#include "../../make_function.hpp"
#include "../call_function.hpp"

namespace luabind {
	template <typename R = object>
	struct function
	{
		typedef R result_type;

		function(luabind::object const& obj)
			: m_func(obj)
		{
		}

		template< typename... Args>
		R operator() (Args&&... args)
		{
			return call_function<R>(m_func, std::forward<Args>(args)...);
		}

		bool is_valid() const { return m_func.is_valid(); }

	private:
		object m_func;
	};

	namespace detail {
		template< typename T >
		struct is_function : public std::false_type {};

		template< typename T >
		struct is_function< std::function< T > > : public std::true_type {};

		template< typename R, typename... Args, typename WrappedType >
		struct call_types <std::function< R(Args...) >, WrappedType >
		{
			typedef meta::type_list< R, Args... > signature_type;
		};
	}

	template <typename F>
	struct default_converter<F, typename std::enable_if<detail::is_function<F>::value>::type>
	{
		typedef std::true_type is_native;

		enum { consumed_args = 1 };

		template <class U>
		void converter_postcall(lua_State*, U const&, int)
		{}

		template <class U>
		static int match(lua_State* L, U, int index)
		{
			if (lua_type(L, index) == LUA_TFUNCTION)
				return 0;
			if (luaL_getmetafield(L, index, "__call")) {
				lua_pop(L, 1);
				return 1;
			}
			return no_match;
		}

		template <class U>
		F to_cpp(lua_State* L, U, int index)
		{
			// If you get a compiler error here, you are probably trying to
			// get a function pointer from Lua. This is not supported:
			// you must use a type which is constructible from a
			// luabind::function, e.g. std::function or boost::function.
			return function<typename F::result_type>(object(from_stack(L, index)));
		}

		void to_lua(lua_State* L, F value)
		{
			make_function(L, value).push(L);
		}
	};
} // namespace luabind

#endif // LUABIND_FUNCTION_CONVERTER_HPP_INCLUDED