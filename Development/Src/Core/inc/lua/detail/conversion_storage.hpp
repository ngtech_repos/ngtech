/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
// Copyright Daniel Wallin 2008. Use, modification and distribution is
// subject to the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef LUABIND_CONVERSION_STORAGE_080930_HPP
# define LUABIND_CONVERSION_STORAGE_080930_HPP

# include "../config.hpp"
# include <type_traits>

namespace luabind {
	namespace detail {
		typedef void(*destruction_function)(void*);

		// This is used by the converters in policy.hpp, and
		// class_rep::convert_to as temporary storage when constructing
		// holders.

		struct conversion_storage
		{
			conversion_storage()
				: destructor(0)
			{}

			~conversion_storage()
			{
				if (destructor)
					destructor(&data);
			}

			// Unfortunately the converters currently doesn't have access to
			// the actual type being converted when this is instantiated, so
			// we have to guess a max size.
			std::aligned_storage<128> data;
			destruction_function destructor;
		};
	}
} // namespace luabind::detail

#endif // LUABIND_CONVERSION_STORAGE_080930_HPP