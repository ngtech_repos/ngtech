/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
// Copyright Daniel Wallin 2008. Use, modification and distribution is
// subject to the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef LUABIND_PROPERTY_081020_HPP
# define LUABIND_PROPERTY_081020_HPP

namespace luabind {
	namespace detail {
		template <class Class, class T, class Result = T>
		struct access_member_ptr
		{
			access_member_ptr(T Class::* mem_ptr)
				: mem_ptr(mem_ptr)
			{}

			Result operator()(Class const& x) const
			{
				return const_cast<Class&>(x).*mem_ptr;
			}

			void operator()(Class& x, T const& value) const
			{
				x.*mem_ptr = value;
			}

			T Class::* mem_ptr;
		};
	}
} // namespace luabind::detail

#endif // LUABIND_PROPERTY_081020_HPP