/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
// Copyright Daniel Wallin 2005. Use, modification and distribution is
// subject to the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef LUABIND_EXCEPTION_HANDLER_050601_HPP
#define LUABIND_EXCEPTION_HANDLER_050601_HPP

#include <lua/config.hpp>           // for LUABIND_API
#include <type_traits>
#include <lua/lua_include.hpp>
#include <lua/detail/meta.hpp>

namespace luabind {
# ifndef LUABIND_NO_EXCEPTIONS

	namespace detail
	{
		struct LUABIND_API exception_handler_base
		{
			exception_handler_base()
				: next(0)
			{}

			virtual ~exception_handler_base() {}
			virtual void handle(lua_State*) const = 0;

			void try_next(lua_State*) const;

			exception_handler_base* next;
		};

		template<class E, class Handler>
		struct exception_handler : exception_handler_base
		{
			typedef E const& argument;

			exception_handler(Handler handler)
				: handler(handler)
			{}

			void handle(lua_State* L) const
			{
				try
				{
					try_next(L);
				}
				catch (argument e)
				{
					handler(L, e);
				}
			}

			Handler handler;
		};

		LUABIND_API void handle_exception_aux(lua_State* L);
		LUABIND_API void register_exception_handler(exception_handler_base*);
	} // namespace detail

# endif

	template<class E, class Handler>
	void register_exception_handler(Handler handler, meta::type<E>* = 0)
	{
# ifndef LUABIND_NO_EXCEPTIONS
		detail::register_exception_handler(
			new detail::exception_handler<E, Handler>(handler)
		);
# endif
	}
} // namespace luabind

#endif // LUABIND_EXCEPTION_HANDLER_050601_HPP