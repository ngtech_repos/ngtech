/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
// Copyright Daniel Wallin 2008. Use, modification and distribution is
// subject to the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef LUABIND_FUNCTION2_081014_HPP
# define LUABIND_FUNCTION2_081014_HPP

# include "make_function.hpp"
# include "scope.hpp"
# include "detail/call_function.hpp"

namespace luabind {
	namespace detail
	{
		template <class F, class PolicyInjectors>
		struct function_registration : registration
		{
			function_registration(char const* name, F f)
				: name(name)
				, f(f)
			{}

			void register_(lua_State* L) const
			{
				object fn = make_function(L, f, PolicyInjectors());
				add_overload(object(from_stack(L, -1)), name, fn);
			}

			char const* name;
			F f;
		};

		LUABIND_API bool is_luabind_function(lua_State* L, int index);
	} // namespace detail

	template <class F, typename... PolicyInjectors>
	scope def(char const* name, F f, policy_list<PolicyInjectors...> const&)
	{
		return scope(std::unique_ptr<detail::registration>(
			new detail::function_registration<F, policy_list<PolicyInjectors...>>(name, f)));
	}

	template <class F>
	scope def(char const* name, F f)
	{
		return def(name, f, no_policies());
	}
} // namespace luabind

#endif // LUABIND_FUNCTION2_081014_HPP