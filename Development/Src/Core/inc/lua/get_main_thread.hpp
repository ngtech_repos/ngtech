/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
// Copyright Daniel Wallin 2009. Use, modification and distribution is
// subject to the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef LUABIND_GET_MAIN_THREAD_090321_HPP
# define LUABIND_GET_MAIN_THREAD_090321_HPP

# include "config.hpp"
# include "lua_state_fwd.hpp"

namespace luabind {
	LUABIND_API lua_State* get_main_thread(lua_State* L);
} // namespace luabind

#endif // LUABIND_GET_MAIN_THREAD_090321_HPP