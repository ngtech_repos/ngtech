/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
/*
** $Id: ldebug.h,v 2.3.1.1 2007/12/27 13:02:25 roberto Exp $
** Auxiliary functions from Debug Interface module
** See Copyright Notice in lua.h
*/

#ifndef ldebug_h
#define ldebug_h

#include "lstate.h"

#define pcRel(pc, p)	(cast(int, (pc) - (p)->code) - 1)

#define getline(f,pc)	(((f)->lineinfo) ? (f)->lineinfo[pc] : 0)

#define resethookcount(L)	(L->hookcount = L->basehookcount)

LUAI_FUNC void luaG_typeerror(lua_State *L, const TValue *o,
	const char *opname);
LUAI_FUNC void luaG_concaterror(lua_State *L, StkId p1, StkId p2);
LUAI_FUNC void luaG_aritherror(lua_State *L, const TValue *p1,
	const TValue *p2);
LUAI_FUNC int luaG_ordererror(lua_State *L, const TValue *p1,
	const TValue *p2);
LUAI_FUNC void luaG_runerror(lua_State *L, const char *fmt, ...);
LUAI_FUNC void luaG_errormsg(lua_State *L);
LUAI_FUNC int luaG_checkcode(const Proto *pt);
LUAI_FUNC int luaG_checkopenop(Instruction i);

#endif
