/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
/*
** $Id: ldo.h,v 2.7.1.1 2007/12/27 13:02:25 roberto Exp $
** Stack and Call structure of Lua
** See Copyright Notice in lua.h
*/

#ifndef ldo_h
#define ldo_h

#include "lobject.h"
#include "lstate.h"
#include "lzio.h"

#define luaD_checkstack(L,n)	\
  if ((char *)L->stack_last - (char *)L->top <= (n)*(int)sizeof(TValue)) \
    luaD_growstack(L, n); \
    else condhardstacktests(luaD_reallocstack(L, L->stacksize - EXTRA_STACK - 1));

#define incr_top(L) {luaD_checkstack(L,1); L->top++;}

#define savestack(L,p)		((char *)(p) - (char *)L->stack)
#define restorestack(L,n)	((TValue *)((char *)L->stack + (n)))

#define saveci(L,p)		((char *)(p) - (char *)L->base_ci)
#define restoreci(L,n)		((CallInfo *)((char *)L->base_ci + (n)))

/* results from luaD_precall */
#define PCRLUA		0	/* initiated a call to a Lua function */
#define PCRC		1	/* did a call to a C function */
#define PCRYIELD	2	/* C funtion yielded */

/* type of protected functions, to be ran by `runprotected' */
typedef void(*Pfunc) (lua_State *L, void *ud);

LUAI_FUNC int luaD_protectedparser(lua_State *L, ZIO *z, const char *name);
LUAI_FUNC void luaD_callhook(lua_State *L, int event, int line);
LUAI_FUNC int luaD_precall(lua_State *L, StkId func, int nresults);
LUAI_FUNC void luaD_call(lua_State *L, StkId func, int nResults);
LUAI_FUNC int luaD_pcall(lua_State *L, Pfunc func, void *u,
	intptr_t oldtop, intptr_t ef);
LUAI_FUNC int luaD_poscall(lua_State *L, StkId firstResult);
LUAI_FUNC void luaD_reallocCI(lua_State *L, int newsize);
LUAI_FUNC void luaD_reallocstack(lua_State *L, int newsize);
LUAI_FUNC void luaD_growstack(lua_State *L, int n);

LUAI_FUNC void luaD_throw(lua_State *L, int errcode);
LUAI_FUNC int luaD_rawrunprotected(lua_State *L, Pfunc f, void *ud);

LUAI_FUNC void luaD_seterrorobj(lua_State *L, int errcode, StkId oldtop);

#endif
