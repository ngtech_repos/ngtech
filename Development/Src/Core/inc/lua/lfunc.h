/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
/*
** $Id: lfunc.h,v 2.4.1.1 2007/12/27 13:02:25 roberto Exp $
** Auxiliary functions to manipulate prototypes and closures
** See Copyright Notice in lua.h
*/

#ifndef lfunc_h
#define lfunc_h

#include "lobject.h"

#define sizeCclosure(n)	(cast(int, sizeof(CClosure)) + \
                         cast(int, sizeof(TValue)*((n)-1)))

#define sizeLclosure(n)	(cast(int, sizeof(LClosure)) + \
                         cast(int, sizeof(TValue *)*((n)-1)))

LUAI_FUNC Proto *luaF_newproto(lua_State *L);
LUAI_FUNC Closure *luaF_newCclosure(lua_State *L, int nelems, Table *e);
LUAI_FUNC Closure *luaF_newLclosure(lua_State *L, int nelems, Table *e);
LUAI_FUNC UpVal *luaF_newupval(lua_State *L);
LUAI_FUNC UpVal *luaF_findupval(lua_State *L, StkId level);
LUAI_FUNC void luaF_close(lua_State *L, StkId level);
LUAI_FUNC void luaF_freeproto(lua_State *L, Proto *f);
LUAI_FUNC void luaF_freeclosure(lua_State *L, Closure *c);
LUAI_FUNC void luaF_freeupval(lua_State *L, UpVal *uv);
LUAI_FUNC const char *luaF_getlocalname(const Proto *func, int local_number,
	int pc);

#endif
