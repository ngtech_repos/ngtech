/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
/*
** $Id: ltable.h,v 2.10.1.1 2007/12/27 13:02:25 roberto Exp $
** Lua tables (hash)
** See Copyright Notice in lua.h
*/

#ifndef ltable_h
#define ltable_h

#include "lobject.h"

#define gnode(t,i)	(&(t)->node[i])
#define gkey(n)		(&(n)->i_key.nk)
#define gval(n)		(&(n)->i_val)
#define gnext(n)	((n)->i_key.nk.next)

#define key2tval(n)	(&(n)->i_key.tvk)

LUAI_FUNC const TValue *luaH_getnum(Table *t, int key);
LUAI_FUNC TValue *luaH_setnum(lua_State *L, Table *t, int key);
LUAI_FUNC const TValue *luaH_getstr(Table *t, TString *key);
LUAI_FUNC TValue *luaH_setstr(lua_State *L, Table *t, TString *key);
LUAI_FUNC const TValue *luaH_get(Table *t, const TValue *key);
LUAI_FUNC TValue *luaH_set(lua_State *L, Table *t, const TValue *key);
LUAI_FUNC Table *luaH_new(lua_State *L, int narray, int lnhash);
LUAI_FUNC void luaH_resizearray(lua_State *L, Table *t, int nasize);
LUAI_FUNC void luaH_free(lua_State *L, Table *t);
LUAI_FUNC int luaH_next(lua_State *L, Table *t, StkId key);
LUAI_FUNC int luaH_getn(Table *t);

#if defined(LUA_DEBUG)
LUAI_FUNC Node *luaH_mainposition(const Table *t, const TValue *key);
LUAI_FUNC int luaH_isdummy(Node *n);
#endif

#endif
