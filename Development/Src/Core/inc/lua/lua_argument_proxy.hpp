/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#ifndef LUA_ARGUMENT_PROXY_HPP_INCLUDED
#define LUA_ARGUMENT_PROXY_HPP_INCLUDED

#include "lua_include.hpp"
#include "lua_proxy_interface.hpp"
#include "lua_index_proxy.hpp"
#include "from_stack.hpp"

namespace luabind {
	namespace adl {
		class argument : public lua_proxy_interface<argument>
		{
		public:
			argument(from_stack const& stack_reference)
				: m_interpreter(stack_reference.interpreter), m_index(stack_reference.index)
			{
				if (m_index < 0) m_index = lua_gettop(m_interpreter) - m_index + 1;
			}

			template<class T>
			index_proxy<argument> operator[](T const& key) const
			{
				return index_proxy<argument>(*this, m_interpreter, key);
			}

			void push(lua_State* L) const
			{
				lua_pushvalue(L, m_index);
			}

			lua_State* interpreter() const
			{
				return m_interpreter;
			}

		private:
			lua_State* m_interpreter;
			int m_index;
		};
	}

	using adl::argument;

	template<>
	struct lua_proxy_traits<argument>
	{
		typedef std::true_type is_specialized;

		static lua_State* interpreter(argument const& value)
		{
			return value.interpreter();
		}

		static void unwrap(lua_State* interpreter, argument const& value)
		{
			value.push(interpreter);
		}

		static bool check(...)
		{
			return true;
		}
	};
}

#endif