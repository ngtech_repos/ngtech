/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
/*
** $Id: lualib.h,v 1.36.1.1 2007/12/27 13:02:25 roberto Exp $
** Lua standard libraries
** See Copyright Notice in lua.h
*/

#ifndef lualib_h
#define lualib_h

#include "lua.h"

/* Key to file-handle type */
#define LUA_FILEHANDLE		"FILE*"

#define LUA_COLIBNAME	"coroutine"
LUALIB_API int (luaopen_base)(lua_State *L);

#define LUA_TABLIBNAME	"table"
LUALIB_API int (luaopen_table)(lua_State *L);

#define LUA_IOLIBNAME	"io"
LUALIB_API int (luaopen_io)(lua_State *L);

#define LUA_OSLIBNAME	"os"
LUALIB_API int (luaopen_os)(lua_State *L);

#define LUA_STRLIBNAME	"string"
LUALIB_API int (luaopen_string)(lua_State *L);

#define LUA_MATHLIBNAME	"math"
LUALIB_API int (luaopen_math)(lua_State *L);

#define LUA_DBLIBNAME	"debug"
LUALIB_API int (luaopen_debug)(lua_State *L);

#define LUA_LOADLIBNAME	"package"
LUALIB_API int (luaopen_package)(lua_State *L);

/* open all previous libraries */
LUALIB_API void (luaL_openlibs)(lua_State *L);

#ifndef lua_assert
#define lua_assert(x)	((void)0)
#endif

#endif
