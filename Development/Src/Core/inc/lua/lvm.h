/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
/*
** $Id: lvm.h,v 2.5.1.1 2007/12/27 13:02:25 roberto Exp $
** Lua virtual machine
** See Copyright Notice in lua.h
*/

#ifndef lvm_h
#define lvm_h

#include "ldo.h"
#include "lobject.h"
#include "ltm.h"

#define tostring(L,o) ((ttype(o) == LUA_TSTRING) || (luaV_tostring(L, o)))

#define tonumber(o,n)	(ttype(o) == LUA_TNUMBER || \
                         (((o) = luaV_tonumber(o,n)) != NULL))

#define equalobj(L,o1,o2) \
	(ttype(o1) == ttype(o2) && luaV_equalval(L, o1, o2))

LUAI_FUNC int luaV_lessthan(lua_State *L, const TValue *l, const TValue *r);
LUAI_FUNC int luaV_equalval(lua_State *L, const TValue *t1, const TValue *t2);
LUAI_FUNC const TValue *luaV_tonumber(const TValue *obj, TValue *n);
LUAI_FUNC int luaV_tostring(lua_State *L, StkId obj);
LUAI_FUNC void luaV_gettable(lua_State *L, const TValue *t, TValue *key,
	StkId val);
LUAI_FUNC void luaV_settable(lua_State *L, const TValue *t, TValue *key,
	StkId val);
LUAI_FUNC void luaV_execute(lua_State *L, int nexeccalls);
LUAI_FUNC void luaV_concat(lua_State *L, int total, int last);

#endif
