/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
// Copyright Daniel Wallin 2010. Use, modification and distribution is
// subject to the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

#ifndef LUABIND_NO_DEPENDENCY_100324_HPP
# define LUABIND_NO_DEPENDENCY_100324_HPP

# include "detail/policy.hpp"

namespace luabind {
	namespace detail
	{
		struct no_dependency_policy
		{
			static void postcall(lua_State*, int results, meta::index_list_tag)
			{}
		};
	} // namespace detail

	using no_dependency = policy_list<call_policy_injector<detail::no_dependency_policy>>;
} // namespace luabind

#endif // LUABIND_NO_DEPENDENCY_100324_HPP