/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include <string>

#include "lua/lua.hpp"
#include "lua/luabind.hpp"
#include "lua/detail/policy.hpp"
#include "lua/out_value_policy.hpp"
#include "lua/operator.hpp"

struct lua_State;

namespace NGTech
{
	class lua_chunk
	{
	public:
		lua_chunk(lua_State* L, const String& script);

		lua_chunk(lua_State* L);

		bool Compile(const String& script);
		int CallSpecificFunction(const String& _name, const String&_File);

		ENGINE_INLINE const String& Get_Errors() const { return errors; }

		luabind::object Run(luabind::object env);

		luabind::object New_Instance();

		String errors;
		bool is_compiled = false;

	private:
		luabind::object chunk;
		int ENV_index = -1;
		std::shared_ptr<lua_State> m_LuaState;
	};
}