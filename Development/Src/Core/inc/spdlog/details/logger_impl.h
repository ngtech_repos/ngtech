/*************************************************************************/
/* spdlog - an extremely fast and easy to use c++11 logging library.     */
/* Copyright (c) 2014 Gabi Melman.                                       */
/*                                                                       */
/* Permission is hereby granted, free of charge, to any person obtaining */
/* a copy of this software and associated documentation files (the       */
/* "Software"), to deal in the Software without restriction, including   */
/* without limitation the rights to use, copy, modify, merge, publish,   */
/* distribute, sublicense, and/or sell copies of the Software, and to    */
/* permit persons to whom the Software is furnished to do so, subject to */
/* the following conditions:                                             */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/

#pragma once
//
// Logger implementation
//

#include "./line_logger.h"

// create logger with given name, sinks and the default pattern formatter
// all other ctors will call this one
template<class It>
inline NGTech::logger::logger(const String& logger_name, const It& begin, const It& end) :
	_name(logger_name),
	_sinks(begin, end),
	_formatter(std::make_shared<pattern_formatter>("%+"))
{
	// no support under vs2013 for member initialization for std::atomic
	_level = level::info;
}

// ctor with sinks as init list
inline NGTech::logger::logger(const String& logger_name, sinks_init_list sinks_list) :
	logger(logger_name, sinks_list.begin(), sinks_list.end()) {}

// ctor with single sink
inline NGTech::logger::logger(const String& logger_name, NGTech::sink_ptr single_sink) :
	logger(logger_name, {
		single_sink
		}) {}

inline NGTech::logger::~logger() = default;

inline void NGTech::logger::set_formatter(NGTech::formatter_ptr msg_formatter)
{
	_set_formatter(msg_formatter);
}

inline void NGTech::logger::set_pattern(const String& pattern)
{
	_set_pattern(pattern);
}

//
// log only if given level>=logger's log level
//

template <typename... Args>
inline NGTech::details::line_logger NGTech::logger::_log_if_enabled(level::level_enum lvl, const char* fmt, const Args&... args)
{
	bool msg_enabled = should_log(lvl);
	details::line_logger l(this, lvl, msg_enabled);
	l.write(fmt, args...);
	return l;
}

inline NGTech::details::line_logger NGTech::logger::_log_if_enabled(level::level_enum lvl)
{
	return details::line_logger(this, lvl, should_log(lvl));
}

template<typename T>
inline NGTech::details::line_logger NGTech::logger::_log_if_enabled(level::level_enum lvl, const T& msg)
{
	bool msg_enabled = should_log(lvl);
	details::line_logger l(this, lvl, msg_enabled);
	l << msg;
	return l;
}

//
// logger.info(cppformat_string, arg1, arg2, arg3, ...) call style
//
template <typename... Args>
inline NGTech::details::line_logger NGTech::logger::trace(const char* fmt, const Args&... args)
{
	return _log_if_enabled(level::trace, fmt, args...);
}

template <typename... Args>
inline NGTech::details::line_logger NGTech::logger::debug(const char* fmt, const Args&... args)
{
	return _log_if_enabled(level::debug, fmt, args...);
}

template <typename... Args>
inline NGTech::details::line_logger NGTech::logger::info(const char* fmt, const Args&... args)
{
	return _log_if_enabled(level::info, fmt, args...);
}

template <typename... Args>
inline NGTech::details::line_logger NGTech::logger::notice(const char* fmt, const Args&... args)
{
	return _log_if_enabled(level::notice, fmt, args...);
}

template <typename... Args>
inline NGTech::details::line_logger NGTech::logger::warn(const char* fmt, const Args&... args)
{
	return _log_if_enabled(level::warn, fmt, args...);
}

template <typename... Args>
inline NGTech::details::line_logger NGTech::logger::error(const char* fmt, const Args&... args)
{
	return _log_if_enabled(level::err, fmt, args...);
}

template <typename... Args>
inline NGTech::details::line_logger NGTech::logger::critical(const char* fmt, const Args&... args)
{
	return _log_if_enabled(level::critical, fmt, args...);
}

template <typename... Args>
inline NGTech::details::line_logger NGTech::logger::alert(const char* fmt, const Args&... args)
{
	return _log_if_enabled(level::alert, fmt, args...);
}

template <typename... Args>
inline NGTech::details::line_logger NGTech::logger::emerg(const char* fmt, const Args&... args)
{
	return _log_if_enabled(level::emerg, fmt, args...);
}

//
// logger.info(msg) << ".." call style
//
template<typename T>
inline NGTech::details::line_logger NGTech::logger::trace(const T& msg)
{
	return _log_if_enabled(level::trace, msg);
}

template<typename T>
inline NGTech::details::line_logger NGTech::logger::debug(const T& msg)
{
	return _log_if_enabled(level::debug, msg);
}

template<typename T>
inline NGTech::details::line_logger NGTech::logger::info(const T& msg)
{
	return _log_if_enabled(level::info, msg);
}

template<typename T>
inline NGTech::details::line_logger NGTech::logger::notice(const T& msg)
{
	return _log_if_enabled(level::notice, msg);
}

template<typename T>
inline NGTech::details::line_logger NGTech::logger::warn(const T& msg)
{
	return _log_if_enabled(level::warn, msg);
}

template<typename T>
inline NGTech::details::line_logger NGTech::logger::error(const T& msg)
{
	return _log_if_enabled(level::err, msg);
}

template<typename T>
inline NGTech::details::line_logger NGTech::logger::critical(const T& msg)
{
	return _log_if_enabled(level::critical, msg);
}

template<typename T>
inline NGTech::details::line_logger NGTech::logger::alert(const T& msg)
{
	return _log_if_enabled(level::alert, msg);
}

template<typename T>
inline NGTech::details::line_logger NGTech::logger::emerg(const T& msg)
{
	return _log_if_enabled(level::emerg, msg);
}

//
// logger.info() << ".." call  style
//
inline NGTech::details::line_logger NGTech::logger::trace()
{
	return _log_if_enabled(level::trace);
}

inline NGTech::details::line_logger NGTech::logger::debug()
{
	return _log_if_enabled(level::debug);
}

inline NGTech::details::line_logger NGTech::logger::info()
{
	return _log_if_enabled(level::info);
}

inline NGTech::details::line_logger NGTech::logger::notice()
{
	return _log_if_enabled(level::notice);
}

inline NGTech::details::line_logger NGTech::logger::warn()
{
	return _log_if_enabled(level::warn);
}

inline NGTech::details::line_logger NGTech::logger::error()
{
	return _log_if_enabled(level::err);
}

inline NGTech::details::line_logger NGTech::logger::critical()
{
	return _log_if_enabled(level::critical);
}

inline NGTech::details::line_logger NGTech::logger::alert()
{
	return _log_if_enabled(level::alert);
}

inline NGTech::details::line_logger NGTech::logger::emerg()
{
	return _log_if_enabled(level::emerg);
}

// always log, no matter what is the actual logger's log level
template <typename... Args>
inline NGTech::details::line_logger NGTech::logger::force_log(level::level_enum lvl, const char* fmt, const Args&... args)
{
	details::line_logger l(this, lvl, true);
	l.write(fmt, args...);
	return l;
}

//
// name and level
//
inline const String& NGTech::logger::name() const
{
	return _name;
}

inline void NGTech::logger::set_level(NGTech::level::level_enum log_level)
{
	_level.store(log_level);
}

inline NGTech::level::level_enum NGTech::logger::level() const
{
	return static_cast<NGTech::level::level_enum>(_level.load(std::memory_order_relaxed));
}

inline bool NGTech::logger::should_log(NGTech::level::level_enum msg_level) const
{
	return msg_level >= _level.load(std::memory_order_relaxed);
}

//
// protected virtual called at end of each user log call (if enabled) by the line_logger
//
inline void NGTech::logger::_log_msg(details::log_msg& msg)
{
	_formatter->format(msg);
	for (auto &sink : _sinks)
		sink->log(msg);
}

inline void NGTech::logger::_set_pattern(const String& pattern)
{
	_formatter = std::make_shared<pattern_formatter>(pattern);
}
inline void NGTech::logger::_set_formatter(formatter_ptr msg_formatter)
{
	_formatter = msg_formatter;
}

inline void NGTech::logger::flush() {
	for (auto& sink : _sinks)
		sink->flush();
}