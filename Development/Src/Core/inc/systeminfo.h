/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

namespace NGTech {
	/*
	 */
	class CORE_API SystemInfo {
	public:
		SystemInfo() { _Init(); }
		~SystemInfo() { _Shutdown(); }

		// binary information string
		static const char* GetBinaryInfo();

		// operating system information string
		static const char* GetSystemInfo();

		// system memory in Mbytes
		static int GetSystemMemory();

		// CPU information string
		static const char* GetCPUInfo();

		// CPU frequency in MHz
		static int GetCPUFrequency();

		// CPU counter
		static int GetCPUCount();

		// CPU extensions
		static int HasMMX();
		static int HasSSE();
		static int HasSSE2();
		static int HasSSE3();
		static int HasSSE4();
		static int HasSSE5();
		static int HasAVX();
		static int Has3DNow();
		static int HasAltiVec();

		// GPU information string
		static const char* GetGPUInfo();

		// GPU memory in Mbytes
		static int GetGPUMemory();

		// GPU counter
		static int GetGPUCount();
	private:
		// initialize system info
		static int _Init();
		static void _Shutdown();
	};
}