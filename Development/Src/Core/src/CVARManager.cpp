/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "CorePrivate.h"
//***************************************************************************
#include "../../Common/StringHelper.h"
//***************************************************************************
#include "CvarManager.h"
#include "Config.h"
//***************************************************************************
// Command line parser
#include "cmdlineparser/CmdLine.h"
using namespace TCLAP;
//***************************************************************************

namespace NGTech {
	//!@todo ��� ����� �� ������ ����
	const Vec3 CVARManager::m_DefaultGravity(Vec3(Math::ZERODELTA, -981.0, Math::ZERODELTA));

	//TODO:������� ��������� ������ �������
	CVARManager::CVARManager(const std::shared_ptr<Config>& _c)
		:cfg(_c),
		//Render
		r_width(_c->GetInt("Render", "r_width", 1920)),
		r_height(_c->GetInt("Render", "r_height", 1080)),
		r_bpp(_c->GetInt("Render", "r_bpp", 32)),
		r_zdepth(_c->GetInt("Render", "r_zdepth", 24)),
		r_aa_mode((AA_MODE)_c->GetInt("Render", "r_aa_mode", AA_NONE)),// By default AA is disabled
		r_fullscreen(_c->GetBool("Render", "r_fullscreen", true)),
		r_shadowsize(_c->GetFloat("Render", "r_shadowsize", 512)),
		r_shadowtype(_c->GetFloat("Render", "r_shadowtype", 4)),
		r_specular(_c->GetBool("Render", "r_specular", true)),
		r_hdr(_c->GetBool("Render", "r_hdr", true)),
		r_parallax(_c->GetInt("Render", "r_parallax", 1)),// By default be use simple Parallax
		r_reflections(_c->GetBool("Render", "r_reflections", true)),
		r_wireframe(_c->GetBool("Render", "r_wireframe", false)),
		r_ShadersAllowCache(_c->GetBool("Render", "r_ShadersAllowCache", true)),
		//Client
		cl_fov(_c->GetInt("Client", "cl_fov", 60)),
		cl_gamma(_c->GetFloat("Client", "cl_gamma", Math::ONEFLOAT)),
		//Window
		w_withoutBorder(_c->GetBool("Window", "w_withoutBorder", false)),
		//Debug
		d_DisplayInfo(_c->GetBool("Debug", "d_DisplayInfo", false)),

		// From System.ltx, Section - [System]
		sys_game_folder("error"),
		sys_game_dll("error"),
		canBeSaved(true)
	{
		//!@todo Static val

		// Physics
		//m_DefaultGravity()

		_ValidateConfig();
		_ParseSystemCFG();
	}

	CVARManager::CVARManager()
		:CVARManager(std::make_shared<Config>("../user.ltx"))
	{
	}

	CVARManager::~CVARManager()
	{
		cfg.reset();
		systemCfg.reset();
	}

	bool CVARManager::SaveConfig()
	{
		Debug(__FUNCTION__);

		if (!canBeSaved)
			return false;

		//Render
		cfg->Put("Render", "r_width", r_width);
		cfg->Put("Render", "r_height", r_height);
		cfg->Put("Render", "r_bpp", r_bpp);
		cfg->Put("Render", "r_zdepth", r_zdepth);
		cfg->Put("Render", "r_fullscreen", r_fullscreen);
		cfg->Put("Render", "r_aa_mode", r_aa_mode);
		cfg->Put("Render", "r_shadowsize", r_shadowsize);
		cfg->Put("Render", "r_shadowtype", r_shadowtype);
		cfg->Put("Render", "r_specular", r_specular);
		cfg->Put("Render", "r_hdr", r_hdr);
		cfg->Put("Render", "r_parallax", r_parallax);
		cfg->Put("Render", "r_reflections", r_reflections);
		cfg->Put("Render", "r_wireframe", r_wireframe);
		cfg->Put("Render", "r_ShadersAllowCache", r_ShadersAllowCache);
		//Client
		cfg->Put("Client", "cl_fov", cl_fov);
		cfg->Put("Client", "cl_gamma", cl_gamma);
		//Window
		cfg->Put("Window", "w_withoutBorder", w_withoutBorder);
		//Debug
		cfg->Put("Debug", "d_DisplayInfo", d_DisplayInfo);

		return true;
	}

	/**
	*/
	void CVARManager::_ValidateConfig()
	{
		Debug(__FUNCTION__);

		if ((r_width <= 1) || (r_height <= 1))
		{
			r_width = 1920;
			r_height = 1080;
		}

		if (cl_fov <= 0)
			cl_fov = 75;

		if (cl_gamma <= Math::ZEROFLOAT)
			cl_gamma = Math::ONEFLOAT;

		// ��������� ��������� ��������
		if (r_aa_mode < AA_NONE) //-V1016
			r_aa_mode = AA_NONE;

		if (r_aa_mode > AA_FXAA_ULTRA) //-V1016
			r_aa_mode = AA_FXAA_ULTRA;
	}

	/**
	*/
	void CVARManager::ParseCommandLine(int argc, const char * const * argv)
	{
		Debug(__FUNCTION__);

		// Define the command line object.
		CmdLine cmd("Command description message", ' ', "0.9");

		r_fullscreen = 0;
		try {
			// argument flag can be only 1 character
			/* Window Mode */
			ValueArg <bool> br_fullscreen("f", "fullscreen", "fullscreen game-mode", false, r_fullscreen, "bool", cmd);

			/* Window Size */
			ValueArg<int> ir_width("w", "r_width", "window width", false, r_width, "int", cmd);
			ValueArg<int> ir_height("h", "r_height", "window height", false, r_height, "int", cmd);

			/*Game directory and module*/
			ValueArg<String> ssys_game_folder("c", "sys_game_folder", "sys game catalog", false, sys_game_folder, "string", cmd);
			ValueArg<String> ssys_game_dll("m", "sys_game_dll", "sys game dll", false, sys_game_folder, "string", cmd);

			if (argv != nullptr)
				cmd.parse(argc, argv);

			// �������� �������� �������� ��������� fullscreen
			// ���� ����� fullscreen �� ����������, ��� ���������� ���� fullscreen = 0,
			// �� ���������� ������� �����
			r_fullscreen = br_fullscreen.getValue();

			// ������������� ��������� ��������
			r_width = ir_width.getValue();
			r_height = ir_height.getValue();

			// ������������� ��������� ������� ����� � game-dll
			if (!ssys_game_folder.getValue().empty())
			{
				sys_game_folder = ssys_game_folder.getValue().data();
			}
			if (!ssys_game_dll.getValue().empty())
			{
				sys_game_dll = ssys_game_dll.getValue().data();
			}
		}
		catch (std::exception &e) {
			std::cout << e.what() << std::endl;
		}
	}

	void CVARManager::_ParseSystemCFG()
	{
		// From System.ltx, Section - [System]
		systemCfg = std::make_shared<Config>("../system.ltx");

		sys_game_dll = systemCfg->GetString("System", "sys_game_dll", "error");
		sys_game_folder = systemCfg->GetString("System", "sys_game_folder", "error");
	}
}