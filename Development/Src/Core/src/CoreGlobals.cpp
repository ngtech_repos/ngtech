/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "CorePrivate.h"
#include "CoreManager.h"

namespace NGTech {
	static CoreManager* core = nullptr;

	/**
	*/
	CORE_API CoreManager* GetCore() {
		return core;
	}

	/**
	*/
	CORE_API void SetCore(CoreManager* _core) {
		// Without asserts, because this function called in ctor of CoreManager, when object not constructed yet
		core = _core;
	}

	/**
	*/
	CORE_API FileSystem* GetVFS() {
		if (core&&core->vfs) {
			return core->vfs;
		}

		ASSERT(core, "Invalid pointer");
		ASSERT(core->vfs, "Invalid pointer");
		return nullptr;
	}

	/**
	*/
	CORE_API I_Window* GetWindow() {
		if (core&&core->iWindow) {
			return core->iWindow;
		}

		ASSERT(core, "Invalid pointer");
		ASSERT(core->iWindow, "Invalid pointer");
		return nullptr;
	}

	/**
	*/
	CORE_API const std::shared_ptr<CVARManager>& GetCvars() {
		static std::shared_ptr<CVARManager> ptr = nullptr;
		if (core&&core->cvars) {
			return core->cvars;
		}

		ASSERT(core, "Invalid pointer");
		ASSERT(core->cvars, "Invalid pointer");
		return ptr;
	}

	/**
	*/
	CORE_API I_RenderLowLevel* GetRender() {
		if (core&&core->iRender) {
			return core->iRender;
		}

		ASSERT(core, "Invalid pointer");
		ASSERT(core->iRender, "Invalid pointer");
		return nullptr;
	}

	/**
	*/
	CORE_API I_Audio* GetAudio() {
		if (core&&core->alSystem) {
			return core->alSystem;
		}

		ASSERT(core, "Invalid pointer");
		ASSERT(core->alSystem, "Invalid pointer");
		return nullptr;
	}

	/**
	*/
	CORE_API PhysSystem* GetPhysics() {
		if (core&&core->physSystem) {
			return core->physSystem;
		}

		ASSERT(core, "Invalid pointer");
		ASSERT(core->physSystem, "Invalid pointer");
		return nullptr;
	}
	/**
	*/
	CORE_API const std::shared_ptr<Cache>& GetCache() {
		static std::shared_ptr<Cache> ptr = nullptr;
		if (core&&core->cache) {
			return core->cache;
		}

		ASSERT(core, "Invalid pointer");
		ASSERT(core->cache, "Invalid pointer");
		return ptr;
	}

	/**
	*/
	CORE_API GUI* GetGUI() {
		if (core&&core->gui) {
			return core->gui;
		}

		ASSERT(core, "Invalid pointer");
		ASSERT(core->gui, "Invalid pointer");
		return nullptr;
	}

	/**
	*/
	CORE_API RenderPipeline* GetRenderPipeline() {
		if (core&&core->rpipeline) {
			return core->rpipeline;
		}

		ASSERT(core, "Invalid pointer");
		ASSERT(core->rpipeline, "Invalid pointer");
		return nullptr;
	}

	/**
	*/
	CORE_API SceneManager* GetScene() {
		if (core&&core->scene) {
			return core->scene;
		}

		ASSERT(core, "Invalid pointer core");
		if (!core) return nullptr;
		ASSERT(core->scene, "Invalid pointer core->scene");
		if (!core->scene) return nullptr;
		return nullptr;
	}

	/**
	*/
	CORE_API const std::shared_ptr<IGame>& GetGame() {
		static std::shared_ptr<IGame> ptr = nullptr;
		if (core&&core->game) {
			return core->game;
		}

		ASSERT(core, "Invalid pointer");
		ASSERT(core->game, "Invalid pointer");
		return ptr;
	}

	/**
	*/
	CORE_API const std::shared_ptr<ScriptManager>& GetScriptManager() {
		static std::shared_ptr<ScriptManager> ptr = nullptr;
		if (core&&core->scriptManager) {
			return core->scriptManager;
		}

		ASSERT(core, "Invalid pointer");
		ASSERT(core->scriptManager, "Invalid pointer");
		return ptr;
	}

	/**
	*/
	CORE_API SkinnedMeshLoader* GetSkinnedMeshLoader() {
		if (core&&core->skinnedMeshLoader) {
			return core->skinnedMeshLoader;
		}

		ASSERT(core, "Invalid pointer");
		ASSERT(core->skinnedMeshLoader, "Invalid pointer");
		return nullptr;
	}

	/**
	*/
	CORE_API MeshLoader* GetMeshLoader() {
		if (core&&core->meshLoader) {
			return core->meshLoader;
		}

		ASSERT(core, "Invalid pointer");
		ASSERT(core->meshLoader, "Invalid pointer");
		return nullptr;
	}

	/**
	*/
	CORE_API DebugLayer* GetDebug() {
		if (core&&core->debug) {
			return core->debug;
		}

		ASSERT(core, "Invalid pointer");
		ASSERT(core->debug, "Invalid pointer");
		return nullptr;
	}

	/**
	*/
#if USE_STEAMWORKS
	CORE_API const std::shared_ptr<SteamWorksMgr>& GetSteamworksMgr() {
		if (core&&core->steamworks) {
			return core->steamworks;
		}

		ASSERT(core, "Invalid pointer");
		ASSERT(core->steamworks, "Invalid pointer");
		return nullptr;
	}
#endif

#ifndef DROP_EDITOR
	/**
	*/
	CORE_API const std::shared_ptr<Statistic>& GetStatistic() {
		static auto stat = std::make_shared<Statistic>();
		ASSERT(stat, "Invalid pointer");
		return stat;
	}

#endif
}