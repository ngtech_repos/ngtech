/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "CorePrivate.h"
//**************************************
#include <stdlib.h>
#include <stdio.h>
//**************************************
#include "../../Common/IScripting.h"
#include "../../Common/ScriptManager.h"
//**************************************
#include "MeshFormatNGGFStatic.h"
#include "SkinnedMeshLoader.h"
#include "MeshFormatNGGFSkinned.h"
//**************************************
#include "SteamWorksMgr.h"
#include "StatsAndAchievements.h"
//**************************************

namespace NGTech {
	/*
	*/
	void DestroyAdditions();
	void InitDbgTools();
	bool InitAdditions();

	/*
	*/
	CoreManager::CoreManager()
	{
		SetCore(this);
		if (!InitAdditions()) {
			PlatformError("Failed InitAdditions", "NGTech error", true);
			return;
		}

		_PreInitFast();
	}

	/*
	*/
	CoreManager::~CoreManager()
	{
		SAFE_RELEASE(iWindow); //-V809
		DefaultResources::CleanDefaultResources();
		SAFE_rESET(info);
		SAFE_rESET(log);
		SAFE_rESET(scriptManager);
		SAFE_DELETE(skinnedMeshLoader);
		SAFE_DELETE(meshLoader);
		SAFE_DELETE(debug);
		SAFE_DELETE(platformM);
#if USE_STEAMWORKS
		SAFE_rESET(steamworks);
#endif
		DestroyAdditions();
	}

	/*
	*/
	void CoreManager::ParseCommandLine(int argc, const char * const * argv) {
		ASSERT(cvars, "Invalid pointer on cvars");
		if (!cvars)
			return;

		cvars->ParseCommandLine(argc, argv);
	}

	/*
	*/
	void CoreManager::_PreInitFast() {
		_InitUserDir();
		platformM = new PlatformManager();
		if (!_InitPlatformCode())
			return;

		debug = new DebugLayer();

		/**
		*/
		meshLoader = new MeshLoader();
		meshLoader->RegisterFormat(new MeshFormatNGGFStatic());

		/**
		*/
		skinnedMeshLoader = new SkinnedMeshLoader();
		skinnedMeshLoader->RegisterFormat(new MeshFormatNGGFSkinned());

		/**/
		scriptManager = std::make_shared<ScriptManager>();

		/**/
#ifdef _ENGINE_DEBUG_
		log = std::make_shared<Log>("EngineLog", false);
#else
		log = std::make_shared<Log>();
#endif

		/*Assert, callback tools*/
		InitDbgTools();

		/*System Info*/
		{
			info = std::make_shared<SystemInfo>();

			ASSERT(info, "[CoreManager::_PreInitFast] Failed Init SystemInfo");

			Debug("[Init] SystemInfo finished");
			LogPrintf(info->GetBinaryInfo());
			LogPrintf(info->GetSystemInfo());
			LogPrintf(info->GetCPUInfo());
			LogPrintf(info->GetGPUInfo());
			LogPrintf("GPU Count: %i", info->GetGPUCount());
			LogPrintf("GPU Memory: %i", info->GetGPUMemory());
		}
	}

	/*
	*/
	void CoreManager::InitAllLuaBindings() {
		for (const auto & interp : m_LuaBindings) {
			if (!interp->IsInited()) {
				if (interp->Initialise())
					interp->SetInited(true);
				else
					Warning("InitAllLuaBindings: failed initialize");
			}
		}
	}

	/*
	*/
	void CoreManager::DestroyAllLuaBindings() {
		for (auto & interp : m_LuaBindings) {
			SAFE_rESET(interp);
		}
	}

	/*
	*/
	void CoreManager::_InitUserDir(const char* _dir) {
		FileUtil::CreateDirectoryIfNotExist(_dir);
	}

	/*
	*/
	bool CoreManager::_InitPlatformCode() {
		if ((platformM == nullptr) || (platformM != nullptr && (platformM->InitPlatformCode() == false)))
		{
			Error(__FUNCTION__, true);
			return false;
		}
		return true;
	}

	/*
	*/
	void CoreManager::RegisterLuaBindings(IScriptInterp*_interp) {
		ASSERT(_interp, "Invalid pointer on _interp");
		RegisterLuaBindings(std::shared_ptr<IScriptInterp>(_interp));
	}

	/*
	*/
	void CoreManager::RegisterLuaBindings(const std::shared_ptr<IScriptInterp> & _interp) {
		ASSERT(_interp, "Invalid pointer on _interp");
		m_LuaBindings.push_back(_interp);
	}
}