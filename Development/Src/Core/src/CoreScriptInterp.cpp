/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "CorePrivate.h"
#include "CoreScriptInterp.h"
//***************************************************
#include "lua/luabind.hpp"
#include "lua/operator.hpp"
#include "lua/lua.hpp"
//***************************************************
#include "../../Common/IAudio.h"
#include "../../Common/IDataManager.h"
//***************************************************
#include "WrappedScriptFunctions.h"
#include "SteamWorks_API.h"
//***************************************************
#include "../inc/lua/out_value_policy.hpp"

namespace NGTech {
	/**
	*/
	CoreScriptInterp::CoreScriptInterp()
		:mLuaState(nullptr)
	{
		m_Type = ScriptType::LUA_SCRIPT;
	}

	/**
	*/
	bool CoreScriptInterp::Initialise() {
		using namespace luabind;
		// Create a new lua state
		mLuaState = luaL_newstate();
		luaL_openlibs(mLuaState);

		// Connect LuaBind to this lua state
		open(mLuaState);
		bindLogFunctions();
		bindMathFunctions();
		bindCoreFunctions();

		return true;
	}

	/**
	*/
	CoreScriptInterp::~CoreScriptInterp() {
		lua_close(mLuaState);
	}

	/**
	*/
	void CoreScriptInterp::bindMathFunctions()
	{
		using namespace luabind;
		using namespace NGTech;

		try {
			module(mLuaState)
				[
					//Math
					class_<Math>("Math")
					.def("clamp", &Math::Clamp<float>)
				.def("angleBetweenVec", &Math::angleBetweenVec)
				.def("insidePolygon", &Math::insidePolygon)
				.def("intersectPlaneByRay", &Math::intersectPlaneByRay)
				.def("intersectPolygonByRay", &Math::intersectPolygonByRay)
				.def("intersectSphereByRay", &Math::intersectSphereByRay)

				//Vec2
				, class_<Vec2>("Vec2")
				.def(constructor<>())
				.def(constructor<float, float>())
				.def(constructor<const Vec2 &>())
				.def("normalize", &Vec2::normalize)
				.def("dot", &Vec2::dot)
				.def("length", &Vec2::length)
				//operators
				.def(self - Vec2())
				.def(self + Vec2())
				.def(const_self / const_self) //-V501
				.def(const_self / other<float>())
				.def(other<float>() / const_self)
				.def(const_self * other<float>())
				.def(const_self * Vec2())//����������,��� ���������
				.def(const_self + const_self)
				.def(const_self - const_self) //-V501
				//TODO:Core:Script:Added:Right?
				.def(self == const_self)
				.def(const_self == const_self) //-V501
				//Vec3
				, class_<Vec3>("Vec3")
				.def(constructor<>())
				.def(constructor<float, float, float>())
				.def(constructor<const Vec3 &>())
				.def(constructor<const Vec4 &>())
				.def("normalize", &Vec3::normalize)
				.def("dot", &Vec3::dot)
				.def("length", &Vec3::length)
				.def("cross", &Vec3::cross)
				//operators
				.def(self - Vec3())
				.def(self + Vec3())
				.def(const_self / const_self) //-V501
				.def(const_self / other<float>())
				.def(other<float>() / const_self)
				.def(const_self * other<float>())
				.def(const_self * Vec3())//����������,��� ���������
				.def(const_self + const_self)
				.def(const_self - const_self) //-V501
				//TODO:Core:Script:Added:Right?
				.def(self == const_self)
				.def(const_self == const_self) //-V501
				//Vec4
				, class_<Vec4>("Vec4")
				.def(constructor<>())
				.def(constructor<float, float, float, float>())
				.def(constructor<const Vec4 &>())
				.def(constructor<const Vec3 &>())
				.def(constructor<const Vec3 &, float>())
				.def("normalize", &Vec4::normalize)
				.def("dot", &Vec4::dot)
				.def("length", &Vec4::length)
				//operators
				.def(self - Vec4())
				.def(self + Vec4())
				.def(const_self / const_self) //-V501
				.def(const_self / other<float>())
				.def(other<float>() / const_self)
				.def(const_self * other<float>())
				.def(const_self * Vec4())//����������,��� ���������
				.def(const_self + const_self)
				.def(const_self - const_self) //-V501
				//TODO:Core:Script:Added:Right?
				.def(self == const_self)
				.def(const_self == const_self) //-V501
				//TBNComputer
				, class_<TBNComputer>("TBNComputer")
				.def("computeN", &TBNComputer::computeN)
				.def("computeTBN", &TBNComputer::computeTBN)
				];
		}
		catch (std::exception& e)
		{
			ErrorWrite("[s] Failed binding %s", __FUNCTION__, e.what());
		}
		/*
		Vec2& operator=(const Vec2 &in);

		float& operator[](int index);
		float operator[](int index) const;

		operator float*();
		operator const float*() const;

		Not Exist in LUA
		Vec2& operator+=(const Vec2 &v);
		Vec2& operator-=(const Vec2 &v);
		Vec2& operator*=(const Vec2 &v);
		Vec2& operator/=(const Vec2 &v);

		bool operator!=(const Vec2 &v) const;

		*/
	}

	/**
	*/
	void CoreScriptInterp::bindLogFunctions() {
		using namespace luabind;
		using namespace NGTech;
		try {
			module(mLuaState)
				[
					def("LogPrintf", &ScriptLogPrintf),
					def("Debug", &ScriptDebug),
					def("Warning", &ScriptWarning),
					def("Error", &ScriptError)
				];
		}
		catch (std::exception& e)
		{
			ErrorWrite("[s] Failed binding %s", __FUNCTION__, e.what());
		}
	}

	/**
	*/
	void CoreScriptInterp::bindCoreFunctions() {
		using namespace luabind;
		using namespace NGTech;
		try {
		}
		catch (std::exception& e)
		{
			ErrorWrite("[s] Failed binding %s", __FUNCTION__, e.what());
		}
	}

	/**
	*/
	void CoreScriptInterp::bindSteamWorks() {
		using namespace luabind;
		using namespace NGTech;
		try {
			module(mLuaState)
				[
					def("API_SteamWorks_SetAchievement", &API_SteamWorks_SetAchievement),
					def("API_SteamWorks_DeleteAchievement", &API_SteamWorks_DeleteAchievement),
					def("API_SteamWorks_SetStat", &API_SteamWorks_SetStat),
					def("API_SteamWorks_GetStat", &API_SteamWorks_GetStat, luabind::out_value<2>())
				];
		}
		catch (std::exception& e)
		{
			ErrorWrite("[s] Failed binding %s", __FUNCTION__, e.what());
		}
	}
}