/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
/*
 * This source file is part of MyGUI. For the latest info, see http://mygui.info/
 * Distributed under the MIT License
 * (See accompanying file COPYING.MIT or copy at http://opensource.org/licenses/MIT)
 */

#include "CorePrivate.h"

#include <fstream>

#include "DataStream.h"

namespace NGTech
{
	DataStream::DataStream() :
		mStream(nullptr),
		mSize((size_t)-1)
	{
	}

	DataStream::DataStream(std::ifstream* _stream) :
		mStream(_stream),
		mSize((size_t)-1)
	{
	}

	DataStream::~DataStream()
	{
	}

	size_t DataStream::Size()
	{
		if (mStream == nullptr) return 0;
		if (mSize == (size_t)-1)
		{
			mStream->seekg(0, std::ios::end);
			mSize = (size_t)mStream->tellg();
			mStream->seekg(0, std::ios::beg);
		}
		return mSize;
	}

	bool DataStream::Eof()
	{
		return mStream == nullptr ? true : mStream->eof();
	}

	void DataStream::Readline(String& _source, unsigned int _delim)
	{
		if (mStream == nullptr) return;
		std::getline(*mStream, _source, (char)_delim);
	}

	size_t DataStream::Read(void* _buf, size_t _count)
	{
		if (mStream == nullptr) return 0;
		size_t count = std::min(Size(), _count);
		mStream->read((char*)_buf, count);
		return count;
	}
} // namespace NGTech