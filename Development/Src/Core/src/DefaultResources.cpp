/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "CorePrivate.h"

#include "DefaultResources.h"

#include "Mesh.h"
#include "SkinnedMesh.h"

namespace NGTech
{
	CORE_API String DefaultResources::Texture_DefaultDiffuse_Name("DefaultDiffuse.dds");
	CORE_API String DefaultResources::Material_Default_Name("defmat.mat");

	//TODO: Replace cube.nggf on error.nggf
	CORE_API Mesh* 				DefaultResources::ErrorMesh = new Mesh("cube.nggf", false);
	CORE_API SkinnedMesh*		DefaultResources::AnimatedErrorMesh = new SkinnedMesh("chammy.nggf", false);
	CORE_API IResource*			DefaultResources::DefaultMaterial = nullptr;

	//!@todo IMPLEMENT THIS
	void DefaultResources::InitDefaultResources(IResource* _DefaultMaterial)
	{
		DefaultMaterial = _DefaultMaterial;

		// ��������� ������ �� ����
		if (DefaultMaterial)
			DefaultMaterial->Load();

		if (ErrorMesh)
			ErrorMesh->OnlyLoad();

		if (AnimatedErrorMesh)
			AnimatedErrorMesh->OnlyLoad();
	}

	void DefaultResources::CleanDefaultResources()
	{
		Texture_DefaultDiffuse_Name.clear();
		Material_Default_Name.clear();

		SAFE_RELEASE_ADV(ErrorMesh, ErrorMesh->GetPath().c_str());
		SAFE_RELEASE_ADV(AnimatedErrorMesh, ErrorMesh->GetPath().c_str());
	}
}