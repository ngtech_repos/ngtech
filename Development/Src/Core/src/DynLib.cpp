/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "CorePrivate.h"
#if  (PLATFORM_OS == PLATFORM_OS_LINUX) || (PLATFORM_OS == PLATFORM_OS_ANDROID)
#include <dlfcn.h>
#endif

#if PLATFORM_OS == PLATFORM_OS_WINDOWS
#include <windows.h>
#endif

namespace NGTech
{
	//-----------------------------------------------------------------------
	DynLib::DynLib(const String& name)
	{
		mName = name;
		m_hInst = NULL;
	}

	//-----------------------------------------------------------------------
	DynLib::~DynLib()
	{
	}

	//-----------------------------------------------------------------------
	void DynLib::Load()
	{
		// Log library load
		LogPrintf("Loading library %s", mName.c_str());

		String name = mName;
#if (PLATFORM_OS == PLATFORM_OS_LINUX) || (PLATFORM_OS == PLATFORM_OS_ANDROID)
		// dlopen() does not add .so to the filename, like windows does for .dll
		if (name.substr(name.length() - 3, 3) != ".so")
			name += ".so";
#endif

		m_hInst = (DYNLIB_HANDLE)DYNLIB_LOAD(name.c_str());

		if (!m_hInst)
			Warning("Could not load dynamic library %s.  System Error: %s DynLib::load", mName.c_str(), GetError().c_str()); //-V111
	}

	//-----------------------------------------------------------------------
	void DynLib::Unload()
	{
		// Log library unload
		LogPrintf("Unloading library %s", mName.c_str()); //-V111

		if (DYNLIB_UNLOAD(m_hInst))
			Warning("Could not unload dynamic library %s.  System Error: %s DynLib::unload", mName.c_str(), GetError().c_str()); //-V111
	}

	//-----------------------------------------------------------------------
	void* DynLib::GetSymbol(const String& strName) const throw()
	{
		return (void*)DYNLIB_GETSYM(m_hInst, strName.c_str());
	}

	//-----------------------------------------------------------------------
	String DynLib::GetError(void)
	{
#if PLATFORM_OS == PLATFORM_OS_WINDOWS
		LPVOID lpMsgBuf;
		FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			GetLastError(),
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR)&lpMsgBuf,
			0,
			NULL
		);
		String ret = (char*)lpMsgBuf;
		// Free the buffer.
		LocalFree(lpMsgBuf);
		return ret;
#elif  (PLATFORM_OS == PLATFORM_OS_LINUX) || (PLATFORM_OS == PLATFORM_OS_ANDROID)
		return String(dlerror());
#elif PLATFORM_OS == PLATFORM_OS_MACOSX
		return String(mac_errorBundle());
#else
		return String("");
#endif
	}
}