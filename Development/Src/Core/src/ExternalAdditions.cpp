/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "CorePrivate.h"

namespace NGTech
{
	/**
	*/
	bool DemoStartCheck();

	/**
	*/
	void ILSystemInit();

	/**
	*/
#if USE_STEAMWORKS
	/**
	SteamWorks
	*/
	void DestroySteamWorks();
#endif

#if USE_OWN_MINIDUMP
	/**
	*/
	bool InitMiniDump();
#endif

	/**
	SteamWorks
	*/
#if USE_STEAMWORKS
	struct SteamWorksMgr* InitSteamWorks();
#endif

	/**
	*/
	bool InitAdditions()
	{
#if USE_STEAMWORKS
		if (!InitSteamWorks())
			return false;
#endif

#ifndef _ENGINE_DEBUG_
#if USE_OWN_MINIDUMP
		if (!InitMiniDump())
			return false;
#endif
#endif
		if (!DemoStartCheck())
			return false;

		ILSystemInit();

		return true;
	}

	/**
	*/
	void DestroyAdditions()
	{
#if USE_STEAMWORKS
		DestroySteamWorks();
#endif
	}
}