/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/

#include "CorePrivate.h"
//***************************************************************************
#include "string.h"//memset for gcc
//***************************************************************************
#include "ILImage.h"
#include "Log.h"
#include "FileHelper.h"
#include "MathLib.h"
#include "VFS.h"
//***************************************************************************
#include "../../../Externals/ResIL/include/IL/il.h"
#include "../../../Externals/ResIL/include/IL/ilu.h"
//***************************************************************************
#define STB_IMAGE_IMPLEMENTATION
#include "../../../Externals/stb/stb_image.h"

namespace NGTech {
	static ENGINE_INLINE ILFormats BPP2Format(int bpp) {
		if (bpp == 3) return ILFormats::RGB;
		if (bpp == 4) return ILFormats::RGBA;
		return ILFormats::UNKNOWN;
	}

	static ENGINE_INLINE int Format2Bpp(ILFormats format) {
		if (format == ILFormats::RGB) return 3;
		if (format == ILFormats::RGBA) return 4;
		return 0;
	}

	static ENGINE_INLINE unsigned int _GetSize(ImageData* data) { ASSERT(data, "Invalid pointer"); return data->Width * data->Height * data->Depth * data->Bpp; }

	static ENGINE_INLINE ImageData *CreateEmpty2d(int width, int height, ILFormats format) {
		ImageData *image = new ImageData();

		image->Width = width;
		image->Height = height;
		image->Depth = 1;
		image->Bpp = Format2Bpp(format);
		image->Format = format;
		image->data.resize(_GetSize(image));

		return image;
	}

	static ENGINE_INLINE ImageData *CreateNoise2D(int width, int height, ILFormats format)
	{
		ImageData *image = new ImageData();

		image->Width = width;
		image->Height = height;
		image->Depth = 1;
		image->Bpp = Format2Bpp(format);
		image->Format = format;
		image->data.resize(_GetSize(image));

		for (auto& i : image->data) {
			i = (ILubyte)rand() % 255;
		}

		return image;
	}

	static ENGINE_INLINE ImageData *CreateEmpty3D(int width, int height, int depth, ILFormats format) {
		ImageData *image = new ImageData();

		image->Width = width;
		image->Height = height;
		image->Depth = depth;
		image->Bpp = Format2Bpp(format);
		image->Format = format;
		image->data.resize(_GetSize(image));

		for (auto& i : image->data) {
			i = (ILubyte)rand() % 255;
		}

		return image;
	}

	static ENGINE_INLINE ImageData *CreateNoise3D(int width, int height, int depth, ILFormats format) {
		ImageData *image = new ImageData();

		image->Width = width;
		image->Height = height;
		image->Depth = depth;
		image->Bpp = Format2Bpp(format);
		image->Format = format;
		image->data.resize(_GetSize(image));

		for (auto& i : image->data) {
			i = (ILubyte)rand() % 255;
		}

		return image;
	}

	static ENGINE_INLINE ImageData* Create2dDDS(const String & _path)
	{
		ImageData *imgData = new ImageData();

		VFileStream stream(_path);
		if (stream.IsDataExist())
		{
			auto fd = stream.LoadFile();
			if (!fd) {
				Warning("Failed Loading image %s!", _path.c_str()); //-V111
				stream.~VFileStream();
				delete imgData;
				return nullptr;
			}

			size_t lumpSize = stream.Size();
			void* lumpData = malloc(lumpSize);
			stream.Read(lumpData, lumpSize, 1);

#ifdef _UNICODE
			// Convert filename to a std::wstring
			std::wstring filename(_path.length(), L' ');
			std::copy(_path.begin(), _path.end(), _path.begin());
#else
			String filename = _path;
#endif

			// Try to determine the image type
			auto imageType = ilTypeFromExt(filename.c_str());
			if (imageType == IL_TYPE_UNKNOWN)
				imageType = ilDetermineTypeL(lumpData, lumpSize);

			// Try to load the image
			if (ilLoadL(imageType, lumpData, lumpSize) == IL_FALSE)
			{
				free(lumpData);
				delete imgData;
				return nullptr;
			}

			free(lumpData);

			// Retrieve image information
			imgData->Width = ilGetInteger(IL_IMAGE_WIDTH);
			imgData->Height = ilGetInteger(IL_IMAGE_HEIGHT);
			imgData->Depth = 1;
			imgData->Bpp = ilGetInteger(IL_IMAGE_BYTES_PER_PIXEL);
			imgData->Format = BPP2Format(imgData->Bpp);
			auto type = ilGetInteger(IL_IMAGE_TYPE);

			// If the format is not supported, convert to a supported format
			// Also convert if the pixel type is not unsigned byte
			auto convertToFormat = static_cast<unsigned int>(imgData->Format);

			switch (static_cast<unsigned int>(imgData->Format))
			{
			case IL_COLOUR_INDEX: convertToFormat = IL_RGB; break;
			case IL_ALPHA: convertToFormat = IL_LUMINANCE_ALPHA; break;
			case IL_BGR: convertToFormat = IL_RGB; break;
			case IL_BGRA: convertToFormat = IL_RGBA; break;
			default: break;
			}

			if ((convertToFormat != static_cast<unsigned int>(imgData->Format)) || (type != IL_UNSIGNED_BYTE))
			{
				if (ilConvertImage(convertToFormat, IL_UNSIGNED_BYTE) == IL_FALSE)
				{
					delete imgData;
					return nullptr;
				}
			}

			// Copy the image data into some new memory
			auto size = ilGetInteger(IL_IMAGE_SIZE_OF_DATA);
			imgData->data.resize(size);
			auto* data = ilGetData();

			memcpy(&(imgData->data[0]), data, size);

			return imgData;
		}

		Warning("Failed Loading image! File not found: %s", _path.c_str());
		return nullptr;
	}

	static ENGINE_INLINE ImageData* Create2dStbImage(const String & path)
	{
		ImageData*imgData = new ImageData();

		VFileStream file(path.c_str());
		if (!file.IsDataExist()) {
			delete imgData;
			return nullptr;
		}

		unsigned char* buffer = (unsigned char*)file.LoadFile();
		int len = file.Size();
		int w = 0;
		int h = 0;
		int components = STBI_default;
		auto req_components = STBI_rgb_alpha;

		auto imageData = stbi_loadf_from_memory(buffer, len, &w, &h, &components, req_components);

		if (imageData == nullptr) {
			Warning("Failed to load texture %s", path.c_str());
			throw(String("Failed to load texture"));
			return nullptr;
		}

		imgData->Width = w;
		imgData->Height = h;

		imgData->Depth = 1;

		imgData->Bpp = Format2Bpp(ILFormats::RGBA);
		imgData->Format = ILFormats::RGBA;
		imgData->DDSFormat = Format::GLFMT_A8R8G8B8;

		auto _size = _GetSize(imgData);
		imgData->data.resize(_size);

		for (unsigned int i = 0; i < _size; i++) {
			if (!imageData[i])
				continue;

			imgData->data[i] = imageData[i];
		}

		stbi_image_free(imageData);

		return imgData;
	}

	ImageData *Image::Load(const String &_path) {
		String strout;
		StringHelper::getExtension(strout, _path);

		// Currently we not have DDS decoder, later this fragment will deleted
		if (strout == "dds")
			return Create2dDDS(_path);
		else
			return Create2dStbImage(_path);
	}

	void Image::GenerateNormalMap(ImageData* data, int k) {
		if (data->Depth > 1) {
			Warning("Can't generate Normal map, because data->depth>1. Value %i", data->Depth);
			return;
		}

		unsigned int byteCount = data->Width * data->Height;
		ILubyte* ndata = new ILubyte[byteCount];

		for (unsigned int i = 0; i < byteCount; i++) {
			ndata[i] = (data->data[i*data->Bpp] + data->data[i*data->Bpp + 1] + data->data[i*data->Bpp + 2]) / 3.0;
		}

		float oneOver255 = 1.0 / 255.0;
		int   offs = 0;

		for (unsigned int i = 0; i < data->Width; i++) {
			for (unsigned int j = 0; j < data->Height; j++)
			{
				float c = ndata[i*data->Height + j] * oneOver255;
				float cx = ndata[i*data->Height + (j + 1) % data->Width] * oneOver255;
				float cy = ndata[((i + 1) % data->Height)*data->Height + j] * oneOver255;

				float dx = (c - cx) * k;
				float dy = (c - cy) * k;

				float len = (float)sqrt(dx*dx + dy * dy + 1);

				float nx = dy / len;
				float ny = -dx / len;
				float nz = 1.0 / len;

				data->data[offs] = (uint8_t)(128 + 127 * nx);
				data->data[offs + 1] = (uint8_t)(128 + 127 * ny);
				data->data[offs + 2] = (uint8_t)(128 + 127 * nz);
				offs += data->Bpp;
			}
		}
		SAFE_DELETE_ARRAY(ndata);
	}

	void Image::ToGreyScale(ImageData * data) {
		for (unsigned int i = 0; i < data->Width*data->Height*data->Depth; i++)
		{
			auto color = (data->data[i*data->Bpp]
				+ data->data[i*data->Bpp + 1]
				+ data->data[i*data->Bpp + 2]) / 3.0;

			data->data[i*data->Bpp] = color;
			data->data[i*data->Bpp + 1] = color;
			data->data[i*data->Bpp + 2] = color;
		}
	}
}