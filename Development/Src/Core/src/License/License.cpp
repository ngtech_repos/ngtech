/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "CorePrivate.h"

namespace NGTech
{
	bool DemoStartCheck()
	{
#ifdef _DEMO_BUILD
#if PLATFORM_OS == PLATFORM_OS_WINDOWS
		SYSTEMTIME st;
		GetSystemTime(&st);

		if (st.wYear > EXPIRES_YEAR || (st.wYear == EXPIRES_YEAR && st.wMonth > EXPIRES_MONTH))
		{
			PlatformError("This evaluation period has expired, go to www.nggames.com to get a newer one!", "EVALUATION PERIOD HAS EXPIRED");
			return false;
		}
#else
		time_t t = time(0);   // get time now
		struct tm  * st = localtime(&t);

		if (st->tm_year > EXPIRES_YEAR || (st->tm_year == EXPIRES_YEAR && st->tm_mon > EXPIRES_MONTH))
		{
			PlatformError("This evaluation period has expired, go to www.nggames.com to get a newer one!", "EVALUATION PERIOD HAS EXPIRED");
			return false;
		}
#endif
#endif

		return true;
	}
}