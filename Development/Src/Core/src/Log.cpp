/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/

#include "CorePrivate.h"

//***************************************************************************
#include <time.h>
#include <stdio.h>
#include <string.h>
#if PLATFORM_OS == PLATFORM_OS_WINDOWS
#include <windows.h>
#endif
//***************************************************************************
#include "Log.h"
#include "FileHelper.h"
//***************************************************************************
#if IS_OS_ANDROID
#include <android/log.h>
#define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, PROJECT_TITLE, __VA_ARGS__))
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, PROJECT_TITLE, __VA_ARGS__))
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, PROJECT_TITLE, __VA_ARGS__))
#define LOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR, PROJECT_TITLE, __VA_ARGS__))
#define LOGEF(...) ((void)__android_log_print(ANDROID_LOG_FATAL, PROJECT_TITLE, __VA_ARGS__))
#else
#define LOGD(...) printf(__VA_ARGS__);
#define LOGI(...) printf(__VA_ARGS__);
#define LOGW(...) printf(__VA_ARGS__);
#define LOGE(...) printf(__VA_ARGS__);
#define LOGEF(...) printf(__VA_ARGS__);
#endif
//***************************************************************************
//!@todo Android: Core:Check work on android - currently disabled
namespace NGTech {
	/**
	*/
	std::vector<Log::ListenerCallback*> Log::callbacks;

	/**
	*/
	void CORE_API DebugF(const String& _text, const char* _file, int _line) {
		char buf[32];
		sprintf(buf, "%d", _line);
		String text = _text;
		String line = String(buf);
#ifdef _ENGINE_DEBUG_
		Log::WriteDebug(text + " In: " + _file + " : " + line);
#else
		MACRO_UNUSED(text);
		MACRO_UNUSED(line);
#endif
	}

	/**
	*/
	void CORE_API Warning(const char *fmt, ...) {
		if (fmt == nullptr)
			return;

		char           msg[8000];

		va_list         argptr;
		va_start(argptr, fmt);
		vsprintf(msg, fmt, argptr);
		va_end(argptr);

		String sMsg = "[WARNING]: ";
		sMsg += msg;

		Log::WriteWarning(sMsg);
	}

	/**
	*/
	void CORE_API DebugM(const char *fmt, ...) {
		if (fmt == nullptr)
			return;

		char           msg[8000];

		va_list         argptr;
		va_start(argptr, fmt);
		vsprintf(msg, fmt, argptr);
		va_end(argptr);

#ifdef _ENGINE_DEBUG_
		Log::WriteDebug(msg);
#endif
	}

	/**
	*/
	void CORE_API LogPrintf(const char *fmt, ...) {
		if (fmt == nullptr)
			return;

		char           msg[8000];

		va_list         argptr;
		va_start(argptr, fmt);
		vsprintf(msg, fmt, argptr);
		va_end(argptr);

		if (msg == nullptr)
			return;

		Log::WriteInfo(msg);
	}

	/**
	*/
	void CORE_API ErrorWrite(const char *fmt, ...) {
		if (fmt == nullptr)
			return;

		char           msg[8000];

		va_list         argptr;
		va_start(argptr, fmt);
		vsprintf(msg, fmt, argptr);
		va_end(argptr);

		Log::ErrorWrite(msg);
	}

	/**
	*/
	std::vector<String> GetCallStackToString();

	void CORE_API WriteCallStack()
	{
		auto stack = GetCallStackToString();
		for (const auto &stackV : stack)
		{
			LogPrintf(stackV.c_str());
		}
	}

	/**
	*/
	void CORE_API Error(const String& text, bool _fatal) {
		Log::ErrorWrite(text);
		WriteCallStack();
		if (_fatal)	PlatformError(text.c_str());
	}

	/**
	*/
	Log::Log(const char* _name, bool _createBackup) :
		mFileName(_name)
	{
#if !IS_OS_ANDROID
		static String dir = "../Logs";

		FileUtil::UtilCreateDirectory(dir);

		try
		{
			//Create console, multi threaded logger
			console = stdout_logger_mt("console");

			//
			// Runtime log levels
			//
#if _ENGINE_DEBUG_
			set_level(level::debug); //Set global log level to info
			console->set_level(level::debug); // Set specific logger's log level
#else
			set_level(level::info); //Set global log level to info
			console->set_level(level::off); // Set specific logger's log level
#endif

			//
			// Create a file rotating logger with 5mb size max and 3 rotated files
			//
			file_logger = rotating_logger_mt("file_logger", _name, 1048576 * 5, 1, true);

			//
			// Customize msg format for all messages
			//
			set_pattern("*** [%H:%M:%S %z] [thread %t] %v ***");
			file_logger->set_pattern("%v");

			//
			// Asynchronous logging is very fast..
			// Just call NGTech::set_async_mode(q_size) and all created loggers from now on will be asynchronous..
			//
			size_t q_size = 1048576; //queue size must be power of 2
			NGTech::set_async_mode(q_size);

			if (_createBackup)
			{
				_CreateBackupFile();
				_PutHiddenHeader();
			}

			_WriteHeader();
		}
		catch (const spdlog_ex& ex)
		{
			LOGE("Log failed: %s", ex.what());
		}
#endif
	}

	/**
	*/
	Log::~Log()
	{
#if !IS_OS_ANDROID
		if (file_logger)
			file_logger->flush();
		file_logger.reset();
		console.reset();
#endif
	}

	/**
	*/
	void Log::WriteInfo(const String& _text) {
		if (_text.empty())
			return;

		String text = _text;
		try {
			_CallbackActions(text);

			text += '\n';

#if PLATFORM_OS==PLATFORM_OS_WINDOWS
			// output to debugger
			OutputDebugStringA(text.c_str());
#else
			LOGI(text.c_str());
#endif

#if !IS_OS_ANDROID
			String HTMLAdd = "<div id=\"msg\">";
			HTMLAdd += text + "</div>\n";

			auto ptr = get("console");
			if (!ptr) return;
			ptr->info(text);

			ptr = get("file_logger");
			if (!ptr) return;
			ptr->info(HTMLAdd);
#endif
		}
		catch (const spdlog_ex& ex)
		{
			LOGE("Log failed: %s", ex.what());
		}
		catch (std::exception&e)
		{
			PlatformError(e.what());
		}
	}

	/**
	*/
	void Log::WriteHeader(const String& _text) {
		String text = _text;
		try {
#if !IS_OS_ANDROID
			if (text.empty())
				return;

			text += '\n';

			String HTMLAdd = "<h2>";
			HTMLAdd += text + "</h2>\n";

			auto ptr = get("file_logger");
			if (!ptr) return;
			ptr->info(HTMLAdd);
#endif
		}
		catch (const spdlog_ex& ex)
		{
			LOGE("Log failed: %s", ex.what());
		}
		catch (std::exception&e)
		{
			PlatformError(e.what());
		}
	}

	/**
	*/
	void Log::ProfilerWrite(const String& _text) {
		if (_text.empty())
			return;

		String text = _text;

		try {
			_CallbackActions(text);

			text += '\n';

#if PLATFORM_OS==PLATFORM_OS_WINDOWS
			// output to debugger
			OutputDebugStringA(text.c_str());
#else
			LOGD(text.c_str());
#endif

#if !IS_OS_ANDROID
			String HTMLAdd = "<div id=\"prof\">";
			HTMLAdd += text + "</div>\n";

			auto ptr = get("file_logger");
			if (!ptr) return;
			ptr->info(HTMLAdd);

			ptr = get("console");
			if (!ptr) return;
			ptr->info(text);
#endif
		}
		catch (const spdlog_ex& ex)
		{
			LOGE("Log failed: %s", ex.what());
		}
		catch (std::exception&e)
		{
			PlatformError(e.what());
		}
	}

	/**
	*/
	void Log::WriteWarning(const String& _text) {
		if (_text.empty())
			return;

		String text = _text;

		try {
			_CallbackActions(text);
			text += '\n';

#if PLATFORM_OS==PLATFORM_OS_WINDOWS
			// output to debugger
			OutputDebugStringA(text.c_str());
#else
			LOGW(text.c_str());
#endif
#if !IS_OS_ANDROID
			String HTMLAdd = "<div id=\"warn\">";
			HTMLAdd += text + "</div>\n";

			auto ptr = get("file_logger");
			if (!ptr) return;
			ptr->warn(HTMLAdd);

			ptr = get("console");
			if (!ptr) return;
			ptr->warn(text);
#endif
		}
		catch (const spdlog_ex& ex)
		{
			LOGE("Log failed: %s", ex.what());
		}
		catch (std::exception&e)
		{
			PlatformError(e.what());
		}
	}

	/**
	*/
	void Log::WriteDebug(const String& _text) {
		if (_text.empty())
			return;

		String text = _text;

		try {
			_CallbackActions(text);
			text += '\n';

#if PLATFORM_OS==PLATFORM_OS_WINDOWS
			// output to debugger
			OutputDebugStringA(text.c_str());
#else
			LOGD(text.c_str());
#endif

#if !IS_OS_ANDROID
			String HTMLAdd = "<div id=\"msg\">";
			HTMLAdd += text + "</div>\n";

			auto ptr = get("file_logger");
			if (!ptr) return;
			ptr->debug(HTMLAdd);

			ptr = get("console");
			if (!ptr) return;
			ptr->debug(text);
#endif
		}
		catch (const spdlog_ex& ex)
		{
			LOGE("Log failed: %s", ex.what());
		}
		catch (std::exception&e)
		{
			PlatformError(e.what());
		}
	}

	/**
	*/
	void Log::ErrorWrite(const String& _text) {
		if (_text.empty())
			return;

		String text = _text;

		try {
			_CallbackActions(text);
			text += '\n';

#if PLATFORM_OS==PLATFORM_OS_WINDOWS
			// output to debugger
			OutputDebugStringA(text.c_str());
#else
			LOGE(text.c_str());
#endif

#if !IS_OS_ANDROID
			String HTMLAdd = "<div id=\"err\">";
			HTMLAdd += text + "</div>\n";

			auto ptr = get("file_logger");
			if (!ptr) return;
			ptr->error(HTMLAdd);
			ptr->flush();

			ptr = get("console");
			if (!ptr) return;
			ptr->error(text);
#endif
		}
		catch (std::exception&e)
		{
			PlatformError(e.what());
		}
	}

	/**
	*/
	void Log::_CreateBackupFile() const
	{
#if IS_OS_WINDOWS
		// simple:
		//		string bakpath = FileUtil::ReplaceExtension(m_szFilename,"bak");
		//		CopyFile(m_szFilename,bakpath.c_str(),false);

		// advanced: to backup directory
		char szPath[MAX_FILENAME_SIZE];

		String sExt = FileUtil::GetExt(mFileName);
		String sFileWithoutExt = FileUtil::GetFileName(mFileName);

		FileUtil::RemoveExtension(sFileWithoutExt);

#define LOG_BACKUP_PATH "LogBackups"

		const char *path = LOG_BACKUP_PATH;

		String szBackupPath;
		String sLogFilename;

		strcpy(szPath, path);

		szBackupPath = "../Logs/Backups/";

		FileUtil::CreateDirectoryIfNotExist(szBackupPath);

		sLogFilename = mFileName;

		FILE *in = fopen(sLogFilename.c_str(), "rb");

		String sBackupNameAttachment;

		// parse backup name attachment
		// e.g. BackupNameAttachment="attachment name"
		if (in)
		{
			bool bKeyFound = false;
			String sName;

			while (!feof(in))
			{
				unsigned char c = fgetc(in);

				if (c == '\"')
				{
					if (!bKeyFound)
					{
						bKeyFound = true;

						if (sName.find("BackupName=") == String::npos)
						{
#if PLATFORM_OS == PLATFORM_OS_WINDOWS
							OutputDebugStringA("Log::CreateBackupFile ERROR '");
							OutputDebugStringA(sName.c_str());
							OutputDebugStringA("' not recognized \n");
#endif
							assert(0);		// broken log file? - first line should include this name - written by LogVersion()
							return;
						}
						sName.clear();
					}
					else
					{
						sBackupNameAttachment = sName;
						break;
					}
					continue;
				}
				if (c >= ' ')
					sName += c;
				else
					break;
			}
			fclose(in);
		}

		String bakdest = FileUtil::Make(szBackupPath, sFileWithoutExt + sBackupNameAttachment + "." + sExt);
		strncpy(m_sBackupFilename, bakdest.c_str(), MAX_FILENAME_SIZE);
		m_sBackupFilename[sizeof(m_sBackupFilename) - 1] = 0;

		TODO("REPLACE ON CROSSPLATFORM");
		CopyFileA(sLogFilename.c_str(), bakdest.c_str(), false);
#endif
	}

	/**
	*/
	void Log::_PutHiddenHeader() const
	{
#if !IS_OS_ANDROID
		if (!file_logger) return;
		// Get time.
		time_t ltime;
		time(&ltime);
		tm *today = localtime(&ltime);

		char s[1024];

		strftime(s, 128, "%d %b %y (%H %M %S)", today);
		//print backup header
		file_logger->info(StringHelper::StickString("BackupName=\" Build from(%s) %s\"  -- hidden,used by backup system\n", __DATE__, s));
#endif
	}

	/**
	*/
	void Log::_WriteHeader() const
	{
#if !IS_OS_ANDROID
		if (!file_logger) return;
		// print log header
		file_logger->info("<html><head>\n");
		file_logger->info("<meta http-equiv=\"Content - Type\" content=\"text / html; charset = windows - 1251\" />\n");
		file_logger->info("<title>NGTech log</title>\n");
		file_logger->info("<style type=\"text/css\">\n");
		file_logger->info("body { background: #061920; padding: 0px; }\n");

		String toStick("h1{font-size: 18pt; font-family: Arial; color: #C9D6D6;margin: 20px;}");
		toStick += "h1{ font - size: 18pt; font - family: Arial; color: #C9D6D6; margin: 20px; }";
		toStick += "h2{font-size: 10pt; font-family: Arial; color: #C9D6D6;margin: 0px;padding-top: 10px;}";
		toStick += "#msg{background-color: #39464C;font-size: 10pt; font-family: Arial; color: white;padding-left: 5px;margin-bottom: 1px;}";
		toStick += "#warn{background-color: #A68600;font-size: 11pt;";
		toStick += "font-weight: bold;font-family: Arial; color: white;padding-left: 15px;margin-bottom: 1px;}";
		toStick += "#warn{background-color: #A68600;font-size: 11pt;font-weight: bold;font-family: Arial; color: white;padding-left: 15px;margin-bottom: 1px;}";
		toStick += "#prof{background-color: #ADD8E6;font-size: 11pt;font-weight: bold;font-family: Arial; color: black;padding-left: 15px;margin-bottom: 1px;}";
		toStick += "#err{background-color: maroon;font-size: 11pt;font-weight: bold;font-family: Arial; color: white;padding-left: 15px;margin-bottom: 1px;}";
		toStick += "</style></head>";

		file_logger->info(toStick);
		file_logger->info("<body><h1>NGTech log</h1>");
#endif
	}

	/**
	*/
	void Log::_CallbackActions(const String& text)
	{
		try {
			for (size_t i = 0; i < callbacks.size(); i++)
			{
				ASSERT(callbacks[i], "Not exist callback");

				if (callbacks[i])
					callbacks[i]->PushToLogCallback(text);
			}
		}
		catch (const spdlog_ex& ex)
		{
			LOGE("Log failed: %s", ex.what());
		}
		catch (std::exception&e)
		{
			Warning(e.what());
		}
	}

	/**
	*/
	void Log::AddListenerCallback(ListenerCallback* _cb) {
		callbacks.push_back(_cb);
	}

	/**
	*/
	Log::ListenerCallback::ListenerCallback() {
		callbacks.push_back(this);
	}
}