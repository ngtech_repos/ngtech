/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "CorePrivate.h"

#include "LuaScript.h"
#include "lua_chunk.h"

#include "IScripting.h"
#include "CoreManager.h"

namespace NGTech
{
	LuaScript* LuaScript::CreateActive(const String &_filename)
	{
		auto ptr = CreateInActive(_filename);
		ASSERT(ptr, "Invalid pointer on ptr");
		ptr->SetActive(true);
		return ptr;
	}

	LuaScript* LuaScript::CreateInActive(const String &_filename)
	{
		auto ptr = new LuaScript(_filename);
		return ptr;
	}

	LuaScript* LuaScript::RunGameEntryPoint(const String &_fromFile)
	{
		auto ptr = CreateActive(_fromFile);
		ASSERT(ptr, "Invalid pointer on ptr");
		ptr->CallSpecificFunction("GameEntryPoint", _fromFile);
		return ptr;
	}

	LuaScript::STATUS LuaScript::CallSpecificFunction(const String& _name, const String&_filename)
	{
		auto result = UNKNOWN_ERROR;

		if (!IsActive() || !m_chunk)
		{
			Warning("Script with name %s is not active, called in function %s", GetName().c_str(), _name.c_str());
			return result;
		}

		result = (STATUS)m_chunk->CallSpecificFunction(_name, _filename);

		return result;
	}

	LuaScript::LuaScript() {
		m_chunk = nullptr;
		m_name = "INVALID SCRIPTNAME";
		_AttachScript();
	}

	LuaScript::LuaScript(const String& _filename)
	{
		m_chunk = nullptr;
		m_name = _filename;

		_AttachScript();
		_CompileFile(m_name);
	}

	void LuaScript::_AttachScript()
	{
		auto _ptr = GetScriptManager();
		if (!_ptr)
		{
			DebugM("Not exist ScriptManager for LuaScript: %s", m_name.c_str());
			return;
		}

		if (_ptr->AttachScript(this))
			DebugM("LuaScript: %s has attached to script manager", m_name.c_str());
	}

	int LuaScript::_CompileFile(const String& _name)
	{
		if (_name.empty())
			return INVALID_FILE;

		VFile file(_name);
		if (!file.IsValid())
			return INVALID_FILE;

		_CreateChunk(_GetLuaState(), file.LoadFile());

		return 0;
	}

	void LuaScript::_CreateChunk(lua_State* L, char* fileContent) {
		m_chunk = std::make_shared<lua_chunk>(L, fileContent);
		m_chunk->New_Instance();
	}

	lua_State* LuaScript::_GetLuaState() const
	{
		static lua_State* state = nullptr;
		if (state)
			return state;

		auto bindings = GetCore()->GetLuaBindings();
		for (const auto & binding : bindings)
		{
			if (binding->GetType() == ScriptType::LUA_SCRIPT)
			{
				state = (lua_State*)binding->GetScriptState();
				ASSERT(state, "INVALID LUA_STATE");
				if (!state)
					return nullptr;

				return state;
			}
		}
		return nullptr;
	}

	LuaScript::~LuaScript() {
		SAFE_rESET(m_chunk);
	}

	Script::STATUS LuaScript::Loaded(void) {
		auto result = UNKNOWN_ERROR;

		if (!IsActive() || !m_chunk)
		{
			DebugM("Script with name %s is not active, called in function %s", GetName().c_str(), "Loaded");
			return result;
		}

		result = (STATUS)m_chunk->CallSpecificFunction("Loaded", GetName());

		return result;
	}

	Script::STATUS LuaScript::LoadedGUI(void) {
		auto result = UNKNOWN_ERROR;

		if (!IsActive() || !m_chunk)
		{
			DebugM("Script with name %s is not active, called in function %s", GetName().c_str(), "LoadedGUI");
			return result;
		}

		result = (STATUS)m_chunk->CallSpecificFunction("LoadedGUI", GetName());

		return result;
	}

	Script::STATUS LuaScript::Update(void) {
		auto result = UNKNOWN_ERROR;

		if (!IsActive() || !m_chunk)
		{
			DebugM("Script with name %s is not active, called in function %s", GetName().c_str(), "Update");
			return result;
		}

		result = (STATUS)m_chunk->CallSpecificFunction("Update", GetName());

		return result;
	}

	Script::STATUS LuaScript::UpdateInput(void) {
		auto result = UNKNOWN_ERROR;

		if (!IsActive() || !m_chunk)
		{
			DebugM("Script with name %s is not active, called in function %s", GetName().c_str(), "UpdateInput");
			return result;
		}

		result = (STATUS)m_chunk->CallSpecificFunction("UpdateInput", GetName());

		return result;
	}

	Script::STATUS LuaScript::UpdateGUI(void) {
		auto result = UNKNOWN_ERROR;

		if (!IsActive() || !m_chunk)
		{
			DebugM("Script with name %s is not active, called in function %s", GetName().c_str(), "UpdateGUI");
			return result;
		}

		result = (STATUS)m_chunk->CallSpecificFunction("UpdateGUI", GetName());

		return result;
	}

	Script::STATUS LuaScript::UpdatePhysics(void) {
		auto result = UNKNOWN_ERROR;

		if (!IsActive() || !m_chunk)
		{
			DebugM("Script with name %s is not active, called in function %s", GetName().c_str(), "UpdatePhysics");
			return result;
		}

		result = (STATUS)m_chunk->CallSpecificFunction("UpdatePhysics", GetName());

		return result;
	}

	Script::STATUS LuaScript::UpdateRender(void) {
		auto result = UNKNOWN_ERROR;

		if (!IsActive() || !m_chunk)
		{
			DebugM("Script with name %s is not active, called in function %s", GetName().c_str(), "UpdateRender");
			return result;
		}

		result = (STATUS)m_chunk->CallSpecificFunction("UpdateRender", GetName());

		return result;
	}

	Script::STATUS LuaScript::UpdateSceneManager(void) {
		auto result = UNKNOWN_ERROR;

		if (!IsActive() || !m_chunk)
		{
			DebugM("Script with name %s is not active, called in function %s", GetName().c_str(), "UpdateSceneManager");
			return result;
		}

		result = (STATUS)m_chunk->CallSpecificFunction("UpdateSceneManager", GetName());

		return result;
	}

	Script::STATUS LuaScript::UpdateAI(void) {
		auto result = UNKNOWN_ERROR;

		if (!IsActive() || !m_chunk)
		{
			DebugM("Script with name %s is not active, called in function %s", GetName().c_str(), "UpdateAI");
			return result;
		}

		result = (STATUS)m_chunk->CallSpecificFunction("UpdateAI", GetName());

		return result;
	}

	Script::STATUS LuaScript::DestroyEvent(void) {
		auto result = UNKNOWN_ERROR;

		if (!IsActive() || !m_chunk)
		{
			DebugM("Script with name %s is not active, called in function %s", GetName().c_str(), "DestroyEvent");
			return result;
		}

		result = (STATUS)m_chunk->CallSpecificFunction("DestroyEvent", GetName());

		return result;
	}

	Script::STATUS LuaScript::ReloadEvent(void) {
		auto result = UNKNOWN_ERROR;

		if (!IsActive() || !m_chunk)
		{
			DebugM("Script with name %s is not active, called in function %s", GetName().c_str(), "ReloadEvent");
			return result;
		}

		result = (STATUS)m_chunk->CallSpecificFunction("ReloadEvent", GetName());

		return result;
	}
}