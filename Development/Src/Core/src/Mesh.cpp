/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "CorePrivate.h"
//**************************************
#include "Mesh.h"
#include "MeshLoader.h"
//**************************************

namespace NGTech {
	/**
	*/
	Mesh::Mesh(const String &path, bool _loadv) {
		SetPath(path);
		if (_loadv)
			_Load(path);
	}

	/**
	*/
	Mesh::Mesh(unsigned int _numSubsets) {
		this->numSubsets = _numSubsets;

		this->subsets = new Subset*[this->numSubsets];

		//process subsets
		for (unsigned int s = 0; s < this->numSubsets; s++)
		{
			this->subsets[s] = new Subset();
			Subset *st = this->subsets[s];

			st->numVertices = 0;
			st->numIndices = 0;
			st->vertices = nullptr;
			st->indices = nullptr;
		}
	}

	/**
	*/
	void Mesh::_Load(const String &path) {
		SetPath(path);
		if (OnlyLoad())
			PushToRender();
	}

	/**
	*/
	Mesh::~Mesh() {
		for (unsigned int s = 0; s < numSubsets; s++) {
			Subset *st = subsets[s];

			if (!st)
			{
				ErrorWrite("not exist st");
				continue;
			}

			// vertices � indices ����� ������� � �����������
			SAFE_DELETE(st); // ������ ���������
		}

		delete[] subsets;
	}

	/**
	*/
	void Mesh::Save(const String &path) {
		//MT::ScopedGuard guard(mutex);
		//!@todo IMPLEMENT THIS
	}

	/**
	*/
	void Mesh::_DrawSubset(unsigned int s) {
		//MT::ScopedGuard guard(mutex);

		if (s > numSubsets)
		{
			ASSERT(false, "Invalid req subset: %i max:%i ", s, numSubsets);
			return;
		}

		auto st = subsets[s];

		ASSERT(st, "Invalid Pointer");
		ASSERT(st->mModelHelper, "Invalid Pointer"); //-V595

		if ((!st) || (!st->mModelHelper))
		{
			ErrorWrite(__FUNCTION__);
			return;
		}

		st->mModelHelper->Draw();
	}

	void Mesh::_DrawSubsetInstanced(unsigned int s) {
		//MT::ScopedGuard guard(mutex);

		if (s > numSubsets)
		{
			ASSERT(false, "Invalid req subset: %i max:%i ", s, numSubsets);
			return;
		}

		auto st = subsets[s];

		ASSERT(st, "Invalid Pointer");
		ASSERT(st->mModelHelper, "Invalid Pointer");

		auto _refCount = this->RefCounter();

		//Warning("Mesh::DrawSubsetInstanced: %i", _refCount);

		st->mModelHelper->DrawInstanced(_refCount, 0);
	}

	/**
	*/
	void Mesh::DrawAllSubsets() {
		//MT::ScopedGuard guard(mutex);
		//!@todo ADD asserts
		auto sub = (Subset *)subsets;
		if (!sub) {
			ErrorWrite("Not exist sub");
			return;
		}
		if (!sub->mModelHelper) {
			ErrorWrite("Not exist sub->mModelHelper");
			return;
		}
		sub->mModelHelper->Draw();
	}

	/**
	*/
	unsigned int Mesh::GetSubset(String name) {
		//MT::ScopedGuard guard(mutex);
		for (unsigned int s = 0; s < numSubsets; s++) {
			if (!subsets[s]) {
				ErrorWrite("Not exist subsets %i", s);
				continue;
			}

			if (subsets[s]->name == name)
				return s;
		}
		return 0;
	}

	/**
	*/
	const String& Mesh::GetSubsetName(unsigned int s) const {
		auto ptr = subsets[s];
		ASSERT(ptr, "[Mesh::GetSubsetName] Incorrect subset index for mesh %s, requested subsetId %i", this->GetPath().c_str(), s);

		if (!ptr)
		{
			Warning("[Mesh::GetSubsetName] Incorrect subset index for mesh %s, requested subsetId %i", this->GetPath().c_str(), s);
			return StringHelper::EMPTY_STRING;
		}
		return ptr->name;
	}

	/**
	*/
	void Mesh::CalculateTBN() {
		//MT::ScopedGuard guard(mutex);
		for (auto s = 0; s < numSubsets; s++)
		{
			auto st = subsets[s];
			if (!st)
			{
				ErrorWrite("[calculateTBN] not exist subsets");
				continue;
			}

			TODO("������� �� ������� �������");
			for (unsigned int iLoop = 0; iLoop < st->numIndices / 3; iLoop++)
			{
				int ind0 = st->indices[iLoop * 3 + 0];
				int ind1 = st->indices[iLoop * 3 + 1];
				int ind2 = st->indices[iLoop * 3 + 2];

				Vec3 n;
				TBNComputer::computeN(n, st->vertices[ind0].position,
					st->vertices[ind1].position,
					st->vertices[ind2].position);

				st->vertices[ind0].normal += n;
				st->vertices[ind1].normal += n;
				st->vertices[ind2].normal += n;
			}

			for (unsigned int vLoop = 0; vLoop < st->numVertices; vLoop++)
			{
				if (!&(st->vertices[vLoop]))
					continue;

				st->vertices[vLoop].normal = Vec3::normalize(st->vertices[vLoop].normal);
			}

			for (auto iLoop = 0; iLoop < st->numIndices / 3; iLoop++)
			{
				int ind0 = st->indices[iLoop * 3 + 0];
				int ind1 = st->indices[iLoop * 3 + 1];
				int ind2 = st->indices[iLoop * 3 + 2];

				Vec3 t[3];
				Vec3 b[3];

				if ((!&st->vertices[ind0]) || (!&st->vertices[ind1]) || (!&st->vertices[ind2]))
					continue;

				TBNComputer::computeTBN(t[0], b[0],
					st->vertices[ind0].position,
					st->vertices[ind1].position,
					st->vertices[ind2].position,
					st->vertices[ind0].texcoord,
					st->vertices[ind1].texcoord,
					st->vertices[ind2].texcoord,
					st->vertices[ind0].normal);
				TBNComputer::computeTBN(t[1], b[1],
					st->vertices[ind1].position,
					st->vertices[ind2].position,
					st->vertices[ind0].position,
					st->vertices[ind1].texcoord,
					st->vertices[ind2].texcoord,
					st->vertices[ind0].texcoord,
					st->vertices[ind1].normal);
				TBNComputer::computeTBN(t[2], b[2],
					st->vertices[ind2].position,
					st->vertices[ind0].position,
					st->vertices[ind1].position,
					st->vertices[ind2].texcoord,
					st->vertices[ind0].texcoord,
					st->vertices[ind1].texcoord,
					st->vertices[ind2].normal);

				st->vertices[ind0].tangent += t[0];
				st->vertices[ind1].tangent += t[1];
				st->vertices[ind2].tangent += t[2];

				st->vertices[ind0].binormal += b[0];
				st->vertices[ind1].binormal += b[1];
				st->vertices[ind2].binormal += b[2];
			}

			for (unsigned int vLoop = 0; vLoop < st->numVertices; vLoop++)
			{
				if (!&(st->vertices[vLoop]))
				{
					ErrorWrite("[calculateTBN] not exist subsets");
					continue;
				}

				st->vertices[vLoop].tangent = Vec3::normalize(st->vertices[vLoop].tangent);
				st->vertices[vLoop].binormal = Vec3::normalize(st->vertices[vLoop].binormal);
			}
		}
	}

	/**
	*/
	void Mesh::CalculateBoundings() {
		//MT::ScopedGuard guard(mutex);
		for (uint32_t s = 0; s < numSubsets; s++)
		{
			auto st = subsets[s];
			if (!st)
			{
				ErrorWrite("[calculateTBN] not exist subsets");
				continue;
			}

			st->bBox = BBox(st->vertices[0].position, st->vertices[0].position);

			for (uint32_t v = 1; v < st->numVertices; v++) {
				if (!st) {
					Error("Invalid pointer", false);
					continue;
				}
				st->bBox.AddPoint(st->vertices[v].position);
			}

			st->bSphere = BSphere(st->bBox.GetCenter(), 0);

			for (uint32_t v = 0; v < st->numVertices; v++) {
				if (!st) {
					Error("Invalid pointer", false);
					continue;
				}
				st->bSphere.AddPoint(st->vertices[v].position);
			}

			if (s == 0) {
				bBox = st->bBox;
			}
			else
			{
				bBox.AddBBox(st->bBox);
			}
		}

		bSphere = BSphere(bBox.GetCenter(), bBox.GetHalfSizeLength());
	}

	/**
	*/
	void Mesh::PushToRender() {
		//MT::ScopedGuard guard(mutex);
		auto _render = GetRender();
		ASSERT(_render, "Invalid pointer");

		for (uint32_t s = 0; s < numSubsets; s++)
		{
			auto st = subsets[s];
			if (!st)
			{
				ErrorWrite(__FUNCTION__);
				continue;
			}

			if (!st->mModelHelper)
			{
				st->mModelHelper = _render->GetModelHelper();

				if (!st->mModelHelper)
				{
					ErrorWrite("st->mModelHelper not exist");
					continue;
				}
			}

			st->mModelHelper->LoadToRender(st->vertices, st->indices, sizeof(Vertex), st->numVertices, st->numIndices);
		}
	}

	/**
	*/
	void Mesh::ClearVertex() {
		//MT::ScopedGuard guard(mutex);
		for (unsigned int s = 0; s < numSubsets; s++)
		{
			auto st = subsets[s];
			if (!st)
			{
				ErrorWrite(__FUNCTION__);
				continue;
			}

			st->ClearVertex();
		}
	}

	/**
	*/
	void Mesh::ClearIndices() {
		//MT::ScopedGuard guard(mutex);
		for (unsigned int s = 0; s < numSubsets; s++)
		{
			auto st = subsets[s];
			if (!st)
			{
				ErrorWrite(__FUNCTION__);
				continue;
			}

			st->ClearIndices();
		}
	}

	/**
	*/
	void Mesh::AddIndex(int _value) {
		//MT::ScopedGuard guard(mutex);
		//!@todo ADD asserts
		auto st = subsets[numSubsets - 1];
		if (!st)
			return;

		auto newInd = new unsigned int[st->numIndices + 1];
		if (!st->indices)
			st->indices = newInd;
		else
		{
			memcpy(newInd, st->indices, sizeof(unsigned int)*(st->numIndices));
		}
		st->numIndices += 1;
		newInd[st->numIndices] = _value;
		memcpy(st->indices, newInd, sizeof(unsigned int)*(st->numIndices));
	}

	/**
	*/
	void Mesh::AddVertexArray(const void *v, int num_vertex) {
		//MT::ScopedGuard guard(mutex);
		//!@todo ADD asserts
		auto st = subsets[numSubsets - 1];
		if (!st)
		{
			ErrorWrite("Not exist subset with number %i", numSubsets - 1);
			return;
		}

		auto newV = st->numVertices + num_vertex;
		auto newvertices = new Vertex[newV];

		if (!st->vertices)
			st->vertices = newvertices;
		else
		{
			memcpy(newvertices, st->vertices, sizeof(Vertex)*(st->numVertices));
		}

		for (unsigned int i = st->numVertices; i != newV; i++)
			newvertices[i] = *(Vertex*)v;

		st->numVertices = newV;
		memcpy(st->vertices, newvertices, sizeof(Vertex)*(st->numVertices));
	}

	bool Mesh::OnlyLoad() {
		//MT::ScopedGuard guard(mutex);
		auto _loader = GetMeshLoader();
		ASSERT(_loader, "Not exist mesh loader");

		if (!_loader->Load(this->GetPath(), this))
		{
			ErrorWrite("[Mesh::OnlyLoad] Failed load on path %s", this->GetPath().c_str());
			return false;
		}

		// TODO:CHECK
		//if (!subsets)
		//	return false;

		CalculateTBN();
		CalculateBoundings();

		SetLoaded(true);

		return true;
	}

	void Mesh::Load() {
		//MT::ScopedGuard guard(mutex);
		_Load(this->GetPath());
	}
}