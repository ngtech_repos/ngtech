/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//**************************************
#include "CorePrivate.h"
//**************************************
#include "Mesh.h"
#include "MeshFormatNGGFStatic.h"
#include <string.h>//memset for gcc
//**************************************

namespace NGTech
{
	/**
	*/
#define NAME_SIZE 64
	/**
	*/
	void ReadName(char *buffer, String &name, VFile *file)
	{
		memset(buffer, 0, NAME_SIZE);
		file->Read(buffer, NAME_SIZE, 1);
		name = buffer;
#ifdef HEAVY_DEBUG
		Debug((String("Loading material: ") + buffer).c_str());
#endif
	}

	/**
	Mesh
	*/
#define MESH_HEADER ('x' | 's' << 8 | 'm' << 16 | 's' << 24)
#define MESH_HEADER_UNWRAPPED ('x' | 's' << 8 | 'm' << 16 | 'u' << 24)

	/**
	*/
	bool MeshFormatNGGFStatic::Load(const String &path, Mesh *mesh)
	{
		ASSERT(mesh, "Invalid ponter on mesh");
		if (mesh == nullptr)
			return false;

		//begin loading
		VFile file(path.c_str());

		if (file.IsDataExist() == false)
		{
			Warning("Not exist file %s", path.c_str());
			return false;
		}

		//buffer
		char nameBuffer[NAME_SIZE];

		//header
		unsigned int header;
		file.Read(&header, sizeof(unsigned int), 1);

		if (header != MESH_HEADER && header != MESH_HEADER_UNWRAPPED)
		{
			Error(String("MeshLoader::Load() error: mesh file '" + path + "' has invalid header"), false);
			return false;
		}

		mesh->lmTexcoords = false;

		if (header == MESH_HEADER_UNWRAPPED)
		{
			Debug("MESH_HEADER_UNWRAPPED");
			mesh->lmTexcoords = true;
		}

		//num_subsets
		file.Read(&mesh->numSubsets, sizeof(unsigned int), 1);

		mesh->subsets = new Subset*[mesh->numSubsets];

		//process subsets
		for (unsigned int s = 0; s < mesh->numSubsets; s++)
		{
			mesh->subsets[s] = new Subset();
			Subset *st = mesh->subsets[s];

			//read the surface name
			ReadName(nameBuffer, st->name, &file);

			//number of vertices
			file.Read(&st->numVertices, sizeof(unsigned int), 1);
			st->vertices = new Vertex[st->numVertices];

			//process vertices
			for (unsigned int v = 0; v < st->numVertices; v++)
			{
				Vertex &vert = st->vertices[v];
				file.Read(&vert.position.x, sizeof(float), 1);
				file.Read(&vert.position.y, sizeof(float), 1);
				file.Read(&vert.position.z, sizeof(float), 1);
				file.Read(&vert.normal.x, sizeof(float), 1);
				file.Read(&vert.normal.y, sizeof(float), 1);
				file.Read(&vert.normal.z, sizeof(float), 1);
				file.Read(&vert.texcoord.x, sizeof(float), 1);
				file.Read(&vert.texcoord.y, sizeof(float), 1);
				if (mesh->lmTexcoords)
				{
					file.Read(&vert.lmTexcoord.x, sizeof(float), 1);
					file.Read(&vert.lmTexcoord.y, sizeof(float), 1);
				}
			}

			//indices
			file.Read(&st->numIndices, sizeof(unsigned int), 1);
			st->indices = new unsigned int[st->numIndices];

			//process faces
			for (unsigned int i = 0; i < st->numIndices; i++)
			{
				file.Read(&st->indices[i], sizeof(unsigned int), 1);
			}
		}
		return true;
	}

	/**
	*/
	bool MeshFormatNGGFStatic::Save(const String &path, Mesh *mesh)
	{
		TODO("IMPLEMENT LATER");
		//!@todo IMPLEMENT LATER

		ASSERT(mesh, "Invalid ponter on mesh");
		if (mesh == nullptr)
			return false;

		return true;
	}
}