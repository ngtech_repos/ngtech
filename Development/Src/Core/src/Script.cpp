/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "CorePrivate.h"

#include "Script.h"
#include "ScriptManager.h"

// for std::cout
#include <iostream>

namespace NGTech
{
	Script::Script(String _name)
		:Script()
	{
		m_name = _name;
		_AttachScript();
	}

	void Script::_AttachScript()
	{
		auto _ptr = GetScriptManager();
		if (!_ptr)
		{
			DebugM("Not exist ScriptManager for Script: %s", m_name.c_str());
			return;
		}

		if (_ptr->AttachScript(this))
			DebugM("Script: %s has attached to script manager", m_name.c_str());
	}
}