/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//**************************************
#include "CorePrivate.h"
//**************************************
#include "SkinnedMesh.h"
#include "SkinnedMeshLoader.h"
//**************************************

namespace NGTech
{
	/**
	*/
	SkinnedMesh::SkinnedMesh(const String &_path, bool _load)
		:numSubsets(1),
		visible(false)
	{
		SetPath(_path);
		if (_load)
			_Load(_path);
	}

	SkinnedMesh::SkinnedMesh(const SkinnedMesh & _other) :IResource(_other)
	{
		_Swap(_other);
	}

	/**
	*/
	SkinnedMesh::~SkinnedMesh() {
		for (unsigned int s = 0; s < numSubsets; s++) {
			auto st = subsets[s];

			if (!st)
			{
				ErrorWrite("not exist st");
				continue;
			}

			// vertices � indices ����� ������� � �����������
			SAFE_DELETE(st); // ������ ���������
		}

		delete[] subsets;
	}

	/**
	*/
	void SkinnedMesh::_Load(const String &_path) {
		//MT::ScopedGuard guard(mutex);
		SetPath(_path);
		Load();
	}

	void SkinnedMesh::Load() {
		//MT::ScopedGuard guard(mutex);
		if (OnlyLoad())
			PushToRender();
	}

	bool SkinnedMesh::OnlyLoad() {
		//	MT::ScopedGuard guard(mutex);

		auto skml = GetSkinnedMeshLoader();
		ASSERT(skml, "Invalid pointer");

		if (!skml->Load(GetPath(), this))
		{
			ErrorWrite("[Mesh::OnlyLoad] Failed load on path %s", GetPath().c_str());
			return false;
		}

		CalculateTBN();
		CalculateBoundings();

		SetLoaded(true);

		return true;
	}

	void SkinnedMesh::PushToRender() {
		//	MT::ScopedGuard guard(mutex);
		_CreateVBO();
	}

	/**
	*/
	void SkinnedMesh::SetFrame(int frame, int from, int to) {
		//	MT::ScopedGuard guard(mutex);

		if (numFrames == 0) {
			Warning("[%s] Invalid animated mesh %s - numFrames = 0 ", __FUNCTION__, this->GetPath().c_str());
			return;
		}

		if (from < 0) from = 0;
		if (to < 0) to = numFrames;

		int frame0 = (int)frame;
		frame -= frame0;
		frame0 += from;
		if (frame0 >= to) frame0 = (frame0 - from) % (to - from) + from;
		int frame1 = frame0 + 1;
		if (frame1 >= to) frame1 = from;

		for (unsigned int i = 0; i < numBones; i++)
		{
			auto ptr0 = &frames[frame0][i];
			auto ptr1 = &frames[frame1][i];
			auto ptr2 = &bones[i];
			if ((!ptr1) || (!ptr0) || (!ptr2))
				continue;

			// calculate matrixes
			auto translate = Mat4::translate(frames[frame0][i].position * (Math::ONEFLOAT - frame) + frames[frame1][i].position * frame);

			Quat rot = Quat::slerp(frames[frame0][i].rotation, frames[frame1][i].rotation, frame);

			bones[i].rotation = Mat4(rot.toMatrix());
			bones[i].transform = translate * bones[i].rotation;
		}

		for (unsigned int i = 0; i < numSubsets; i++)
		{
			// calculate vertexes
			auto st = subsets[i];
			if (!st)
				continue;

			st->bBox = BBox(st->vertices[0].position, st->vertices[0].position);

			for (unsigned int j = 0; j < st->numVertices; j++)
			{
				Vertex *v = &st->vertices[j];

				if (!v)
					continue;

				v->position = Vec3::ZERO;
				v->normal = Vec3::ZERO;
				v->tangent = Vec3::ZERO;
				v->binormal = Vec3::ZERO;

				for (int k = 0; k < v->numWeights; k++)
				{
					Weight *w = &v->weights[k];
					v->position += bones[w->bone].transform * w->position * w->weight;
					v->normal -= bones[w->bone].rotation * w->normal * w->weight;
					v->tangent += bones[w->bone].rotation * w->tangent * w->weight;
					v->binormal -= bones[w->bone].rotation * w->binormal * w->weight;
				}

				st->bBox.AddPoint(v->position);
			}

			st->bSphere = BSphere(st->bBox.GetCenter(), ((st->bBox.maxes - st->bBox.mins) * 0.5f).length());
		}

		bBox = subsets[0]->bBox;
		for (uint32_t s = 1; s < numSubsets; s++)
		{
			auto st = subsets[s];
			if (!st)
				continue;

			bBox.AddBBox(st->bBox);
		}

		bSphere = BSphere(bBox.GetCenter(), bBox.GetHalfSizeLength());
	}

	/**
	*/
	void SkinnedMesh::_DrawSubset(unsigned int s) {
		//MT::ScopedGuard guard(mutex);

		if (s > numSubsets)
		{
			ASSERT(false, "Invalid req subset: %i max:%i ", s, numSubsets);
			return;
		}

		auto st = subsets[s];

		ASSERT(st, "Invalid Pointer");
		ASSERT(st->mModelHelper, "Invalid Pointer"); //-V595

		if ((!st) || (!st->mModelHelper))
		{
			ErrorWrite(__FUNCTION__);
			return;
		}

		st->mModelHelper->UpdateAndDraw(0);
	}

	void SkinnedMesh::_DrawSubsetInstanced(unsigned int s) {
		//MT::ScopedGuard guard(mutex);

		if (s > numSubsets)
		{
			ASSERT(false, "Invalid req subset: %i max:%i ", s, numSubsets);
			return;
		}

		auto st = subsets[s];

		ASSERT(st, "Invalid Pointer");
		ASSERT(st->mModelHelper, "Invalid Pointer");

		auto _refCount = this->RefCounter();

		//Warning("Mesh::DrawSubsetInstanced: %i", _refCount);

		st->mModelHelper->UpdateAndDrawInstanced(0, _refCount, 0);
	}

	/**
	*/
	unsigned int SkinnedMesh::GetSubset(String name) {
		//MT::ScopedGuard guard(mutex);

		for (auto s = 0; s < numSubsets; s++)
		{
			auto ptr = subsets[s];
			if (!ptr)
				continue;

			if (ptr->name == name)
				return s;
		}
		return 0;
	}

	/**
	*/
	const String& SkinnedMesh::GetSubsetName(unsigned int s) {
		//MT::ScopedGuard guard(mutex);

		auto ptr = subsets[s];
		if (!ptr)
		{
			Warning("[SkinnedMesh::GetSubsetName] Incorrect subset index for mesh %s, requested subsetId %i", this->GetPath().c_str(), s);
			return StringHelper::EMPTY_STRING;
		}
		return ptr->name;
	}

	/**
	*/
	void SkinnedMesh::CalculateTBN() {
		//MT::ScopedGuard guard(mutex);

		SetFrame(0.0);

		for (unsigned int s = 0; s < numSubsets; s++)
		{
			auto st = subsets[s];
			if (!st)
				continue;

			for (unsigned int iLoop = 0; iLoop < st->numIndices / 3; iLoop++)
			{
				int ind0 = st->indices[iLoop * 3 + 0];
				int ind1 = st->indices[iLoop * 3 + 1];
				int ind2 = st->indices[iLoop * 3 + 2];

				if ((!ind0) || (!ind1) || (!ind2))
					continue;

				Vec3 t[3];
				Vec3 b[3];

				auto ptr0 = &st->vertices[ind0];
				auto ptr1 = &st->vertices[ind1];
				auto ptr2 = &st->vertices[ind2];

				if ((!ptr0) || (!ptr1) || (!ptr2))
					continue;

				TBNComputer::computeTBN(t[0], b[0],
					st->vertices[ind0].position,
					st->vertices[ind1].position,
					st->vertices[ind2].position,
					st->vertices[ind0].texcoord,
					st->vertices[ind1].texcoord,
					st->vertices[ind2].texcoord,
					st->vertices[ind0].normal);
				TBNComputer::computeTBN(t[1], b[1],
					st->vertices[ind1].position,
					st->vertices[ind2].position,
					st->vertices[ind0].position,
					st->vertices[ind1].texcoord,
					st->vertices[ind2].texcoord,
					st->vertices[ind0].texcoord,
					st->vertices[ind1].normal);
				TBNComputer::computeTBN(t[2], b[2],
					st->vertices[ind2].position,
					st->vertices[ind0].position,
					st->vertices[ind1].position,
					st->vertices[ind2].texcoord,
					st->vertices[ind0].texcoord,
					st->vertices[ind1].texcoord,
					st->vertices[ind2].normal);

				for (int w = 0; w < st->vertices[ind0].numWeights; w++)
				{
					st->vertices[ind0].weights[w].tangent += t[0];
					st->vertices[ind0].weights[w].binormal += b[0];
				}
				for (int w = 0; w < st->vertices[ind1].numWeights; w++)
				{
					st->vertices[ind1].weights[w].tangent += t[1];
					st->vertices[ind1].weights[w].binormal += b[1];
				}
				for (int w = 0; w < st->vertices[ind2].numWeights; w++)
				{
					st->vertices[ind2].weights[w].tangent += t[2];
					st->vertices[ind2].weights[w].binormal += b[2];
				}
			}

			for (unsigned int vLoop = 0; vLoop < st->numVertices; vLoop++)
			{
				for (int w = 0; w < st->vertices[vLoop].numWeights; w++)
				{
					if (!&st->vertices[vLoop])
						continue;

					st->vertices[vLoop].weights[w].tangent = Vec3::normalize(st->vertices[vLoop].weights[w].tangent);
					st->vertices[vLoop].weights[w].binormal = Vec3::normalize(st->vertices[vLoop].weights[w].binormal);
				}
			}
		}
		SetFrame(0.0);
	}

	/**
	*/
	void SkinnedMesh::CalculateBoundings() {
		//MT::ScopedGuard guard(mutex);

		SetFrame(0.0);
		for (uint32_t s = 0; s < numSubsets; s++)
		{
			auto st = subsets[s];
			if (!st)
			{
				Error("Invalid pointer", false);
				continue;
			}

			st->bBox = BBox(st->vertices[0].position, st->vertices[0].position);

			for (uint32_t v = 1; v < st->numVertices; v++) {
				if (!st) {
					Error("Invalid pointer", false);
					continue;
				}
				st->bBox.AddPoint(st->vertices[v].position);
			}

			st->bSphere = BSphere(st->bBox.GetCenter(), 0);

			for (uint32_t v = 0; v < st->numVertices; v++) {
				if (!st) {
					Error("Invalid pointer", false);
					continue;
				}
				st->bSphere.AddPoint(st->vertices[v].position);
			}

			if (s == 0) {
				bBox = st->bBox;
			}
			else
			{
				bBox.AddBBox(st->bBox);
			}
		}

		bSphere = BSphere(bBox.GetCenter(), ((bBox.maxes - bBox.mins) * 0.5f).length());
	}

	/**
	*/
	void SkinnedMesh::_CreateVBO() {
		//MT::ScopedGuard guard(mutex);

		auto _render = GetRender();
		ASSERT(_render, "Invalid pointer");

		for (uint32_t s = 0; s < numSubsets; s++)
		{
			auto st = subsets[s];
			if (!st)
			{
				Error("Invalid pointer", false);
				continue;
			}

			if (!st->mModelHelper)
				st->mModelHelper = _render->GetModelHelper();

			if (!st->mModelHelper)
			{
				Error("Invalid pointer", false);
				continue;
			}
			st->mModelHelper->LoadToRender(st->vertices, st->indices, sizeof(Vertex), st->numVertices, st->numIndices);
		}
	}
}