/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//**************************************
#include "CorePrivate.h"
//**************************************
#include "SkinnedMeshLoader.h"
#include "SkinnedMesh.h"
//**************************************

namespace NGTech
{
	/*
	*/
	SkinnedMeshLoader::SkinnedMeshLoader()
	{
	}

	/*
	*/
	SkinnedMeshLoader::~SkinnedMeshLoader()
	{
		for (auto & format : formats)
		{
			SAFE_DELETE(format);
		}
		formats.clear();
	}

	/*
	*/
	void SkinnedMeshLoader::RegisterFormat(I_MeshFormatSkinned *format) {
		formats.push_back(format);
	}

	/*
	*/
	void SkinnedMeshLoader::Save(const String &path, SkinnedMesh *mesh)
	{
		ASSERT(mesh, "Passed invalid pointer on mesh");
		//!@todo Implement this
	}

	/*
	*/
	bool SkinnedMeshLoader::Load(const String &path, SkinnedMesh *mesh)
	{
		ASSERT(mesh, "Passed invalid pointer on mesh");
		String ext = StringHelper::EMPTY_STRING;
		StringHelper::getExtension(ext, path);

		if (formats.empty()) {
			Error("NOT EXIST ANY SKINNEDMESH LOADER", true);
			return false;
		}

		for (auto & format : formats)
		{
			if (format == nullptr)
				continue;

			if (format->GetExt() == ext)
			{
				format->Load(path, mesh);
				return true;
			}
		}

		return false;
	}
}