/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//**************************************
#include "CorePrivate.h"
//**************************************
#include "MeshLoader.h"
#include "I_MeshFormat.h"
//**************************************

namespace NGTech
{
	/*
	*/
	MeshLoader::MeshLoader()
	{
	}

	/*
	*/
	MeshLoader::~MeshLoader()
	{
		for (auto & format : formats)
		{
			if (!format)
				continue;

			SAFE_DELETE(format);
		}
		formats.clear();
	}

	/*
	*/
	void MeshLoader::RegisterFormat(I_MeshFormat *format) {
		ASSERT(format, "Passed invalid pointer on format");
		if (format == nullptr) return;
		formats.push_back(format);
	}

	/*
	*/
	bool MeshLoader::Save(const String &path, Mesh *mesh) {
		ASSERT(mesh, "Passed invalid pointer on mesh");
		//!@todo Implement this

		return true;
	}

	/*
	*/
	bool MeshLoader::Load(const String &path, Mesh *mesh)
	{
		ASSERT(mesh, "Passed invalid pointer on mesh");
		String ext = StringHelper::EMPTY_STRING;
		StringHelper::getExtension(ext, path);

		if (formats.empty()) {
			Error("NOT EXIST ANY MESH LOADER", true);
			return false;
		}

		for (auto & format : formats)
		{
			if (format == nullptr)
				continue;

			if (format->GetExt() == ext)
			{
				format->Load(path, mesh);
				return true;
			}
		}

		Warning("MeshLoader::Load() error: %s format is not supported or not found file %s. ", ext.c_str(), path.c_str());

		return false;
	}
}