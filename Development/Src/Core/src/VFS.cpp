/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* File:    ALSystem.cpp
* Desc:    OpenAL sound system.
* Version: 1.0
* Author:  George Evmenov <altren@mygui.info> , Nick Galko <galek@vegaengine.com>
*
* This file is part of the NGTech (http://vegaengine.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://vegaengine.com/
*/
#include "CorePrivate.h"

#include <fstream>
#include <algorithm>

#include "../../Common/IDataManager.h"

#include "DataFileStream.h"
#include "FileSystemInfo/FileSystemInfo.h"
#include "VFS.h"
#include "UString.h"

namespace NGTech
{
	FileSystem::FileSystem() :
		mIsInitialise(false)
	{
	}

	void FileSystem::Initialise() {
		mIsInitialise = true;
	}

	void FileSystem::Shutdown() {
		mIsInitialise = false;
	}

	IDataStream* FileSystem::GetData(const String& _name) {
		String filepath = GetDataPath(_name);
		if (filepath.empty())
			return nullptr;

		std::ifstream* stream = new std::ifstream();
		stream->open(filepath.c_str(), std::ios_base::binary);

		if (!stream->is_open())
		{
			delete stream;
			return nullptr;
		}

		DataFileStream* data = new DataFileStream(stream);

		return data;
	}

	bool FileSystem::IsDataExist(const String& _name) {
		const VectorString& files = GetDataListNames(_name);
		return files.size() == 1;
	}

	//TODO:Rewrite
	const VectorString& FileSystem::GetDataListNames(const String& _pattern) {
		static VectorString result;
		VectorWString wresult;
		result.clear();

		for (auto item = mPaths.begin(); item != mPaths.end(); ++item) {
			scanFolder(wresult, (*item).name, (*item).recursive, NGTech::UString(_pattern).asWStr(), false);
		}

		for (VectorWString::const_iterator item = wresult.begin(); item != wresult.end(); ++item)
		{
			result.push_back(NGTech::UString(*item).asUTF8());
		}
		return result;
	}

	//TODO:Rewrite
	const String& FileSystem::GetDataPath(const String& _name) {
		static String path;
		VectorString result;
		VectorWString wresult;

		for (auto item = mPaths.begin(); item != mPaths.end(); ++item)
			scanFolder(wresult, (*item).name, (*item).recursive, NGTech::UString(_name).asWStr(), true);

		for (VectorWString::const_iterator item = wresult.begin(); item != wresult.end(); ++item)
			result.push_back(NGTech::UString(*item).asUTF8());

		//	path = result.size() == 1 ? result[0] : "";
		if (!result.empty())
		{
			path = result[0];
			if (result.size() > 1)
			{
				Warning("There are several files with name '", _name.c_str(), "'. '", path.c_str(), "' was used.");
				Warning("Other candidater are:");
				for (size_t index = 1; index < result.size(); index++)
					Warning(" - '", result[index].c_str(), "'");
			}
		}
		return path;
	}

	void FileSystem::AddResourceLocation(const String& _name, bool _recursive) {
		ArchiveInfo info;
		info.name = NGTech::UString(_name).asWStr();
		info.recursive = _recursive;

		bool found = false;

		// Don't add duplicates to the list - std::find not works:(
		for (auto it = mPaths.begin(); it != mPaths.end(); it++)
		{
			if (it->name == info.name)
			{
				found = true;
				return;
			}
		}

		if (!found)
			mPaths.push_back(info);
	}

	void FileSystem::FreeData(IDataStream* _data) {
		ASSERT(_data, "Invalid pointer on _data");
		delete _data;
	}
} // namespace NGTech