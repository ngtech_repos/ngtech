/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "CorePrivate.h"
//***************************************************************************
#include <fstream>      // std::ifstream, std::ofstream
#include <vector>
#include <future>		// std::async
//***************************************************************************

namespace NGTech {
	bool CORE_API saveBinary
	(const char* _file,
		std::vector<unsigned char> _Data
	)
	{
		DebugM("saveBinary %s", _file);
		std::async(std::launch::async, [](const char* _filename, std::vector<unsigned char> Data)
		{
			try
			{
				std::ofstream outfile(_filename, std::ios::binary);
				if (!outfile.is_open())
					return false;

				// write to outfile
				outfile.write((char *)&Data[0], Data.size());
				outfile.close();
				return true;
			}
			catch (std::exception&e)
			{
				Warning(e.what());
				return false;
			}
			return false;
		}, _file, _Data);

		return true;
	}

	bool CORE_API saveBinary
	(const char* _file,
		unsigned int const _Format,
		std::vector<unsigned char> _Data,
		int const _Size
	)
	{
		DebugM("saveBinary %s", _file);

		std::async(std::launch::async, [](const char* _p, unsigned int const Format, std::vector<unsigned char> Data, int const Size)
		{
			try
			{
				std::ofstream outfile(_p, std::ios::binary);

				if (!outfile.is_open())
					return false;

				// write to outfile
				outfile.write((char *)&Format, sizeof(unsigned int));
				outfile.write((char *)&Size, sizeof(Size));
				outfile.write((char *)&Data[0], Size);

				outfile.close();
				return true;
			}
			catch (std::exception&e)
			{
				Warning(e.what());
				return false;
			}
			return false;
		}, _file, _Format, _Data, _Size);

		return true;
	}

	bool CORE_API loadBinary
	(const char* _file,
		unsigned int & Format,
		std::vector<unsigned char>& Data,
		int & Size)
	{
		DebugM("loadBinary with format %s", _file);

		std::basic_ifstream<unsigned char>fileStream(_file, std::ios::binary);
		if (!fileStream.is_open())
			return false;

		fileStream.read((unsigned char*)&Format, sizeof(unsigned int));
		fileStream.read((unsigned char*)&Size, Size);
		Data.resize(Size);
		fileStream.read(&Data[0], Size);
		fileStream.close();

		return true;
	}

	bool CORE_API loadBinary
	(const char* _file,
		std::vector<unsigned char>& Data,
		int & Size)
	{
		DebugM("loadBinary %s", _file);

		std::basic_ifstream<unsigned char> fileStream(_file, std::ios::binary);
		if (!fileStream.is_open())
			return false;

		fileStream.seekg(0, std::ios_base::end);
		Size = fileStream.tellg();
		fileStream.seekg(0, std::ios_base::beg);
		Data.resize(Size);
		fileStream.read(&Data[0], Size);
		fileStream.close();

		return true;
	}

	bool CORE_API deleteBinary(const char* _file)
	{
		if (remove(_file) != 0)
			return false; // Error

		return true;
	}
}