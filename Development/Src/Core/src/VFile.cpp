/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//***************************************************************************
#include "CorePrivate.h"
//***************************************************************************
#include <iostream>
#include <fstream>
#include <stdarg.h>
//***************************************************************************
#include "string.h"//memset for gcc
//***************************************************************************
#include "VFS.h"
#include "FileHelper.h"
//***************************************************************************
#if PLATFORM_OS == PLATFORM_OS_WINDOWS
#include  <io.h>
#include  <stdio.h>
#include  <stdlib.h>
#else
#include <unistd.h>
#endif
//***************************************************************************

namespace NGTech
{
	/**
	*/
	VFile::VFile(const String & _name, FILE_MODE _mode, bool _notSearch)
		:mName(_name), mCurrentPos(0), mSize(0), mFile(nullptr)
	{
		useSearch = true;
		memoryBuffer.clear();
		_OpenFile(_name, _mode, _notSearch);
	}

	/**
	*/
	VFile::~VFile() {
		Close();
		mSize = 0;
		mCurrentPos = 0;
	}

	/**
	*/
	bool VFile::IsDataExist(bool _warn) {
		auto _vfs = GetVFS();
		ASSERT(_vfs, "Invalid pointer on VFS");
		if (_vfs == nullptr)
			return false;

		bool status = _vfs->IsDataExist(mName);
		if ((!status) && _warn)
			Warning("File %s :is not exist", mName.c_str());

		return status;
	}

	/**
	*/
	bool VFile::IsDataExist(const char* _name, bool _warn) {
		auto _vfs = GetVFS();
		ASSERT(_vfs, "Invalid pointer on VFS");
		if (_vfs == nullptr)
			return false;

		bool status = _vfs->IsDataExist(_name);
		if ((!status) && _warn)
			Warning("File %s :is not exist", _name);

		return status;
	}

	/**
	*/
	char* VFile::LoadFile()
	{
		if ((!IsDataExist()) || (!mFile))
			return nullptr;

		//PROFILER_START(VFile::LoadFile);

		// obtain file size:
		fseek(mFile, 0, SEEK_END);
		mSize = ftell(mFile);
		rewind(mFile);

		// allocate memory to contain the whole file:
		memoryBuffer.resize(mSize);
		fseek(mFile, 0, SEEK_SET);
		fread(&memoryBuffer[0], sizeof(char), mSize, mFile);
		memoryBuffer.push_back('\0');

#if HEAVY_DEBUG
		Warning(memoryBuffer.data());
#endif

		//PROFILER_END();

		return memoryBuffer.data();
	}

	/**
	*/
	size_t VFile::FTell() {
		if (mFile)
			return ftell(mFile);

		return (size_t)-1;
	}

	/**
	*/
	size_t VFile::FSeek(long offset, int mode)
	{
		if (mFile)
			return fseek(mFile, offset, mode);
		return 1;
	}

	/**
	*/
	void VFile::Read(void *buf, int size, int count)
	{
		ReadAsSizeT(buf, size, count);
	}

	size_t VFile::ReadAsSizeT(void *buf, int size, int count)
	{
		size_t _size = 0;

		if (mFile)
			_size = fread(buf, size, count, mFile);

		if (!memoryBuffer.empty())
		{
			size_t bytes = size * count;
			auto ptr = (memoryBuffer[0]) + mCurrentPos;
			size_t bytes_to_end = size - ptr;

			if (bytes <= bytes_to_end) {
				memcpy(buf, &ptr, bytes);
				mCurrentPos += bytes;
				_size = mCurrentPos;
				return _size;
			}
			else {
				memcpy(buf, &ptr, bytes_to_end);
				ptr += bytes_to_end;
				_size = bytes_to_end / size;
				return _size;
			}
		}
		return _size;
	}

	/**
	*/
	bool VFile::IsEof() {
		if (mFile)
			return feof(mFile) != 0;

		if (!memoryBuffer.empty())
			return (mCurrentPos > mSize);

		return true;
	}

	/**
	*/
	void VFile::Write(void *buf, int size, int count)
	{
		if (mFile)
		{
			fwrite(buf, size, count, mFile);
		}

		if (!memoryBuffer.empty())
		{
			memcpy((char*)(&memoryBuffer[0]) + mCurrentPos, (char*)buf, size*count);
			mCurrentPos += size * count;
		}
	}

	/**
	*/
	const char*  VFile::GetDataPath() {
		auto _vfs = GetVFS();
		ASSERT(_vfs, "Invalid pointer on VFS");
		if (_vfs == nullptr)
			return nullptr;

		return _vfs->GetDataPath(mName).c_str();
	}

	/**
	*/
	size_t VFile::Size()
	{
		if (mSize == 0)
		{
			DebugM("[FileSystem] loading file with zero size.All is correctly? Filename: %s", mName.c_str());
			LoadFile();
		}

		return mSize;
	}

	/**
	*/
	String VFile::GetLine()
	{
		if (!mFile) return "";
		if (IsEof()) return "";

		String output = "";
		unsigned char h = fgetc(mFile);

		while ((h != '\n') && (!feof(mFile))) {
			output += h;
			h = fgetc(mFile);
		}
		return output;
	}

	/**
	*/
	void VFile::WriteString(const String &text) {
		if (mFile)
			fprintf(mFile, "%s\n", text.c_str());
	}

	/**
	*/
	void VFile::_OpenFile(const String&path, FILE_MODE _mode, bool _notSearch)
	{
		useSearch = !_notSearch;
		if (_notSearch) {
			_FileOpen(path, _mode);
		}
		else {
			if (!IsDataExist())
				return;

			_FileOpen(GetDataPath(), _mode);
		}
	}

	void VFile::_FileOpen(const String&path, FILE_MODE _mode)
	{
		if (_mode == FILE_MODE::READ_TEXT)
			mFile = fopen(path.c_str(), "rt");
		else if (_mode == FILE_MODE::READ_BIN)
			mFile = fopen(path.c_str(), "rb");
		else if (_mode == FILE_MODE::WRITE_BIN)
			mFile = fopen(path.c_str(), "wb");
		else
			Warning("[VFile::_FileOpen] incorrect FILE_MODE");
	}

	/**
	*/
	void VFile::ScanF(const char *format, ...)
	{
		if (mFile)
		{
			va_list ap;
			va_start(ap, format);
			vfscanf(mFile, format, ap);
			va_end(ap);
		}
	}

	/**
	*/
	int VFile::Close()
	{
		if (mFile)
			return fclose(mFile);

		return 1;
	}

	/**
	*/
	bool VFile::HasAccess()
	{
		return !FileUtil::FileExist(mName);
	}
}