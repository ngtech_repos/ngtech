/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//***************************************************************************
#include "CorePrivate.h"
//***************************************************************************
#include <iostream>
#include <fstream>
#include <stdarg.h>
//***************************************************************************
#include "string.h"//memset for gcc
//***************************************************************************
#include "VFS.h"
#include "FileHelper.h"
//***************************************************************************
#if PLATFORM_OS == PLATFORM_OS_WINDOWS
#include  <io.h>
#include  <stdio.h>
#include  <stdlib.h>
#else
#include  <unistd.h>
#endif
//***************************************************************************

namespace NGTech
{
	/**
	*/
	VFileStream::VFileStream(const String & _name, FILE_MODE _mode, bool _notSearch, bool _stillOpened)
		:mName(_name), mCurrentPos(0), mSize(0), mode(_mode),
		stillOpened(_stillOpened), useSearch(!_notSearch)
	{
		memoryBuffer.clear();
		_OpenFileStream(mName, mode, useSearch);
		Close();
	}

	/**
	*/
	VFileStream::~VFileStream() {
		Close();
		mSize = 0;
		mCurrentPos = 0;
	}

	/**
	*/
	bool VFileStream::IsDataExist(bool _warn) {
		auto _vfs = GetVFS();
		ASSERT(_vfs, "Invalid pointer on _vfs");
		if (!_vfs)
			return false;

		bool status = _vfs->IsDataExist(mName);
		if ((!status) && _warn)
			Warning("File %s :is not exist", mName.c_str());
		return status;
	}

	/**
	*/
	bool VFileStream::IsDataExist(const char* _name, bool _warn) {
		auto _vfs = GetVFS();
		ASSERT(_vfs, "Invalid pointer on _vfs");
		if (!_vfs)
			return false;

		bool status = _vfs->IsDataExist(_name);
		if ((!status) && _warn)
			Warning("File %s :is not exist", _name);

		return status;
	}

	/**
	*/
	char* VFileStream::LoadFile()
	{
		if (!IsDataExist()) {
			Warning("[%s] IsDataExist %s", __FUNCTION__, mName.c_str());
			return nullptr;
		}

		bool res = _OpenFileStream(mName, mode, useSearch);
		if (!res) {
			Warning("[%s] !fileStream %s", __FUNCTION__, mName.c_str());
			return nullptr;
		}

		if (!fileStream.eof() && !fileStream.fail())
		{
			fileStream.seekg(0, std::ios_base::end);
			std::streampos fileSize = fileStream.tellg();
			memoryBuffer.resize(fileSize);

			mSize = fileSize;

			fileStream.seekg(0, std::ios_base::beg);
			fileStream.read(&memoryBuffer[0], fileSize);
			memoryBuffer.push_back('\0');
		}

		if (!stillOpened)
			Close();

		return memoryBuffer.data();
	}

	/**
	*/
	size_t VFileStream::FTell() {
		if (!fileStream.is_open())
			return (size_t)-1;

		return fileStream.tellg();
	}

	/**
	*/
	size_t VFileStream::Seek(long offset)
	{
		if (!fileStream.is_open())
			return 1;

		fileStream.seekg(offset, std::ios::beg);

		return 0;
	}

	/**
	*/
	void VFileStream::Read(void *buf, int size, int count)
	{
		if (!fileStream.is_open()) {
			if (!_OpenFileStream(mName, mode, useSearch)) {
				return;
			}
		}
		else {
			fileStream.read((char*)buf, sizeof(buf)*count);
		}

		if (!memoryBuffer.empty())
		{
			memcpy((char*)buf, (char*)(&memoryBuffer[0]) + mCurrentPos, size*count);
			mCurrentPos += size * count;
		}

		if (!stillOpened)
			Close();
	}

	/**
	*/
	bool VFileStream::IsEof()
	{
		if (!memoryBuffer.empty())
			return (mCurrentPos > mSize);
		else
		{
			return fileStream.eof() != 0;
		}
	}

	/**
	*/
	void VFileStream::Write(void *buf, int size, int count)
	{
		bool res = _OpenFileStream(mName, mode, useSearch);
		if (!res) {
			Warning("[%s] !fileStream %s", __FUNCTION__, mName.c_str());
			return;
		}

		if (IsValid()) {
			fileOStream.width(count);
			fileOStream << buf;
		}

		Close();

		if (!memoryBuffer.empty())
		{
			memcpy((char*)(&memoryBuffer[0]) + mCurrentPos, (char*)buf, size*count);
			mCurrentPos += size * count;
		}
	}

	/**
	*/
	const char*  VFileStream::GetDataPath() {
		auto _vfs = GetVFS();
		ASSERT(_vfs, "Invalid pointer on _vfs");
		if (!_vfs)
			return nullptr;

		return _vfs->GetDataPath(mName).c_str();
	}

	/**
	*/
	size_t VFileStream::Size()
	{
		if (mSize == 0)
		{
			DebugM("[FileSystem] loading file with zero size.All is correctly? Filename: %s", mName.c_str());
			LoadFile();
		}

		return mSize;
	}

	/**
	*/
	String VFileStream::GetLine()
	{
		if (!IsValid()) return "";

		if (!fileStream.is_open())
			_OpenFileStream(mName, mode, useSearch);

		String line;
		std::getline(fileStream, line);

		return line;
	}

	/**
	*/
	String VFileStream::GetFileExt() {
		if (mName.size() == 0)
			return "UNKNOWN_EXT";

		int i = (int)mName.size() - 1;
		String buf;

		while ((mName[i] != '.') && (i > 0)) {
			buf = mName[i] + buf;
			i--;
		}
		return buf;
	}

	/**
	*/
	void VFileStream::WriteString(const String &text) {
		bool res = _OpenFileStream(mName, mode, useSearch);
		if (!res) {
			Warning("[%s] !fileStream %s", __FUNCTION__, mName.c_str());
			return;
		}

		fileOStream << text << "\n";

		Close();
	}

	/**
	*/
	String VFileStream::CutFileExt() {
		if (mName.size() == 0)
			return "UNKNOWN_EXT";

		String buf;

		int i = 0;
		while ((mName[i] != '.') && (i < (int)mName.size())) {
			buf += mName[i];
			i++;
		}
		return buf;
	}

	/**
	*/
	bool VFileStream::_OpenFileStream(const String&path, FILE_MODE _mode, bool _Search, bool _stillOpened)
	{
		useSearch = _Search;
		stillOpened = _stillOpened;

		if (!useSearch)
		{
			return _StreamOpen(path, _mode);
		}
		else {
			if (!IsDataExist())
				return false;

			return _StreamOpen(GetDataPath(), _mode);
		}
	}

	bool VFileStream::_StreamOpen(const String&_path, FILE_MODE _mode)
	{
		bool result = false;
		try {
			if (_mode == FILE_MODE::READ_TEXT) {
				fileStream.open(_path.c_str(), std::ifstream::in);
				result = fileStream.is_open();
			}
			else if (_mode == FILE_MODE::READ_BIN) {
				fileStream.open(_path.c_str(), std::ifstream::binary);
				result = fileStream.is_open();
			}
			else if (_mode == FILE_MODE::WRITE_BIN) {
				fileOStream.open(_path, std::ios::out | std::ios::app | std::ios::binary);
				result = fileOStream.is_open();
			}
			else if (_mode == FILE_MODE::WRITE_TEXT) {
				fileOStream.open(_path, std::ios::out | std::ios::app);
				result = fileOStream.is_open();
			}
			else
				Warning("[VFile::_StreamOpen] incorrect FILE_MODE");

			if (result == false)
			{
				Warning("%s", strerror(errno));
			}
		}
#if IS_OS_ANDROID
		catch (std::exception& e) {
			ErrorWrite("%s %s", e.what(), "\n");
		}
#else
		catch (std::ios_base::failure& e) {
			if (e.code() == std::make_error_condition(std::io_errc::stream))
				ErrorWrite("%s", e.what());
			else
				ErrorWrite("Unknown failure opening file.");
		}
#endif

		return result;
	}

	/**
	*/
	void VFileStream::ScanF(const char *format, ...)
	{
		Warning(__FUNCTION__);
		//!@todo IMPLEMENT VFileStream::ScanF
		/*if (mFile)
		{
			va_list ap;
			va_start(ap, format);
			vfscanf(mFile, format, ap);
			va_end(ap);
		}*/
	}

	/**
	*/
	int VFileStream::Close()
	{
		if (!fileStream.is_open())
			return 1;

		if (mode < FILE_MODE::WRITE_BIN)
			fileStream.close();
		else
			fileOStream.close();

		return 0;
	}

	VFileStream::VFileStream()
	{
		//prepare fileStream to throw if failbit gets set
		std::ios_base::iostate exceptionMask = fileStream.exceptions() | std::ios::failbit;
		fileStream.exceptions(exceptionMask);

		//prepare fileOStream to throw if failbit gets set
		exceptionMask = fileOStream.exceptions() | std::ios::failbit;
		fileOStream.exceptions(exceptionMask);
	}

	/**
	*/
	//bool VFileStream::HasAccess()
	//{
	//	return !FileUtil::fileExist(mName);
	//}
}