/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "CorePrivate.h"

namespace NGTech
{
	/**/
	void WriteCallStack();

	/**/
	static ENGINE_INLINE assert_action assert_callback(const char* cond, const char* msg, const char* file, unsigned int line, void* /*user_data*/)
	{
		String pExpression = "assert fail at: %s( %u ): %s -> \"%s\"\n";
		pExpression = StringHelper::StickString(pExpression.c_str(), file, line, cond, msg);
		// Write error to log
		{
			ErrorWrite(pExpression.c_str());
			WriteCallStack();
		}
#if IS_OS_WINDOWS
		int result = MessageBox(NULL, pExpression.c_str(), "Assertion Failed", MB_SYSTEMMODAL | MB_ABORTRETRYIGNORE);

		if (result == IDIGNORE)
		{
			// ... skip all asserts ...
			return ASSERT_ACTION_NONE;
		}
		else
			// ... Activate debugger ...
			return ASSERT_ACTION_BREAK;
#else
#if _ENGINE_DEBUG_
		// ... Activate debugger ...
		return ASSERT_ACTION_BREAK;
#else
		// ... skip all asserts ...
		return ASSERT_ACTION_NONE;
#endif
#endif
	}

	/**/
	void InitDbgTools()
	{
		assert_register_callback(assert_callback, 0x0);
	}
}