/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "CorePrivate.h"

#include "dds.h"

#include <cstdio>

namespace NGTech
{
	namespace ddsShared
	{
		unsigned int GetImageSize(unsigned int width, unsigned int height, unsigned int bytes, unsigned int miplevels)
		{
			unsigned int w = width;
			unsigned int h = height;
			unsigned int bytesize = 0;

			for (unsigned int i = 0; i < miplevels; ++i)
			{
				bytesize += Math::Max<unsigned int>(1, w) * Math::Max<unsigned int>(1, h) * bytes;

				w = Math::Max<unsigned int>(w / 2, 1);
				h = Math::Max<unsigned int>(h / 2, 1);
			}

			return bytesize;
		}

		unsigned int GetCompressedImageSize(unsigned int width, unsigned int height, unsigned int miplevels, const Format& format)
		{
			unsigned int w = width;
			unsigned int h = height;
			unsigned int bytesize = 0;

			if (format == Format::GLFMT_DXT1 || format == Format::GLFMT_DXT5)
			{
				unsigned int mult = ((format == Format::GLFMT_DXT5) ? 16 : 8);

				if (w != h)
				{
					for (unsigned int i = 0; i < miplevels; ++i)
					{
						bytesize += Math::Max<unsigned int>(1, w / 4) * Math::Max<unsigned int>(1, h / 4) * mult; //-V112

						w = Math::Max<unsigned int>(w / 2, 1);
						h = Math::Max<unsigned int>(h / 2, 1);
					}
				}
				else
				{
					bytesize = ((w / 4) * (h / 4) * mult); //-V112
					w = bytesize;

					for (unsigned int i = 1; i < miplevels; ++i)
					{
						w = Math::Max(mult, w / 4); //-V112
						bytesize += w;
					}
				}
			}

			return bytesize;
		}

		unsigned int GetCompressedLevelSize(unsigned int width, unsigned int height, unsigned int level, const Format& format)
		{
			unsigned int w = width;
			unsigned int h = height;
			unsigned int bytesize = 0;

			if (format == Format::GLFMT_DXT1 || format == Format::GLFMT_DXT5)
			{
				unsigned int mult = ((format == Format::GLFMT_DXT5) ? 16 : 8);

				if (w != h)
				{
					w = Math::Max<unsigned int>(w / (1 << level), 1);
					h = Math::Max<unsigned int>(h / (1 << level), 1);

					bytesize = Math::Max<unsigned int>(1, w / 4) * Math::Max<unsigned int>(1, h / 4) * mult; //-V112
				}
				else
				{
					bytesize = ((w / 4) * (h / 4) * mult); //-V112

					if (level > 0)
					{
						w = bytesize;

						for (unsigned int i = 0; i < level; ++i)
							w = Math::Max<unsigned int>(mult, w / 4); //-V112

						bytesize = w;
					}
				}
			}

			return bytesize;
		}

		unsigned int GetCompressedImageSize(unsigned int width, unsigned int height, unsigned int depth, unsigned int miplevels, const Format& format)
		{
			unsigned int w = width;
			unsigned int h = height;
			unsigned int d = depth;
			unsigned int bytesize = 0;

			if (format == Format::GLFMT_DXT1 || format == Format::GLFMT_DXT5)
			{
				unsigned int mult = ((format == Format::GLFMT_DXT5) ? 16 : 8);

				for (unsigned int i = 0; i < miplevels; ++i)
				{
					bytesize += Math::Max<unsigned int>(1, w / 4) * Math::Max<unsigned int>(1, h / 4) * d * mult; //-V112

					w = Math::Max<unsigned int>(w / 2, 1);
					h = Math::Max<unsigned int>(h / 2, 1);
				}
			}

			return bytesize;
		}

		unsigned int NextPow2(unsigned int x)
		{
			--x;

			x |= x >> 1;
			x |= x >> 2;
			x |= x >> 4;
			x |= x >> 8;
			x |= x >> 16;

			return ++x;
		}
	}
	using namespace ddsShared;

#define DWORD							unsigned int
#define WORD							unsigned short
#define BYTE							unsigned char
#define UINT							unsigned int

#define DDS_MAGIC						0x20534444
#define DDPF_FOURCC						0x00000004
#define DDPF_RGB						0x40
#define DDPF_RGBA						0x41

#define DDSD_CAPS						0x1
#define DDSD_HEIGHT						0x2
#define DDSD_WIDTH						0x4
#define DDSD_PITCH						0x8
#define DDSD_PIXELFORMAT				0x1000
#define DDSD_MIPMAPCOUNT				0x20000
#define DDSD_LINEARSIZE					0x80000
#define DDSD_DEPTH						0x800000

#define DDSCAPS_COMPLEX					0x8
#define DDSCAPS_MIPMAP					0x400000
#define DDSCAPS_TEXTURE					0x1000

#define DDSCAPS2_CUBEMAP				0x200
#define DDSCAPS2_CUBEMAP_POSITIVEX		0x400
#define DDSCAPS2_VOLUME					0x200000

#ifndef MAKEFOURCC
#	define MAKEFOURCC(ch0, ch1, ch2, ch3) \
		((DWORD)(BYTE)(ch0)|((DWORD)(BYTE)(ch1) << 8)| \
		((DWORD)(BYTE)(ch2) << 16)|((DWORD)(BYTE)(ch3) << 24))
#endif

#define DDSD_CAPS						0x1
#define DDSD_HEIGHT						0x2
#define DDSD_WIDTH						0x4
#define DDSD_PITCH						0x8
#define DDSD_PIXELFORMAT				0x1000
#define DDSD_MIPMAPCOUNT				0x20000
#define DDSD_LINEARSIZE					0x80000
#define DDSD_DEPTH						0x800000

	struct DDS_PIXELFORMAT
	{
		DWORD dwSize;
		DWORD dwFlags;
		DWORD dwFourCC;
		DWORD dwRGBBitCount;
		DWORD dwRBitMask;
		DWORD dwGBitMask;
		DWORD dwBBitMask;
		DWORD dwABitMask;
	};

	const DDS_PIXELFORMAT DDSPF_DXT1 =
	{
		sizeof(DDS_PIXELFORMAT),
		DDPF_FOURCC,
		MAKEFOURCC('D','X','T','1'),
		0, 0, 0, 0, 0
	};

	const DDS_PIXELFORMAT DDSPF_DXT2 =
	{
		sizeof(DDS_PIXELFORMAT),
		DDPF_FOURCC,
		MAKEFOURCC('D','X','T','2'),
		0, 0, 0, 0, 0
	};

	const DDS_PIXELFORMAT DDSPF_DXT3 =
	{
		sizeof(DDS_PIXELFORMAT),
		DDPF_FOURCC,
		MAKEFOURCC('D','X','T','3'),
		0, 0, 0, 0, 0
	};

	const DDS_PIXELFORMAT DDSPF_DXT4 =
	{
		sizeof(DDS_PIXELFORMAT),
		DDPF_FOURCC,
		MAKEFOURCC('D','X','T','4'),
		0, 0, 0, 0, 0
	};

	const DDS_PIXELFORMAT DDSPF_DXT5 =
	{
		sizeof(DDS_PIXELFORMAT),
		DDPF_FOURCC,
		MAKEFOURCC('D','X','T','5'),
		0, 0, 0, 0, 0
	};

	const DDS_PIXELFORMAT DDSPF_A8R8G8B8 =
	{
		sizeof(DDS_PIXELFORMAT),
		DDPF_RGBA, 0, 32,
		0x00ff0000,
		0x0000ff00,
		0x000000ff,
		0xff000000
	};

	const DDS_PIXELFORMAT DDSPF_A1R5G5B5 =
	{
		sizeof(DDS_PIXELFORMAT),
		DDPF_RGBA, 0, 16,
		0x00007c00,
		0x000003e0,
		0x0000001f,
		0x00008000
	};

	const DDS_PIXELFORMAT DDSPF_A4R4G4B4 =
	{
		sizeof(DDS_PIXELFORMAT),
		DDPF_RGBA, 0, 16,
		0x00000f00,
		0x000000f0,
		0x0000000f,
		0x0000f000
	};

	const DDS_PIXELFORMAT DDSPF_R8G8B8 =
	{
		sizeof(DDS_PIXELFORMAT),
		DDPF_RGB, 0, 24,
		0x00ff0000,
		0x0000ff00,
		0x000000ff,
		0x00000000
	};

	const DDS_PIXELFORMAT DDSPF_R5G6B5 =
	{
		sizeof(DDS_PIXELFORMAT),
		DDPF_RGB, 0, 16,
		0x0000f800,
		0x000007e0,
		0x0000001f,
		0x00000000
	};

	const DDS_PIXELFORMAT DDSPF_DX10 =
	{
		sizeof(DDS_PIXELFORMAT),
		DDPF_FOURCC,
		MAKEFOURCC('D','X','1','0'),
		0, 0, 0, 0, 0
	};

	struct DDS_HEADER
	{
		DWORD dwSize;
		DWORD dwHeaderFlags;
		DWORD dwHeight;
		DWORD dwWidth;
		DWORD dwPitchOrLinearSize;
		DWORD dwDepth;
		DWORD dwMipMapCount;
		DWORD dwReserved1[11];

		DDS_PIXELFORMAT ddspf;

		DWORD dwCaps;
		DWORD dwCaps2;
		DWORD dwCaps3;
		DWORD dwCaps4;
		DWORD dwReserved2;
	};

	static DWORD PackRGBA_DXT1(BYTE r, BYTE g, BYTE b, BYTE a) {
		return ((a << 24) | (b << 16) | (g << 8) | r);
	}

	static DWORD PackRGBA_DXT5(BYTE r, BYTE g, BYTE b, BYTE a) {
		return ((a << 24) | (b << 16) | (g << 8) | r);
	}

	static void DecompressBlockDXT1(DWORD x, DWORD y, DWORD width, BYTE* blockStorage, DWORD* image)
	{
		WORD color0 = *(WORD*)(blockStorage);
		WORD color1 = *(WORD*)(blockStorage + 2);
		DWORD temp;

		temp = (color0 >> 11) * 255 + 16;
		BYTE r0 = (BYTE)((temp / 32 + temp) / 32);

		temp = ((color0 & 0x07e0) >> 5) * 255 + 32;
		BYTE g0 = (BYTE)((temp / 64 + temp) / 64);

		temp = (color0 & 0x001f) * 255 + 16;
		BYTE b0 = (BYTE)((temp / 32 + temp) / 32);

		temp = (color1 >> 11) * 255 + 16;
		BYTE r1 = (BYTE)((temp / 32 + temp) / 32);

		temp = ((color1 & 0x07e0) >> 5) * 255 + 32;
		BYTE g1 = (BYTE)((temp / 64 + temp) / 64);

		temp = (color1 & 0x001f) * 255 + 16;
		BYTE b1 = (BYTE)((temp / 32 + temp) / 32);

		DWORD code = *(DWORD*)(blockStorage + 4);

		for (int j = 0; j < 4; ++j)
		{
			for (int i = 0; i < 4; ++i)
			{
				DWORD finalColor = 0;
				BYTE positionCode = (code >> 2 * (4 * j + i)) & 0x03;

				if (color0 > color1)
				{
					switch (positionCode)
					{
					case 0:
						finalColor = PackRGBA_DXT1(r0, g0, b0, 255);
						break;

					case 1:
						finalColor = PackRGBA_DXT1(r1, g1, b1, 255);
						break;

					case 2:
						finalColor = PackRGBA_DXT1((2 * r0 + r1) / 3, (2 * g0 + g1) / 3, (2 * b0 + b1) / 3, 255);
						break;

					case 3:
						finalColor = PackRGBA_DXT1((r0 + 2 * r1) / 3, (g0 + 2 * g1) / 3, (b0 + 2 * b1) / 3, 255);
						break;
					}
				}
				else
				{
					switch (positionCode)
					{
					case 0:
						finalColor = PackRGBA_DXT1(r0, g0, b0, 255);
						break;

					case 1:
						finalColor = PackRGBA_DXT1(r1, g1, b1, 255);
						break;

					case 2:
						finalColor = PackRGBA_DXT1((r0 + r1) / 2, (g0 + g1) / 2, (b0 + b1) / 2, 255);
						break;

					case 3:
						finalColor = PackRGBA_DXT1(0, 0, 0, 255);
						break;
					}
				}

				if ((x + i) < width)
					image[(y + j) * width + (x + i)] = finalColor;
			}
		}
	}

	static void DecompressBlockDXT5(DWORD x, DWORD y, DWORD width, BYTE* blockStorage, DWORD* image)
	{
		BYTE alpha0 = *(BYTE*)(blockStorage);
		BYTE alpha1 = *(BYTE*)(blockStorage + 1);

		BYTE* bits = blockStorage + 2;

		DWORD alphaCode1 = bits[2] | (bits[3] << 8) | (bits[4] << 16) | (bits[5] << 24);
		WORD alphaCode2 = bits[0] | (bits[1] << 8);

		WORD color0 = *(WORD*)(blockStorage + 8);
		WORD color1 = *(WORD*)(blockStorage + 10);

		DWORD temp;

		temp = (color0 >> 11) * 255 + 16;
		BYTE r0 = (BYTE)((temp / 32 + temp) / 32);

		temp = ((color0 & 0x07e0) >> 5) * 255 + 32;
		BYTE g0 = (BYTE)((temp / 64 + temp) / 64);

		temp = (color0 & 0x001f) * 255 + 16;
		BYTE b0 = (BYTE)((temp / 32 + temp) / 32);

		temp = (color1 >> 11) * 255 + 16;
		BYTE r1 = (BYTE)((temp / 32 + temp) / 32);

		temp = ((color1 & 0x07e0) >> 5) * 255 + 32;
		BYTE g1 = (BYTE)((temp / 64 + temp) / 64);

		temp = (color1 & 0x001f) * 255 + 16;
		BYTE b1 = (BYTE)((temp / 32 + temp) / 32);

		DWORD code = *(DWORD*)(blockStorage + 12);

		for (int j = 0; j < 4; ++j) //-V112
		{
			for (int i = 0; i < 4; ++i) //-V112
			{
				int alphaCodeIndex = 3 * (4 * j + i); //-V112
				int alphaCode;

				if (alphaCodeIndex <= 12)
				{
					alphaCode = (alphaCode2 >> alphaCodeIndex) & 0x07;
				}
				else if (alphaCodeIndex == 15)
				{
					alphaCode = (alphaCode2 >> 15) | ((alphaCode1 << 1) & 0x06);
				}
				else
				{
					alphaCode = (alphaCode1 >> (alphaCodeIndex - 16)) & 0x07;
				}

				BYTE finalAlpha;

				if (alphaCode == 0)
					finalAlpha = alpha0;
				else if (alphaCode == 1)
					finalAlpha = alpha1;
				else
				{
					if (alpha0 > alpha1)
						finalAlpha = ((8 - alphaCode) * alpha0 + (alphaCode - 1) * alpha1) / 7;
					else
					{
						if (alphaCode == 6)
							finalAlpha = 0;
						else if (alphaCode == 7)
							finalAlpha = 255;
						else
							finalAlpha = ((6 - alphaCode) * alpha0 + (alphaCode - 1) * alpha1) / 5;
					}
				}

				BYTE colorCode = (code >> 2 * (4 * j + i)) & 0x03; //-V112
				DWORD finalColor = 0;

				switch (colorCode)
				{
				case 0:
					finalColor = PackRGBA_DXT5(r0, g0, b0, finalAlpha);
					break;

				case 1:
					finalColor = PackRGBA_DXT5(r1, g1, b1, finalAlpha);
					break;

				case 2:
					finalColor = PackRGBA_DXT5((2 * r0 + r1) / 3, (2 * g0 + g1) / 3, (2 * b0 + b1) / 3, finalAlpha);
					break;

				case 3:
					finalColor = PackRGBA_DXT5((r0 + 2 * r1) / 3, (g0 + 2 * g1) / 3, (b0 + 2 * b1) / 3, finalAlpha);
					break;
				}

				if (x + i < width)
					image[(y + j) * width + (x + i)] = finalColor;
			}
		}
	}

	void BlockDecompressImageDXT1(DWORD width, DWORD height, BYTE* in, DWORD* out)
	{
		DWORD blockCountX = (width + 3) / 4;
		DWORD blockCountY = (height + 3) / 4;

		for (DWORD j = 0; j < blockCountY; ++j)
		{
			for (DWORD i = 0; i < blockCountX; ++i)
				DecompressBlockDXT1(i * 4, j * 4, width, in + i * 8, out);

			in += blockCountX * 8;
		}
	}

	void BlockDecompressImageDXT5(DWORD width, DWORD height, BYTE* in, DWORD* out)
	{
		DWORD blockCountX = (width + 3) / 4;
		DWORD blockCountY = (height + 3) / 4;

		for (DWORD j = 0; j < blockCountY; ++j)
		{
			for (DWORD i = 0; i < blockCountX; ++i)
				DecompressBlockDXT5(i * 4, j * 4, width, in + i * 16, out);

			in += blockCountX * 16;
		}
	}

	bool LoadFromDDS(const char* _file, DDS_Image_Info* outinfo)
	{
		DDS_HEADER	header;
		DWORD		magic;
		UINT		bytesize = 0;

		ASSERT(outinfo, "Invalid poitner on imageInfo");

		if (!outinfo)
			return false;

		TODO("PORT ON STREAM");
		VFile file(_file);

		if (!file.IsValid())
			return false;

		file.Read(&magic, sizeof(DWORD), 1);

		if (magic != DDS_MAGIC) {
			goto _fail;
		}

		file.Read(&header.dwSize, sizeof(DWORD), 1);
		file.Read(&header.dwHeaderFlags, sizeof(DWORD), 1);
		file.Read(&header.dwHeight, sizeof(DWORD), 1);
		file.Read(&header.dwWidth, sizeof(DWORD), 1);
		file.Read(&header.dwPitchOrLinearSize, sizeof(DWORD), 1);
		file.Read(&header.dwDepth, sizeof(DWORD), 1);
		file.Read(&header.dwMipMapCount, sizeof(DWORD), 1);

		file.Read(&header.dwReserved1, sizeof(header.dwReserved1), 1);

		file.Read(&header.ddspf.dwSize, sizeof(DWORD), 1);
		file.Read(&header.ddspf.dwFlags, sizeof(DWORD), 1);
		file.Read(&header.ddspf.dwFourCC, sizeof(DWORD), 1);
		file.Read(&header.ddspf.dwRGBBitCount, sizeof(DWORD), 1);
		file.Read(&header.ddspf.dwRBitMask, sizeof(DWORD), 1);
		file.Read(&header.ddspf.dwGBitMask, sizeof(DWORD), 1);
		file.Read(&header.ddspf.dwBBitMask, sizeof(DWORD), 1);
		file.Read(&header.ddspf.dwABitMask, sizeof(DWORD), 1);

		file.Read(&header.dwCaps, sizeof(DWORD), 1);
		file.Read(&header.dwCaps2, sizeof(DWORD), 1);
		file.Read(&header.dwCaps3, sizeof(DWORD), 1);
		file.Read(&header.dwCaps4, sizeof(DWORD), 1);
		file.Read(&header.dwReserved2, sizeof(DWORD), 1);

		if (header.dwSize != sizeof(DDS_HEADER) || header.ddspf.dwSize != sizeof(DDS_PIXELFORMAT)) {
			goto _fail;
		}

		outinfo->Width = header.dwWidth;
		outinfo->Height = header.dwHeight;
		outinfo->Depth = header.dwDepth;
		outinfo->Format = Format::GLFMT_UNKNOWN;
		outinfo->MipLevels = (header.dwMipMapCount == 0 ? 1 : header.dwMipMapCount);
		outinfo->Data = 0;

		if (header.ddspf.dwFlags & DDPF_FOURCC)
		{
			if (header.ddspf.dwFourCC == DDSPF_DXT1.dwFourCC)
			{
				outinfo->Format = Format::GLFMT_DXT1;
				//Warning("(header.ddspf.dwFourCC == DDSPF_DXT1.dwFourCC)");
			}
			else if (header.ddspf.dwFourCC == DDSPF_DXT5.dwFourCC)
			{
				outinfo->Format = Format::GLFMT_DXT5;
				//Warning("(header.ddspf.dwFourCC == DDSPF_DXT5.dwFourCC)");
			}
			else if (header.ddspf.dwFourCC == 0x70)
			{
				outinfo->Format = Format::GLFMT_G16R16F;
				//Warning("(header.ddspf.dwFourCC == 0x70)");
			}
			else if (header.ddspf.dwFourCC == 0x71)
			{
				outinfo->Format = Format::GLFMT_A16B16G16R16F;
				//Warning("(header.ddspf.dwFourCC == 0x71)");
			}
			else if (header.ddspf.dwFourCC == 0x73)
			{
				outinfo->Format = Format::GLFMT_G32R32F;
				//Warning("(header.ddspf.dwFourCC == 0x73)");
			}
			else
			{
				//Warning("(header.ddspf.dwFourCC == unsupported)");
				goto _fail; // unsupported
			}
		}
		else if (header.ddspf.dwRGBBitCount == 32)
		{
			outinfo->Format = Format::GLFMT_A8R8G8B8;
			//Warning("(header.ddspf.dwRGBBitCount == 32)");
		}
		else if (header.ddspf.dwRGBBitCount == 24)
		{
			//Warning("(header.ddspf.dwRGBBitCount == 24)");
			if (header.ddspf.dwRBitMask & 0x00ff0000)
			{
				//Warning("(header.ddspf.dwRBitMask & 0x00ff0000)");

				// ARGB (BGRA)
				//!@todo IMPLEMENT FORMAT_B8G8R8 support, ������ ��������� � ������� ��������
				outinfo->Format = Format::GLFMT_R8G8B8/*FORMAT_B8G8R8*/;
			}
			else
			{
				//Warning("(header.ddspf.dwRBitMask & 0x00ff0000) ELSE");
				// ABGR (RGBA)
				outinfo->Format = Format::GLFMT_R8G8B8;
			}
		}
		else
		{
			//Warning("else if (header.ddspf.dwRGBBitCount == 24) FAIL");
			goto _fail;
		}

		if (header.dwCaps2 & DDSCAPS2_VOLUME)
		{
			if (outinfo->Format == Format::GLFMT_DXT1 || outinfo->Format == Format::GLFMT_DXT5)
			{
				// compressed volume texture
				bytesize = GetCompressedImageSize(outinfo->Width, outinfo->Height, outinfo->Depth, outinfo->MipLevels, outinfo->Format);

				outinfo->Data = malloc(bytesize);
				outinfo->DataSize = bytesize;

				file.Read(&outinfo->Data, bytesize, 1);
			}
			else
			{
				// uncompressed volume texture
				bytesize = GetImageSize(outinfo->Width, outinfo->Height, (header.ddspf.dwRGBBitCount / 8), outinfo->MipLevels) * outinfo->Depth;
				outinfo->Data = malloc(bytesize);

				file.Read(outinfo->Data, bytesize, 1);
				outinfo->DataSize = bytesize;
			}
		}
		else if (header.dwCaps2 & DDSCAPS2_CUBEMAP)
		{
			if (outinfo->Format == Format::GLFMT_DXT1 || outinfo->Format == Format::GLFMT_DXT5)
			{
				// compressed cubemap
				bytesize = GetCompressedImageSize(outinfo->Width, outinfo->Height, outinfo->MipLevels, outinfo->Format) * 6;

				outinfo->Data = malloc(bytesize);
				outinfo->DataSize = bytesize;

				file.Read(outinfo->Data, 1, bytesize);
			}
			else
			{
				// uncompressed cubemap
				bytesize = GetImageSize(outinfo->Width, outinfo->Height, (header.ddspf.dwRGBBitCount / 8), outinfo->MipLevels) * 6;
				outinfo->Data = malloc(bytesize);

				file.Read((char*)outinfo->Data, 1, bytesize);
				outinfo->DataSize = bytesize;
			}
		}
		else
		{
			if (outinfo->Format == Format::GLFMT_DXT1 || outinfo->Format == Format::GLFMT_DXT5)
			{
				// compressed
				bytesize = GetCompressedImageSize(outinfo->Width, outinfo->Height, outinfo->MipLevels, outinfo->Format);

				outinfo->Data = malloc(bytesize);
				outinfo->DataSize = bytesize;

				file.Read(outinfo->Data, 1, bytesize);
			}
			else
			{
				// uncompressed
				bytesize = GetImageSize(outinfo->Width, outinfo->Height, (header.ddspf.dwRGBBitCount / 8), outinfo->MipLevels);

				outinfo->Data = malloc(bytesize);
				outinfo->DataSize = bytesize;

				file.Read(outinfo->Data, 1, bytesize);
			}
		}

	_fail:
		file.Close();

		ASSERT(outinfo->Data, "Invalid");

		return (outinfo->Data != 0);
	}

	bool SaveToDDS(const char* file, const DDS_Image_Info* info)
	{
		DDS_HEADER	header;
		FILE*		outfile = 0;
		DWORD		magic = DDS_MAGIC;

		if (!info)
			return false;

#ifdef _MSC_VER
		fopen_s(&outfile, file, "wb");
#else
		outfile = fopen(file, "wb");
#endif

		if (!outfile)
			return false;

		memset(&header, 0, sizeof(DDS_HEADER));

		// NOTE: RGBA16F cubemap only
		header.dwSize = sizeof(DDS_HEADER);
		header.dwHeaderFlags = 0x2 | 0x4;
		header.dwHeight = info->Height;
		header.dwWidth = info->Width;
		header.dwPitchOrLinearSize = header.dwWidth * 8;
		header.dwDepth = 0;
		header.dwMipMapCount = info->MipLevels;

		header.ddspf.dwSize = sizeof(DDS_PIXELFORMAT);
		header.ddspf.dwFlags = 0x4;
		header.ddspf.dwFourCC = 0x71;
		header.ddspf.dwRGBBitCount = 64;
		header.ddspf.dwRBitMask = 0;
		header.ddspf.dwGBitMask = 0;
		header.ddspf.dwBBitMask = 0;
		header.ddspf.dwABitMask = 0;

		header.dwCaps = 0x00401008;
		header.dwCaps2 = 0xfe00;
		header.dwCaps3 = 0;
		header.dwReserved2 = 0;

		fwrite(&magic, sizeof(DWORD), 1, outfile);
		fwrite(&header, sizeof(DDS_HEADER), 1, outfile);
		fwrite((char*)info->Data, 1, info->DataSize, outfile);

		fclose(outfile);
		return true;
	}
}