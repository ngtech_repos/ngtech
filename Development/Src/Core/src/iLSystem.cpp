/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "CorePrivate.h"
//***************************************************************************
#include "../../../Externals/ResIL/include/IL/il.h"
#include "../../../Externals/ResIL/include/IL/ilu.h"
//***************************************************************************

namespace NGTech {
	/**
	*/
	CORE_API void ILSystemInit()
	{
#if !IS_OS_ANDROID
		ilInit();
		iluInit();

		// ��� �� �������� �� ���� ���������� ���� ������
		ilEnable(IL_ORIGIN_SET);
		ilSetInteger(IL_ORIGIN_MODE, IL_ORIGIN_LOWER_LEFT);
#else
		//!@todo Core:IlSystem: port on ANDROID!
#endif
	}

	/**
	*/
}