/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "CorePrivate.h"
//***************************************************
#include "lua_chunk.h"
//***************************************************
// Implementation of lua_State, requested for gcc/clang
#include "../inc/lua/lstate.h"
//***************************************************

namespace NGTech
{
	// from http://zeuxcg.org/2010/11/07/lua-callstack-with-c-debugge
	static void lua_stacktrace(lua_State* L)
	{
		lua_Debug entry;
		int depth = 0;

		while (lua_getstack(L, depth, &entry))
		{
			int status = lua_getinfo(L, "L", &entry);
			assert(status);

			ErrorWrite("[%d] %s:%d -- %s [%s]\n",
				depth, entry.short_src, entry.currentline,
				(entry.name ? entry.name : "<unknown>"), entry.what);
			depth++;
		}
	}

	lua_chunk::lua_chunk(lua_State* L, const String& script) {
		m_LuaState = std::shared_ptr<lua_State>(L);
		Compile(script);
	}

	lua_chunk::lua_chunk(lua_State* L) {
		m_LuaState = std::shared_ptr<lua_State>(L);
	}

	int lua_chunk::CallSpecificFunction(const String & _name, const String&_File)
	{
		int result = 0;

		auto lua_state = m_LuaState;
		try
		{
			if (lua_state)
				result = luabind::call_function<int>(lua_state.get(), _name.c_str(), 0, 1);
		}
		catch (std::exception&e)
		{
			Warning("Script Error, in %s, function %s, desc: %s", _File.c_str(), _name.c_str(), e.what());
		}

		return result;
	}

	bool lua_chunk::Compile(const String& script) {
		is_compiled = false;

#ifndef LUA_OK
#define LUA_OK	0
#endif

		//luaL_loadstring(L, script.c_str()) || lua_pcall(L, 0, LUA_MULTRET, 0))
		const int res = luaL_loadstring(m_LuaState.get(), script.c_str());
		if (res != LUA_OK) {
			const char *msg = lua_tostring(m_LuaState.get(), -1);
			errors = msg ? msg : "unknown error";

			//switch (res)
			//{
			//case LUA_ERRSYNTAX:
			//	break;
			//case LUA_ERRFILE:
			//	break;
			//default :
			//	break;
			//}

			lua_stacktrace(m_LuaState.get());

			return false;
		}
		is_compiled = true;
		using namespace luabind;
		chunk = object(from_stack(m_LuaState.get(), -1));

		for (int index = 1;; ++index) { // find _ENV upvalue, then use it index to changing chunk ENV for reEVAL.
			auto upv = luabind::getupvalue(chunk, index);
			auto name = std::get<0>(upv);
			static String _ENV_NAME = "_ENV";
			if (!name) break;
			if (name == _ENV_NAME) {
				ENV_index = index;
				break;
			}
		}
		return is_compiled;
	}

	luabind::object lua_chunk::Run(luabind::object env) {
		using namespace luabind;

		if (!is_compiled) {
			ErrorWrite(errors.c_str());
			lua_stacktrace(m_LuaState.get());
			return object();
		}

		if (env) {
			setupvalue(chunk, 1, env);
		}
		else {
			object _G = rawget(globals(m_LuaState.get()), "_G");
			setupvalue(chunk, 1, _G);
		}

		luabind::function<luabind::object> chunk_func = chunk;
		return chunk_func();
	}

	luabind::object lua_chunk::New_Instance() {
		using namespace luabind;
		object oldEnv = rawget(globals(m_LuaState.get()), "_G");
		object newEnv = newtable(m_LuaState.get());
		object newEnvMeta = newtable(m_LuaState.get());
		settable(newEnvMeta, "__index", oldEnv);
		setmetatable(newEnv, newEnvMeta);
		Run(newEnv);
		return newEnv;
	}
}