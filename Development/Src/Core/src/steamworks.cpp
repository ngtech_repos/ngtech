/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "CorePrivate.h"

//**************************************
#if USE_STEAMWORKS
#include "steamworks/steam/steam_api.h"
//**************************************
#include "SteamWorksMgr.h"
#include "StatsAndAchievements.h"
//**************************************

#ifdef STEAM_CEG
// Steam DRM header file
#include "cegclient.h"
#else
#define Steamworks_InitCEGLibrary() (true)
#define Steamworks_TermCEGLibrary()
#define Steamworks_TestSecret()
#define Steamworks_SelfCheck()
#endif
#ifdef WIN32
#include <direct.h>
#else
#define MAX_PATH PATH_MAX
#define _getcwd getcwd
#endif

#if IS_OS_WINDOW && IS_PLATFORM_32BIT
#pragma comment(lib,"steamworks/redistributable_bin/steam_api.lib")
#elif IS_OS_WINDOWS && IS_PLATFORM_64BIT
#pragma comment(lib,"steamworks/redistributable_bin/win64/steam_api64.lib")
#endif

namespace NGTech
{
	/**
	*/
	int Alert(const char *lpCaption, const char *lpText)
	{
#ifndef _DEVELOP
		PlatformError(lpText, lpCaption);
		return 0;
#else
		return 0;
#endif
	}

	/**
	*/
	extern "C" void __cdecl SteamAPIDebugTextHook(int nSeverity, const char *pchDebugText)
	{
		// if you're running in the debugger, only warnings (nSeverity >= 1) will be sent
		// if you add -debug_steamapi to the command-line, a lot of extra informational messages will also be sent
		::OutputDebugString(pchDebugText);

		if (nSeverity >= 1)
		{
			// place to set a breakpoint for catching API errors
			int x = 3;
			x = x;
		}
	}

	/**
	*/
	SteamWorksMgr* InitSteamWorks()
	{
		static SteamWorksMgr steam;
		if (SteamAPI_RestartAppIfNecessary(k_uAppIdInvalid))
			return false;

		if (!Steamworks_InitCEGLibrary())
		{
#if !defined(ENGINE_RELEASE) && (!defined(_FINALRELEASE) || defined(EMULATE_DEVELOPER_FINAL_RELEASE)) && !defined(NGTECH_STATIC_LIBS)
			Debug("Steamworks_InitCEGLibrary() failed");
#endif
			Alert("Fatal Error", "Steam must be running to play this game.\n");
			return nullptr;
		}

		if (!SteamAPI_Init())
		{
#if !defined(ENGINE_RELEASE) && (!defined(_FINALRELEASE) || defined(EMULATE_DEVELOPER_FINAL_RELEASE)) && !defined(NGTECH_STATIC_LIBS)
			Debug("SteamAPI_Init() failed");
#endif
			Alert("Fatal Error", "Steam must be running to play this game.\n");
			return nullptr;
		}

		SteamClient()->SetWarningMessageHook(&SteamAPIDebugTextHook);
		SteamUtils()->SetOverlayNotificationPosition(k_EPositionTopRight);

		// do a DRM self check
		Steamworks_SelfCheck();

		//Inition of Stats And Achievements
		steam.stats = &StatsAndAchievements();
		return &steam;
	}

	/**
	*/
	void DestroySteamWorks() {
		if (SteamAPI_IsSteamRunning())
			SteamAPI_Shutdown();
		Steamworks_TermCEGLibrary();
	}
}
#endif