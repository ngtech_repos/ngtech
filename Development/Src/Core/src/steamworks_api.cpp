/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "CorePrivate.h"

#include "CoreManager.h"

#include "SteamWorksMgr.h"
#include "StatsAndAchievements.h"

namespace NGTech {
	/**
	*/
	CORE_API void API_SteamWorks_SetAchievement(const char* _name)
	{
#if USE_STEAMWORKS
		auto steam = GetSteamworksMgr();
		ASSERT(steam, "Invalid pointer on SteamWorksMgr in GetSteamworksMgr()");
		if (!steam) {
			Error("Invalid pointer on SteamWorksMgr in GetSteamworksMgr()", false);
			return;
		}

		steam->stats->SetAchievement(_name);
#else
		LogPrintf("Function %s,is not implemented.For using you must define 'USE_STEAMWORKS'", __FUNCTION__);
#endif
	}

	/**
	*/
	CORE_API void API_SteamWorks_DeleteAchievement(const char* _name)
	{
#if USE_STEAMWORKS
		auto steam = GetSteamworksMgr();
		ASSERT(steam, "Invalid pointer on SteamWorksMgr in GetSteamworksMgr()");
		if (!steam) {
			Error("Invalid pointer on SteamWorksMgr in GetSteamworksMgr()", false);
			return;
		}

		steam->stats->DeleteAchievement(_name);
#else
		LogPrintf("Function %s,is not implemented.For using you must define 'USE_STEAMWORKS'", __FUNCTION__);
#endif
	}

	/**
	*/
	CORE_API void API_SteamWorks_SetStat(const char*_name, int _value)
	{
#if USE_STEAMWORKS
		auto steam = GetSteamworksMgr();
		ASSERT(steam, "Invalid pointer on SteamWorksMgr in GetSteamworksMgr()");
		if (!steam) {
			Error("Invalid pointer on SteamWorksMgr in GetSteamworksMgr()", false);
			return;
		}

		steam->stats->SetStat(_name, _value);
#else
		LogPrintf("Function %s,is not implemented.For using you must define 'USE_STEAMWORKS'", __FUNCTION__);
#endif
	}

	/**
	*/
	CORE_API void API_SteamWorks_GetStat(const char* _name, int &_value)
	{
#if USE_STEAMWORKS
		auto steam = GetSteamworksMgr();
		ASSERT(steam, "Invalid pointer on SteamWorksMgr in GetSteamworksMgr()");
		if (!steam) {
			Error("Invalid pointer on SteamWorksMgr in GetSteamworksMgr()", false);
			return;
		}

		steam->stats->GetStat(_name, _value);
#else
		LogPrintf("Function %s,is not implemented.For using you must define 'USE_STEAMWORKS'", __FUNCTION__);
#endif
	}
}