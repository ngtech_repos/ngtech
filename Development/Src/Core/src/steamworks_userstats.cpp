/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "CorePrivate.h"

#if USE_STEAMWORKS
//**************************************
#include "steamworks/steam/steam_api.h"
#include "steamworks/steam/ISteamUserStats.h"
//**************************************
#include "StatsAndAchievements.h"
//**************************************

namespace NGTech
{
	/**
	*/
	StatsAndAchievements::StatsAndAchievements()
		:m_pSteamUserStats(NULL)
	{
		m_pSteamUserStats = SteamUserStats();

		m_bRequestedStats = false;
		m_bStatsValid = false;
		m_bStoreStats = false;
	}

	/**
	*/
	bool StatsAndAchievements::SetAchievement(const char*_ach)
	{
		SteamUserStats()->SetAchievement(_ach);
		return SteamUserStats()->StoreStats();
	}

	/**
	*/
	bool StatsAndAchievements::DeleteAchievement(const char*_ach)
	{
		SteamUserStats()->ClearAchievement(_ach);
		return SteamUserStats()->StoreStats();
	}

	/**
	*/
	void StatsAndAchievements::SetStat(const char*_name, int _value)
	{
		if (m_bStoreStats)
		{
			m_pSteamUserStats->SetStat(_name, _value);
			// If this failed, we never sent anything to the server, try
			// again later.
			m_bStoreStats = !m_pSteamUserStats->StoreStats();
		}
	}

	/**
	*/
	void StatsAndAchievements::GetStat(const char*_name, int &_value)
	{
		if (m_bStoreStats)
		{
			int32* val = &_value;
			m_pSteamUserStats->GetStat(_name, val);
			// If this failed, we never sent anything to the server, try
			// again later.
			m_bStoreStats = !m_pSteamUserStats->StoreStats();
		}
	}

	/**
	*/
}
#endif