﻿/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.

*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/

#region License

// Copyright (c) 2009 Sander van Rossen
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#endregion License

using System;
using System.Drawing;

namespace Graph.Items
{
    public sealed class NodeCheckboxItem : NodeItem
    {
        public NodeCheckboxItem(string text, bool inputEnabled, bool outputEnabled) :
            base(inputEnabled, outputEnabled)
        {
            Text = text;
        }

        #region Text

        private string internalText = string.Empty;

        public string Text
        {
            get => internalText;
            set
            {
                if (internalText == value)
                {
                    return;
                }

                internalText = value;
                TextSize = Size.Empty;
            }
        }

        #endregion Text

        #region Checked

        private bool internalChecked = false;

        public bool Checked
        {
            get => internalChecked;
            set
            {
                if (internalChecked == value)
                {
                    return;
                }

                internalChecked = value;
                TextSize = Size.Empty;
            }
        }

        #endregion Checked

        public override bool OnClick()
        {
            base.OnClick();
            Checked = !Checked;
            return true;
        }

        internal SizeF TextSize;

        internal override SizeF Measure(Graphics graphics)
        {
            if (!string.IsNullOrWhiteSpace(Text))
            {
                if (TextSize.IsEmpty)
                {
                    Size size = new Size(GraphConstants.MinimumItemWidth, GraphConstants.MinimumItemHeight);

                    TextSize = graphics.MeasureString(Text, SystemFonts.MenuFont, size, GraphConstants.CenterMeasureTextStringFormat);

                    TextSize.Width = Math.Max(size.Width, TextSize.Width);
                    TextSize.Height = Math.Max(size.Height, TextSize.Height);
                }

                return TextSize;
            }
            else
            {
                return new SizeF(GraphConstants.MinimumItemWidth, GraphConstants.TitleHeight + GraphConstants.TopHeight);
            }
        }

        internal override void Render(Graphics graphics, SizeF minimumSize, PointF location)
        {
            SizeF size = Measure(graphics);
            size.Width = Math.Max(minimumSize.Width, size.Width);
            size.Height = Math.Max(minimumSize.Height, size.Height);

            using (System.Drawing.Drawing2D.GraphicsPath path = GraphRenderer.CreateRoundedRectangle(size, location))
            {
                RectangleF rect = new RectangleF(location, size);
                if (Checked)
                {
                    using (SolidBrush brush = new SolidBrush(Color.FromArgb(128 + 32, Color.White)))
                    {
                        graphics.FillPath(brush, path);
                    }
                }
                else
                {
                    using (SolidBrush brush = new SolidBrush(Color.FromArgb(64, Color.Black)))
                    {
                        graphics.FillPath(brush, path);
                    }
                }
                graphics.DrawString(Text, SystemFonts.MenuFont, Brushes.Black, rect, GraphConstants.CenterTextStringFormat);

                if ((state & RenderState.Hover) != 0)
                {
                    graphics.DrawPath(Pens.White, path);
                }
                else
                {
                    graphics.DrawPath(Pens.Black, path);
                }
            }
        }
    }

    public sealed class NodeCheckboxItemInverse : NodeItemInverseOnlyInput
    {
        public NodeCheckboxItemInverse(string text, bool inputEnabled, bool outputEnabled)
        {
            Text = text;
        }

        #region Text

        private string internalText = string.Empty;

        public string Text
        {
            get => internalText;
            set
            {
                if (internalText == value)
                {
                    return;
                }

                internalText = value;
                TextSize = Size.Empty;
            }
        }

        #endregion Text

        #region Checked

        private bool internalChecked = false;

        public bool Checked
        {
            get => internalChecked;
            set
            {
                if (internalChecked == value)
                {
                    return;
                }

                internalChecked = value;
                TextSize = Size.Empty;
            }
        }

        #endregion Checked

        public override bool OnClick()
        {
            base.OnClick();
            Checked = !Checked;
            return true;
        }

        internal SizeF TextSize;

        internal override SizeF Measure(Graphics graphics)
        {
            if (!string.IsNullOrWhiteSpace(Text))
            {
                if (TextSize.IsEmpty)
                {
                    Size size = new Size(GraphConstants.MinimumItemWidth, GraphConstants.MinimumItemHeight);

                    TextSize = graphics.MeasureString(Text, SystemFonts.MenuFont, size, GraphConstants.CenterMeasureTextStringFormat);

                    TextSize.Width = Math.Max(size.Width, TextSize.Width);
                    TextSize.Height = Math.Max(size.Height, TextSize.Height);
                }

                return TextSize;
            }
            else
            {
                return new SizeF(GraphConstants.MinimumItemWidth, GraphConstants.TitleHeight + GraphConstants.TopHeight);
            }
        }

        internal override void Render(Graphics graphics, SizeF minimumSize, PointF location)
        {
            SizeF size = Measure(graphics);
            size.Width = Math.Max(minimumSize.Width, size.Width);
            size.Height = Math.Max(minimumSize.Height, size.Height);

            using (System.Drawing.Drawing2D.GraphicsPath path = GraphRenderer.CreateRoundedRectangle(size, location))
            {
                RectangleF rect = new RectangleF(location, size);
                if (Checked)
                {
                    using (SolidBrush brush = new SolidBrush(Color.FromArgb(128 + 32, Color.White)))
                    {
                        graphics.FillPath(brush, path);
                    }
                }
                else
                {
                    using (SolidBrush brush = new SolidBrush(Color.FromArgb(64, Color.Black)))
                    {
                        graphics.FillPath(brush, path);
                    }
                }
                graphics.DrawString(Text, SystemFonts.MenuFont, Brushes.Black, rect, GraphConstants.CenterTextStringFormat);

                if ((state & RenderState.Hover) != 0)
                {
                    graphics.DrawPath(Pens.White, path);
                }
                else
                {
                    graphics.DrawPath(Pens.Black, path);
                }
            }
        }
    }
}