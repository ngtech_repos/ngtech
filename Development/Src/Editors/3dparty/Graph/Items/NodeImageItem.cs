﻿/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.

*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/

#region License

// Copyright (c) 2009 Sander van Rossen
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#endregion License

using System;
using System.Drawing;

namespace Graph.Items
{
    public sealed class NodeImageItem : NodeItem
    {
        public event EventHandler<NodeItemEventArgs> Clicked;

        public NodeImageItem(Image image, bool inputEnabled = false, bool outputEnabled = false) :
            base(inputEnabled, outputEnabled)
        {
            Image = image;
        }

        public NodeImageItem(Image image, int width, int height, bool inputEnabled = false, bool outputEnabled = false) :
            base(inputEnabled, outputEnabled)
        {
            Width = width;
            Height = height;
            Image = image;
        }

        public int? Width { get; set; }
        public int? Height { get; set; }
        public Image Image { get; set; }

        public override bool OnClick()
        {
            base.OnClick();
            if (Clicked != null)
            {
                Clicked(this, new NodeItemEventArgs(this));
            }

            return true;
        }

        internal override SizeF Measure(Graphics graphics)
        {
            if (Image != null)
            {
                SizeF size = new Size(GraphConstants.MinimumItemWidth, GraphConstants.MinimumItemHeight);

                if (Width.HasValue)
                {
                    size.Width = Math.Max(size.Width, Width.Value);
                }
                else
                {
                    size.Width = Math.Max(size.Width, Image.Width);
                }

                if (Height.HasValue)
                {
                    size.Height = Math.Max(size.Height, Height.Value);
                }
                else
                {
                    size.Height = Math.Max(size.Height, Image.Height);
                }

                return size;
            }
            else
            {
                SizeF size = new SizeF(GraphConstants.MinimumItemWidth, GraphConstants.MinimumItemHeight);
                if (Width.HasValue)
                {
                    size.Width = Math.Max(size.Width, Width.Value);
                }

                if (Height.HasValue)
                {
                    size.Height = Math.Max(size.Height, Height.Value);
                }

                return size;
            }
        }

        internal override void Render(Graphics graphics, SizeF minimumSize, PointF location)
        {
            SizeF size = Measure(graphics);
            size.Width = Math.Max(minimumSize.Width, size.Width);
            size.Height = Math.Max(minimumSize.Height, size.Height);

            if (Width.HasValue &&
                size.Width > Width.Value)
            {
                location.X += (size.Width - (Width.Value)) / 2.0f;
                size.Width = (Width.Value);
            }
            RectangleF rect = new RectangleF(location, size);

            if (Image != null)
            {
                graphics.DrawImage(Image, rect);
            }

            if ((state & RenderState.Hover) != 0)
            {
                graphics.DrawRectangle(Pens.White, rect.Left, rect.Top, rect.Width, rect.Height);
            }
            else
            {
                graphics.DrawRectangle(Pens.Black, rect.Left, rect.Top, rect.Width, rect.Height);
            }
        }
    }
}