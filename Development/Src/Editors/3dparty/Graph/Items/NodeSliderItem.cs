﻿/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.

*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/

#region License

// Copyright (c) 2009 Sander van Rossen
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#endregion License

using System;
using System.Drawing;

namespace Graph.Items
{
    public class NodeSliderItem : NodeItem
    {
        public event EventHandler<NodeItemEventArgs> Clicked;

        public event EventHandler<NodeItemEventArgs> ValueChanged;

        public NodeSliderItem(string text, float sliderSize, float textSize, float minValue, float maxValue, float defaultValue, bool inputEnabled, bool outputEnabled) :
            base(inputEnabled, outputEnabled)
        {
            Text = text;
            MinimumSliderSize = sliderSize;
            TextSize = textSize;
            MinValue = Math.Min(minValue, maxValue);
            MaxValue = Math.Max(minValue, maxValue);
            Value = defaultValue;
        }

        #region Text

        private string internalText = string.Empty;

        public string Text
        {
            get => internalText;
            set
            {
                if (internalText == value)
                {
                    return;
                }

                internalText = value;
                itemSize = Size.Empty;
            }
        }

        #endregion Text

        #region Dragging

        internal bool Dragging { get; set; }

        #endregion Dragging

        public float MinimumSliderSize { get; set; }
        public float TextSize { get; set; }

        public float MinValue { get; set; }
        public float MaxValue { get; set; }

        private float internalValue = 0.0f;

        public float Value
        {
            get => internalValue;
            set
            {
                float newValue = value;
                if (newValue < MinValue)
                {
                    newValue = MinValue;
                }

                if (newValue > MaxValue)
                {
                    newValue = MaxValue;
                }

                if (internalValue == newValue)
                {
                    return;
                }

                internalValue = newValue;
                if (ValueChanged != null)
                {
                    ValueChanged(this, new NodeItemEventArgs(this));
                }
            }
        }

        public override bool OnClick()
        {
            base.OnClick();
            if (Clicked != null)
            {
                Clicked(this, new NodeItemEventArgs(this));
            }

            return true;
        }

        public override bool OnStartDrag(PointF location, out PointF original_location)
        {
            base.OnStartDrag(location, out original_location);
            float size = (MaxValue - MinValue);
            original_location.Y = location.Y;
            original_location.X = ((Value / size) * sliderRect.Width) + sliderRect.Left;
            Value = ((location.X - sliderRect.Left) / sliderRect.Width) * size;
            Dragging = true;
            return true;
        }

        public override bool OnDrag(PointF location)
        {
            base.OnDrag(location);
            float size = (MaxValue - MinValue);
            Value = ((location.X - sliderRect.Left) / sliderRect.Width) * size;
            return true;
        }

        public override bool OnEndDrag()
        {
            base.OnEndDrag(); Dragging = false; return true;
        }

        internal SizeF itemSize;
        internal SizeF textSize;
        internal RectangleF sliderRect;

        protected const int SliderBoxSize = 4;
        protected const int SliderHeight = 8;
        protected const int Spacing = 2;

        internal override SizeF Measure(Graphics graphics)
        {
            if (!string.IsNullOrWhiteSpace(Text))
            {
                if (itemSize.IsEmpty)
                {
                    Size size = new Size(GraphConstants.MinimumItemWidth, GraphConstants.MinimumItemHeight);
                    float sliderWidth = MinimumSliderSize + SliderBoxSize;

                    textSize = graphics.MeasureString(Text, SystemFonts.MenuFont, size, GraphConstants.LeftMeasureTextStringFormat);
                    textSize.Width = Math.Max(TextSize, textSize.Width + 4);
                    itemSize.Width = Math.Max(size.Width, textSize.Width + sliderWidth + Spacing);
                    itemSize.Height = Math.Max(size.Height, textSize.Height);
                }
                return itemSize;
            }
            else
            {
                return new SizeF(GraphConstants.MinimumItemWidth, GraphConstants.MinimumItemHeight);
            }
        }

        internal override void Render(Graphics graphics, SizeF minimumSize, PointF location)
        {
            SizeF size = Measure(graphics);
            size.Width = Math.Max(minimumSize.Width, size.Width);
            size.Height = Math.Max(minimumSize.Height, size.Height);

            float sliderOffset = Spacing + textSize.Width;
            float sliderWidth = size.Width - (Spacing + textSize.Width);

            RectangleF textRect = new RectangleF(location, size);
            RectangleF sliderBox = new RectangleF(location, size);
            RectangleF sliderRect = new RectangleF(location, size);
            sliderRect.X = sliderRect.Right - sliderWidth;
            sliderRect.Y += ((sliderRect.Bottom - sliderRect.Top) - SliderHeight) / 2.0f;
            sliderRect.Width = sliderWidth;
            sliderRect.Height = SliderHeight;
            textRect.Width -= sliderWidth + Spacing;

            float valueSize = (MaxValue - MinValue);
            this.sliderRect = sliderRect;
            this.sliderRect.Width -= SliderBoxSize;
            this.sliderRect.X += SliderBoxSize / 2.0f;

            sliderBox.Width = SliderBoxSize;
            sliderBox.X = sliderRect.X + (Value * this.sliderRect.Width) / valueSize;

            graphics.DrawString(Text, SystemFonts.MenuFont, Brushes.Black, textRect, GraphConstants.LeftTextStringFormat);

            using (System.Drawing.Drawing2D.GraphicsPath path = GraphRenderer.CreateRoundedRectangle(sliderRect.Size, sliderRect.Location))
            {
                if ((state & (RenderState.Hover | RenderState.Dragging)) != 0)
                {
                    graphics.DrawPath(Pens.White, path);
                }
                else
                {
                    graphics.DrawPath(Pens.Black, path);
                }
            }

            graphics.FillRectangle(Brushes.LightGray, sliderBox.X, sliderBox.Y, sliderBox.Width, sliderBox.Height);

            if ((state & (RenderState.Hover | RenderState.Dragging)) != 0)
            {
                graphics.DrawRectangle(Pens.White, sliderBox.X, sliderBox.Y, sliderBox.Width, sliderBox.Height);
            }
            else
            {
                graphics.DrawRectangle(Pens.Black, sliderBox.X, sliderBox.Y, sliderBox.Width, sliderBox.Height);
            }
        }
    }
}