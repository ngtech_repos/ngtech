﻿/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.

*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/

#region License

// Copyright (c) 2009 Sander van Rossen
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#endregion License

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Graph.Items
{
    public sealed class AcceptNodeTextChangedEventArgs : CancelEventArgs
    {
        public AcceptNodeTextChangedEventArgs(string old_text, string new_text)
        {
            PreviousText = old_text; Text = new_text;
        }

        public AcceptNodeTextChangedEventArgs(string old_text, string new_text, bool cancel) : base(cancel)
        {
            PreviousText = old_text; Text = new_text;
        }

        public string PreviousText { get; private set; }
        public string Text { get; set; }
    }

    public sealed class NodeTextBoxItem : NodeItem
    {
        public event EventHandler<AcceptNodeTextChangedEventArgs> TextChanged;

        public NodeTextBoxItem(string text, bool inputEnabled, bool outputEnabled) :
            base(inputEnabled, outputEnabled)
        {
            Text = text;
        }

        public NodeTextBoxItem(string text) :
            this(text, false, false)
        {
            Text = text;
        }

        #region Name

        public string Name
        {
            get;
            set;
        }

        #endregion Name

        #region Text

        private string internalText = string.Empty;

        public string Text
        {
            get => internalText;
            set
            {
                if (internalText == value)
                {
                    return;
                }

                if (TextChanged != null)
                {
                    AcceptNodeTextChangedEventArgs eventArgs = new AcceptNodeTextChangedEventArgs(internalText, value);
                    TextChanged(this, eventArgs);
                    if (eventArgs.Cancel)
                    {
                        return;
                    }

                    internalText = eventArgs.Text;
                }
                else
                {
                    internalText = value;
                }

                TextSize = Size.Empty;
            }
        }

        #endregion Text

        internal SizeF TextSize;

        public override bool OnDoubleClick()
        {
            base.OnDoubleClick();
            TextEditForm form = new TextEditForm
            {
                Text = Name ?? "Edit text",
                InputText = Text
            };
            DialogResult result = form.ShowDialog();
            if (result == DialogResult.OK)
            {
                Text = form.InputText;
            }

            return true;
        }

        internal override SizeF Measure(Graphics graphics)
        {
            if (!string.IsNullOrWhiteSpace(Text))
            {
                if (TextSize.IsEmpty)
                {
                    Size size = new Size(GraphConstants.MinimumItemWidth, GraphConstants.MinimumItemHeight);

                    TextSize = graphics.MeasureString(Text, SystemFonts.MenuFont, size, GraphConstants.LeftMeasureTextStringFormat);

                    TextSize.Width = Math.Max(size.Width, TextSize.Width + 8);
                    TextSize.Height = Math.Max(size.Height, TextSize.Height + 2);
                }
                return TextSize;
            }
            else
            {
                return new SizeF(GraphConstants.MinimumItemWidth, GraphConstants.MinimumItemHeight);
            }
        }

        internal override void Render(Graphics graphics, SizeF minimumSize, PointF location)
        {
            SizeF size = Measure(graphics);
            size.Width = Math.Max(minimumSize.Width, size.Width);
            size.Height = Math.Max(minimumSize.Height, size.Height);

            System.Drawing.Drawing2D.GraphicsPath path = GraphRenderer.CreateRoundedRectangle(size, location);

            location.Y += 1;
            location.X += 1;

            if ((state & RenderState.Hover) == RenderState.Hover)
            {
                graphics.DrawPath(Pens.White, path);
                graphics.DrawString(Text, SystemFonts.MenuFont, Brushes.Black, new RectangleF(location, size), GraphConstants.LeftTextStringFormat);
            }
            else
            {
                graphics.DrawPath(Pens.Black, path);
                graphics.DrawString(Text, SystemFonts.MenuFont, Brushes.Black, new RectangleF(location, size), GraphConstants.LeftTextStringFormat);
            }
        }
    }
}