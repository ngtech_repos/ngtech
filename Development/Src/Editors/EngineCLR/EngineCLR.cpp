/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
// ������� DLL-����.

#include "stdafx.h"

#ifndef DROP_EDITOR
#include "EngineCLR.h"
#include "../../Engine/inc/Engine.h"
#include "../../Engine/inc/EngineAppBase.h"
#include "../../Engine/inc/Scene.h"

// API
#include "../../Engine/API/EngineAPI.h"
#include "../../Engine/inc/WrappedScriptFunctions.h"
#include "../../Engine/inc/Camera.h"
#include "../../ExampleGame/ExampleGame.h"
// ManagedAPI
#include "ManagedAPI.h"

namespace EngineCLR
{
	EngineCLR::~EngineCLR()
	{
		SAFE_DELETE(engine);
	}

	void EngineCLR::EngineInit()
	{
		if (!engine)
		{
			if (!mInited && !engine)
			{
				engine = new EngineAppBase(hwnd, true); // ������� ������� ����� ���������(��������� �������������� ������ ������)
				API_LoadStartupModuleEditor("../system.ltx", 0, NULL); // ��������� ����, ������� dllStartPluginEditor

				mInited = true;
			}
		}

		MouseGrab(false);
		api::EDITOR_PauseEngine(true);
	}

	void EngineCLR::EngineDestroy()
	{
		if (!engine)
			return;

		API_DestroyEngine();
	}

	void EngineCLR::Update() {
		if (!engine)
			return;

		engine->Update();
	}

	void EngineCLR::CameraSetDirection(int x, int y)
	{
		posX = x;
		posY = y;
		auto cam = GetScene()->m_ActiveScene.GetCurrentCamera();
		if (cam && engine)
			cam->LookAt(posX, posY);
		else
			Warning("[Editor]Camera not exist");
	}

	void EngineCLR::NewScene()
	{
		API_NewScene();
	}

	void EngineCLR::LoadScene()
	{
		throw gcnew System::NotImplementedException();
	}

	void SaveLoadHelper::LoadEngineFormat(System::String ^  _p, Type^ type) {
		NGTech::String _a = "Empty";
		MarshalString(_p, _a);
		API_EditorLoadEngineFormat(_a);
	}

	void SaveLoadHelper::SaveEngineFormat(System::String ^ _p, Type^ type)
	{
		NGTech::String _a = "Empty";
		MarshalString(_p, _a);
		if (type == Type::SCENE)
			API_SaveScene(_a);
		else if (type == Type::MATERIAL)
			API_SaveMaterial(_a);
		else// AUTODETECT
			API_EditorSaveEngineFormat(_a.c_str());
	}

	PropertyBags::PropertyBag^ EngineCLR::UpdateSceneVars()
	{
		if (!m_sceneEditor)
			return nullptr;

		return m_sceneEditor->UpdateSceneVars();
	}
	PropertyBags::PropertyBag^ EngineCLR::UpdateActorMeshVars(int uid)
	{
		if (!m_sceneEditor)
			return nullptr;

		return m_sceneEditor->UpdateActorMeshVars(uid);
	}
	PropertyBags::PropertyBag^ EngineCLR::UpdateActorLightVars(int uid)
	{
		if (!m_sceneEditor)
			return nullptr;

		return m_sceneEditor->UpdateActorLightVars(uid);
	}

	void EngineCLR::CleanVars()
	{
		if (!m_sceneEditor)
			return;

		m_sceneEditor->CleanVars();
	}
}

#endif