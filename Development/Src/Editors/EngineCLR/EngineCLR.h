/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#ifndef DROP_EDITOR

#include "Helpers.h"
#include "PropertyHelper.h"
#include "SliderEditor.h"
#include "SceneEditor.h"

namespace NGTech {
	struct ICallback;
	struct EngineAppBase;
}

namespace EngineCLR {
	public ref class SaveLoadHelper
	{
	public:
		enum class Type
		{
			UNDEFINED = 0,
			SCENE,
			MODEL,
			MATERIAL,
			PHYSICS_MATERIAL,
			SOUND_CUE,
			ANIM_SET,
			PHYSICS_SETTINGS
		};
		/*Save/Load*/
		static void LoadEngineFormat(System::String ^  _p, Type^);
		static void SaveEngineFormat(System::String ^  _p, Type^);
	};

	public ref class EngineCLR
	{
	public:
		EngineCLR(void* _hwnd)
			:hwnd(_hwnd),
			mInited(false),
			engine(nullptr),
			posX(0),
			posY(0)
		{
			m_sceneEditor = SceneEditor::Instance;
		}

		~EngineCLR();

		void EngineInit();
		void EngineDestroy();

		ENGINE_INLINE bool isInited() { return mInited; }

		void Update();

		ENGINE_INLINE void Resize(int _w, int _h) {
			API_ResizeWindow(_w, _h);
		}

		void CameraSetDirection(int x, int y);
		/*Input*/
		ENGINE_INLINE void KeyDown(int _key) { if (GetWindow() && engine) GetWindow()->SetKeyDown(_key); }
		ENGINE_INLINE void KeyUp(int _key) { GetWindow()->SetKeyUp(_key); }
		ENGINE_INLINE bool isMouseGrabed() { if (GetWindow() && engine) return GetWindow()->IsMouseGrabbed(); return false; }
		ENGINE_INLINE void MouseGrab(bool _val) { if (GetWindow() && engine) GetWindow()->GrabMouse(_val); }
		ENGINE_INLINE void SetShowCursor(bool _s) { GetWindow()->ShowOSCursor(_s); }
		// TODO:ENGINECLR:Move to SceneAPI
		ENGINE_INLINE System::String^ GetCurrentSceneName() { auto sceneName = GetScene()->GetCurrentSceneName();	return Helpers::ToCLIString(sceneName); }

		/*Scene*/
		void NewScene();
		void LoadScene();

		PropertyBags::PropertyBag^ UpdateSceneVars();
		PropertyBags::PropertyBag^ UpdateActorMeshVars(int uid);
		PropertyBags::PropertyBag^ UpdateActorLightVars(int uid);

		void CleanVars();
		ENGINE_INLINE size_t CountofObjectsOnScene() {
			return API_CountofObjectsOnScene();
		}
	private:
		SceneEditor^ m_sceneEditor;

		EngineAppBase* engine = nullptr;
		void* hwnd = nullptr;

		bool mInited = false;

		float posX, posY;
	};
}

#endif