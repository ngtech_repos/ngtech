#include "stdafx.h"

#ifndef DROP_EDITOR
#include "Helpers.h"

namespace EngineCLR {
	void Helpers::PrintError(Exception^ ex, System::String ^ text)
	{
		// TODO:RENAME DEFINE
#ifndef DROP_EDITOR
		if (didError)
			return;

		auto total = ToCppString(text) + "\n" + ToCppString(ex->Source) + "\n" + ToCppString(ex->StackTrace) + "\n" + ToCppString(ex->Message) + "\n\n You will only see one EngineCLR Error per run; EngineCLR behavior after this point may become indeterminate.";
		LogPrintf(total.c_str());
		didError = true;
#endif
	}
}

#endif