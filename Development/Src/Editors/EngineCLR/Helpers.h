/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#ifndef DROP_EDITOR

using namespace System;
using namespace System::Runtime::InteropServices;
using namespace System::Text;

#define DISPOSABLE(x) x
#define DISPOSE(x) ~x()
#define FINALIZER(x) !x()

#define USE_MANAGED_ASSERT 1

#if defined(_DEBUG) || defined(USE_MANAGED_ASSERT)
#define CLIASSERT(condition, ...) System::Diagnostics::Debug::Assert(condition, ##__VA_ARGS__)
#else
#define CLIASSERT(condition, ...) ASSERT(condition, ##__VA_ARGS__) // This macro will completely evaporate in Release.
#endif

// <summary>
/// Managed System helper functions, such as Managed-STL string conversion and error printing.
// </summary>
#ifndef DOXYGEN_IGNORE
namespace EngineCLR
{
#endif

	ref class Helpers
	{
	private:
		static bool didError;
	public:
		static ENGINE_INLINE NGTech::String ToCppString(System::String ^ str)
		{
			// Create two different encodings.
			Encoding^ ascii = Encoding::ASCII;
			Encoding^ unicode = Encoding::Unicode;

			// Convert the string into a Byte->Item[].
			array<Byte>^unicodeBytes = unicode->GetBytes(str);

			// Perform the conversion from one encoding to the other.
			array<Byte>^asciiBytes = Encoding::Convert(unicode, ascii, unicodeBytes);

			// Convert the new Byte into[] a char and[] then into a string.
			// This is a slightly different approach to converting to illustrate
			// the use of GetCharCount/GetChars.
			array<Char>^asciiChars = gcnew array<Char>(ascii->GetCharCount(asciiBytes, 0, asciiBytes->Length));
			ascii->GetChars(asciiBytes, 0, asciiBytes->Length, asciiChars, 0);
			System::String^ asciiString = gcnew System::String(asciiChars);

			System::IntPtr ptr(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(asciiString));
			NGTech::String ret(static_cast<const char *>(static_cast<void *>(ptr)));
			System::Runtime::InteropServices::Marshal::FreeCoTaskMem(ptr);
			return(ret);
		}

		static ENGINE_INLINE System::String^ ToCLIString(NGTech::String str)
		{
			return Marshal::PtrToStringAnsi(IntPtr((void*)str.c_str()), (int)str.length());
		}

		static ENGINE_INLINE std::wstring ToCppStringw(System::String ^ str)
		{
			System::IntPtr
				ptr(System::Runtime::InteropServices::Marshal::StringToHGlobalUni(str));
			std::wstring  ret(static_cast<const wchar_t *>(static_cast<void *>(ptr)));
			System::Runtime::InteropServices::Marshal::FreeCoTaskMem(ptr);
			return(ret);
		}

		static ENGINE_INLINE System::String^ ToCLIString(std::wstring str)
		{
			return Marshal::PtrToStringUni(IntPtr((void*)str.c_str()), (int)str.length());
		}

		static ENGINE_INLINE void PrintError(Exception^ ex, System::String ^ text);

		static const System::String^ EMPTY_STRING = ("");
	};
}

#endif