/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "stdafx.h"

#ifndef DROP_EDITOR
#include "EngineCLR.h"
#include "LevelBuildingTool.h"

// EngineCore
#include "../../Engine/inc/PhysXSerializer.h"

namespace EngineCLR
{
	ENGINE_INLINE void LevelBuildingTool::CalculateCollisions() {
		CLIASSERT(SceneEditor::Instance != nullptr, "Invalid pointer on instance of SceneEditor");
		if (!SceneEditor::Instance) return;

		if (SceneEditor::Instance->IsValid() == false) return;
		CLIASSERT(SceneEditor::Instance->IsValid(), "Instance is not valid scene");


		CLIASSERT(SceneEditor::Instance->GetSceneName() != System::String::Empty, "Invalid sceneName");
		if (SceneEditor::Instance->GetSceneName() == System::String::Empty) return;

		PhysXSerializer::SerializeAllObjectsFromWorld(Helpers::ToCppString(SceneEditor::Instance->GetDirectoryName() + m_DataDirectoryName).c_str(), Helpers::ToCppString(SceneEditor::Instance->GetSceneName()).c_str());
	}
}

#endif