#pragma once

#ifndef DROP_EDITOR
namespace EngineCLR
{
	public ref class LevelBuildingTool sealed
	{
	public:
		ENGINE_INLINE void CalculateLightMapsForGI()
		{}

		// !@ Creation of collisions
		ENGINE_INLINE void CalculateCollisions();

	private:
		System::String^ m_DataDirectoryName = gcnew System::String("/Data/");
	};
}

#endif