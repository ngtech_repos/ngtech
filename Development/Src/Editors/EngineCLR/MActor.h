#pragma once

#ifndef DROP_EDITOR
using namespace System;

namespace EngineCLR
{
	public interface class MActor
	{
		property System::String^ ClassName { System::String^ get();	}

		property bool IsHidden {
			bool get();
			void set(bool _v);
		}

		property bool IsFrozen { bool get(); }

		property bool IsSelectable { bool get(); }

		property bool IsSelected {
			bool get();
			void set(bool value);
		}

		property bool IsPrefab { bool get(); }
		property bool IsLight { bool get(); }
	};
}

#endif