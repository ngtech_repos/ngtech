/* Copyright (C) 2009-2017, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
*
#include "stdafx.h"

#ifndef DROP_EDITOR
#include "../../Engine/inc/EditableObject.h"
#include "ManagedEditableObject.h"

namespace EngineCLR
{
	/// <summary>
	/// ctor wraps an existing unmanaged pointer
	/// </summary>
	MEditableObject::MEditableObject(EditableObject* _obj)
		:m_edObj(_obj)
	{
		CLIASSERT(m_edObj, "invalid pointer m_edObj in %s", __FUNCTION__);
	}

	MEditableObject::MEditableObject() : m_edObj(nullptr) {}

	ENGINE_INLINE std::vector<EditorVar>& MEditableObject::GetEditorVars() {
		CLIASSERT(m_edObj, "invalid pointer m_edObj in %s", __FUNCTION__);
		return m_edObj->GetEditorVars();
	}

	ENGINE_INLINE System::String^ MEditableObject::GetGUIDString() {
		CLIASSERT(m_edObj, "invalid pointer m_edObj in %s", __FUNCTION__);

		return Helpers::ToCLIString(m_edObj->GetGUIDString());
	}

	ENGINE_INLINE MEditableObject::~MEditableObject() {
		SAFE_DELETE(m_edObj);
	}
}
#endif