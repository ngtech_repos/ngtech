#include "stdafx.h"

#ifndef DROP_EDITOR
#include "MFileNameEditor.h"

namespace EngineCLR
{
	void MFileNameEditor::InitializeDialog(System::Windows::Forms::OpenFileDialog^ openFileDialog)
	{
		openFileDialog->Filter = "All|*.*";
		openFileDialog->Title = L"Select file";
		Filename = openFileDialog->FileName;
		MFileNameEditorBase::InitializeDialog(openFileDialog);
	}

	MFileNameEditor::MFileNameEditor()
	{
		filename = "";
	}
}
#endif