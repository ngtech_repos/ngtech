/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#ifndef DROP_EDITOR
using namespace PropertyBags;
using namespace System;
using namespace System::IO;
using namespace System::Drawing::Design;
using namespace System::Configuration;
using namespace System::ComponentModel;
using namespace System::Windows::Forms;
using namespace System::Windows::Forms::Design;
using namespace System::Collections;

#pragma comment(lib,"user32.lib")

using namespace DevExpress::XtraVerticalGrid;
using namespace DevExpress::XtraEditors::Repository;

#include "MFileNameEditorBase.h"

namespace EngineCLR
{
	/**/
	public ref class MFileNameEditor sealed : public MFileNameEditorBase
	{
	public:
		MFileNameEditor();
		virtual ~MFileNameEditor() {}

		// ���������� ��� �������� �������
		virtual void InitializeDialog(System::Windows::Forms::OpenFileDialog^ openFileDialog) override;

		ENGINE_INLINE MFileNameEditor::MFileNameEditor(System::String^_filename) :MFileNameEditorBase(_filename) {}

		// ��� ������������ ����� ����������
		static ENGINE_INLINE operator MFileNameEditor ^ (System::String^ _string) {
			return gcnew MFileNameEditor(_string);
		}
		// �� ������ �����, �� ����������
		static ENGINE_INLINE operator System::String ^ (MFileNameEditor^ _editor) {
			return gcnew System::String(_editor->Filename);
		}

		ENGINE_INLINE operator System::String ^ () {
			return gcnew System::String(this->Filename);
		}
	};
}
#endif