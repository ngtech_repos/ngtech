/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#ifndef DROP_EDITOR
using namespace PropertyBags;
using namespace System;
using namespace System::IO;
using namespace System::Drawing::Design;
using namespace System::Configuration;
using namespace System::ComponentModel;
using namespace System::Windows::Forms;
using namespace System::Windows::Forms::Design;
using namespace System::Collections;

#pragma comment(lib,"user32.lib")

namespace EngineCLR
{
	/**/
	enum CustomEditorType
	{
		FolderBrowserEditor = 0,
		FileDialogEditor,
		MaterialDialogEditor,
		TextureDialogEditor,
		ShaderDialogEditor,
		ImportDialogEditor,
	};

	/**/
	[AttributeUsage(System::AttributeTargets::Property | System::AttributeTargets::Field)]
	public ref class EditorAliasAttribute :public System::Attribute
	{
	public:
		CustomEditorType  _editorType;

		ENGINE_INLINE EditorAliasAttribute(CustomEditorType editorType) {
			_editorType = editorType;
		}

		property CustomEditorType EditorType {
			ENGINE_INLINE CustomEditorType get() { return _editorType; }
			ENGINE_INLINE void set(CustomEditorType value) { _editorType = value; }
		};
	};

	/**/
	public ref class MFileNameEditorBase : public System::Windows::Forms::Design::FileNameEditor
	{
	public:
		property System::String^ Filename
		{
			System::String^ get()
			{
				return filename;
			}
			void set(System::String^ value)
			{
				filename = value;
			}
		}

		System::String^ filename;
	public:
		// ������ ����������� ������ ������������� ���� ����(����� �����, � ��� ������).
		// �� ��� �� ������ ������ ����,�.� ��� ���� ����� ��������������� ������ ����� �������� �������
		ENGINE_INLINE MFileNameEditorBase::MFileNameEditorBase() {
			filename = "";
		}

		ENGINE_INLINE virtual ~MFileNameEditorBase() {}

		ENGINE_INLINE MFileNameEditorBase::MFileNameEditorBase(System::String^_filename) {
			Filename = _filename;
		}

		ENGINE_INLINE virtual void InitializeDialog(System::Windows::Forms::OpenFileDialog^ openFileDialog) override
		{
			//!@todo Editor:EngineCLR:��� ���� ����� ImportDialog
			openFileDialog->Multiselect = false;
			openFileDialog->Filter = "All|*.*";
			Filename = openFileDialog->FileName;
		}

		// � ����� ���������� ����, � �� ��� ������
		ENGINE_INLINE virtual System::String^ ToString() override {
			return filename;
		}

		// ��� ������������ ����� ����������
		static ENGINE_INLINE operator MFileNameEditorBase ^ (System::String^ _string) {
			return gcnew MFileNameEditorBase(_string);
		}
		// �� ������ �����, �� ����������
		static ENGINE_INLINE operator System::String ^ (MFileNameEditorBase^ _editor) {
			return gcnew System::String(_editor->Filename);
		}

		ENGINE_INLINE operator System::String ^ () {
			return gcnew System::String(this->Filename);
		}
	};
}
#endif