/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#ifndef DROP_EDITOR
#include "../../Engine/inc/Light.h"
#include "ManagedEditableObject.h"

using namespace System;
using namespace NGTech;

namespace EngineCLR
{
	public ref class MObjectLight :public MEditableObject
	{
	internal:
		ENGINE_INLINE MObjectLight(NGTech::Light* _light) {
			m_edObj = _light;
		}
	public:
		ENGINE_INLINE MObjectLight(NGTech::LightSpot obj) {
			m_edObj = new NGTech::LightSpot();
			_SetLightBufferDataByGet<LightSpot*>(&obj);
		}
		ENGINE_INLINE MObjectLight(NGTech::LightPoint obj) {
			m_edObj = new NGTech::LightPoint();
			_SetLightBufferDataByGet<LightPoint*>(&obj);
		}
		ENGINE_INLINE MObjectLight(NGTech::LightDirect obj) {
			m_edObj = new NGTech::LightDirect();
			_SetLightBufferDataByGet<LightDirect*>(&obj);
		}

		ENGINE_INLINE MObjectLight(NGTech::LightBufferData::LightType _type) {
			m_edObj = API_CreateLight(_type);
		}

		ENGINE_INLINE MObjectLight() {
			m_edObj = API_CreateLight(LightBufferData::LIGHT_OMNI);
		}

		ENGINE_INLINE MObjectLight(MObjectLight^ _light) {
			auto actor = static_cast<Light*>(_light->m_edObj);
			if (!actor)
			{
				throw gcnew System::Exception("invalid cast pointer m_edObj to actor in " + __FUNCTION__);
				return;
			}
			m_edObj = API_CreateLight(actor->GetType());
		}

		virtual property bool IsFrozen
		{
			ENGINE_INLINE virtual bool get() override {
				return false;
			}
		}

		virtual property bool IsSelectable
		{
			ENGINE_INLINE virtual bool get() override {
				return true;
			}
		}

		virtual property bool IsPrefab
		{
			ENGINE_INLINE virtual bool get() override {
				return false;
			}
		}

		virtual property bool IsLight
		{
			ENGINE_INLINE virtual bool get() override {
				return true;
			}
		}

		property NGTech::LightBufferData::LightType LightType
		{
			ENGINE_INLINE NGTech::LightBufferData::LightType get() {
				CLIASSERT(m_edObj, "Invalid pointer");

				auto actor = static_cast<Light*>(m_edObj);
				if (!actor)
				{
					throw gcnew System::Exception("invalid cast pointer m_edObj to actor in " + __FUNCTION__);
					return NGTech::LightBufferData::LightType::LIGHT_ERROR;
				}
				return actor->GetType();
			}
		}

	private:
		template<class T, class P1> ENGINE_INLINE void _SetLightBufferDataByGet(P1 obj)
		{
			CLIASSERT(m_edObj, "Invalid pointer");

			auto actor = static_cast<T>(m_edObj);

			CLIASSERT(actor, "Invalid cast");
			CLIASSERT(obj, "Invalid pointer");

			actor->SetLightBufferData(obj->GetLightBufferData());
		}
	};
}
#endif