/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#ifndef DROP_EDITOR
#include "../../Engine/inc/Object.h"
#include "ManagedEditableObject.h"

using namespace System;
using namespace NGTech;

namespace EngineCLR
{
	public ref class MObjectMesh :public MEditableObject
	{
	internal:
		ENGINE_INLINE MObjectMesh(NGTech::ObjectMesh* vector) {
			m_edObj = vector;
		}

	public:
		//!@todo Clean
		//ENGINE_INLINE MObjectMesh(NGTech::ObjectMesh obj) {
		//	m_edObj = new NGTech::ObjectMesh(obj.GetLoadingPath());
		//}

		ENGINE_INLINE MObjectMesh(System::String^ str) {
			m_edObj = new NGTech::ObjectMesh(Helpers::ToCppString(str).c_str());
		}

		ENGINE_INLINE MObjectMesh(NGTech::String str) {
			m_edObj = new NGTech::ObjectMesh(str.c_str());
		}

		ENGINE_INLINE MObjectMesh() {
			// ������� ��������� ������, ��� ���������� ��� � ������ �����
			m_edObj = new NGTech::ObjectMesh(false);
		}

		ENGINE_INLINE MObjectMesh(MObjectMesh^ vector) {
			m_edObj = vector->m_edObj;
		}

		ENGINE_INLINE virtual ~MObjectMesh() {}

		property System::String^ LoadingPath
		{
			System::String^ get()
			{
				auto actor = static_cast<ObjectMesh*>(m_edObj);
				if (!actor)
				{
					throw gcnew System::Exception("invalid cast pointer m_edObj to actor in " + __FUNCTION__);
					return Helpers::ToCLIString(StringHelper::EMPTY_STRING);
				}

				return Helpers::ToCLIString(actor->GetLoadingPath());
			}

			void set(System::String^ _path)
			{
				auto actor = static_cast<ObjectMesh*>(m_edObj);
				if (!actor)
				{
					throw gcnew System::Exception("invalid cast pointer m_edObj to actor in " + __FUNCTION__);
					return;
				}

				auto path = Helpers::ToCppString(_path);

				actor->LoadFromPath(path);
			}
		}

		virtual property bool IsHidden
		{
			ENGINE_INLINE virtual bool get() override {
				auto actor = static_cast<ObjectMesh*>(m_edObj);
				if (!actor)
				{
					throw gcnew System::Exception("invalid cast pointer m_edObj to actor in " + __FUNCTION__);
					return false;
				}
				return !actor->GetEnabled();
			}

			ENGINE_INLINE virtual void set(bool _v) override {
				auto actor = static_cast<ObjectMesh*>(m_edObj);
				if (!actor)
				{
					throw gcnew System::Exception("invalid cast pointer m_edObj to actor in " + __FUNCTION__);
					return;
				}

				actor->SetEnabled(_v);
			}
		}

		virtual property bool IsSelectable
		{
			ENGINE_INLINE virtual bool get() override {
				return true;
			}
		}

		virtual property bool IsPrefab
		{
			ENGINE_INLINE virtual bool get() override {
				return true;
			}
		}

		virtual property bool IsLight
		{
			ENGINE_INLINE virtual bool get() override {
				return false;
			}
		}

		System::String^ GetMaterialsAsString()
		{
			auto actor = static_cast<ObjectMesh*>(m_edObj);
			if (!actor)
			{
				throw gcnew System::Exception("invalid cast pointer m_edObj to actor in " + __FUNCTION__);
				return Helpers::ToCLIString(StringHelper::EMPTY_STRING);
			}

			return Helpers::ToCLIString(actor->GetMaterialsAsString());
		}
	};
}

#endif