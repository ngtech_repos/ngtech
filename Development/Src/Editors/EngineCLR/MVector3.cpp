/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "stdafx.h"

#ifndef DROP_EDITOR
#include "MVector3.h"

namespace EngineCLR
{
	MVec3^ MVec3::operator-(MVec3^ first)
	{
		return gcnew MVec3(-first->m_vector->x, -first->m_vector->y, -first->m_vector->z);
	}

	bool MVec3::operator!=(MVec3^ first, MVec3^ second)
	{
		if (Object::ReferenceEquals(first, nullptr) &&
			Object::ReferenceEquals(second, nullptr))
			return false;
		if (!Object::ReferenceEquals(first, nullptr) &&
			Object::ReferenceEquals(second, nullptr))
			return true;
		if (Object::ReferenceEquals(first, nullptr) &&
			!Object::ReferenceEquals(second, nullptr))
			return true;
		return *first->m_vector != *second->m_vector;
	}

	MVec3^ MVec3::operator/(MVec3^ first, float second)
	{
		return gcnew MVec3(first->m_vector->x / second,
			first->m_vector->y / second,
			first->m_vector->z / second);
	}

	MVec3^ MVec3::operator*(MVec3^ first, MVec3^ second)
	{
		return gcnew MVec3(first->m_vector->x * second->m_vector->x,
			first->m_vector->y * second->m_vector->y,
			first->m_vector->z * second->m_vector->z);
	}

	MVec3^ MVec3::operator*(float second, MVec3^ first)
	{
		return gcnew MVec3(first->m_vector->x * second,
			first->m_vector->y * second,
			first->m_vector->z * second);
	}

	MVec3^ MVec3::operator*(MVec3^ first, float second)
	{
		return gcnew MVec3(first->m_vector->x * second,
			first->m_vector->y * second,
			first->m_vector->z * second);
	}

	MVec3^ MVec3::operator-(MVec3^ first, MVec3^ second)
	{
		return gcnew MVec3(first->m_vector->x - second->m_vector->x,
			first->m_vector->y - second->m_vector->y,
			first->m_vector->z - second->m_vector->z);
	}

	MVec3^ MVec3::operator+(MVec3^ first, MVec3^ second)
	{
		return gcnew MVec3(first->m_vector->x + second->m_vector->x,
			first->m_vector->y + second->m_vector->y,
			first->m_vector->z + second->m_vector->z);
	}

	void MVec3::SetFromString(System::String^ theString)
	{
		_SetFromString(theString, 3);
	}

	bool MVec3::operator==(MVec3^ first, MVec3^ second)
	{
		if (Object::ReferenceEquals(first, nullptr) &&
			Object::ReferenceEquals(second, nullptr))
			return true;
		if (!Object::ReferenceEquals(first, nullptr) &&
			Object::ReferenceEquals(second, nullptr))
			return false;
		if (Object::ReferenceEquals(first, nullptr) &&
			!Object::ReferenceEquals(second, nullptr))
			return false;
		return *first->m_vector == *second->m_vector;
	}

	void MVec3::Set(MVec3^ source)
	{
		*m_vector = *source->m_vector;
	}

	bool MVec3::Equals(Object^ obj)
	{
		if (obj == nullptr)
			return false;
		MVec3^ other = (MVec3^)obj;
		if (m_vector->x == other->m_vector->x &&
			m_vector->y == other->m_vector->y &&
			m_vector->z == other->m_vector->z)
			return true;
		return false;
	}

	MVec3^ MVec3::FromString(System::String^ theString)
	{
		MVec3^ vector = gcnew MVec3();

		vector->SetFromString(theString);

		return vector;
	}
}

#endif