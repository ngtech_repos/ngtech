/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#ifndef DROP_EDITOR

#include "../../Common/MathLib.h"
#include "MVector4.h"

using namespace NGTech;
namespace EngineCLR
{
	public ref class MVec3 :public MVec4
	{
	public:
		MVec3(Vec3 * vector) :MVec4(new Vec4(vector->x, vector->y, vector->z, 0))
		{}
		//PROPERTIES
	public:
		/// <summary>
		/// ctor
		/// </summary>
		MVec3(Vec3 vector) :MVec4(Vec4(vector))
		{}

		/// <summary>
		/// ctor
		/// </summary>
		MVec3() :MVec4()
		{}

		/// <summary>
		/// ctor -- set from components
		/// </summary>
		MVec3(float X, float Y, float Z) :MVec4(X, Y, Z, 0)
		{}

		/// <summary>
		/// ctor -- copies another vectors _values_
		/// </summary>
		MVec3(MVec3^ vector) :MVec4(vector)
		{}

		MVec3(System::String^ theString)
			:MVec3()
		{
			SetFromString(theString);
		}

		virtual void SetFromString(System::String^ theString) override;

		// ��� ������������ ����� ����������
		static ENGINE_INLINE operator MVec3 ^ (System::String^ _string) {
			return FromString(_string);
		}
		// �� ������ �����, �� ����������
		static ENGINE_INLINE operator System::String ^ (MVec3^ _vector) {
			return gcnew System::String(_vector->ToString());
		}

		ENGINE_INLINE operator System::String ^ () {
			return gcnew System::String(ToString());
		}

		virtual System::String^ ToString(System::IFormatProvider^ provider) override {
			return ToString();
		}

		virtual System::Object^ ToType() override { return this; }
		virtual System::Object^ ToType(System::Type ^, System::IFormatProvider ^) override { return this; }

		/*	property System::Object^ Value { System::Object^ get() { return this->ToString(); } void set(System::Object^ _v) { } }*/

	protected:
		FINALIZER(MVec3)
		{
			if (needToDelete &&	m_vector)
			{
				SAFE_DELETE(m_vector);
			}
		}

		virtual ~MVec3()
		{
			SAFE_DELETE(m_vector);
		}
	public:
		/// <summary>
		/// x component
		/// </summary>
		property float X
#ifdef DOXYGEN_IGNORE
			; _()
#endif
		{
			float get()
			{
				return m_vector->x;
			}
			void set(float value)
			{
				m_vector->x = value;
			}
		}

		/// <summary>
		/// y component
		/// </summary>
		property float Y
#ifdef DOXYGEN_IGNORE
			; _()
#endif
		{
			float get()
			{
				return m_vector->y;
			}
			void set(float value)
			{
				m_vector->y = value;
			}
		}

		/// <summary>
		/// z component
		/// </summary>
		property float Z
#ifdef DOXYGEN_IGNORE
			; _()
#endif
		{
			float get()
			{
				return m_vector->z;
			}
			void set(float value)
			{
				m_vector->z = value;
			}
		}

		/// <summary>
		/// w component
		/// </summary>
		property float W
#ifdef DOXYGEN_IGNORE
			; _()
#endif
		{
			float get()
			{
				return 0;
			}
			void set(float value)
			{
			}
		}

		// OPERATOR OVERLOADS
	public:

		static MVec3^ operator + (MVec3^ first, MVec3^ second);

		static MVec3^ operator - (MVec3^ first, MVec3^ second);

		static MVec3^ operator * (MVec3^ first, float second);
		static MVec3^ operator * (float second, MVec3^ first);

		static MVec3^ operator * (MVec3^ first, MVec3^ second);

		static MVec3^ operator / (MVec3^ first, float second);

		static bool operator == (MVec3^ first, MVec3^ second);

		static bool operator != (MVec3^ first, MVec3^ second);

		static MVec3^ operator - (MVec3^ first);

		/// <summary>
		/// Sets this vector's _values_ from another vector object
		/// </summary>
		void Set(MVec3^ source);

		virtual bool Equals(Object^ obj) override;

		/// <summary>
		/// Creates the following string: (x, y, z).
		/// </summary>
		virtual System::String^ ToString() override {
			//return gcnew System::String("Separate");
			return gcnew System::String(X + "; " + Y + "; " + Z);
		}

		static MVec3^ FromString(System::String^ theString);
	};
}

#endif