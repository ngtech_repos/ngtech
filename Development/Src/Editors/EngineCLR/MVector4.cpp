/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "stdafx.h"

#ifndef DROP_EDITOR

#include "MVector4.h"

using namespace EngineCLR;

MVec4::MVec4(Vec4 * vector)
{
	m_vector = vector;
	needToDelete = false;
}

MVec4::MVec4(Vec4 vector)
{
	m_vector = new Vec4(vector.x, vector.y, vector.z, vector.w);
	needToDelete = true;
}

MVec4::MVec4()
{
	m_vector = new Vec4(0, 0, 0, 0);
	needToDelete = true;
}

MVec4::MVec4(float X, float Y, float Z, float W)
{
	m_vector = new Vec4(X, Y, Z, W);
	needToDelete = true;
}

MVec4::MVec4(MVec4^ vector)
{
	m_vector = new Vec4();
	*m_vector = *vector->m_vector;
	needToDelete = true;
}

MVec4::MVec4(System::String^ theString) :MVec4()
{
	SetFromString(theString);
}

void MVec4::SetFromString(System::String^ theString)
{
	_SetFromString(theString, 4);
}

void MVec4::_SetFromString(System::String^ theString, unsigned long elemCount)
{
	array<System::String^, 1>^ separator = gcnew array<System::String^, 1>(1);
	separator[0] = L",";
	array<System::String^>^ splits = theString->Replace(L"f", L"")->Split(separator, System::StringSplitOptions::RemoveEmptyEntries);
	if (splits->Length < elemCount)
		return;

	/*TODO:Scripting: Rewrite when refactor math lib*/
	for (auto i = 0; i < elemCount; i++)
	{
		if (i == 0)
			m_vector->x = (float)Convert::ToDouble(splits[i]);
		else if (i == 1)
			m_vector->y = (float)Convert::ToDouble(splits[i]);
		else if (i == 2)
			m_vector->z = (float)Convert::ToDouble(splits[i]);
		else if (i == 3)
			m_vector->w = (float)Convert::ToDouble(splits[i]);
		else
			System::Exception("INVALID INDEX");
	}
}

bool MVec4::Equals(Object^ obj)
{
	if (obj == nullptr)
		return false;
	MVec4^ other = (MVec4^)obj;
	if (m_vector->x == other->m_vector->x &&
		m_vector->y == other->m_vector->y &&
		m_vector->z == other->m_vector->z &&
		m_vector->w == other->m_vector->w)
		return true;

	return false;
}

MVec4^ MVec4::FromString(System::String^ theString)
{
	MVec4^ vector = gcnew MVec4();

	vector->SetFromString(theString);

	return vector;
}

bool MVec4::operator==(MVec4^ first, MVec4^ second)
{
	if (Object::ReferenceEquals(first, nullptr) &&
		Object::ReferenceEquals(second, nullptr))
		return true;

	if (!Object::ReferenceEquals(first, nullptr) &&
		Object::ReferenceEquals(second, nullptr))
		return false;

	if (Object::ReferenceEquals(first, nullptr) &&
		!Object::ReferenceEquals(second, nullptr))
		return false;

	if (Object::ReferenceEquals(first, nullptr) &&
		!Object::ReferenceEquals(second, nullptr))
		return false;

	return *first->m_vector == *second->m_vector;
}

bool MVec4::operator!=(MVec4^ first, MVec4^ second)
{
	if (Object::ReferenceEquals(first, nullptr) &&
		Object::ReferenceEquals(second, nullptr))
		return false;

	if (!Object::ReferenceEquals(first, nullptr) &&
		Object::ReferenceEquals(second, nullptr))
		return true;

	if (Object::ReferenceEquals(first, nullptr) &&
		!Object::ReferenceEquals(second, nullptr))
		return true;

	if (Object::ReferenceEquals(first, nullptr) &&
		!Object::ReferenceEquals(second, nullptr))
		return true;

	return *first->m_vector != *second->m_vector;
}

MVec4^ MVec4::operator-(MVec4^ first)
{
	return gcnew MVec4(-first->m_vector->x, -first->m_vector->y, -first->m_vector->z, -first->m_vector->w);
}

MVec4^ MVec4::operator/(MVec4^ first, float second)
{
	return gcnew MVec4(first->m_vector->x / second,
		first->m_vector->y / second,
		first->m_vector->z / second,
		first->m_vector->w / second);
}

MVec4^ MVec4::operator*(MVec4^ first, MVec4^ second)
{
	return gcnew MVec4(first->m_vector->x * second->m_vector->x,
		first->m_vector->y * second->m_vector->y,
		first->m_vector->z * second->m_vector->z,
		first->m_vector->w * second->m_vector->w);
}

MVec4^ MVec4::operator*(float second, MVec4^ first)
{
	return gcnew MVec4(first->m_vector->x * second,
		first->m_vector->y * second,
		first->m_vector->z * second,
		first->m_vector->w * second);
}

MVec4^ MVec4::operator*(MVec4^ first, float second)
{
	return gcnew MVec4(first->m_vector->x * second,
		first->m_vector->y * second,
		first->m_vector->z * second,
		first->m_vector->w * second);
}

MVec4^ MVec4::operator-(MVec4^ first, MVec4^ second)
{
	return gcnew MVec4(first->m_vector->x - second->m_vector->x,
		first->m_vector->y - second->m_vector->y,
		first->m_vector->z - second->m_vector->z,
		first->m_vector->w - second->m_vector->w);
}

MVec4^ MVec4::operator+(MVec4^ first, MVec4^ second)
{
	return gcnew MVec4(first->m_vector->x + second->m_vector->x,
		first->m_vector->y + second->m_vector->y,
		first->m_vector->z + second->m_vector->z,
		first->m_vector->w + second->m_vector->w);
}

#endif