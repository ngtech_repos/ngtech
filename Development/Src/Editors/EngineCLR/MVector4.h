/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#ifndef DROP_EDITOR
#include "../../Common/MathLib.h"

using namespace NGTech;
namespace EngineCLR
{
	public ref class MVec4 : public IConvertible
	{
	public:
		Vec4*		m_vector;
		bool		needToDelete;

		MVec4(Vec4 * vector);
		//PROPERTIES
	public:
		/// <summary>
		/// ctor
		/// </summary>
		MVec4(Vec4 vector);

		/// <summary>
		/// ctor
		/// </summary>
		MVec4();

		/// <summary>
		/// ctor -- set from components
		/// </summary>
		MVec4(float X, float Y, float Z, float W);

		/// <summary>
		/// ctor -- copies another vectors _values_
		/// </summary>
		MVec4(MVec4^ vector);

		MVec4(System::String^ theString);

		virtual void SetFromString(System::String^ theString);

		void _SetFromString(System::String^ theString, unsigned long elemCount);

		// ��� ������������ ����� ����������
		static ENGINE_INLINE operator MVec4 ^ (System::String^ _string) {
			return gcnew MVec4(_string);
		}
		// �� ������ �����, �� ����������
		static ENGINE_INLINE operator System::String ^ (MVec4^ _editor) {
			Warning(__FUNCTION__);
			return gcnew System::String(/*_editor*/"1");
		}

		ENGINE_INLINE operator System::String ^ () {
			Warning(__FUNCTION__);
			return gcnew System::String("1");
		}

		virtual System::TypeCode GetTypeCode() override {
			Warning(__FUNCTION__);
			return System::TypeCode::Object;
		}

		virtual System::String^ ToString(System::IFormatProvider^ provider) override {
			Warning(__FUNCTION__);
			return ToString();
		}

		virtual bool ToBoolean(System::IFormatProvider^ provider) override {
			Warning(__FUNCTION__);
			return false;
		}

		virtual System::DateTime ToDateTime(System::IFormatProvider^ provider) override {
			Warning(__FUNCTION__);
			return System::DateTime::UtcNow;
		}

		virtual double ToDouble(System::IFormatProvider^ provider) override {
			Warning(__FUNCTION__);
			return 0;
		}

		virtual float ToSingle() override {
			throw gcnew Exception(__FUNCTION__);
			return 0;
		}
		virtual float ToSingle(System::IFormatProvider ^) override {
			Warning(__FUNCTION__);
			return 0;
		}

		virtual short ToInt16() override {
			Warning(__FUNCTION__);
			return 0;
		}
		virtual short ToInt16(System::IFormatProvider ^) override {
			Warning(__FUNCTION__);
			return 0;
		}
		virtual int ToInt32(System::IFormatProvider ^) override {
			Warning(__FUNCTION__);
			return 0;
		}
		virtual __int64 ToInt64(System::IFormatProvider ^) override {
			Warning(__FUNCTION__);
			return 0;
		}

		virtual signed char ToSByte() override {
			Warning(__FUNCTION__);
			return 0;
		}
		virtual signed char ToSByte(System::IFormatProvider ^) override {
			Warning(__FUNCTION__);
			return 0;
		}

		virtual Decimal ToDecimal() override {
			Warning(__FUNCTION__);
			return 0;
		}
		virtual Decimal ToDecimal(System::IFormatProvider ^) override {
			Warning(__FUNCTION__);
			return 0;
		}

		virtual System::Object^ ToType() override {
			Warning(__FUNCTION__);
			return this->ToString();
		}
		virtual System::Object^ ToType(System::Type ^, System::IFormatProvider ^) override {
			Warning(__FUNCTION__);
			return this->ToString();
		}

		virtual unsigned char ToByte(System::IFormatProvider ^) override {
			Warning(__FUNCTION__);
			return 0;
		}

		virtual unsigned short ToUInt16(System::IFormatProvider ^) override {
			Warning(__FUNCTION__);
			return 0;
		}
		virtual unsigned int ToUInt32(System::IFormatProvider ^) override {
			Warning(__FUNCTION__);
			return 0;
		}
		virtual unsigned __int64 ToUInt64(System::IFormatProvider ^) override {
			Warning(__FUNCTION__);
			return 0;
		}

		virtual Char ToChar(System::IFormatProvider^) override {
			Warning(__FUNCTION__);
			return Char("");
		}

	protected:
		FINALIZER(MVec4)
		{
			if (needToDelete &&	m_vector)
			{
				SAFE_DELETE(m_vector);
			}
		}

		virtual ~MVec4()
		{
			SAFE_DELETE(m_vector);
		}
	public:
		/// <summary>
		/// x component
		/// </summary>
		property float X
#ifdef DOXYGEN_IGNORE
			; _()
#endif
		{
			float get()
			{
				return m_vector->x;
			}
			void set(float value)
			{
				m_vector->x = value;
			}
		}

		/// <summary>
		/// y component
		/// </summary>
		property float Y
#ifdef DOXYGEN_IGNORE
			; _()
#endif
		{
			float get()
			{
				return m_vector->y;
			}
			void set(float value)
			{
				m_vector->y = value;
			}
		}

		/// <summary>
		/// z component
		/// </summary>
		property float Z
#ifdef DOXYGEN_IGNORE
			; _()
#endif
		{
			float get()
			{
				return m_vector->z;
			}
			void set(float value)
			{
				m_vector->z = value;
			}
		}

		/// <summary>
		/// w component
		/// </summary>
		property float W
#ifdef DOXYGEN_IGNORE
			; _()
#endif
		{
			float get()
			{
				return m_vector->w;
			}
			void set(float value)
			{
				m_vector->w = value;
			}
		}

		// OPERATOR OVERLOADS
	public:

		static MVec4^ operator + (MVec4^ first, MVec4^ second);

		static MVec4^ operator - (MVec4^ first, MVec4^ second);

		static MVec4^ operator * (MVec4^ first, float second);
		static MVec4^ operator * (float second, MVec4^ first);

		static MVec4^ operator * (MVec4^ first, MVec4^ second);

		static MVec4^ operator / (MVec4^ first, float second);

		static bool operator == (MVec4^ first, MVec4^ second);

		static bool operator != (MVec4^ first, MVec4^ second);

		static MVec4^ operator - (MVec4^ first);

		/// <summary>
		/// Sets this vector's _values_ from another vector object
		/// </summary>
		void Set(MVec4^ source)
		{
			*m_vector = *source->m_vector;
		}

		virtual bool Equals(Object^ obj) override;

		/// <summary>
		/// Creates the following string: (x, y, z, w).
		/// </summary>
		virtual System::String^ ToString() override {
			Warning(__FUNCTION__);
			return gcnew System::String(X + ", " + Y + ", " + Z + ", " + W);
		}

		static MVec4^ FromString(System::String^ theString);
	};
}

#endif