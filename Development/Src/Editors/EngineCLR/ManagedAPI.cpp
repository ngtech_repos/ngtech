/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "stdafx.h"

#ifndef DROP_EDITOR
#include "..\..\..\API\NGTechEngineAPI.h"
#include "..\..\Engine\inc\EditorObjectManager.h"
using namespace NGTech;

#include "ManagedAPI.h"
#include "ManagedEditableObject.h"

namespace EngineCLR {
	void api::Make_ScreenShot() {
		API_Make_ScreenShot();
	}

	void api::EDITOR_CheckWhatWasModificated() {
		API_EditorCheckWhatWasModificated();
	}

	void api::EDITOR_PauseEngine(bool _v) {
		API_EditorPauseEngine(_v);
	}

	bool api::EDITOR_IsPausedEngine() {
		// info:https://stackoverflow.com/questions/18591924/how-to-use-bitmask
		bool _paused = ((GetEngineState() & (STATE_PAUSED | STATE_PHYSICS_PAUSED | STATE_EDITOR_PAUSED)) == (STATE_PAUSED | STATE_PHYSICS_PAUSED | STATE_EDITOR_PAUSED));
		return _paused;
	}

	void CacheApi::ReloadShaders() {
		auto cache = GetCache();
		if (!cache) {
			throw gcnew System::NotImplementedException();
			return;
		}
		CLIASSERT(cache != nullptr, "Not exist cache");

		cache->ReloadShaders();
	}

	void CacheApi::ReloadTextures() {
		auto cache = GetCache();
		if (!cache) {
			throw gcnew System::NotImplementedException();
			return;
		}
		CLIASSERT(cache != nullptr, "Not exist cache");

		cache->ReloadTextures();
	}

	void CacheApi::ReloadMeshes() {
		auto cache = GetCache();
		if (!cache) {
			throw gcnew System::NotImplementedException();
			return;
		}
		CLIASSERT(cache != nullptr, "Not exist cache");

		cache->ReloadMeshes();
	}

	void CacheApi::ReloadAnimatedMeshes() {
		auto cache = GetCache();
		if (!cache) {
			throw gcnew System::NotImplementedException();
			return;
		}
		CLIASSERT(cache != nullptr, "Not exist cache");

		cache->ReloadAnimatedMeshes();
	}

	void CacheApi::ReloadSounds() {
		auto cache = GetCache();
		if (!cache) {
			throw gcnew System::NotImplementedException();
			return;
		}
		CLIASSERT(cache != nullptr, "Not exist cache");

		cache->ReloadSounds();
	}

	void CacheApi::ReloadAll() {
		auto cache = GetCache();
		if (!cache) {
			throw gcnew System::NotImplementedException();
			return;
		}
		CLIASSERT(cache != nullptr, "Not exist cache");

		cache->ReloadAll();
	}

	/**/
	ENGINE_INLINE void RenderApi::EnableDisableWireframe(bool _use) {
		API_EnableDisableWireframe(_use);
	}

	ENGINE_INLINE void RenderApi::SetDebugDrawMode(unsigned int _mode) {
		API_SetDebugDrawMode(_mode);
	}

	ENGINE_INLINE void RenderApi::VisualizateBRDF(bool _) {
		SetDebugDrawMode(SceneMatrixes::BRDF_VIS);
	}

	ENGINE_INLINE void RenderApi::VisualizateDiffuse() {
		SetDebugDrawMode(SceneMatrixes::DIFFUSE_VIS);
	}

	ENGINE_INLINE void RenderApi::VisualizateNormals() {
		SetDebugDrawMode(SceneMatrixes::NORMALS_VISG);
	}

	ENGINE_INLINE void RenderApi::VisualizatePositions() {
		SetDebugDrawMode(SceneMatrixes::POSITIONS_VIS);
	}

	ENGINE_INLINE void RenderApi::VisualizateRoughness() {
		SetDebugDrawMode(SceneMatrixes::ROUGHNESS_VIS);
	}

	ENGINE_INLINE void RenderApi::VisualizateSpecular() {
		SetDebugDrawMode(SceneMatrixes::SPECULAR_VIS);
	}

	ENGINE_INLINE void RenderApi::VisualizateSpecularPower() {
		SetDebugDrawMode(SceneMatrixes::SPECULARPOWER_VIS);
	}

	ENGINE_INLINE void RenderApi::VisualizateFinalLighting() {
		SetDebugDrawMode(SceneMatrixes::FINAL_LIGHTING);
	}

	ENGINE_INLINE void RenderApi::VisualizateDepth() {
		SetDebugDrawMode(SceneMatrixes::DEPTH_VIS);
	}

	ENGINE_INLINE const System::String^ CvarsCLR::GetGameName()
	{
		auto cvars = GetCvars();
		if (!cvars)
		{
			ASSERT("CVARS is nullptr");
			return Helpers::EMPTY_STRING;
		}

		return Helpers::ToCLIString(cvars->sys_game_folder);
	}

	ENGINE_INLINE const System::String^ api::GetProjectNameAndVersion()
	{
		return PROJECT_TITLE + " " + ENGINE_VERSION_STRING;
	}

	ENGINE_INLINE size_t EditableObjectAPI::EditableObjectCount()
	{
		auto objManager = GetEditorObjectManager();

		if (!objManager)
			return 0;

		return objManager->GetCount();
	}

	ENGINE_INLINE MEditableObject^ EditableObjectAPI::GetEditableObject(size_t index)
	{
		auto objManager = GetEditorObjectManager();

		if (!objManager)
			return nullptr;

		return gcnew MEditableObject(objManager->GetObjById(index));
	}

	ENGINE_INLINE void log::LogPrintf(System::String^ _s)
	{
		using namespace NGTech;
		NGTech::LogPrintf(Helpers::ToCppString(_s).c_str());
	}

	ENGINE_INLINE void log::Warning(System::String^ _s)
	{
		using namespace NGTech;
		NGTech::Warning(Helpers::ToCppString(_s).c_str());
	}

	ENGINE_INLINE void log::ErrorWrite(System::String^ _s)
	{
		using namespace NGTech;
		NGTech::ErrorWrite(Helpers::ToCppString(_s).c_str());
	}

	ENGINE_INLINE void log::DebugPrintf(System::String^ _s)
	{
		using namespace NGTech;
		NGTech::DebugM(Helpers::ToCppString(_s).c_str());
	}
}

#endif