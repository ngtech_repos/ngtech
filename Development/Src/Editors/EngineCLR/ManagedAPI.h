/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#ifndef DROP_EDITOR

namespace EngineCLR {
	ref class MEditableObject;

	public ref class api
	{
	public:
		static void Make_ScreenShot();
		/// Refresh all params
		static void EDITOR_CheckWhatWasModificated();
		static bool EDITOR_IsPausedEngine();

		static void EDITOR_PauseEngine(bool _v);
		static const System::String^ GetProjectNameAndVersion();
	};

	public ref class EditableObjectAPI
	{
	public:
		// Editable Object
		static size_t EditableObjectCount();
		static MEditableObject^ GetEditableObject(size_t index);
	};

	public ref class CacheApi
	{
	public:
		/*!@ Recompile All Shaders*/
		static void ReloadShaders();
		/*!@ Reload All Textures*/
		static void ReloadTextures();
		/*!@ Reload All Meshes*/
		static void ReloadMeshes();
		/*!@ Reload All Animated Meshes*/
		static void ReloadAnimatedMeshes();
		/*!@ Reload All Sounds*/
		static void ReloadSounds();
		/*!@ Reload All resources*/
		static void ReloadAll();
	};

	public ref class RenderApi
	{
	public:
		static void EnableDisableWireframe(bool _use);
		/**/
		static void VisualizateBRDF(bool _);
		static void VisualizateDiffuse();
		static void VisualizateNormals();
		static void VisualizatePositions();
		static void VisualizateRoughness();
		static void VisualizateSpecular();
		static void VisualizateSpecularPower();
		static void VisualizateFinalLighting();
		static void VisualizateDepth();
	private:
		static void SetDebugDrawMode(unsigned int _mode);
	};

	public ref class log {
	public:
		static void LogPrintf(System::String^ _s);
		static void Warning(System::String^ _s);
		static void ErrorWrite(System::String^ _s);
		static void DebugPrintf(System::String^ _s);
	};

	public ref class CvarsCLR
	{
	public:
		static const System::String^ GetGameName();
	};
}

#endif