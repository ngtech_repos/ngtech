/* Copyright (C) 2009-2017, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#ifndef DROP_EDITOR
#include "../../Engine/inc/EditableObject.h"
#include "MActor.h"

namespace EngineCLR
{
	/// <summary>
	/// Wraps and provides access to the capabilities of NGTech's Editable object., Used as interface
	/// </summary>
	public ref class MEditableObject :public MActor
	{
	internal:
		EditableObject * m_edObj = nullptr;
		bool needToDelete = false;
	public:

		/// <summary>
		/// ctor wraps an existing unmanaged pointer
		/// </summary>
		MEditableObject(EditableObject* _obj);
		MEditableObject();

		ENGINE_INLINE virtual std::vector<EditorVar>& GetEditorVars();

		ENGINE_INLINE virtual System::String^ GetGUIDString();
	protected:
		/// <summary>
		/// dtor
		/// </summary>
		FINALIZER(MEditableObject)
		{
			if (needToDelete && m_edObj)
			{
				SAFE_DELETE(m_edObj);
			}
		}

		ENGINE_INLINE virtual ~MEditableObject();
		// Interface
	public:
		virtual property System::String^ ClassName
		{
			ENGINE_INLINE virtual System::String^ get()
			{
				if (!m_edObj)
				{
					throw gcnew System::Exception("invalid pointer m_edObj in " + __FUNCTION__);
					return Helpers::ToCLIString(StringHelper::EMPTY_STRING);
				}
				return Helpers::ToCLIString(m_edObj->ClassName());
			}
		}

		virtual property bool IsSelectable
		{
			ENGINE_INLINE virtual bool get() {
				if (m_edObj && m_edObj->IsSelectable)
					return true;

				return false;
			}
		}

		virtual property bool IsSelected
		{
			ENGINE_INLINE virtual bool get() {
				if (!m_edObj) {
					throw gcnew System::Exception("invalid pointer m_edObj in " + __FUNCTION__);
					return false;
				}
				return m_edObj->IsSelected;
			}

			ENGINE_INLINE virtual void set(bool value) {
				if (!m_edObj)
					return;

				m_edObj->IsSelected = value;
			}
		}

		virtual property bool IsHidden
		{
			// TODO:ADD CHECK
		// TODO: ����������� ����������� ��������� �������� � ���������
			ENGINE_INLINE virtual bool get() {
				//if (m_edObj && m_edObj->IsSelectable)
				//	return true;
				return false;
			}

			ENGINE_INLINE virtual void set(bool _v) {}
		}

		virtual property bool IsPrefab
		{
			ENGINE_INLINE virtual bool get() {
				if (m_edObj && m_edObj->IsPrefab)
					return true;
				return false;
			}
		}

		virtual property bool IsLight
		{
			ENGINE_INLINE virtual bool get() {
				if (m_edObj && m_edObj->IsLight)
					return true;
				return false;
			}
		}

		// TODO: ����������� ����������� ��������� �������� � ���������
		virtual property bool IsFrozen
		{
			ENGINE_INLINE virtual bool get() {
			/*	if (m_edObj && m_edObj->I)
					return true;*/
				return false;
			}
		}
	};
}
#endif