#include "stdafx.h"

#ifndef DROP_EDITOR
#include "ManagedMaterial.h"

namespace EngineCLR
{
	MMaterial::MMaterial(Material* material) :MEditableObject(material)
	{
		m_material = material;
		needToDelete = false;
	}

	MMaterial::MMaterial(System::String^ _name)
	{
		//TODO:last param false
		m_material = new Material(Helpers::ToCppString(_name), false, true);
		m_edObj = m_material;

		needToDelete = true;
	}

	std::vector<NGTech::EditorVar>& MMaterial::GetEditorVars()
	{
		CLIASSERT(m_material, "Invalid pointer on m_material");
		return m_material->GetEditorVars();
	}

	void MMaterial::Load(System::String^ MaterialFile)
	{
		CLIASSERT(m_material, "invalid pointer in %s", __FUNCTION__);
		m_material->Load(Helpers::ToCppString(MaterialFile).c_str());
	}

	void MMaterial::AddRef()
	{
		CLIASSERT(m_material, "invalid pointer in %s", __FUNCTION__);
		m_material->AddRef();
	}

	void MMaterial::Release()
	{
		SAFE_RELEASE(m_material);
		needToDelete = false;
	}
}
#endif