/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#ifndef DROP_EDITOR

#include "../../Engine/inc/Material.h"
#include "ManagedEditableObject.h"

namespace EngineCLR
{
	/// <summary>
	/// Wraps and provides access to the capabilities of NGTech's Material. A material wholly encapsulates the surface properties of any object
	/// </summary>
	public ref class MMaterial :public MEditableObject
	{
	internal:
		Material * m_material = nullptr;
		bool needToDelete = false;
	public:

		/// <summary>
		/// ctor wraps an existing unmanaged material
		/// </summary>
		MMaterial(Material* material);

		/// <summary>
		/// ctor creates a new NGTech Material to associate with this object
		/// </summary>
		MMaterial(System::String^ _name);

		virtual	std::vector<EditorVar>& GetEditorVars() override;

		/// <summary>
		/// Loads the material parameters from an external Material JSON file, expected to reside in "Materials" directory unless path otherwise specified.
		/// </summary>
		void Load(System::String^ MaterialFile);

		/// <summary>
		/// Adds one reference count to the Material -- A Material will only be auto-deleted when all its references are Released.
		/// </summary>
		void AddRef();

		/// <summary>
		/// Releases one reference count to the Material -- A Material will only be auto-deleted when all its references are Released.
		/// </summary>
		void Release();

		/// <summary>
		/// FileName of the Material
		/// </summary>
		property System::String^ FileName
#ifdef DOXYGEN_IGNORE
			; _()
#endif
		{
			System::String^ get()
			{
				CLIASSERT(m_material, "invalid pointer in %s", __FUNCTION__);
				return Helpers::ToCLIString(m_material->GetPath());
			}
			void set(System::String^ value)
			{
				CLIASSERT(m_material, "invalid pointer in %s", __FUNCTION__);

				m_material->SetPath(Helpers::ToCppString(value).c_str());
			}
		}

	protected:
		/// <summary>
		/// dtor
		/// </summary>
		FINALIZER(MMaterial)
		{
			if (needToDelete &&	m_material)
				SAFE_RELEASE(m_material);
		}

		ENGINE_INLINE virtual ~MMaterial()
		{
			SAFE_DELETE(m_material);
		}
	};
}

#endif