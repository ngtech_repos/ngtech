#include "stdafx.h"

#ifndef DROP_EDITOR
#include "MaterialNameEditor.h"

namespace EngineCLR
{
	void MaterialNameEditor::InitializeDialog(System::Windows::Forms::OpenFileDialog^ openFileDialog)
	{
		//!@todo Uncomment when i will change inherit class
		//MFileNameEditorBase::InitializeDialog(openFileDialog);

		openFileDialog->FileName = "";
		openFileDialog->InitialDirectory = InitialDirectory;
		openFileDialog->Filter = L"Material File (*.mat) |*.mat";
		openFileDialog->Title = L"Select Material";
		openFileDialog->RestoreDirectory = false;
	}
}

#endif