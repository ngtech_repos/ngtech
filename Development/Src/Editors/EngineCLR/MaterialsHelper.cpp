/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "stdafx.h"

#ifndef DROP_EDITOR
#include "MaterialsHelper.h"
#include "ManagedMaterial.h"
#include "PropertyHelper.h"

namespace EngineCLR {
	PropertyBags::PropertyBag^ MaterialsHelper::UpdateMaterialVars(System::String ^ uid) {
		PropertyHelper propHelper;
		propHelper.UpdateVars(GetMaterialEditorVars(uid));
		return propHelper.class1;
	}

	MMaterial^ MaterialsHelper::GetMaterial(System::String^ _name) {
		return gcnew MMaterial(_name);
	}

	std::vector<EditorVar>& MaterialsHelper::GetMaterialEditorVars(System::String^ _name) {
		auto ptr = GetMaterial(_name);
		CLIASSERT(ptr != nullptr, "Invalid pointer");

		return ptr->GetEditorVars();
	}
}

#endif