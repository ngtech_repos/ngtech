/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "stdafx.h"

#ifndef DROP_EDITOR

#pragma comment(lib,"user32.lib")

#include "MFileNameEditorBase.h"
#include "MFileNameEditor.h"
#include "MaterialNameEditor.h"
#include "TextureNameEditor.h"
#include "ShaderNameEditor.h"
#include "ScriptNameEditor.h"

namespace EngineCLR
{
	void PropertyHelper::UpdateVars(std::vector<EditorVar>& vars) {
		if (!currentVars) {
			throw gcnew System::Exception("[PropertyHelper::UpdateVars] currentVars is nullptr.");
			return;
		}

		currentVars->clear();

		if ((class1 == nullptr) || ((class1 != nullptr) && (class1->Properties == nullptr))) {
			throw gcnew System::Exception("[PropertyHelper::UpdateVars] class1 is nullptr or class1->Properties is nullptr.");
			return;
		}

		class1->Properties->Clear();

		if (vars.empty()) {
			throw gcnew System::Exception("[PropertyHelper::UpdateVars]-vars.empty()");
			return;
		}
		else
			currentVars->push_back(&vars);

		// Add script vars
		for (auto& valueRef : vars)
		{
			if (!&valueRef)
			{
				throw gcnew System::Exception("[PropertyHelper::UpdateVars]-not exist vars[i]");
				continue;
			}

			if (!valueRef.IsBrowsable())
				continue;

			Type ^ type = nullptr;

			auto _unamangedType = valueRef.GetType();

			if (_unamangedType == EditorVar::BOOL)
				type = bool::typeid;
			else if (_unamangedType == EditorVar::STRING)
				type = System::String::typeid;
			else if (_unamangedType == EditorVar::FILENAME)
				type = MFileNameEditor::typeid;
			else if (_unamangedType == EditorVar::TEXTURE)
				type = TextureNameEditor::typeid;
			else if (_unamangedType == EditorVar::MATERIAL)
				type = MaterialNameEditor::typeid;
			else if (_unamangedType == EditorVar::SHADER)
				type = ShaderNameEditor::typeid;
			else if (_unamangedType == EditorVar::SCRIPT)
				type = ScriptNameEditor::typeid;

			else if (_unamangedType == EditorVar::INT)
				type = int::typeid;
			else if (_unamangedType == EditorVar::UNINT)
				type = UInt32::typeid;
			else if (_unamangedType == EditorVar::FLOAT)
				type = Single::typeid;
			else if (_unamangedType == EditorVar::DOUBLE)
				type = Double::typeid;
			else if (_unamangedType == EditorVar::COLOR)
				type = System::Drawing::Color::typeid;
			else if (_unamangedType == EditorVar::FLOAT3)
				type = MVec3::typeid;
			else if (_unamangedType == EditorVar::FLOAT4)
				type = MVec4::typeid;

			PropertySpec^ spec = gcnew PropertySpec(gcnew System::String(valueRef.GetName().c_str()), type, gcnew System::String(valueRef.GetCategory().c_str()), gcnew System::String(valueRef.GetDesc().c_str()));

			if (spec == nullptr) {
				throw gcnew System::Exception("[PropertyHelper::UpdateVars]- not exist spec for vars[i]");
				continue;
			}

			if ((_unamangedType == EditorVar::INT || _unamangedType == EditorVar::UNINT ||
				_unamangedType == EditorVar::FLOAT || _unamangedType == EditorVar::DOUBLE) && valueRef.IsHasSlider())
			{
				Type^ ed = SliderEditor::typeid;
				spec->EditorTypeName = ed->AssemblyQualifiedName;
				SliderRange^ sRange = gcnew SliderRange();

				sRange->MaxValue = valueRef.GetSliderMax();
				sRange->MinValue = valueRef.GetSliderMin();
				sRange->Factor = valueRef.GetSliderFactor();

				spec->Attributes = gcnew array<Attribute^>(1);
				spec->Attributes[0] = sRange;
			}

			if (_unamangedType == EditorVar::FILENAME || _unamangedType == EditorVar::TEXTURE || _unamangedType == EditorVar::MATERIAL || _unamangedType == EditorVar::SHADER || _unamangedType == EditorVar::SCRIPT)
			{
				Type^ ed = nullptr;

				if (_unamangedType == EditorVar::MATERIAL)
					ed = MaterialNameEditor::typeid;
				else if (_unamangedType == EditorVar::TEXTURE)
					ed = TextureNameEditor::typeid;
				else if (_unamangedType == EditorVar::SHADER)
					ed = ShaderNameEditor::typeid;
				else if (_unamangedType == EditorVar::SCRIPT)
					ed = ScriptNameEditor::typeid;
				else
					ed = MFileNameEditor::typeid;

				spec->EditorTypeName = ed->AssemblyQualifiedName;
				spec->TypeName = ed->AssemblyQualifiedName;
			}
			class1->Properties->Add(spec);
		}
	}

	void PropertyHelper::UpdateVarsTwo(std::vector<EditorVar>& vars, std::vector<EditorVar>& vars2) {
		if (!currentVars) {
			throw gcnew System::Exception("[PropertyHelper::UpdateVars] currentVars is nullptr.");
			return;
		}

		currentVars->clear();

		if ((class1 == nullptr) || ((class1 != nullptr) && (class1->Properties == nullptr))) {
			throw gcnew System::Exception("[PropertyHelper::UpdateVars] class1 is nullptr or class1->Properties is nullptr.");
			return;
		}
		class1->Properties->Clear();

		if (!vars.empty())
			currentVars->push_back(&vars);

		if (!vars2.empty())
			currentVars->push_back(&vars2);

		// Add script vars
		if (!vars.empty())
		{
			for (auto& valueRef : vars)
			{
				if (!&valueRef)
				{
					throw gcnew System::Exception("[PropertyHelper::UpdateVarsTwo]-not exist vars[i]");
					continue;
				}

				Type ^ type = nullptr;

				auto _unamangedType = valueRef.GetType();

				if (_unamangedType == EditorVar::BOOL)
					type = bool::typeid;
				else if (_unamangedType == EditorVar::STRING)
					type = System::String::typeid;

				else if (_unamangedType == EditorVar::FILENAME)
					type = MFileNameEditor::typeid;
				else if (_unamangedType == EditorVar::TEXTURE)
					type = TextureNameEditor::typeid;
				else if (_unamangedType == EditorVar::MATERIAL)
					type = MaterialNameEditor::typeid;
				else if (_unamangedType == EditorVar::SHADER)
					type = ShaderNameEditor::typeid;
				else if (_unamangedType == EditorVar::SCRIPT)
					type = ScriptNameEditor::typeid;

				else if (_unamangedType == EditorVar::INT)
					type = int::typeid;
				else if (_unamangedType == EditorVar::UNINT)
					type = UInt32::typeid;
				else if (_unamangedType == EditorVar::FLOAT)
					type = Single::typeid;
				else if (_unamangedType == EditorVar::DOUBLE)
					type = Double::typeid;
				else if (_unamangedType == EditorVar::COLOR)
					type = System::Drawing::Color::typeid;
				else if (_unamangedType == EditorVar::FLOAT3)
					type = MVec3::typeid;
				else if (_unamangedType == EditorVar::FLOAT4)
					type = MVec4::typeid;

				PropertySpec^ spec = gcnew PropertySpec(gcnew System::String(valueRef.GetName().c_str()), type, gcnew System::String(valueRef.GetCategory().c_str()), gcnew System::String(valueRef.GetDesc().c_str()));

				if (spec == nullptr) {
					throw gcnew System::Exception("[PropertyHelper::UpdateVarsTwo]- not exist spec for vars[i]");
					continue;
				}

				if (_unamangedType == EditorVar::FILENAME || _unamangedType == EditorVar::TEXTURE || _unamangedType == EditorVar::MATERIAL || _unamangedType == EditorVar::SHADER || _unamangedType == EditorVar::SCRIPT)
				{
					Type^ conv = System::Drawing::Design::UITypeEditor::typeid;

					Type^ ed = nullptr;

					if (_unamangedType == EditorVar::MATERIAL)
						ed = MaterialNameEditor::typeid;
					else if (_unamangedType == EditorVar::TEXTURE)
						ed = TextureNameEditor::typeid;
					else if (_unamangedType == EditorVar::SHADER)
						ed = ShaderNameEditor::typeid;
					else if (_unamangedType == EditorVar::SCRIPT)
						ed = ScriptNameEditor::typeid;
					else
						ed = MFileNameEditor::typeid;

					spec->EditorTypeName = ed->AssemblyQualifiedName;
					spec->TypeName = ed->AssemblyQualifiedName;
				}

				class1->Properties->Add(spec);
			}
		}

		// Add second set of script vars
		if (!vars2.empty())
		{
			for (auto& valueRef : vars2)
			{
				Type ^ type = nullptr;
				auto _unamangedType = valueRef.GetType();

				if (_unamangedType == EditorVar::BOOL)
					type = bool::typeid;
				else if (_unamangedType == EditorVar::STRING)
					type = System::String::typeid;
				else if (_unamangedType == EditorVar::FILENAME)
					type = MFileNameEditor::typeid;
				else if (_unamangedType == EditorVar::TEXTURE)
					type = TextureNameEditor::typeid;
				else if (_unamangedType == EditorVar::MATERIAL)
					type = MaterialNameEditor::typeid;
				else if (_unamangedType == EditorVar::INT)
					type = int::typeid;
				else if (_unamangedType == EditorVar::UNINT)
					type = UInt32::typeid;
				else if (_unamangedType == EditorVar::FLOAT)
					type = Single::typeid;
				else if (_unamangedType == EditorVar::DOUBLE)
					type = Double::typeid;
				else if (_unamangedType == EditorVar::COLOR)
					type = System::Drawing::Color::typeid;
				else if (_unamangedType == EditorVar::FLOAT3)
					type = MVec3::typeid;
				else if (_unamangedType == EditorVar::FLOAT4)
					type = MVec4::typeid;

				PropertySpec^ spec = gcnew PropertySpec(gcnew System::String(valueRef.GetName().c_str()), type, gcnew System::String(valueRef.GetCategory().c_str()), gcnew System::String(valueRef.GetDesc().c_str()));

				if (spec == nullptr) {
					throw gcnew System::Exception("[PropertyHelper::UpdateVarsTwo]- not exist spec for vars2[i]");
					continue;
				}

				if (_unamangedType == EditorVar::FILENAME || _unamangedType == EditorVar::TEXTURE || _unamangedType == EditorVar::MATERIAL || _unamangedType == EditorVar::SHADER || _unamangedType == EditorVar::SCRIPT)
				{
					Type^ conv = System::Drawing::Design::UITypeEditor::typeid;

					Type^ ed = nullptr;

					if (_unamangedType == EditorVar::MATERIAL)
						ed = MaterialNameEditor::typeid;
					else if (_unamangedType == EditorVar::TEXTURE)
						ed = TextureNameEditor::typeid;
					else if (_unamangedType == EditorVar::SHADER)
						ed = ShaderNameEditor::typeid;
					else if (_unamangedType == EditorVar::SCRIPT)
						ed = ScriptNameEditor::typeid;
					else
						ed = MFileNameEditor::typeid;

					spec->EditorTypeName = ed->AssemblyQualifiedName;
					spec->TypeName = ed->AssemblyQualifiedName;
				}
				class1->Properties->Add(spec);
			}
		}
	}

	// <summary>
	// Sets UI values from source params
	// TODO:FIXME: This gets called a lot of times, maybe should use index compare instead of string compare
	// </summary>
	void PropertyHelper::GetValue(Object^ sender, PropertySpecEventArgs^ e) {
		if (e == nullptr) {
			throw gcnew System::Exception("[PropertyHelper::GetValue] - invalid pointer on e");
			return;
		}

		if ((!currentVars) || (currentVars && (currentVars->empty())))
			return;

		for (size_t j = 0; j < currentVars->size(); j++)
		{
			std::vector<EditorVar>& vars = *(*currentVars)[j];
			for (size_t i = 0; i < vars.size(); i++)
			{
				auto _unamangedType = vars[i].GetType();

				//!@todo FIX THIS CRASH - Delete one mesh and click on props

				if (gcnew System::String(vars[i].GetName().c_str()) == e->Property->Name)
				{
					if (_unamangedType == EditorVar::BOOL) {
						e->Value = *(bool*)vars[i].GetData();
					}
					else if (_unamangedType == EditorVar::INT) {
						e->Value = *(int*)vars[i].GetData();
					}
					else if (_unamangedType == EditorVar::UNINT) {
						e->Value = *(UInt32*)vars[i].GetData();
					}
					else if (_unamangedType == EditorVar::FLOAT) {
						e->Value = *(float*)vars[i].GetData();
					}
					else if (_unamangedType == EditorVar::DOUBLE) {
						e->Value = *(double*)vars[i].GetData();
					}
					else if (_unamangedType == EditorVar::FLOAT3) {
						Vec3* v = (Vec3*)vars[i].GetData();
						if (v == nullptr) {
							throw gcnew System::Exception("[PropertyHelper::GetValue] - failed cast");
							continue;
						}
						e->Value = gcnew MVec3(v->x, v->y, v->z);
					}
					else if (_unamangedType == EditorVar::FLOAT4) {
						Vec4* v = (Vec4*)vars[i].GetData();
						if (v == nullptr) {
							throw gcnew System::Exception("[PropertyHelper::GetValue] - failed cast");
							continue;
						}
						e->Value = gcnew MVec4(v->x, v->y, v->z, v->w);
					}
					else if (_unamangedType == EditorVar::COLOR) {
						NGTech::Color* sCol = (NGTech::Color*)vars[i].GetData();
						if (sCol == nullptr) {
							throw gcnew System::Exception("[PropertyHelper::GetValue] - failed cast");
							continue;
						}
						sCol->Clamp();

						e->Value = System::Drawing::Color::FromArgb(sCol->w * 255, sCol->x * 255, sCol->y * 255, sCol->z * 255);
					}
					else if (_unamangedType == EditorVar::STRING) {
						e->Value = gcnew System::String((*(NGTech::String*)vars[i].GetData()).c_str());
					}
					else if (_unamangedType == EditorVar::FILENAME || _unamangedType == EditorVar::TEXTURE || _unamangedType == EditorVar::MATERIAL || _unamangedType == EditorVar::SHADER || _unamangedType == EditorVar::SCRIPT) {
						// Just add texture name without path
						System::String^ name = gcnew System::String((*(NGTech::String*)vars[i].GetData()).c_str());
						name = Path::GetFileName(name);
						e->Value = name;
					}
				}
			}
		}
	}

	// <summary>
	// Updates source values from UI values
	// </summary>
	void PropertyHelper::SetValue(Object^ sender, PropertySpecEventArgs^ e) {
		if (e == nullptr) {
			throw gcnew System::Exception("[PropertyHelper::GetValue] - invalid pointer on e");
			return;
		}

		// We update ALL registered params, this allows for multi-selection application
		for (size_t j = 0; j < currentVars->size(); j++)
		{
			std::vector<EditorVar>& vars = *(*currentVars)[j];
			for (size_t i = 0; i < vars.size(); i++)
			{
				if (gcnew System::String(vars[i].GetName().c_str()) == e->Property->Name)
				{
					auto _unamangedType = vars[i].GetType();

					if (_unamangedType == EditorVar::BOOL) {
						vars[i].SetData((bool*)*reinterpret_cast<bool^>(e->Value));
					}
					else if (_unamangedType == EditorVar::STRING) {
						vars[i].SetData(Helpers::ToCppString(reinterpret_cast<System::String^>(e->Value)));
					}
					else if (_unamangedType == EditorVar::FILENAME) {
						vars[i].SetData(Helpers::ToCppString(reinterpret_cast<System::String^>(e->Value)));
#if NICK//TODO:CLean
						// Semi-intelligent filename parsing
						if (AssetBrowser::me->selectedMat && AsLower(*(string*)vars[i].data).find(".fx") != -1)
						{
							AssetBrowser::me->selectedMat->mat->Initialize(AssetBrowser::me->selectedMat->mat->m_ShaderName.c_str(), "", true);
							AssetBrowser::me->MaterialChanged(nullptr, nullptr);
						}
						if (value->LastIndexOf("\\") > 0)
							ShaderNameEditor::InitialDirectory = value->Substring(0, value->LastIndexOf("\\"));
#endif
					}
					else if (_unamangedType == EditorVar::FLOAT) {
						vars[i].SetData(*reinterpret_cast<float^>(e->Value));
					}
					else if (_unamangedType == EditorVar::DOUBLE) {
						vars[i].SetData(*reinterpret_cast<double^>(e->Value));
					}
					else if (_unamangedType == EditorVar::INT) {
						vars[i].SetData((int*)*reinterpret_cast<int^>(e->Value));
					}
					else if (_unamangedType == EditorVar::UNINT) {
						vars[i].SetData((UInt32*)*reinterpret_cast<UInt32^>(e->Value));
					}
					else if (_unamangedType == EditorVar::FLOAT3) {
						MVec3^ f = reinterpret_cast<MVec3^>(e->Value);
						vars[i].SetData(Vec3(f->X, f->Y, f->Z));
					}
					else if (_unamangedType == EditorVar::COLOR) {
						System::Drawing::Color^ col = reinterpret_cast<System::Drawing::Color^>(e->Value);
						vars[i].SetData(NGTech::Color(float(col->R) / 255.f, float(col->G) / 255.f, float(col->B) / 255.f, float(col->A) / 255.f));
					}
					else if (_unamangedType == EditorVar::FLOAT4) {
						MVec4^ f = reinterpret_cast<MVec4^>(e->Value);
						vars[i].SetData(Vec4(f->X, f->Y, f->Z, f->W));
					}
					else if (_unamangedType == EditorVar::TEXTURE) {
						System::String^ value = reinterpret_cast<System::String^>(e->Value);
						//TODO:OPTIMIZATION: ��� ����� ������ ���������� ��� ����� ��������,
						// � ������, ���� ����������, �� ������ ������ ��� ����
						// Gots file name from path
						value = Path::GetFileName(value);
						vars[i].SetData(Helpers::ToCppString(value));
					}
					else if (_unamangedType == EditorVar::MATERIAL) {
						System::String^ value = reinterpret_cast<System::String^>(e->Value);
						//TODO:OPTIMIZATION: ��� ����� ������ ���������� ��� ����� ��������,
						// � ������, ���� ����������, �� ������ ������ ��� ����
						// Gots file name from path
						value = Path::GetFileName(value);
						vars[i].SetData(Helpers::ToCppString(value));
					}
					else if (_unamangedType == EditorVar::SHADER) {
						System::String^ value = reinterpret_cast<System::String^>(e->Value);
						//TODO:OPTIMIZATION: ��� ����� ������ ���������� ��� ����� ��������,
						// � ������, ���� ����������, �� ������ ������ ��� ����
						// Gots file name from path
						value = Path::GetFileName(value);
						vars[i].SetData(Helpers::ToCppString(value));
					}
					else if (_unamangedType == EditorVar::SCRIPT) {
						System::String^ value = reinterpret_cast<System::String^>(e->Value);
						//TODO:OPTIMIZATION: ��� ����� ������ ���������� ��� ����� ��������,
						// � ������, ���� ����������, �� ������ ������ ��� ����
						// Gots file name from path
						value = Path::GetFileName(value);
						vars[i].SetData(Helpers::ToCppString(value));
					}
				}
			}
		}

		/// Refresh all params
		API_EditorCheckWhatWasModificated();
		/// Event, what scene was modificated
		_PropertyWasChanged();
	}

	void PropertyHelper::_PropertyWasChanged()
	{}

	void PropertyHelper::_PropertyWasSavedOrRevert()
	{}
}

#endif