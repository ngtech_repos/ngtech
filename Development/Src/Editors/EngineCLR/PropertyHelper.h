/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#ifndef DROP_EDITOR
namespace EngineCLR
{
	using namespace NGTech;
	/// <summary>
	/// Handles propertybags and editorvars
	/// </summary>
	public ref class PropertyHelper
	{
	public:
		std::vector<std::vector<EditorVar>*>* currentVars;
		PropertyBags::PropertyBag^  class1;

		PropertyHelper()
		{
			currentVars = new std::vector<std::vector<EditorVar>*>;

			class1 = gcnew PropertyBags::PropertyBag();
			// Register events
			class1->GetValue += gcnew PropertyBags::PropertySpecEventHandler(this, &PropertyHelper::GetValue);
			class1->SetValue += gcnew PropertyBags::PropertySpecEventHandler(this, &PropertyHelper::SetValue);
		}

		void UpdateVars(std::vector<EditorVar>& vars);
		void UpdateVarsTwo(std::vector<EditorVar>& vars, std::vector<EditorVar>& vars2);
		void GetValue(Object^ sender, PropertyBags::PropertySpecEventArgs^ e);
		void SetValue(Object^ sender, PropertyBags::PropertySpecEventArgs^ e);

		void CleanVars() { if (!currentVars) return; currentVars->clear(); }
	private:
		void _PropertyWasChanged();
		void _PropertyWasSavedOrRevert();
	};
}

#endif