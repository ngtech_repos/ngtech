/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "stdafx.h"

#ifndef DROP_EDITOR
#include "ReflectionSystem_Engine_Helper.h"
using namespace EngineCLR;

#include "..\..\..\API\NGTechEngineAPI.h"
using namespace NGTech;

#include "..\..\Engine\inc\EditorObjectManager.h"
#include "..\..\Core\inc\ReflectionSystem.h"

static ENGINE_INLINE ReflectionSystem<EditableObject>::Creators GetReflectionSystemCreators()
{
	auto objM = GetEditorObjectManager();
	if (!objM)
	{
		throw gcnew System::Exception("[Engine_ReflectionSystem_Helper::GetReflectionSystemCreators] GetEditorObjectManager is reverted null");
	}

	auto objRC = objM->GeReflectionCreators();
	return objRC;
}

size_t Engine_ReflectionSystem_Helper::GetCountOfCreators() {
	return GetReflectionSystemCreators().size();
}

System::String^ Engine_ReflectionSystem_Helper::GetReflectableClassName(size_t uid) {
	auto refs = GetReflectionSystemCreators();
	if (uid > refs.size()) {
		throw gcnew System::Exception("Engine_ReflectionSystem_Helper::GetReflectableClassName incorrect requested id: " + uid.ToString() + ", maximal uid:" + refs.size());
		return Helpers::ToCLIString("INVALID");
	}

	// ������� ��� �������
	size_t counter = 0;
	for (auto &it : refs)
	{
		if (counter == uid)
		{
			return Helpers::ToCLIString(it.first);
		}
		counter++;
	}

	throw gcnew System::Exception("[Engine_ReflectionSystem_Helper::GetReflectionSystemCreators] GetEditorObjectManager is reverted null");

	return Helpers::ToCLIString("INVALID");
}

// ��������� ��� � �� ����� ����� C# ������
MEditableObject^ Engine_ReflectionSystem_Helper::GetReflectableByName(System::String^ _name)
{
	auto objM = GetEditorObjectManager();
	if (!objM)
	{
		throw gcnew System::Exception("[Engine_ReflectionSystem_Helper::GetReflectionSystemCreators] GetEditorObjectManager is reverted null");
	}

	auto objRC = objM->m_Reflection_EditableObjs;
	auto ptr = objRC->Create(Helpers::ToCppString(_name));

	auto obj = gcnew MEditableObject(ptr); // call this function

	return obj;
}

#if REFLECTION_USE_DEBUG_GLOBAL_CONTAINER
MEditableObject^ Engine_ReflectionSystem_Helper::GetReflectableByName_DEBUG(System::String^ _name)
{
	auto objM = GetEditorObjectManager();
	if (!objM)
	{
		throw gcnew System::Exception("[Engine_ReflectionSystem_Helper::GetReflectionSystemCreators] GetEditorObjectManager is reverted null");
	}

	auto objRC = objM->m_Reflection_EditableObjs;

	LightPoint* ptr = (LightPoint*)objRC->CreateFromGlobals(Helpers::ToCppString(_name));
	auto obj = gcnew MEditableObject(ptr); // call this function

	return obj;
}
#endif

#endif