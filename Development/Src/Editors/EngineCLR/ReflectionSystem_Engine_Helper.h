/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#ifndef DROP_EDITOR
#include "ManagedEditableObject.h"

namespace EngineCLR {
	static public ref class Engine_ReflectionSystem_Helper
	{
	public:
		static size_t GetCountOfCreators();

		static System::String^ GetReflectableClassName(size_t uid);
		static MEditableObject^ GetReflectableByName(System::String^ _name);
#if REFLECTION_USE_DEBUG_GLOBAL_CONTAINER
		static MEditableObject^ GetReflectableByName_DEBUG(System::String^ _name);
#endif
	};
}

#endif