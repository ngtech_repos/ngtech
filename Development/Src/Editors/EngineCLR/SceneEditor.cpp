/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "stdafx.h"

#ifndef DROP_EDITOR

using namespace PropertyBags;

#include "SelectorActor.h"

namespace EngineCLR
{
	/**/
	ENGINE_INLINE SceneEditor::SceneEditor() {
		propHelper = gcnew PropertyHelper();

		selector = new SelectorActor();
	}

	ENGINE_INLINE void SceneEditor::DestroyPropHelper()
	{
		if (propHelper) {
			delete propHelper;
			propHelper = nullptr;
		}

		SAFE_DELETE(selector);
	}

	void SceneEditor::SetDirectoryPath(System::String^ _path) {
		Directory = _path;
	}

	ENGINE_INLINE void SceneEditor::CleanVars()
	{
		CLIASSERT(propHelper != nullptr, "Invalid pointer on propHelper");
		if (propHelper == nullptr) return;

		propHelper->CleanVars();
	}

	ENGINE_INLINE PropertyBags::PropertyBag^ SceneEditor::UpdateSceneVars() {
		CLIASSERT(propHelper != nullptr, "Invalid pointer on propHelper");
		propHelper->UpdateVars(GetSceneEditorVars());
		return propHelper->class1;
	}

	ENGINE_INLINE PropertyBags::PropertyBag^ SceneEditor::UpdateActorMeshVars(int uid) {
		CLIASSERT(propHelper != nullptr, "Invalid pointer on propHelper");
		propHelper->UpdateVars(GetActorMeshEditorVars(uid));
		return propHelper->class1;
	}

	ENGINE_INLINE PropertyBags::PropertyBag^ SceneEditor::UpdateActorLightVars(int uid) {
		CLIASSERT(propHelper != nullptr, "Invalid pointer on propHelper");
		propHelper->UpdateVars(GetActorLightEditorVars(uid));
		return propHelper->class1;
	}

	// Selector
	ENGINE_INLINE unsigned long SceneEditor::SelectorActorsSize() {
		CLIASSERT(selector != nullptr, "Invalid pointer on selector");

		if (!selector) return 0;

		return selector->SelectedActorsSize();
	}

	ENGINE_INLINE System::String^ SceneEditor::SelectorActorsGetGUIDString(unsigned long uid) {
		CLIASSERT(selector != nullptr, "Invalid pointer on selector");
		if (!selector)
			return gcnew System::String("");

		return Helpers::ToCLIString(selector->GetGUIDString(uid));
	}

	ENGINE_INLINE void SceneEditor::SelectorUnSelectAll() {
		CLIASSERT(selector != nullptr, "Invalid pointer on selector");

		if (!selector) return;
		return selector->UnSelectAll();
	}

	ENGINE_INLINE void SceneEditor::SelectorDeleteSelectedEntity() {
		CLIASSERT(selector != nullptr, "Invalid pointer on selector");
		if (!selector) return; selector->DeleteSelected();
	}

	ENGINE_INLINE bool SceneEditor::SelectorCanFindObject(System::String^ _str) {
		CLIASSERT(selector != nullptr, "Invalid pointer on selector");
		if (!selector) return false;
		return selector->CanFindObject(_str);
	}

	ENGINE_INLINE System::Object^ SceneEditor::SelectorSelectObject(System::String^ _str) {
		CLIASSERT(selector != nullptr, "[SelectorSelectObject] ERROR");
		return selector->SelectObject(_str);
	}
}

#endif