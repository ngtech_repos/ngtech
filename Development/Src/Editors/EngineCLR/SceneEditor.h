/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include "..\..\..\API\NGTechEngineAPI.h"
using namespace NGTech;

#ifndef DROP_EDITOR
#include "..\..\Engine\inc\EditorObjectManager.h"

#include "PropertyHelper.h"

#include "MActor.h"
#include "MObjectMesh.h"
#include "MObjectLight.h"
#include "ManagedMaterial.h"

namespace EngineCLR {
	/**
	*/
	class SelectorActor;
	/**
	*/
	public ref class SceneEditor
	{
	public:
		static property SceneEditor^ Instance { SceneEditor^ get() { return %m_instance; } }

		ENGINE_INLINE void DestroyPropHelper();


		void SetSceneName(System::String^ _path) {
			Path = _path;
			auto scene = GetSceneWrapper();
			if (!scene) return;

			scene->SetCurrentSceneName(Helpers::ToCppString(_path));
		}

		void SetDirectoryPath(System::String^ _path);

		/**/
		ENGINE_INLINE PropertyBags::PropertyBag^ UpdateSceneVars();
		ENGINE_INLINE PropertyBags::PropertyBag^ UpdateActorMeshVars(int uid);
		ENGINE_INLINE PropertyBags::PropertyBag^ UpdateActorLightVars(int uid);

		ENGINE_INLINE void CleanVars();

		ENGINE_INLINE size_t GetMeshesCount() {
			if (!IsValid()) return 0;

			return GetSceneWrapper()->MeshesCount();
		}

		ENGINE_INLINE size_t GetLightsCount() {
			if (!IsValid()) return 0;
			return GetSceneWrapper()->LightsCount();
		}

		ENGINE_INLINE MObjectMesh^ GetActorMesh(unsigned int uid) {
			if (!IsValid()) return nullptr;

			return gcnew MObjectMesh(GetSceneWrapper()->GetMeshById(uid));
		}

		ENGINE_INLINE MObjectLight^ GetActorLight(unsigned int uid) {
			if (!IsValid()) return nullptr;

			return gcnew MObjectLight(GetSceneWrapper()->GetLightById(uid));
		}

		/*Base Objects*/
		ENGINE_INLINE MEditableObject^ GetBaseObject(unsigned int _uid) {
			if (!IsValid()) return nullptr;

			return gcnew MEditableObject(GetSceneWrapper()->GetSceneForChange().GetSceneData().GetBaseEntities()[_uid]);
		}

		ENGINE_INLINE size_t GetBaseObjectCount() {
			if (!IsValid()) return 0;

			return GetSceneWrapper()->GetSceneForChange().GetSceneData().GetBaseEntities().size();
		}

		/**
		Selector
		*/
		ENGINE_INLINE unsigned long SelectorActorsSize();
		ENGINE_INLINE System::String^ SelectorActorsGetGUIDString(unsigned long uid);

		/**
		*/
		ENGINE_INLINE void SelectorUnSelectAll();
		ENGINE_INLINE void SelectorDeleteSelectedEntity();

		/**
		*/
		ENGINE_INLINE bool SelectorCanFindObject(System::String^ _str);

		/**
		*/
		ENGINE_INLINE System::Object^ SelectorSelectObject(System::String^ _str);

		/**
		*/
		ENGINE_INLINE System::String^ GetSceneName() {
			return Helpers::ToCLIString(GetSceneWrapper()->GetCurrentSceneName());
		}

		ENGINE_INLINE System::String^ GetDirectoryName() {
			return Directory;
		}

		static ENGINE_INLINE SceneManager* GetSceneWrapper()
		{
			auto ptr = GetScene();
			CLIASSERT(ptr != nullptr, "Invalid pointer on Scene");
			if (!ptr) return nullptr;

			return ptr;
		}

		bool IsValid()
		{
			if (!GetCore())
				return false;

			if (!GetCore()->scene)
				return false;

			return true;
		}

	private:
		ENGINE_INLINE std::vector<EditorVar>& GetSceneEditorVars() { return GetSceneWrapper()->GetEditorVars(); }
		ENGINE_INLINE std::vector<EditorVar>& GetActorMeshEditorVars(int uid) { return GetSceneWrapper()->GetMeshById(uid)->GetEditorVars(); }
		ENGINE_INLINE std::vector<EditorVar>& GetActorLightEditorVars(int uid) { return GetSceneWrapper()->GetLightById(uid)->GetEditorVars(); }

		ENGINE_INLINE SceneEditor();
		ENGINE_INLINE SceneEditor(SceneEditor%) { CLIASSERT(NULL, "singleton cannot be copy-constructed"); }
		static SceneEditor m_instance;

		PropertyHelper^ propHelper;
		SelectorActor* selector;

		System::String ^Path, ^Directory;
	};
}
#endif