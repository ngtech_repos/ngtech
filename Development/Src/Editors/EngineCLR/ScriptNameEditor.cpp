/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "stdafx.h"

#ifndef DROP_EDITOR
#include "ScriptNameEditor.h"

namespace EngineCLR
{
	/*
	*/
	void ScriptNameEditor::InitializeDialog(System::Windows::Forms::OpenFileDialog^ openFileDialog)
	{
		MessageBox::Show("1");
		MFileNameEditorBase::InitializeDialog(openFileDialog);

		openFileDialog->Multiselect = false;
		openFileDialog->FileName = "";
		openFileDialog->InitialDirectory = InitialDirectory;
		openFileDialog->Filter = "Script File (*.lua) |*.lua";
		openFileDialog->Title = "Select Script";
		openFileDialog->RestoreDirectory = false;
	}
}
#endif