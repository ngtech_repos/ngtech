/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#ifndef DROP_EDITOR

namespace EngineCLR {
	class SelectorActor
	{
	public:
		SelectorActor() { m_SelectedActors.clear(); }

		ENGINE_INLINE size_t SelectedActorsSize() { return m_SelectedActors.size(); }
		ENGINE_INLINE const char* GetGUIDString(size_t uid) { return m_SelectedActors[uid]->GetGUIDString(); }

		void UnSelectAll();
		void DeleteSelected();

		// Without error check!
		bool CanFindObject(System::String^_str);

		System::Object ^ SelectObject(System::String^ _str);

	private:
		std::vector<NGTech::EditableObject*> m_SelectedActors;
	};
}

#endif