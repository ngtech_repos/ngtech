/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "Stdafx.h"

#ifndef DROP_EDITOR
#include "Slider.h"

namespace EngineCLR
{
	void Slider::InitializeComponent(void)
	{
		this->label1 = (gcnew System::Windows::Forms::Label());
		this->trackBar1 = (gcnew System::Windows::Forms::TrackBar());
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar1))->BeginInit();
		this->SuspendLayout();
		//
		// label1
		//
		this->label1->Dock = System::Windows::Forms::DockStyle::Top;
		this->label1->Location = System::Drawing::Point(0, 0);
		this->label1->Name = L"label1";
		this->label1->RightToLeft = System::Windows::Forms::RightToLeft::Yes;
		this->label1->Size = System::Drawing::Size(285, 13);
		this->label1->TabIndex = 0;
		this->label1->Text = L"0";
		this->label1->TextAlign = System::Drawing::ContentAlignment::TopCenter;
		this->label1->Click += gcnew System::EventHandler(this, &Slider::label1_Click);
		//
		// trackBar1
		//
		this->trackBar1->Dock = System::Windows::Forms::DockStyle::Fill;
		this->trackBar1->Location = System::Drawing::Point(0, 13);
		this->trackBar1->Name = L"trackBar1";
		this->trackBar1->Size = System::Drawing::Size(285, 36);
		this->trackBar1->TabIndex = 1;
		this->trackBar1->ValueChanged += gcnew System::EventHandler(this, &Slider::trackBar1_ValueChanged);
		//
		// Slider
		//
		this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
		this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
		this->Controls->Add(this->trackBar1);
		this->Controls->Add(this->label1);
		this->Name = L"Slider";
		this->Size = System::Drawing::Size(285, 49);
		this->Load += gcnew System::EventHandler(this, &Slider::Slider_Load);
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar1))->EndInit();
		this->ResumeLayout(false);
		this->PerformLayout();
	}

	Slider::~Slider()
	{
		if (components)
		{
			delete components;
		}
	}

	System::Void Slider::Slider_Load(System::Object^  sender, System::EventArgs^  e) {}
	System::Void Slider::label1_Click(System::Object^  sender, System::EventArgs^  e) {}
	System::Void Slider::trackBar1_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
		label1->Text = Convert::ToString(trackBar1->Value / Convert::ToSingle(label1->Tag));
	}
}
#endif