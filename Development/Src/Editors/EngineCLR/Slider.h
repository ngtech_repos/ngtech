/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#ifndef DROP_EDITOR

using namespace System;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

namespace EngineCLR {
	/// <summary>
	/// Summary for Slider
	/// </summary>
	public ref class Slider : public System::Windows::Forms::UserControl
	{
	public:
		Slider(void)
		{
			InitializeComponent();
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Slider();
	public:
		System::Windows::Forms::Label^  label1;
		System::Windows::Forms::TrackBar^  trackBar1;
	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void);
#pragma endregion
	private:
		System::Void Slider_Load(System::Object^  sender, System::EventArgs^  e);
		System::Void label1_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void trackBar1_ValueChanged(System::Object^  sender, System::EventArgs^  e);
	};
}

#endif