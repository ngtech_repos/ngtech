/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "stdafx.h"

#ifndef DROP_EDITOR

namespace EngineCLR
{
	void SliderEditor::TrackBar_ValueChanged(Object^ sender, System::EventArgs^ e)
	{
		TrackBar ^ sliderControl = (TrackBar^)sender;
		System::ComponentModel::ITypeDescriptorContext^ context = (System::ComponentModel::ITypeDescriptorContext^)sliderControl->Tag;

		int factor = 1;
		for (int i = 0; i < context->PropertyDescriptor->Attributes->Count; i++)
		{
			if (context->PropertyDescriptor->Attributes[i]->GetType() == SliderRange::typeid)
			{
				SliderRange^ range = (SliderRange^)context->PropertyDescriptor->Attributes[i];
				factor = range->Factor;
				break;
			}
		}
		// Return the value in the appropriate data format.
		if (context->PropertyDescriptor->PropertyType == double::typeid)
			context->PropertyDescriptor->SetValue(context->Instance, (double)(sliderControl->Value / (double)factor));
		else
			if (context->PropertyDescriptor->PropertyType == float::typeid)
				context->PropertyDescriptor->SetValue(context->Instance, (float)(sliderControl->Value / (float)factor));
			else
				if (context->PropertyDescriptor->PropertyType == int::typeid)
					context->PropertyDescriptor->SetValue(context->Instance, Convert::ToInt32(sliderControl->Value / factor));

		if (!Rendering)
		{
			Rendering = true;
			//!@todo clean
#if 0
			MEditor::RenderPanel->Refresh();
			Editor::Instance()->Update(Editor::Instance()->m_World);
			GetEngine()->physics->FlushSceneFx();
			GetEngine()->physics->FlushScene();
			Editor::Instance()->m_World->Tick();
			GetEngine()->physics->UpdateScene();
			GetEngine()->physics->UpdateSceneFx();
			GetEngine()->render->DoRendering();
#endif
			Application::DoEvents();
			Rendering = false;
		}
	}
}

#endif