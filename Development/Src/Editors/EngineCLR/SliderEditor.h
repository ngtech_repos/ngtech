/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma	once

#ifndef DROP_EDITOR
using namespace System;
using namespace System::Reflection;
using namespace System::Drawing;
using namespace System::Drawing::Design;
using namespace System::Windows::Forms;
using namespace System::Windows::Forms::Design;

#include "Slider.h"

namespace EngineCLR
{
	public ref class SliderRange : public System::Attribute
	{
		int maxValue;
		int minValue;
		int factor;
	public:
		SliderRange()
		{
			factor = 1;
		}

		property int MaxValue
		{
			int get()
			{
				return maxValue;
			}

			void set(int value)
			{
				maxValue = value;
			}
		}

		property int MinValue
		{
			int get()
			{
				return minValue;
			}

			void set(int value)
			{
				minValue = value;
			}
		}

		property int Factor
		{
			int get()
			{
				return factor;
			}

			void set(int value)
			{
				factor = value;
			}
		}
	};

	[System::Security::Permissions::PermissionSetAttribute
	(System::Security::Permissions::SecurityAction::InheritanceDemand, Name = "FullTrust")]
	[System::Security::Permissions::PermissionSetAttribute
	(System::Security::Permissions::SecurityAction::LinkDemand, Name = "FullTrust")]
	public ref class SliderEditor : public System::Drawing::Design::UITypeEditor
	{
	private:

	public:
		SliderEditor()
		{
		}

		static System::Windows::Forms::Timer^ timer = gcnew System::Windows::Forms::Timer();
		static bool Rendering;

		virtual System::Drawing::Design::UITypeEditorEditStyle GetEditStyle(System::ComponentModel::ITypeDescriptorContext^ context) override
		{
			return UITypeEditorEditStyle::DropDown;
		}

		// Displays the UI for value selection.
		virtual Object^ EditValue(System::ComponentModel::ITypeDescriptorContext^ context, System::IServiceProvider^ provider, Object^ value) override
		{
			// Return the value if the value is not of type Int32, Double and Single.
			if (value->GetType() != double::typeid && value->GetType() != float::typeid && value->GetType() != int::typeid)
				return value;

			int maxValue = 100;
			int minValue = 0;
			int factor = 1;
			for (int i = 0; i < context->PropertyDescriptor->Attributes->Count; i++)
			{
				if (context->PropertyDescriptor->Attributes[i]->GetType() == SliderRange::typeid)
				{
					SliderRange^ range = (SliderRange^)context->PropertyDescriptor->Attributes[i];
					minValue = range->MinValue;
					maxValue = range->MaxValue;
					factor = range->Factor;
					break;
				}
			}

			if ((Convert::ToInt32(value)) > maxValue || (Convert::ToInt32(value)) < minValue)
				return value;

			IWindowsFormsEditorService^ edSvc = dynamic_cast<IWindowsFormsEditorService^>(provider->GetService(IWindowsFormsEditorService::typeid));
			if (edSvc != nullptr)
			{
				// Display an angle selection control and retrieve the value.
				Slider ^ sliderControl = gcnew Slider();
				sliderControl->trackBar1->Maximum = maxValue;
				sliderControl->trackBar1->Minimum = minValue;
				sliderControl->trackBar1->Tag = context;
				sliderControl->label1->Tag = factor;
				sliderControl->trackBar1->ValueChanged += gcnew System::EventHandler(this, &SliderEditor::TrackBar_ValueChanged);

				timer->Stop();
				Rendering = false;
				timer->Interval = 200;
				timer->Tick += gcnew System::EventHandler(this, &SliderEditor::Timer_Tick);

				if (value->GetType() == float::typeid)
					sliderControl->trackBar1->Value = Convert::ToInt32((float)value*factor);
				else if (value->GetType() == double::typeid)
					sliderControl->trackBar1->Value = Convert::ToInt32((double)value*factor);
				else if (value->GetType() == int::typeid)
					sliderControl->trackBar1->Value = Convert::ToInt32((int)value*factor);

				timer->Start();

				edSvc->DropDownControl(sliderControl);

				timer->Stop();
				Rendering = true;

				// Return the value in the appropraite data format.
				if (value->GetType() == double::typeid)
					return (double)(sliderControl->trackBar1->Value / (double)factor);
				else
					if (value->GetType() == float::typeid)
						return (float)(sliderControl->trackBar1->Value / (float)factor);
					else
						if (value->GetType() == int::typeid)
							return Convert::ToInt32(sliderControl->trackBar1->Value / factor);
			}

			return value;
		}
		void Timer_Tick(Object^ sender, System::EventArgs^ e)
		{
			if (!Rendering)
			{
				Rendering = true;
				System::Diagnostics::Debug::WriteLine("Rendering");
#if 0
				MEditor::RenderPanel->Refresh();
				Editor::Instance()->Update(Editor::Instance()->m_World);
				GetEngine()->physics->FlushSceneFx();
				GetEngine()->physics->FlushScene();
				Editor::Instance()->m_World->Tick();
				GetEngine()->physics->UpdateScene();
				GetEngine()->physics->UpdateSceneFx();
				GetEngine()->render->DoRendering();
#endif
				Application::DoEvents();
				Rendering = false;
			}
		}

		void TrackBar_ValueChanged(Object^ sender, System::EventArgs^ e);

		bool GetPaintValueSupported(System::ComponentModel::ITypeDescriptorContext^ context) new
		{
			return false;
		}
	};

	public ref class SaveMethod : public System::Attribute
	{
		System::String^ methodName;
	public:
		SaveMethod()
		{
			methodName = "";
		}

		property System::String^ MethodName
		{
			System::String^ get()
			{
				return methodName;
			}

			void set(System::String^ value)
			{
				methodName = value;
			}
		}
	};

	[System::Security::Permissions::PermissionSetAttribute
	(System::Security::Permissions::SecurityAction::InheritanceDemand, Name = "FullTrust")]
	[System::Security::Permissions::PermissionSetAttribute
	(System::Security::Permissions::SecurityAction::LinkDemand, Name = "FullTrust")]
	public ref class FileSaveEditor : public System::Drawing::Design::UITypeEditor
	{
	private:
		System::String^ filename;
	public:
		FileSaveEditor()
		{
			filename = "";
		}

		virtual System::Drawing::Design::UITypeEditorEditStyle GetEditStyle(System::ComponentModel::ITypeDescriptorContext^ context) override
		{
			return UITypeEditorEditStyle::DropDown;
		}

		// Displays the UI for value selection.
		virtual Object^ EditValue(System::ComponentModel::ITypeDescriptorContext^ context, System::IServiceProvider^ provider, Object^ value) override
		{
			IWindowsFormsEditorService^ edSvc = dynamic_cast<IWindowsFormsEditorService^>(provider->GetService(IWindowsFormsEditorService::typeid));
			if (edSvc != nullptr)
			{
				Button^ button = gcnew Button();
				button->Tag = context;
				button->Text = "Save";
				button->Click += gcnew System::EventHandler(this, &FileSaveEditor::Button_Click);
				edSvc->DropDownControl(button);
				return filename;
			}

			return value;
		}

		void Button_Click(Object^ sender, System::EventArgs^ e)
		{
			Button ^ button = (Button^)sender;
			System::ComponentModel::ITypeDescriptorContext^ context = (System::ComponentModel::ITypeDescriptorContext^)button->Tag;

			SaveFileDialog ^ fileDialog = gcnew SaveFileDialog();
			fileDialog->FileName = (System::String^)context->PropertyDescriptor->GetValue(context->Instance);
			fileDialog->Filter = "Fluid Effect File|*.fef";
			filename = fileDialog->FileName;
			if (fileDialog->ShowDialog() == System::Windows::Forms::DialogResult::OK)
			{
				filename = fileDialog->FileName;
				context->PropertyDescriptor->SetValue(context->Instance, filename);
				for (int i = 0; i < context->PropertyDescriptor->Attributes->Count; i++)
				{
					if (context->PropertyDescriptor->Attributes[i]->GetType() == SaveMethod::typeid)
					{
						SaveMethod^ saveMethod = (SaveMethod^)context->PropertyDescriptor->Attributes[i];
						context->Instance->GetType()->InvokeMember(saveMethod->MethodName, BindingFlags::DeclaredOnly |
							BindingFlags::Public | BindingFlags::NonPublic |
							BindingFlags::Instance | BindingFlags::InvokeMethod, nullptr, context->Instance, nullptr);
						break;
					}
				}
			}
		}

		bool GetPaintValueSupported(System::ComponentModel::ITypeDescriptorContext^ context) new
		{
			return false;
		}
	};
}

#endif