/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#ifndef DROP_EDITOR
#include "../../Common/Statistic.h"
using namespace NGTech;

namespace EngineCLR {
	public ref class Statistic
	{
	public:
		// Physics
		ENGINE_INLINE unsigned long GetPhysicsDelta() {
			return GetStatistic()->GetPhysicsDelta();
		}

		// Audio
		ENGINE_INLINE unsigned long GetAudioDelta() {
			return GetStatistic()->GetAudioDelta();
		}

		// GUI
		ENGINE_INLINE unsigned long GetGUIDelta() {
			return GetStatistic()->GetGUIDelta();
		}

		// Render
		ENGINE_INLINE unsigned long GetRenderDelta() {
			return GetStatistic()->GetRenderDelta();
		}

		ENGINE_INLINE unsigned int GetDrawCalls() {
			return GetStatistic()->GetDrawCalls();
		}

		ENGINE_INLINE unsigned int GetMaxDrawCalls() {
			return GetStatistic()->GetMaxDrawCalls();
		}

		ENGINE_INLINE unsigned int GetRenderStateChanges() {
			return GetStatistic()->GetRenderStateChanges();
		}

		ENGINE_INLINE unsigned int GetMaxRenderStateChanges() {
			return GetStatistic()->GetMaxRenderStateChanges();
		}

		ENGINE_INLINE unsigned int GetLastFPS() {
			return GetStatistic()->GetLastFPS();
		}

		ENGINE_INLINE float GetGPUMemoryRead() {
			return GetStatistic()->GetGPUMemoryRead();
		}

		ENGINE_INLINE float GetMaxGPUMemoryRead() {
			return GetStatistic()->GetMaxGPUMemoryRead();
		}

		ENGINE_INLINE float GetGPUMemoryWrite() {
			return GetStatistic()->GetGPUMemoryWrite();
		}

		ENGINE_INLINE float GetMaxGPUMemoryWrite() {
			return GetStatistic()->GetMaxGPUMemoryWrite();
		}

		// Scene

		ENGINE_INLINE unsigned int GetLightsCount() {
			return GetStatistic()->GetLightsCount();
		}

		ENGINE_INLINE unsigned int GetEntityCount() {
			return GetStatistic()->GetEntityCount();
		}

		ENGINE_INLINE unsigned int GetCamerasCount() {
			return GetStatistic()->GetCamerasCount();
		}

		// Main
		ENGINE_INLINE void Reset() {
			GetStatistic()->PrepareToNewFrame();
		}
	};
}

#endif