/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
// stdafx.h: ���������� ���� ��� ����������� ��������� ���������� ������
// ��� ���������� ������ ��� ����������� �������, ������� ����� ������������,
// �� �� ����� ����������

#pragma once

#include "..\..\BuildDefines.h"
#include "..\..\..\API\NGTechEngineAPI.h"
using namespace NGTech;

#ifdef DROP_EDITOR
#error Editor is skipped
#endif

#include "MarshallString.h"

#include "MActor.h"
#include "EngineCLR.h"
#include "Helpers.h"
#include "MVector3.h"
#include "ManagedMaterial.h"
#include "MObjectMesh.h"
#include "MObjectLight.h"
#include "Statistic.h"