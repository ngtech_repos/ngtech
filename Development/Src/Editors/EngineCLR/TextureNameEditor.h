/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#ifndef DROP_EDITOR
using namespace PropertyBags;
using namespace System;
using namespace System::IO;
using namespace System::Drawing::Design;
using namespace System::Configuration;
using namespace System::ComponentModel;
using namespace System::Windows::Forms;
using namespace System::Windows::Forms::Design;
using namespace System::Collections;

#pragma comment(lib,"user32.lib")

#include "MFileNameEditorBase.h"

namespace EngineCLR
{
	/**/
	public ref class TextureNameEditor sealed : public MFileNameEditorBase
	{
	public:
		ENGINE_INLINE TextureNameEditor() {}

		ENGINE_INLINE virtual ~TextureNameEditor() {
			if (myDialog != nullptr)
			{
				dialogs->Remove(myDialog);
				myDialog = nullptr;
			}
		}
		static ENGINE_INLINE void RefreshDialogs(System::String^ Directory)
		{
			InitialDirectory = Directory;
			for (int i = 0; i < dialogs->Count; i++)
			{
				((System::Windows::Forms::OpenFileDialog^)dialogs[i])->InitialDirectory = InitialDirectory;
			}
		}
	protected:
		System::Windows::Forms::OpenFileDialog^ myDialog;

		!TextureNameEditor()
		{
			if (myDialog != nullptr)
			{
				dialogs->Remove(myDialog);
				myDialog = nullptr;
			}
		}
	public:
		ENGINE_INLINE virtual void InitializeDialog(System::Windows::Forms::OpenFileDialog^ openFileDialog) override
		{
			myDialog = openFileDialog;
			dialogs->Add(myDialog);
			openFileDialog->FileName = "";
			openFileDialog->InitialDirectory = InitialDirectory;
			openFileDialog->Filter = L"Image File (*.dds,*.tga,*.png,*.bmp,*.jpg,*.gif,*.avi,*.hdr) |*.dds;*.tga;*.png;*.bmp;*.jpg;*.gif;*.avi;*.hdr|All Files (*.*) | *.*";
			openFileDialog->Title = L"Open Texture";
			openFileDialog->RestoreDirectory = false;
		}
	private:
		static System::String^ InitialDirectory = L"..//Textures";
		static ArrayList^ dialogs = gcnew ArrayList();
	};
}
#endif