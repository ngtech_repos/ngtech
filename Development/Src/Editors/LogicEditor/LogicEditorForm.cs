﻿/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/

using Graph;
using Graph.Compatibility;
using Graph.Items;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace GraphNodes
{
    public partial class LogicEditorForm : Form
    {
        public LogicEditorForm()
        {
            InitializeComponent();

            graphControl.CompatibilityStrategy = new TagTypeCompatibility();

            Node someNode = new Node("My Title")
            {
                Location = new Point(500, 100)
            };
            NodeCheckboxItem check1Item = new NodeCheckboxItem("Check 1", true, false) { Tag = 31337 };
            someNode.AddItem(check1Item);
            someNode.AddItem(new NodeCheckboxItem("Check 2", true, false) { Tag = 42f });

            graphControl.AddNode(someNode);

            Node colorNode = new Node("Color")
            {
                Location = new Point(200, 50)
            };
            NodeSliderItem redChannel = new NodeSliderItem("R", 64.0f, 16.0f, 0, 1.0f, 0.0f, false, false);
            NodeSliderItem greenChannel = new NodeSliderItem("G", 64.0f, 16.0f, 0, 1.0f, 0.0f, false, false);
            NodeSliderItem blueChannel = new NodeSliderItem("B", 64.0f, 16.0f, 0, 1.0f, 0.0f, false, false);
            NodeColorItem colorItem = new NodeColorItem("Color", Color.Black, false, true) { Tag = 1337 };

            EventHandler<NodeItemEventArgs> channelChangedDelegate = delegate (object sender, NodeItemEventArgs args)
            {
                float red = redChannel.Value;
                float green = blueChannel.Value;
                float blue = greenChannel.Value;
                colorItem.Color = Color.FromArgb((int)Math.Round(red * 255), (int)Math.Round(green * 255), (int)Math.Round(blue * 255));
            };
            redChannel.ValueChanged += channelChangedDelegate;
            greenChannel.ValueChanged += channelChangedDelegate;
            blueChannel.ValueChanged += channelChangedDelegate;

            colorNode.AddItem(redChannel);
            colorNode.AddItem(greenChannel);
            colorNode.AddItem(blueChannel);

            colorItem.Clicked += new EventHandler<NodeItemEventArgs>(OnColClicked);
            colorNode.AddItem(colorItem);
            graphControl.AddNode(colorNode);

            Node textureNode = new Node("Texture")
            {
                Location = new Point(300, 150)
            };
            NodeImageItem imageItem = new NodeImageItem(Properties.Resources.example, 64, 64, false, true) { Tag = 1000f };
            imageItem.Clicked += new EventHandler<NodeItemEventArgs>(OnImgClicked);
            textureNode.AddItem(imageItem);
            graphControl.AddNode(textureNode);

            graphControl.ConnectionAdded += new EventHandler<AcceptNodeConnectionEventArgs>(OnConnectionAdded);
            graphControl.ConnectionAdding += new EventHandler<AcceptNodeConnectionEventArgs>(OnConnectionAdding);
            graphControl.ConnectionRemoving += new EventHandler<AcceptNodeConnectionEventArgs>(OnConnectionRemoved);
            graphControl.ShowElementMenu += new EventHandler<AcceptElementLocationEventArgs>(OnShowElementMenu);

            graphControl.Connect(colorItem, check1Item);
        }

        /*
        Запрещает закрытие формы(и не освобождает память=>валидный указатель на этот редактор),
        вместо этого прячет форму
        */

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }

        private void OnImgClicked(object sender, NodeItemEventArgs e)
        {
            MessageBox.Show("IMAGE");
        }

        private void OnColClicked(object sender, NodeItemEventArgs e)
        {
            MessageBox.Show("Color");
        }

        private void OnConnectionRemoved(object sender, AcceptNodeConnectionEventArgs e)
        {
            //e.Cancel = true;
        }

        private void OnShowElementMenu(object sender, AcceptElementLocationEventArgs e)
        {
            if (e.Element == null)
            {
                // Show a test menu for when you click on nothing
                testMenuItem.Text = "(clicked on nothing)";
                nodeMenu.Show(e.Position);
                e.Cancel = false;
            }
            else
            if (e.Element is Node)
            {
                // Show a test menu for a node
                testMenuItem.Text = ((Node)e.Element).Title;
                nodeMenu.Show(e.Position);
                e.Cancel = false;
            }
            else
            if (e.Element is NodeItem)
            {
                // Show a test menu for a nodeItem
                testMenuItem.Text = e.Element.GetType().Name;
                nodeMenu.Show(e.Position);
                e.Cancel = false;
            }
            else
            {
                // if you don't want to show a menu for this item (but perhaps show a menu for something more higher up)
                // then you can cancel the event
                e.Cancel = true;
            }
        }

        private void OnConnectionAdding(object sender, AcceptNodeConnectionEventArgs e)
        {
            //e.Cancel = true;
        }

        private static int counter = 1;

        private void OnConnectionAdded(object sender, AcceptNodeConnectionEventArgs e)
        {
            //e.Cancel = true;
            e.Connection.Name = "Connection " + counter++;
            e.Connection.DoubleClick += new EventHandler<NodeConnectionEventArgs>(OnConnectionDoubleClick);
        }

        private void OnConnectionDoubleClick(object sender, NodeConnectionEventArgs e)
        {
            e.Connection.Name = "Connection " + counter++;
        }

        private void SomeNode_MouseDown(object sender, MouseEventArgs e)
        {
            Node node = new Node("Some node");
            node.AddItem(new NodeLabelItem("Entry 1", true, false));
            node.AddItem(new NodeLabelItem("Entry 2", true, false));
            node.AddItem(new NodeLabelItem("Entry 3", false, true));
            node.AddItem(new NodeTextBoxItem("TEXTTEXT", false, true));
            node.AddItem(new NodeDropDownItem(new string[] { "1", "2", "3", "4" }, 0, false, false));
            DoDragDrop(node, DragDropEffects.Copy);
        }

        private void TextureNode_MouseDown(object sender, MouseEventArgs e)
        {
            Node textureNode = new Node("Texture")
            {
                Location = new Point(300, 150)
            };
            NodeImageItem imageItem = new NodeImageItem(Properties.Resources.example, 64, 64, false, true);
            imageItem.Clicked += new EventHandler<NodeItemEventArgs>(OnImgClicked);
            textureNode.AddItem(imageItem);
            DoDragDrop(textureNode, DragDropEffects.Copy);
        }

        private void ColorNode_MouseDown(object sender, MouseEventArgs e)
        {
            Node colorNode = new Node("Color")
            {
                Location = new Point(200, 50)
            };
            NodeSliderItem redChannel = new NodeSliderItem("R", 64.0f, 16.0f, 0, 1.0f, 0.0f, false, false);
            NodeSliderItem greenChannel = new NodeSliderItem("G", 64.0f, 16.0f, 0, 1.0f, 0.0f, false, false);
            NodeSliderItem blueChannel = new NodeSliderItem("B", 64.0f, 16.0f, 0, 1.0f, 0.0f, false, false);
            NodeColorItem colorItem = new NodeColorItem("Color", Color.Black, false, true);

            EventHandler<NodeItemEventArgs> channelChangedDelegate = delegate (object s, NodeItemEventArgs args)
            {
                float red = redChannel.Value;
                float green = blueChannel.Value;
                float blue = greenChannel.Value;
                colorItem.Color = Color.FromArgb((int)Math.Round(red * 255), (int)Math.Round(green * 255), (int)Math.Round(blue * 255));
            };
            redChannel.ValueChanged += channelChangedDelegate;
            greenChannel.ValueChanged += channelChangedDelegate;
            blueChannel.ValueChanged += channelChangedDelegate;

            colorNode.AddItem(redChannel);
            colorNode.AddItem(greenChannel);
            colorNode.AddItem(blueChannel);

            colorItem.Clicked += new EventHandler<NodeItemEventArgs>(OnColClicked);
            colorNode.AddItem(colorItem);

            DoDragDrop(colorNode, DragDropEffects.Copy);
        }

        private void OnShowLabelsChanged(object sender, EventArgs e)
        {
            graphControl.ShowLabels = showLabelsCheckBox.Checked;
        }
    }
}