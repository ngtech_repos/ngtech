﻿/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/

using System;
using System.Diagnostics;
using System.IO;

namespace NGEd
{
    public class EditorState
    {
        /// <summary>
        ///
        /// </summary>
        public void SetDefaultEditorTitle()
        {
            DefaultSceneEditorTitle = "NGEd Sandbox";
            if (IntPtr.Size == 8)
            {
                DefaultSceneEditorTitle += " Win64";
            }
            else
            {
                DefaultSceneEditorTitle += " Win32";
            }

            DefaultSceneEditorTitle += NameOfSelectedRender;

            DefaultSceneEditorTitle += " - ";

            SceneEditorTitle = DefaultSceneEditorTitle;

            OldSceneEditorTitle = SceneEditorTitle;
            IsSceneModificated = false;
        }

        public void _PropertyWasChanged()
        {
            OldSceneEditorTitle = SceneEditorTitle;
            SceneEditorTitle += _Star;
            IsSceneModificated = true;
        }

        public void _PropertyWasSavedOrRevert()
        {
            string tmp = OldSceneEditorTitle;
            OldSceneEditorTitle = SceneEditorTitle;
            SceneEditorTitle = tmp;
            IsSceneModificated = false;
        }

        public void UpdateIfLoadedNewScene(string _name)
        {
            CurrentSceneName = _name;
        }

        public void Destroy()
        {
            if (GetEngine != null)
            {
                GetEngine.EngineDestroy();
            }
        }

        public void SetSceneName(string _name, string _directory)
        {
            SceneEditorTitle = DefaultSceneEditorTitle;
            SceneEditorTitle += _name;

            EngineCLR.SceneEditor.Instance.SetSceneName(_name);
            EngineCLR.SceneEditor.Instance.SetDirectoryPath(_directory);
        }

        public void DefaultSceneName()
        {
            SceneEditorTitle = DefaultSceneEditorTitle;
            OldSceneEditorTitle = DefaultSceneEditorTitle;
        }

        public void NewScene()
        {
            DefaultSceneName();

            if (GetEngine == null)
            {
                return;
            }

            GetEngine.NewScene();
        }

        /// <summary>
        ///
        /// </summary>
        public EngineCLR.EngineCLR GetEngine
        {
            get => m_engine;
            set => m_engine = value;
        }

        /// <summary>
        ///  value=false->running
        ///  value=true->pause
        /// </summary>
        public bool IsSimPaused
        {
            get
            {
                EngineCLR.api.EDITOR_CheckWhatWasModificated();
                return EngineCLR.api.EDITOR_IsPausedEngine();
            }

            set
            {
                if (m_engine == null)
                {
                    return;
                }
                Debug.Assert(m_engine != null, "ENGINE IS NULL");

                // Отключаем симуляцию(физика,звук,ии)
                EngineCLR.api.EDITOR_PauseEngine(value);
                EngineCLR.api.EDITOR_CheckWhatWasModificated();
            }
        }

        public bool IsSceneModificated
        {
            get
            {
                EngineCLR.api.EDITOR_CheckWhatWasModificated();
                return m_IsSceneModificated;
            }
            set
            {
                m_IsSceneModificated = value;
                EngineCLR.api.EDITOR_CheckWhatWasModificated();
            }
        }

        public string GetSceneEditorTitle => SceneEditorTitle;

        private string SceneEditorTitle;
        private string OldSceneEditorTitle;

        private static string DefaultSceneEditorTitle;
        private readonly string NameOfSelectedRender = "(OpenGL)";

        public string CurrentSceneName;
        private const string _Star = "*";
        private bool m_IsSceneModificated = false;
        private static EngineCLR.EngineCLR m_engine = null;

        /// <summary>
        /// Singleton Part
        /// </summary>
        private static EditorState instance;

        private EditorState()
        {
            CurrentSceneName = "";
            SetDefaultEditorTitle();
        }

        public static EditorState Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new EditorState();
                }
                return instance;
            }
        }
    };
}