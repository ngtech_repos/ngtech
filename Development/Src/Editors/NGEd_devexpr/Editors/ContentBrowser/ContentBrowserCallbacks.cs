﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ContentBrowser;

namespace NGEd.Editors.ContentBrowser
{
    class ContentBrowserCallbacks
    {
        /// <summary>
        /// Функция для обработки событий клавиатуры. НЕЗАКОНЧЕНА!
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void OnKeyPressed(object sender, System.Windows.Input.KeyEventArgs e)
        {
            bool bCtrlPressed = ((Keyboard.IsKeyDown(Key.RightCtrl)) ||
                                    (Keyboard.IsKeyDown(Key.LeftCtrl)));

            bool bShiftPressed = ((Keyboard.IsKeyDown(Key.RightShift)) ||
                                    (Keyboard.IsKeyDown(Key.LeftShift)));

            bool bAltPressed = ((Keyboard.IsKeyDown(Key.RightAlt)) ||
                                    (Keyboard.IsKeyDown(Key.LeftAlt)));

            bool bEnterPressed = ((Keyboard.IsKeyDown(Key.Return)) ||
                                    (Keyboard.IsKeyDown(Key.Enter)));


            //TODO:NICK:IMPLEMENT LATER
            string KeyString = e.Key.ToString();
            // Alt + Key Events are handled as system events.  The KeyName is "System"
            //FName KeyName = bEnterPressed ? FName(TEXT("Enter")) : FName(*KeyString);
            //GApp->CheckIfGlobalHotkey(KeyName, bCtrlPressed, bShiftPressed, bAltPressed);
        }

    }
}
