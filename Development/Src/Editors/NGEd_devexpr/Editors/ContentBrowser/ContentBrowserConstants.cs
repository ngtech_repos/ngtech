﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGEd
{
    /** Maximum size of name. */
    enum ContentBrowserConstants { NAME_SIZE = 1024 };

    enum ELastDir
    {
        LD_UNR = 0,
        LD_BRUSH,
        LD_2DS,
        LD_PSA,
#if WITH_FBX
	LD_FBX_ANIM,
#endif // WITH_FBX
        LD_GENERIC_IMPORT,
        LD_GENERIC_EXPORT,
        LD_GENERIC_OPEN,
        LD_GENERIC_SAVE,
        LD_MESH_IMPORT_EXPORT,

        NUM_LAST_DIRECTORIES
    };
}
