﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NGEd
{
    /** GameAssetDatabase design constraints */
    struct GADDefs
    {
        /** Journal entry ver.2: Added support for asset 'full names' */
        public static int JournalEntryVersionNumber_AssetFullNames = 2;

        /** Current journal entry version number */
        public static int JournalEntryVersionNumber = JournalEntryVersionNumber_AssetFullNames;

        /** Checkpoint file ver.2: Added persistent collections */
        public static int CheckpointFileVersionNumber_PersistentCollections = 2;

        /** Checkpoint file ver.3: Added support for asset 'full names' */
        public static int CheckpointFileVersionNumber_AssetFullNames = 3;

        /** Current checkpoint file version number */
        public static int CheckpointFileVersionNumber = CheckpointFileVersionNumber_AssetFullNames;

        /** Current journal file version number */
        public static int JournalFileVersionNumber = 1;


        /** Name of each type */
        public static string[] SystemTagTypeNames =
        {
          "<Invalid>",
          "ObjectType",
          "Package",
          "Collection",
          "PrivateCollection",
          "LocalCollection",
          "Unverified",
          "Ghost",
          "Archetype",
          "DateAdded",
          "Quarantined"
        };

        /** Character used to separate a private collection tag from the user's name it's associated with */
        public static char PrivateCollectionUserDelimiter = '@';

        /** Character used to separate the system tag type prefix from the actual tag name */
        public static char SystemTagPreDelimiter = '[';
        public static char SystemTagPostDelimiter = ']';

        /** Name of each journal entry type */
        // NOTE: Type name should be 10 characters or less (for SQL performance)
        public static string[] JournalEntryTypeNames = { "Invalid", "Add", "Remove", "CreateTag", "DestroyTag" };

        /** Character used to separate text elements in a journal entry field */
        public static char JournalEntryFieldDelimiter = '|';

        /** Checkpoint file header bytes ('G'ame 'A'sset 'D'atabase 'C'heckpoint) */
        public static char[] CheckpointFileHeaderChars = { 'G', 'A', 'D', 'C' };

        /** How many days before already-checkpointed journal entries will be deleted from the SQL server.  This
            means that a user must sync up to a later checkpoint file within this span to have correct data */
        public static int DeleteJournalEntriesOlderThanDays = 3;

        /** How many days before we'll purge a non-existent asset from the database.  These are usually assets that
            were deleted, but could also be an asset that was tagged by a user but never submitted to source control */
        public static int DeleteGhostAssetsOlderThanDays = 7;

        /** Maximum supported length of a tag name */
        public static int MaxTagLength = (int)ContentBrowserConstants.NAME_SIZE;
    };
}
