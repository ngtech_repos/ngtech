﻿namespace NGEd
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            WeifenLuo.WinFormsUI.Docking.DockPanelSkin dockPanelSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPanelSkin();
            WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin autoHideStripSkin1 = new WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient1 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin dockPaneStripSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient dockPaneStripGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient2 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient2 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient3 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient dockPaneStripToolWindowGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient4 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient5 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient3 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient6 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient7 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            this.dockPanel1 = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            this.ribbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.applicationMenu1 = new DevExpress.XtraBars.Ribbon.ApplicationMenu(this.components);
            this.NewButton = new DevExpress.XtraBars.BarButtonItem();
            this.OpenStripButton1 = new DevExpress.XtraBars.BarButtonItem();
            this.SaveButton = new DevExpress.XtraBars.BarButtonItem();
            this.SaveAsB = new DevExpress.XtraBars.BarButtonItem();
            this.ImportButton = new DevExpress.XtraBars.BarButtonItem();
            this.iAbout = new DevExpress.XtraBars.BarButtonItem();
            this.rgbiSkins = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.iExit = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonImageCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.spreadsheetCommandBarButtonItem1 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem2 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem3 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem4 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarSubItem1 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem5 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem6 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem7 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarSubItem2 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem8 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem9 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarSubItem3 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem10 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem11 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem12 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem13 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarCheckItem1 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarCheckItem2 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarCheckItem3 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarSubItem4 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem14 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem15 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem16 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarSubItem5 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem17 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem18 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem19 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem20 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarSubItem6 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem21 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem22 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem23 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem24 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem25 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarSubItem7 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem26 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem27 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarCheckItem4 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarCheckItem5 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarCheckItem6 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarCheckItem7 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.galleryPivotStylesItem1 = new DevExpress.XtraSpreadsheet.UI.GalleryPivotStylesItem();
            this.spreadsheetCommandBarSubItem8 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem28 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem29 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarSubItem9 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem30 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem31 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.renameTableItem1 = new DevExpress.XtraSpreadsheet.UI.RenameTableItem();
            this.spreadsheetCommandBarCheckItem8 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarCheckItem9 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarCheckItem10 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarCheckItem11 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarCheckItem12 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarCheckItem13 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarCheckItem14 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.galleryTableStylesItem1 = new DevExpress.XtraSpreadsheet.UI.GalleryTableStylesItem();
            this.spreadsheetCommandBarButtonItem32 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem33 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem34 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.galleryChartLayoutItem1 = new DevExpress.XtraSpreadsheet.UI.GalleryChartLayoutItem();
            this.galleryChartStyleItem1 = new DevExpress.XtraSpreadsheet.UI.GalleryChartStyleItem();
            this.spreadsheetCommandBarButtonGalleryDropDownItem1 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem();
            this.spreadsheetCommandBarSubItem10 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonGalleryDropDownItem2 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem();
            this.spreadsheetCommandBarButtonGalleryDropDownItem3 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem();
            this.spreadsheetCommandBarButtonGalleryDropDownItem4 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem();
            this.spreadsheetCommandBarButtonGalleryDropDownItem5 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem();
            this.spreadsheetCommandBarSubItem11 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonGalleryDropDownItem6 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem();
            this.spreadsheetCommandBarButtonGalleryDropDownItem7 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem();
            this.spreadsheetCommandBarSubItem12 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonGalleryDropDownItem8 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem();
            this.spreadsheetCommandBarButtonGalleryDropDownItem9 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem();
            this.spreadsheetCommandBarButtonGalleryDropDownItem10 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem();
            this.spreadsheetCommandBarButtonGalleryDropDownItem11 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem();
            this.spreadsheetCommandBarButtonGalleryDropDownItem12 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem();
            this.spreadsheetCommandBarCheckItem15 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarCheckItem16 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarButtonItem35 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem36 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem37 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarSubItem13 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem38 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem39 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem40 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem41 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem42 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem43 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem44 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem45 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem46 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem47 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem48 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem49 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem50 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem51 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem52 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarCheckItem17 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarButtonItem53 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem54 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarSubItem14 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem55 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem56 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem57 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarSubItem15 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem58 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem59 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarSubItem16 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem60 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem61 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem62 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem63 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem64 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarSubItem17 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem65 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem66 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem67 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem68 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem69 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.functionsFinancialItem1 = new DevExpress.XtraSpreadsheet.UI.FunctionsFinancialItem();
            this.functionsLogicalItem1 = new DevExpress.XtraSpreadsheet.UI.FunctionsLogicalItem();
            this.functionsTextItem1 = new DevExpress.XtraSpreadsheet.UI.FunctionsTextItem();
            this.functionsDateAndTimeItem1 = new DevExpress.XtraSpreadsheet.UI.FunctionsDateAndTimeItem();
            this.functionsLookupAndReferenceItem1 = new DevExpress.XtraSpreadsheet.UI.FunctionsLookupAndReferenceItem();
            this.functionsMathAndTrigonometryItem1 = new DevExpress.XtraSpreadsheet.UI.FunctionsMathAndTrigonometryItem();
            this.spreadsheetCommandBarSubItem18 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.functionsStatisticalItem1 = new DevExpress.XtraSpreadsheet.UI.FunctionsStatisticalItem();
            this.functionsEngineeringItem1 = new DevExpress.XtraSpreadsheet.UI.FunctionsEngineeringItem();
            this.functionsInformationItem1 = new DevExpress.XtraSpreadsheet.UI.FunctionsInformationItem();
            this.functionsCompatibilityItem1 = new DevExpress.XtraSpreadsheet.UI.FunctionsCompatibilityItem();
            this.functionsWebItem1 = new DevExpress.XtraSpreadsheet.UI.FunctionsWebItem();
            this.spreadsheetCommandBarButtonItem70 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem71 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.definedNameListItem1 = new DevExpress.XtraSpreadsheet.UI.DefinedNameListItem();
            this.spreadsheetCommandBarButtonItem72 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarCheckItem18 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarSubItem19 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarCheckItem19 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarCheckItem20 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarButtonItem73 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem74 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarSubItem20 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarCheckItem21 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarCheckItem22 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarCheckItem23 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarButtonItem75 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarSubItem21 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarCheckItem24 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarCheckItem25 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.pageSetupPaperKindItem1 = new DevExpress.XtraSpreadsheet.UI.PageSetupPaperKindItem();
            this.spreadsheetCommandBarSubItem22 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem76 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem77 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem78 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarCheckItem26 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarCheckItem27 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarButtonItem79 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem80 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem81 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonGalleryDropDownItem13 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem();
            this.spreadsheetCommandBarButtonGalleryDropDownItem14 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem();
            this.spreadsheetCommandBarButtonGalleryDropDownItem15 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem();
            this.spreadsheetCommandBarButtonGalleryDropDownItem16 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem();
            this.spreadsheetCommandBarButtonGalleryDropDownItem17 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem();
            this.spreadsheetCommandBarButtonGalleryDropDownItem18 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem();
            this.spreadsheetCommandBarButtonGalleryDropDownItem19 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem();
            this.spreadsheetCommandBarButtonItem82 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem83 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem84 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem85 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem86 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem87 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.barButtonGroup1 = new DevExpress.XtraBars.BarButtonGroup();
            this.changeFontNameItem1 = new DevExpress.XtraSpreadsheet.UI.ChangeFontNameItem();
            this.changeFontSizeItem1 = new DevExpress.XtraSpreadsheet.UI.ChangeFontSizeItem();
            this.spreadsheetCommandBarButtonItem88 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem89 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.barButtonGroup2 = new DevExpress.XtraBars.BarButtonGroup();
            this.spreadsheetCommandBarCheckItem28 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarCheckItem29 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarCheckItem30 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarCheckItem31 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.barButtonGroup3 = new DevExpress.XtraBars.BarButtonGroup();
            this.spreadsheetCommandBarSubItem23 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem90 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem91 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem92 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem93 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem94 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem95 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem96 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem97 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem98 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem99 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem100 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem101 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem102 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.changeBorderLineColorItem1 = new DevExpress.XtraSpreadsheet.UI.ChangeBorderLineColorItem();
            this.changeBorderLineStyleItem1 = new DevExpress.XtraSpreadsheet.UI.ChangeBorderLineStyleItem();
            this.barButtonGroup4 = new DevExpress.XtraBars.BarButtonGroup();
            this.changeCellFillColorItem1 = new DevExpress.XtraSpreadsheet.UI.ChangeCellFillColorItem();
            this.changeFontColorItem1 = new DevExpress.XtraSpreadsheet.UI.ChangeFontColorItem();
            this.barButtonGroup5 = new DevExpress.XtraBars.BarButtonGroup();
            this.spreadsheetCommandBarCheckItem32 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarCheckItem33 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarCheckItem34 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.barButtonGroup6 = new DevExpress.XtraBars.BarButtonGroup();
            this.spreadsheetCommandBarCheckItem35 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarCheckItem36 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarCheckItem37 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.barButtonGroup7 = new DevExpress.XtraBars.BarButtonGroup();
            this.spreadsheetCommandBarButtonItem103 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem104 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarCheckItem38 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarSubItem24 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarCheckItem39 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarButtonItem105 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem106 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem107 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.barButtonGroup8 = new DevExpress.XtraBars.BarButtonGroup();
            this.changeNumberFormatItem1 = new DevExpress.XtraSpreadsheet.UI.ChangeNumberFormatItem();
            this.barButtonGroup9 = new DevExpress.XtraBars.BarButtonGroup();
            this.spreadsheetCommandBarSubItem25 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem108 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem109 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem110 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem111 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem112 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem113 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem114 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.barButtonGroup10 = new DevExpress.XtraBars.BarButtonGroup();
            this.spreadsheetCommandBarButtonItem115 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem116 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarSubItem26 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarSubItem27 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem117 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem118 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem119 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem120 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem121 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem122 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem123 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarSubItem28 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem124 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem125 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem126 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem127 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem128 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem129 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonGalleryDropDownItem20 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem();
            this.spreadsheetCommandBarButtonGalleryDropDownItem21 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem();
            this.spreadsheetCommandBarButtonGalleryDropDownItem22 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem();
            this.spreadsheetCommandBarSubItem29 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem130 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem131 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.galleryFormatAsTableItem1 = new DevExpress.XtraSpreadsheet.UI.GalleryFormatAsTableItem();
            this.galleryChangeStyleItem1 = new DevExpress.XtraSpreadsheet.UI.GalleryChangeStyleItem();
            this.spreadsheetCommandBarSubItem30 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem132 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem133 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem134 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarSubItem31 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem135 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem136 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem137 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarSubItem32 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem138 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem139 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem140 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem141 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem142 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarSubItem33 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem143 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem144 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem145 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem146 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem147 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem148 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem149 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem150 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.changeSheetTabColorItem1 = new DevExpress.XtraSpreadsheet.UI.ChangeSheetTabColorItem();
            this.spreadsheetCommandBarCheckItem40 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem();
            this.spreadsheetCommandBarButtonItem152 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarSubItem34 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarSubItem35 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem158 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem159 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem160 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem161 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarSubItem36 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem162 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem163 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem164 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem165 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem166 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem167 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarSubItem37 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarSubItem38 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem();
            this.spreadsheetCommandBarButtonItem172 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem173 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem174 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem175 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem176 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem177 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem178 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem179 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem180 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem181 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem182 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem183 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem184 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem185 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem186 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem187 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.spreadsheetCommandBarButtonItem188 = new DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.simulateToolStripMenuItem = new DevExpress.XtraBars.BarCheckItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.barCheckItem1 = new DevExpress.XtraBars.BarCheckItem();
            this.skinRibbonGalleryBarItem1 = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem10 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem11 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem12 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem13 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem14 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem15 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem16 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem17 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem18 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem19 = new DevExpress.XtraBars.BarButtonItem();
            this.barCheckItem2 = new DevExpress.XtraBars.BarCheckItem();
            this.barButtonItem20 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem21 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem22 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem23 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem24 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem25 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem26 = new DevExpress.XtraBars.BarButtonItem();
            this.barCheckItem3 = new DevExpress.XtraBars.BarCheckItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.DiffuseB = new DevExpress.XtraBars.BarButtonItem();
            this.SpecularB = new DevExpress.XtraBars.BarButtonItem();
            this.NormalsB = new DevExpress.XtraBars.BarButtonItem();
            this.WireframeB = new DevExpress.XtraBars.BarCheckItem();
            this.PositionsB = new DevExpress.XtraBars.BarButtonItem();
            this.roughnessB = new DevExpress.XtraBars.BarButtonItem();
            this.SpecularPowerB = new DevExpress.XtraBars.BarButtonItem();
            this.FinalLightingB = new DevExpress.XtraBars.BarButtonItem();
            this.SoundMixEdB = new DevExpress.XtraBars.BarButtonItem();
            this.MatEditorB = new DevExpress.XtraBars.BarButtonItem();
            this.LogicEditorB = new DevExpress.XtraBars.BarButtonItem();
            this.LogicEditorB1 = new DevExpress.XtraBars.BarButtonItem();
            this.ContBrButton = new DevExpress.XtraBars.BarButtonItem();
            this.SelectObjsB = new DevExpress.XtraBars.BarButtonItem();
            this.MakeScrs_B = new DevExpress.XtraBars.BarButtonItem();
            this.calcGILightmaps = new DevExpress.XtraBars.BarButtonItem();
            this.SelectButton = new DevExpress.XtraBars.BarButtonItem();
            this.ScaleB = new DevExpress.XtraBars.BarButtonItem();
            this.RotateB = new DevExpress.XtraBars.BarButtonItem();
            this.TranslateB = new DevExpress.XtraBars.BarButtonItem();
            this.AutoSaveItem = new DevExpress.XtraBars.BarCheckItem();
            this.GameFolderItem = new DevExpress.XtraBars.BarStaticItem();
            this.GameNameItem = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.RenderDepthB = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem27 = new DevExpress.XtraBars.BarButtonItem();
            this.PopUp_Add_Actor = new DevExpress.XtraBars.BarSubItem();
            this.MaterialsButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.ShadersButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.MaterialsBRDFVisB = new DevExpress.XtraBars.BarCheckItem();
            this.RenderDoc = new DevExpress.XtraBars.BarButtonItem();
            this.ScriptDebugger = new DevExpress.XtraBars.BarButtonItem();
            this.Brofiler = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem3 = new DevExpress.XtraBars.BarSubItem();
            this.calccollisions = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonImageCollectionLarge = new DevExpress.Utils.ImageCollection(this.components);
            this.MainPage = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.EditorsPG = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ToolsPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.DebugPage = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.RenderPG = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.RefreshPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.DeveloperToolsGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.LevelBuilding = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.siStatus = new DevExpress.XtraBars.BarStaticItem();
            this.selectedObjCapt = new DevExpress.XtraBars.BarStaticItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.formAssistant1 = new DevExpress.XtraBars.FormAssistant();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.autosaveTimer = new System.Windows.Forms.Timer(this.components);
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel4 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel4_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.dockPanel2 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel2_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.dockPanel3 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel3_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollectionLarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel4.SuspendLayout();
            this.dockPanel2.SuspendLayout();
            this.dockPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            this.SuspendLayout();
            // 
            // dockPanel1
            // 
            this.dockPanel1.ActiveAutoHideContent = null;
            resources.ApplyResources(this.dockPanel1, "dockPanel1");
            this.dockPanel1.DockBackColor = System.Drawing.SystemColors.Control;
            this.dockPanel1.DocumentStyle = WeifenLuo.WinFormsUI.Docking.DocumentStyle.DockingWindow;
            this.dockPanel1.Name = "dockPanel1";
            dockPanelGradient1.EndColor = System.Drawing.SystemColors.ControlLight;
            dockPanelGradient1.StartColor = System.Drawing.SystemColors.ControlLight;
            autoHideStripSkin1.DockStripGradient = dockPanelGradient1;
            tabGradient1.EndColor = System.Drawing.SystemColors.Control;
            tabGradient1.StartColor = System.Drawing.SystemColors.Control;
            tabGradient1.TextColor = System.Drawing.SystemColors.ControlDarkDark;
            autoHideStripSkin1.TabGradient = tabGradient1;
            dockPanelSkin1.AutoHideStripSkin = autoHideStripSkin1;
            tabGradient2.EndColor = System.Drawing.SystemColors.ControlLightLight;
            tabGradient2.StartColor = System.Drawing.SystemColors.ControlLightLight;
            tabGradient2.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripGradient1.ActiveTabGradient = tabGradient2;
            dockPanelGradient2.EndColor = System.Drawing.SystemColors.Control;
            dockPanelGradient2.StartColor = System.Drawing.SystemColors.Control;
            dockPaneStripGradient1.DockStripGradient = dockPanelGradient2;
            tabGradient3.EndColor = System.Drawing.SystemColors.ControlLight;
            tabGradient3.StartColor = System.Drawing.SystemColors.ControlLight;
            tabGradient3.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripGradient1.InactiveTabGradient = tabGradient3;
            dockPaneStripSkin1.DocumentGradient = dockPaneStripGradient1;
            tabGradient4.EndColor = System.Drawing.SystemColors.ActiveCaption;
            tabGradient4.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient4.StartColor = System.Drawing.SystemColors.GradientActiveCaption;
            tabGradient4.TextColor = System.Drawing.SystemColors.ActiveCaptionText;
            dockPaneStripToolWindowGradient1.ActiveCaptionGradient = tabGradient4;
            tabGradient5.EndColor = System.Drawing.SystemColors.Control;
            tabGradient5.StartColor = System.Drawing.SystemColors.Control;
            tabGradient5.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripToolWindowGradient1.ActiveTabGradient = tabGradient5;
            dockPanelGradient3.EndColor = System.Drawing.SystemColors.ControlLight;
            dockPanelGradient3.StartColor = System.Drawing.SystemColors.ControlLight;
            dockPaneStripToolWindowGradient1.DockStripGradient = dockPanelGradient3;
            tabGradient6.EndColor = System.Drawing.SystemColors.GradientInactiveCaption;
            tabGradient6.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient6.StartColor = System.Drawing.SystemColors.GradientInactiveCaption;
            tabGradient6.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripToolWindowGradient1.InactiveCaptionGradient = tabGradient6;
            tabGradient7.EndColor = System.Drawing.Color.Transparent;
            tabGradient7.StartColor = System.Drawing.Color.Transparent;
            tabGradient7.TextColor = System.Drawing.SystemColors.ControlDarkDark;
            dockPaneStripToolWindowGradient1.InactiveTabGradient = tabGradient7;
            dockPaneStripSkin1.ToolWindowGradient = dockPaneStripToolWindowGradient1;
            dockPanelSkin1.DockPaneStripSkin = dockPaneStripSkin1;
            this.dockPanel1.Skin = dockPanelSkin1;
            this.dockPanel1.Resize += new System.EventHandler(this.dockPanel1_Resize);
            // 
            // ribbonControl
            // 
            this.ribbonControl.ApplicationButtonDropDownControl = this.applicationMenu1;
            resources.ApplyResources(this.ribbonControl, "ribbonControl");
            this.ribbonControl.ExpandCollapseItem.Id = 0;
            this.ribbonControl.Images = this.ribbonImageCollection;
            this.ribbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl.ExpandCollapseItem,
            this.iExit,
            this.iAbout,
            this.rgbiSkins,
            this.spreadsheetCommandBarButtonItem1,
            this.spreadsheetCommandBarButtonItem2,
            this.spreadsheetCommandBarButtonItem3,
            this.spreadsheetCommandBarButtonItem4,
            this.spreadsheetCommandBarSubItem1,
            this.spreadsheetCommandBarButtonItem5,
            this.spreadsheetCommandBarButtonItem6,
            this.spreadsheetCommandBarButtonItem7,
            this.spreadsheetCommandBarSubItem2,
            this.spreadsheetCommandBarButtonItem8,
            this.spreadsheetCommandBarButtonItem9,
            this.spreadsheetCommandBarSubItem3,
            this.spreadsheetCommandBarButtonItem10,
            this.spreadsheetCommandBarButtonItem11,
            this.spreadsheetCommandBarButtonItem12,
            this.spreadsheetCommandBarButtonItem13,
            this.spreadsheetCommandBarCheckItem1,
            this.spreadsheetCommandBarCheckItem2,
            this.spreadsheetCommandBarCheckItem3,
            this.spreadsheetCommandBarSubItem4,
            this.spreadsheetCommandBarButtonItem14,
            this.spreadsheetCommandBarButtonItem15,
            this.spreadsheetCommandBarButtonItem16,
            this.spreadsheetCommandBarSubItem5,
            this.spreadsheetCommandBarButtonItem17,
            this.spreadsheetCommandBarButtonItem18,
            this.spreadsheetCommandBarButtonItem19,
            this.spreadsheetCommandBarButtonItem20,
            this.spreadsheetCommandBarSubItem6,
            this.spreadsheetCommandBarButtonItem21,
            this.spreadsheetCommandBarButtonItem22,
            this.spreadsheetCommandBarButtonItem23,
            this.spreadsheetCommandBarButtonItem24,
            this.spreadsheetCommandBarButtonItem25,
            this.spreadsheetCommandBarSubItem7,
            this.spreadsheetCommandBarButtonItem26,
            this.spreadsheetCommandBarButtonItem27,
            this.spreadsheetCommandBarCheckItem4,
            this.spreadsheetCommandBarCheckItem5,
            this.spreadsheetCommandBarCheckItem6,
            this.spreadsheetCommandBarCheckItem7,
            this.galleryPivotStylesItem1,
            this.spreadsheetCommandBarSubItem8,
            this.spreadsheetCommandBarButtonItem28,
            this.spreadsheetCommandBarButtonItem29,
            this.spreadsheetCommandBarSubItem9,
            this.spreadsheetCommandBarButtonItem30,
            this.spreadsheetCommandBarButtonItem31,
            this.barStaticItem1,
            this.renameTableItem1,
            this.spreadsheetCommandBarCheckItem8,
            this.spreadsheetCommandBarCheckItem9,
            this.spreadsheetCommandBarCheckItem10,
            this.spreadsheetCommandBarCheckItem11,
            this.spreadsheetCommandBarCheckItem12,
            this.spreadsheetCommandBarCheckItem13,
            this.spreadsheetCommandBarCheckItem14,
            this.galleryTableStylesItem1,
            this.spreadsheetCommandBarButtonItem32,
            this.spreadsheetCommandBarButtonItem33,
            this.spreadsheetCommandBarButtonItem34,
            this.galleryChartLayoutItem1,
            this.galleryChartStyleItem1,
            this.spreadsheetCommandBarButtonGalleryDropDownItem1,
            this.spreadsheetCommandBarSubItem10,
            this.spreadsheetCommandBarButtonGalleryDropDownItem2,
            this.spreadsheetCommandBarButtonGalleryDropDownItem3,
            this.spreadsheetCommandBarButtonGalleryDropDownItem4,
            this.spreadsheetCommandBarButtonGalleryDropDownItem5,
            this.spreadsheetCommandBarSubItem11,
            this.spreadsheetCommandBarButtonGalleryDropDownItem6,
            this.spreadsheetCommandBarButtonGalleryDropDownItem7,
            this.spreadsheetCommandBarSubItem12,
            this.spreadsheetCommandBarButtonGalleryDropDownItem8,
            this.spreadsheetCommandBarButtonGalleryDropDownItem9,
            this.spreadsheetCommandBarButtonGalleryDropDownItem10,
            this.spreadsheetCommandBarButtonGalleryDropDownItem11,
            this.spreadsheetCommandBarButtonGalleryDropDownItem12,
            this.spreadsheetCommandBarCheckItem15,
            this.spreadsheetCommandBarCheckItem16,
            this.spreadsheetCommandBarButtonItem35,
            this.spreadsheetCommandBarButtonItem36,
            this.spreadsheetCommandBarButtonItem37,
            this.spreadsheetCommandBarSubItem13,
            this.spreadsheetCommandBarButtonItem38,
            this.spreadsheetCommandBarButtonItem39,
            this.spreadsheetCommandBarButtonItem40,
            this.spreadsheetCommandBarButtonItem41,
            this.spreadsheetCommandBarButtonItem42,
            this.spreadsheetCommandBarButtonItem43,
            this.spreadsheetCommandBarButtonItem44,
            this.spreadsheetCommandBarButtonItem45,
            this.spreadsheetCommandBarButtonItem46,
            this.spreadsheetCommandBarButtonItem47,
            this.spreadsheetCommandBarButtonItem48,
            this.spreadsheetCommandBarButtonItem49,
            this.spreadsheetCommandBarButtonItem50,
            this.spreadsheetCommandBarButtonItem51,
            this.spreadsheetCommandBarButtonItem52,
            this.spreadsheetCommandBarCheckItem17,
            this.spreadsheetCommandBarButtonItem53,
            this.spreadsheetCommandBarButtonItem54,
            this.spreadsheetCommandBarSubItem14,
            this.spreadsheetCommandBarButtonItem55,
            this.spreadsheetCommandBarButtonItem56,
            this.spreadsheetCommandBarButtonItem57,
            this.spreadsheetCommandBarSubItem15,
            this.spreadsheetCommandBarButtonItem58,
            this.spreadsheetCommandBarButtonItem59,
            this.spreadsheetCommandBarSubItem16,
            this.spreadsheetCommandBarButtonItem60,
            this.spreadsheetCommandBarButtonItem61,
            this.spreadsheetCommandBarButtonItem62,
            this.spreadsheetCommandBarButtonItem63,
            this.spreadsheetCommandBarButtonItem64,
            this.spreadsheetCommandBarSubItem17,
            this.spreadsheetCommandBarButtonItem65,
            this.spreadsheetCommandBarButtonItem66,
            this.spreadsheetCommandBarButtonItem67,
            this.spreadsheetCommandBarButtonItem68,
            this.spreadsheetCommandBarButtonItem69,
            this.functionsFinancialItem1,
            this.functionsLogicalItem1,
            this.functionsTextItem1,
            this.functionsDateAndTimeItem1,
            this.functionsLookupAndReferenceItem1,
            this.functionsMathAndTrigonometryItem1,
            this.spreadsheetCommandBarSubItem18,
            this.functionsStatisticalItem1,
            this.functionsEngineeringItem1,
            this.functionsInformationItem1,
            this.functionsCompatibilityItem1,
            this.functionsWebItem1,
            this.spreadsheetCommandBarButtonItem70,
            this.spreadsheetCommandBarButtonItem71,
            this.definedNameListItem1,
            this.spreadsheetCommandBarButtonItem72,
            this.spreadsheetCommandBarCheckItem18,
            this.spreadsheetCommandBarSubItem19,
            this.spreadsheetCommandBarCheckItem19,
            this.spreadsheetCommandBarCheckItem20,
            this.spreadsheetCommandBarButtonItem73,
            this.spreadsheetCommandBarButtonItem74,
            this.spreadsheetCommandBarSubItem20,
            this.spreadsheetCommandBarCheckItem21,
            this.spreadsheetCommandBarCheckItem22,
            this.spreadsheetCommandBarCheckItem23,
            this.spreadsheetCommandBarButtonItem75,
            this.spreadsheetCommandBarSubItem21,
            this.spreadsheetCommandBarCheckItem24,
            this.spreadsheetCommandBarCheckItem25,
            this.pageSetupPaperKindItem1,
            this.spreadsheetCommandBarSubItem22,
            this.spreadsheetCommandBarButtonItem76,
            this.spreadsheetCommandBarButtonItem77,
            this.spreadsheetCommandBarButtonItem78,
            this.spreadsheetCommandBarCheckItem26,
            this.spreadsheetCommandBarCheckItem27,
            this.spreadsheetCommandBarButtonItem79,
            this.spreadsheetCommandBarButtonItem80,
            this.spreadsheetCommandBarButtonItem81,
            this.spreadsheetCommandBarButtonGalleryDropDownItem13,
            this.spreadsheetCommandBarButtonGalleryDropDownItem14,
            this.spreadsheetCommandBarButtonGalleryDropDownItem15,
            this.spreadsheetCommandBarButtonGalleryDropDownItem16,
            this.spreadsheetCommandBarButtonGalleryDropDownItem17,
            this.spreadsheetCommandBarButtonGalleryDropDownItem18,
            this.spreadsheetCommandBarButtonGalleryDropDownItem19,
            this.spreadsheetCommandBarButtonItem82,
            this.spreadsheetCommandBarButtonItem83,
            this.spreadsheetCommandBarButtonItem84,
            this.spreadsheetCommandBarButtonItem85,
            this.spreadsheetCommandBarButtonItem86,
            this.spreadsheetCommandBarButtonItem87,
            this.barButtonGroup1,
            this.changeFontNameItem1,
            this.changeFontSizeItem1,
            this.spreadsheetCommandBarButtonItem88,
            this.spreadsheetCommandBarButtonItem89,
            this.barButtonGroup2,
            this.spreadsheetCommandBarCheckItem28,
            this.spreadsheetCommandBarCheckItem29,
            this.spreadsheetCommandBarCheckItem30,
            this.spreadsheetCommandBarCheckItem31,
            this.barButtonGroup3,
            this.spreadsheetCommandBarSubItem23,
            this.spreadsheetCommandBarButtonItem90,
            this.spreadsheetCommandBarButtonItem91,
            this.spreadsheetCommandBarButtonItem92,
            this.spreadsheetCommandBarButtonItem93,
            this.spreadsheetCommandBarButtonItem94,
            this.spreadsheetCommandBarButtonItem95,
            this.spreadsheetCommandBarButtonItem96,
            this.spreadsheetCommandBarButtonItem97,
            this.spreadsheetCommandBarButtonItem98,
            this.spreadsheetCommandBarButtonItem99,
            this.spreadsheetCommandBarButtonItem100,
            this.spreadsheetCommandBarButtonItem101,
            this.spreadsheetCommandBarButtonItem102,
            this.changeBorderLineColorItem1,
            this.changeBorderLineStyleItem1,
            this.barButtonGroup4,
            this.changeCellFillColorItem1,
            this.changeFontColorItem1,
            this.barButtonGroup5,
            this.spreadsheetCommandBarCheckItem32,
            this.spreadsheetCommandBarCheckItem33,
            this.spreadsheetCommandBarCheckItem34,
            this.barButtonGroup6,
            this.spreadsheetCommandBarCheckItem35,
            this.spreadsheetCommandBarCheckItem36,
            this.spreadsheetCommandBarCheckItem37,
            this.barButtonGroup7,
            this.spreadsheetCommandBarButtonItem103,
            this.spreadsheetCommandBarButtonItem104,
            this.spreadsheetCommandBarCheckItem38,
            this.spreadsheetCommandBarSubItem24,
            this.spreadsheetCommandBarCheckItem39,
            this.spreadsheetCommandBarButtonItem105,
            this.spreadsheetCommandBarButtonItem106,
            this.spreadsheetCommandBarButtonItem107,
            this.barButtonGroup8,
            this.changeNumberFormatItem1,
            this.barButtonGroup9,
            this.spreadsheetCommandBarSubItem25,
            this.spreadsheetCommandBarButtonItem108,
            this.spreadsheetCommandBarButtonItem109,
            this.spreadsheetCommandBarButtonItem110,
            this.spreadsheetCommandBarButtonItem111,
            this.spreadsheetCommandBarButtonItem112,
            this.spreadsheetCommandBarButtonItem113,
            this.spreadsheetCommandBarButtonItem114,
            this.barButtonGroup10,
            this.spreadsheetCommandBarButtonItem115,
            this.spreadsheetCommandBarButtonItem116,
            this.spreadsheetCommandBarSubItem26,
            this.spreadsheetCommandBarButtonItem117,
            this.spreadsheetCommandBarButtonItem118,
            this.spreadsheetCommandBarButtonItem119,
            this.spreadsheetCommandBarButtonItem120,
            this.spreadsheetCommandBarButtonItem121,
            this.spreadsheetCommandBarButtonItem122,
            this.spreadsheetCommandBarButtonItem123,
            this.spreadsheetCommandBarSubItem27,
            this.spreadsheetCommandBarButtonItem124,
            this.spreadsheetCommandBarButtonItem125,
            this.spreadsheetCommandBarButtonItem126,
            this.spreadsheetCommandBarButtonItem127,
            this.spreadsheetCommandBarButtonItem128,
            this.spreadsheetCommandBarButtonItem129,
            this.spreadsheetCommandBarSubItem28,
            this.spreadsheetCommandBarButtonGalleryDropDownItem20,
            this.spreadsheetCommandBarButtonGalleryDropDownItem21,
            this.spreadsheetCommandBarButtonGalleryDropDownItem22,
            this.spreadsheetCommandBarButtonItem130,
            this.spreadsheetCommandBarButtonItem131,
            this.spreadsheetCommandBarSubItem29,
            this.galleryFormatAsTableItem1,
            this.galleryChangeStyleItem1,
            this.spreadsheetCommandBarSubItem30,
            this.spreadsheetCommandBarButtonItem132,
            this.spreadsheetCommandBarButtonItem133,
            this.spreadsheetCommandBarButtonItem134,
            this.spreadsheetCommandBarSubItem31,
            this.spreadsheetCommandBarButtonItem135,
            this.spreadsheetCommandBarButtonItem136,
            this.spreadsheetCommandBarButtonItem137,
            this.spreadsheetCommandBarSubItem32,
            this.spreadsheetCommandBarButtonItem138,
            this.spreadsheetCommandBarButtonItem139,
            this.spreadsheetCommandBarButtonItem140,
            this.spreadsheetCommandBarButtonItem141,
            this.spreadsheetCommandBarButtonItem142,
            this.spreadsheetCommandBarButtonItem143,
            this.spreadsheetCommandBarButtonItem144,
            this.spreadsheetCommandBarButtonItem145,
            this.spreadsheetCommandBarButtonItem146,
            this.spreadsheetCommandBarButtonItem147,
            this.spreadsheetCommandBarButtonItem148,
            this.spreadsheetCommandBarSubItem33,
            this.spreadsheetCommandBarButtonItem149,
            this.spreadsheetCommandBarButtonItem150,
            this.changeSheetTabColorItem1,
            this.spreadsheetCommandBarCheckItem40,
            this.spreadsheetCommandBarButtonItem152,
            this.spreadsheetCommandBarSubItem34,
            this.spreadsheetCommandBarSubItem35,
            this.spreadsheetCommandBarButtonItem158,
            this.spreadsheetCommandBarButtonItem159,
            this.spreadsheetCommandBarButtonItem160,
            this.spreadsheetCommandBarButtonItem161,
            this.spreadsheetCommandBarSubItem36,
            this.spreadsheetCommandBarButtonItem162,
            this.spreadsheetCommandBarButtonItem163,
            this.spreadsheetCommandBarButtonItem164,
            this.spreadsheetCommandBarButtonItem165,
            this.spreadsheetCommandBarButtonItem166,
            this.spreadsheetCommandBarButtonItem167,
            this.spreadsheetCommandBarSubItem37,
            this.spreadsheetCommandBarSubItem38,
            this.spreadsheetCommandBarButtonItem172,
            this.spreadsheetCommandBarButtonItem173,
            this.spreadsheetCommandBarButtonItem174,
            this.spreadsheetCommandBarButtonItem175,
            this.spreadsheetCommandBarButtonItem176,
            this.spreadsheetCommandBarButtonItem177,
            this.spreadsheetCommandBarButtonItem178,
            this.spreadsheetCommandBarButtonItem179,
            this.spreadsheetCommandBarButtonItem180,
            this.spreadsheetCommandBarButtonItem181,
            this.spreadsheetCommandBarButtonItem182,
            this.spreadsheetCommandBarButtonItem183,
            this.spreadsheetCommandBarButtonItem184,
            this.spreadsheetCommandBarButtonItem185,
            this.spreadsheetCommandBarButtonItem186,
            this.spreadsheetCommandBarButtonItem187,
            this.spreadsheetCommandBarButtonItem188,
            this.barButtonItem1,
            this.simulateToolStripMenuItem,
            this.NewButton,
            this.OpenStripButton1,
            this.barButtonItem7,
            this.barButtonItem8,
            this.barButtonItem9,
            this.barCheckItem1,
            this.skinRibbonGalleryBarItem1,
            this.barButtonItem6,
            this.barButtonItem10,
            this.barButtonItem11,
            this.barButtonItem12,
            this.barButtonItem13,
            this.barButtonItem14,
            this.barButtonItem15,
            this.barButtonItem16,
            this.barButtonItem17,
            this.barButtonItem18,
            this.barButtonItem19,
            this.barCheckItem2,
            this.barButtonItem20,
            this.barButtonItem21,
            this.barButtonItem22,
            this.barButtonItem23,
            this.barButtonItem24,
            this.barButtonItem25,
            this.barButtonItem26,
            this.barCheckItem3,
            this.barSubItem1,
            this.DiffuseB,
            this.SpecularB,
            this.NormalsB,
            this.WireframeB,
            this.PositionsB,
            this.roughnessB,
            this.SpecularPowerB,
            this.FinalLightingB,
            this.SoundMixEdB,
            this.MatEditorB,
            this.LogicEditorB,
            this.LogicEditorB1,
            this.ContBrButton,
            this.SelectObjsB,
            this.MakeScrs_B,
            this.SaveButton,
            this.SaveAsB,
            this.ImportButton,
            this.calcGILightmaps,
            this.SelectButton,
            this.ScaleB,
            this.RotateB,
            this.TranslateB,
            this.AutoSaveItem,
            this.GameFolderItem,
            this.GameNameItem,
            this.barStaticItem2,
            this.RenderDepthB,
            this.barButtonItem2,
            this.barButtonItem27,
            this.PopUp_Add_Actor,
            this.MaterialsButtonItem,
            this.ShadersButtonItem,
            this.MaterialsBRDFVisB,
            this.RenderDoc,
            this.ScriptDebugger,
            this.Brofiler,
            this.barSubItem3,
            this.calccollisions});
            this.ribbonControl.LargeImages = this.ribbonImageCollectionLarge;
            this.ribbonControl.MaxItemId = 2;
            this.ribbonControl.Name = "ribbonControl";
            this.ribbonControl.PageHeaderItemLinks.Add(this.iAbout);
            this.ribbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.MainPage,
            this.DebugPage,
            this.ribbonPage1,
            this.ribbonPage2});
            this.ribbonControl.QuickToolbarItemLinks.Add(this.NewButton);
            this.ribbonControl.QuickToolbarItemLinks.Add(this.OpenStripButton1);
            this.ribbonControl.QuickToolbarItemLinks.Add(this.SaveButton);
            this.ribbonControl.QuickToolbarItemLinks.Add(this.SaveAsB);
            this.ribbonControl.QuickToolbarItemLinks.Add(this.skinRibbonGalleryBarItem1);
            this.ribbonControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2007;
            this.ribbonControl.StatusBar = this.ribbonStatusBar;
            // 
            // applicationMenu1
            // 
            this.applicationMenu1.ItemLinks.Add(this.NewButton);
            this.applicationMenu1.ItemLinks.Add(this.OpenStripButton1);
            this.applicationMenu1.ItemLinks.Add(this.SaveButton);
            this.applicationMenu1.ItemLinks.Add(this.SaveAsB);
            this.applicationMenu1.ItemLinks.Add(this.ImportButton);
            this.applicationMenu1.ItemLinks.Add(this.iAbout);
            this.applicationMenu1.ItemLinks.Add(this.rgbiSkins);
            this.applicationMenu1.ItemLinks.Add(this.iExit);
            this.applicationMenu1.Name = "applicationMenu1";
            this.applicationMenu1.Ribbon = this.ribbonControl;
            // 
            // NewButton
            // 
            resources.ApplyResources(this.NewButton, "NewButton");
            this.NewButton.Id = 387;
            this.NewButton.ImageIndexDisabled = 0;
            this.NewButton.ImageOptions.DisabledImageIndex = 0;
            this.NewButton.ImageOptions.ImageIndex = 0;
            this.NewButton.ImageOptions.ImageUri.Uri = "Copy";
            this.NewButton.Name = "NewButton";
            this.NewButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.NewButton_ItemClick);
            // 
            // OpenStripButton1
            // 
            resources.ApplyResources(this.OpenStripButton1, "OpenStripButton1");
            this.OpenStripButton1.Id = 388;
            this.OpenStripButton1.ImageIndexDisabled = 1;
            this.OpenStripButton1.ImageOptions.DisabledImageIndex = 1;
            this.OpenStripButton1.ImageOptions.ImageIndex = 1;
            this.OpenStripButton1.ImageOptions.ImageUri.Uri = "Open";
            this.OpenStripButton1.Name = "OpenStripButton1";
            this.OpenStripButton1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.OpenStripButton1_ItemClick);
            // 
            // SaveButton
            // 
            resources.ApplyResources(this.SaveButton, "SaveButton");
            this.SaveButton.Id = 446;
            this.SaveButton.ImageIndexDisabled = 4;
            this.SaveButton.ImageOptions.DisabledImageIndex = 4;
            this.SaveButton.ImageOptions.DisabledLargeImageIndex = 4;
            this.SaveButton.ImageOptions.ImageIndex = 4;
            this.SaveButton.ImageOptions.ImageUri.Uri = "Save";
            this.SaveButton.ImageOptions.LargeImageIndex = 4;
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.SaveButton_ItemClick);
            // 
            // SaveAsB
            // 
            resources.ApplyResources(this.SaveAsB, "SaveAsB");
            this.SaveAsB.Id = 447;
            this.SaveAsB.ImageIndexDisabled = 5;
            this.SaveAsB.ImageOptions.DisabledImageIndex = 5;
            this.SaveAsB.ImageOptions.DisabledLargeImageIndex = 5;
            this.SaveAsB.ImageOptions.ImageIndex = 5;
            this.SaveAsB.ImageOptions.LargeImageIndex = 5;
            this.SaveAsB.Name = "SaveAsB";
            this.SaveAsB.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.SaveAsB_ItemClick);
            // 
            // ImportButton
            // 
            resources.ApplyResources(this.ImportButton, "ImportButton");
            this.ImportButton.Id = 448;
            this.ImportButton.ImageOptions.ImageUri.Uri = "AddItem";
            this.ImportButton.Name = "ImportButton";
            this.ImportButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ImportButton_ItemClick);
            // 
            // iAbout
            // 
            resources.ApplyResources(this.iAbout, "iAbout");
            this.iAbout.Id = 24;
            this.iAbout.ImageOptions.ImageIndex = 8;
            this.iAbout.ImageOptions.LargeImageIndex = 8;
            this.iAbout.Name = "iAbout";
            this.iAbout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iAbout_ItemClick);
            // 
            // rgbiSkins
            // 
            resources.ApplyResources(this.rgbiSkins, "rgbiSkins");
            // 
            // 
            // 
            this.rgbiSkins.Gallery.AllowGlyphSkinning = true;
            this.rgbiSkins.Gallery.AllowHoverImages = true;
            this.rgbiSkins.Gallery.AllowHtmlText = true;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseFont = true;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseTextOptions = true;
            this.rgbiSkins.Gallery.Appearance.ItemCaptionAppearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.rgbiSkins.Gallery.ColumnCount = 4;
            this.rgbiSkins.Gallery.FixedHoverImageSize = false;
            this.rgbiSkins.Gallery.ImageSize = new System.Drawing.Size(32, 17);
            this.rgbiSkins.Gallery.ItemImageLocation = DevExpress.Utils.Locations.Top;
            this.rgbiSkins.Gallery.RowCount = 4;
            this.rgbiSkins.Id = 60;
            this.rgbiSkins.Name = "rgbiSkins";
            // 
            // iExit
            // 
            resources.ApplyResources(this.iExit, "iExit");
            this.iExit.Id = 20;
            this.iExit.ImageOptions.ImageIndex = 6;
            this.iExit.ImageOptions.LargeImageIndex = 6;
            this.iExit.Name = "iExit";
            this.iExit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.iExit_ItemClick);
            // 
            // ribbonImageCollection
            // 
            this.ribbonImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ribbonImageCollection.ImageStream")));
            this.ribbonImageCollection.Images.SetKeyName(0, "Ribbon_Exit_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(1, "Ribbon_Content_16x16.png");
            this.ribbonImageCollection.Images.SetKeyName(2, "Ribbon_Info_16x16.png");
            // 
            // spreadsheetCommandBarButtonItem1
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem1, "spreadsheetCommandBarButtonItem1");
            this.spreadsheetCommandBarButtonItem1.CommandName = "OptionsPivotTable";
            this.spreadsheetCommandBarButtonItem1.Enabled = false;
            this.spreadsheetCommandBarButtonItem1.Id = 62;
            this.spreadsheetCommandBarButtonItem1.Name = "spreadsheetCommandBarButtonItem1";
            // 
            // spreadsheetCommandBarButtonItem2
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem2, "spreadsheetCommandBarButtonItem2");
            this.spreadsheetCommandBarButtonItem2.CommandName = "SelectFieldTypePivotTable";
            this.spreadsheetCommandBarButtonItem2.Enabled = false;
            this.spreadsheetCommandBarButtonItem2.Id = 63;
            this.spreadsheetCommandBarButtonItem2.Name = "spreadsheetCommandBarButtonItem2";
            // 
            // spreadsheetCommandBarButtonItem3
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem3, "spreadsheetCommandBarButtonItem3");
            this.spreadsheetCommandBarButtonItem3.CommandName = "PivotTableExpandField";
            this.spreadsheetCommandBarButtonItem3.Enabled = false;
            this.spreadsheetCommandBarButtonItem3.Id = 64;
            this.spreadsheetCommandBarButtonItem3.Name = "spreadsheetCommandBarButtonItem3";
            this.spreadsheetCommandBarButtonItem3.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // spreadsheetCommandBarButtonItem4
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem4, "spreadsheetCommandBarButtonItem4");
            this.spreadsheetCommandBarButtonItem4.CommandName = "PivotTableCollapseField";
            this.spreadsheetCommandBarButtonItem4.Enabled = false;
            this.spreadsheetCommandBarButtonItem4.Id = 65;
            this.spreadsheetCommandBarButtonItem4.Name = "spreadsheetCommandBarButtonItem4";
            this.spreadsheetCommandBarButtonItem4.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // spreadsheetCommandBarSubItem1
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem1, "spreadsheetCommandBarSubItem1");
            this.spreadsheetCommandBarSubItem1.CommandName = "PivotTableDataRefreshGroup";
            this.spreadsheetCommandBarSubItem1.Enabled = false;
            this.spreadsheetCommandBarSubItem1.Id = 66;
            this.spreadsheetCommandBarSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem5),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem6)});
            this.spreadsheetCommandBarSubItem1.Name = "spreadsheetCommandBarSubItem1";
            // 
            // spreadsheetCommandBarButtonItem5
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem5, "spreadsheetCommandBarButtonItem5");
            this.spreadsheetCommandBarButtonItem5.CommandName = "RefreshPivotTable";
            this.spreadsheetCommandBarButtonItem5.Enabled = false;
            this.spreadsheetCommandBarButtonItem5.Id = 67;
            this.spreadsheetCommandBarButtonItem5.Name = "spreadsheetCommandBarButtonItem5";
            // 
            // spreadsheetCommandBarButtonItem6
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem6, "spreadsheetCommandBarButtonItem6");
            this.spreadsheetCommandBarButtonItem6.CommandName = "RefreshAllPivotTable";
            this.spreadsheetCommandBarButtonItem6.Enabled = false;
            this.spreadsheetCommandBarButtonItem6.Id = 68;
            this.spreadsheetCommandBarButtonItem6.Name = "spreadsheetCommandBarButtonItem6";
            // 
            // spreadsheetCommandBarButtonItem7
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem7, "spreadsheetCommandBarButtonItem7");
            this.spreadsheetCommandBarButtonItem7.CommandName = "ChangeDataSourcePivotTable";
            this.spreadsheetCommandBarButtonItem7.Enabled = false;
            this.spreadsheetCommandBarButtonItem7.Id = 69;
            this.spreadsheetCommandBarButtonItem7.Name = "spreadsheetCommandBarButtonItem7";
            // 
            // spreadsheetCommandBarSubItem2
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem2, "spreadsheetCommandBarSubItem2");
            this.spreadsheetCommandBarSubItem2.CommandName = "PivotTableActionsClearGroup";
            this.spreadsheetCommandBarSubItem2.Enabled = false;
            this.spreadsheetCommandBarSubItem2.Id = 70;
            this.spreadsheetCommandBarSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem8),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem9)});
            this.spreadsheetCommandBarSubItem2.Name = "spreadsheetCommandBarSubItem2";
            // 
            // spreadsheetCommandBarButtonItem8
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem8, "spreadsheetCommandBarButtonItem8");
            this.spreadsheetCommandBarButtonItem8.CommandName = "ClearAllPivotTable";
            this.spreadsheetCommandBarButtonItem8.Enabled = false;
            this.spreadsheetCommandBarButtonItem8.Id = 71;
            this.spreadsheetCommandBarButtonItem8.Name = "spreadsheetCommandBarButtonItem8";
            // 
            // spreadsheetCommandBarButtonItem9
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem9, "spreadsheetCommandBarButtonItem9");
            this.spreadsheetCommandBarButtonItem9.CommandName = "ClearFiltersPivotTable";
            this.spreadsheetCommandBarButtonItem9.Enabled = false;
            this.spreadsheetCommandBarButtonItem9.Id = 72;
            this.spreadsheetCommandBarButtonItem9.Name = "spreadsheetCommandBarButtonItem9";
            // 
            // spreadsheetCommandBarSubItem3
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem3, "spreadsheetCommandBarSubItem3");
            this.spreadsheetCommandBarSubItem3.CommandName = "PivotTableActionsSelectGroup";
            this.spreadsheetCommandBarSubItem3.Enabled = false;
            this.spreadsheetCommandBarSubItem3.Id = 73;
            this.spreadsheetCommandBarSubItem3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem10),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem11),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem12)});
            this.spreadsheetCommandBarSubItem3.Name = "spreadsheetCommandBarSubItem3";
            // 
            // spreadsheetCommandBarButtonItem10
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem10, "spreadsheetCommandBarButtonItem10");
            this.spreadsheetCommandBarButtonItem10.CommandName = "SelectValuesPivotTable";
            this.spreadsheetCommandBarButtonItem10.Enabled = false;
            this.spreadsheetCommandBarButtonItem10.Id = 74;
            this.spreadsheetCommandBarButtonItem10.Name = "spreadsheetCommandBarButtonItem10";
            // 
            // spreadsheetCommandBarButtonItem11
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem11, "spreadsheetCommandBarButtonItem11");
            this.spreadsheetCommandBarButtonItem11.CommandName = "SelectLabelsPivotTable";
            this.spreadsheetCommandBarButtonItem11.Enabled = false;
            this.spreadsheetCommandBarButtonItem11.Id = 75;
            this.spreadsheetCommandBarButtonItem11.Name = "spreadsheetCommandBarButtonItem11";
            // 
            // spreadsheetCommandBarButtonItem12
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem12, "spreadsheetCommandBarButtonItem12");
            this.spreadsheetCommandBarButtonItem12.CommandName = "SelectEntirePivotTable";
            this.spreadsheetCommandBarButtonItem12.Enabled = false;
            this.spreadsheetCommandBarButtonItem12.Id = 76;
            this.spreadsheetCommandBarButtonItem12.Name = "spreadsheetCommandBarButtonItem12";
            // 
            // spreadsheetCommandBarButtonItem13
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem13, "spreadsheetCommandBarButtonItem13");
            this.spreadsheetCommandBarButtonItem13.CommandName = "MovePivotTable";
            this.spreadsheetCommandBarButtonItem13.Enabled = false;
            this.spreadsheetCommandBarButtonItem13.Id = 77;
            this.spreadsheetCommandBarButtonItem13.Name = "spreadsheetCommandBarButtonItem13";
            // 
            // spreadsheetCommandBarCheckItem1
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem1, "spreadsheetCommandBarCheckItem1");
            this.spreadsheetCommandBarCheckItem1.CommandName = "FieldListPanelPivotTable";
            this.spreadsheetCommandBarCheckItem1.Enabled = false;
            this.spreadsheetCommandBarCheckItem1.Id = 78;
            this.spreadsheetCommandBarCheckItem1.Name = "spreadsheetCommandBarCheckItem1";
            // 
            // spreadsheetCommandBarCheckItem2
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem2, "spreadsheetCommandBarCheckItem2");
            this.spreadsheetCommandBarCheckItem2.CommandName = "ShowPivotTableExpandCollapseButtons";
            this.spreadsheetCommandBarCheckItem2.Enabled = false;
            this.spreadsheetCommandBarCheckItem2.Id = 79;
            this.spreadsheetCommandBarCheckItem2.Name = "spreadsheetCommandBarCheckItem2";
            // 
            // spreadsheetCommandBarCheckItem3
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem3, "spreadsheetCommandBarCheckItem3");
            this.spreadsheetCommandBarCheckItem3.CommandName = "ShowPivotTableFieldHeaders";
            this.spreadsheetCommandBarCheckItem3.Enabled = false;
            this.spreadsheetCommandBarCheckItem3.Id = 80;
            this.spreadsheetCommandBarCheckItem3.Name = "spreadsheetCommandBarCheckItem3";
            // 
            // spreadsheetCommandBarSubItem4
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem4, "spreadsheetCommandBarSubItem4");
            this.spreadsheetCommandBarSubItem4.CommandName = "PivotTableLayoutSubtotalsGroup";
            this.spreadsheetCommandBarSubItem4.Enabled = false;
            this.spreadsheetCommandBarSubItem4.Id = 81;
            this.spreadsheetCommandBarSubItem4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem14),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem15),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem16)});
            this.spreadsheetCommandBarSubItem4.Name = "spreadsheetCommandBarSubItem4";
            // 
            // spreadsheetCommandBarButtonItem14
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem14, "spreadsheetCommandBarButtonItem14");
            this.spreadsheetCommandBarButtonItem14.CommandName = "PivotTableDoNotShowSubtotals";
            this.spreadsheetCommandBarButtonItem14.Enabled = false;
            this.spreadsheetCommandBarButtonItem14.Id = 82;
            this.spreadsheetCommandBarButtonItem14.Name = "spreadsheetCommandBarButtonItem14";
            this.spreadsheetCommandBarButtonItem14.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonItem15
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem15, "spreadsheetCommandBarButtonItem15");
            this.spreadsheetCommandBarButtonItem15.CommandName = "PivotTableShowAllSubtotalsAtBottom";
            this.spreadsheetCommandBarButtonItem15.Enabled = false;
            this.spreadsheetCommandBarButtonItem15.Id = 83;
            this.spreadsheetCommandBarButtonItem15.Name = "spreadsheetCommandBarButtonItem15";
            this.spreadsheetCommandBarButtonItem15.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonItem16
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem16, "spreadsheetCommandBarButtonItem16");
            this.spreadsheetCommandBarButtonItem16.CommandName = "PivotTableShowAllSubtotalsAtTop";
            this.spreadsheetCommandBarButtonItem16.Enabled = false;
            this.spreadsheetCommandBarButtonItem16.Id = 84;
            this.spreadsheetCommandBarButtonItem16.Name = "spreadsheetCommandBarButtonItem16";
            this.spreadsheetCommandBarButtonItem16.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarSubItem5
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem5, "spreadsheetCommandBarSubItem5");
            this.spreadsheetCommandBarSubItem5.CommandName = "PivotTableLayoutGrandTotalsGroup";
            this.spreadsheetCommandBarSubItem5.Enabled = false;
            this.spreadsheetCommandBarSubItem5.Id = 85;
            this.spreadsheetCommandBarSubItem5.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem17),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem18),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem19),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem20)});
            this.spreadsheetCommandBarSubItem5.Name = "spreadsheetCommandBarSubItem5";
            // 
            // spreadsheetCommandBarButtonItem17
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem17, "spreadsheetCommandBarButtonItem17");
            this.spreadsheetCommandBarButtonItem17.CommandName = "PivotTableGrandTotalsOffRowsColumns";
            this.spreadsheetCommandBarButtonItem17.Enabled = false;
            this.spreadsheetCommandBarButtonItem17.Id = 86;
            this.spreadsheetCommandBarButtonItem17.Name = "spreadsheetCommandBarButtonItem17";
            this.spreadsheetCommandBarButtonItem17.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonItem18
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem18, "spreadsheetCommandBarButtonItem18");
            this.spreadsheetCommandBarButtonItem18.CommandName = "PivotTableGrandTotalsOnRowsColumns";
            this.spreadsheetCommandBarButtonItem18.Enabled = false;
            this.spreadsheetCommandBarButtonItem18.Id = 87;
            this.spreadsheetCommandBarButtonItem18.Name = "spreadsheetCommandBarButtonItem18";
            this.spreadsheetCommandBarButtonItem18.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonItem19
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem19, "spreadsheetCommandBarButtonItem19");
            this.spreadsheetCommandBarButtonItem19.CommandName = "PivotTableGrandTotalsOnRowsOnly";
            this.spreadsheetCommandBarButtonItem19.Enabled = false;
            this.spreadsheetCommandBarButtonItem19.Id = 88;
            this.spreadsheetCommandBarButtonItem19.Name = "spreadsheetCommandBarButtonItem19";
            this.spreadsheetCommandBarButtonItem19.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonItem20
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem20, "spreadsheetCommandBarButtonItem20");
            this.spreadsheetCommandBarButtonItem20.CommandName = "PivotTableGrandTotalsOnColumnsOnly";
            this.spreadsheetCommandBarButtonItem20.Enabled = false;
            this.spreadsheetCommandBarButtonItem20.Id = 89;
            this.spreadsheetCommandBarButtonItem20.Name = "spreadsheetCommandBarButtonItem20";
            this.spreadsheetCommandBarButtonItem20.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarSubItem6
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem6, "spreadsheetCommandBarSubItem6");
            this.spreadsheetCommandBarSubItem6.CommandName = "PivotTableLayoutReportLayoutGroup";
            this.spreadsheetCommandBarSubItem6.Enabled = false;
            this.spreadsheetCommandBarSubItem6.Id = 90;
            this.spreadsheetCommandBarSubItem6.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem21),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem22),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem23),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem24),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem25)});
            this.spreadsheetCommandBarSubItem6.Name = "spreadsheetCommandBarSubItem6";
            // 
            // spreadsheetCommandBarButtonItem21
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem21, "spreadsheetCommandBarButtonItem21");
            this.spreadsheetCommandBarButtonItem21.CommandName = "PivotTableShowCompactForm";
            this.spreadsheetCommandBarButtonItem21.Enabled = false;
            this.spreadsheetCommandBarButtonItem21.Id = 91;
            this.spreadsheetCommandBarButtonItem21.Name = "spreadsheetCommandBarButtonItem21";
            this.spreadsheetCommandBarButtonItem21.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonItem22
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem22, "spreadsheetCommandBarButtonItem22");
            this.spreadsheetCommandBarButtonItem22.CommandName = "PivotTableShowOutlineForm";
            this.spreadsheetCommandBarButtonItem22.Enabled = false;
            this.spreadsheetCommandBarButtonItem22.Id = 92;
            this.spreadsheetCommandBarButtonItem22.Name = "spreadsheetCommandBarButtonItem22";
            this.spreadsheetCommandBarButtonItem22.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonItem23
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem23, "spreadsheetCommandBarButtonItem23");
            this.spreadsheetCommandBarButtonItem23.CommandName = "PivotTableShowTabularForm";
            this.spreadsheetCommandBarButtonItem23.Enabled = false;
            this.spreadsheetCommandBarButtonItem23.Id = 93;
            this.spreadsheetCommandBarButtonItem23.Name = "spreadsheetCommandBarButtonItem23";
            this.spreadsheetCommandBarButtonItem23.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonItem24
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem24, "spreadsheetCommandBarButtonItem24");
            this.spreadsheetCommandBarButtonItem24.CommandName = "PivotTableRepeatAllItemLabels";
            this.spreadsheetCommandBarButtonItem24.Enabled = false;
            this.spreadsheetCommandBarButtonItem24.Id = 94;
            this.spreadsheetCommandBarButtonItem24.Name = "spreadsheetCommandBarButtonItem24";
            this.spreadsheetCommandBarButtonItem24.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonItem25
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem25, "spreadsheetCommandBarButtonItem25");
            this.spreadsheetCommandBarButtonItem25.CommandName = "PivotTableDoNotRepeatItemLabels";
            this.spreadsheetCommandBarButtonItem25.Enabled = false;
            this.spreadsheetCommandBarButtonItem25.Id = 95;
            this.spreadsheetCommandBarButtonItem25.Name = "spreadsheetCommandBarButtonItem25";
            this.spreadsheetCommandBarButtonItem25.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarSubItem7
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem7, "spreadsheetCommandBarSubItem7");
            this.spreadsheetCommandBarSubItem7.CommandName = "PivotTableLayoutBlankRowsGroup";
            this.spreadsheetCommandBarSubItem7.Enabled = false;
            this.spreadsheetCommandBarSubItem7.Id = 96;
            this.spreadsheetCommandBarSubItem7.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem26),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem27)});
            this.spreadsheetCommandBarSubItem7.Name = "spreadsheetCommandBarSubItem7";
            // 
            // spreadsheetCommandBarButtonItem26
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem26, "spreadsheetCommandBarButtonItem26");
            this.spreadsheetCommandBarButtonItem26.CommandName = "PivotTableInsertBlankLineEachItem";
            this.spreadsheetCommandBarButtonItem26.Enabled = false;
            this.spreadsheetCommandBarButtonItem26.Id = 97;
            this.spreadsheetCommandBarButtonItem26.Name = "spreadsheetCommandBarButtonItem26";
            this.spreadsheetCommandBarButtonItem26.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonItem27
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem27, "spreadsheetCommandBarButtonItem27");
            this.spreadsheetCommandBarButtonItem27.CommandName = "PivotTableRemoveBlankLineEachItem";
            this.spreadsheetCommandBarButtonItem27.Enabled = false;
            this.spreadsheetCommandBarButtonItem27.Id = 98;
            this.spreadsheetCommandBarButtonItem27.Name = "spreadsheetCommandBarButtonItem27";
            this.spreadsheetCommandBarButtonItem27.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarCheckItem4
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem4, "spreadsheetCommandBarCheckItem4");
            this.spreadsheetCommandBarCheckItem4.CheckBoxVisibility = DevExpress.XtraBars.CheckBoxVisibility.BeforeText;
            this.spreadsheetCommandBarCheckItem4.CommandName = "PivotTableToggleRowHeaders";
            this.spreadsheetCommandBarCheckItem4.Enabled = false;
            this.spreadsheetCommandBarCheckItem4.Id = 99;
            this.spreadsheetCommandBarCheckItem4.Name = "spreadsheetCommandBarCheckItem4";
            // 
            // spreadsheetCommandBarCheckItem5
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem5, "spreadsheetCommandBarCheckItem5");
            this.spreadsheetCommandBarCheckItem5.CheckBoxVisibility = DevExpress.XtraBars.CheckBoxVisibility.BeforeText;
            this.spreadsheetCommandBarCheckItem5.CommandName = "PivotTableToggleColumnHeaders";
            this.spreadsheetCommandBarCheckItem5.Enabled = false;
            this.spreadsheetCommandBarCheckItem5.Id = 100;
            this.spreadsheetCommandBarCheckItem5.Name = "spreadsheetCommandBarCheckItem5";
            // 
            // spreadsheetCommandBarCheckItem6
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem6, "spreadsheetCommandBarCheckItem6");
            this.spreadsheetCommandBarCheckItem6.CheckBoxVisibility = DevExpress.XtraBars.CheckBoxVisibility.BeforeText;
            this.spreadsheetCommandBarCheckItem6.CommandName = "PivotTableToggleBandedRows";
            this.spreadsheetCommandBarCheckItem6.Enabled = false;
            this.spreadsheetCommandBarCheckItem6.Id = 101;
            this.spreadsheetCommandBarCheckItem6.Name = "spreadsheetCommandBarCheckItem6";
            // 
            // spreadsheetCommandBarCheckItem7
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem7, "spreadsheetCommandBarCheckItem7");
            this.spreadsheetCommandBarCheckItem7.CheckBoxVisibility = DevExpress.XtraBars.CheckBoxVisibility.BeforeText;
            this.spreadsheetCommandBarCheckItem7.CommandName = "PivotTableToggleBandedColumns";
            this.spreadsheetCommandBarCheckItem7.Enabled = false;
            this.spreadsheetCommandBarCheckItem7.Id = 102;
            this.spreadsheetCommandBarCheckItem7.Name = "spreadsheetCommandBarCheckItem7";
            // 
            // galleryPivotStylesItem1
            // 
            this.galleryPivotStylesItem1.Enabled = false;
            // 
            // 
            // 
            this.galleryPivotStylesItem1.Gallery.ColumnCount = 7;
            this.galleryPivotStylesItem1.Gallery.DrawImageBackground = false;
            this.galleryPivotStylesItem1.Gallery.ImageSize = new System.Drawing.Size(65, 46);
            this.galleryPivotStylesItem1.Gallery.ItemAutoSizeMode = DevExpress.XtraBars.Ribbon.Gallery.GalleryItemAutoSizeMode.None;
            this.galleryPivotStylesItem1.Gallery.ItemSize = new System.Drawing.Size(73, 61);
            this.galleryPivotStylesItem1.Gallery.RowCount = 10;
            this.galleryPivotStylesItem1.Id = 103;
            this.galleryPivotStylesItem1.Name = "galleryPivotStylesItem1";
            // 
            // spreadsheetCommandBarSubItem8
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem8, "spreadsheetCommandBarSubItem8");
            this.spreadsheetCommandBarSubItem8.CommandName = "ArrangeBringForwardCommandGroup";
            this.spreadsheetCommandBarSubItem8.Enabled = false;
            this.spreadsheetCommandBarSubItem8.Id = 104;
            this.spreadsheetCommandBarSubItem8.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem28),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem29)});
            this.spreadsheetCommandBarSubItem8.Name = "spreadsheetCommandBarSubItem8";
            // 
            // spreadsheetCommandBarButtonItem28
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem28, "spreadsheetCommandBarButtonItem28");
            this.spreadsheetCommandBarButtonItem28.CommandName = "ArrangeBringForward";
            this.spreadsheetCommandBarButtonItem28.Enabled = false;
            this.spreadsheetCommandBarButtonItem28.Id = 105;
            this.spreadsheetCommandBarButtonItem28.Name = "spreadsheetCommandBarButtonItem28";
            // 
            // spreadsheetCommandBarButtonItem29
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem29, "spreadsheetCommandBarButtonItem29");
            this.spreadsheetCommandBarButtonItem29.CommandName = "ArrangeBringToFront";
            this.spreadsheetCommandBarButtonItem29.Enabled = false;
            this.spreadsheetCommandBarButtonItem29.Id = 106;
            this.spreadsheetCommandBarButtonItem29.Name = "spreadsheetCommandBarButtonItem29";
            // 
            // spreadsheetCommandBarSubItem9
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem9, "spreadsheetCommandBarSubItem9");
            this.spreadsheetCommandBarSubItem9.CommandName = "ArrangeSendBackwardCommandGroup";
            this.spreadsheetCommandBarSubItem9.Enabled = false;
            this.spreadsheetCommandBarSubItem9.Id = 107;
            this.spreadsheetCommandBarSubItem9.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem30),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem31)});
            this.spreadsheetCommandBarSubItem9.Name = "spreadsheetCommandBarSubItem9";
            // 
            // spreadsheetCommandBarButtonItem30
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem30, "spreadsheetCommandBarButtonItem30");
            this.spreadsheetCommandBarButtonItem30.CommandName = "ArrangeSendBackward";
            this.spreadsheetCommandBarButtonItem30.Enabled = false;
            this.spreadsheetCommandBarButtonItem30.Id = 108;
            this.spreadsheetCommandBarButtonItem30.Name = "spreadsheetCommandBarButtonItem30";
            // 
            // spreadsheetCommandBarButtonItem31
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem31, "spreadsheetCommandBarButtonItem31");
            this.spreadsheetCommandBarButtonItem31.CommandName = "ArrangeSendToBack";
            this.spreadsheetCommandBarButtonItem31.Enabled = false;
            this.spreadsheetCommandBarButtonItem31.Id = 109;
            this.spreadsheetCommandBarButtonItem31.Name = "spreadsheetCommandBarButtonItem31";
            // 
            // barStaticItem1
            // 
            resources.ApplyResources(this.barStaticItem1, "barStaticItem1");
            this.barStaticItem1.Id = 110;
            this.barStaticItem1.Name = "barStaticItem1";
            // 
            // renameTableItem1
            // 
            this.renameTableItem1.Edit = null;
            this.renameTableItem1.Enabled = false;
            this.renameTableItem1.Id = 111;
            this.renameTableItem1.Name = "renameTableItem1";
            // 
            // spreadsheetCommandBarCheckItem8
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem8, "spreadsheetCommandBarCheckItem8");
            this.spreadsheetCommandBarCheckItem8.CommandName = "TableToolsConvertToRange";
            this.spreadsheetCommandBarCheckItem8.Enabled = false;
            this.spreadsheetCommandBarCheckItem8.Id = 112;
            this.spreadsheetCommandBarCheckItem8.Name = "spreadsheetCommandBarCheckItem8";
            this.spreadsheetCommandBarCheckItem8.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // spreadsheetCommandBarCheckItem9
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem9, "spreadsheetCommandBarCheckItem9");
            this.spreadsheetCommandBarCheckItem9.CheckBoxVisibility = DevExpress.XtraBars.CheckBoxVisibility.BeforeText;
            this.spreadsheetCommandBarCheckItem9.CommandName = "TableToolsToggleHeaderRow";
            this.spreadsheetCommandBarCheckItem9.Enabled = false;
            this.spreadsheetCommandBarCheckItem9.Id = 113;
            this.spreadsheetCommandBarCheckItem9.Name = "spreadsheetCommandBarCheckItem9";
            // 
            // spreadsheetCommandBarCheckItem10
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem10, "spreadsheetCommandBarCheckItem10");
            this.spreadsheetCommandBarCheckItem10.CheckBoxVisibility = DevExpress.XtraBars.CheckBoxVisibility.BeforeText;
            this.spreadsheetCommandBarCheckItem10.CommandName = "TableToolsToggleTotalRow";
            this.spreadsheetCommandBarCheckItem10.Enabled = false;
            this.spreadsheetCommandBarCheckItem10.Id = 114;
            this.spreadsheetCommandBarCheckItem10.Name = "spreadsheetCommandBarCheckItem10";
            // 
            // spreadsheetCommandBarCheckItem11
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem11, "spreadsheetCommandBarCheckItem11");
            this.spreadsheetCommandBarCheckItem11.CheckBoxVisibility = DevExpress.XtraBars.CheckBoxVisibility.BeforeText;
            this.spreadsheetCommandBarCheckItem11.CommandName = "TableToolsToggleBandedColumns";
            this.spreadsheetCommandBarCheckItem11.Enabled = false;
            this.spreadsheetCommandBarCheckItem11.Id = 115;
            this.spreadsheetCommandBarCheckItem11.Name = "spreadsheetCommandBarCheckItem11";
            // 
            // spreadsheetCommandBarCheckItem12
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem12, "spreadsheetCommandBarCheckItem12");
            this.spreadsheetCommandBarCheckItem12.CheckBoxVisibility = DevExpress.XtraBars.CheckBoxVisibility.BeforeText;
            this.spreadsheetCommandBarCheckItem12.CommandName = "TableToolsToggleFirstColumn";
            this.spreadsheetCommandBarCheckItem12.Enabled = false;
            this.spreadsheetCommandBarCheckItem12.Id = 116;
            this.spreadsheetCommandBarCheckItem12.Name = "spreadsheetCommandBarCheckItem12";
            // 
            // spreadsheetCommandBarCheckItem13
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem13, "spreadsheetCommandBarCheckItem13");
            this.spreadsheetCommandBarCheckItem13.CheckBoxVisibility = DevExpress.XtraBars.CheckBoxVisibility.BeforeText;
            this.spreadsheetCommandBarCheckItem13.CommandName = "TableToolsToggleLastColumn";
            this.spreadsheetCommandBarCheckItem13.Enabled = false;
            this.spreadsheetCommandBarCheckItem13.Id = 117;
            this.spreadsheetCommandBarCheckItem13.Name = "spreadsheetCommandBarCheckItem13";
            // 
            // spreadsheetCommandBarCheckItem14
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem14, "spreadsheetCommandBarCheckItem14");
            this.spreadsheetCommandBarCheckItem14.CheckBoxVisibility = DevExpress.XtraBars.CheckBoxVisibility.BeforeText;
            this.spreadsheetCommandBarCheckItem14.CommandName = "TableToolsToggleBandedRows";
            this.spreadsheetCommandBarCheckItem14.Enabled = false;
            this.spreadsheetCommandBarCheckItem14.Id = 118;
            this.spreadsheetCommandBarCheckItem14.Name = "spreadsheetCommandBarCheckItem14";
            // 
            // galleryTableStylesItem1
            // 
            this.galleryTableStylesItem1.Enabled = false;
            // 
            // 
            // 
            this.galleryTableStylesItem1.Gallery.ColumnCount = 7;
            this.galleryTableStylesItem1.Gallery.DrawImageBackground = false;
            this.galleryTableStylesItem1.Gallery.ImageSize = new System.Drawing.Size(65, 46);
            this.galleryTableStylesItem1.Gallery.ItemAutoSizeMode = DevExpress.XtraBars.Ribbon.Gallery.GalleryItemAutoSizeMode.None;
            this.galleryTableStylesItem1.Gallery.ItemSize = new System.Drawing.Size(73, 58);
            this.galleryTableStylesItem1.Gallery.RowCount = 10;
            this.galleryTableStylesItem1.Id = 119;
            this.galleryTableStylesItem1.Name = "galleryTableStylesItem1";
            // 
            // spreadsheetCommandBarButtonItem32
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem32, "spreadsheetCommandBarButtonItem32");
            this.spreadsheetCommandBarButtonItem32.CommandName = "ChartChangeType";
            this.spreadsheetCommandBarButtonItem32.Enabled = false;
            this.spreadsheetCommandBarButtonItem32.Id = 120;
            this.spreadsheetCommandBarButtonItem32.Name = "spreadsheetCommandBarButtonItem32";
            // 
            // spreadsheetCommandBarButtonItem33
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem33, "spreadsheetCommandBarButtonItem33");
            this.spreadsheetCommandBarButtonItem33.CommandName = "ChartSwitchRowColumn";
            this.spreadsheetCommandBarButtonItem33.Enabled = false;
            this.spreadsheetCommandBarButtonItem33.Id = 121;
            this.spreadsheetCommandBarButtonItem33.Name = "spreadsheetCommandBarButtonItem33";
            // 
            // spreadsheetCommandBarButtonItem34
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem34, "spreadsheetCommandBarButtonItem34");
            this.spreadsheetCommandBarButtonItem34.CommandName = "ChartSelectData";
            this.spreadsheetCommandBarButtonItem34.Enabled = false;
            this.spreadsheetCommandBarButtonItem34.Id = 122;
            this.spreadsheetCommandBarButtonItem34.Name = "spreadsheetCommandBarButtonItem34";
            // 
            // galleryChartLayoutItem1
            // 
            this.galleryChartLayoutItem1.Enabled = false;
            // 
            // 
            // 
            this.galleryChartLayoutItem1.Gallery.ColumnCount = 6;
            this.galleryChartLayoutItem1.Gallery.DrawImageBackground = false;
            this.galleryChartLayoutItem1.Gallery.ImageSize = new System.Drawing.Size(48, 48);
            this.galleryChartLayoutItem1.Gallery.RowCount = 2;
            this.galleryChartLayoutItem1.Id = 123;
            this.galleryChartLayoutItem1.Name = "galleryChartLayoutItem1";
            // 
            // galleryChartStyleItem1
            // 
            this.galleryChartStyleItem1.Enabled = false;
            // 
            // 
            // 
            this.galleryChartStyleItem1.Gallery.ColumnCount = 8;
            this.galleryChartStyleItem1.Gallery.DrawImageBackground = false;
            this.galleryChartStyleItem1.Gallery.ImageSize = new System.Drawing.Size(65, 46);
            this.galleryChartStyleItem1.Gallery.ItemAutoSizeMode = DevExpress.XtraBars.Ribbon.Gallery.GalleryItemAutoSizeMode.None;
            this.galleryChartStyleItem1.Gallery.ItemSize = new System.Drawing.Size(93, 56);
            this.galleryChartStyleItem1.Gallery.MinimumColumnCount = 4;
            this.galleryChartStyleItem1.Gallery.RowCount = 6;
            this.galleryChartStyleItem1.Id = 124;
            this.galleryChartStyleItem1.Name = "galleryChartStyleItem1";
            // 
            // spreadsheetCommandBarButtonGalleryDropDownItem1
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonGalleryDropDownItem1, "spreadsheetCommandBarButtonGalleryDropDownItem1");
            this.spreadsheetCommandBarButtonGalleryDropDownItem1.CommandName = "ChartTitleCommandGroup";
            this.spreadsheetCommandBarButtonGalleryDropDownItem1.Enabled = false;
            this.spreadsheetCommandBarButtonGalleryDropDownItem1.Id = 125;
            this.spreadsheetCommandBarButtonGalleryDropDownItem1.Name = "spreadsheetCommandBarButtonGalleryDropDownItem1";
            // 
            // spreadsheetCommandBarSubItem10
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem10, "spreadsheetCommandBarSubItem10");
            this.spreadsheetCommandBarSubItem10.CommandName = "ChartAxisTitlesCommandGroup";
            this.spreadsheetCommandBarSubItem10.Enabled = false;
            this.spreadsheetCommandBarSubItem10.Id = 126;
            this.spreadsheetCommandBarSubItem10.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonGalleryDropDownItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonGalleryDropDownItem3)});
            this.spreadsheetCommandBarSubItem10.Name = "spreadsheetCommandBarSubItem10";
            // 
            // spreadsheetCommandBarButtonGalleryDropDownItem2
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonGalleryDropDownItem2, "spreadsheetCommandBarButtonGalleryDropDownItem2");
            this.spreadsheetCommandBarButtonGalleryDropDownItem2.CommandName = "ChartPrimaryHorizontalAxisTitleCommandGroup";
            this.spreadsheetCommandBarButtonGalleryDropDownItem2.Enabled = false;
            this.spreadsheetCommandBarButtonGalleryDropDownItem2.Id = 127;
            this.spreadsheetCommandBarButtonGalleryDropDownItem2.Name = "spreadsheetCommandBarButtonGalleryDropDownItem2";
            // 
            // spreadsheetCommandBarButtonGalleryDropDownItem3
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonGalleryDropDownItem3, "spreadsheetCommandBarButtonGalleryDropDownItem3");
            this.spreadsheetCommandBarButtonGalleryDropDownItem3.CommandName = "ChartPrimaryVerticalAxisTitleCommandGroup";
            this.spreadsheetCommandBarButtonGalleryDropDownItem3.Enabled = false;
            this.spreadsheetCommandBarButtonGalleryDropDownItem3.Id = 128;
            this.spreadsheetCommandBarButtonGalleryDropDownItem3.Name = "spreadsheetCommandBarButtonGalleryDropDownItem3";
            // 
            // spreadsheetCommandBarButtonGalleryDropDownItem4
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonGalleryDropDownItem4, "spreadsheetCommandBarButtonGalleryDropDownItem4");
            this.spreadsheetCommandBarButtonGalleryDropDownItem4.CommandName = "ChartLegendCommandGroup";
            this.spreadsheetCommandBarButtonGalleryDropDownItem4.Enabled = false;
            this.spreadsheetCommandBarButtonGalleryDropDownItem4.Id = 129;
            this.spreadsheetCommandBarButtonGalleryDropDownItem4.Name = "spreadsheetCommandBarButtonGalleryDropDownItem4";
            // 
            // spreadsheetCommandBarButtonGalleryDropDownItem5
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonGalleryDropDownItem5, "spreadsheetCommandBarButtonGalleryDropDownItem5");
            this.spreadsheetCommandBarButtonGalleryDropDownItem5.CommandName = "ChartDataLabelsCommandGroup";
            this.spreadsheetCommandBarButtonGalleryDropDownItem5.Enabled = false;
            this.spreadsheetCommandBarButtonGalleryDropDownItem5.Id = 130;
            this.spreadsheetCommandBarButtonGalleryDropDownItem5.Name = "spreadsheetCommandBarButtonGalleryDropDownItem5";
            // 
            // spreadsheetCommandBarSubItem11
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem11, "spreadsheetCommandBarSubItem11");
            this.spreadsheetCommandBarSubItem11.CommandName = "ChartAxesCommandGroup";
            this.spreadsheetCommandBarSubItem11.Enabled = false;
            this.spreadsheetCommandBarSubItem11.Id = 131;
            this.spreadsheetCommandBarSubItem11.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonGalleryDropDownItem6),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonGalleryDropDownItem7)});
            this.spreadsheetCommandBarSubItem11.Name = "spreadsheetCommandBarSubItem11";
            // 
            // spreadsheetCommandBarButtonGalleryDropDownItem6
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonGalleryDropDownItem6, "spreadsheetCommandBarButtonGalleryDropDownItem6");
            this.spreadsheetCommandBarButtonGalleryDropDownItem6.CommandName = "ChartPrimaryHorizontalAxisCommandGroup";
            this.spreadsheetCommandBarButtonGalleryDropDownItem6.Enabled = false;
            this.spreadsheetCommandBarButtonGalleryDropDownItem6.Id = 132;
            this.spreadsheetCommandBarButtonGalleryDropDownItem6.Name = "spreadsheetCommandBarButtonGalleryDropDownItem6";
            // 
            // spreadsheetCommandBarButtonGalleryDropDownItem7
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonGalleryDropDownItem7, "spreadsheetCommandBarButtonGalleryDropDownItem7");
            this.spreadsheetCommandBarButtonGalleryDropDownItem7.CommandName = "ChartPrimaryVerticalAxisCommandGroup";
            this.spreadsheetCommandBarButtonGalleryDropDownItem7.Enabled = false;
            this.spreadsheetCommandBarButtonGalleryDropDownItem7.Id = 133;
            this.spreadsheetCommandBarButtonGalleryDropDownItem7.Name = "spreadsheetCommandBarButtonGalleryDropDownItem7";
            // 
            // spreadsheetCommandBarSubItem12
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem12, "spreadsheetCommandBarSubItem12");
            this.spreadsheetCommandBarSubItem12.CommandName = "ChartGridlinesCommandGroup";
            this.spreadsheetCommandBarSubItem12.Enabled = false;
            this.spreadsheetCommandBarSubItem12.Id = 134;
            this.spreadsheetCommandBarSubItem12.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonGalleryDropDownItem8),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonGalleryDropDownItem9)});
            this.spreadsheetCommandBarSubItem12.Name = "spreadsheetCommandBarSubItem12";
            // 
            // spreadsheetCommandBarButtonGalleryDropDownItem8
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonGalleryDropDownItem8, "spreadsheetCommandBarButtonGalleryDropDownItem8");
            this.spreadsheetCommandBarButtonGalleryDropDownItem8.CommandName = "ChartPrimaryHorizontalGridlinesCommandGroup";
            this.spreadsheetCommandBarButtonGalleryDropDownItem8.Enabled = false;
            this.spreadsheetCommandBarButtonGalleryDropDownItem8.Id = 135;
            this.spreadsheetCommandBarButtonGalleryDropDownItem8.Name = "spreadsheetCommandBarButtonGalleryDropDownItem8";
            // 
            // spreadsheetCommandBarButtonGalleryDropDownItem9
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonGalleryDropDownItem9, "spreadsheetCommandBarButtonGalleryDropDownItem9");
            this.spreadsheetCommandBarButtonGalleryDropDownItem9.CommandName = "ChartPrimaryVerticalGridlinesCommandGroup";
            this.spreadsheetCommandBarButtonGalleryDropDownItem9.Enabled = false;
            this.spreadsheetCommandBarButtonGalleryDropDownItem9.Id = 136;
            this.spreadsheetCommandBarButtonGalleryDropDownItem9.Name = "spreadsheetCommandBarButtonGalleryDropDownItem9";
            // 
            // spreadsheetCommandBarButtonGalleryDropDownItem10
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonGalleryDropDownItem10, "spreadsheetCommandBarButtonGalleryDropDownItem10");
            this.spreadsheetCommandBarButtonGalleryDropDownItem10.CommandName = "ChartLinesCommandGroup";
            this.spreadsheetCommandBarButtonGalleryDropDownItem10.Enabled = false;
            this.spreadsheetCommandBarButtonGalleryDropDownItem10.Id = 137;
            this.spreadsheetCommandBarButtonGalleryDropDownItem10.Name = "spreadsheetCommandBarButtonGalleryDropDownItem10";
            // 
            // spreadsheetCommandBarButtonGalleryDropDownItem11
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonGalleryDropDownItem11, "spreadsheetCommandBarButtonGalleryDropDownItem11");
            this.spreadsheetCommandBarButtonGalleryDropDownItem11.CommandName = "ChartUpDownBarsCommandGroup";
            this.spreadsheetCommandBarButtonGalleryDropDownItem11.Enabled = false;
            this.spreadsheetCommandBarButtonGalleryDropDownItem11.Id = 138;
            this.spreadsheetCommandBarButtonGalleryDropDownItem11.Name = "spreadsheetCommandBarButtonGalleryDropDownItem11";
            // 
            // spreadsheetCommandBarButtonGalleryDropDownItem12
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonGalleryDropDownItem12, "spreadsheetCommandBarButtonGalleryDropDownItem12");
            this.spreadsheetCommandBarButtonGalleryDropDownItem12.CommandName = "ChartErrorBarsCommandGroup";
            this.spreadsheetCommandBarButtonGalleryDropDownItem12.Enabled = false;
            this.spreadsheetCommandBarButtonGalleryDropDownItem12.Id = 139;
            this.spreadsheetCommandBarButtonGalleryDropDownItem12.Name = "spreadsheetCommandBarButtonGalleryDropDownItem12";
            // 
            // spreadsheetCommandBarCheckItem15
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem15, "spreadsheetCommandBarCheckItem15");
            this.spreadsheetCommandBarCheckItem15.CheckBoxVisibility = DevExpress.XtraBars.CheckBoxVisibility.BeforeText;
            this.spreadsheetCommandBarCheckItem15.CommandName = "ViewShowGridlines";
            this.spreadsheetCommandBarCheckItem15.Enabled = false;
            this.spreadsheetCommandBarCheckItem15.Id = 140;
            this.spreadsheetCommandBarCheckItem15.Name = "spreadsheetCommandBarCheckItem15";
            // 
            // spreadsheetCommandBarCheckItem16
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem16, "spreadsheetCommandBarCheckItem16");
            this.spreadsheetCommandBarCheckItem16.CheckBoxVisibility = DevExpress.XtraBars.CheckBoxVisibility.BeforeText;
            this.spreadsheetCommandBarCheckItem16.CommandName = "ViewShowHeadings";
            this.spreadsheetCommandBarCheckItem16.Enabled = false;
            this.spreadsheetCommandBarCheckItem16.Id = 141;
            this.spreadsheetCommandBarCheckItem16.Name = "spreadsheetCommandBarCheckItem16";
            // 
            // spreadsheetCommandBarButtonItem35
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem35, "spreadsheetCommandBarButtonItem35");
            this.spreadsheetCommandBarButtonItem35.CommandName = "ViewZoomOut";
            this.spreadsheetCommandBarButtonItem35.Enabled = false;
            this.spreadsheetCommandBarButtonItem35.Id = 142;
            this.spreadsheetCommandBarButtonItem35.Name = "spreadsheetCommandBarButtonItem35";
            // 
            // spreadsheetCommandBarButtonItem36
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem36, "spreadsheetCommandBarButtonItem36");
            this.spreadsheetCommandBarButtonItem36.CommandName = "ViewZoomIn";
            this.spreadsheetCommandBarButtonItem36.Enabled = false;
            this.spreadsheetCommandBarButtonItem36.Id = 143;
            this.spreadsheetCommandBarButtonItem36.Name = "spreadsheetCommandBarButtonItem36";
            // 
            // spreadsheetCommandBarButtonItem37
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem37, "spreadsheetCommandBarButtonItem37");
            this.spreadsheetCommandBarButtonItem37.CommandName = "ViewZoom100Percent";
            this.spreadsheetCommandBarButtonItem37.Enabled = false;
            this.spreadsheetCommandBarButtonItem37.Id = 144;
            this.spreadsheetCommandBarButtonItem37.Name = "spreadsheetCommandBarButtonItem37";
            // 
            // spreadsheetCommandBarSubItem13
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem13, "spreadsheetCommandBarSubItem13");
            this.spreadsheetCommandBarSubItem13.CommandName = "ViewFreezePanesCommandGroup";
            this.spreadsheetCommandBarSubItem13.Enabled = false;
            this.spreadsheetCommandBarSubItem13.Id = 145;
            this.spreadsheetCommandBarSubItem13.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem38),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem39),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem40),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem41)});
            this.spreadsheetCommandBarSubItem13.Name = "spreadsheetCommandBarSubItem13";
            // 
            // spreadsheetCommandBarButtonItem38
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem38, "spreadsheetCommandBarButtonItem38");
            this.spreadsheetCommandBarButtonItem38.CommandName = "ViewFreezePanes";
            this.spreadsheetCommandBarButtonItem38.Enabled = false;
            this.spreadsheetCommandBarButtonItem38.Id = 146;
            this.spreadsheetCommandBarButtonItem38.Name = "spreadsheetCommandBarButtonItem38";
            // 
            // spreadsheetCommandBarButtonItem39
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem39, "spreadsheetCommandBarButtonItem39");
            this.spreadsheetCommandBarButtonItem39.CommandName = "ViewUnfreezePanes";
            this.spreadsheetCommandBarButtonItem39.Enabled = false;
            this.spreadsheetCommandBarButtonItem39.Id = 147;
            this.spreadsheetCommandBarButtonItem39.Name = "spreadsheetCommandBarButtonItem39";
            // 
            // spreadsheetCommandBarButtonItem40
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem40, "spreadsheetCommandBarButtonItem40");
            this.spreadsheetCommandBarButtonItem40.CommandName = "ViewFreezeTopRow";
            this.spreadsheetCommandBarButtonItem40.Enabled = false;
            this.spreadsheetCommandBarButtonItem40.Id = 148;
            this.spreadsheetCommandBarButtonItem40.Name = "spreadsheetCommandBarButtonItem40";
            // 
            // spreadsheetCommandBarButtonItem41
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem41, "spreadsheetCommandBarButtonItem41");
            this.spreadsheetCommandBarButtonItem41.CommandName = "ViewFreezeFirstColumn";
            this.spreadsheetCommandBarButtonItem41.Enabled = false;
            this.spreadsheetCommandBarButtonItem41.Id = 149;
            this.spreadsheetCommandBarButtonItem41.Name = "spreadsheetCommandBarButtonItem41";
            // 
            // spreadsheetCommandBarButtonItem42
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem42, "spreadsheetCommandBarButtonItem42");
            this.spreadsheetCommandBarButtonItem42.CommandName = "ReviewInsertComment";
            this.spreadsheetCommandBarButtonItem42.Enabled = false;
            this.spreadsheetCommandBarButtonItem42.Id = 150;
            this.spreadsheetCommandBarButtonItem42.Name = "spreadsheetCommandBarButtonItem42";
            // 
            // spreadsheetCommandBarButtonItem43
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem43, "spreadsheetCommandBarButtonItem43");
            this.spreadsheetCommandBarButtonItem43.CommandName = "ReviewEditComment";
            this.spreadsheetCommandBarButtonItem43.Enabled = false;
            this.spreadsheetCommandBarButtonItem43.Id = 151;
            this.spreadsheetCommandBarButtonItem43.Name = "spreadsheetCommandBarButtonItem43";
            // 
            // spreadsheetCommandBarButtonItem44
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem44, "spreadsheetCommandBarButtonItem44");
            this.spreadsheetCommandBarButtonItem44.CommandName = "ReviewDeleteComment";
            this.spreadsheetCommandBarButtonItem44.Enabled = false;
            this.spreadsheetCommandBarButtonItem44.Id = 152;
            this.spreadsheetCommandBarButtonItem44.Name = "spreadsheetCommandBarButtonItem44";
            // 
            // spreadsheetCommandBarButtonItem45
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem45, "spreadsheetCommandBarButtonItem45");
            this.spreadsheetCommandBarButtonItem45.CommandName = "ReviewShowHideComment";
            this.spreadsheetCommandBarButtonItem45.Enabled = false;
            this.spreadsheetCommandBarButtonItem45.Id = 153;
            this.spreadsheetCommandBarButtonItem45.Name = "spreadsheetCommandBarButtonItem45";
            this.spreadsheetCommandBarButtonItem45.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // spreadsheetCommandBarButtonItem46
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem46, "spreadsheetCommandBarButtonItem46");
            this.spreadsheetCommandBarButtonItem46.CommandName = "ReviewProtectSheet";
            this.spreadsheetCommandBarButtonItem46.Enabled = false;
            this.spreadsheetCommandBarButtonItem46.Id = 154;
            this.spreadsheetCommandBarButtonItem46.Name = "spreadsheetCommandBarButtonItem46";
            // 
            // spreadsheetCommandBarButtonItem47
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem47, "spreadsheetCommandBarButtonItem47");
            this.spreadsheetCommandBarButtonItem47.CommandName = "ReviewUnprotectSheet";
            this.spreadsheetCommandBarButtonItem47.Enabled = false;
            this.spreadsheetCommandBarButtonItem47.Id = 155;
            this.spreadsheetCommandBarButtonItem47.Name = "spreadsheetCommandBarButtonItem47";
            // 
            // spreadsheetCommandBarButtonItem48
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem48, "spreadsheetCommandBarButtonItem48");
            this.spreadsheetCommandBarButtonItem48.CommandName = "ReviewProtectWorkbook";
            this.spreadsheetCommandBarButtonItem48.Enabled = false;
            this.spreadsheetCommandBarButtonItem48.Id = 156;
            this.spreadsheetCommandBarButtonItem48.Name = "spreadsheetCommandBarButtonItem48";
            // 
            // spreadsheetCommandBarButtonItem49
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem49, "spreadsheetCommandBarButtonItem49");
            this.spreadsheetCommandBarButtonItem49.CommandName = "ReviewUnprotectWorkbook";
            this.spreadsheetCommandBarButtonItem49.Enabled = false;
            this.spreadsheetCommandBarButtonItem49.Id = 157;
            this.spreadsheetCommandBarButtonItem49.Name = "spreadsheetCommandBarButtonItem49";
            // 
            // spreadsheetCommandBarButtonItem50
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem50, "spreadsheetCommandBarButtonItem50");
            this.spreadsheetCommandBarButtonItem50.CommandName = "ReviewShowProtectedRangeManager";
            this.spreadsheetCommandBarButtonItem50.Enabled = false;
            this.spreadsheetCommandBarButtonItem50.Id = 158;
            this.spreadsheetCommandBarButtonItem50.Name = "spreadsheetCommandBarButtonItem50";
            // 
            // spreadsheetCommandBarButtonItem51
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem51, "spreadsheetCommandBarButtonItem51");
            this.spreadsheetCommandBarButtonItem51.CommandName = "DataSortAscending";
            this.spreadsheetCommandBarButtonItem51.Enabled = false;
            this.spreadsheetCommandBarButtonItem51.Id = 159;
            this.spreadsheetCommandBarButtonItem51.Name = "spreadsheetCommandBarButtonItem51";
            // 
            // spreadsheetCommandBarButtonItem52
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem52, "spreadsheetCommandBarButtonItem52");
            this.spreadsheetCommandBarButtonItem52.CommandName = "DataSortDescending";
            this.spreadsheetCommandBarButtonItem52.Enabled = false;
            this.spreadsheetCommandBarButtonItem52.Id = 160;
            this.spreadsheetCommandBarButtonItem52.Name = "spreadsheetCommandBarButtonItem52";
            // 
            // spreadsheetCommandBarCheckItem17
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem17, "spreadsheetCommandBarCheckItem17");
            this.spreadsheetCommandBarCheckItem17.CommandName = "DataFilterToggle";
            this.spreadsheetCommandBarCheckItem17.Enabled = false;
            this.spreadsheetCommandBarCheckItem17.Id = 161;
            this.spreadsheetCommandBarCheckItem17.Name = "spreadsheetCommandBarCheckItem17";
            // 
            // spreadsheetCommandBarButtonItem53
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem53, "spreadsheetCommandBarButtonItem53");
            this.spreadsheetCommandBarButtonItem53.CommandName = "DataFilterClear";
            this.spreadsheetCommandBarButtonItem53.Enabled = false;
            this.spreadsheetCommandBarButtonItem53.Id = 162;
            this.spreadsheetCommandBarButtonItem53.Name = "spreadsheetCommandBarButtonItem53";
            // 
            // spreadsheetCommandBarButtonItem54
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem54, "spreadsheetCommandBarButtonItem54");
            this.spreadsheetCommandBarButtonItem54.CommandName = "DataFilterReApply";
            this.spreadsheetCommandBarButtonItem54.Enabled = false;
            this.spreadsheetCommandBarButtonItem54.Id = 163;
            this.spreadsheetCommandBarButtonItem54.Name = "spreadsheetCommandBarButtonItem54";
            // 
            // spreadsheetCommandBarSubItem14
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem14, "spreadsheetCommandBarSubItem14");
            this.spreadsheetCommandBarSubItem14.CommandName = "DataToolsDataValidationCommandGroup";
            this.spreadsheetCommandBarSubItem14.Enabled = false;
            this.spreadsheetCommandBarSubItem14.Id = 164;
            this.spreadsheetCommandBarSubItem14.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem55),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem56),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem57)});
            this.spreadsheetCommandBarSubItem14.Name = "spreadsheetCommandBarSubItem14";
            // 
            // spreadsheetCommandBarButtonItem55
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem55, "spreadsheetCommandBarButtonItem55");
            this.spreadsheetCommandBarButtonItem55.CommandName = "DataToolsDataValidation";
            this.spreadsheetCommandBarButtonItem55.Enabled = false;
            this.spreadsheetCommandBarButtonItem55.Id = 165;
            this.spreadsheetCommandBarButtonItem55.Name = "spreadsheetCommandBarButtonItem55";
            // 
            // spreadsheetCommandBarButtonItem56
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem56, "spreadsheetCommandBarButtonItem56");
            this.spreadsheetCommandBarButtonItem56.CommandName = "DataToolsCircleInvalidData";
            this.spreadsheetCommandBarButtonItem56.Enabled = false;
            this.spreadsheetCommandBarButtonItem56.Id = 166;
            this.spreadsheetCommandBarButtonItem56.Name = "spreadsheetCommandBarButtonItem56";
            // 
            // spreadsheetCommandBarButtonItem57
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem57, "spreadsheetCommandBarButtonItem57");
            this.spreadsheetCommandBarButtonItem57.CommandName = "DataToolsClearValidationCircles";
            this.spreadsheetCommandBarButtonItem57.Enabled = false;
            this.spreadsheetCommandBarButtonItem57.Id = 167;
            this.spreadsheetCommandBarButtonItem57.Name = "spreadsheetCommandBarButtonItem57";
            // 
            // spreadsheetCommandBarSubItem15
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem15, "spreadsheetCommandBarSubItem15");
            this.spreadsheetCommandBarSubItem15.CommandName = "OutlineGroupCommandGroup";
            this.spreadsheetCommandBarSubItem15.Enabled = false;
            this.spreadsheetCommandBarSubItem15.Id = 168;
            this.spreadsheetCommandBarSubItem15.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem58),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem59)});
            this.spreadsheetCommandBarSubItem15.Name = "spreadsheetCommandBarSubItem15";
            this.spreadsheetCommandBarSubItem15.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonItem58
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem58, "spreadsheetCommandBarButtonItem58");
            this.spreadsheetCommandBarButtonItem58.CommandName = "GroupOutline";
            this.spreadsheetCommandBarButtonItem58.Enabled = false;
            this.spreadsheetCommandBarButtonItem58.Id = 169;
            this.spreadsheetCommandBarButtonItem58.Name = "spreadsheetCommandBarButtonItem58";
            // 
            // spreadsheetCommandBarButtonItem59
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem59, "spreadsheetCommandBarButtonItem59");
            this.spreadsheetCommandBarButtonItem59.CommandName = "AutoOutline";
            this.spreadsheetCommandBarButtonItem59.Enabled = false;
            this.spreadsheetCommandBarButtonItem59.Id = 170;
            this.spreadsheetCommandBarButtonItem59.Name = "spreadsheetCommandBarButtonItem59";
            // 
            // spreadsheetCommandBarSubItem16
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem16, "spreadsheetCommandBarSubItem16");
            this.spreadsheetCommandBarSubItem16.CommandName = "OutlineUngroupCommandGroup";
            this.spreadsheetCommandBarSubItem16.Enabled = false;
            this.spreadsheetCommandBarSubItem16.Id = 171;
            this.spreadsheetCommandBarSubItem16.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem60),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem61)});
            this.spreadsheetCommandBarSubItem16.Name = "spreadsheetCommandBarSubItem16";
            this.spreadsheetCommandBarSubItem16.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonItem60
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem60, "spreadsheetCommandBarButtonItem60");
            this.spreadsheetCommandBarButtonItem60.CommandName = "UngroupOutline";
            this.spreadsheetCommandBarButtonItem60.Enabled = false;
            this.spreadsheetCommandBarButtonItem60.Id = 172;
            this.spreadsheetCommandBarButtonItem60.Name = "spreadsheetCommandBarButtonItem60";
            // 
            // spreadsheetCommandBarButtonItem61
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem61, "spreadsheetCommandBarButtonItem61");
            this.spreadsheetCommandBarButtonItem61.CommandName = "ClearOutline";
            this.spreadsheetCommandBarButtonItem61.Enabled = false;
            this.spreadsheetCommandBarButtonItem61.Id = 173;
            this.spreadsheetCommandBarButtonItem61.Name = "spreadsheetCommandBarButtonItem61";
            // 
            // spreadsheetCommandBarButtonItem62
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem62, "spreadsheetCommandBarButtonItem62");
            this.spreadsheetCommandBarButtonItem62.CommandName = "Subtotal";
            this.spreadsheetCommandBarButtonItem62.Enabled = false;
            this.spreadsheetCommandBarButtonItem62.Id = 174;
            this.spreadsheetCommandBarButtonItem62.Name = "spreadsheetCommandBarButtonItem62";
            this.spreadsheetCommandBarButtonItem62.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonItem63
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem63, "spreadsheetCommandBarButtonItem63");
            this.spreadsheetCommandBarButtonItem63.CommandName = "ShowDetail";
            this.spreadsheetCommandBarButtonItem63.Enabled = false;
            this.spreadsheetCommandBarButtonItem63.Id = 175;
            this.spreadsheetCommandBarButtonItem63.Name = "spreadsheetCommandBarButtonItem63";
            this.spreadsheetCommandBarButtonItem63.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // spreadsheetCommandBarButtonItem64
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem64, "spreadsheetCommandBarButtonItem64");
            this.spreadsheetCommandBarButtonItem64.CommandName = "HideDetail";
            this.spreadsheetCommandBarButtonItem64.Enabled = false;
            this.spreadsheetCommandBarButtonItem64.Id = 176;
            this.spreadsheetCommandBarButtonItem64.Name = "spreadsheetCommandBarButtonItem64";
            this.spreadsheetCommandBarButtonItem64.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // spreadsheetCommandBarSubItem17
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem17, "spreadsheetCommandBarSubItem17");
            this.spreadsheetCommandBarSubItem17.CommandName = "FunctionsAutoSumCommandGroup";
            this.spreadsheetCommandBarSubItem17.Enabled = false;
            this.spreadsheetCommandBarSubItem17.Id = 177;
            this.spreadsheetCommandBarSubItem17.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem65),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem66),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem67),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem68),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem69)});
            this.spreadsheetCommandBarSubItem17.Name = "spreadsheetCommandBarSubItem17";
            // 
            // spreadsheetCommandBarButtonItem65
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem65, "spreadsheetCommandBarButtonItem65");
            this.spreadsheetCommandBarButtonItem65.CommandName = "FunctionsInsertSum";
            this.spreadsheetCommandBarButtonItem65.Enabled = false;
            this.spreadsheetCommandBarButtonItem65.Id = 178;
            this.spreadsheetCommandBarButtonItem65.Name = "spreadsheetCommandBarButtonItem65";
            // 
            // spreadsheetCommandBarButtonItem66
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem66, "spreadsheetCommandBarButtonItem66");
            this.spreadsheetCommandBarButtonItem66.CommandName = "FunctionsInsertAverage";
            this.spreadsheetCommandBarButtonItem66.Enabled = false;
            this.spreadsheetCommandBarButtonItem66.Id = 179;
            this.spreadsheetCommandBarButtonItem66.Name = "spreadsheetCommandBarButtonItem66";
            // 
            // spreadsheetCommandBarButtonItem67
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem67, "spreadsheetCommandBarButtonItem67");
            this.spreadsheetCommandBarButtonItem67.CommandName = "FunctionsInsertCountNumbers";
            this.spreadsheetCommandBarButtonItem67.Enabled = false;
            this.spreadsheetCommandBarButtonItem67.Id = 180;
            this.spreadsheetCommandBarButtonItem67.Name = "spreadsheetCommandBarButtonItem67";
            // 
            // spreadsheetCommandBarButtonItem68
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem68, "spreadsheetCommandBarButtonItem68");
            this.spreadsheetCommandBarButtonItem68.CommandName = "FunctionsInsertMax";
            this.spreadsheetCommandBarButtonItem68.Enabled = false;
            this.spreadsheetCommandBarButtonItem68.Id = 181;
            this.spreadsheetCommandBarButtonItem68.Name = "spreadsheetCommandBarButtonItem68";
            // 
            // spreadsheetCommandBarButtonItem69
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem69, "spreadsheetCommandBarButtonItem69");
            this.spreadsheetCommandBarButtonItem69.CommandName = "FunctionsInsertMin";
            this.spreadsheetCommandBarButtonItem69.Enabled = false;
            this.spreadsheetCommandBarButtonItem69.Id = 182;
            this.spreadsheetCommandBarButtonItem69.Name = "spreadsheetCommandBarButtonItem69";
            // 
            // functionsFinancialItem1
            // 
            resources.ApplyResources(this.functionsFinancialItem1, "functionsFinancialItem1");
            this.functionsFinancialItem1.Enabled = false;
            this.functionsFinancialItem1.Id = 183;
            this.functionsFinancialItem1.Name = "functionsFinancialItem1";
            // 
            // functionsLogicalItem1
            // 
            resources.ApplyResources(this.functionsLogicalItem1, "functionsLogicalItem1");
            this.functionsLogicalItem1.Enabled = false;
            this.functionsLogicalItem1.Id = 184;
            this.functionsLogicalItem1.Name = "functionsLogicalItem1";
            // 
            // functionsTextItem1
            // 
            resources.ApplyResources(this.functionsTextItem1, "functionsTextItem1");
            this.functionsTextItem1.Enabled = false;
            this.functionsTextItem1.Id = 185;
            this.functionsTextItem1.Name = "functionsTextItem1";
            // 
            // functionsDateAndTimeItem1
            // 
            resources.ApplyResources(this.functionsDateAndTimeItem1, "functionsDateAndTimeItem1");
            this.functionsDateAndTimeItem1.Enabled = false;
            this.functionsDateAndTimeItem1.Id = 186;
            this.functionsDateAndTimeItem1.Name = "functionsDateAndTimeItem1";
            // 
            // functionsLookupAndReferenceItem1
            // 
            resources.ApplyResources(this.functionsLookupAndReferenceItem1, "functionsLookupAndReferenceItem1");
            this.functionsLookupAndReferenceItem1.Enabled = false;
            this.functionsLookupAndReferenceItem1.Id = 187;
            this.functionsLookupAndReferenceItem1.Name = "functionsLookupAndReferenceItem1";
            // 
            // functionsMathAndTrigonometryItem1
            // 
            resources.ApplyResources(this.functionsMathAndTrigonometryItem1, "functionsMathAndTrigonometryItem1");
            this.functionsMathAndTrigonometryItem1.Enabled = false;
            this.functionsMathAndTrigonometryItem1.Id = 188;
            this.functionsMathAndTrigonometryItem1.Name = "functionsMathAndTrigonometryItem1";
            // 
            // spreadsheetCommandBarSubItem18
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem18, "spreadsheetCommandBarSubItem18");
            this.spreadsheetCommandBarSubItem18.CommandName = "FunctionsMoreCommandGroup";
            this.spreadsheetCommandBarSubItem18.Enabled = false;
            this.spreadsheetCommandBarSubItem18.Id = 189;
            this.spreadsheetCommandBarSubItem18.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.functionsStatisticalItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.functionsEngineeringItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.functionsInformationItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.functionsCompatibilityItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.functionsWebItem1)});
            this.spreadsheetCommandBarSubItem18.Name = "spreadsheetCommandBarSubItem18";
            // 
            // functionsStatisticalItem1
            // 
            resources.ApplyResources(this.functionsStatisticalItem1, "functionsStatisticalItem1");
            this.functionsStatisticalItem1.Enabled = false;
            this.functionsStatisticalItem1.Id = 190;
            this.functionsStatisticalItem1.Name = "functionsStatisticalItem1";
            // 
            // functionsEngineeringItem1
            // 
            resources.ApplyResources(this.functionsEngineeringItem1, "functionsEngineeringItem1");
            this.functionsEngineeringItem1.Enabled = false;
            this.functionsEngineeringItem1.Id = 191;
            this.functionsEngineeringItem1.Name = "functionsEngineeringItem1";
            // 
            // functionsInformationItem1
            // 
            resources.ApplyResources(this.functionsInformationItem1, "functionsInformationItem1");
            this.functionsInformationItem1.Enabled = false;
            this.functionsInformationItem1.Id = 192;
            this.functionsInformationItem1.Name = "functionsInformationItem1";
            // 
            // functionsCompatibilityItem1
            // 
            resources.ApplyResources(this.functionsCompatibilityItem1, "functionsCompatibilityItem1");
            this.functionsCompatibilityItem1.Enabled = false;
            this.functionsCompatibilityItem1.Id = 193;
            this.functionsCompatibilityItem1.Name = "functionsCompatibilityItem1";
            // 
            // functionsWebItem1
            // 
            resources.ApplyResources(this.functionsWebItem1, "functionsWebItem1");
            this.functionsWebItem1.Enabled = false;
            this.functionsWebItem1.Id = 194;
            this.functionsWebItem1.Name = "functionsWebItem1";
            // 
            // spreadsheetCommandBarButtonItem70
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem70, "spreadsheetCommandBarButtonItem70");
            this.spreadsheetCommandBarButtonItem70.CommandName = "FormulasShowNameManager";
            this.spreadsheetCommandBarButtonItem70.Enabled = false;
            this.spreadsheetCommandBarButtonItem70.Id = 195;
            this.spreadsheetCommandBarButtonItem70.Name = "spreadsheetCommandBarButtonItem70";
            // 
            // spreadsheetCommandBarButtonItem71
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem71, "spreadsheetCommandBarButtonItem71");
            this.spreadsheetCommandBarButtonItem71.CommandName = "FormulasDefineNameCommand";
            this.spreadsheetCommandBarButtonItem71.Enabled = false;
            this.spreadsheetCommandBarButtonItem71.Id = 196;
            this.spreadsheetCommandBarButtonItem71.Name = "spreadsheetCommandBarButtonItem71";
            this.spreadsheetCommandBarButtonItem71.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // definedNameListItem1
            // 
            resources.ApplyResources(this.definedNameListItem1, "definedNameListItem1");
            this.definedNameListItem1.Enabled = false;
            this.definedNameListItem1.Id = 197;
            this.definedNameListItem1.Name = "definedNameListItem1";
            this.definedNameListItem1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // spreadsheetCommandBarButtonItem72
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem72, "spreadsheetCommandBarButtonItem72");
            this.spreadsheetCommandBarButtonItem72.CommandName = "FormulasCreateDefinedNamesFromSelection";
            this.spreadsheetCommandBarButtonItem72.Enabled = false;
            this.spreadsheetCommandBarButtonItem72.Id = 198;
            this.spreadsheetCommandBarButtonItem72.Name = "spreadsheetCommandBarButtonItem72";
            this.spreadsheetCommandBarButtonItem72.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // spreadsheetCommandBarCheckItem18
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem18, "spreadsheetCommandBarCheckItem18");
            this.spreadsheetCommandBarCheckItem18.CommandName = "ViewShowFormulas";
            this.spreadsheetCommandBarCheckItem18.Enabled = false;
            this.spreadsheetCommandBarCheckItem18.Id = 199;
            this.spreadsheetCommandBarCheckItem18.Name = "spreadsheetCommandBarCheckItem18";
            // 
            // spreadsheetCommandBarSubItem19
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem19, "spreadsheetCommandBarSubItem19");
            this.spreadsheetCommandBarSubItem19.CommandName = "FormulasCalculationOptionsCommandGroup";
            this.spreadsheetCommandBarSubItem19.Enabled = false;
            this.spreadsheetCommandBarSubItem19.Id = 200;
            this.spreadsheetCommandBarSubItem19.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarCheckItem19),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarCheckItem20)});
            this.spreadsheetCommandBarSubItem19.Name = "spreadsheetCommandBarSubItem19";
            // 
            // spreadsheetCommandBarCheckItem19
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem19, "spreadsheetCommandBarCheckItem19");
            this.spreadsheetCommandBarCheckItem19.CommandName = "FormulasCalculationModeAutomatic";
            this.spreadsheetCommandBarCheckItem19.Enabled = false;
            this.spreadsheetCommandBarCheckItem19.Id = 201;
            this.spreadsheetCommandBarCheckItem19.Name = "spreadsheetCommandBarCheckItem19";
            // 
            // spreadsheetCommandBarCheckItem20
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem20, "spreadsheetCommandBarCheckItem20");
            this.spreadsheetCommandBarCheckItem20.CommandName = "FormulasCalculationModeManual";
            this.spreadsheetCommandBarCheckItem20.Enabled = false;
            this.spreadsheetCommandBarCheckItem20.Id = 202;
            this.spreadsheetCommandBarCheckItem20.Name = "spreadsheetCommandBarCheckItem20";
            // 
            // spreadsheetCommandBarButtonItem73
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem73, "spreadsheetCommandBarButtonItem73");
            this.spreadsheetCommandBarButtonItem73.CommandName = "FormulasCalculateNow";
            this.spreadsheetCommandBarButtonItem73.Enabled = false;
            this.spreadsheetCommandBarButtonItem73.Id = 203;
            this.spreadsheetCommandBarButtonItem73.Name = "spreadsheetCommandBarButtonItem73";
            this.spreadsheetCommandBarButtonItem73.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // spreadsheetCommandBarButtonItem74
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem74, "spreadsheetCommandBarButtonItem74");
            this.spreadsheetCommandBarButtonItem74.CommandName = "FormulasCalculateSheet";
            this.spreadsheetCommandBarButtonItem74.Enabled = false;
            this.spreadsheetCommandBarButtonItem74.Id = 204;
            this.spreadsheetCommandBarButtonItem74.Name = "spreadsheetCommandBarButtonItem74";
            this.spreadsheetCommandBarButtonItem74.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // spreadsheetCommandBarSubItem20
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem20, "spreadsheetCommandBarSubItem20");
            this.spreadsheetCommandBarSubItem20.CommandName = "PageSetupMarginsCommandGroup";
            this.spreadsheetCommandBarSubItem20.Enabled = false;
            this.spreadsheetCommandBarSubItem20.Id = 205;
            this.spreadsheetCommandBarSubItem20.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarCheckItem21),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarCheckItem22),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarCheckItem23),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem75)});
            this.spreadsheetCommandBarSubItem20.Name = "spreadsheetCommandBarSubItem20";
            // 
            // spreadsheetCommandBarCheckItem21
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem21, "spreadsheetCommandBarCheckItem21");
            this.spreadsheetCommandBarCheckItem21.CaptionDependOnUnits = true;
            this.spreadsheetCommandBarCheckItem21.CommandName = "PageSetupMarginsNormal";
            this.spreadsheetCommandBarCheckItem21.Enabled = false;
            this.spreadsheetCommandBarCheckItem21.Id = 206;
            this.spreadsheetCommandBarCheckItem21.Name = "spreadsheetCommandBarCheckItem21";
            // 
            // spreadsheetCommandBarCheckItem22
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem22, "spreadsheetCommandBarCheckItem22");
            this.spreadsheetCommandBarCheckItem22.CaptionDependOnUnits = true;
            this.spreadsheetCommandBarCheckItem22.CommandName = "PageSetupMarginsWide";
            this.spreadsheetCommandBarCheckItem22.Enabled = false;
            this.spreadsheetCommandBarCheckItem22.Id = 207;
            this.spreadsheetCommandBarCheckItem22.Name = "spreadsheetCommandBarCheckItem22";
            // 
            // spreadsheetCommandBarCheckItem23
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem23, "spreadsheetCommandBarCheckItem23");
            this.spreadsheetCommandBarCheckItem23.CaptionDependOnUnits = true;
            this.spreadsheetCommandBarCheckItem23.CommandName = "PageSetupMarginsNarrow";
            this.spreadsheetCommandBarCheckItem23.Enabled = false;
            this.spreadsheetCommandBarCheckItem23.Id = 208;
            this.spreadsheetCommandBarCheckItem23.Name = "spreadsheetCommandBarCheckItem23";
            // 
            // spreadsheetCommandBarButtonItem75
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem75, "spreadsheetCommandBarButtonItem75");
            this.spreadsheetCommandBarButtonItem75.CommandName = "PageSetupCustomMargins";
            this.spreadsheetCommandBarButtonItem75.Enabled = false;
            this.spreadsheetCommandBarButtonItem75.Id = 209;
            this.spreadsheetCommandBarButtonItem75.Name = "spreadsheetCommandBarButtonItem75";
            // 
            // spreadsheetCommandBarSubItem21
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem21, "spreadsheetCommandBarSubItem21");
            this.spreadsheetCommandBarSubItem21.CommandName = "PageSetupOrientationCommandGroup";
            this.spreadsheetCommandBarSubItem21.Enabled = false;
            this.spreadsheetCommandBarSubItem21.Id = 210;
            this.spreadsheetCommandBarSubItem21.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarCheckItem24),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarCheckItem25)});
            this.spreadsheetCommandBarSubItem21.Name = "spreadsheetCommandBarSubItem21";
            // 
            // spreadsheetCommandBarCheckItem24
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem24, "spreadsheetCommandBarCheckItem24");
            this.spreadsheetCommandBarCheckItem24.CommandName = "PageSetupOrientationPortrait";
            this.spreadsheetCommandBarCheckItem24.Enabled = false;
            this.spreadsheetCommandBarCheckItem24.Id = 211;
            this.spreadsheetCommandBarCheckItem24.Name = "spreadsheetCommandBarCheckItem24";
            // 
            // spreadsheetCommandBarCheckItem25
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem25, "spreadsheetCommandBarCheckItem25");
            this.spreadsheetCommandBarCheckItem25.CommandName = "PageSetupOrientationLandscape";
            this.spreadsheetCommandBarCheckItem25.Enabled = false;
            this.spreadsheetCommandBarCheckItem25.Id = 212;
            this.spreadsheetCommandBarCheckItem25.Name = "spreadsheetCommandBarCheckItem25";
            // 
            // pageSetupPaperKindItem1
            // 
            resources.ApplyResources(this.pageSetupPaperKindItem1, "pageSetupPaperKindItem1");
            this.pageSetupPaperKindItem1.Enabled = false;
            this.pageSetupPaperKindItem1.Id = 213;
            this.pageSetupPaperKindItem1.Name = "pageSetupPaperKindItem1";
            // 
            // spreadsheetCommandBarSubItem22
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem22, "spreadsheetCommandBarSubItem22");
            this.spreadsheetCommandBarSubItem22.CommandName = "PageSetupPrintAreaCommandGroup";
            this.spreadsheetCommandBarSubItem22.Enabled = false;
            this.spreadsheetCommandBarSubItem22.Id = 214;
            this.spreadsheetCommandBarSubItem22.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem76),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem77),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem78)});
            this.spreadsheetCommandBarSubItem22.Name = "spreadsheetCommandBarSubItem22";
            // 
            // spreadsheetCommandBarButtonItem76
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem76, "spreadsheetCommandBarButtonItem76");
            this.spreadsheetCommandBarButtonItem76.CommandName = "PageSetupSetPrintArea";
            this.spreadsheetCommandBarButtonItem76.Enabled = false;
            this.spreadsheetCommandBarButtonItem76.Id = 215;
            this.spreadsheetCommandBarButtonItem76.Name = "spreadsheetCommandBarButtonItem76";
            // 
            // spreadsheetCommandBarButtonItem77
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem77, "spreadsheetCommandBarButtonItem77");
            this.spreadsheetCommandBarButtonItem77.CommandName = "PageSetupClearPrintArea";
            this.spreadsheetCommandBarButtonItem77.Enabled = false;
            this.spreadsheetCommandBarButtonItem77.Id = 216;
            this.spreadsheetCommandBarButtonItem77.Name = "spreadsheetCommandBarButtonItem77";
            // 
            // spreadsheetCommandBarButtonItem78
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem78, "spreadsheetCommandBarButtonItem78");
            this.spreadsheetCommandBarButtonItem78.CommandName = "PageSetupAddPrintArea";
            this.spreadsheetCommandBarButtonItem78.Enabled = false;
            this.spreadsheetCommandBarButtonItem78.Id = 217;
            this.spreadsheetCommandBarButtonItem78.Name = "spreadsheetCommandBarButtonItem78";
            // 
            // spreadsheetCommandBarCheckItem26
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem26, "spreadsheetCommandBarCheckItem26");
            this.spreadsheetCommandBarCheckItem26.CheckBoxVisibility = DevExpress.XtraBars.CheckBoxVisibility.BeforeText;
            this.spreadsheetCommandBarCheckItem26.CommandName = "PageSetupPrintGridlines";
            this.spreadsheetCommandBarCheckItem26.Enabled = false;
            this.spreadsheetCommandBarCheckItem26.Id = 218;
            this.spreadsheetCommandBarCheckItem26.Name = "spreadsheetCommandBarCheckItem26";
            // 
            // spreadsheetCommandBarCheckItem27
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem27, "spreadsheetCommandBarCheckItem27");
            this.spreadsheetCommandBarCheckItem27.CheckBoxVisibility = DevExpress.XtraBars.CheckBoxVisibility.BeforeText;
            this.spreadsheetCommandBarCheckItem27.CommandName = "PageSetupPrintHeadings";
            this.spreadsheetCommandBarCheckItem27.Enabled = false;
            this.spreadsheetCommandBarCheckItem27.Id = 219;
            this.spreadsheetCommandBarCheckItem27.Name = "spreadsheetCommandBarCheckItem27";
            // 
            // spreadsheetCommandBarButtonItem79
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem79, "spreadsheetCommandBarButtonItem79");
            this.spreadsheetCommandBarButtonItem79.CommandName = "InsertPivotTable";
            this.spreadsheetCommandBarButtonItem79.Enabled = false;
            this.spreadsheetCommandBarButtonItem79.Id = 220;
            this.spreadsheetCommandBarButtonItem79.Name = "spreadsheetCommandBarButtonItem79";
            // 
            // spreadsheetCommandBarButtonItem80
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem80, "spreadsheetCommandBarButtonItem80");
            this.spreadsheetCommandBarButtonItem80.CommandName = "InsertTable";
            this.spreadsheetCommandBarButtonItem80.Enabled = false;
            this.spreadsheetCommandBarButtonItem80.Id = 221;
            this.spreadsheetCommandBarButtonItem80.Name = "spreadsheetCommandBarButtonItem80";
            // 
            // spreadsheetCommandBarButtonItem81
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem81, "spreadsheetCommandBarButtonItem81");
            this.spreadsheetCommandBarButtonItem81.CommandName = "InsertPicture";
            this.spreadsheetCommandBarButtonItem81.Enabled = false;
            this.spreadsheetCommandBarButtonItem81.Id = 222;
            this.spreadsheetCommandBarButtonItem81.Name = "spreadsheetCommandBarButtonItem81";
            // 
            // spreadsheetCommandBarButtonGalleryDropDownItem13
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonGalleryDropDownItem13, "spreadsheetCommandBarButtonGalleryDropDownItem13");
            this.spreadsheetCommandBarButtonGalleryDropDownItem13.CommandName = "InsertChartColumnCommandGroup";
            this.spreadsheetCommandBarButtonGalleryDropDownItem13.Enabled = false;
            this.spreadsheetCommandBarButtonGalleryDropDownItem13.Id = 223;
            this.spreadsheetCommandBarButtonGalleryDropDownItem13.Name = "spreadsheetCommandBarButtonGalleryDropDownItem13";
            // 
            // spreadsheetCommandBarButtonGalleryDropDownItem14
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonGalleryDropDownItem14, "spreadsheetCommandBarButtonGalleryDropDownItem14");
            this.spreadsheetCommandBarButtonGalleryDropDownItem14.CommandName = "InsertChartLineCommandGroup";
            this.spreadsheetCommandBarButtonGalleryDropDownItem14.Enabled = false;
            this.spreadsheetCommandBarButtonGalleryDropDownItem14.Id = 224;
            this.spreadsheetCommandBarButtonGalleryDropDownItem14.Name = "spreadsheetCommandBarButtonGalleryDropDownItem14";
            // 
            // spreadsheetCommandBarButtonGalleryDropDownItem15
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonGalleryDropDownItem15, "spreadsheetCommandBarButtonGalleryDropDownItem15");
            this.spreadsheetCommandBarButtonGalleryDropDownItem15.CommandName = "InsertChartPieCommandGroup";
            this.spreadsheetCommandBarButtonGalleryDropDownItem15.Enabled = false;
            this.spreadsheetCommandBarButtonGalleryDropDownItem15.Id = 225;
            this.spreadsheetCommandBarButtonGalleryDropDownItem15.Name = "spreadsheetCommandBarButtonGalleryDropDownItem15";
            // 
            // spreadsheetCommandBarButtonGalleryDropDownItem16
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonGalleryDropDownItem16, "spreadsheetCommandBarButtonGalleryDropDownItem16");
            this.spreadsheetCommandBarButtonGalleryDropDownItem16.CommandName = "InsertChartBarCommandGroup";
            this.spreadsheetCommandBarButtonGalleryDropDownItem16.Enabled = false;
            this.spreadsheetCommandBarButtonGalleryDropDownItem16.Id = 226;
            this.spreadsheetCommandBarButtonGalleryDropDownItem16.Name = "spreadsheetCommandBarButtonGalleryDropDownItem16";
            // 
            // spreadsheetCommandBarButtonGalleryDropDownItem17
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonGalleryDropDownItem17, "spreadsheetCommandBarButtonGalleryDropDownItem17");
            this.spreadsheetCommandBarButtonGalleryDropDownItem17.CommandName = "InsertChartAreaCommandGroup";
            this.spreadsheetCommandBarButtonGalleryDropDownItem17.Enabled = false;
            this.spreadsheetCommandBarButtonGalleryDropDownItem17.Id = 227;
            this.spreadsheetCommandBarButtonGalleryDropDownItem17.Name = "spreadsheetCommandBarButtonGalleryDropDownItem17";
            // 
            // spreadsheetCommandBarButtonGalleryDropDownItem18
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonGalleryDropDownItem18, "spreadsheetCommandBarButtonGalleryDropDownItem18");
            this.spreadsheetCommandBarButtonGalleryDropDownItem18.CommandName = "InsertChartScatterCommandGroup";
            this.spreadsheetCommandBarButtonGalleryDropDownItem18.Enabled = false;
            this.spreadsheetCommandBarButtonGalleryDropDownItem18.Id = 228;
            this.spreadsheetCommandBarButtonGalleryDropDownItem18.Name = "spreadsheetCommandBarButtonGalleryDropDownItem18";
            // 
            // spreadsheetCommandBarButtonGalleryDropDownItem19
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonGalleryDropDownItem19, "spreadsheetCommandBarButtonGalleryDropDownItem19");
            this.spreadsheetCommandBarButtonGalleryDropDownItem19.CommandName = "InsertChartOtherCommandGroup";
            this.spreadsheetCommandBarButtonGalleryDropDownItem19.Enabled = false;
            this.spreadsheetCommandBarButtonGalleryDropDownItem19.Id = 229;
            this.spreadsheetCommandBarButtonGalleryDropDownItem19.Name = "spreadsheetCommandBarButtonGalleryDropDownItem19";
            // 
            // spreadsheetCommandBarButtonItem82
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem82, "spreadsheetCommandBarButtonItem82");
            this.spreadsheetCommandBarButtonItem82.CommandName = "InsertHyperlink";
            this.spreadsheetCommandBarButtonItem82.Enabled = false;
            this.spreadsheetCommandBarButtonItem82.Id = 230;
            this.spreadsheetCommandBarButtonItem82.Name = "spreadsheetCommandBarButtonItem82";
            // 
            // spreadsheetCommandBarButtonItem83
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem83, "spreadsheetCommandBarButtonItem83");
            this.spreadsheetCommandBarButtonItem83.CommandName = "InsertSymbol";
            this.spreadsheetCommandBarButtonItem83.Enabled = false;
            this.spreadsheetCommandBarButtonItem83.Id = 231;
            this.spreadsheetCommandBarButtonItem83.Name = "spreadsheetCommandBarButtonItem83";
            // 
            // spreadsheetCommandBarButtonItem84
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem84, "spreadsheetCommandBarButtonItem84");
            this.spreadsheetCommandBarButtonItem84.CommandName = "PasteSelection";
            this.spreadsheetCommandBarButtonItem84.Enabled = false;
            this.spreadsheetCommandBarButtonItem84.Id = 242;
            this.spreadsheetCommandBarButtonItem84.Name = "spreadsheetCommandBarButtonItem84";
            this.spreadsheetCommandBarButtonItem84.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonItem85
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem85, "spreadsheetCommandBarButtonItem85");
            this.spreadsheetCommandBarButtonItem85.CommandName = "CutSelection";
            this.spreadsheetCommandBarButtonItem85.Enabled = false;
            this.spreadsheetCommandBarButtonItem85.Id = 243;
            this.spreadsheetCommandBarButtonItem85.Name = "spreadsheetCommandBarButtonItem85";
            this.spreadsheetCommandBarButtonItem85.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // spreadsheetCommandBarButtonItem86
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem86, "spreadsheetCommandBarButtonItem86");
            this.spreadsheetCommandBarButtonItem86.CommandName = "CopySelection";
            this.spreadsheetCommandBarButtonItem86.Enabled = false;
            this.spreadsheetCommandBarButtonItem86.Id = 244;
            this.spreadsheetCommandBarButtonItem86.Name = "spreadsheetCommandBarButtonItem86";
            this.spreadsheetCommandBarButtonItem86.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // spreadsheetCommandBarButtonItem87
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem87, "spreadsheetCommandBarButtonItem87");
            this.spreadsheetCommandBarButtonItem87.CommandName = "ShowPasteSpecialForm";
            this.spreadsheetCommandBarButtonItem87.Enabled = false;
            this.spreadsheetCommandBarButtonItem87.Id = 245;
            this.spreadsheetCommandBarButtonItem87.Name = "spreadsheetCommandBarButtonItem87";
            this.spreadsheetCommandBarButtonItem87.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)((DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // barButtonGroup1
            // 
            this.barButtonGroup1.Id = 232;
            this.barButtonGroup1.ItemLinks.Add(this.changeFontNameItem1);
            this.barButtonGroup1.ItemLinks.Add(this.changeFontSizeItem1);
            this.barButtonGroup1.ItemLinks.Add(this.spreadsheetCommandBarButtonItem88);
            this.barButtonGroup1.ItemLinks.Add(this.spreadsheetCommandBarButtonItem89);
            this.barButtonGroup1.Name = "barButtonGroup1";
            this.barButtonGroup1.Tag = "{B0CA3FA8-82D6-4BC4-BD31-D9AE56C1D033}";
            // 
            // changeFontNameItem1
            // 
            this.changeFontNameItem1.Edit = null;
            this.changeFontNameItem1.Enabled = false;
            this.changeFontNameItem1.Id = 246;
            this.changeFontNameItem1.Name = "changeFontNameItem1";
            // 
            // changeFontSizeItem1
            // 
            this.changeFontSizeItem1.Edit = null;
            this.changeFontSizeItem1.Enabled = false;
            this.changeFontSizeItem1.Id = 247;
            this.changeFontSizeItem1.Name = "changeFontSizeItem1";
            // 
            // spreadsheetCommandBarButtonItem88
            // 
            this.spreadsheetCommandBarButtonItem88.ButtonGroupTag = "{B0CA3FA8-82D6-4BC4-BD31-D9AE56C1D033}";
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem88, "spreadsheetCommandBarButtonItem88");
            this.spreadsheetCommandBarButtonItem88.CommandName = "FormatIncreaseFontSize";
            this.spreadsheetCommandBarButtonItem88.Enabled = false;
            this.spreadsheetCommandBarButtonItem88.Id = 248;
            this.spreadsheetCommandBarButtonItem88.Name = "spreadsheetCommandBarButtonItem88";
            this.spreadsheetCommandBarButtonItem88.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // spreadsheetCommandBarButtonItem89
            // 
            this.spreadsheetCommandBarButtonItem89.ButtonGroupTag = "{B0CA3FA8-82D6-4BC4-BD31-D9AE56C1D033}";
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem89, "spreadsheetCommandBarButtonItem89");
            this.spreadsheetCommandBarButtonItem89.CommandName = "FormatDecreaseFontSize";
            this.spreadsheetCommandBarButtonItem89.Enabled = false;
            this.spreadsheetCommandBarButtonItem89.Id = 249;
            this.spreadsheetCommandBarButtonItem89.Name = "spreadsheetCommandBarButtonItem89";
            this.spreadsheetCommandBarButtonItem89.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // barButtonGroup2
            // 
            this.barButtonGroup2.Id = 233;
            this.barButtonGroup2.ItemLinks.Add(this.spreadsheetCommandBarCheckItem28);
            this.barButtonGroup2.ItemLinks.Add(this.spreadsheetCommandBarCheckItem29);
            this.barButtonGroup2.ItemLinks.Add(this.spreadsheetCommandBarCheckItem30);
            this.barButtonGroup2.ItemLinks.Add(this.spreadsheetCommandBarCheckItem31);
            this.barButtonGroup2.Name = "barButtonGroup2";
            this.barButtonGroup2.Tag = "{56C139FB-52E5-405B-A03F-FA7DCABD1D17}";
            // 
            // spreadsheetCommandBarCheckItem28
            // 
            this.spreadsheetCommandBarCheckItem28.ButtonGroupTag = "{56C139FB-52E5-405B-A03F-FA7DCABD1D17}";
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem28, "spreadsheetCommandBarCheckItem28");
            this.spreadsheetCommandBarCheckItem28.CommandName = "FormatFontBold";
            this.spreadsheetCommandBarCheckItem28.Enabled = false;
            this.spreadsheetCommandBarCheckItem28.Id = 250;
            this.spreadsheetCommandBarCheckItem28.Name = "spreadsheetCommandBarCheckItem28";
            this.spreadsheetCommandBarCheckItem28.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // spreadsheetCommandBarCheckItem29
            // 
            this.spreadsheetCommandBarCheckItem29.ButtonGroupTag = "{56C139FB-52E5-405B-A03F-FA7DCABD1D17}";
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem29, "spreadsheetCommandBarCheckItem29");
            this.spreadsheetCommandBarCheckItem29.CommandName = "FormatFontItalic";
            this.spreadsheetCommandBarCheckItem29.Enabled = false;
            this.spreadsheetCommandBarCheckItem29.Id = 251;
            this.spreadsheetCommandBarCheckItem29.Name = "spreadsheetCommandBarCheckItem29";
            this.spreadsheetCommandBarCheckItem29.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // spreadsheetCommandBarCheckItem30
            // 
            this.spreadsheetCommandBarCheckItem30.ButtonGroupTag = "{56C139FB-52E5-405B-A03F-FA7DCABD1D17}";
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem30, "spreadsheetCommandBarCheckItem30");
            this.spreadsheetCommandBarCheckItem30.CommandName = "FormatFontUnderline";
            this.spreadsheetCommandBarCheckItem30.Enabled = false;
            this.spreadsheetCommandBarCheckItem30.Id = 252;
            this.spreadsheetCommandBarCheckItem30.Name = "spreadsheetCommandBarCheckItem30";
            this.spreadsheetCommandBarCheckItem30.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // spreadsheetCommandBarCheckItem31
            // 
            this.spreadsheetCommandBarCheckItem31.ButtonGroupTag = "{56C139FB-52E5-405B-A03F-FA7DCABD1D17}";
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem31, "spreadsheetCommandBarCheckItem31");
            this.spreadsheetCommandBarCheckItem31.CommandName = "FormatFontStrikeout";
            this.spreadsheetCommandBarCheckItem31.Enabled = false;
            this.spreadsheetCommandBarCheckItem31.Id = 253;
            this.spreadsheetCommandBarCheckItem31.Name = "spreadsheetCommandBarCheckItem31";
            this.spreadsheetCommandBarCheckItem31.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // barButtonGroup3
            // 
            this.barButtonGroup3.Id = 234;
            this.barButtonGroup3.ItemLinks.Add(this.spreadsheetCommandBarSubItem23);
            this.barButtonGroup3.Name = "barButtonGroup3";
            this.barButtonGroup3.Tag = "{DDB05A32-9207-4556-85CB-FE3403A197C7}";
            // 
            // spreadsheetCommandBarSubItem23
            // 
            this.spreadsheetCommandBarSubItem23.ButtonGroupTag = "{DDB05A32-9207-4556-85CB-FE3403A197C7}";
            resources.ApplyResources(this.spreadsheetCommandBarSubItem23, "spreadsheetCommandBarSubItem23");
            this.spreadsheetCommandBarSubItem23.CommandName = "FormatBordersCommandGroup";
            this.spreadsheetCommandBarSubItem23.Enabled = false;
            this.spreadsheetCommandBarSubItem23.Id = 254;
            this.spreadsheetCommandBarSubItem23.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem90),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem91),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem92),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem93),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem94),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem95),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem96),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem97),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem98),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem99),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem100),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem101),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem102),
            new DevExpress.XtraBars.LinkPersistInfo(this.changeBorderLineColorItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.changeBorderLineStyleItem1)});
            this.spreadsheetCommandBarSubItem23.Name = "spreadsheetCommandBarSubItem23";
            this.spreadsheetCommandBarSubItem23.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // spreadsheetCommandBarButtonItem90
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem90, "spreadsheetCommandBarButtonItem90");
            this.spreadsheetCommandBarButtonItem90.CommandName = "FormatBottomBorder";
            this.spreadsheetCommandBarButtonItem90.Enabled = false;
            this.spreadsheetCommandBarButtonItem90.Id = 255;
            this.spreadsheetCommandBarButtonItem90.Name = "spreadsheetCommandBarButtonItem90";
            // 
            // spreadsheetCommandBarButtonItem91
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem91, "spreadsheetCommandBarButtonItem91");
            this.spreadsheetCommandBarButtonItem91.CommandName = "FormatTopBorder";
            this.spreadsheetCommandBarButtonItem91.Enabled = false;
            this.spreadsheetCommandBarButtonItem91.Id = 256;
            this.spreadsheetCommandBarButtonItem91.Name = "spreadsheetCommandBarButtonItem91";
            // 
            // spreadsheetCommandBarButtonItem92
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem92, "spreadsheetCommandBarButtonItem92");
            this.spreadsheetCommandBarButtonItem92.CommandName = "FormatLeftBorder";
            this.spreadsheetCommandBarButtonItem92.Enabled = false;
            this.spreadsheetCommandBarButtonItem92.Id = 257;
            this.spreadsheetCommandBarButtonItem92.Name = "spreadsheetCommandBarButtonItem92";
            // 
            // spreadsheetCommandBarButtonItem93
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem93, "spreadsheetCommandBarButtonItem93");
            this.spreadsheetCommandBarButtonItem93.CommandName = "FormatRightBorder";
            this.spreadsheetCommandBarButtonItem93.Enabled = false;
            this.spreadsheetCommandBarButtonItem93.Id = 258;
            this.spreadsheetCommandBarButtonItem93.Name = "spreadsheetCommandBarButtonItem93";
            // 
            // spreadsheetCommandBarButtonItem94
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem94, "spreadsheetCommandBarButtonItem94");
            this.spreadsheetCommandBarButtonItem94.CommandName = "FormatNoBorders";
            this.spreadsheetCommandBarButtonItem94.Enabled = false;
            this.spreadsheetCommandBarButtonItem94.Id = 259;
            this.spreadsheetCommandBarButtonItem94.Name = "spreadsheetCommandBarButtonItem94";
            // 
            // spreadsheetCommandBarButtonItem95
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem95, "spreadsheetCommandBarButtonItem95");
            this.spreadsheetCommandBarButtonItem95.CommandName = "FormatAllBorders";
            this.spreadsheetCommandBarButtonItem95.Enabled = false;
            this.spreadsheetCommandBarButtonItem95.Id = 260;
            this.spreadsheetCommandBarButtonItem95.Name = "spreadsheetCommandBarButtonItem95";
            // 
            // spreadsheetCommandBarButtonItem96
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem96, "spreadsheetCommandBarButtonItem96");
            this.spreadsheetCommandBarButtonItem96.CommandName = "FormatOutsideBorders";
            this.spreadsheetCommandBarButtonItem96.Enabled = false;
            this.spreadsheetCommandBarButtonItem96.Id = 261;
            this.spreadsheetCommandBarButtonItem96.Name = "spreadsheetCommandBarButtonItem96";
            // 
            // spreadsheetCommandBarButtonItem97
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem97, "spreadsheetCommandBarButtonItem97");
            this.spreadsheetCommandBarButtonItem97.CommandName = "FormatThickBorder";
            this.spreadsheetCommandBarButtonItem97.Enabled = false;
            this.spreadsheetCommandBarButtonItem97.Id = 262;
            this.spreadsheetCommandBarButtonItem97.Name = "spreadsheetCommandBarButtonItem97";
            // 
            // spreadsheetCommandBarButtonItem98
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem98, "spreadsheetCommandBarButtonItem98");
            this.spreadsheetCommandBarButtonItem98.CommandName = "FormatBottomDoubleBorder";
            this.spreadsheetCommandBarButtonItem98.Enabled = false;
            this.spreadsheetCommandBarButtonItem98.Id = 263;
            this.spreadsheetCommandBarButtonItem98.Name = "spreadsheetCommandBarButtonItem98";
            // 
            // spreadsheetCommandBarButtonItem99
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem99, "spreadsheetCommandBarButtonItem99");
            this.spreadsheetCommandBarButtonItem99.CommandName = "FormatBottomThickBorder";
            this.spreadsheetCommandBarButtonItem99.Enabled = false;
            this.spreadsheetCommandBarButtonItem99.Id = 264;
            this.spreadsheetCommandBarButtonItem99.Name = "spreadsheetCommandBarButtonItem99";
            // 
            // spreadsheetCommandBarButtonItem100
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem100, "spreadsheetCommandBarButtonItem100");
            this.spreadsheetCommandBarButtonItem100.CommandName = "FormatTopAndBottomBorder";
            this.spreadsheetCommandBarButtonItem100.Enabled = false;
            this.spreadsheetCommandBarButtonItem100.Id = 265;
            this.spreadsheetCommandBarButtonItem100.Name = "spreadsheetCommandBarButtonItem100";
            // 
            // spreadsheetCommandBarButtonItem101
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem101, "spreadsheetCommandBarButtonItem101");
            this.spreadsheetCommandBarButtonItem101.CommandName = "FormatTopAndThickBottomBorder";
            this.spreadsheetCommandBarButtonItem101.Enabled = false;
            this.spreadsheetCommandBarButtonItem101.Id = 266;
            this.spreadsheetCommandBarButtonItem101.Name = "spreadsheetCommandBarButtonItem101";
            // 
            // spreadsheetCommandBarButtonItem102
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem102, "spreadsheetCommandBarButtonItem102");
            this.spreadsheetCommandBarButtonItem102.CommandName = "FormatTopAndDoubleBottomBorder";
            this.spreadsheetCommandBarButtonItem102.Enabled = false;
            this.spreadsheetCommandBarButtonItem102.Id = 267;
            this.spreadsheetCommandBarButtonItem102.Name = "spreadsheetCommandBarButtonItem102";
            // 
            // changeBorderLineColorItem1
            // 
            this.changeBorderLineColorItem1.ActAsDropDown = true;
            resources.ApplyResources(this.changeBorderLineColorItem1, "changeBorderLineColorItem1");
            this.changeBorderLineColorItem1.Enabled = false;
            this.changeBorderLineColorItem1.Id = 268;
            this.changeBorderLineColorItem1.Name = "changeBorderLineColorItem1";
            // 
            // changeBorderLineStyleItem1
            // 
            resources.ApplyResources(this.changeBorderLineStyleItem1, "changeBorderLineStyleItem1");
            this.changeBorderLineStyleItem1.Enabled = false;
            this.changeBorderLineStyleItem1.Id = 269;
            this.changeBorderLineStyleItem1.Name = "changeBorderLineStyleItem1";
            // 
            // barButtonGroup4
            // 
            this.barButtonGroup4.Id = 235;
            this.barButtonGroup4.ItemLinks.Add(this.changeCellFillColorItem1);
            this.barButtonGroup4.ItemLinks.Add(this.changeFontColorItem1);
            this.barButtonGroup4.Name = "barButtonGroup4";
            this.barButtonGroup4.Tag = "{C2275623-04A3-41E8-8D6A-EB5C7F8541D1}";
            // 
            // changeCellFillColorItem1
            // 
            resources.ApplyResources(this.changeCellFillColorItem1, "changeCellFillColorItem1");
            this.changeCellFillColorItem1.Enabled = false;
            this.changeCellFillColorItem1.Id = 270;
            this.changeCellFillColorItem1.Name = "changeCellFillColorItem1";
            // 
            // changeFontColorItem1
            // 
            resources.ApplyResources(this.changeFontColorItem1, "changeFontColorItem1");
            this.changeFontColorItem1.Enabled = false;
            this.changeFontColorItem1.Id = 271;
            this.changeFontColorItem1.Name = "changeFontColorItem1";
            // 
            // barButtonGroup5
            // 
            this.barButtonGroup5.Id = 236;
            this.barButtonGroup5.ItemLinks.Add(this.spreadsheetCommandBarCheckItem32);
            this.barButtonGroup5.ItemLinks.Add(this.spreadsheetCommandBarCheckItem33);
            this.barButtonGroup5.ItemLinks.Add(this.spreadsheetCommandBarCheckItem34);
            this.barButtonGroup5.Name = "barButtonGroup5";
            this.barButtonGroup5.Tag = "{03A0322B-12A2-4434-A487-8B5AAF64CCFC}";
            // 
            // spreadsheetCommandBarCheckItem32
            // 
            this.spreadsheetCommandBarCheckItem32.ButtonGroupTag = "{03A0322B-12A2-4434-A487-8B5AAF64CCFC}";
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem32, "spreadsheetCommandBarCheckItem32");
            this.spreadsheetCommandBarCheckItem32.CommandName = "FormatAlignmentTop";
            this.spreadsheetCommandBarCheckItem32.Enabled = false;
            this.spreadsheetCommandBarCheckItem32.Id = 272;
            this.spreadsheetCommandBarCheckItem32.Name = "spreadsheetCommandBarCheckItem32";
            this.spreadsheetCommandBarCheckItem32.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // spreadsheetCommandBarCheckItem33
            // 
            this.spreadsheetCommandBarCheckItem33.ButtonGroupTag = "{03A0322B-12A2-4434-A487-8B5AAF64CCFC}";
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem33, "spreadsheetCommandBarCheckItem33");
            this.spreadsheetCommandBarCheckItem33.CommandName = "FormatAlignmentMiddle";
            this.spreadsheetCommandBarCheckItem33.Enabled = false;
            this.spreadsheetCommandBarCheckItem33.Id = 273;
            this.spreadsheetCommandBarCheckItem33.Name = "spreadsheetCommandBarCheckItem33";
            this.spreadsheetCommandBarCheckItem33.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // spreadsheetCommandBarCheckItem34
            // 
            this.spreadsheetCommandBarCheckItem34.ButtonGroupTag = "{03A0322B-12A2-4434-A487-8B5AAF64CCFC}";
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem34, "spreadsheetCommandBarCheckItem34");
            this.spreadsheetCommandBarCheckItem34.CommandName = "FormatAlignmentBottom";
            this.spreadsheetCommandBarCheckItem34.Enabled = false;
            this.spreadsheetCommandBarCheckItem34.Id = 274;
            this.spreadsheetCommandBarCheckItem34.Name = "spreadsheetCommandBarCheckItem34";
            this.spreadsheetCommandBarCheckItem34.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // barButtonGroup6
            // 
            this.barButtonGroup6.Id = 237;
            this.barButtonGroup6.ItemLinks.Add(this.spreadsheetCommandBarCheckItem35);
            this.barButtonGroup6.ItemLinks.Add(this.spreadsheetCommandBarCheckItem36);
            this.barButtonGroup6.ItemLinks.Add(this.spreadsheetCommandBarCheckItem37);
            this.barButtonGroup6.Name = "barButtonGroup6";
            this.barButtonGroup6.Tag = "{ECC693B7-EF59-4007-A0DB-A9550214A0F2}";
            // 
            // spreadsheetCommandBarCheckItem35
            // 
            this.spreadsheetCommandBarCheckItem35.ButtonGroupTag = "{ECC693B7-EF59-4007-A0DB-A9550214A0F2}";
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem35, "spreadsheetCommandBarCheckItem35");
            this.spreadsheetCommandBarCheckItem35.CommandName = "FormatAlignmentLeft";
            this.spreadsheetCommandBarCheckItem35.Enabled = false;
            this.spreadsheetCommandBarCheckItem35.Id = 275;
            this.spreadsheetCommandBarCheckItem35.Name = "spreadsheetCommandBarCheckItem35";
            this.spreadsheetCommandBarCheckItem35.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // spreadsheetCommandBarCheckItem36
            // 
            this.spreadsheetCommandBarCheckItem36.ButtonGroupTag = "{ECC693B7-EF59-4007-A0DB-A9550214A0F2}";
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem36, "spreadsheetCommandBarCheckItem36");
            this.spreadsheetCommandBarCheckItem36.CommandName = "FormatAlignmentCenter";
            this.spreadsheetCommandBarCheckItem36.Enabled = false;
            this.spreadsheetCommandBarCheckItem36.Id = 276;
            this.spreadsheetCommandBarCheckItem36.Name = "spreadsheetCommandBarCheckItem36";
            this.spreadsheetCommandBarCheckItem36.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // spreadsheetCommandBarCheckItem37
            // 
            this.spreadsheetCommandBarCheckItem37.ButtonGroupTag = "{ECC693B7-EF59-4007-A0DB-A9550214A0F2}";
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem37, "spreadsheetCommandBarCheckItem37");
            this.spreadsheetCommandBarCheckItem37.CommandName = "FormatAlignmentRight";
            this.spreadsheetCommandBarCheckItem37.Enabled = false;
            this.spreadsheetCommandBarCheckItem37.Id = 277;
            this.spreadsheetCommandBarCheckItem37.Name = "spreadsheetCommandBarCheckItem37";
            this.spreadsheetCommandBarCheckItem37.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // barButtonGroup7
            // 
            this.barButtonGroup7.Id = 238;
            this.barButtonGroup7.ItemLinks.Add(this.spreadsheetCommandBarButtonItem103);
            this.barButtonGroup7.ItemLinks.Add(this.spreadsheetCommandBarButtonItem104);
            this.barButtonGroup7.Name = "barButtonGroup7";
            this.barButtonGroup7.Tag = "{A5E37DED-106E-44FC-8044-CE3824C08225}";
            // 
            // spreadsheetCommandBarButtonItem103
            // 
            this.spreadsheetCommandBarButtonItem103.ButtonGroupTag = "{A5E37DED-106E-44FC-8044-CE3824C08225}";
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem103, "spreadsheetCommandBarButtonItem103");
            this.spreadsheetCommandBarButtonItem103.CommandName = "FormatDecreaseIndent";
            this.spreadsheetCommandBarButtonItem103.Enabled = false;
            this.spreadsheetCommandBarButtonItem103.Id = 278;
            this.spreadsheetCommandBarButtonItem103.Name = "spreadsheetCommandBarButtonItem103";
            this.spreadsheetCommandBarButtonItem103.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // spreadsheetCommandBarButtonItem104
            // 
            this.spreadsheetCommandBarButtonItem104.ButtonGroupTag = "{A5E37DED-106E-44FC-8044-CE3824C08225}";
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem104, "spreadsheetCommandBarButtonItem104");
            this.spreadsheetCommandBarButtonItem104.CommandName = "FormatIncreaseIndent";
            this.spreadsheetCommandBarButtonItem104.Enabled = false;
            this.spreadsheetCommandBarButtonItem104.Id = 279;
            this.spreadsheetCommandBarButtonItem104.Name = "spreadsheetCommandBarButtonItem104";
            this.spreadsheetCommandBarButtonItem104.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // spreadsheetCommandBarCheckItem38
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem38, "spreadsheetCommandBarCheckItem38");
            this.spreadsheetCommandBarCheckItem38.CommandName = "FormatWrapText";
            this.spreadsheetCommandBarCheckItem38.Enabled = false;
            this.spreadsheetCommandBarCheckItem38.Id = 280;
            this.spreadsheetCommandBarCheckItem38.Name = "spreadsheetCommandBarCheckItem38";
            this.spreadsheetCommandBarCheckItem38.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // spreadsheetCommandBarSubItem24
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem24, "spreadsheetCommandBarSubItem24");
            this.spreadsheetCommandBarSubItem24.CommandName = "EditingMergeCellsCommandGroup";
            this.spreadsheetCommandBarSubItem24.Enabled = false;
            this.spreadsheetCommandBarSubItem24.Id = 281;
            this.spreadsheetCommandBarSubItem24.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarCheckItem39),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem105),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem106),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem107)});
            this.spreadsheetCommandBarSubItem24.Name = "spreadsheetCommandBarSubItem24";
            this.spreadsheetCommandBarSubItem24.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // spreadsheetCommandBarCheckItem39
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem39, "spreadsheetCommandBarCheckItem39");
            this.spreadsheetCommandBarCheckItem39.CommandName = "EditingMergeAndCenterCells";
            this.spreadsheetCommandBarCheckItem39.Enabled = false;
            this.spreadsheetCommandBarCheckItem39.Id = 282;
            this.spreadsheetCommandBarCheckItem39.Name = "spreadsheetCommandBarCheckItem39";
            // 
            // spreadsheetCommandBarButtonItem105
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem105, "spreadsheetCommandBarButtonItem105");
            this.spreadsheetCommandBarButtonItem105.CommandName = "EditingMergeCellsAcross";
            this.spreadsheetCommandBarButtonItem105.Enabled = false;
            this.spreadsheetCommandBarButtonItem105.Id = 283;
            this.spreadsheetCommandBarButtonItem105.Name = "spreadsheetCommandBarButtonItem105";
            // 
            // spreadsheetCommandBarButtonItem106
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem106, "spreadsheetCommandBarButtonItem106");
            this.spreadsheetCommandBarButtonItem106.CommandName = "EditingMergeCells";
            this.spreadsheetCommandBarButtonItem106.Enabled = false;
            this.spreadsheetCommandBarButtonItem106.Id = 284;
            this.spreadsheetCommandBarButtonItem106.Name = "spreadsheetCommandBarButtonItem106";
            // 
            // spreadsheetCommandBarButtonItem107
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem107, "spreadsheetCommandBarButtonItem107");
            this.spreadsheetCommandBarButtonItem107.CommandName = "EditingUnmergeCells";
            this.spreadsheetCommandBarButtonItem107.Enabled = false;
            this.spreadsheetCommandBarButtonItem107.Id = 285;
            this.spreadsheetCommandBarButtonItem107.Name = "spreadsheetCommandBarButtonItem107";
            // 
            // barButtonGroup8
            // 
            this.barButtonGroup8.Id = 239;
            this.barButtonGroup8.ItemLinks.Add(this.changeNumberFormatItem1);
            this.barButtonGroup8.Name = "barButtonGroup8";
            this.barButtonGroup8.Tag = "{0B3A7A43-3079-4ce0-83A8-3789F5F6DC9F}";
            // 
            // changeNumberFormatItem1
            // 
            this.changeNumberFormatItem1.Edit = null;
            this.changeNumberFormatItem1.Enabled = false;
            this.changeNumberFormatItem1.Id = 286;
            this.changeNumberFormatItem1.Name = "changeNumberFormatItem1";
            // 
            // barButtonGroup9
            // 
            this.barButtonGroup9.Id = 240;
            this.barButtonGroup9.ItemLinks.Add(this.spreadsheetCommandBarSubItem25);
            this.barButtonGroup9.ItemLinks.Add(this.spreadsheetCommandBarButtonItem113);
            this.barButtonGroup9.ItemLinks.Add(this.spreadsheetCommandBarButtonItem114);
            this.barButtonGroup9.Name = "barButtonGroup9";
            this.barButtonGroup9.Tag = "{508C2CE6-E1C8-4DD1-BA50-6C210FDB31B0}";
            // 
            // spreadsheetCommandBarSubItem25
            // 
            this.spreadsheetCommandBarSubItem25.ButtonGroupTag = "{508C2CE6-E1C8-4DD1-BA50-6C210FDB31B0}";
            resources.ApplyResources(this.spreadsheetCommandBarSubItem25, "spreadsheetCommandBarSubItem25");
            this.spreadsheetCommandBarSubItem25.CommandName = "FormatNumberAccountingCommandGroup";
            this.spreadsheetCommandBarSubItem25.Enabled = false;
            this.spreadsheetCommandBarSubItem25.Id = 287;
            this.spreadsheetCommandBarSubItem25.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem108),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem109),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem110),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem111),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem112)});
            this.spreadsheetCommandBarSubItem25.Name = "spreadsheetCommandBarSubItem25";
            this.spreadsheetCommandBarSubItem25.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // spreadsheetCommandBarButtonItem108
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem108, "spreadsheetCommandBarButtonItem108");
            this.spreadsheetCommandBarButtonItem108.CommandName = "FormatNumberAccountingUS";
            this.spreadsheetCommandBarButtonItem108.Enabled = false;
            this.spreadsheetCommandBarButtonItem108.Id = 288;
            this.spreadsheetCommandBarButtonItem108.Name = "spreadsheetCommandBarButtonItem108";
            // 
            // spreadsheetCommandBarButtonItem109
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem109, "spreadsheetCommandBarButtonItem109");
            this.spreadsheetCommandBarButtonItem109.CommandName = "FormatNumberAccountingUK";
            this.spreadsheetCommandBarButtonItem109.Enabled = false;
            this.spreadsheetCommandBarButtonItem109.Id = 289;
            this.spreadsheetCommandBarButtonItem109.Name = "spreadsheetCommandBarButtonItem109";
            // 
            // spreadsheetCommandBarButtonItem110
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem110, "spreadsheetCommandBarButtonItem110");
            this.spreadsheetCommandBarButtonItem110.CommandName = "FormatNumberAccountingEuro";
            this.spreadsheetCommandBarButtonItem110.Enabled = false;
            this.spreadsheetCommandBarButtonItem110.Id = 290;
            this.spreadsheetCommandBarButtonItem110.Name = "spreadsheetCommandBarButtonItem110";
            // 
            // spreadsheetCommandBarButtonItem111
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem111, "spreadsheetCommandBarButtonItem111");
            this.spreadsheetCommandBarButtonItem111.CommandName = "FormatNumberAccountingPRC";
            this.spreadsheetCommandBarButtonItem111.Enabled = false;
            this.spreadsheetCommandBarButtonItem111.Id = 291;
            this.spreadsheetCommandBarButtonItem111.Name = "spreadsheetCommandBarButtonItem111";
            // 
            // spreadsheetCommandBarButtonItem112
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem112, "spreadsheetCommandBarButtonItem112");
            this.spreadsheetCommandBarButtonItem112.CommandName = "FormatNumberAccountingSwiss";
            this.spreadsheetCommandBarButtonItem112.Enabled = false;
            this.spreadsheetCommandBarButtonItem112.Id = 292;
            this.spreadsheetCommandBarButtonItem112.Name = "spreadsheetCommandBarButtonItem112";
            // 
            // spreadsheetCommandBarButtonItem113
            // 
            this.spreadsheetCommandBarButtonItem113.ButtonGroupTag = "{508C2CE6-E1C8-4DD1-BA50-6C210FDB31B0}";
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem113, "spreadsheetCommandBarButtonItem113");
            this.spreadsheetCommandBarButtonItem113.CommandName = "FormatNumberPercent";
            this.spreadsheetCommandBarButtonItem113.Enabled = false;
            this.spreadsheetCommandBarButtonItem113.Id = 293;
            this.spreadsheetCommandBarButtonItem113.Name = "spreadsheetCommandBarButtonItem113";
            this.spreadsheetCommandBarButtonItem113.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // spreadsheetCommandBarButtonItem114
            // 
            this.spreadsheetCommandBarButtonItem114.ButtonGroupTag = "{508C2CE6-E1C8-4DD1-BA50-6C210FDB31B0}";
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem114, "spreadsheetCommandBarButtonItem114");
            this.spreadsheetCommandBarButtonItem114.CommandName = "FormatNumberAccounting";
            this.spreadsheetCommandBarButtonItem114.Enabled = false;
            this.spreadsheetCommandBarButtonItem114.Id = 294;
            this.spreadsheetCommandBarButtonItem114.Name = "spreadsheetCommandBarButtonItem114";
            this.spreadsheetCommandBarButtonItem114.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // barButtonGroup10
            // 
            this.barButtonGroup10.Id = 241;
            this.barButtonGroup10.ItemLinks.Add(this.spreadsheetCommandBarButtonItem115);
            this.barButtonGroup10.ItemLinks.Add(this.spreadsheetCommandBarButtonItem116);
            this.barButtonGroup10.Name = "barButtonGroup10";
            this.barButtonGroup10.Tag = "{BBAB348B-BDB2-487A-A883-EFB9982DC698}";
            // 
            // spreadsheetCommandBarButtonItem115
            // 
            this.spreadsheetCommandBarButtonItem115.ButtonGroupTag = "{BBAB348B-BDB2-487A-A883-EFB9982DC698}";
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem115, "spreadsheetCommandBarButtonItem115");
            this.spreadsheetCommandBarButtonItem115.CommandName = "FormatNumberIncreaseDecimal";
            this.spreadsheetCommandBarButtonItem115.Enabled = false;
            this.spreadsheetCommandBarButtonItem115.Id = 295;
            this.spreadsheetCommandBarButtonItem115.Name = "spreadsheetCommandBarButtonItem115";
            this.spreadsheetCommandBarButtonItem115.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // spreadsheetCommandBarButtonItem116
            // 
            this.spreadsheetCommandBarButtonItem116.ButtonGroupTag = "{BBAB348B-BDB2-487A-A883-EFB9982DC698}";
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem116, "spreadsheetCommandBarButtonItem116");
            this.spreadsheetCommandBarButtonItem116.CommandName = "FormatNumberDecreaseDecimal";
            this.spreadsheetCommandBarButtonItem116.Enabled = false;
            this.spreadsheetCommandBarButtonItem116.Id = 296;
            this.spreadsheetCommandBarButtonItem116.Name = "spreadsheetCommandBarButtonItem116";
            this.spreadsheetCommandBarButtonItem116.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText;
            // 
            // spreadsheetCommandBarSubItem26
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem26, "spreadsheetCommandBarSubItem26");
            this.spreadsheetCommandBarSubItem26.CommandName = "ConditionalFormattingCommandGroup";
            this.spreadsheetCommandBarSubItem26.Enabled = false;
            this.spreadsheetCommandBarSubItem26.Id = 297;
            this.spreadsheetCommandBarSubItem26.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarSubItem27),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarSubItem28),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonGalleryDropDownItem20),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonGalleryDropDownItem21),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonGalleryDropDownItem22),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarSubItem29)});
            this.spreadsheetCommandBarSubItem26.Name = "spreadsheetCommandBarSubItem26";
            // 
            // spreadsheetCommandBarSubItem27
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem27, "spreadsheetCommandBarSubItem27");
            this.spreadsheetCommandBarSubItem27.CommandName = "ConditionalFormattingHighlightCellsRuleCommandGroup";
            this.spreadsheetCommandBarSubItem27.Enabled = false;
            this.spreadsheetCommandBarSubItem27.Id = 305;
            this.spreadsheetCommandBarSubItem27.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem117),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem118),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem119),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem120),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem121),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem122),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem123)});
            this.spreadsheetCommandBarSubItem27.Name = "spreadsheetCommandBarSubItem27";
            // 
            // spreadsheetCommandBarButtonItem117
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem117, "spreadsheetCommandBarButtonItem117");
            this.spreadsheetCommandBarButtonItem117.CommandName = "ConditionalFormattingGreaterThanRuleCommand";
            this.spreadsheetCommandBarButtonItem117.Enabled = false;
            this.spreadsheetCommandBarButtonItem117.Id = 298;
            this.spreadsheetCommandBarButtonItem117.Name = "spreadsheetCommandBarButtonItem117";
            this.spreadsheetCommandBarButtonItem117.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonItem118
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem118, "spreadsheetCommandBarButtonItem118");
            this.spreadsheetCommandBarButtonItem118.CommandName = "ConditionalFormattingLessThanRuleCommand";
            this.spreadsheetCommandBarButtonItem118.Enabled = false;
            this.spreadsheetCommandBarButtonItem118.Id = 299;
            this.spreadsheetCommandBarButtonItem118.Name = "spreadsheetCommandBarButtonItem118";
            this.spreadsheetCommandBarButtonItem118.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonItem119
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem119, "spreadsheetCommandBarButtonItem119");
            this.spreadsheetCommandBarButtonItem119.CommandName = "ConditionalFormattingBetweenRuleCommand";
            this.spreadsheetCommandBarButtonItem119.Enabled = false;
            this.spreadsheetCommandBarButtonItem119.Id = 300;
            this.spreadsheetCommandBarButtonItem119.Name = "spreadsheetCommandBarButtonItem119";
            this.spreadsheetCommandBarButtonItem119.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonItem120
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem120, "spreadsheetCommandBarButtonItem120");
            this.spreadsheetCommandBarButtonItem120.CommandName = "ConditionalFormattingEqualToRuleCommand";
            this.spreadsheetCommandBarButtonItem120.Enabled = false;
            this.spreadsheetCommandBarButtonItem120.Id = 301;
            this.spreadsheetCommandBarButtonItem120.Name = "spreadsheetCommandBarButtonItem120";
            this.spreadsheetCommandBarButtonItem120.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonItem121
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem121, "spreadsheetCommandBarButtonItem121");
            this.spreadsheetCommandBarButtonItem121.CommandName = "ConditionalFormattingTextContainsRuleCommand";
            this.spreadsheetCommandBarButtonItem121.Enabled = false;
            this.spreadsheetCommandBarButtonItem121.Id = 302;
            this.spreadsheetCommandBarButtonItem121.Name = "spreadsheetCommandBarButtonItem121";
            this.spreadsheetCommandBarButtonItem121.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonItem122
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem122, "spreadsheetCommandBarButtonItem122");
            this.spreadsheetCommandBarButtonItem122.CommandName = "ConditionalFormattingDateOccurringRuleCommand";
            this.spreadsheetCommandBarButtonItem122.Enabled = false;
            this.spreadsheetCommandBarButtonItem122.Id = 303;
            this.spreadsheetCommandBarButtonItem122.Name = "spreadsheetCommandBarButtonItem122";
            this.spreadsheetCommandBarButtonItem122.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonItem123
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem123, "spreadsheetCommandBarButtonItem123");
            this.spreadsheetCommandBarButtonItem123.CommandName = "ConditionalFormattingDuplicateValuesRuleCommand";
            this.spreadsheetCommandBarButtonItem123.Enabled = false;
            this.spreadsheetCommandBarButtonItem123.Id = 304;
            this.spreadsheetCommandBarButtonItem123.Name = "spreadsheetCommandBarButtonItem123";
            this.spreadsheetCommandBarButtonItem123.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarSubItem28
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem28, "spreadsheetCommandBarSubItem28");
            this.spreadsheetCommandBarSubItem28.CommandName = "ConditionalFormattingTopBottomRuleCommandGroup";
            this.spreadsheetCommandBarSubItem28.Enabled = false;
            this.spreadsheetCommandBarSubItem28.Id = 312;
            this.spreadsheetCommandBarSubItem28.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem124),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem125),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem126),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem127),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem128),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem129)});
            this.spreadsheetCommandBarSubItem28.Name = "spreadsheetCommandBarSubItem28";
            // 
            // spreadsheetCommandBarButtonItem124
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem124, "spreadsheetCommandBarButtonItem124");
            this.spreadsheetCommandBarButtonItem124.CommandName = "ConditionalFormattingTop10RuleCommand";
            this.spreadsheetCommandBarButtonItem124.Enabled = false;
            this.spreadsheetCommandBarButtonItem124.Id = 306;
            this.spreadsheetCommandBarButtonItem124.Name = "spreadsheetCommandBarButtonItem124";
            this.spreadsheetCommandBarButtonItem124.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonItem125
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem125, "spreadsheetCommandBarButtonItem125");
            this.spreadsheetCommandBarButtonItem125.CommandName = "ConditionalFormattingTop10PercentRuleCommand";
            this.spreadsheetCommandBarButtonItem125.Enabled = false;
            this.spreadsheetCommandBarButtonItem125.Id = 307;
            this.spreadsheetCommandBarButtonItem125.Name = "spreadsheetCommandBarButtonItem125";
            this.spreadsheetCommandBarButtonItem125.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonItem126
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem126, "spreadsheetCommandBarButtonItem126");
            this.spreadsheetCommandBarButtonItem126.CommandName = "ConditionalFormattingBottom10RuleCommand";
            this.spreadsheetCommandBarButtonItem126.Enabled = false;
            this.spreadsheetCommandBarButtonItem126.Id = 308;
            this.spreadsheetCommandBarButtonItem126.Name = "spreadsheetCommandBarButtonItem126";
            this.spreadsheetCommandBarButtonItem126.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonItem127
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem127, "spreadsheetCommandBarButtonItem127");
            this.spreadsheetCommandBarButtonItem127.CommandName = "ConditionalFormattingBottom10PercentRuleCommand";
            this.spreadsheetCommandBarButtonItem127.Enabled = false;
            this.spreadsheetCommandBarButtonItem127.Id = 309;
            this.spreadsheetCommandBarButtonItem127.Name = "spreadsheetCommandBarButtonItem127";
            this.spreadsheetCommandBarButtonItem127.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonItem128
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem128, "spreadsheetCommandBarButtonItem128");
            this.spreadsheetCommandBarButtonItem128.CommandName = "ConditionalFormattingAboveAverageRuleCommand";
            this.spreadsheetCommandBarButtonItem128.Enabled = false;
            this.spreadsheetCommandBarButtonItem128.Id = 310;
            this.spreadsheetCommandBarButtonItem128.Name = "spreadsheetCommandBarButtonItem128";
            this.spreadsheetCommandBarButtonItem128.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonItem129
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem129, "spreadsheetCommandBarButtonItem129");
            this.spreadsheetCommandBarButtonItem129.CommandName = "ConditionalFormattingBelowAverageRuleCommand";
            this.spreadsheetCommandBarButtonItem129.Enabled = false;
            this.spreadsheetCommandBarButtonItem129.Id = 311;
            this.spreadsheetCommandBarButtonItem129.Name = "spreadsheetCommandBarButtonItem129";
            this.spreadsheetCommandBarButtonItem129.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonGalleryDropDownItem20
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonGalleryDropDownItem20, "spreadsheetCommandBarButtonGalleryDropDownItem20");
            this.spreadsheetCommandBarButtonGalleryDropDownItem20.CommandName = "ConditionalFormattingDataBarsCommandGroup";
            this.spreadsheetCommandBarButtonGalleryDropDownItem20.Enabled = false;
            this.spreadsheetCommandBarButtonGalleryDropDownItem20.Id = 313;
            this.spreadsheetCommandBarButtonGalleryDropDownItem20.Name = "spreadsheetCommandBarButtonGalleryDropDownItem20";
            this.spreadsheetCommandBarButtonGalleryDropDownItem20.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonGalleryDropDownItem21
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonGalleryDropDownItem21, "spreadsheetCommandBarButtonGalleryDropDownItem21");
            this.spreadsheetCommandBarButtonGalleryDropDownItem21.CommandName = "ConditionalFormattingColorScalesCommandGroup";
            this.spreadsheetCommandBarButtonGalleryDropDownItem21.Enabled = false;
            this.spreadsheetCommandBarButtonGalleryDropDownItem21.Id = 314;
            this.spreadsheetCommandBarButtonGalleryDropDownItem21.Name = "spreadsheetCommandBarButtonGalleryDropDownItem21";
            this.spreadsheetCommandBarButtonGalleryDropDownItem21.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarButtonGalleryDropDownItem22
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonGalleryDropDownItem22, "spreadsheetCommandBarButtonGalleryDropDownItem22");
            this.spreadsheetCommandBarButtonGalleryDropDownItem22.CommandName = "ConditionalFormattingIconSetsCommandGroup";
            this.spreadsheetCommandBarButtonGalleryDropDownItem22.Enabled = false;
            this.spreadsheetCommandBarButtonGalleryDropDownItem22.Id = 315;
            this.spreadsheetCommandBarButtonGalleryDropDownItem22.Name = "spreadsheetCommandBarButtonGalleryDropDownItem22";
            this.spreadsheetCommandBarButtonGalleryDropDownItem22.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // spreadsheetCommandBarSubItem29
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem29, "spreadsheetCommandBarSubItem29");
            this.spreadsheetCommandBarSubItem29.CommandName = "ConditionalFormattingRemoveCommandGroup";
            this.spreadsheetCommandBarSubItem29.Enabled = false;
            this.spreadsheetCommandBarSubItem29.Id = 318;
            this.spreadsheetCommandBarSubItem29.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem130),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem131)});
            this.spreadsheetCommandBarSubItem29.Name = "spreadsheetCommandBarSubItem29";
            // 
            // spreadsheetCommandBarButtonItem130
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem130, "spreadsheetCommandBarButtonItem130");
            this.spreadsheetCommandBarButtonItem130.CommandName = "ConditionalFormattingRemoveFromSheet";
            this.spreadsheetCommandBarButtonItem130.Enabled = false;
            this.spreadsheetCommandBarButtonItem130.Id = 316;
            this.spreadsheetCommandBarButtonItem130.Name = "spreadsheetCommandBarButtonItem130";
            // 
            // spreadsheetCommandBarButtonItem131
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem131, "spreadsheetCommandBarButtonItem131");
            this.spreadsheetCommandBarButtonItem131.CommandName = "ConditionalFormattingRemove";
            this.spreadsheetCommandBarButtonItem131.Enabled = false;
            this.spreadsheetCommandBarButtonItem131.Id = 317;
            this.spreadsheetCommandBarButtonItem131.Name = "spreadsheetCommandBarButtonItem131";
            // 
            // galleryFormatAsTableItem1
            // 
            resources.ApplyResources(this.galleryFormatAsTableItem1, "galleryFormatAsTableItem1");
            this.galleryFormatAsTableItem1.Enabled = false;
            this.galleryFormatAsTableItem1.Id = 319;
            this.galleryFormatAsTableItem1.Name = "galleryFormatAsTableItem1";
            // 
            // galleryChangeStyleItem1
            // 
            resources.ApplyResources(this.galleryChangeStyleItem1, "galleryChangeStyleItem1");
            this.galleryChangeStyleItem1.Enabled = false;
            // 
            // 
            // 
            this.galleryChangeStyleItem1.Gallery.DrawImageBackground = false;
            this.galleryChangeStyleItem1.Gallery.ImageSize = new System.Drawing.Size(65, 46);
            this.galleryChangeStyleItem1.Gallery.ItemAutoSizeMode = DevExpress.XtraBars.Ribbon.Gallery.GalleryItemAutoSizeMode.None;
            this.galleryChangeStyleItem1.Gallery.ItemSize = new System.Drawing.Size(106, 28);
            this.galleryChangeStyleItem1.Gallery.RowCount = 9;
            this.galleryChangeStyleItem1.Gallery.ShowItemText = true;
            this.galleryChangeStyleItem1.Id = 320;
            this.galleryChangeStyleItem1.Name = "galleryChangeStyleItem1";
            // 
            // spreadsheetCommandBarSubItem30
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem30, "spreadsheetCommandBarSubItem30");
            this.spreadsheetCommandBarSubItem30.CommandName = "InsertCellsCommandGroup";
            this.spreadsheetCommandBarSubItem30.Enabled = false;
            this.spreadsheetCommandBarSubItem30.Id = 321;
            this.spreadsheetCommandBarSubItem30.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem132),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem133),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem134)});
            this.spreadsheetCommandBarSubItem30.Name = "spreadsheetCommandBarSubItem30";
            // 
            // spreadsheetCommandBarButtonItem132
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem132, "spreadsheetCommandBarButtonItem132");
            this.spreadsheetCommandBarButtonItem132.CommandName = "InsertSheetRows";
            this.spreadsheetCommandBarButtonItem132.Enabled = false;
            this.spreadsheetCommandBarButtonItem132.Id = 322;
            this.spreadsheetCommandBarButtonItem132.Name = "spreadsheetCommandBarButtonItem132";
            // 
            // spreadsheetCommandBarButtonItem133
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem133, "spreadsheetCommandBarButtonItem133");
            this.spreadsheetCommandBarButtonItem133.CommandName = "InsertSheetColumns";
            this.spreadsheetCommandBarButtonItem133.Enabled = false;
            this.spreadsheetCommandBarButtonItem133.Id = 323;
            this.spreadsheetCommandBarButtonItem133.Name = "spreadsheetCommandBarButtonItem133";
            // 
            // spreadsheetCommandBarButtonItem134
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem134, "spreadsheetCommandBarButtonItem134");
            this.spreadsheetCommandBarButtonItem134.CommandName = "InsertSheet";
            this.spreadsheetCommandBarButtonItem134.Enabled = false;
            this.spreadsheetCommandBarButtonItem134.Id = 324;
            this.spreadsheetCommandBarButtonItem134.Name = "spreadsheetCommandBarButtonItem134";
            // 
            // spreadsheetCommandBarSubItem31
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem31, "spreadsheetCommandBarSubItem31");
            this.spreadsheetCommandBarSubItem31.CommandName = "RemoveCellsCommandGroup";
            this.spreadsheetCommandBarSubItem31.Enabled = false;
            this.spreadsheetCommandBarSubItem31.Id = 325;
            this.spreadsheetCommandBarSubItem31.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem135),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem136),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem137)});
            this.spreadsheetCommandBarSubItem31.Name = "spreadsheetCommandBarSubItem31";
            // 
            // spreadsheetCommandBarButtonItem135
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem135, "spreadsheetCommandBarButtonItem135");
            this.spreadsheetCommandBarButtonItem135.CommandName = "RemoveSheetRows";
            this.spreadsheetCommandBarButtonItem135.Enabled = false;
            this.spreadsheetCommandBarButtonItem135.Id = 326;
            this.spreadsheetCommandBarButtonItem135.Name = "spreadsheetCommandBarButtonItem135";
            // 
            // spreadsheetCommandBarButtonItem136
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem136, "spreadsheetCommandBarButtonItem136");
            this.spreadsheetCommandBarButtonItem136.CommandName = "RemoveSheetColumns";
            this.spreadsheetCommandBarButtonItem136.Enabled = false;
            this.spreadsheetCommandBarButtonItem136.Id = 327;
            this.spreadsheetCommandBarButtonItem136.Name = "spreadsheetCommandBarButtonItem136";
            // 
            // spreadsheetCommandBarButtonItem137
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem137, "spreadsheetCommandBarButtonItem137");
            this.spreadsheetCommandBarButtonItem137.CommandName = "RemoveSheet";
            this.spreadsheetCommandBarButtonItem137.Enabled = false;
            this.spreadsheetCommandBarButtonItem137.Id = 328;
            this.spreadsheetCommandBarButtonItem137.Name = "spreadsheetCommandBarButtonItem137";
            // 
            // spreadsheetCommandBarSubItem32
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem32, "spreadsheetCommandBarSubItem32");
            this.spreadsheetCommandBarSubItem32.CommandName = "FormatCommandGroup";
            this.spreadsheetCommandBarSubItem32.Enabled = false;
            this.spreadsheetCommandBarSubItem32.Id = 329;
            this.spreadsheetCommandBarSubItem32.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem138),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem139),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem140),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem141),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem142),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarSubItem33),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem149),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem150),
            new DevExpress.XtraBars.LinkPersistInfo(this.changeSheetTabColorItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem46),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarCheckItem40),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem152)});
            this.spreadsheetCommandBarSubItem32.Name = "spreadsheetCommandBarSubItem32";
            // 
            // spreadsheetCommandBarButtonItem138
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem138, "spreadsheetCommandBarButtonItem138");
            this.spreadsheetCommandBarButtonItem138.CommandName = "FormatRowHeight";
            this.spreadsheetCommandBarButtonItem138.Enabled = false;
            this.spreadsheetCommandBarButtonItem138.Id = 330;
            this.spreadsheetCommandBarButtonItem138.Name = "spreadsheetCommandBarButtonItem138";
            // 
            // spreadsheetCommandBarButtonItem139
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem139, "spreadsheetCommandBarButtonItem139");
            this.spreadsheetCommandBarButtonItem139.CommandName = "FormatAutoFitRowHeight";
            this.spreadsheetCommandBarButtonItem139.Enabled = false;
            this.spreadsheetCommandBarButtonItem139.Id = 331;
            this.spreadsheetCommandBarButtonItem139.Name = "spreadsheetCommandBarButtonItem139";
            // 
            // spreadsheetCommandBarButtonItem140
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem140, "spreadsheetCommandBarButtonItem140");
            this.spreadsheetCommandBarButtonItem140.CommandName = "FormatColumnWidth";
            this.spreadsheetCommandBarButtonItem140.Enabled = false;
            this.spreadsheetCommandBarButtonItem140.Id = 332;
            this.spreadsheetCommandBarButtonItem140.Name = "spreadsheetCommandBarButtonItem140";
            // 
            // spreadsheetCommandBarButtonItem141
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem141, "spreadsheetCommandBarButtonItem141");
            this.spreadsheetCommandBarButtonItem141.CommandName = "FormatAutoFitColumnWidth";
            this.spreadsheetCommandBarButtonItem141.Enabled = false;
            this.spreadsheetCommandBarButtonItem141.Id = 333;
            this.spreadsheetCommandBarButtonItem141.Name = "spreadsheetCommandBarButtonItem141";
            // 
            // spreadsheetCommandBarButtonItem142
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem142, "spreadsheetCommandBarButtonItem142");
            this.spreadsheetCommandBarButtonItem142.CommandName = "FormatDefaultColumnWidth";
            this.spreadsheetCommandBarButtonItem142.Enabled = false;
            this.spreadsheetCommandBarButtonItem142.Id = 334;
            this.spreadsheetCommandBarButtonItem142.Name = "spreadsheetCommandBarButtonItem142";
            // 
            // spreadsheetCommandBarSubItem33
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem33, "spreadsheetCommandBarSubItem33");
            this.spreadsheetCommandBarSubItem33.CommandName = "HideAndUnhideCommandGroup";
            this.spreadsheetCommandBarSubItem33.Enabled = false;
            this.spreadsheetCommandBarSubItem33.Id = 341;
            this.spreadsheetCommandBarSubItem33.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem143),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem144),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem145),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem146),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem147),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem148)});
            this.spreadsheetCommandBarSubItem33.Name = "spreadsheetCommandBarSubItem33";
            // 
            // spreadsheetCommandBarButtonItem143
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem143, "spreadsheetCommandBarButtonItem143");
            this.spreadsheetCommandBarButtonItem143.CommandName = "HideRows";
            this.spreadsheetCommandBarButtonItem143.Enabled = false;
            this.spreadsheetCommandBarButtonItem143.Id = 335;
            this.spreadsheetCommandBarButtonItem143.Name = "spreadsheetCommandBarButtonItem143";
            // 
            // spreadsheetCommandBarButtonItem144
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem144, "spreadsheetCommandBarButtonItem144");
            this.spreadsheetCommandBarButtonItem144.CommandName = "HideColumns";
            this.spreadsheetCommandBarButtonItem144.Enabled = false;
            this.spreadsheetCommandBarButtonItem144.Id = 336;
            this.spreadsheetCommandBarButtonItem144.Name = "spreadsheetCommandBarButtonItem144";
            // 
            // spreadsheetCommandBarButtonItem145
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem145, "spreadsheetCommandBarButtonItem145");
            this.spreadsheetCommandBarButtonItem145.CommandName = "HideSheet";
            this.spreadsheetCommandBarButtonItem145.Enabled = false;
            this.spreadsheetCommandBarButtonItem145.Id = 337;
            this.spreadsheetCommandBarButtonItem145.Name = "spreadsheetCommandBarButtonItem145";
            // 
            // spreadsheetCommandBarButtonItem146
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem146, "spreadsheetCommandBarButtonItem146");
            this.spreadsheetCommandBarButtonItem146.CommandName = "UnhideRows";
            this.spreadsheetCommandBarButtonItem146.Enabled = false;
            this.spreadsheetCommandBarButtonItem146.Id = 338;
            this.spreadsheetCommandBarButtonItem146.Name = "spreadsheetCommandBarButtonItem146";
            // 
            // spreadsheetCommandBarButtonItem147
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem147, "spreadsheetCommandBarButtonItem147");
            this.spreadsheetCommandBarButtonItem147.CommandName = "UnhideColumns";
            this.spreadsheetCommandBarButtonItem147.Enabled = false;
            this.spreadsheetCommandBarButtonItem147.Id = 339;
            this.spreadsheetCommandBarButtonItem147.Name = "spreadsheetCommandBarButtonItem147";
            // 
            // spreadsheetCommandBarButtonItem148
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem148, "spreadsheetCommandBarButtonItem148");
            this.spreadsheetCommandBarButtonItem148.CommandName = "UnhideSheet";
            this.spreadsheetCommandBarButtonItem148.Enabled = false;
            this.spreadsheetCommandBarButtonItem148.Id = 340;
            this.spreadsheetCommandBarButtonItem148.Name = "spreadsheetCommandBarButtonItem148";
            // 
            // spreadsheetCommandBarButtonItem149
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem149, "spreadsheetCommandBarButtonItem149");
            this.spreadsheetCommandBarButtonItem149.CommandName = "RenameSheet";
            this.spreadsheetCommandBarButtonItem149.Enabled = false;
            this.spreadsheetCommandBarButtonItem149.Id = 342;
            this.spreadsheetCommandBarButtonItem149.Name = "spreadsheetCommandBarButtonItem149";
            // 
            // spreadsheetCommandBarButtonItem150
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem150, "spreadsheetCommandBarButtonItem150");
            this.spreadsheetCommandBarButtonItem150.CommandName = "MoveOrCopySheet";
            this.spreadsheetCommandBarButtonItem150.Enabled = false;
            this.spreadsheetCommandBarButtonItem150.Id = 343;
            this.spreadsheetCommandBarButtonItem150.Name = "spreadsheetCommandBarButtonItem150";
            // 
            // changeSheetTabColorItem1
            // 
            this.changeSheetTabColorItem1.ActAsDropDown = true;
            resources.ApplyResources(this.changeSheetTabColorItem1, "changeSheetTabColorItem1");
            this.changeSheetTabColorItem1.Enabled = false;
            this.changeSheetTabColorItem1.Id = 344;
            this.changeSheetTabColorItem1.Name = "changeSheetTabColorItem1";
            // 
            // spreadsheetCommandBarCheckItem40
            // 
            resources.ApplyResources(this.spreadsheetCommandBarCheckItem40, "spreadsheetCommandBarCheckItem40");
            this.spreadsheetCommandBarCheckItem40.CommandName = "FormatCellLocked";
            this.spreadsheetCommandBarCheckItem40.Enabled = false;
            this.spreadsheetCommandBarCheckItem40.Id = 345;
            this.spreadsheetCommandBarCheckItem40.Name = "spreadsheetCommandBarCheckItem40";
            // 
            // spreadsheetCommandBarButtonItem152
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem152, "spreadsheetCommandBarButtonItem152");
            this.spreadsheetCommandBarButtonItem152.CommandName = "FormatCellsContextMenuItem";
            this.spreadsheetCommandBarButtonItem152.Enabled = false;
            this.spreadsheetCommandBarButtonItem152.Id = 346;
            this.spreadsheetCommandBarButtonItem152.Name = "spreadsheetCommandBarButtonItem152";
            // 
            // spreadsheetCommandBarSubItem34
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem34, "spreadsheetCommandBarSubItem34");
            this.spreadsheetCommandBarSubItem34.CommandName = "EditingAutoSumCommandGroup";
            this.spreadsheetCommandBarSubItem34.Enabled = false;
            this.spreadsheetCommandBarSubItem34.Id = 347;
            this.spreadsheetCommandBarSubItem34.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem65),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem66),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem67),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem68),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem69)});
            this.spreadsheetCommandBarSubItem34.Name = "spreadsheetCommandBarSubItem34";
            this.spreadsheetCommandBarSubItem34.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // spreadsheetCommandBarSubItem35
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem35, "spreadsheetCommandBarSubItem35");
            this.spreadsheetCommandBarSubItem35.CommandName = "EditingFillCommandGroup";
            this.spreadsheetCommandBarSubItem35.Enabled = false;
            this.spreadsheetCommandBarSubItem35.Id = 348;
            this.spreadsheetCommandBarSubItem35.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem158),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem159),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem160),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem161)});
            this.spreadsheetCommandBarSubItem35.Name = "spreadsheetCommandBarSubItem35";
            this.spreadsheetCommandBarSubItem35.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // spreadsheetCommandBarButtonItem158
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem158, "spreadsheetCommandBarButtonItem158");
            this.spreadsheetCommandBarButtonItem158.CommandName = "EditingFillDown";
            this.spreadsheetCommandBarButtonItem158.Enabled = false;
            this.spreadsheetCommandBarButtonItem158.Id = 349;
            this.spreadsheetCommandBarButtonItem158.Name = "spreadsheetCommandBarButtonItem158";
            // 
            // spreadsheetCommandBarButtonItem159
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem159, "spreadsheetCommandBarButtonItem159");
            this.spreadsheetCommandBarButtonItem159.CommandName = "EditingFillRight";
            this.spreadsheetCommandBarButtonItem159.Enabled = false;
            this.spreadsheetCommandBarButtonItem159.Id = 350;
            this.spreadsheetCommandBarButtonItem159.Name = "spreadsheetCommandBarButtonItem159";
            // 
            // spreadsheetCommandBarButtonItem160
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem160, "spreadsheetCommandBarButtonItem160");
            this.spreadsheetCommandBarButtonItem160.CommandName = "EditingFillUp";
            this.spreadsheetCommandBarButtonItem160.Enabled = false;
            this.spreadsheetCommandBarButtonItem160.Id = 351;
            this.spreadsheetCommandBarButtonItem160.Name = "spreadsheetCommandBarButtonItem160";
            // 
            // spreadsheetCommandBarButtonItem161
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem161, "spreadsheetCommandBarButtonItem161");
            this.spreadsheetCommandBarButtonItem161.CommandName = "EditingFillLeft";
            this.spreadsheetCommandBarButtonItem161.Enabled = false;
            this.spreadsheetCommandBarButtonItem161.Id = 352;
            this.spreadsheetCommandBarButtonItem161.Name = "spreadsheetCommandBarButtonItem161";
            // 
            // spreadsheetCommandBarSubItem36
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem36, "spreadsheetCommandBarSubItem36");
            this.spreadsheetCommandBarSubItem36.CommandName = "FormatClearCommandGroup";
            this.spreadsheetCommandBarSubItem36.Enabled = false;
            this.spreadsheetCommandBarSubItem36.Id = 353;
            this.spreadsheetCommandBarSubItem36.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem162),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem163),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem164),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem165),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem166),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem167)});
            this.spreadsheetCommandBarSubItem36.Name = "spreadsheetCommandBarSubItem36";
            this.spreadsheetCommandBarSubItem36.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            // 
            // spreadsheetCommandBarButtonItem162
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem162, "spreadsheetCommandBarButtonItem162");
            this.spreadsheetCommandBarButtonItem162.CommandName = "FormatClearAll";
            this.spreadsheetCommandBarButtonItem162.Enabled = false;
            this.spreadsheetCommandBarButtonItem162.Id = 354;
            this.spreadsheetCommandBarButtonItem162.Name = "spreadsheetCommandBarButtonItem162";
            // 
            // spreadsheetCommandBarButtonItem163
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem163, "spreadsheetCommandBarButtonItem163");
            this.spreadsheetCommandBarButtonItem163.CommandName = "FormatClearFormats";
            this.spreadsheetCommandBarButtonItem163.Enabled = false;
            this.spreadsheetCommandBarButtonItem163.Id = 355;
            this.spreadsheetCommandBarButtonItem163.Name = "spreadsheetCommandBarButtonItem163";
            // 
            // spreadsheetCommandBarButtonItem164
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem164, "spreadsheetCommandBarButtonItem164");
            this.spreadsheetCommandBarButtonItem164.CommandName = "FormatClearContents";
            this.spreadsheetCommandBarButtonItem164.Enabled = false;
            this.spreadsheetCommandBarButtonItem164.Id = 356;
            this.spreadsheetCommandBarButtonItem164.Name = "spreadsheetCommandBarButtonItem164";
            // 
            // spreadsheetCommandBarButtonItem165
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem165, "spreadsheetCommandBarButtonItem165");
            this.spreadsheetCommandBarButtonItem165.CommandName = "FormatClearComments";
            this.spreadsheetCommandBarButtonItem165.Enabled = false;
            this.spreadsheetCommandBarButtonItem165.Id = 357;
            this.spreadsheetCommandBarButtonItem165.Name = "spreadsheetCommandBarButtonItem165";
            // 
            // spreadsheetCommandBarButtonItem166
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem166, "spreadsheetCommandBarButtonItem166");
            this.spreadsheetCommandBarButtonItem166.CommandName = "FormatClearHyperlinks";
            this.spreadsheetCommandBarButtonItem166.Enabled = false;
            this.spreadsheetCommandBarButtonItem166.Id = 358;
            this.spreadsheetCommandBarButtonItem166.Name = "spreadsheetCommandBarButtonItem166";
            // 
            // spreadsheetCommandBarButtonItem167
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem167, "spreadsheetCommandBarButtonItem167");
            this.spreadsheetCommandBarButtonItem167.CommandName = "FormatRemoveHyperlinks";
            this.spreadsheetCommandBarButtonItem167.Enabled = false;
            this.spreadsheetCommandBarButtonItem167.Id = 359;
            this.spreadsheetCommandBarButtonItem167.Name = "spreadsheetCommandBarButtonItem167";
            // 
            // spreadsheetCommandBarSubItem37
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem37, "spreadsheetCommandBarSubItem37");
            this.spreadsheetCommandBarSubItem37.CommandName = "EditingSortAndFilterCommandGroup";
            this.spreadsheetCommandBarSubItem37.Enabled = false;
            this.spreadsheetCommandBarSubItem37.Id = 360;
            this.spreadsheetCommandBarSubItem37.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem51),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem52),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarCheckItem17),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem53),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem54)});
            this.spreadsheetCommandBarSubItem37.Name = "spreadsheetCommandBarSubItem37";
            // 
            // spreadsheetCommandBarSubItem38
            // 
            resources.ApplyResources(this.spreadsheetCommandBarSubItem38, "spreadsheetCommandBarSubItem38");
            this.spreadsheetCommandBarSubItem38.CommandName = "EditingFindAndSelectCommandGroup";
            this.spreadsheetCommandBarSubItem38.Enabled = false;
            this.spreadsheetCommandBarSubItem38.Id = 361;
            this.spreadsheetCommandBarSubItem38.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem172),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem173),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem174),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem175),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem176),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem177),
            new DevExpress.XtraBars.LinkPersistInfo(this.spreadsheetCommandBarButtonItem178)});
            this.spreadsheetCommandBarSubItem38.Name = "spreadsheetCommandBarSubItem38";
            // 
            // spreadsheetCommandBarButtonItem172
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem172, "spreadsheetCommandBarButtonItem172");
            this.spreadsheetCommandBarButtonItem172.CommandName = "EditingFind";
            this.spreadsheetCommandBarButtonItem172.Enabled = false;
            this.spreadsheetCommandBarButtonItem172.Id = 362;
            this.spreadsheetCommandBarButtonItem172.Name = "spreadsheetCommandBarButtonItem172";
            // 
            // spreadsheetCommandBarButtonItem173
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem173, "spreadsheetCommandBarButtonItem173");
            this.spreadsheetCommandBarButtonItem173.CommandName = "EditingReplace";
            this.spreadsheetCommandBarButtonItem173.Enabled = false;
            this.spreadsheetCommandBarButtonItem173.Id = 363;
            this.spreadsheetCommandBarButtonItem173.Name = "spreadsheetCommandBarButtonItem173";
            // 
            // spreadsheetCommandBarButtonItem174
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem174, "spreadsheetCommandBarButtonItem174");
            this.spreadsheetCommandBarButtonItem174.CommandName = "EditingSelectFormulas";
            this.spreadsheetCommandBarButtonItem174.Enabled = false;
            this.spreadsheetCommandBarButtonItem174.Id = 364;
            this.spreadsheetCommandBarButtonItem174.Name = "spreadsheetCommandBarButtonItem174";
            // 
            // spreadsheetCommandBarButtonItem175
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem175, "spreadsheetCommandBarButtonItem175");
            this.spreadsheetCommandBarButtonItem175.CommandName = "EditingSelectComments";
            this.spreadsheetCommandBarButtonItem175.Enabled = false;
            this.spreadsheetCommandBarButtonItem175.Id = 365;
            this.spreadsheetCommandBarButtonItem175.Name = "spreadsheetCommandBarButtonItem175";
            // 
            // spreadsheetCommandBarButtonItem176
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem176, "spreadsheetCommandBarButtonItem176");
            this.spreadsheetCommandBarButtonItem176.CommandName = "EditingSelectConditionalFormatting";
            this.spreadsheetCommandBarButtonItem176.Enabled = false;
            this.spreadsheetCommandBarButtonItem176.Id = 366;
            this.spreadsheetCommandBarButtonItem176.Name = "spreadsheetCommandBarButtonItem176";
            // 
            // spreadsheetCommandBarButtonItem177
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem177, "spreadsheetCommandBarButtonItem177");
            this.spreadsheetCommandBarButtonItem177.CommandName = "EditingSelectConstants";
            this.spreadsheetCommandBarButtonItem177.Enabled = false;
            this.spreadsheetCommandBarButtonItem177.Id = 367;
            this.spreadsheetCommandBarButtonItem177.Name = "spreadsheetCommandBarButtonItem177";
            // 
            // spreadsheetCommandBarButtonItem178
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem178, "spreadsheetCommandBarButtonItem178");
            this.spreadsheetCommandBarButtonItem178.CommandName = "EditingSelectDataValidation";
            this.spreadsheetCommandBarButtonItem178.Enabled = false;
            this.spreadsheetCommandBarButtonItem178.Id = 368;
            this.spreadsheetCommandBarButtonItem178.Name = "spreadsheetCommandBarButtonItem178";
            // 
            // spreadsheetCommandBarButtonItem179
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem179, "spreadsheetCommandBarButtonItem179");
            this.spreadsheetCommandBarButtonItem179.CommandName = "FileNew";
            this.spreadsheetCommandBarButtonItem179.Enabled = false;
            this.spreadsheetCommandBarButtonItem179.Id = 369;
            this.spreadsheetCommandBarButtonItem179.Name = "spreadsheetCommandBarButtonItem179";
            // 
            // spreadsheetCommandBarButtonItem180
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem180, "spreadsheetCommandBarButtonItem180");
            this.spreadsheetCommandBarButtonItem180.CommandName = "FileOpen";
            this.spreadsheetCommandBarButtonItem180.Enabled = false;
            this.spreadsheetCommandBarButtonItem180.Id = 370;
            this.spreadsheetCommandBarButtonItem180.Name = "spreadsheetCommandBarButtonItem180";
            // 
            // spreadsheetCommandBarButtonItem181
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem181, "spreadsheetCommandBarButtonItem181");
            this.spreadsheetCommandBarButtonItem181.CommandName = "FileSave";
            this.spreadsheetCommandBarButtonItem181.Enabled = false;
            this.spreadsheetCommandBarButtonItem181.Id = 371;
            this.spreadsheetCommandBarButtonItem181.Name = "spreadsheetCommandBarButtonItem181";
            // 
            // spreadsheetCommandBarButtonItem182
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem182, "spreadsheetCommandBarButtonItem182");
            this.spreadsheetCommandBarButtonItem182.CommandName = "FileSaveAs";
            this.spreadsheetCommandBarButtonItem182.Enabled = false;
            this.spreadsheetCommandBarButtonItem182.Id = 372;
            this.spreadsheetCommandBarButtonItem182.Name = "spreadsheetCommandBarButtonItem182";
            // 
            // spreadsheetCommandBarButtonItem183
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem183, "spreadsheetCommandBarButtonItem183");
            this.spreadsheetCommandBarButtonItem183.CommandName = "FileQuickPrint";
            this.spreadsheetCommandBarButtonItem183.Enabled = false;
            this.spreadsheetCommandBarButtonItem183.Id = 373;
            this.spreadsheetCommandBarButtonItem183.Name = "spreadsheetCommandBarButtonItem183";
            // 
            // spreadsheetCommandBarButtonItem184
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem184, "spreadsheetCommandBarButtonItem184");
            this.spreadsheetCommandBarButtonItem184.CommandName = "FilePrint";
            this.spreadsheetCommandBarButtonItem184.Enabled = false;
            this.spreadsheetCommandBarButtonItem184.Id = 374;
            this.spreadsheetCommandBarButtonItem184.Name = "spreadsheetCommandBarButtonItem184";
            // 
            // spreadsheetCommandBarButtonItem185
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem185, "spreadsheetCommandBarButtonItem185");
            this.spreadsheetCommandBarButtonItem185.CommandName = "FilePrintPreview";
            this.spreadsheetCommandBarButtonItem185.Enabled = false;
            this.spreadsheetCommandBarButtonItem185.Id = 375;
            this.spreadsheetCommandBarButtonItem185.Name = "spreadsheetCommandBarButtonItem185";
            // 
            // spreadsheetCommandBarButtonItem186
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem186, "spreadsheetCommandBarButtonItem186");
            this.spreadsheetCommandBarButtonItem186.CommandName = "FileUndo";
            this.spreadsheetCommandBarButtonItem186.Enabled = false;
            this.spreadsheetCommandBarButtonItem186.Id = 376;
            this.spreadsheetCommandBarButtonItem186.Name = "spreadsheetCommandBarButtonItem186";
            // 
            // spreadsheetCommandBarButtonItem187
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem187, "spreadsheetCommandBarButtonItem187");
            this.spreadsheetCommandBarButtonItem187.CommandName = "FileRedo";
            this.spreadsheetCommandBarButtonItem187.Enabled = false;
            this.spreadsheetCommandBarButtonItem187.Id = 377;
            this.spreadsheetCommandBarButtonItem187.Name = "spreadsheetCommandBarButtonItem187";
            // 
            // spreadsheetCommandBarButtonItem188
            // 
            resources.ApplyResources(this.spreadsheetCommandBarButtonItem188, "spreadsheetCommandBarButtonItem188");
            this.spreadsheetCommandBarButtonItem188.CommandName = "FileShowDocumentProperties";
            this.spreadsheetCommandBarButtonItem188.Enabled = false;
            this.spreadsheetCommandBarButtonItem188.Id = 378;
            this.spreadsheetCommandBarButtonItem188.Name = "spreadsheetCommandBarButtonItem188";
            // 
            // barButtonItem1
            // 
            resources.ApplyResources(this.barButtonItem1, "barButtonItem1");
            this.barButtonItem1.Id = 380;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // simulateToolStripMenuItem
            // 
            resources.ApplyResources(this.simulateToolStripMenuItem, "simulateToolStripMenuItem");
            this.simulateToolStripMenuItem.Id = 386;
            this.simulateToolStripMenuItem.Name = "simulateToolStripMenuItem";
            this.simulateToolStripMenuItem.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItem1_CheckedChanged);
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Id = 393;
            this.barButtonItem7.Name = "barButtonItem7";
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Id = 394;
            this.barButtonItem8.Name = "barButtonItem8";
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Id = 395;
            this.barButtonItem9.Name = "barButtonItem9";
            // 
            // barCheckItem1
            // 
            this.barCheckItem1.Id = 397;
            this.barCheckItem1.Name = "barCheckItem1";
            // 
            // skinRibbonGalleryBarItem1
            // 
            this.skinRibbonGalleryBarItem1.Id = 398;
            this.skinRibbonGalleryBarItem1.Name = "skinRibbonGalleryBarItem1";
            // 
            // barButtonItem6
            // 
            resources.ApplyResources(this.barButtonItem6, "barButtonItem6");
            this.barButtonItem6.Id = 406;
            this.barButtonItem6.Name = "barButtonItem6";
            // 
            // barButtonItem10
            // 
            this.barButtonItem10.Id = 407;
            this.barButtonItem10.Name = "barButtonItem10";
            // 
            // barButtonItem11
            // 
            this.barButtonItem11.Id = 408;
            this.barButtonItem11.Name = "barButtonItem11";
            // 
            // barButtonItem12
            // 
            this.barButtonItem12.Id = 409;
            this.barButtonItem12.Name = "barButtonItem12";
            // 
            // barButtonItem13
            // 
            this.barButtonItem13.Id = 410;
            this.barButtonItem13.Name = "barButtonItem13";
            // 
            // barButtonItem14
            // 
            this.barButtonItem14.Id = 411;
            this.barButtonItem14.Name = "barButtonItem14";
            // 
            // barButtonItem15
            // 
            this.barButtonItem15.Id = 412;
            this.barButtonItem15.Name = "barButtonItem15";
            // 
            // barButtonItem16
            // 
            this.barButtonItem16.Id = 413;
            this.barButtonItem16.Name = "barButtonItem16";
            // 
            // barButtonItem17
            // 
            this.barButtonItem17.Id = 414;
            this.barButtonItem17.Name = "barButtonItem17";
            // 
            // barButtonItem18
            // 
            this.barButtonItem18.Id = 415;
            this.barButtonItem18.Name = "barButtonItem18";
            // 
            // barButtonItem19
            // 
            this.barButtonItem19.Id = 416;
            this.barButtonItem19.Name = "barButtonItem19";
            // 
            // barCheckItem2
            // 
            this.barCheckItem2.Id = 417;
            this.barCheckItem2.Name = "barCheckItem2";
            // 
            // barButtonItem20
            // 
            this.barButtonItem20.Id = 418;
            this.barButtonItem20.Name = "barButtonItem20";
            // 
            // barButtonItem21
            // 
            this.barButtonItem21.Id = 419;
            this.barButtonItem21.Name = "barButtonItem21";
            // 
            // barButtonItem22
            // 
            this.barButtonItem22.Id = 420;
            this.barButtonItem22.Name = "barButtonItem22";
            // 
            // barButtonItem23
            // 
            this.barButtonItem23.Id = 421;
            this.barButtonItem23.Name = "barButtonItem23";
            // 
            // barButtonItem24
            // 
            this.barButtonItem24.Id = 422;
            this.barButtonItem24.Name = "barButtonItem24";
            // 
            // barButtonItem25
            // 
            this.barButtonItem25.Id = 423;
            this.barButtonItem25.Name = "barButtonItem25";
            // 
            // barButtonItem26
            // 
            this.barButtonItem26.Id = 425;
            this.barButtonItem26.Name = "barButtonItem26";
            // 
            // barCheckItem3
            // 
            this.barCheckItem3.Id = 426;
            this.barCheckItem3.Name = "barCheckItem3";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Id = 427;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // DiffuseB
            // 
            resources.ApplyResources(this.DiffuseB, "DiffuseB");
            this.DiffuseB.Id = 428;
            this.DiffuseB.Name = "DiffuseB";
            this.DiffuseB.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.DiffuseB_ItemClick);
            // 
            // SpecularB
            // 
            resources.ApplyResources(this.SpecularB, "SpecularB");
            this.SpecularB.Id = 429;
            this.SpecularB.Name = "SpecularB";
            this.SpecularB.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.SpecularB_ItemClick);
            // 
            // NormalsB
            // 
            resources.ApplyResources(this.NormalsB, "NormalsB");
            this.NormalsB.Id = 430;
            this.NormalsB.Name = "NormalsB";
            this.NormalsB.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.NormalsB_ItemClick);
            // 
            // WireframeB
            // 
            resources.ApplyResources(this.WireframeB, "WireframeB");
            this.WireframeB.Id = 431;
            this.WireframeB.Name = "WireframeB";
            this.WireframeB.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.WireframeB_CheckedChanged);
            // 
            // PositionsB
            // 
            resources.ApplyResources(this.PositionsB, "PositionsB");
            this.PositionsB.Id = 432;
            this.PositionsB.Name = "PositionsB";
            this.PositionsB.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.PositionsB_ItemClick);
            // 
            // roughnessB
            // 
            resources.ApplyResources(this.roughnessB, "roughnessB");
            this.roughnessB.Id = 433;
            this.roughnessB.Name = "roughnessB";
            this.roughnessB.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.roughnessB_ItemClick);
            // 
            // SpecularPowerB
            // 
            resources.ApplyResources(this.SpecularPowerB, "SpecularPowerB");
            this.SpecularPowerB.Id = 434;
            this.SpecularPowerB.Name = "SpecularPowerB";
            this.SpecularPowerB.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.SpecularPowerB_ItemClick);
            // 
            // FinalLightingB
            // 
            resources.ApplyResources(this.FinalLightingB, "FinalLightingB");
            this.FinalLightingB.Id = 435;
            this.FinalLightingB.Name = "FinalLightingB";
            this.FinalLightingB.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.FinalLightingB_ItemClick);
            // 
            // SoundMixEdB
            // 
            resources.ApplyResources(this.SoundMixEdB, "SoundMixEdB");
            this.SoundMixEdB.Id = 439;
            this.SoundMixEdB.Name = "SoundMixEdB";
            this.SoundMixEdB.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.SoundMixB_ItemClick);
            // 
            // MatEditorB
            // 
            resources.ApplyResources(this.MatEditorB, "MatEditorB");
            this.MatEditorB.Id = 440;
            this.MatEditorB.Name = "MatEditorB";
            this.MatEditorB.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.MatEditorB_ItemClick);
            // 
            // LogicEditorB
            // 
            resources.ApplyResources(this.LogicEditorB, "LogicEditorB");
            this.LogicEditorB.Id = 441;
            this.LogicEditorB.Name = "LogicEditorB";
            this.LogicEditorB.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.LogicEditorB_ItemClick);
            // 
            // LogicEditorB1
            // 
            resources.ApplyResources(this.LogicEditorB1, "LogicEditorB1");
            this.LogicEditorB1.Id = 442;
            this.LogicEditorB1.Name = "LogicEditorB1";
            this.LogicEditorB1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.LogicEditorB1_ItemClick);
            // 
            // ContBrButton
            // 
            resources.ApplyResources(this.ContBrButton, "ContBrButton");
            this.ContBrButton.Id = 443;
            this.ContBrButton.Name = "ContBrButton";
            this.ContBrButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ContBrButton_ItemClick);
            // 
            // SelectObjsB
            // 
            resources.ApplyResources(this.SelectObjsB, "SelectObjsB");
            this.SelectObjsB.Id = 444;
            this.SelectObjsB.Name = "SelectObjsB";
            this.SelectObjsB.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.SelectObjsB_ItemClick);
            // 
            // MakeScrs_B
            // 
            resources.ApplyResources(this.MakeScrs_B, "MakeScrs_B");
            this.MakeScrs_B.Id = 445;
            this.MakeScrs_B.Name = "MakeScrs_B";
            this.MakeScrs_B.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.MakeScrs_B_ItemClick);
            // 
            // calcGILightmaps
            // 
            this.calcGILightmaps.Id = 494;
            this.calcGILightmaps.Name = "calcGILightmaps";
            // 
            // SelectButton
            // 
            this.SelectButton.Id = 495;
            this.SelectButton.Name = "SelectButton";
            // 
            // ScaleB
            // 
            this.ScaleB.Id = 496;
            this.ScaleB.Name = "ScaleB";
            // 
            // RotateB
            // 
            this.RotateB.Id = 497;
            this.RotateB.Name = "RotateB";
            // 
            // TranslateB
            // 
            this.TranslateB.Id = 498;
            this.TranslateB.Name = "TranslateB";
            // 
            // AutoSaveItem
            // 
            this.AutoSaveItem.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.AutoSaveItem.BindableChecked = true;
            resources.ApplyResources(this.AutoSaveItem, "AutoSaveItem");
            this.AutoSaveItem.Checked = true;
            this.AutoSaveItem.Id = 454;
            this.AutoSaveItem.Name = "AutoSaveItem";
            this.AutoSaveItem.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.AutoSaveItem_CheckedChanged);
            // 
            // GameFolderItem
            // 
            this.GameFolderItem.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            resources.ApplyResources(this.GameFolderItem, "GameFolderItem");
            this.GameFolderItem.Id = 455;
            this.GameFolderItem.Name = "GameFolderItem";
            // 
            // GameNameItem
            // 
            this.GameNameItem.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            resources.ApplyResources(this.GameNameItem, "GameNameItem");
            this.GameNameItem.Id = 456;
            this.GameNameItem.Name = "GameNameItem";
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            resources.ApplyResources(this.barStaticItem2, "barStaticItem2");
            this.barStaticItem2.Id = 457;
            this.barStaticItem2.Name = "barStaticItem2";
            // 
            // RenderDepthB
            // 
            resources.ApplyResources(this.RenderDepthB, "RenderDepthB");
            this.RenderDepthB.Id = 458;
            this.RenderDepthB.Name = "RenderDepthB";
            this.RenderDepthB.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.RenderDepthB_ItemClick);
            // 
            // barButtonItem2
            // 
            resources.ApplyResources(this.barButtonItem2, "barButtonItem2");
            this.barButtonItem2.Id = 459;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItem27
            // 
            resources.ApplyResources(this.barButtonItem27, "barButtonItem27");
            this.barButtonItem27.Id = 460;
            this.barButtonItem27.Name = "barButtonItem27";
            this.barButtonItem27.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem27_ItemClick);
            // 
            // PopUp_Add_Actor
            // 
            resources.ApplyResources(this.PopUp_Add_Actor, "PopUp_Add_Actor");
            this.PopUp_Add_Actor.Id = 462;
            this.PopUp_Add_Actor.Name = "PopUp_Add_Actor";
            // 
            // MaterialsButtonItem
            // 
            resources.ApplyResources(this.MaterialsButtonItem, "MaterialsButtonItem");
            this.MaterialsButtonItem.Id = 463;
            this.MaterialsButtonItem.Name = "MaterialsButtonItem";
            // 
            // ShadersButtonItem
            // 
            resources.ApplyResources(this.ShadersButtonItem, "ShadersButtonItem");
            this.ShadersButtonItem.Id = 464;
            this.ShadersButtonItem.Name = "ShadersButtonItem";
            this.ShadersButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ShadersButtonItem_ItemClick);
            // 
            // MaterialsBRDFVisB
            // 
            resources.ApplyResources(this.MaterialsBRDFVisB, "MaterialsBRDFVisB");
            this.MaterialsBRDFVisB.Id = 466;
            this.MaterialsBRDFVisB.Name = "MaterialsBRDFVisB";
            this.MaterialsBRDFVisB.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.MaterialsBRDFVisB_CheckedChanged);
            // 
            // RenderDoc
            // 
            resources.ApplyResources(this.RenderDoc, "RenderDoc");
            this.RenderDoc.Id = 467;
            this.RenderDoc.Name = "RenderDoc";
            this.RenderDoc.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.RenderDoc_ItemClick);
            // 
            // ScriptDebugger
            // 
            resources.ApplyResources(this.ScriptDebugger, "ScriptDebugger");
            this.ScriptDebugger.Id = 468;
            this.ScriptDebugger.Name = "ScriptDebugger";
            this.ScriptDebugger.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.ScriptDebugger_ItemClick);
            // 
            // Brofiler
            // 
            resources.ApplyResources(this.Brofiler, "Brofiler");
            this.Brofiler.Id = 469;
            this.Brofiler.Name = "Brofiler";
            this.Brofiler.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.Brofiler_ItemClick);
            // 
            // barSubItem3
            // 
            resources.ApplyResources(this.barSubItem3, "barSubItem3");
            this.barSubItem3.Id = 488;
            this.barSubItem3.Name = "barSubItem3";
            this.barSubItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barSubItem3_ItemClick);
            // 
            // calccollisions
            // 
            resources.ApplyResources(this.calccollisions, "calccollisions");
            this.calccollisions.Id = 490;
            this.calccollisions.Name = "calccollisions";
            this.calccollisions.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.calccollisions_ItemClick);
            // 
            // ribbonImageCollectionLarge
            // 
            resources.ApplyResources(this.ribbonImageCollectionLarge, "ribbonImageCollectionLarge");
            this.ribbonImageCollectionLarge.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("ribbonImageCollectionLarge.ImageStream")));
            this.ribbonImageCollectionLarge.Images.SetKeyName(0, "Ribbon_Exit_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(1, "Ribbon_Content_32x32.png");
            this.ribbonImageCollectionLarge.Images.SetKeyName(2, "Ribbon_Info_32x32.png");
            // 
            // MainPage
            // 
            this.MainPage.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.EditorsPG,
            this.ToolsPageGroup,
            this.ribbonPageGroup4});
            this.MainPage.Name = "MainPage";
            resources.ApplyResources(this.MainPage, "MainPage");
            // 
            // EditorsPG
            // 
            this.EditorsPG.ItemLinks.Add(this.SoundMixEdB);
            this.EditorsPG.ItemLinks.Add(this.MatEditorB);
            this.EditorsPG.ItemLinks.Add(this.LogicEditorB);
            this.EditorsPG.ItemLinks.Add(this.LogicEditorB1);
            this.EditorsPG.ItemLinks.Add(this.ContBrButton);
            this.EditorsPG.ItemLinks.Add(this.SelectObjsB);
            this.EditorsPG.Name = "EditorsPG";
            resources.ApplyResources(this.EditorsPG, "EditorsPG");
            // 
            // ToolsPageGroup
            // 
            this.ToolsPageGroup.ItemLinks.Add(this.MakeScrs_B);
            this.ToolsPageGroup.Name = "ToolsPageGroup";
            resources.ApplyResources(this.ToolsPageGroup, "ToolsPageGroup");
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.barSubItem3);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            resources.ApplyResources(this.ribbonPageGroup4, "ribbonPageGroup4");
            // 
            // DebugPage
            // 
            this.DebugPage.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.RenderPG,
            this.RefreshPageGroup,
            this.DeveloperToolsGroup});
            this.DebugPage.Name = "DebugPage";
            resources.ApplyResources(this.DebugPage, "DebugPage");
            // 
            // RenderPG
            // 
            this.RenderPG.ItemLinks.Add(this.DiffuseB);
            this.RenderPG.ItemLinks.Add(this.NormalsB);
            this.RenderPG.ItemLinks.Add(this.WireframeB);
            this.RenderPG.ItemLinks.Add(this.PositionsB);
            this.RenderPG.ItemLinks.Add(this.roughnessB);
            this.RenderPG.ItemLinks.Add(this.SpecularPowerB);
            this.RenderPG.ItemLinks.Add(this.SpecularB);
            this.RenderPG.ItemLinks.Add(this.RenderDepthB);
            this.RenderPG.ItemLinks.Add(this.FinalLightingB);
            this.RenderPG.ItemLinks.Add(this.MaterialsBRDFVisB);
            this.RenderPG.Name = "RenderPG";
            resources.ApplyResources(this.RenderPG, "RenderPG");
            // 
            // RefreshPageGroup
            // 
            this.RefreshPageGroup.ItemLinks.Add(this.MaterialsButtonItem);
            this.RefreshPageGroup.ItemLinks.Add(this.ShadersButtonItem);
            this.RefreshPageGroup.Name = "RefreshPageGroup";
            resources.ApplyResources(this.RefreshPageGroup, "RefreshPageGroup");
            // 
            // DeveloperToolsGroup
            // 
            this.DeveloperToolsGroup.ItemLinks.Add(this.RenderDoc);
            this.DeveloperToolsGroup.ItemLinks.Add(this.ScriptDebugger);
            this.DeveloperToolsGroup.ItemLinks.Add(this.Brofiler);
            this.DeveloperToolsGroup.Name = "DeveloperToolsGroup";
            resources.ApplyResources(this.DeveloperToolsGroup, "DeveloperToolsGroup");
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2,
            this.LevelBuilding});
            this.ribbonPage1.Name = "ribbonPage1";
            resources.ApplyResources(this.ribbonPage1, "ribbonPage1");
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem1);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            resources.ApplyResources(this.ribbonPageGroup1, "ribbonPageGroup1");
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.simulateToolStripMenuItem);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            resources.ApplyResources(this.ribbonPageGroup2, "ribbonPageGroup2");
            // 
            // LevelBuilding
            // 
            this.LevelBuilding.ItemLinks.Add(this.calccollisions);
            this.LevelBuilding.Name = "LevelBuilding";
            resources.ApplyResources(this.LevelBuilding, "LevelBuilding");
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup3});
            this.ribbonPage2.Name = "ribbonPage2";
            resources.ApplyResources(this.ribbonPage2, "ribbonPage2");
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem2);
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem27);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            resources.ApplyResources(this.ribbonPageGroup3, "ribbonPageGroup3");
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.siStatus);
            this.ribbonStatusBar.ItemLinks.Add(this.selectedObjCapt);
            this.ribbonStatusBar.ItemLinks.Add(this.GameFolderItem);
            this.ribbonStatusBar.ItemLinks.Add(this.GameNameItem);
            this.ribbonStatusBar.ItemLinks.Add(this.barStaticItem2);
            this.ribbonStatusBar.ItemLinks.Add(this.AutoSaveItem);
            resources.ApplyResources(this.ribbonStatusBar, "ribbonStatusBar");
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbonControl;
            // 
            // siStatus
            // 
            resources.ApplyResources(this.siStatus, "siStatus");
            this.siStatus.Id = 31;
            this.siStatus.Name = "siStatus";
            // 
            // selectedObjCapt
            // 
            resources.ApplyResources(this.selectedObjCapt, "selectedObjCapt");
            this.selectedObjCapt.Id = 32;
            this.selectedObjCapt.Name = "selectedObjCapt";
            // 
            // barButtonItem3
            // 
            resources.ApplyResources(this.barButtonItem3, "barButtonItem3");
            this.barButtonItem3.Id = 399;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Black";
            // 
            // autosaveTimer
            // 
            this.autosaveTimer.Enabled = true;
            this.autosaveTimer.Interval = 600000;
            this.autosaveTimer.Tick += new System.EventHandler(this.autosaveTimer_Tick);
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.HiddenPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel4});
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel2,
            this.dockPanel3});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"});
            // 
            // dockPanel4
            // 
            this.dockPanel4.Controls.Add(this.dockPanel4_Container);
            this.dockPanel4.Dock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this.dockPanel4.ID = new System.Guid("226c02f3-be24-4059-8e3e-387de2eabfad");
            resources.ApplyResources(this.dockPanel4, "dockPanel4");
            this.dockPanel4.Name = "dockPanel4";
            this.dockPanel4.OriginalSize = new System.Drawing.Size(200, 200);
            this.dockPanel4.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this.dockPanel4.SavedIndex = 2;
            this.dockPanel4.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            // 
            // dockPanel4_Container
            // 
            resources.ApplyResources(this.dockPanel4_Container, "dockPanel4_Container");
            this.dockPanel4_Container.Name = "dockPanel4_Container";
            // 
            // dockPanel2
            // 
            this.dockPanel2.Controls.Add(this.dockPanel2_Container);
            this.dockPanel2.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel2.ID = new System.Guid("15bb7992-5e45-4df4-ba76-dd9d1fa838bd");
            resources.ApplyResources(this.dockPanel2, "dockPanel2");
            this.dockPanel2.Name = "dockPanel2";
            this.dockPanel2.OriginalSize = new System.Drawing.Size(200, 200);
            // 
            // dockPanel2_Container
            // 
            resources.ApplyResources(this.dockPanel2_Container, "dockPanel2_Container");
            this.dockPanel2_Container.Name = "dockPanel2_Container";
            // 
            // dockPanel3
            // 
            resources.ApplyResources(this.dockPanel3, "dockPanel3");
            this.dockPanel3.Controls.Add(this.dockPanel3_Container);
            this.dockPanel3.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanel3.ID = new System.Guid("75b1a570-c17e-4e0b-8b0f-9899f019d7f5");
            this.dockPanel3.Name = "dockPanel3";
            this.dockPanel3.OriginalSize = new System.Drawing.Size(467, 200);
            // 
            // dockPanel3_Container
            // 
            resources.ApplyResources(this.dockPanel3_Container, "dockPanel3_Container");
            this.dockPanel3_Container.Name = "dockPanel3_Container";
            // 
            // popupMenu1
            // 
            this.popupMenu1.ItemLinks.Add(this.PopUp_Add_Actor);
            this.popupMenu1.Name = "popupMenu1";
            this.popupMenu1.Ribbon = this.ribbonControl;
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.dockPanel1);
            this.Controls.Add(this.dockPanel3);
            this.Controls.Add(this.dockPanel2);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbonControl);
            this.Name = "MainForm";
            this.Ribbon = this.ribbonControl;
            this.StatusBar = this.ribbonStatusBar;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResizeBegin += new System.EventHandler(this.MainForm_ResizeBegin);
            this.ResizeEnd += new System.EventHandler(this.MainForm_ResizeEnd);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonImageCollectionLarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel4.ResumeLayout(false);
            this.dockPanel2.ResumeLayout(false);
            this.dockPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private WeifenLuo.WinFormsUI.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl;
        private DevExpress.Utils.ImageCollection ribbonImageCollection;
        private DevExpress.XtraBars.BarButtonItem iExit;
        private DevExpress.XtraBars.BarButtonItem iAbout;
        private DevExpress.XtraBars.RibbonGalleryBarItem rgbiSkins;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem1;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem2;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem3;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem4;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem1;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem5;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem6;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem7;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem2;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem8;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem9;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem3;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem10;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem11;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem12;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem13;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem1;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem2;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem3;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem4;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem14;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem15;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem16;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem5;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem17;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem18;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem19;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem20;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem6;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem21;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem22;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem23;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem24;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem25;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem7;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem26;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem27;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem4;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem5;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem6;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem7;
        private DevExpress.XtraSpreadsheet.UI.GalleryPivotStylesItem galleryPivotStylesItem1;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem8;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem28;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem29;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem9;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem30;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem31;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraSpreadsheet.UI.RenameTableItem renameTableItem1;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem8;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem9;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem10;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem11;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem12;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem13;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem14;
        private DevExpress.XtraSpreadsheet.UI.GalleryTableStylesItem galleryTableStylesItem1;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem32;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem33;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem34;
        private DevExpress.XtraSpreadsheet.UI.GalleryChartLayoutItem galleryChartLayoutItem1;
        private DevExpress.XtraSpreadsheet.UI.GalleryChartStyleItem galleryChartStyleItem1;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem spreadsheetCommandBarButtonGalleryDropDownItem1;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem10;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem spreadsheetCommandBarButtonGalleryDropDownItem2;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem spreadsheetCommandBarButtonGalleryDropDownItem3;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem spreadsheetCommandBarButtonGalleryDropDownItem4;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem spreadsheetCommandBarButtonGalleryDropDownItem5;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem11;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem spreadsheetCommandBarButtonGalleryDropDownItem6;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem spreadsheetCommandBarButtonGalleryDropDownItem7;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem12;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem spreadsheetCommandBarButtonGalleryDropDownItem8;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem spreadsheetCommandBarButtonGalleryDropDownItem9;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem spreadsheetCommandBarButtonGalleryDropDownItem10;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem spreadsheetCommandBarButtonGalleryDropDownItem11;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem spreadsheetCommandBarButtonGalleryDropDownItem12;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem15;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem16;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem35;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem36;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem37;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem13;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem38;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem39;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem40;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem41;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem42;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem43;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem44;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem45;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem46;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem47;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem48;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem49;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem50;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem51;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem52;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem17;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem53;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem54;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem14;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem55;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem56;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem57;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem15;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem58;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem59;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem16;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem60;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem61;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem62;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem63;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem64;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem17;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem65;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem66;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem67;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem68;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem69;
        private DevExpress.XtraSpreadsheet.UI.FunctionsFinancialItem functionsFinancialItem1;
        private DevExpress.XtraSpreadsheet.UI.FunctionsLogicalItem functionsLogicalItem1;
        private DevExpress.XtraSpreadsheet.UI.FunctionsTextItem functionsTextItem1;
        private DevExpress.XtraSpreadsheet.UI.FunctionsDateAndTimeItem functionsDateAndTimeItem1;
        private DevExpress.XtraSpreadsheet.UI.FunctionsLookupAndReferenceItem functionsLookupAndReferenceItem1;
        private DevExpress.XtraSpreadsheet.UI.FunctionsMathAndTrigonometryItem functionsMathAndTrigonometryItem1;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem18;
        private DevExpress.XtraSpreadsheet.UI.FunctionsStatisticalItem functionsStatisticalItem1;
        private DevExpress.XtraSpreadsheet.UI.FunctionsEngineeringItem functionsEngineeringItem1;
        private DevExpress.XtraSpreadsheet.UI.FunctionsInformationItem functionsInformationItem1;
        private DevExpress.XtraSpreadsheet.UI.FunctionsCompatibilityItem functionsCompatibilityItem1;
        private DevExpress.XtraSpreadsheet.UI.FunctionsWebItem functionsWebItem1;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem70;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem71;
        private DevExpress.XtraSpreadsheet.UI.DefinedNameListItem definedNameListItem1;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem72;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem18;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem19;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem19;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem20;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem73;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem74;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem20;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem21;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem22;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem23;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem75;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem21;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem24;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem25;
        private DevExpress.XtraSpreadsheet.UI.PageSetupPaperKindItem pageSetupPaperKindItem1;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem22;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem76;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem77;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem78;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem26;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem27;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem79;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem80;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem81;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem spreadsheetCommandBarButtonGalleryDropDownItem13;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem spreadsheetCommandBarButtonGalleryDropDownItem14;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem spreadsheetCommandBarButtonGalleryDropDownItem15;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem spreadsheetCommandBarButtonGalleryDropDownItem16;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem spreadsheetCommandBarButtonGalleryDropDownItem17;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem spreadsheetCommandBarButtonGalleryDropDownItem18;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem spreadsheetCommandBarButtonGalleryDropDownItem19;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem82;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem83;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem84;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem85;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem86;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem87;
        private DevExpress.XtraBars.BarButtonGroup barButtonGroup1;
        private DevExpress.XtraSpreadsheet.UI.ChangeFontNameItem changeFontNameItem1;
        private DevExpress.XtraSpreadsheet.UI.ChangeFontSizeItem changeFontSizeItem1;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem88;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem89;
        private DevExpress.XtraBars.BarButtonGroup barButtonGroup2;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem28;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem29;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem30;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem31;
        private DevExpress.XtraBars.BarButtonGroup barButtonGroup3;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem23;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem90;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem91;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem92;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem93;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem94;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem95;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem96;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem97;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem98;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem99;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem100;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem101;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem102;
        private DevExpress.XtraSpreadsheet.UI.ChangeBorderLineColorItem changeBorderLineColorItem1;
        private DevExpress.XtraSpreadsheet.UI.ChangeBorderLineStyleItem changeBorderLineStyleItem1;
        private DevExpress.XtraBars.BarButtonGroup barButtonGroup4;
        private DevExpress.XtraSpreadsheet.UI.ChangeCellFillColorItem changeCellFillColorItem1;
        private DevExpress.XtraSpreadsheet.UI.ChangeFontColorItem changeFontColorItem1;
        private DevExpress.XtraBars.BarButtonGroup barButtonGroup5;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem32;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem33;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem34;
        private DevExpress.XtraBars.BarButtonGroup barButtonGroup6;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem35;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem36;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem37;
        private DevExpress.XtraBars.BarButtonGroup barButtonGroup7;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem103;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem104;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem38;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem24;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem39;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem105;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem106;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem107;
        private DevExpress.XtraBars.BarButtonGroup barButtonGroup8;
        private DevExpress.XtraSpreadsheet.UI.ChangeNumberFormatItem changeNumberFormatItem1;
        private DevExpress.XtraBars.BarButtonGroup barButtonGroup9;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem25;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem108;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem109;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem110;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem111;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem112;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem113;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem114;
        private DevExpress.XtraBars.BarButtonGroup barButtonGroup10;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem115;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem116;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem26;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem27;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem117;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem118;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem119;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem120;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem121;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem122;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem123;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem28;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem124;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem125;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem126;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem127;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem128;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem129;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem spreadsheetCommandBarButtonGalleryDropDownItem20;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem spreadsheetCommandBarButtonGalleryDropDownItem21;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonGalleryDropDownItem spreadsheetCommandBarButtonGalleryDropDownItem22;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem29;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem130;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem131;
        private DevExpress.XtraSpreadsheet.UI.GalleryFormatAsTableItem galleryFormatAsTableItem1;
        private DevExpress.XtraSpreadsheet.UI.GalleryChangeStyleItem galleryChangeStyleItem1;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem30;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem132;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem133;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem134;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem31;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem135;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem136;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem137;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem32;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem138;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem139;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem140;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem141;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem142;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem33;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem143;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem144;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem145;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem146;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem147;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem148;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem149;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem150;
        private DevExpress.XtraSpreadsheet.UI.ChangeSheetTabColorItem changeSheetTabColorItem1;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarCheckItem spreadsheetCommandBarCheckItem40;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem152;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem34;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem35;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem158;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem159;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem160;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem161;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem36;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem162;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem163;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem164;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem165;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem166;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem167;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem37;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarSubItem spreadsheetCommandBarSubItem38;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem172;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem173;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem174;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem175;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem176;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem177;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem178;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem179;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem180;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem181;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem182;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem183;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem184;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem185;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem186;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem187;
        private DevExpress.XtraSpreadsheet.UI.SpreadsheetCommandBarButtonItem spreadsheetCommandBarButtonItem188;
        private DevExpress.Utils.ImageCollection ribbonImageCollectionLarge;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.BarStaticItem siStatus;
        private DevExpress.XtraBars.BarStaticItem selectedObjCapt;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarCheckItem simulateToolStripMenuItem;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.BarButtonItem NewButton;
        private DevExpress.XtraBars.BarButtonItem OpenStripButton1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        private DevExpress.XtraBars.BarCheckItem barCheckItem1;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGalleryBarItem1;
        private DevExpress.XtraBars.Ribbon.ApplicationMenu applicationMenu1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem10;
        private DevExpress.XtraBars.BarButtonItem barButtonItem11;
        private DevExpress.XtraBars.BarButtonItem barButtonItem12;
        private DevExpress.XtraBars.BarButtonItem barButtonItem13;
        private DevExpress.XtraBars.BarButtonItem barButtonItem14;
        private DevExpress.XtraBars.BarButtonItem barButtonItem15;
        private DevExpress.XtraBars.BarButtonItem barButtonItem16;
        private DevExpress.XtraBars.BarButtonItem barButtonItem17;
        private DevExpress.XtraBars.BarButtonItem barButtonItem18;
        private DevExpress.XtraBars.BarButtonItem barButtonItem19;
        private DevExpress.XtraBars.BarCheckItem barCheckItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem20;
        private DevExpress.XtraBars.BarButtonItem barButtonItem21;
        private DevExpress.XtraBars.BarButtonItem barButtonItem22;
        private DevExpress.XtraBars.BarButtonItem barButtonItem23;
        private DevExpress.XtraBars.BarButtonItem barButtonItem24;
        private DevExpress.XtraBars.BarButtonItem barButtonItem25;
        private DevExpress.XtraBars.BarButtonItem barButtonItem26;
        private DevExpress.XtraBars.BarCheckItem barCheckItem3;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.Ribbon.RibbonPage DebugPage;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup RenderPG;
        private DevExpress.XtraBars.Ribbon.RibbonPage MainPage;
        private DevExpress.XtraBars.BarButtonItem DiffuseB;
        private DevExpress.XtraBars.BarButtonItem SpecularB;
        private DevExpress.XtraBars.BarButtonItem NormalsB;
        private DevExpress.XtraBars.BarCheckItem WireframeB;
        private DevExpress.XtraBars.BarButtonItem PositionsB;
        private DevExpress.XtraBars.BarButtonItem roughnessB;
        private DevExpress.XtraBars.BarButtonItem SpecularPowerB;
        private DevExpress.XtraBars.BarButtonItem FinalLightingB;
        private DevExpress.XtraBars.BarButtonItem SoundMixEdB;
        private DevExpress.XtraBars.BarButtonItem MatEditorB;
        private DevExpress.XtraBars.BarButtonItem LogicEditorB;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup EditorsPG;
        private DevExpress.XtraBars.BarButtonItem LogicEditorB1;
        private DevExpress.XtraBars.BarButtonItem ContBrButton;
        private DevExpress.XtraBars.BarButtonItem SelectObjsB;
        private DevExpress.XtraBars.BarButtonItem MakeScrs_B;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ToolsPageGroup;
        private DevExpress.XtraBars.BarButtonItem SaveButton;
        private DevExpress.XtraBars.BarButtonItem SaveAsB;
        private DevExpress.XtraBars.BarButtonItem ImportButton;
        private DevExpress.XtraBars.BarButtonItem calcGILightmaps;
        private DevExpress.XtraBars.FormAssistant formAssistant1;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraBars.BarButtonItem SelectButton;
        private DevExpress.XtraBars.BarButtonItem ScaleB;
        private DevExpress.XtraBars.BarButtonItem RotateB;
        private DevExpress.XtraBars.BarButtonItem TranslateB;
        private DevExpress.XtraBars.BarCheckItem AutoSaveItem;
        private DevExpress.XtraBars.BarStaticItem GameFolderItem;
        private DevExpress.XtraBars.BarStaticItem GameNameItem;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarButtonItem RenderDepthB;
        private System.Windows.Forms.Timer autosaveTimer;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel4;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel4_Container;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel3;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel3_Container;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel2;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel2_Container;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem27;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarSubItem PopUp_Add_Actor;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup RefreshPageGroup;
        private DevExpress.XtraBars.BarButtonItem MaterialsButtonItem;
        private DevExpress.XtraBars.BarButtonItem ShadersButtonItem;
        private DevExpress.XtraBars.BarCheckItem MaterialsBRDFVisB;
        private DevExpress.XtraBars.BarButtonItem RenderDoc;
        private DevExpress.XtraBars.BarButtonItem ScriptDebugger;
        private DevExpress.XtraBars.BarButtonItem Brofiler;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup DeveloperToolsGroup;
        private DevExpress.XtraBars.BarSubItem barSubItem3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.BarButtonItem calccollisions;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup LevelBuilding;
    }
}

