﻿/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/

using DevExpress.XtraBars;
using DevExpress.XtraBars.Helpers;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using EngineCLR;
using NGEd.Tools;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace NGEd
{
    public partial class MainForm : RibbonForm
    {
        private static GraphNodes.LogicEditorForm mLogicEditor = null;
        /*About box, created once, on first call*/
        private static AboutBox about = null;
        private static ContentBrowserForm cb = null;
        /*Statistic*/
        private static Statistic statistic = null;
        private static EditorState editorState = null;
        private static SelectForm selectForm = null;
        private static MainProperties propWindow = null;
        private static ViewPort viewPortWindow = null;
        /**/
        private AutoSaveTools autosavetools = new AutoSaveTools();
        /**/
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = null;

        /**/
        private static string _LayoutName = "MainForm.xml";

        public MainForm()
        {
            InitializeComponent();

            splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::NGEd.SplashForm), false, false);

            _InitStateManager();

            _InitDockingPanels();

            _InitSelectForm();
            _InitAutoSave();
            _InitSkinGallery();
            _NewScene();

            DockingUtils.CreateEditorFolderIfNotExist();
            DockingUtils.LoadLayoutConfiguration(_LayoutName, dockManager1);
        }

        /// <summary>
        /// Функция инициализирует диалог который открывается при двойном нажатии колисиком мышки,
        /// а так же все диалоги которые требуют наличия уже запущенного движка
        /// </summary>
        private void _InitPopupMenus()
        {
            viewPortWindow.PopUpMenu = popupMenu1;
            _InitObjectMenus();
        }

        /// <summary>
        /// Составляет список доступных объектов для создания. Требует УЖЕ ИНИЦИАЛИЗИРОВАННЫЙ ДВИЖОК.
        /// Изначально(в конструкторе) составление не происходит, по причине того, что движок еще не инициализирован.
        /// Поэтому будет составлять список при первом клике. При последующих происходит очистка списка
        /// и создание по новой
        /// </summary>
        private void _InitObjectMenus()
        {
            barSubItem3.Reset();
            PopUp_Add_Actor.Reset();

            for (ulong i = 0; i < Engine_ReflectionSystem_Helper.GetCountOfCreators(); i++)
            {
                string name = Engine_ReflectionSystem_Helper.GetReflectableClassName(i);
                BarButtonItem button = new BarButtonItem
                {
                    Caption = name
                };
                button.ItemClick += (s, ee) => { Engine_ReflectionSystem_Helper.GetReflectableByName(name); };
                barSubItem3.AddItems(new BarItem[] { button });

                // А теперь добавим эти же менюшки но в сплывающее окно
                // Добавляем в список AddActors
                PopUp_Add_Actor.AddItems(new BarItem[] { button });
            }
        }

        /// <summary>
        /// Updates captions of labels
        /// </summary>
        private void _UpdateLabels()
        {
            GameNameItem.Caption = EngineCLR.CvarsCLR.GetGameName();
        }

        /// <summary>
        /// Init docking panels
        /// </summary>
        private void _InitDockingPanels()
        {
            propWindow = new MainProperties
            {
                Parent = dockPanel3
            };
            dockPanel3.Text = propWindow.Text;
            propWindow.Show();

            viewPortWindow = new ViewPort
            {
                Parent = dockPanel1
            };
            dockPanel1.Text = viewPortWindow.Text;
        }

        /// <summary>
        /// Inits state manager
        /// </summary>
        private void _InitStateManager()
        {
            editorState = EditorState.Instance;
            autosavetools.GetState = editorState;
        }

        private void _InitEngine()
        {
            if (editorState.GetEngine == null)
            {
                viewPortWindow.InitEngine();
                editorState.GetEngine = viewPortWindow.Engine;
                //!@todo Потом сделай кэллбек и меняй если изменилось свойство
                editorState.IsSceneModificated = true;
            }
            if (autosavetools.GetEngine == null)
            {
                autosavetools.GetEngine = viewPortWindow.Engine;
            }
            if (mLogicEditor == null)
            {
                mLogicEditor = new GraphNodes.LogicEditorForm();
            }

            {
                statistic = new EngineCLR.Statistic();
                _UpdateStatistic();
            }

            _NewScene();

            // Hide Splash, Show main window
            splashScreenManager1.WaitForSplashFormClose();

            _InitPopupMenus();
        }

        public void DoUpdate()
        {
            Application.DoEvents();

            _UpdateState();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            _InitEngine();
        }

        /// <summary>
        ///  Function for updates statistic tab
        /// </summary>
        private void _UpdateStatistic()
        {
            if (statistic == null)
            {
                return;
            }

            /*Rendering*/
            propWindow.label13.Text = statistic.GetDrawCalls().ToString();
            propWindow.label14.Text = statistic.GetMaxDrawCalls().ToString();
            propWindow.label15.Text = statistic.GetRenderStateChanges().ToString();
            propWindow.label16.Text = statistic.GetMaxRenderStateChanges().ToString();

            propWindow.label17.Text = ((int)statistic.GetGPUMemoryWrite() >> 20).ToString() + " MB";
            propWindow.label18.Text = ((int)statistic.GetMaxGPUMemoryWrite() >> 20).ToString() + " MB";

            propWindow.label19.Text = ((int)statistic.GetGPUMemoryRead() >> 20).ToString() + " MB";
            propWindow.label20.Text = ((int)statistic.GetMaxGPUMemoryRead() >> 20).ToString() + " MB";
            /*Scene*/
            propWindow.label21.Text = statistic.GetLightsCount().ToString();
            propWindow.label22.Text = (statistic.GetEntityCount() - statistic.GetLightsCount()).ToString();
            propWindow.label23.Text = statistic.GetCamerasCount().ToString();
            propWindow.label24.Text = statistic.GetLastFPS().ToString();
            /*Delta Updates*/
            propWindow.label25.Text = statistic.GetPhysicsDelta().ToString() + " ms";
            propWindow.label26.Text = statistic.GetAudioDelta().ToString() + " ms";
            propWindow.label27.Text = statistic.GetRenderDelta().ToString() + " ms";
            propWindow.label28.Text = statistic.GetGUIDelta().ToString() + " ms";
        }

        /// <summary>
        /// Function for update of editor state
        /// </summary>
        private void _UpdateStateWithFill()
        {
            if (editorState == null)
            {
                editorState = EditorState.Instance;
            }

            if (editorState == null || editorState.GetEngine == null)
            {
                return;
            }

            Text = editorState.GetSceneEditorTitle;

            if (selectForm != null)
            {
                selectForm.Fill();
            }

            _UpdateState();
        }

        private void _UpdateState()
        {
            _UpdateStatusBar();
            _UpdateStatistic();
            _UpdateLabels();

            if (editorState.GetEngine == null)
            {
                return;
            }

            editorState.GetEngine.Update();
        }

        protected override bool ProcessKeyPreview(ref Message msg)
        {
            if (editorState.GetEngine == null)
            {
                return false;
            }

            const int WM_KEYUP = 0x0101;
            const int WM_KEYDOWN = 0x0100;

            if (msg.Msg == WM_KEYDOWN)
            {
                editorState.GetEngine.KeyDown((int)(Keys)msg.WParam);
            }
            else if (msg.Msg == WM_KEYUP)
            {
                editorState.GetEngine.KeyUp((int)(Keys)msg.WParam);
            }

            return base.ProcessKeyPreview(ref msg);
        }

        /// <summary>
        ///
        /// </summary>
        private void StopSimulationAndSetUnChecked()
        {
            simulateToolStripMenuItem.Checked = false;
            editorState.IsSimPaused = !simulateToolStripMenuItem.Checked;
        }

        /// <summary>
        /// Function what created new scene, with activation new state
        /// </summary>
        private bool _NewScene()
        {
            if (editorState.GetEngine == null)
            {
                return false;
            }

            if (statistic != null)
            {
                statistic.Reset();
            }

            editorState.NewScene();
            _UpdateStateWithFill();
            SetZeroObjForPropGrid();

            return true;
        }

        /// <summary>
        ///
        /// </summary>
        private void _OpenScene()
        {
            Debug.Assert(editorState.GetEngine != null, "editorState.GetEngine is null");
            if (editorState.GetEngine == null)
            {
                return;
            }

            if (statistic != null)
            {
                statistic.Reset();
            }

            Debug.Assert(statistic != null, "statistic is null");

            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = "../",
                Filter = "Scene files (*.scene)|*.scene|All files (*.*)|*.*",
                FilterIndex = 1,
                RestoreDirectory = true
            };
            if ((openFileDialog1.ShowDialog() == DialogResult.OK) && (openFileDialog1.SafeFileName != ""))
            {
                StopSimulationAndSetUnChecked();
                SaveLoadHelper.LoadEngineFormat(openFileDialog1.SafeFileName, SaveLoadHelper.Type.SCENE);
            }
            openFileDialog1.Dispose();

            editorState.SetSceneName(openFileDialog1.SafeFileName, Path.GetDirectoryName(openFileDialog1.FileName));

            SetZeroObjForPropGrid();
            _UpdateStateWithFill();
        }

        /// <summary>
        ///
        /// </summary>
        private void _ImportObject()
        {
            if (editorState.GetEngine == null)
            {
                return;
            }

            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = "../",
                Filter = "Mesh files (*.nggf)|*.nggf|All files (*.*)|*.*",
                FilterIndex = 1,
                RestoreDirectory = true
            };
            if ((openFileDialog1.ShowDialog() == DialogResult.OK) && (openFileDialog1.SafeFileName != ""))
            {
                StopSimulationAndSetUnChecked();
                SaveLoadHelper.LoadEngineFormat(openFileDialog1.SafeFileName, SaveLoadHelper.Type.UNDEFINED);
            }
            openFileDialog1.Dispose();

            _UpdateStateWithFill();
        }

        /// <summary>
        /// Function for saving scene
        /// </summary>
        private void _SaveScene()
        {
            if (editorState.GetEngine == null)
            {
                return;
            }

            StopSimulationAndSetUnChecked();

            SaveFileDialog openFileDialog1 = new SaveFileDialog
            {
                InitialDirectory = "../",
                Filter = "Scene files (*.scene)|*.scene|All files (*.*)|*.*",
                FilterIndex = 2,
                RestoreDirectory = true
            };
            if ((openFileDialog1.ShowDialog() == DialogResult.OK) && (openFileDialog1.FileName != ""))
            {
                SaveLoadHelper.SaveEngineFormat(openFileDialog1.FileName, SaveLoadHelper.Type.SCENE);
            }
            openFileDialog1.Dispose();
        }

        /// <summary>
        ///
        /// </summary>
        private void _InitAutoSave()
        {
            // Count of selected objects
            _UpdateStatusBar();
        }

        private void _UpdateStatusBar()
        {
            selectedObjCapt.Caption = "Selected: " + EngineCLR.SceneEditor.Instance.SelectorActorsSize();
        }

        private void _InitSkinGallery()
        {
            SkinHelper.InitSkinGallery(rgbiSkins, true);
        }

        public void UpdateActorMeshPropGrid(int uid)
        {
            if (editorState.GetEngine == null)
            {
                return;
            }

            propWindow.tabPage1.Show();

            propWindow.propertyGrid1.SelectedObject = editorState.GetEngine.UpdateActorMeshVars(uid);
        }

        public void RefreshPropGrid()
        {
#if USE_DX
            propWindow.propertyGrid1.RetrieveFields();
#endif
        }

        /// <summary>
        /// Сбрасываем выбранный объект до пустого
        /// </summary>
        public void SetZeroObjForPropGrid()
        {
            propWindow.propertyGrid1.SelectedObject = null;
#if USE_DX
            propWindow.propertyGrid1.UpdateRows();
#endif
        }

        public void UpdateActorLightPropGrid(int uid)
        {
            if (editorState.GetEngine == null)
            {
                return;
            }

            propWindow.tabPage1.Show();

            propWindow.propertyGrid1.SelectedObject = editorState.GetEngine.UpdateActorLightVars(uid);
        }

        public void SelectObjectForPropGrid(ulong uid)
        {
            if (editorState.GetEngine == null)
            {
                return;
            }

            propWindow.tabPage1.Show();

            propWindow.propertyGrid1.SelectedObject = EngineCLR.EditableObjectAPI.GetEditableObject(uid);
        }

        /// <summary>
        /// Function for initialize selection form
        /// </summary>
        private void _InitSelectForm()
        {
            if (selectForm == null)
            {
                selectForm = new SelectForm();
            }

            selectForm.GetMainForm = this;
            selectForm.Parent = dockPanel2;
            dockPanel2.Text = selectForm.Text;
            selectForm.Show();
        }

        public void RefreshSelectForm()
        {
            if (selectForm == null)
            {
                return;
            }

            selectForm.UpdateSelectTable();
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (editorState.GetEngine == null)
            {
                return;
            }

            dockPanel3.Show();
            propWindow.tabPage1.Show();

            propWindow.propertyGrid1.SelectedObject = editorState.GetEngine.UpdateSceneVars();
        }

        private void barCheckItem1_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            editorState.IsSimPaused = !simulateToolStripMenuItem.Checked;
        }

        private void AutoSaveItem_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            autosavetools.TimerEnabled = AutoSaveItem.Checked;
        }

        private void NewButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _NewScene();
        }

        private void OpenStripButton1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _OpenScene();
        }

        private void SoundMixB_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SoundMix form = new SoundMix();
            form.Show();
        }

        private void MatEditorB_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            MaterialEditor form = new MaterialEditor();
            form.SetState(editorState);
            form.Show();
        }

        private void LogicEditorB_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (mLogicEditor == null)
            {
                return;
            }

            mLogicEditor.Show();
        }

        private void LogicEditorB1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LogicEditor form = new LogicEditor();
            form.Show();
        }

        private void ContBrButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (cb == null)
            {
                cb = new ContentBrowserForm();
            }
            cb.Show();
        }

        private void SelectObjsB_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _InitSelectForm();
        }

        private void MakeScrs_B_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EngineCLR.api.Make_ScreenShot();
        }

        private void SaveButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (editorState.GetEngine == null
           || string.IsNullOrEmpty(editorState.CurrentSceneName)
           || editorState.CurrentSceneName == "EMPTY")
            {
                return;
            }

            StopSimulationAndSetUnChecked();
            //!@todo GameName+Levels+SceneName
            SaveLoadHelper.SaveEngineFormat(editorState.CurrentSceneName, SaveLoadHelper.Type.UNDEFINED);
        }

        private void iExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _Exit();
        }

        private void SaveAsB_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _SaveScene();
        }

        private void ImportButton_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _ImportObject();
        }

        /// <summary>
        /// Function of shutdown editor
        /// </summary>
        private void _Exit()
        {
            DockingUtils.SaveLayoutConfiguration(_LayoutName, dockManager1);

            if (editorState != null)
            {
                _AskAboutSaveScene();
                editorState.Destroy();
            }

            Environment.Exit(0);
        }

        private void iAbout_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (about == null)
            {
                about = new AboutBox();
            }

            about.ShowDialog();
        }

        private void DiffuseB_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EngineCLR.RenderApi.VisualizateDiffuse();
        }

        //!todo Test todo list
        private void NormalsB_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EngineCLR.RenderApi.VisualizateNormals();
        }

        private void WireframeB_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EngineCLR.RenderApi.EnableDisableWireframe(WireframeB.Checked);
        }

        private void PositionsB_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EngineCLR.RenderApi.VisualizatePositions();
        }

        private void roughnessB_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EngineCLR.RenderApi.VisualizateRoughness();
        }

        private void SpecularPowerB_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EngineCLR.RenderApi.VisualizateSpecularPower();
        }

        private void SpecularB_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EngineCLR.RenderApi.VisualizateSpecular();
        }

        private void FinalLightingB_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EngineCLR.RenderApi.VisualizateFinalLighting();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (XtraMessageBox.Show("Really?", Text,
                       MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
            {
                e.Cancel = true;
                return;
            }
            else
            {
                _AskAboutSaveScene();
            }
        }

        private void _AskAboutSaveScene()
        {
            if (autosavetools == null)
            {
                return;
            }

            DockingUtils.SaveLayoutConfiguration(_LayoutName, dockManager1);
            autosavetools.AskAboutSaveScene();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (autosavetools == null)
            {
                return;
            }

            autosavetools.AskAboutLoadAutoSaveScene();
        }

        /// <summary>
        /// AutoSave every 10 minutes (600000 msecs)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void autosaveTimer_Tick(object sender, EventArgs e)
        {
            if (autosavetools == null || AutoSaveItem.Checked == false)
            {
                return;
            }

            autosavetools.SaveSnapshot(true);
        }

        private void RenderDepthB_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EngineCLR.RenderApi.VisualizateDepth();
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (viewPortWindow == null)
            {
                return;
            }

            viewPortWindow.ResizeEngine(dockPanel1.Width, dockPanel1.Height);
        }

        private void MainForm_ResizeBegin(object sender, EventArgs e)
        {
            MainForm_Resize(sender, e);
        }

        private void MainForm_ResizeEnd(object sender, EventArgs e)
        {
            MainForm_Resize(sender, e);
        }

        private void dockPanel1_Resize(object sender, EventArgs e)
        {
            MainForm_Resize(sender, e);
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DockingUtils.SaveLayoutConfiguration(_LayoutName, dockManager1);
        }

        private void barButtonItem27_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DockingUtils.LoadLayoutConfiguration(_LayoutName, dockManager1);
        }

        private void ShadersButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EngineCLR.CacheApi.ReloadShaders();
        }

        private void MaterialsBRDF_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // TODO: Implement later
        }

        private void MaterialsBRDFVisB_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EngineCLR.RenderApi.VisualizateBRDF(MaterialsBRDFVisB.Checked);
        }

        private void RenderDoc_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bool result = false;
            try
            {
                if (Utils.IsX64Platform())
                {
                    result = Utils.LaunchAppFromPathWithArgs("../../Development/Tools/RenderDoc/x64/Release/RenderDoc.exe", "");
                }
                else
                {
                    result = Utils.LaunchAppFromPathWithArgs("../../Development/Tools/RenderDoc/Win32/Release/RenderDoc.exe", "");
                }
            }
            catch (Exception exception)
            {
                string err = "Failed loading RenderDoc.";
                err += " Error log:" + exception;
                XtraMessageBox.Show(err, "Error");
            }
            if (!result)
            {
                XtraMessageBox.Show("Failed loading RenderDoc", "Error");
            }
        }

        private void ScriptDebugger_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bool result = false;
            result = Utils.LaunchAppFromPathWithArgs("../../Development/Tools/ScriptDebugger/ScriptDebugger.exe", "");
        }

        private void Brofiler_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bool result = false;
            try
            {
                if (Utils.IsX64Platform())
                {
                    result = Utils.LaunchAppFromPathWithArgs("../../Development/Tools/Brofiler/Bin/Release/x64/Brofiler.exe", "");
                }
                else
                {
                    result = Utils.LaunchAppFromPathWithArgs("../../Development/Tools/Brofiler/Bin/Release/Win32/Brofiler.exe", "");
                }
            }
            catch (Exception exception)
            {
                string err = "Failed loading Brofiler.";
                err += " Error log:" + exception;
                XtraMessageBox.Show(err, "Error");
            }
            if (!result)
            {
                XtraMessageBox.Show("Failed loading Brofiler", "Error");
            }
        }

        private void barSubItem3_ItemClick(object sender, ItemClickEventArgs e)
        {
            _InitObjectMenus();
        }

        private void calccollisions_ItemClick(object sender, ItemClickEventArgs e)
        {
            LevelBuildingTool levelbuildingTool = new LevelBuildingTool();
            levelbuildingTool.CalculateCollisions();
        }
    }
}