﻿/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/

using DevExpress.XtraEditors;
using EngineCLR;
using NGEd.Tools;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace NGEd
{
    public partial class MaterialEditor : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public MaterialEditor()
        {
            InitializeComponent();
            _InitEditor();
        }

        private void _InitEditor()
        {
            AutoSize = true;
            graphControl = new NGEd.FormComponents.NodeComponent
            {
                Parent = dockPanel4
            };
            graphControl.Show();
            /**/
            propWindow = new PropertiesTab
            {
                Parent = dockPanel3
            };
            propWindow.Show();
            /**/
            //var viewport = new ViewPort();
            //propWindow.Parent = dockPanel1;
            //viewport.Text = "Material Preview";
            //viewport.InitEngine();

            _InitMaterialNode();
            _RegisterAllNodes();

            graphControl.GraphControl.ShowElementMenu += new EventHandler<Graph.AcceptElementLocationEventArgs>(OnShowElementMenu);

            DockingUtils.CreateEditorFolderIfNotExist();
            DockingUtils.LoadLayoutConfiguration(_LayoutName, dockManager1);
        }

        public void SetState(EditorState _state)
        {
            editorState = _state;
        }

        private void _InitMaterialNode()
        {
            Graph.Node someNode = new Graph.Node("Material Inputs")
            {
                Location = new Point(508, 305)
            };
            someNode.AddItem(new Graph.Items.NodeCheckboxItemInverse("Base Color", true, false) { Tag = -1 });
            someNode.AddItem(new Graph.Items.NodeCheckboxItemInverse("Metallic", true, false) { Tag = -2 });
            someNode.AddItem(new Graph.Items.NodeCheckboxItemInverse("Specular", true, false) { Tag = -3 });
            someNode.AddItem(new Graph.Items.NodeCheckboxItemInverse("Roughness", true, false) { Tag = -4 });
            someNode.AddItem(new Graph.Items.NodeCheckboxItemInverse("Emissive Color", true, false) { Tag = -5 });
            someNode.AddItem(new Graph.Items.NodeCheckboxItemInverse("Opacity", true, false) { Tag = -6 });
            someNode.AddItem(new Graph.Items.NodeCheckboxItemInverse("Opacity Mask", true, false) { Tag = -7 });
            someNode.AddItem(new Graph.Items.NodeCheckboxItemInverse("Normal", true, false) { Tag = -8 });
            someNode.AddItem(new Graph.Items.NodeCheckboxItemInverse("World Position Offset", true, false) { Tag = -9 });
            someNode.AddItem(new Graph.Items.NodeCheckboxItemInverse("World Displacement", true, false) { Tag = -10 });
            someNode.AddItem(new Graph.Items.NodeCheckboxItemInverse("Tessellation Multiplier", true, false) { Tag = -11 });
            someNode.AddItem(new Graph.Items.NodeCheckboxItemInverse("SubSurface Color", true, false) { Tag = -12 });
            someNode.AddItem(new Graph.Items.NodeCheckboxItemInverse("Clear Coat", true, false) { Tag = -13 });
            someNode.AddItem(new Graph.Items.NodeCheckboxItemInverse("Clear Coat Roughness", true, false) { Tag = -14 });
            someNode.AddItem(new Graph.Items.NodeCheckboxItemInverse("Ambient Occlusion", true, false) { Tag = -15 });
            someNode.AddItem(new Graph.Items.NodeCheckboxItemInverse("Refraction", true, false) { Tag = -16 });
            someNode.AddItem(new Graph.Items.NodeCheckboxItemInverse("Pixel Depth Offset", true, false) { Tag = -17 });

            graphControl.GraphControl.AddNode(someNode);

            // Tests
            someNode = new Graph.Node("My Title")
            {
                Location = new Point(500, 100)
            };
            Graph.Items.NodeCheckboxItem check1Item = new Graph.Items.NodeCheckboxItem("Check 1", true, true) { Tag = 31337 };
            someNode.AddItem(check1Item);
            someNode.AddItem(new Graph.Items.NodeCheckboxItem("Check 2", true, true) { Tag = 42f });

            graphControl.GraphControl.AddNode(someNode);
        }

        private void OnShowElementMenu(object sender, Graph.AcceptElementLocationEventArgs e)
        {
            XtraMessageBox.Show("!");
            //if (e.Element == null)
            //{
            //    // Show a test menu for when you click on nothing
            //    testMenuItem.Text = "(clicked on nothing)";
            //    nodeMenu.Show(e.Position);
            //    e.Cancel = false;
            //}
            //else
            //if (e.Element is Graph.Node)
            //{
            //    // Show a test menu for a node
            //    testMenuItem.Text = ((Graph.Node)e.Element).Title;
            //    nodeMenu.Show(e.Position);
            //    e.Cancel = false;
            //}
            //else
            //if (e.Element is Graph.NodeItem)
            //{
            //    // Show a test menu for a nodeItem
            //    testMenuItem.Text = e.Element.GetType().Name;
            //    nodeMenu.Show(e.Position);
            //    e.Cancel = false;
            //}
            //else
            //{
            //    // if you don't want to show a menu for this item (but perhaps show a menu for something more higher up)
            //    // then you can cancel the event
            //    e.Cancel = true;
            //}
        }

        /// <summary>
        ///!@todo REWRITE
        /// </summary>
        private void _RegisterAllNodes()
        {
            ToolStripMenuItem item = new ToolStripMenuItem();
            item.Click += new EventHandler(SomeNode_TestMouseDown);
            item.Text = "Sub-item 1";
            //nodeMenu.Items.Add(item);
        }

        /// <summary>
        ///!@todo TEST ONLY. Тестовая функция
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SomeNode_TestMouseDown(object sender, EventArgs e)
        {
            Graph.Node node = new Graph.Node("Some node");
            node.AddItem(new Graph.Items.NodeLabelItem("Entry 1", true, false));
            node.AddItem(new Graph.Items.NodeLabelItem("Entry 2", true, false));
            node.AddItem(new Graph.Items.NodeLabelItem("Entry 3", false, true));
            node.AddItem(new Graph.Items.NodeTextBoxItem("TEXTTEXT", false, true));
            node.AddItem(new Graph.Items.NodeDropDownItem(new string[] { "1", "2", "3", "4" }, 0, false, false));
            // Устанавливает позицию, относительно graphControl
            node.Location = new Point(graphControl.PointToClient(Cursor.Position).X, graphControl.PointToClient(Cursor.Position).Y);
            XtraMessageBox.Show(graphControl.PointToClient(Cursor.Position).X.ToString(), graphControl.PointToClient(Cursor.Position).Y.ToString());
            DoDragDrop(node, DragDropEffects.Copy);
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (editorState.GetEngine == null)
            {
                return;
            }

            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = "../",
                Filter = "Material files (*.mat)|*.mat|All files (*.*)|*.*",
                FilterIndex = 1,
                RestoreDirectory = true
            };
            if ((openFileDialog1.ShowDialog() == DialogResult.OK) && (openFileDialog1.SafeFileName != ""))
            {
                _SelectMaterial(openFileDialog1.SafeFileName);
            }
            openFileDialog1.Dispose();
        }

        private void _SelectMaterial(string _materialName)
        {
            if (_materialName == string.Empty ||
                    editorState.GetEngine == null)
            {
                return;
            }

            EngineCLR.log.DebugPrintf("Loaded material " + _materialName + " to editor");

            propWindow.propertyGrid1.SelectedObject = EngineCLR.MaterialsHelper.UpdateMaterialVars(_materialName);
            propWindow.propertyGrid1.RetrieveFields();
            propWindow.propertyGrid1.UpdateRows();
            propWindow.propertyGrid1.Refresh();
        }

        private EditorState editorState = null;
        private PropertiesTab propWindow = null;
        private NGEd.FormComponents.NodeComponent graphControl = null;

        private void barButtonItem8_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DockingUtils.SaveLayoutConfiguration(_LayoutName, dockManager1);
        }

        private void barButtonItem9_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DockingUtils.LoadLayoutConfiguration(_LayoutName, dockManager1);
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                InitialDirectory = "../",
                Filter = "Material file (*.mat)|*.mat|All files (*.*)|*.*",
                FilterIndex = 1,
                RestoreDirectory = false
            };
            if ((openFileDialog1.ShowDialog() == DialogResult.OK) && (openFileDialog1.SafeFileName != ""))
            {
                _SelectMaterial(openFileDialog1.SafeFileName);
            }
            openFileDialog1.Dispose();
        }

        private static string _LayoutName = "MaterialEditor.xml";

        private void MaterialEditor_FormClosing(object sender, FormClosingEventArgs e)
        {
            DockingUtils.SaveLayoutConfiguration(_LayoutName, dockManager1);
        }

        private void barButtonItem10_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SaveFileDialog openFileDialog1 = new SaveFileDialog
            {
                InitialDirectory = "../",
                Filter = "Material file (*.mat)|*.mat|All files (*.*)|*.*",
                FilterIndex = 2,
                RestoreDirectory = false
            };
            if ((openFileDialog1.ShowDialog() == DialogResult.OK) && (openFileDialog1.FileName != ""))
            {
                SaveLoadHelper.SaveEngineFormat(openFileDialog1.FileName, SaveLoadHelper.Type.MATERIAL);
            }
            openFileDialog1.Dispose();
        }
    }
}