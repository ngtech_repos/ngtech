﻿namespace NGEd
{
    partial class SelectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.selectObjectsTable = new DevExpress.XtraTreeList.TreeList();
            this.ColumnName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.Type = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.Material = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.guid = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            ((System.ComponentModel.ISupportInitialize)(this.selectObjectsTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // selectObjectsTable
            // 
            this.selectObjectsTable.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.ColumnName,
            this.Type,
            this.Material,
            this.guid});
            this.selectObjectsTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectObjectsTable.Location = new System.Drawing.Point(2, 2);
            this.selectObjectsTable.Name = "selectObjectsTable";
            this.selectObjectsTable.OptionsClipboard.AllowCopy = DevExpress.Utils.DefaultBoolean.True;
            this.selectObjectsTable.OptionsClipboard.CopyNodeHierarchy = DevExpress.Utils.DefaultBoolean.True;
            this.selectObjectsTable.Size = new System.Drawing.Size(581, 497);
            this.selectObjectsTable.TabIndex = 27;
            this.selectObjectsTable.KeyDown += new System.Windows.Forms.KeyEventHandler(this.selectObjectsTable_KeyDown_1);
            this.selectObjectsTable.MouseClick += new System.Windows.Forms.MouseEventHandler(this.selectObjectsTable_MouseClick);
            // 
            // Name
            // 
            this.ColumnName.Caption = "Name";
            this.ColumnName.FieldName = "Name";
            this.ColumnName.Name = "Name";
            this.ColumnName.Visible = true;
            this.ColumnName.VisibleIndex = 0;
            // 
            // Type
            // 
            this.Type.Caption = "Type";
            this.Type.FieldName = "Type";
            this.Type.Name = "Type";
            this.Type.Visible = true;
            this.Type.VisibleIndex = 1;
            // 
            // Material
            // 
            this.Material.Caption = "Material";
            this.Material.FieldName = "Material";
            this.Material.Name = "Material";
            this.Material.Visible = true;
            this.Material.VisibleIndex = 2;
            // 
            // guid
            // 
            this.guid.Caption = "guid";
            this.guid.FieldName = "guid";
            this.guid.Name = "guid";
            // 
            // SelectForm2
            // 
            this.AutoSize = true;
            this.Controls.Add(this.selectObjectsTable);
            this.Size = new System.Drawing.Size(585, 501);
            ((System.ComponentModel.ISupportInitialize)(this.selectObjectsTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTreeList.TreeList selectObjectsTable;
        private DevExpress.XtraTreeList.Columns.TreeListColumn ColumnName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn Type;
        private DevExpress.XtraTreeList.Columns.TreeListColumn Material;
        private DevExpress.XtraTreeList.Columns.TreeListColumn guid;
    }
}