﻿/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/

using System;
using System.Windows.Forms;

namespace NGEd
{
    public partial class SelectForm : DevExpress.XtraEditors.PanelControl
    {
        public SelectForm()
        {
            InitializeComponent();
            Fill();
            AutoSize = true;
            Dock = System.Windows.Forms.DockStyle.Fill;
            Text = "Objects List";

            DisableEditMode();
        }

        /// <summary>
        ///  Отключаем режим редактирования в таблице(что бы нельзя было переименовывать ноды)
        /// </summary>
        private void DisableEditMode()
        {
            selectObjectsTable.OptionsBehavior.Editable = false;
            selectObjectsTable.OptionsBehavior.ReadOnly = true;
        }

        private void SelectForm_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        public void Fill()
        {
            if (EngineCLR.SceneEditor.Instance == null || GetMainForm == null)
            {
                return;
            }

            selectObjectsTable.ClearNodes();

            selectObjectsTable.BeginUnboundLoad();
            //!@todo Тут категорию указывать в типе управляемого объекта - создавать категории
            DevExpress.XtraTreeList.Nodes.TreeListNode m_meshesRootNode = selectObjectsTable.AppendNode(new object[] { "Meshes", "", "", "" }, null);
            DevExpress.XtraTreeList.Nodes.TreeListNode m_lightsRootNode = selectObjectsTable.AppendNode(new object[] { "Lights", "", "", "" }, null);

            //!@todo Тут категорию указывать в типе управляемого объекта
            // Add world actors/meshes
            for (uint i = 0; i < EngineCLR.SceneEditor.Instance.GetMeshesCount(); i++)
            {
                EngineCLR.MObjectMesh a = EngineCLR.SceneEditor.Instance.GetActorMesh(i);

                if ((a == null) || (string.IsNullOrEmpty(a.LoadingPath) || !a.IsSelectable))
                {
                    continue;
                }

                string type, matName;

                // Insert row with name, type, matname
                // Gather material name(s)
                {
                    matName = a.GetMaterialsAsString();
                }
                type = a.ClassName;
                if (a.IsHidden)
                {
                    type += " (Hidden)";
                }

                if (a.IsFrozen) //!@todo DEPRECATED
                {
                    type += " (Frozen)";
                }

                // Add listing to table
                if (!string.IsNullOrEmpty(type))
                {
                    selectObjectsTable.AppendNode(new object[] { a.LoadingPath, type, matName, a.GetGUIDString() }, m_meshesRootNode);
                }
            }

            // Add world actors/Lights
            for (uint i = 0; i < EngineCLR.SceneEditor.Instance.GetLightsCount(); i++)
            {
                EngineCLR.MObjectLight a = EngineCLR.SceneEditor.Instance.GetActorLight(i);

                if ((a == null) || (!a.IsSelectable))
                {
                    continue;
                }

                string type, matName;

                // Insert row with name, type
                {
                    matName = "";
                }
                type = a.ClassName;
                if (a.IsHidden)
                {
                    type += " (Hidden)";
                }

                if (a.IsFrozen) //!@todo DEPRECATED
                {
                    type += " (Frozen)";
                }

                // Add listing to table
                if (!string.IsNullOrEmpty(type))
                {
                    selectObjectsTable.AppendNode(new object[] { a.ClassName + i.ToString(), type, matName, a.GetGUIDString() }, m_lightsRootNode);
                }
            }

            UpdateRowsFromSelected();

            selectObjectsTable.EndUnboundLoad();
        }

        private void UpdateRowsFromSelected()
        {
            selectObjectsTable.Selection.Clear();
            for (uint j = 0; j < EngineCLR.SceneEditor.Instance.SelectorActorsSize(); j++)
            {
                for (int i = 0; i < selectObjectsTable.Selection.Count; i++)
                {
                    bool selected = EngineCLR.SceneEditor.Instance.SelectorActorsGetGUIDString(j) == selectObjectsTable.Selection[i].GetValue(3).ToString();
                    if (selected)
                    {
                        selectObjectsTable.Selection[i].Selected = true;
                    }
                }
            }
        }

        //!@todo Тут не доделано обновление через общий интерфейс
        private void UpdateSelections()
        {
            EngineCLR.SceneEditor.Instance.SelectorUnSelectAll();

            for (int i = 0; i < selectObjectsTable.Selection.Count; i++)
            {
                string guid = selectObjectsTable.Selection[i].GetValue(3).ToString();

                bool a = EngineCLR.SceneEditor.Instance.SelectorCanFindObject(guid);

                if (a)
                {
                    object selectedObject = EngineCLR.SceneEditor.Instance.SelectorSelectObject(guid);

                    uint index = 0;
                    //TODO: Говнокод, тут надо переписать все, но сейчас главное прототип
                    for (uint j = 0; j < EngineCLR.SceneEditor.Instance.GetMeshesCount(); j++)
                    {
                        EngineCLR.MObjectMesh mesh = EngineCLR.SceneEditor.Instance.GetActorMesh(j);
                        if (guid == mesh.GetGUIDString())
                        {
                            index = j;
                            if (GetMainForm != null)
                            {
                                GetMainForm.UpdateActorMeshPropGrid((int)index);
                            }

                            break;
                        }
                    }
                    //TODO: Говнокод, тут надо переписать все, но сейчас главное прототип
                    for (uint j = 0; j < EngineCLR.SceneEditor.Instance.GetLightsCount(); j++)
                    {
                        EngineCLR.MObjectLight light = EngineCLR.SceneEditor.Instance.GetActorLight(j);
                        if (guid == light.GetGUIDString())
                        {
                            index = j;
                            if (GetMainForm != null)
                            {
                                GetMainForm.UpdateActorLightPropGrid((int)index);
                            }

                            break;
                        }
                    }
                }
                else
                {
                    // Actor must have been deleted, remove it
                    selectObjectsTable.Selection.RemoveAt(i);
                    i--;

                    if (GetMainForm != null)
                    {
                        GetMainForm.RefreshPropGrid();
                        GetMainForm.RefreshSelectForm();
                    }
                }
            }

            // Выбрали объект, и теперь отобразим его свойства
            if (GetMainForm != null)
            {
                GetMainForm.RefreshPropGrid();
            }
        }

        public MainForm GetMainForm { get; set; }

        private void checkHidden_CheckedChanged(object sender, EventArgs e)
        {
            Fill();
        }

        private void checkFrozen_CheckedChanged(object sender, EventArgs e)
        {
            Fill();
        }

        private void selectObjectsTable_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Delete)
            {
                if (GetMainForm != null)
                {
                    GetMainForm.SetZeroObjForPropGrid();
                }

                EngineCLR.SceneEditor.Instance.SelectorDeleteSelectedEntity();
                for (int i = 0; i < selectObjectsTable.Selection.Count; i++)
                {
                    selectObjectsTable.Selection.RemoveAt(i);
                    i--;
                    if (GetMainForm != null)
                    {
                        GetMainForm.RefreshPropGrid();
                        GetMainForm.RefreshSelectForm();
                    }
                }
            }
            UpdateSelections();
        }

        private void selectObjectsTable_MouseClick(object sender, MouseEventArgs e)
        {
            UpdateSelections();
        }

        public void UpdateSelectTable()
        {
            selectObjectsTable.ClearNodes();
            Fill();
        }
    }
}