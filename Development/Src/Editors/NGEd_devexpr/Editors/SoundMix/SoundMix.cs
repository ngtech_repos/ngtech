﻿/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/

using DevExpress.XtraBars.Ribbon;
using NGEd.Tools;

namespace NGEd
{
    public partial class SoundMix : RibbonForm
    {
        public SoundMix()
        {
            InitializeComponent();
            _Init();
        }

        private void _Init()
        {
            AutoSize = true;
            graphControl = new NGEd.FormComponents.NodeComponent
            {
                Parent = dockPanel4
            };
            graphControl.Show();

            /**/
            propWindow = new PropertiesTab
            {
                Parent = dockPanel3
            };
            propWindow.Show();

            DockingUtils.CreateEditorFolderIfNotExist();
            DockingUtils.LoadLayoutConfiguration(_LayoutName, dockManager1);
        }

        private PropertiesTab propWindow = null;
        private NGEd.FormComponents.NodeComponent graphControl = null;

        private void SoundMix_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            DockingUtils.SaveLayoutConfiguration(_LayoutName, dockManager1);
        }

        private static string _LayoutName = "SoundMix.xml";

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DockingUtils.SaveLayoutConfiguration(_LayoutName, dockManager1);
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DockingUtils.LoadLayoutConfiguration(_LayoutName, dockManager1);
        }
    }
}