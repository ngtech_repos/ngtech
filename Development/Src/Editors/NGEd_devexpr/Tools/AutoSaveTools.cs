﻿/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/

using DevExpress.XtraEditors;
using EngineCLR;
using System;
using System.IO;
using System.Windows.Forms;

namespace NGEd.Tools
{
    /// <summary>
    /// Class what implemented autosave
    /// </summary>
    internal class AutoSaveTools
    {
        public
        EngineCLR.EngineCLR GetEngine
        {
            get => m_engine;
            set => m_engine = value;
        }

        public
        EditorState GetState
        {
            get => m_state;
            set => m_state = value;
        }

        /// <summary>
        /// This function defined is enabled timer. Can enable/disable this
        /// </summary>
        public
        bool TimerEnabled
        {
            get => m_TimerEnabled;
            set => m_TimerEnabled = value;
        }

        /// <summary>
        /// This function will called in timer
        /// </summary>
        /// <param name="auto_save"></param>
        public void SaveSnapshot(bool auto_save)
        {
            // Auto-save.
            if (auto_save && TimerEnabled)
            {
                _SaveSceneImpl(string.Empty, m_bakName);
            }
        }

        /// <summary>
        /// This function asks about save of scene, if this was modificated
        /// </summary>
        public void AskAboutSaveScene()
        {
            if (GetEngine.CountofObjectsOnScene() == 0 || GetState.IsSceneModificated == false)
            {
                return;
            }

            if (XtraMessageBox.Show("Scene was modificated. Save at?", "Save at?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Save();
            }
        }

        /// <summary>
        /// Asks about restore scene from backup
        /// </summary>
        public void AskAboutLoadAutoSaveScene()
        {
            // See if the file exists.
            if (File.Exists(m_state.CurrentSceneName + m_bakName))
            {
                // Ask the user if we should load this file.
                if (XtraMessageBox.Show(
                    "An auto-save file exists. Do you want to load it?",
                    "Restore?", MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question)
                    == DialogResult.Yes)
                {
                    // Load the file.
                    LoadFromFile(m_state.CurrentSceneName + m_bakName);
                }
            }
        }

        /// <summary>
        /// Load scene from backup file
        /// </summary>
        /// <param name="_file"></param>
        private void LoadFromFile(string _file)
        {
            //!@todo Implement this
            throw new NotImplementedException();
        }

        /// <summary>
        /// Save scene in backup file
        /// </summary>
        private void Save()
        {
            if (GetEngine == null || GetState == null)
            {
                return;
            }

            // останавливаем симуляцию
            m_state.IsSimPaused = true;
            _SaveSceneImpl(string.Empty, string.Empty);

            // Deletion of existing bak file
            if (File.Exists(m_state.CurrentSceneName + m_bakName))
            {
                File.Delete(m_state.CurrentSceneName + m_bakName);
            }
        }

        /// <summary>
        /// Implementation of SaveScene
        /// </summary>
        /// <param name="_prefix"></param>
        /// <param name="_postExt"></param>
        private void _SaveSceneImpl(string _prefix, string _postExt)
        {
            if (m_engine == null || m_state == null || string.IsNullOrEmpty(m_state.CurrentSceneName) ||
                m_state.CurrentSceneName == "EMPTY"
                || m_state.IsSimPaused == false
                )
            {
                return;
            }

            SaveLoadHelper.SaveEngineFormat(_prefix + m_state.CurrentSceneName + _postExt, SaveLoadHelper.Type.SCENE);
        }

        /// <summary>
        /// EngineCLR pointer
        /// </summary>
        private
        EngineCLR.EngineCLR m_engine = null;

        /// <summary>
        /// Editor state pointer
        /// </summary>
        private
        EditorState m_state = null;

        /// <summary>
        /// Fixed bak extension for file
        /// </summary>
        private readonly string m_bakName = ".bak";

        /// <summary>
        /// Timer enabled boolean value
        /// </summary>
        private
        bool m_TimerEnabled = true;
    }
}