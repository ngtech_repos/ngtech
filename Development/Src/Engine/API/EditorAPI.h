/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#if IS_OS_WINDOWS

#include "../inc/DLLDef.h"

#include <string>

#include <guiddef.h>

namespace NGTech
{
	struct EditableObject;

	ENGINE_API EditableObject*	API_Editor_FindActor(GUID guid);
	ENGINE_API EditableObject*	API_Editor_FindActorGUIDString(const char* sGuid);
	ENGINE_API void				API_Editor_DestroyActorGUIDString(const char* sGuid);

#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
	/**/
	void ENGINE_API				API_EditorCheckWhatWasModificated();
#endif
}

#endif