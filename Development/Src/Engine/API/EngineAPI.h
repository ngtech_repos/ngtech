/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include "../inc/DLLDef.h"

namespace NGTech
{
	/**
	*/
	class Camera;
	class Material;
	class Light;
	class LoadingScreen;
	/**
	*/
	void ENGINE_API API_LoadEngineModule(const String & _name);
	void ENGINE_API API_LoadStartupModule(const String & _cfg, int argc, const char * const * argv);
	void ENGINE_API API_LoadStartupModuleEditor(const String & _cfg, int argc, const char * const * argv);
	void ENGINE_API API_EngineAutoStart(int argc, const char * const * argv);
	void ENGINE_API API_DestroyEngine();
	ENGINE_API Camera* API_GetCurrentCamera();
	/*Scene*/
	size_t ENGINE_API API_CountofObjectsOnScene();
	void ENGINE_API API_SaveScene(const String &path);
	void ENGINE_API API_LoadSceneNew(const String & path, bool isEditor);
	void ENGINE_API API_NewScene();
	void ENGINE_API API_SetLoadingScreen(const char* _path);
	void ENGINE_API API_SetLoadingScreen(LoadingScreen* _screen);
	BBox ENGINE_API CalculateShadowBox(Light* _light);
	/*Physics*/
	void ENGINE_API API_SetGravity(const Vec3& _gravity);
	/*Material*/
	void ENGINE_API API_SaveMaterial(Material* mat, const String & path);
	void ENGINE_API API_SaveMaterial(const String & path);
	/*Light*/
	ENGINE_API Light* API_CreateLight(unsigned int _type);
}