/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include "../inc/DLLDef.h"

namespace NGTech
{
	/**
	*/
	ENGINE_API void API_Make_ScreenShot();
	/**
	Ambient Lighting, Not used if exist any light source - in PBR will exist 1 global light source
	Will be used in editor
	*/
	ENGINE_API void API_SetAmbient(const Vec3 &);
	ENGINE_API void API_ResizeWindow(int _w, int _h);
	ENGINE_API void API_EnableDisableWireframe(bool _use);
	ENGINE_API void API_SetDebugDrawMode(unsigned int _dm);
}