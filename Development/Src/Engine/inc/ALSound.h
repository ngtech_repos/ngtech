/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//***************************************************************************
#include "../../Core/inc/IncludesAndLibs.h"
#include "../../Common/IResource.h"
//***************************************************************************
#include "DLLDef.h"
//***************************************************************************

namespace NGTech {
	/**/
	class ALSoundSource;
	/*
	Desc: Sound common class-loading
	*/
	class ENGINE_API ALSound :public IResource
	{
	public:
		struct Desc
		{
			/*Sound path, will be serialized*/
			String path = "debugcontent/dummyfile";
		private:
			/*Sound format, not must be serialized*/
			unsigned int _format = (unsigned int)-1;
			/*Sound rate, not must be serialized*/
			unsigned int _rate = (unsigned int)-1;
			/*Number samples of sound, not must be serialized*/
			unsigned int _numSamples = (unsigned int)-1;
			/*BufferID, not must be serialized*/
			unsigned int _buffID = (unsigned int)-1;
			/*Samples ptr, not must be serialized*/
			short *_samples = nullptr;
			/*Number channels of sound, not must be serialized*/
			unsigned int _numChannels = (unsigned int)-1;
			/*Sound size, not must be serialized**/
			unsigned int _size = (unsigned int)-1;
		public:
			Desc()
			{
				_format = (unsigned int)-1;
				_rate = (unsigned int)-1;
				_size = (unsigned int)-1;
				_numChannels = (unsigned int)-1;
				_numSamples = (unsigned int)-1;
				_buffID = (unsigned int)-1;
				_samples = nullptr;
				path = "debugcontent/dummyfile";
			}

			friend ALSound;
			friend ALSoundSource;
		};
		Desc desc;
	public:
		ALSound(const String &path, bool _load = true);
		ALSound(Desc desc, bool _load = true);
		virtual ~ALSound();

		ENGINE_INLINE virtual void Load() override { _LoadFile(); }

		ENGINE_INLINE virtual bool OnlyLoad() override { _LoadFile(); return true; }

		ENGINE_INLINE virtual String GetResourceType() const override { return "ALSound"; }

		void SetDescAndLoad(Desc desc, bool _load);
	private:
		ALSound() :IResource() { }
		bool _LoadFile();
		bool _CheckFormatSupport(const String &path);

		friend Desc;
		friend ALSoundSource;
	};
}