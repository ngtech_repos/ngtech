/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//***************************************************************************
#include "../../Core/inc/IncludesAndLibs.h"
//***************************************************************************
#include "ALSound.h"
#include "DLLDef.h"
//***************************************************************************

namespace NGTech {
	/*
	Desc: Sound source class, it's not ambient light
	*/
	class ENGINE_API ALSoundSource :public RefCount
	{
	public:
		struct Desc {
			enum Sound_Presets
			{
				DEFAULT = 0,
				SOUND_SOURCE,
				SOUND_BACKGROUND,
				SOUND_COUNT
			};

			bool				Paused;
			bool				Stoped;
			bool				Looped;
			bool				Relative;
			TimeDelta			Gain;
			TimeDelta			RollOffFactor;
			TimeDelta			MaxDistance_in_Meter;
			TimeDelta			MinDistance_in_Meter;
			Vec3				Position;
			/*Sound preset, will be serialized*/
			Sound_Presets		preset;
		public:
			ENGINE_INLINE bool IsValid() { return alID != -1; }

			Desc()
				:alID((unsigned int)-1),
				sound(nullptr),
				Paused(false),
				Stoped(true),
				Looped(false),
				Relative(false),
				Gain(Math::ZEROFLOAT),
				RollOffFactor(Math::ZEROFLOAT),
				MaxDistance_in_Meter(Math::ZEROFLOAT),
				MinDistance_in_Meter(Math::ZEROFLOAT),
				Position(Vec3::ZERO),
				preset(Sound_Presets::DEFAULT)
			{}

			Desc(const Desc&_other)
				:Paused(_other.Paused),
				Stoped(_other.Stoped),
				Looped(_other.Looped),
				Relative(_other.Relative),
				Gain(_other.Gain),
				RollOffFactor(_other.RollOffFactor),
				MaxDistance_in_Meter(_other.MaxDistance_in_Meter),
				MinDistance_in_Meter(_other.MinDistance_in_Meter),
				Position(_other.Position),
				preset(_other.preset),
				alID(_other.alID),
				sound(_other.sound) {}

			Desc &operator=(const Desc &_other)
			{
				if (this != &_other)
				{
					Paused = _other.Paused;
					Stoped = _other.Stoped;
					Looped = _other.Looped;
					Relative = _other.Relative;
					Gain = _other.Gain;
					RollOffFactor = _other.RollOffFactor;
					MaxDistance_in_Meter = _other.MaxDistance_in_Meter;
					MinDistance_in_Meter = _other.MinDistance_in_Meter;
					Position = _other.Position;
					preset = _other.preset;
					alID = _other.alID;
					sound = _other.sound;
				}
				return *this;
			}

			~Desc()
			{
				SAFE_RELEASE_ADV(sound, sound->desc.path.c_str());
			}

			String GetPath() {
				if (sound == nullptr || sound && sound->desc.path.empty())
				{
					ASSERT(false, "Invalid");
					return "INVALID";
				}

				return sound->desc.path;
			}
		private:
			unsigned int				alID;
			ALSound*					sound = nullptr;
			friend ALSoundSource;
		};
	public:
		ALSoundSource(const String &sound);
		/**
		Desc:Not avalible in script
		*/
		ALSoundSource(ALSound*sound);
		~ALSoundSource();

		/**
		Desc:
		*/
		static ALSoundSource* CreateWithSoundSourcePreset(const String &sound);
		static ALSoundSource* CreateWithSoundSourcePreset(ALSound* sound);

		/**
		Desc:
		*/
		static ALSoundSource* CreateWithBackGroundMusicPreset(const String &sound);
		static ALSoundSource* CreateWithBackGroundMusicPreset(ALSound* sound);

		static ALSoundSource* ConstructDesc(const Desc&_desc, const String&name);

		void Play();
		void Stop();
		void Pause();
		bool isPlaying();

		void SetLooping(bool loop);
		void SetRelative(bool relative);

		void SetGain(TimeDelta gain);
		void SetRolloffFactor(TimeDelta rolloffFactor);

		//Attenuation
		void SetMaxDistance(TimeDelta _maxDistance_in_Meter);
		void SetMinDistance(TimeDelta _minDistance_in_Meter);

		void SetPosition(const Vec3 &position);

		void LoadSoundSourcePreset();
		void LoadBackgroundPreset();

		ENGINE_INLINE ALSound* GetSound() const { return desc.sound; }
		ENGINE_INLINE Desc GetDesc() const { return desc; }

		void SetDescAndApply(const Desc& _desc);
		ENGINE_INLINE void SetPreset(Desc::Sound_Presets _pr) { desc.preset = _pr; }
	private:
		ALSoundSource() = delete;
		void _CreateFromSound(ALSound* _sound);

	private:
		friend ALSound::Desc;
		friend ALSoundSource::Desc;
		Desc desc;
		MT::Mutex mutex;
	};
}