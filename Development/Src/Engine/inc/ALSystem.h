/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//***************************************************************************
#include "../../Core/inc/IncludesAndLibs.h"
//***************************************************************************
#include "../../Common/IAudio.h"
#include "ALSound.h"
#include "ALSoundSource.h"
//***************************************************************************

namespace NGTech
{
	class AudioUpdateJob;
	class Camera;

	/**
	*/
	struct AudioExt
	{
		bool Init();
		void ReportStatusExtensions();

		// Check extension
		static int CheckExtension(ALCdevice* _device, const char* _name);

		// Check OpenAL errors
		static int  CheckErrors();
		static int  CheckErrors(int _result);
		static int  CheckContextErrors(ALCdevice*_device);
		static int  CheckContextErrors(int _result);

		//TODO:������� �����
		bool FormatQUAD16IsSupported() { return EXT_AL_FORMAT_QUAD16 != 0; }

		// multi-channel formats
		static unsigned int EXT_AL_FORMAT_QUAD16;
		static unsigned int EXT_AL_FORMAT_51CHN16;
		static unsigned int EXT_AL_FORMAT_61CHN16;
		static unsigned int EXT_AL_FORMAT_71CHN16;
	};

	//---------------------------------------------------------------------------
	//Desc: Engine`s main sound system. Created one time
	//---------------------------------------------------------------------------
	class ENGINE_API ALSystem : public I_Audio {
	public:
		ALSystem(bool _singleThread, Engine* _engine);
		virtual ~ALSystem();
		/**
		*/
		virtual void Initialise() override;
		virtual void Update() override;
		/**
		*/
		virtual String GetVendor() override;
		virtual String GetRenderer() override;
		virtual String GetVersion() override;
		virtual String GetExtensions() override;
		/**
		*/
		virtual void SetListener(Vec3 pos, Vec3 dir) override;
		void SetListener(Camera* _cam);
		/**
		*/
		bool CheckExtension(const char*_name);
		/**
		*/
		ENGINE_INLINE int hasEXTEfx() const { return has_ext_efx; }
		ENGINE_INLINE int hasEFXFilter() const { return has_efx_filter; }
		ENGINE_INLINE int hasEFXReverb() const { return has_efx_reverb; }
		ENGINE_INLINE int hasEAXReverb() const { return has_eax_reverb; }

	private:
		ALSystem();
		void _InitThread();
		void _DestroyThread();
		static void _UpdateLoop(void*);

	private:
		AudioExt audioExt;
		MT::Thread updateThread;

		int has_ext_efx;						// efx extension
		int has_efx_filter;						// efx filter extension
		int has_efx_reverb;						// efx reverb extension
		int has_eax_reverb;						// eax reverb extension

		bool m_UseSingleThread = false;
		bool m_NeedlyDestroyThread = false;

		TimeDelta time;							// current time
		TimeDelta total_time;					// total time

		Mat4 listener_itransform;	// listener transformation
		Mat3 listener_irotation;	// listener rotation
		Vec3 listener_position;		// listener position
		Vec3 listener_velocity;		// listener velocity

		ALCcontext *alContext = nullptr;
		ALCdevice  *alDevice = nullptr;
		Engine	   *mEngine = nullptr;
	};
}