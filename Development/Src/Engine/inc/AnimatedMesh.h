/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include "ObjectMesh.h"

namespace NGTech
{
	/*
	@ ����� ������������� ���������
	*/
	struct AnimState
	{
		int from = -1;
		int to = -1;
		String name = "Invalid AnimState Name";

		AnimState(int _from, int _to, String _name) :from(_from), to(_to), name(_name)
		{}

		~AnimState()
		{
			name.clear();
		}
	};

	static AnimState DefaultAnimState = { 0, 0, "default" };

	/*
	@availability in script: yes
	*/
	class ENGINE_API AnimatedMesh : public ObjectMesh
	{
		//!@todo Uncomment this
		//CLASS_AVALIBLE_IN_EDITOR(AnimatedMesh);
		std::vector<std::shared_ptr<AnimState>> m_AnimStates;
		AnimState				m_ActivatedAnimState = DefaultAnimState;
	public:
		void RegisterAnimState(const std::shared_ptr<AnimState>& _state)
		{
			ASSERT(_state, "Invalid AnimState");
			if (_state == nullptr)
				return;

			m_AnimStates.push_back(_state);
		}

		// ������� ������� ���������
		void CreateAnimStateManually(int _from, int _to, String _name)
		{
			auto state = std::make_shared<AnimState>(_from, _to, _name);
			RegisterAnimState(state);
		}

		// �� �������� ��� �������� �������������� �������� ������ �� ������� frames

		void PrintListAnimStates()
		{
			for (const auto&ptr : m_AnimStates)
			{
				if (ptr == nullptr)
				{
					Warning("Detected invalid AnimState");
					continue;
				}

				LogPrintf("Detected AnimState: from %i, to %i, name %s", ptr->from, ptr->to, ptr->name.c_str());
			}
		}
	public:
		explicit AnimatedMesh(const String &path);
		virtual ~AnimatedMesh();

		virtual void LoadFromPath(const String &_path) override;
		/**
		draws object subset
		*/
		virtual void DrawSubset(size_t s, TimeDelta _delta) override;

		virtual const Vec3& GetMax() override;
		virtual const Vec3& GetMin()  override;
		virtual const BBox& GetBBox() override;

		virtual const Vec3& GetCenter() override;
		virtual float GetRadius()      override;

		virtual const Vec3& GetMax(unsigned int s)	 override;
		virtual const Vec3& GetMin(unsigned int s)	 override;
		virtual const Vec3& GetCenter(unsigned int s) override;
		virtual float GetRadius(unsigned int s)      override;

		/*Will call DrawSubset with OQ*/
		virtual bool RenderQuery(I_RenderLowLevel* _render, size_t _index, TimeDelta _delta) override;
		/*Will call DrawSubset only*/
		virtual bool RenderFast(I_RenderLowLevel* _render, size_t _index, TimeDelta _delta) override;

		/**
		get BBox of the object subset
		*/
		virtual const BBox& GetBBox(unsigned int subset) override;

		/**
		get BSphere of the object subset
		*/
		virtual const BSphere& GetBSphere(unsigned int subset) override;

		/**
		get subset material by number
		*/
		virtual Material* GetMaterial(unsigned int subset) const override;

		/*
		Not virtual
		*/
		void SetAnimationFrame(int frame, int from = -1, int to = -1);
		void SetMaterial(const String &path, const String &subset = "*");
		void PlayAnimationUpdate(TimeDelta _dtime);

		virtual ObjectType GetType() override { return OBJECT_SKELETEAL_MESH; }

		ENGINE_INLINE bool IsPlaying() { return mIsPlaying; }

		void SetPhysicsConvexHullImpl(TimeDelta mass)
		{
			Util_SetPhysicsConvexHull(tempdata.mesh.m_AnimatedMesh, mass, this, "AnimatedMesh");
		}
	private:
		void _AddToSceneList();
		void _RemoveFromScene();
		void _RemoveFromSceneAnimatedMesh();
	private:
		bool mIsPlaying = false;

		friend class RenderPipeline;
	};
}