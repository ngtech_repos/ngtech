/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#ifndef PHYSBODY_H
#include "PhysBody.h"
#endif

namespace NGTech
{
	class ALSound;
	class PhysBody;

	struct ENGINE_API PhysicsObjectData
	{
		// Only Raw Pointers - temp data
		PhysBody* m_AttachedPBody = nullptr;
		bool bMultipart = false;

		PhysBodyDescSh m_PhysDesc;

		PhysicsObjectData()
		{}

		~PhysicsObjectData()
		{
			SetPhysicsNone();
		}

		void SetPhysXBodyTransform(const Mat4 & _trans);
		void SetPhysicsNone();
	};

	/*
	Base class for Entities with Physics
	@availability in script: only as subpart
	*/
	template<class T>
	struct ENGINE_API BasePhysicsObjectEntity
	{
		BasePhysicsObjectEntity(T _parent) :parent(_parent), mPhysicsObjectData(new PhysicsObjectData())
		{}

		~BasePhysicsObjectEntity() {
			// SetPhysicsNone will called in ~dctor
			SAFE_DELETE(mPhysicsObjectData);
		}

		/*Only for physics simulations */
		ENGINE_INLINE bool GetMultipart() const { ASSERT(mPhysicsObjectData, "Invalid Pointer on mPhysicsObjectData");  if (!mPhysicsObjectData) return false;  return mPhysicsObjectData->bMultipart; }
		ENGINE_INLINE void SetMultipart(bool _m) { ASSERT(mPhysicsObjectData, "Invalid Pointer on mPhysicsObjectData"); if (!mPhysicsObjectData) return; mPhysicsObjectData->bMultipart = _m; }

		ENGINE_INLINE Vec3 GetCollisionSize() { auto ph = GetPhysBody(); if (!ph) return Vec3::ZERO; return ph->GetCollisionSize(); }
		ENGINE_INLINE float GetCollisionRad() { auto ph = GetPhysBody(); if (!ph) return Math::ZEROFLOAT; return ph->GetCollisionRad(); }
		ENGINE_INLINE float GetCollisionWidth() { auto ph = GetPhysBody(); if (!ph) return Math::ZEROFLOAT; return ph->GetCollisionWidth(); }
		ENGINE_INLINE float GetCollisionHeight() { auto ph = GetPhysBody(); if (!ph) return Math::ZEROFLOAT; return ph->GetCollisionHeight(); }
		ENGINE_INLINE float GetCollisionMass() { auto ph = GetPhysBody(); if (!ph) return Math::ZEROFLOAT; return ph->GetMass(); }

		ENGINE_INLINE void SetPhysicsByDesc(const PhysBodyDescSh &_desc) {
			ASSERT(mPhysicsObjectData, "Invalid pointer on mPhysicsObjectData");
			if (!mPhysicsObjectData) return;
			if (mPhysicsObjectData->m_PhysDesc == _desc) return;

			mPhysicsObjectData->m_PhysDesc = _desc;

			switch (_desc.mCollisionType)
			{
			case PhysBodyDescSh::CollisionType::CT_NONE:
				SetPhysicsNone();
				break;

			case PhysBodyDescSh::CollisionType::CT_BOX:
				_setPhysicsBox(_desc.CollisionSize, _desc.CollisionMass, _desc.TransformTmp);
				break;

			case PhysBodyDescSh::CollisionType::CT_SPHERE:
				_setPhysicsSphere(_desc.CollisionRadius, _desc.CollisionMass, _desc.TransformTmp);
				break;

			case PhysBodyDescSh::CollisionType::CT_CYLINDER:
				_setPhysicsCylinder(_desc.CollisionMass, _desc.CollisionWidth, _desc.CollisionMass, _desc.TransformTmp);
				break;

			case PhysBodyDescSh::CollisionType::CT_CAPSULE:
				_setPhysicsCapsule(_desc.CollisionRadius, _desc.CollisionHeight, _desc.CollisionMass, _desc.TransformTmp);
				break;

			case PhysBodyDescSh::CollisionType::CT_CONVEX:
				SetPhysicsConvexHull(_desc.CollisionMass);
				break;

			case PhysBodyDescSh::CollisionType::CT_STATIC:
				SetPhysicsStaticMesh();
				break;
			default:
				ErrorWrite("[ObjectMesh::SetPhysicsByDesc] Invalid type. Collision not applied");
			}
		}

		ENGINE_INLINE void ReplacePhysicsDesc(const PhysBodyDescSh &_desc)
		{
			SetPhysicsNone();
			SetPhysicsByDesc(_desc);
		}

		ENGINE_INLINE PhysBody *GetPhysBody() { ASSERT(mPhysicsObjectData, "Invalid Pointer on mPhysicsObjectData"); return mPhysicsObjectData->m_AttachedPBody; }
		ENGINE_INLINE void SetPhysBody(PhysBody*pb) { ASSERT(mPhysicsObjectData, "Invalid Pointer on mPhysicsObjectData"); mPhysicsObjectData->m_AttachedPBody = pb; }

		// Must be overloaded - is needed mesh info
		ENGINE_INLINE void SetPhysicsConvexHull(TimeDelta mass) {
			if (parent)
				parent->_SetPhysicsConvexHullImpl(mass);
		}

		// Must be overloaded - is needed mesh info
		ENGINE_INLINE void SetPhysicsStaticMesh() {
			if (parent)
				parent->_SetPhysicsStaticMeshImpl();
		}

		ENGINE_INLINE void SetPhysicsNone() {
			ASSERT(mPhysicsObjectData, "Invalid pointer on mPhysicsObjectData");
			if (!mPhysicsObjectData) return;

			mPhysicsObjectData->SetPhysicsNone();
		}

		ENGINE_INLINE PhysBodyDescSh::CollisionType GetCollisionType()
		{
			ASSERT(mPhysicsObjectData, "Invalid pointer on mPhysicsObjectData");
			if (!mPhysicsObjectData) return PhysBodyDescSh::CollisionType::CT_NONE;

			ASSERT(mPhysicsObjectData->m_AttachedPBody, "Invalid pointer on mPhysicsObjectData->m_AttachedPBody");
			if (mPhysicsObjectData->m_AttachedPBody == nullptr) return PhysBodyDescSh::CollisionType::CT_NONE;

			return mPhysicsObjectData->m_AttachedPBody->GetCollisionType();
		}


		ENGINE_INLINE void SetSaveAsMeshNamePhysX(const String&_mSaveAsMeshName) {
			ASSERT(mPhysicsObjectData, "Invalid pointer on mPhysicsObjectData");
			if (!mPhysicsObjectData) return;
			mPhysicsObjectData->m_PhysDesc.mSaveAsMeshNameTmp = _mSaveAsMeshName;
		}

		ENGINE_INLINE void SetOriginalMeshNamePhysX(const String&_mSaveAsMeshName) {
			ASSERT(mPhysicsObjectData, "Invalid pointer on mPhysicsObjectData");
			if (!mPhysicsObjectData) return;
			mPhysicsObjectData->m_PhysDesc.mOriginalMeshNameTmp = _mSaveAsMeshName;
		}

		ENGINE_INLINE String GetSaveAsMeshNamePhysX() const {
			ASSERT(mPhysicsObjectData, "Invalid pointer on mPhysicsObjectData");
			if (!mPhysicsObjectData) return StringHelper::EMPTY_STRING;
			return mPhysicsObjectData->m_PhysDesc.mSaveAsMeshNameTmp;
		}

		ENGINE_INLINE bool SaveAsMeshNameEmptyPhysX() {
			ASSERT(mPhysicsObjectData, "Invalid pointer on mPhysicsObjectData");
			if (!mPhysicsObjectData) return true;
			return mPhysicsObjectData->m_PhysDesc.mSaveAsMeshNameTmp.empty();
		}

		ENGINE_INLINE bool OriginalMeshNameEmptyPhysX() {
			ASSERT(mPhysicsObjectData, "Invalid pointer on mPhysicsObjectData");
			if (!mPhysicsObjectData) return true;
			return mPhysicsObjectData->m_PhysDesc.mOriginalMeshNameTmp.empty();
		}

		ENGINE_INLINE String GetOriginalMeshNamePhysX() const {
			ASSERT(mPhysicsObjectData, "Invalid pointer on mPhysicsObjectData");
			if (!mPhysicsObjectData) return StringHelper::EMPTY_STRING;
			return mPhysicsObjectData->m_PhysDesc.mOriginalMeshNameTmp;
		}

		///!@ Called for setting position ONLY PhysX body
		ENGINE_INLINE void SetPhysXBodyTransform(const Mat4 & _trans) {
			ASSERT(mPhysicsObjectData, "Invalid pointer on mPhysicsObjectData");
			if (!mPhysicsObjectData) return;

			mPhysicsObjectData->SetPhysXBodyTransform(_trans);
		}

		///!@ Called for setting position ONLY PhysX body
		ENGINE_INLINE void _setPhysicsBox(const Vec3 & size, TimeDelta mass, const Mat4 & _trans) {
			// TODO: ����������� �������� DEFAULT_DENSITY
			_ReplacePointer(PhysBody::CreateBox(size, _trans, mass, PhysBodyDescSh::DEFAULT_DENSITY));
		}

		ENGINE_INLINE void _setPhysicsSphere(TimeDelta rad, TimeDelta mass, const Mat4 & _trans) {
			_ReplacePointer(PhysBody::CreateSphere(rad, _trans, mass, PhysBodyDescSh::DEFAULT_DENSITY));
		}
		ENGINE_INLINE void _setPhysicsCylinder(TimeDelta radius, TimeDelta width, TimeDelta mass, const Mat4 & _trans) {
			_ReplacePointer(PhysBody::CreateCylinderConvex(radius, width, _trans, mass, PhysBodyDescSh::DEFAULT_DENSITY));
		}

		ENGINE_INLINE void _setPhysicsCapsule(TimeDelta radius, TimeDelta height, TimeDelta mass, const Mat4 & _trans) {
			_ReplacePointer(PhysBody::CreateCapsule(radius, height, _trans, mass, PhysBodyDescSh::DEFAULT_DENSITY));
		}

		ENGINE_INLINE const Mat4 GetTransformPhysXBody() const {
			ASSERT(mPhysicsObjectData, "Invalid pointer on mPhysicsObjectData");

			if ((mPhysicsObjectData == nullptr) || (mPhysicsObjectData && mPhysicsObjectData->m_AttachedPBody == nullptr))
				return Mat4::ZERO;

			return mPhysicsObjectData->m_AttachedPBody->GetTransform();
		}

		ENGINE_INLINE bool IsValidPhysXBody() const {
			// ��� ASSERT �� �����, ��� ����� ����� ������ �������������� ��� �������� Entity � ���������
			// ASSERT(mPhysicsObjectData, "Invalid pointer");

			if (mPhysicsObjectData == nullptr)
				return false;

			return mPhysicsObjectData->m_AttachedPBody != nullptr;
		}

		PhysicsObjectData * mPhysicsObjectData = nullptr;

	private:
		// TODO: �� �� ��������� ������, �� �� �������� ������ ������� � Desc ��������
		ENGINE_INLINE void _ReplacePointer(PhysBody*_newPointer)
		{
			ASSERT(_newPointer, "Invalid pointer on _newPointer");
			if (!_newPointer) return;

			ASSERT(mPhysicsObjectData, "Invalid pointer on mPhysicsObjectData");
			if (!mPhysicsObjectData) return;

			auto older = mPhysicsObjectData->m_AttachedPBody;
			mPhysicsObjectData->m_AttachedPBody = _newPointer;

			SAFE_DELETE(older);
		}

		T parent;
	};

	template<class T1, class T2>
	ENGINE_INLINE void Util_SetPhysicsConvexHull(T1 meshtype, TimeDelta mass, T2 owner, String _from)
	{
		ASSERT(meshtype, "Invalid pointer on meshtype");
		if (!meshtype) return;
		ASSERT(owner, "Invalid pointer on owner");
		if (!owner) return;

#if !DISABLE_PHYSICS
		size_t numVertices = 0;
		for (unsigned int i = 0; i < meshtype->GetNumSubsets(); i++) {
			numVertices += meshtype->GetSubset(i)->numVertices;
		}

		unsigned int size = meshtype->GetNumSubsets();

		// ����� ����������� ����� size<=0
		ASSERT(size >= 0, "[Util_SetPhysicsConvexHull] Incorrect NumSubsets. From %s", _from.c_str()); //-V547
		if (size <= 0) {
			Error("[Util_SetPhysicsConvexHull] Incorrect NumSubsets. From " + _from, false);
			return;
		}

		PhysBody* pb = new PhysBody[size]();

		// ����������� ����, ��� �� ������������ PhysX Cooking
		//!@todo ������� ��� � ��������� �������
		auto _mLoadingPath = owner->GetLoadingPath();

		if (owner->m_PhysicsEntity)
		{
			if (owner->m_PhysicsEntity->SaveAsMeshNameEmptyPhysX())
				owner->m_PhysicsEntity->SetSaveAsMeshNamePhysX(_mLoadingPath);

			if (owner->m_PhysicsEntity->OriginalMeshNameEmptyPhysX())
				owner->m_PhysicsEntity->SetOriginalMeshNamePhysX(_mLoadingPath);
		}

		for (unsigned int i = 0; i < size; i++)
		{
			// ��������� ����� pb.size() < i
			ASSERT(size > i, "Size: %i, req: %i", size, i);
			String subsetItName = " " + std::to_string(i);

			// TODO: Implement DEFAULT_DENSITY as parametr
			if (!owner->m_PhysicsEntity)
				continue;

			pb[i] = *PhysBody::CreateConvexHull(meshtype->GetSubset(i)->numVertices, meshtype->GetSubset(i)->numIndices, owner->mState.transform, meshtype->GetSubset(i)->vertices, meshtype->GetSubset(i)->indices, mass, PhysBodyDescSh::DEFAULT_DENSITY,
				owner->m_PhysicsEntity->GetSaveAsMeshNamePhysX() + subsetItName, owner->m_PhysicsEntity->GetOriginalMeshNamePhysX() + subsetItName);
		}

		if (owner->m_PhysicsEntity)
		{
			owner->m_PhysicsEntity->SetPhysBody(pb);
			owner->m_PhysicsEntity->SetMultipart(true);
		}
#endif
	}
}