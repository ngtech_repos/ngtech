/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

namespace NGTech
{
	class ALSound;
	class PhysBody;

	/**
	Utilte class. Without memory cleaning
	*/
	struct ENGINE_API MaterialSubset
	{
		Material*					material = nullptr;
		String						materialName = StringHelper::EMPTY_STRING;
		String						subsetName = StringHelper::EMPTY_STRING;

		~MaterialSubset();
	};

	struct BaseVisualObjectEntityTempData
	{
		Vec3 Max = Vec3::ZERO;
		Vec3 Min = Vec3::ZERO;
		Vec3 Center = Vec3::ZERO;
		BBox Box = BBox();
		BSphere Sphere = BSphere();
		TimeDelta Radius = Math::ZERODELTA;
		String SubsetName = StringHelper::EMPTY_STRING;

		union {
			struct MeshType
			{
				Mesh*			 m_StaticMesh = nullptr;
				SkinnedMesh*     m_AnimatedMesh = nullptr;
			} mesh;
		};

		RenderLayer mRenderLayer = RenderLayer::LAYER_1;

		I_OcclusionQuery* query = nullptr;

		std::vector<MaterialSubset> materials;
		std::vector<MaterialSubset> OldMaterials;

		unsigned int mNumSubsets = 1;
		String mLoadingPath = StringHelper::EMPTY_STRING;

		/**
		*/
		BaseVisualObjectEntityTempData()
		{
			materials.clear();
			OldMaterials.clear();
		}

		ENGINE_INLINE void _Swap(const BaseVisualObjectEntityTempData &ob) {
			if (this != &ob) {
				mesh = ob.mesh;
				mRenderLayer = ob.mRenderLayer;
				query = ob.query;
				materials = ob.materials;
				OldMaterials = ob.OldMaterials;
				mNumSubsets = ob.mNumSubsets;
				mLoadingPath = ob.mLoadingPath;
			}
		}
	};

	/*
	Base class for Entities with Visual
	@availability in script: only as subpart
	*/
	class ENGINE_API BaseVisualObjectEntity :protected EditableObjectExtension
	{
		CLASS_AVALIBLE_IN_EDITOR(BaseVisualObjectEntity);
	public:
		BaseVisualObjectEntity()
		{
			SetRenderLayer(LAYER_1);
			_AddToScene();
		}

		BaseVisualObjectEntity(const BaseVisualObjectEntity&ob) {
			_Swap(ob);
		}

		BaseVisualObjectEntity &operator =(const BaseVisualObjectEntity &ob) {
			_Swap(ob);
			return *this;
		}

		virtual ~BaseVisualObjectEntity() {}

		ENGINE_INLINE virtual void LoadFromPath(const String &_path) {
			ASSERT("[BaseVisualObjectEntity::LoadFromPath(const String &_path)] must be overloaded");
		}

		/**
		Renderable
		*/
		ENGINE_INLINE virtual Material* GetMaterial(unsigned int s) const {
			ASSERT("[BaseVisualObjectEntity::GetMaterial(unsigned int s) const] must be overloaded");
			return nullptr;
		}
		ENGINE_INLINE virtual void SetMaterial(const String &path, const String &subsetName = "*") {
			ASSERT("[BaseVisualObjectEntity::SetMaterial(const String &path, const String &subsetName) const] must be overloaded");
		}
		/*Setting material and loading Immediately*/
		ENGINE_INLINE virtual void SetMaterial(Material *mat, const String &subsetName = "*") {
			ASSERT("[BaseVisualObjectEntity::SetMaterial(const Material &mat, const String &subsetName) const] must be overloaded");
		}

		ENGINE_INLINE virtual void DrawSubset(size_t s, TimeDelta _delta) {
			ASSERT("[BaseVisualObjectEntity::DrawSubset] must be overloaded");
		}

		ENGINE_INLINE virtual const Vec3& GetMax() {
			ASSERT("[BaseVisualObjectEntity::GetMax] must be overloaded");
			return tempdata.Max;
		}

		ENGINE_INLINE virtual const Vec3& GetMin() {
			ASSERT("[BaseVisualObjectEntity::GetMin] must be overloaded");
			return tempdata.Min;
		}

		ENGINE_INLINE virtual const Vec3& GetCenter() {
			ASSERT("[BaseVisualObjectEntity::GetCenter] must be overloaded");
			return tempdata.Center;
		}

		ENGINE_INLINE virtual float GetRadius() {
			ASSERT("[BaseVisualObjectEntity::GetRadius] must be overloaded");
			return Math::ZEROFLOAT;
		}
		ENGINE_INLINE virtual float GetRadius(unsigned int s) {
			ASSERT("[BaseVisualObjectEntity::GetRadius(unsigned int s)] must be overloaded");
			return Math::ZEROFLOAT;
		}

		ENGINE_INLINE virtual const Vec3& GetMax(unsigned int s) {
			ASSERT("[BaseVisualObjectEntity::GetMax(unsigned int s)] must be overloaded");
			return tempdata.Max;
		}
		ENGINE_INLINE virtual const Vec3& GetMin(unsigned int s) {
			ASSERT("[BaseVisualObjectEntity::GetMin(unsigned int s)] must be overloaded");
			return tempdata.Min;
		}
		ENGINE_INLINE virtual const Vec3& GetCenter(unsigned int s) {
			ASSERT("[BaseVisualObjectEntity::GetCenter(unsigned int s)] must be overloaded");
			return tempdata.Center;
		}

		ENGINE_INLINE virtual const String& GetSubsetName(unsigned int id) {
			ASSERT("[BaseVisualObjectEntity::GetSubsetName(unsigned int s)] must be overloaded");
			return tempdata.SubsetName;
		}

		// query
		/*Activation Occlusion Query - can be slow*/
		ENGINE_INLINE virtual void UseHWOcclusionQuery(bool q) {
			ASSERT("[BaseVisualObjectEntity::UseHWOcclusionQuery] must be overloaded");
		}

		ENGINE_INLINE virtual bool UseQuery() const {
			ASSERT("[BaseVisualObjectEntity::UseQuery] must be overloaded");
			return false;
		}

		ENGINE_INLINE virtual bool HasQuery() const {
			ASSERT("[BaseVisualObjectEntity::HasQuery] must be overloaded");
			return 0;
		}

		ENGINE_INLINE virtual bool RenderQuery(I_RenderLowLevel*, size_t _index, TimeDelta _delta) {
			ASSERT("[BaseVisualObjectEntity::RenderQuery] must be overloaded");
			return false;
		}

		ENGINE_INLINE virtual bool RenderFast(I_RenderLowLevel*, size_t _index, TimeDelta _delta) {
			ASSERT("[BaseVisualObjectEntity::RenderFast] must be overloaded");
			return false;
		}

		ENGINE_INLINE virtual const BBox& GetBBox() {
			ASSERT("[BaseVisualObjectEntity::GetBBox] must be overloaded");
			return tempdata.Box;
		}

		ENGINE_INLINE virtual const BSphere& GetBSphere() {
			ASSERT("[BaseVisualObjectEntity::GetBSphere] must be overloaded");
			return tempdata.Sphere;
		}

		ENGINE_INLINE virtual const BBox& GetTransformedBBox() {
			ASSERT("[BaseVisualObjectEntity::GetTransformedBBox] must be overloaded");
			return tempdata.Box;
		}

		ENGINE_INLINE virtual const BSphere& GetTransformedBSphere() {
			ASSERT("[BaseVisualObjectEntity::GetTransformedBSphere] must be overloaded");
			return tempdata.Sphere;
		}

		/**
		get BBox of the object subset
		*/
		ENGINE_INLINE virtual const BBox& GetBBox(unsigned int subset) {
			ASSERT("[BaseVisualObjectEntity::GetBBox] must be overloaded");
			return tempdata.Box;
		}
		/**
		get BSphere of the object subset
		*/
		ENGINE_INLINE virtual const BSphere& GetBSphere(unsigned int subset) {
			ASSERT("[BaseVisualObjectEntity::GetBSphere] must be overloaded");
			return tempdata.Sphere;
		}

		/*Render Layer*/
		ENGINE_INLINE RenderLayer GetRenderLayer() const {
			return tempdata.mRenderLayer;
		}
		ENGINE_INLINE void SetRenderLayer(RenderLayer _v) {
			tempdata.mRenderLayer = _v;
		}

		/**
		get number of object subsets
		*/
		ENGINE_INLINE virtual unsigned int GetNumSubsets() const {
			return tempdata.mNumSubsets;
		};

		ENGINE_INLINE std::vector<MaterialSubset>& GetMaterials() {
			return tempdata.materials;
		}

		ENGINE_INLINE virtual String GetMaterialsAsString() {
			auto mats = GetMaterials();
			String matName;

			for (auto it = mats.begin(); it != mats.end(); ++it)
			{
				matName += (*it).materialName.c_str();
				auto index = it - mats.begin();
				if (index < mats.size() - 1)
					matName += ", ";
			}
			return matName;
		}

		ENGINE_INLINE String		GetLoadingPath() const {
			return tempdata.mLoadingPath;
		}
		ENGINE_INLINE void			SetLoadingPath(const String& _s) {
			tempdata.mLoadingPath = _s;
		}

		// IMPORTANT: Only copy
		virtual Mat4 GetTransform() {
			return Mat4();
		}

		void _AddToScene();
		void _DeleteFromScene();
	private:
		ENGINE_INLINE void _Swap(const BaseVisualObjectEntity &_obj) {
			if (this != &_obj) {
				tempdata = _obj.tempdata;
			}
		}
	protected:
		BaseVisualObjectEntityTempData tempdata;
		friend class RenderPipeline;
	};

	/*Util function, used in .dector only*/
	void DeleteMatFromList(std::vector<MaterialSubset> &materials, const std::shared_ptr<Cache> &_cache);
}