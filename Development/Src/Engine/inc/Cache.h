/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//**************************************
#include <vector>
#include <unordered_map>
#include <memory>
//**************************************
#include "../../Common/StringHelper.h"
#include "CacheNode.h"
//**************************************
#include "Material.h"
//**************************************

namespace NGTech
{
	class Mesh;
	class Material;
	class ALSound;

	/**/
	struct ENGINE_API Cache {
		/*!@ Recompile All Shaders*/
		void ReloadShaders();
		/*!@ Reload All Textures*/
		void ReloadTextures();
		/*!@ Reload All Meshes*/
		void ReloadMeshes();
		/*!@ Reload All Animated Meshes*/
		void ReloadAnimatedMeshes();
		/*!@ Reload All Sounds*/
		void ReloadSounds();
		/*!@ Reload All resources*/
		void ReloadAll();
		/*!@ Delete All resources*/
		void DestroyAllResources();
		/*
		!@ Marks all unused resources as ready for delete
		Loading new scene and delete unused
		*/
		void DestroyAllUnUsedResources();

		// TODO: Implement this
		void CancelLoading() {}

		static ALSound* LoadSound(const String& _path);
		static I_Shader* LoadShader(const String& _path, const String& _defines);
		static Mesh* LoadModel(const String& _path);
		static SkinnedMesh* LoadSkinnedMesh(const String& _path);
		static Material* LoadMaterial(const String& _name);
		static I_Texture* LoadTexture(const std::shared_ptr<TextureParamsTransfer>&_obj);

		static void DeleteResource(I_Texture* _texture);
		static void DeleteResource(I_Shader* _shader);

		static void DeleteResource(SkinnedMesh* _mesh);
		static void DeleteResource(Mesh* _mesh);
		static void DeleteResource(Material* _material);

		[[deprecated]]
		ENGINE_INLINE bool LoadingIsFinished() { return m_LoadingIsFinished; }
	private:
		ALSound* _LoadSoundUtil(const String& _path);
		I_Shader* _LoadShaderUtil(const String &_path, const String& _defines);
		Mesh* _LoadModelUtil(const String& _path);
		SkinnedMesh* _LoadSkinnedMeshUtil(const String& _path);
		I_Texture* _LoadTextureUtil(const std::shared_ptr<TextureParamsTransfer>&_obj);
	private:
		CacheNode<Mesh*, void*, CacheReleaseCallback<Mesh*>>
			models;
		CacheNode<SkinnedMesh*, void*, CacheReleaseCallback<SkinnedMesh*>>
			skinnedmodels;
		CacheNode<ALSound*, void*, CacheReleaseCallback<ALSound*>>
			sounds;
		CacheNode<Material*, void*, CacheReleaseCallback<Material*>>
			materials;
		CacheNode<I_Texture*, const std::shared_ptr<TextureParamsTransfer>&, CacheReleaseCallback<I_Texture*>>
			textures;
		CacheNode<I_Shader*, const String &, CacheReleaseCallback<I_Shader*>>
			shaders;
	private:
		bool m_useCacheSystem = true;
		[[deprecated]]
		bool m_LoadingIsFinished = false;
	};
}