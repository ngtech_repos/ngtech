/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//**************************************
#include <vector>
#include <unordered_map>
#include <memory>
//**************************************

namespace NGTech
{
	class Mesh;
	class Material;
	class ALSound;
	struct TextureParamsTransfer;

	template <class TObject>
	struct ENGINE_API CacheReleaseCallback
	{
		template <class TContainer>
		constexpr void Action(TObject _object, TContainer _container)
		{
			ASSERT(_object, "Invalid pointer");
			if (!_object) return;

			// ��� ��� � ������ ���� :) - ������������ ��� ���, ��� �������� �����������
			// ��������� ����� material->referenceCounter < 0 ��� ��� ����� ����� ������ ==0
			//ASSERT((_object->RefCounter() > 0) || (_object->RefCounter() != 0), "Cache: FreeResource: referenceCounter < 0. or == 0. Count %i", _object->RefCounter());
			//if (_object->RefCounter() == 0)

			{
				auto path = _object->GetPath();
				ASSERT(!path.empty(), "Invalid path");
				if (path.empty()) return;

				auto it = _container.find(path);
				if (it != _container.end())
				{
					DebugM("AHTUNG. Remove restype: %s, from path: %s", _object->GetResourceType().c_str(), path.c_str());
					_container.erase(it);
					return;
				}
				else
				{
					// Not found
					DebugM("AHTUNG!AHTUNG! Not found restype: %s, from path: %s", _object->GetResourceType().c_str(), path.c_str());
				}
				// ������ ������ ��� ��������� ������ ���� �������� ������ ReleaseWithCallback
			}
		}
	};

	template <class T, class Desc, class _Callback>
	struct ENGINE_API CacheNode
	{
		T LoadObj(const String &_path, Desc _userData, void *_defaultResource, bool _LoadImmedly);

		~CacheNode() {
			Clean();
		}

		/*!@ Delete resources in container*/
		constexpr void DeleteResource(T _obj)
		{
			ASSERT(_obj, "Invalid pointer on _obj");
			if (!_obj) return;
			// �������� ������ �� ����� - ������ ������� ���������.
			_obj->ReleaseWithCallback(_Callback, _CacheContainer, _obj);
			// TODO: ��������� ������ - ������� ���������� ���������
		}

		constexpr void AddToDequeDeferredLoading(T _obj) {
			// TODO: implement this
			if (_obj)
				_obj->Load();
		}

		constexpr void AddToDequeImmedlyLoading(T _obj) {
			// TODO: implement this
			if (_obj)
				_obj->Load();
		}

		/*!@ Load all resources in container, immedly. Currently is singlethread*/
		constexpr void LoadImmedly()
		{
			for (const auto&all : _CacheContainer)
			{
				auto ptr = all.second;
				if (!ptr) continue;

				ptr->Load();
			}
		}

		/*!@ Clean all resources in container*/
		constexpr void Clean()
		{
			for (const auto&all : _CacheContainer)
			{
				auto ptr = all.second;
				if (!ptr) continue;

				DeleteResource(ptr);
			}
			_CacheContainer.clear();
		}

		constexpr void DestroyAllUnUsedResources()
		{
			for (const auto&all : _CacheContainer)
			{
				auto ptr = all.second;
				if (!ptr) continue;

				MarkAsReadyForDelete(ptr);
			}
		}

		constexpr void MarkAsReadyForDelete(T _obj)
		{
			if (!_obj) return;

			if (_obj->RefCounter() <= 0)
				DeleteResource(_obj);
		}

		CacheReleaseCallback<T>			_Callback;
		std::unordered_map<String, T>	_CacheContainer;
	};

	template<>
	ENGINE_INLINE I_Shader*
		CacheNode<I_Shader*, const String &, CacheReleaseCallback<I_Shader*>>::LoadObj(const String & _path, const String & _defines, void * _defaultResource, bool _LoadImmedly);

	template<>
	ENGINE_INLINE SkinnedMesh*
		CacheNode<SkinnedMesh*, void*, CacheReleaseCallback<SkinnedMesh*>>::LoadObj(const String & _path, void * _userData, void * _defaultResource, bool _LoadImmedly);

	template<>
	ENGINE_INLINE Mesh*
		CacheNode<Mesh*, void*, CacheReleaseCallback<Mesh*>>::LoadObj(const String & _path, void * _userData, void * _defaultResource, bool _LoadImmedly);

	template<>
	ENGINE_INLINE ALSound*
		CacheNode<ALSound*, void*, CacheReleaseCallback<ALSound*>>::LoadObj(const String & _path, void* _desc/*can be null. not used*/, void * _defaultResource, bool _LoadImmedly);

	template<>
	ENGINE_INLINE I_Texture*
		CacheNode<I_Texture*, const std::shared_ptr<TextureParamsTransfer>&, CacheReleaseCallback<I_Texture*>>::LoadObj(const String & _path, const std::shared_ptr<TextureParamsTransfer>& texP, void * _defaultResource, bool _LoadImmedly);

	template<>
	ENGINE_INLINE Material*
		CacheNode<Material*, void*, CacheReleaseCallback<Material*>>::LoadObj(const String & _path, void * _userData, void * _defaultResource, bool _LoadImmedly);

}