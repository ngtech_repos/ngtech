/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//**************************************
#include "Object.h"
//**************************************

namespace NGTech
{
	//Forward Declaration
	class Frustum;
	struct CharacterControllerDesc;

	/**
	Camera base class.
	Availability in script: yes
	*/
	class ENGINE_API Camera :public BaseEntity {
	public:
		enum CameraType
		{
			CAMERA_INVALID = 0,
			CAMERA_FPS,
			CAMERA_FREE,
			CAMERA_FIXED,
			CAMERA_CUSTOM_TYPE,
			CAMERA_COUNT
		};

	public:
		/**
		Creates new Camera.
		\param[in] _autoswitch AutoSwitch on creation.
		*/
		Camera(bool _automaticSwitch = false);

		/**
		Copies constructor.
		\param[in] _cam param on camera ref
		*/
		Camera(const Camera& _cam) {
			_Swap(_cam);
		}

		/**
		Destructor.
		*/
		virtual ~Camera();

		/**
		Returns type of BaseEntity.
		\return ObjectType type of BaseEntity.
		*/
		ENGINE_INLINE virtual ObjectType GetType() { /*MT::ScopedGuard guard(mutex);*/ return OBJECT_CAMERA; }

		/**
		Returns type of camera.
		\return CameraType type of camera.
		*/
		ENGINE_INLINE virtual CameraType GetCameraType() { /*MT::ScopedGuard guard(mutex);*/ return CAMERA_INVALID; }

		/**
		Gets camera direction. Must be copy for threading.
		\return Vec3 direction of camera.
		*/
		ENGINE_INLINE Vec3 GetDirection() const { /*MT::ScopedGuard guard(mutex);*/ return bufferData.direction; }

		virtual void SetPosition(const Vec3&_position) override { /*MT::ScopedGuard guard(mutex);*/ bufferData._position = _position; BaseEntity::SetPosition(_position); }
		ENGINE_INLINE virtual Vec3 GetPosition() const override { /*MT::ScopedGuard guard(mutex);*/ return bufferData._position; }

		ENGINE_INLINE void SetDirection(const Vec3 &_direction) { /*MT::ScopedGuard guard(mutex);*/ this->bufferData.direction = _direction; }

		ENGINE_INLINE float GetMaxVelocity() const { /*MT::ScopedGuard guard(mutex);*/ return maxVelocity; }

		ENGINE_INLINE void SetMaxVelocity(float _maxVelocity) { /*MT::ScopedGuard guard(mutex);*/ this->maxVelocity = _maxVelocity; }

		virtual const Vec3 &getMax(unsigned int s) { /*MT::ScopedGuard guard(mutex);*/ return Vec3::ZERO; }
		virtual const Vec3 &getMin(unsigned int s) { /*MT::ScopedGuard guard(mutex);*/ return Vec3::ZERO; }
		virtual const Vec3 &getCenter(unsigned int s) { /*MT::ScopedGuard guard(mutex);*/ return Vec3::ZERO; }

		virtual const Vec3 &getMax() { /*MT::ScopedGuard guard(mutex);*/ return Vec3::ZERO; }
		virtual const Vec3 &getMin() { /*MT::ScopedGuard guard(mutex);*/ return Vec3::ZERO; }
		virtual const Vec3 &getCenter() { /*MT::ScopedGuard guard(mutex);*/ return Vec3::ZERO; }

		/**
		Gets camera fov.
		\return fov.
		*/
		ENGINE_INLINE float GetFOV() const { /*MT::ScopedGuard guard(mutex);*/ return fov; }

		/**
		Sets camera Fov.
		\param[in] _fov Camera FOV.
		*/
		void SetFOV(float _fov);

		/**
		Gets camera screen aspect.
		\return aspect.
		*/
		ENGINE_INLINE float GetAspect() const { /*MT::ScopedGuard guard(mutex);*/ return bufferData.aspectRatio; }

		/**
		Sets camera screen aspect.
		\param[in] _aspect ASpect Ratio.
		*/
		ENGINE_INLINE void SetAspect(float _aspect) { /*MT::ScopedGuard guard(mutex);*/ bufferData.aspectRatio = _aspect; }

		/**
		Gets camera zNear clip plane distance.
		\return zNear.
		*/
		ENGINE_INLINE float GetZNear() const { /*MT::ScopedGuard guard(mutex);*/ return bufferData.clipDistance.y; }

		/**
		Sets camera zNear clip plane distance.
		\param[in] _zNear zNear value.
		*/
		ENGINE_INLINE void SetZNear(float _zNear) { /*MT::ScopedGuard guard(mutex);*/ bufferData.clipDistance.y = _zNear; }

		/**
		Gets camera zFar clip plane distance
		\return distance zFar
		*/
		ENGINE_INLINE float GetZFar() const { /*MT::ScopedGuard guard(mutex);*/ return bufferData.clipDistance.x; }

		/**
		Gets camera zFar clip plane distance.
		\return Vec2 where x-farClipDistance, y-nearFarClipDistance
		*/
		ENGINE_INLINE const Vec2& GetClipDistance() const { /*MT::ScopedGuard guard(mutex);*/ return bufferData.clipDistance; }

		/**
		Baked data for reconstructing Z from depth buffer used in deferred shading.
		\return Vec4. Where xy: (far / (far - near), far * near / (near - far)) zw: (tan(fov * Math::DegreesToRadians * 0.5) * (viewport.x / viewport.y)), tan(fov * Math::DegreesToRadians * 0.5))
		*/
		ENGINE_INLINE const Vec4& GetBakedData() const { /*MT::ScopedGuard guard(mutex);*/ return bufferData._bakedData; }

		/**
		Sets camera zFar clip plane distance.
		\param[in] _zFar zFar value.
		*/
		ENGINE_INLINE void SetZFar(float _zFar) {/* MT::ScopedGuard guard(mutex);*/ bufferData.clipDistance.x = _zFar; }

		virtual const Mat4& GetTransformForChange() override;
		virtual Mat4 GetTransform() override;

		/**
		Sets camera projection matrix.
		\param[in] _projection Camera Pojection matrix.
		*/
		void SetProjection(const Mat4 &_projection) { /*MT::ScopedGuard guard(mutex);*/ this->bufferData.projMatrix = _projection; }

		/**
		Gets camera projection matrix.
		\return Mat4 Camera Pojection matrix.
		*/
		ENGINE_INLINE Mat4 GetProjMatrix() { /*MT::ScopedGuard guard(mutex);*/  _RecalculateProjection(); _RecalculateFrustum(); return this->bufferData.projMatrix; }

		/*
		Recalculates projection matrix-with modification.
		\param[in] _vec where x-farClipDistance, y-nearFarClipDistance.
		*/
		void _RecalculateProjection(const Vec2&_vec);

		/*
		Recalculates projection matrix,without modification.
		bufferData.nearFarClipDistance.
		bufferData.farClipDistance.
		*/
		void _RecalculateProjection();

		/*
		Recalculates view matrix.
		*/
		void _RecalculateView();

		/*
		Recalculates frustum.
		*/
		void _RecalculateFrustum();

		/**
		Set view matrix for current camera.
		\param[in] _view ViewMatrix.
		*/
		void SetView(const Mat4 &_view);

		/**
		*/
		ENGINE_INLINE Mat4 GetViewMatrix() { /*MT::ScopedGuard guard(mutex);*/ _RecalculateView(); _RecalculateFrustum(); return this->bufferData.viewMatrix; }

		/**
		Apples Vec2 as param.

		\param[in] _vec where x = bufferData.nearFarClipDistance; y = bufferData.farClipDistance;
		*/
		virtual void Update(const Vec2& _vec);

		/**
		Updates without clip distances
		*/
		ENGINE_INLINE virtual void UpdateWCD() {
			//MT::ScopedGuard guard(mutex);
			_RecalculateView();
			_RecalculateProjection();
			_RecalculateFrustum();
		}

		//TODO:RENAME ON EILER ANGLE
		/**
		*/
		ENGINE_INLINE float GetAngle(EulerAngle _i) { /*MT::ScopedGuard guard(mutex);*/ if (_i < EulerAngle::ANGLE_COUNT) return angle[_i]; else return 0; }

		/**
		*/
		ENGINE_INLINE void SetAngle(EulerAngle _i, TimeDelta _v) { /*MT::ScopedGuard guard(mutex);*/ if (_i < EulerAngle::ANGLE_COUNT) angle[_i] = _v; }

		/**
		TODO: Replace 0.4 on mouse sens
		*/
		ENGINE_INLINE void LookAt(TimeDelta _x, TimeDelta _y) { /*MT::ScopedGuard guard(mutex);*/ angle[0] = -0.4 * _x; angle[1] = -0.4 * _y; }

		/**
		*/
		ENGINE_INLINE Frustum* GetFrustum() const { /*MT::ScopedGuard guard(mutex);*/ return frustum; }

		/**
		Moves vector that difference beetween _pos and current cam pos.
		In this class it's equivalent of SetPosition.
		*/
		ENGINE_INLINE virtual void MoveToPoint(const Vec3&_pos) { /*MT::ScopedGuard guard(mutex);*/ this->SetPosition(_pos); }

		ENGINE_INLINE const Vec3& getPhysicsSize() const { /*MT::ScopedGuard guard(mutex);*/ return phSize; }
		virtual void SetPhysics(const Vec3&_phSize);

		ENGINE_INLINE Camera &operator =(const Camera &_cam)
		{
			_Swap(_cam);
			return *this;
		}
	protected:
		/**
		Gets camera phys body
		\return phys body pointer. nullptr in this class.
		*/
		ENGINE_INLINE virtual PhysBody *getPhysBody() { /*MT::ScopedGuard guard(mutex);*/ return nullptr; }
	private:
		/**
		*/
		ENGINE_INLINE virtual unsigned int getNumSubsets() { /*MT::ScopedGuard guard(mutex);*/ return 0; };
		/**
		*/
		ENGINE_INLINE virtual float GetRadius(unsigned int s) { /*MT::ScopedGuard guard(mutex);*/ return 0; };
		/**
		*/
		ENGINE_INLINE virtual float GetRadius() { /*MT::ScopedGuard guard(mutex);*/ return 0; };
		/**
		*/
		ENGINE_INLINE virtual Material* GetMaterial(unsigned int s) { /*MT::ScopedGuard guard(mutex);*/ return nullptr; };

		ENGINE_INLINE void _Swap(const Camera&_cam) {
			if (this != &_cam) {
				bufferData = _cam.bufferData;
				transform = _cam.transform;
				frustum = _cam.frustum;
				angle[ANGLE_0] = _cam.angle[ANGLE_0];
				angle[ANGLE_1] = _cam.angle[ANGLE_1];
				fov = _cam.fov;
				phSize = _cam.phSize;
				maxVelocity = _cam.maxVelocity;
			}
		}

	public:

		struct BufferData
		{
			BufferData()
			{
				viewMatrix.Identity();
				projMatrix.Identity();
				viewProjMatrix.Identity();
				invViewProjMatrix.Identity();
				/**/
				_position = Vec3::ZERO;
				_bakedData = Vec4::ZERO;
				/**/
				aspectRatio = Math::ZEROFLOAT;
				clipDistance = Vec2::ZERO;
				direction = Vec3(Math::ZEROFLOAT, Math::ZEROFLOAT, Math::ONEFLOAT);
			}

			BufferData(const BufferData&bd) {
				_Swap(bd);
			}

			ENGINE_INLINE BufferData &operator =(const BufferData &bd) {
				_Swap(bd);
				return *this;
			}

			Mat4 viewMatrix;
			Mat4 projMatrix;
			Mat4 viewProjMatrix;
			Mat4 invViewProjMatrix;
			Vec3 _position; // used only for convertation object state to camera state
			Vec4 _bakedData;
			//соотношение сторон
			float aspectRatio;
			Vec2 clipDistance; // x-farClipDistance, y-nearFarClipDistance
			Vec3 direction;

		private:
			ENGINE_INLINE void _Swap(const BufferData&_bd) {
				if (this != &_bd) {
					viewMatrix = _bd.viewMatrix;
					projMatrix = _bd.projMatrix;
					viewProjMatrix = _bd.viewProjMatrix;
					invViewProjMatrix = _bd.invViewProjMatrix;
					_position = _bd._position;
					_bakedData = _bd._bakedData;
					aspectRatio = _bd.aspectRatio;
					clipDistance = _bd.clipDistance;
					direction = _bd.direction;
				}
			}
		};
		/**
		Note: Used in multithread, must be copy
		*/
		ENGINE_INLINE BufferData GetBufferData() { MT::ScopedGuard guard(mutex); return bufferData; }
	protected:
		BufferData bufferData;
		Mat4 transform;
		Frustum *frustum = nullptr;
		volatile TimeDelta angle[EulerAngle::ANGLE_COUNT];
		TimeDelta fov;
		Vec3 phSize;

		float maxVelocity;
	};
}