/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//**************************************
#include "Camera.h"
//**************************************

namespace NGTech
{
	/**
	Class of the Fixed Camera.
	Availability in script: yes
	*/
	class ENGINE_API CameraFixed : public Camera {
	public:
		/**
		Creates new Fixed Camera
		*/
		CameraFixed();

		/**
		Destructor of Fixed Camera
		*/
		virtual ~CameraFixed();

		/**
		Returns type of camera.
		\return CameraType type of camera
		*/
		ENGINE_INLINE virtual CameraType GetCameraType() override { return CAMERA_FIXED; };

		/**
		Updates without clip distances.
		*/
		virtual void UpdateWCD() override;

		/**
		Apples Vec2 as param.
		\param[in] _v.x where _v.x = bufferData.nearFarClipDistance;
		\param[in] _v.y where _v.y = bufferData.farClipDistance;
		*/
		virtual void Update(const Vec2& _v) override;

		/**
		Momental moving on _pos. But it's not teleport!
		In this class it's equivalent of SetPosition.
		\param[in] _pos - position.
		*/
		void Move(const Vec3&_pos);

		/**
		Moves vector that difference beetween _pos and current camera position.
		In this class it's equivalent of SetPosition.
		\param[in] _pos - position
		*/
		virtual void MoveToPoint(const Vec3&_pos) override;
	public:
		/**
		Sets size of character controller. Empty for this type of camera.
		\param[in] size - size of character controller.
		*/
		virtual void SetPhysics(const Vec3 &size) override {}
	};
}