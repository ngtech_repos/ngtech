/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//**************************************
#include "Camera.h"
//**************************************

namespace NGTech
{
	class PhysXCharacterController;
	/**
	Class of the Camera Free.
	Availability in script: yes
	*/
	class ENGINE_API CameraFree : public Camera {
	public:
		/**
		Creates new Free Camera.
		\param[in] _autoswitch AutoSwitch on creation
		*/
		explicit CameraFree(bool _autoswitch);

		/**
		Destructor of Camera FPS.
		*/
		virtual ~CameraFree();

		/**
		Returns type of camera.
		\return CameraType type of camera.
		*/
		ENGINE_INLINE virtual CameraType GetCameraType() override { return CAMERA_FREE; }

		/**
		Updates without clip distances
		*/
		virtual void UpdateWCD() override;

		/**
		Apples Vec2 as param.
		\param[in] _v.x where _v.x = bufferData.nearFarClipDistance;
		\param[in] _v.y where _v.y = bufferData.farClipDistance;
		*/
		virtual void Update(const Vec2& _v) override;

		/**
		Sets camera position
		If physics is enabled, then camera and body will momentally teleported,
		else sets local camera position.
		\param[in] _pos Position vector.
		*/
		virtual void SetPosition(const Vec3 &_pos) override;

		/**
		Get Current position of camera.
		If physics is enabled, then returns the value ph. body position else local camera position.
		\return Vec3 Position.
		*/
		virtual Vec3 GetPosition() const override;

		/**
		Momental moving on _pos. But it's not teleport!
		\param[in] _pos Position vector.
		*/
		void Move(const Vec3&_pos);

		/**
		Moves vector that difference beetween _pos and current Camera Position.
		\param[in] _pos Position vector.
		*/
		virtual void MoveToPoint(const Vec3&_pos) override;

		/**
		Sets size of character controller.
		Currently works only with Y component of vector.
		\param[in] size - size of character controller.
		*/
		virtual void SetPhysics(const Vec3 &size) override;
	private:
		void _SetPhysics(const CharacterControllerDesc &desc);
	private:
		class PhysXCharacterController* pBody = nullptr;
	};
}