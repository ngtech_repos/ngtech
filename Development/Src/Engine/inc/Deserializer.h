/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#ifndef DESERIALIZER_H
#define DESERIALIZER_H

//**************************************

#include "SerializerSharedIncls.h"

//**************************************
// Nick: Fix for incorrect assert for valid file in debug
#ifndef RAPIDJSON_ASSERT
#define RAPIDJSON_ASSERT(x) ASSERT(x)
#endif

#include "rapidjson/include/rapidjson/filereadstream.h"
#include "rapidjson/include/rapidjson/document.h"

namespace NGTech {
	using namespace rapidjson;
	using namespace std;

	class ENGINE_API Deserializer {
	private:
		Document doc;
		String   filename;
		bool      mDebugOut = false;
	public:
		explicit Deserializer(const String& _path);

	public:
		template<typename T>
		ENGINE_INLINE ENGINE_API
			T Deserialize(const Value& val, T defV);
		template<typename T>
		ENGINE_INLINE ENGINE_API
			T DeserializeUtil(const Value& val, const char* key, T defV);

		template<typename T>
		ENGINE_INLINE ENGINE_API
			T DeserializeWithNull(const Value& val, void* userdata);

		template<typename T>
		ENGINE_INLINE ENGINE_API
			T DeserializeFromFile(const String& _path, T _defV);

		// ONLY REFS!
		template<typename T>
		ENGINE_INLINE ENGINE_API
			void DeserializeModify(const Value& val, T _ptr);
		template<typename T>
		ENGINE_INLINE ENGINE_API
			void DeserializeModify(T _ptr) {
			DeserializeModify<T>(doc, _ptr);
		}
	};
}
#endif