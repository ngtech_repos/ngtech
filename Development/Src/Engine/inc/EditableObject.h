/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
#include <guiddef.h>
#endif

// is needed for MakeAvalibleForReflection
#include "EditorObjectManager.h"

namespace NGTech
{
	struct ReflectionObjectShared
	{
#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
		_CLASS_NAME_REFLECTION_BASE(ReflectionObjectShared);
	protected:
		ReflectionObjectShared() {}
		virtual ~ReflectionObjectShared() {}

		bool m_IsAvalibleForReflection = false;

		void _Swap(const ReflectionObjectShared& _other) {
			if (this != &_other) {
				// reflection system
				this->m_IsAvalibleForReflection = _other.m_IsAvalibleForReflection;
			}
		}
#endif
	};
	/*****
	Editor Part
	*****/
	struct EditableObjectShared :public ReflectionObjectShared
	{
#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
		ENGINE_INLINE GUID& GetGUID() { return m_GUID; }

		/// Saved into scene file on export?
		bool				bExportable;
		/// Selected in editor?
		bool				IsSelected;
		/// Selectable, and visible on selection lists, in NGEd?
		bool				IsSelectable;
		/// Is Light
		bool				IsLight;
		/// Is Prefab
		bool				IsPrefab;
		/// Manual Update is needed
		bool IsNeedlyManualUpdate;

		/// Globally unique identifier for actor
		GUID		m_GUID;

		/// Editable variables displayed in NGEd
		std::vector<EditorVar>	m_EditorVars;

		ENGINE_INLINE void _Swap(const EditableObjectShared& _other)
		{
			if (this != &_other) {
				// Editable object shared
				this->bExportable = _other.bExportable;
				this->IsLight = _other.IsLight;
				this->IsPrefab = _other.IsPrefab;
				this->IsSelectable = _other.IsSelectable;
				this->IsSelected = _other.IsSelected;
				this->m_EditorVars = _other.m_EditorVars;
				this->m_GUID = _other.m_GUID;
				ReflectionObjectShared::_Swap(_other);
			}
		}
#else
		ENGINE_INLINE void _Swap(const EditableObjectShared& _other) {}
#endif
	};

	struct ENGINE_API EditableObjectExtension :public EditableObjectShared
	{
		CLASS_AVALIBLE_IN_EDITOR_BASE(EditableObjectExtension);
#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
		ENGINE_INLINE virtual void AddEditorVarsTo(std::vector<EditorVar>&_to) {
			for (auto &obj : m_EditorVars) {
				_to.push_back(obj);
			}
		}

	protected:
		virtual void _AddEditorVars(const std::vector<EditorVar>&_vars);
		virtual void _AddEditorVars();
#endif
	};

	struct ENGINE_API EditableObject :public EditableObjectExtension
	{
#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
		EditableObject();
		EditableObject(const EditableObject& _other);

		virtual ~EditableObject();

		EditableObject & operator=(const EditableObject & _other) {
			_Swap(_other);
			return *this;
		}

		// !@ Creator method MUST be overloaded for any inhered class, if you want use reflection
		static EditableObject* Creator() { Warning(__FUNCTION__); return nullptr; }

		ENGINE_INLINE std::vector<EditorVar>& GetEditorVars() { return m_EditorVars; }

		/// Returns the Actor's GUID as a string
		const char* GetGUIDString();
		void		GenerateNewGUID();

		_CLASS_CHECKWHATWASMODIFICATED_BASE(EditableObject);
		/// Returns class name, with C# override
		_CLASS_NAME_BASE(EditableObject);

		ENGINE_INLINE virtual bool IsNeededManualUpdate() { return IsNeedlyManualUpdate; }
		virtual void ReInitEditorVars();

	protected:
		// must be called for enable reflection - this called ONLY from this class
		template<class T>
		void MakeAvalibleForReflection(T *_ptr) { if (m_IsAvalibleForReflection) return; GetEditorObjectManager()->MakeAvalibileForReflection<T>(_ptr); m_IsAvalibleForReflection = true; }
	private:
		void _Swap(const EditableObject &_other) {
			if (this != &_other) {
				bExportable = _other.bExportable;
				IsSelected = _other.IsSelected;
				IsSelectable = _other.IsSelectable;
				IsLight = _other.IsLight;
				IsPrefab = _other.IsPrefab;
				IsNeedlyManualUpdate = _other.IsNeedlyManualUpdate;
				m_GUID = _other.m_GUID;
				m_EditorVars = _other.m_EditorVars;
			}
		}
#endif
	};
}