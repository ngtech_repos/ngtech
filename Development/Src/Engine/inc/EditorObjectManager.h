#pragma once

#ifndef DROP_EDITOR

#include "../../Core/inc/ReflectionSystem.h"

namespace NGTech
{
	struct EditableObject;

	class ENGINE_API EditorObjectManager
	{
	public:
		ENGINE_INLINE EditorObjectManager() {
			m_EditableObjects.clear();
			m_Reflection_EditableObjs = std::make_shared<ReflectionSystemForEditableObject>();
		}

		// Called in EditableObject::EditableObject()
		ENGINE_INLINE void AddEditableObject(EditableObject*_edit) {
			m_EditableObjects.push_back(_edit);
		}

		template<class T>
		ENGINE_INLINE void MakeAvalibileForReflection(T *_ptr) {
			if (!m_Reflection_EditableObjs || _ptr == nullptr) {
				Error(__FUNCTION__, true);
				return;
			}
			m_Reflection_EditableObjs->Register(_ptr->ClassName(), &T::Creator);
		}
		template<class T>
		ENGINE_INLINE void MakeAvalibileForReflection() {
			if (!m_Reflection_EditableObjs) {
				Error(__FUNCTION__, true);
				return;
			}
			m_Reflection_EditableObjs->Register(T::ClassName_Reflection(), &T::Creator);
		}

		// m_Reflection_EditableObjs can't be null
		ENGINE_INLINE ReflectionSystem<EditableObject>::Creators GeReflectionCreators() {
			return m_Reflection_EditableObjs->_creators;
		}

		// Called in EditableObject::~EditableObject()
		ENGINE_INLINE void DeleteObject(EditableObject*_edit) {
			m_EditableObjects.erase(std::remove(m_EditableObjects.begin(), m_EditableObjects.end(), _edit), m_EditableObjects.end());
		}

		ENGINE_INLINE size_t GetCount() { return m_EditableObjects.size(); }
		ENGINE_INLINE EditableObject* GetObjById(size_t _id) { if (_id > m_EditableObjects.size()) return nullptr; return m_EditableObjects[_id]; }

		std::vector<EditableObject*> m_EditableObjects;
		std::shared_ptr<ReflectionSystemForEditableObject> m_Reflection_EditableObjs;
	};
}

#endif