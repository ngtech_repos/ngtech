/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//***************************************************
#include "DLLDef.h"
//***************************************************
#include "../../Core/inc/CoreManager.h"
//***************************************************

namespace NGTech {
	/**/
	struct IGame;
	class EnginePlugins;
	class GameUpdateJob;
	class ScriptUpdateJob;
#ifndef DROP_EDITOR
	class EditorObjectManager;
#endif
	class Console;

	/**
	* \brief Base update callback for engine
	 \authors Nick galek Galko
	 \version 1.0
	 \date 03.2019

	 Base callback will called in Engine::DoUpdate and Engine::DoRender
	*/
	class BaseEngineUpdateCallback
	{
	public:

		BaseEngineUpdateCallback()
		{
		}
		virtual ~BaseEngineUpdateCallback()
		{
		}

		/**
		_FixedUpdate will called in Engine::Loop() after Engine::DoUpdate()
		\param[in] _time DeltaTime
		*/
		virtual void _FixedUpdate(TimeDelta _time) {}

		/**
		_FixedUpdate will called in Engine::Loop() after Engine::DoRender()
		\param[in] _time DeltaTime
		*/
		virtual void _VariableUpdate(TimeDelta _time) {}
	};

	/**
	* \brief Engine`s main class. Created once
	 \authors Nick galek Galko
	 \version 1.0
	 \date 03.2019

	 Manages all subsystems
	*/
	class ENGINE_API Engine : public CoreManager {
	public:
		Engine()
			:console(nullptr),
			plugins(nullptr),
			gameUpdJob(nullptr),
			m_ScriptUpdateJob(nullptr),
			m_renderUpdateJob(nullptr),
			m_AIUpdateJob(nullptr),
			m_PhysicsUpdateJob(nullptr)
		{
			_PreInitFast();
		}

		virtual ~Engine();

		/**
		engines main loop in Runtime mode
		*/
		virtual void MainLoop() override;

		/**
		engines main loop in Editor mode
		*/
		void EditorLoop();

		/**
		exits the main loop
		*/
		virtual void Quit() override;

		/**
		Initializes engine with/without existing HWND
		\param[in] _hwnd pointer on HWND
		*/
		virtual void Initialise(void* _hwnd = nullptr) override;

		/**
		Sets pointer on game interface
		\param[in] _game pointer on game
		*/
		void SetGame(IGame*_game);

		/**
		Loads engine module by name
		\param[in] _name name of module (string)
		*/
		virtual void LoadEngineModule(const char* _name) override;

		/**
		Loads engine module by name with editor flag
		\param[in] _name name of module (string)
		*/
		virtual void LoadEngineModuleEditor(const char*_name) override;

		/**
		Loads game, with parsing system.ltx
		\param[in] _cfg name of config (string)
		\param[in] _argc count of arguments
		\param[in] _argv string of arguments
		*/
		void LoadStartupModule(const char*_cfg, int _argc, const char * const *_argv);

		/**
		Loads game, with parsing system.ltx. In Editor mode
		\param[in] _cfg name of config (string)
		\param[in] _argc count of arguments
		\param[in] _argv string of arguments
		*/
		void LoadStartupModuleEditor(const char*_cfg, int _argc, const char * const * _argv);

		/**
		Adds resource location
		\param[in] _name relative directory path (string)
		*/
		void SetResources(const char* _name);

		/**
		Update callback setter
		\param[in] _callback pointer on callback class
		*/
		ENGINE_INLINE void SetUpdateCallback(const std::shared_ptr<BaseEngineUpdateCallback>&_callback) { ASSERT(_callback, "Invalid ponter on BaseENgineUpdateCallback"); m_onUpdate_callback = _callback; }

	private:
		void _PreInitFast();
		void _PreInit();
		void _CreateWindow();
		void _SetCriticalResources();
		void _Destroy();
		void _GetAppDir();
		void _UpdateTimer();
		void _Loop();
		bool _LoopImpl();

		/**
		Will call in DoRender
		*/
		void _PostUpdate();
		//void _ShowLoadingScreen(TimeDelta _delta);

		void DoUpdate(TimeDelta _f);
		void DoRender();

		void _LoadStartupModule(const char*_cfg, int argc, const char * const * argv, bool isEditor);

		void _InitReflection();
	private:
		Console*				console = nullptr;
		EnginePlugins*			plugins = nullptr;
		GameUpdateJob*			gameUpdJob = nullptr;
		String					app_path;
		// Scriptable
		std::shared_ptr<ScriptUpdateJob> m_ScriptUpdateJob = nullptr;
		std::shared_ptr<ScriptUpdateJob> m_renderUpdateJob = nullptr;
		std::shared_ptr<ScriptUpdateJob> m_AIUpdateJob = nullptr;
		std::shared_ptr<ScriptUpdateJob> m_PhysicsUpdateJob = nullptr;
		// Callbacks and frame update
		Timer _timer;
		TimeDelta _gameFrameInterval;
		TimeDelta _gameFrameTime;
		//!@todo ����� ���� ������ � ��������� ������ ������������
#ifdef USE_MULTITHREAD_ENGINE
		bool m_UseSingleThread = false;
#else
		bool m_UseSingleThread = true;
#endif

		std::shared_ptr<BaseEngineUpdateCallback> m_onUpdate_callback = nullptr;
	};
	ENGINE_API Engine* GetEngine();
	ENGINE_API void DestroyEngine();
#ifndef DROP_EDITOR
	ENGINE_API const std::shared_ptr<EditorObjectManager>& GetEditorObjectManager();
#endif
}