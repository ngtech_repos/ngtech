/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include "DLLDef.h"
#include "UpdatableObject.h"
#include "EditableObject.h"
#include "../../Common/ScriptableObject.h"

namespace NGTech
{
	// ��������� �������
	struct ObjectStateDesc
	{
		ENGINE_INLINE ObjectStateDesc() {}

		ENGINE_INLINE ObjectStateDesc(const ObjectStateDesc& _other) {
			_Swap(_other);
		}

		/**/
		ENGINE_INLINE bool operator ==(const ObjectStateDesc&b) {
			bool res1 = this->transform == b.transform;
			bool res2 = this->upVector == b.upVector;
			bool res3 = this->m_Scale == b.m_Scale;
			bool res4 = this->sphere.center == b.sphere.center;
			bool res6 = this->box.maxes == b.box.maxes;
			bool res7 = this->box.mins == b.box.mins;
			bool res8 = this->useHWOQuery == b.useHWOQuery;

			bool res9 = Math::IsEqual<float, float>(this->angle, b.angle);
			bool res10 = Math::IsEqual<float, float>(this->sphere.radius, b.sphere.radius);

			return res1 && res2&&res3&&
				res4&&res6&&
				res4&&res6&&
				res7&&res8&&res9&&res10;
		}
		ENGINE_INLINE bool operator !=(const ObjectStateDesc& b) {
			return !operator ==(b);
		}

		ENGINE_INLINE ObjectStateDesc & operator=(const ObjectStateDesc & _other) {
			_Swap(_other);
			return *this;
		}

	private:
		ENGINE_INLINE void _Swap(const ObjectStateDesc & _other) {
			if (this != &_other) {
				box = _other.box;
				sphere = _other.sphere;
				transform = _other.transform;
				upVector = _other.upVector;
				m_Scale = _other.m_Scale;
				angle = _other.angle;
				useHWOQuery = _other.useHWOQuery;
			}
		}

	public:
		BBox box;
		BSphere sphere;

		Mat4 transform;

		Vec3 upVector = Vec3::ZERO;
		/*Scale, will be applied in Get/Set Transform */
		Vec3 m_Scale = Vec3::ONE;
		float angle = Math::ZEROFLOAT;

		// Deprecated: ���� ��������� � baseVisual
		[[deprecated]]
		bool useHWOQuery = false;
	};

	struct InterfaceState
	{
		ENGINE_INLINE InterfaceState() {}
		ENGINE_INLINE InterfaceState(const InterfaceState&_other) {
			_Swap(_other);
		}
		virtual ~InterfaceState() {}

		ENGINE_INLINE void SetNewState(const ObjectStateDesc& _newState) {
			if (mState != _newState)
			{
				mSavedState = mState;
				mState = _newState;
			}
		}

		ENGINE_INLINE void SetDefaultState(const ObjectStateDesc& _newState) {
			mState = _newState;
			mSavedState = _newState;
		}

		ENGINE_INLINE void ResetSavedState() {
			mSavedState = mState;
		}

		ENGINE_INLINE InterfaceState& operator=(const InterfaceState&_other) {
			_Swap(_other);
			return *this;
		}

		ENGINE_INLINE void _Swap(const InterfaceState&_other) {
			if (this != &_other) {
				this->mState = _other.mState;
				this->mSavedState = _other.mSavedState;
			}
		}

		ObjectStateDesc mState;
		ObjectStateDesc mSavedState;
	};

	struct EngineObject : public InterfaceState, public RefCount, public UpdatableObject, public EditableObject, public ScriptableObject
	{
		ENGINE_INLINE EngineObject() { AddRef(); }
		virtual ~EngineObject() {}
	};
}