/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//*************************
#include "../../Common/MathLib.h"
#include "../../Common/BBox.h"
#include "../../Common/BSphere.h"
//*************************

namespace NGTech {
	/**
	* \brief View frustum
	 \authors Nick galek Galko
	 \version 1.0
	 \date 03.2019

	 Frustum class, used in Frutum Culling.
	 Availability in script: yes
	*/
	class Frustum {
	public:
		Frustum() {}
		~Frustum() {}

		/**
		gets the current view frustum from Meshview and Projection matrix
		\param[in] _proj  Model Proj matrix
		\param[in] _model Model View matrix
		*/
		void Get(const Mat4&proj, const Mat4&modl);

		/**
		Gets the current view frustum from modelview and projection matrix
		\param[in] _matrix  Model*Proj(MVP) matrix
		*/
		void Build(const Mat4 &_matrix);

		/**
		checks wether the point is inside of the frustum
		\param[in] _point  Coordinates of point
		\return true if inside
		*/
		bool IsInside(const Vec3 &_point);

		/**
		checks weather the bounding box is inside the frustum
		\param[in] _min box`s min and max
		\param[in] _max  sphere radius
		\return true if inside
		*/
		bool IsInside(const Vec3 &_min, const Vec3 &_max);

		/**
		checks weather the bounding sphere is inside the frustum
		\param[in] center  sphere center
		\param[in] radius  sphere radius
		\return true if inside
		*/
		bool IsInside(const Vec3 &_Center, float _Radius);

		/**
		checks weather the bounding box is inside the frustum
		\param[in] _box, min and max will taken from BBox
		\return true if  inside
		*/
		bool IsInside(const BBox &_box);

		/**
		checks weather the bounding sphere is inside the frustum
		\param[in] sphere, center and radius will taken from BSphere
		\return true if  inside
		*/
		bool IsInside(const BSphere &sphere);
	private:
		float plane[6][4];
	};
}