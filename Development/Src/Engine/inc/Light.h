/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include <math.h> // for fmaxf
//**************************************
#include "EditableObject.h"
#include "../../Common/IRender.h"
//**************************************

namespace NGTech {
	/**
	Forward Declaration
	*/
	class Camera;
	class Material;

	/*By default used point light params*/
	struct ENGINE_API LightBufferData
	{
		enum LightType {
			LIGHT_ERROR = 0,
			LIGHT_OMNI,
			LIGHT_SPOT,
			LIGHT_DIRECT,
			LIGHT_ENVPROBE,
			LIGHT_AREA,
			LIGHT_TYPE_COUNT
		} mType = LIGHT_ERROR;

		Color color = Color::WHITE;

		Quat orientation = Quat::ZERO;	/* for area lights */
		Vec4 mins, maxes;
		Vec3 direction = Math::Y_AXIS; // when spot
		Vec2 angles/* when spot(inner, outer)*/, clipplanes; /*when spot (near, far)*/

		bool enabled = true,
			visible = true,
			castShadows = true,
			disableByDistance = false,
			m_Occluded = false, /*Used for calculation of visability*/
			useFrustumCulling = true;

		TimeDelta fov = 90,
			lightArea = Math::ZEROFLOAT,
			disableByDistanceV = 100000.f,
			lumFlux = 1.0,
			widthArea, /*Actual only for Area Light*/
			heightArea, /*Actual only for Area Light*/
			innerTmp,
			outerTmp,
			radius = Math::ONEFLOAT,
			/*Common params - NOT NEEDLY SERILAZE*/
			lumintensityTmp = 1.0,
			luminanceTmp = 1.0;

		Vec3 dirright = Vec3::ZERO/* when area */,
			dirup = Math::Y_AXIS/* when area */;

		/**/
		LightBufferData();

		LightBufferData(const LightBufferData&_lb) {
			_Swap(_lb);
		}

		~LightBufferData() {}

		ENGINE_INLINE bool operator ==(const LightBufferData &b)
		{
			return
				/*(this->transform == b.transform)
				&&*/ (this->direction == b.direction)
				&& (this->color == b.color)
				&& (Math::IsEqual(this->radius, b.radius))
				&& (this->mins == b.mins)
				&& (this->maxes == b.maxes)
				&& (this->enabled == b.enabled)
				&& (this->visible == b.visible)
				&& (this->m_Occluded == b.m_Occluded)
				&& (this->castShadows == b.castShadows)
				&& (this->disableByDistance == b.disableByDistance)
				&& (this->useFrustumCulling == b.useFrustumCulling)
				&& (this->angles == b.angles)
				&& (this->orientation == b.orientation)
				&& (this->dirright == b.dirright)
				&& (this->dirup == b.dirup)
				&& (this->clipplanes == b.clipplanes)
				&& (this->mType == b.mType)
				&& (Math::IsEqual(this->fov, b.fov))
				&& (Math::IsEqual(this->lightArea, b.lightArea))
				&& (Math::IsEqual(this->disableByDistanceV, b.disableByDistanceV))
				&& (Math::IsEqual(this->lumFlux, b.lumFlux))
				&& (Math::IsEqual(this->widthArea, b.widthArea))
				&& (Math::IsEqual(this->heightArea, b.heightArea))
				&& (Math::IsEqual(this->outerTmp, b.outerTmp));
		}

		ENGINE_INLINE bool operator !=(const LightBufferData & b) {
			return !operator ==(b);
		}

		ENGINE_INLINE LightBufferData& operator=(const LightBufferData& b) {
			if (this == &b) return *this;
			_Swap(b);
			return *this;
		}

		void SetOmniParams(TimeDelta _LumFlux);

		void SetupAsArea(TimeDelta lumflux, Quat orient, TimeDelta _width, TimeDelta _height, TimeDelta _radius);

		void SetupAsEnvProbe();

		void SetupAsSpot(TimeDelta lumflux, Vec3 _pos, Vec3 _dir, TimeDelta _radius, TimeDelta _inner, TimeDelta _outer);

		void InitAsSpot(TimeDelta _lumflux) {
			SetupAsSpot(_lumflux, Vec3::ZERO, Vec3::ONE, Math::ONEFLOAT, Math::ZEROFLOAT, 44.0);
		}


	private:
		void _ReComputeLumFlux(TimeDelta _LumFlux, TimeDelta _width, TimeDelta _height, TimeDelta _outer, TimeDelta _radius);

		void _Swap(const LightBufferData&_lb) {
			if (this != &_lb) {
				this->mType = _lb.mType;
				this->color = _lb.color;
				this->orientation = _lb.orientation;
				this->mins = _lb.mins;
				this->maxes = _lb.maxes;
				this->direction = _lb.direction;
				this->angles = _lb.angles;
				this->clipplanes = _lb.clipplanes;
				this->enabled = _lb.enabled;
				this->visible = _lb.visible;
				this->castShadows = _lb.castShadows;
				this->disableByDistance = _lb.disableByDistance;
				this->disableByDistanceV = _lb.disableByDistanceV;
				this->lumFlux = _lb.lumFlux;
				this->widthArea = _lb.widthArea;
				this->heightArea = _lb.heightArea;
				this->innerTmp = _lb.innerTmp;
				this->outerTmp = _lb.outerTmp;
				this->radius = _lb.radius;
				this->lumintensityTmp = _lb.lumintensityTmp;
				this->luminanceTmp = _lb.luminanceTmp;
				this->lightArea = _lb.lightArea;
				this->fov = _lb.fov;
				this->dirright = _lb.dirright;
				this->dirup = _lb.dirup;
				this->useFrustumCulling = _lb.useFrustumCulling;
				this->m_Occluded = _lb.m_Occluded;
			}
		}

		friend Light;
	};

	/**
	@availability in script: NO, TODO:only as subpart
	*/
	class ENGINE_API Light :public BaseEntity
	{
		CLASS_AVALIBLE_IN_EDITOR(Light);
	public:
		Light();

		virtual ~Light();

		/**/
		ENGINE_INLINE void SetDirectionRight(const Vec3& _dirRight) {
			//MT::ScopedGuard guard(mutex);
			this->lightBD.dirright = _dirRight;
		}
		ENGINE_INLINE void SetDirectionUp(const Vec3& _dirup) {
			//MT::ScopedGuard guard(mutex);
			this->lightBD.dirup = _dirup;
		}
		ENGINE_INLINE void SetLumFlux(TimeDelta _lumFlux) {
			//MT::ScopedGuard guard(mutex);
			this->lightBD.lumFlux = _lumFlux;
		}

		/**/
		ENGINE_INLINE const Vec3& GetDirectionRight() const {
			//MT::ScopedGuard guard(mutex);
			return this->lightBD.dirright;
		}
		ENGINE_INLINE const Vec3& GetDirectionUp() const {
			//MT::ScopedGuard guard(mutex);
			return this->lightBD.dirup;
		}
		ENGINE_INLINE TimeDelta GetLumFlux() const {
			//MT::ScopedGuard guard(mutex);
			return this->lightBD.lumFlux;
		}
		ENGINE_INLINE LightBufferData::LightType GetType() const {
			//MT::ScopedGuard guard(mutex);
			return this->lightBD.mType;
		}

		ENGINE_INLINE const Vec2& GetClipPlanes() const {
			//MT::ScopedGuard guard(mutex);
			return this->lightBD.clipplanes;
		}

		[[deprecated]]
		ENGINE_INLINE const Quat& GeOrientation() const {
			//MT::ScopedGuard guard(mutex);
			return lightBD.orientation;
		}
		[[deprecated]]
		ENGINE_INLINE void SetOrientation(const Quat& _orientation) {
			//MT::ScopedGuard guard(mutex);
			this->lightBD.orientation = _orientation;
		}

		/**/
		ENGINE_INLINE float GetRadius() const {
			//MT::ScopedGuard guard(mutex);
			return lightBD.radius;
		}

		ENGINE_INLINE float GetIRadius() const {
			//MT::ScopedGuard guard(mutex);
			return ((lightBD.radius == FLT_MIN) ? 0 : Math::ONEFLOAT / lightBD.radius);
		}

		ENGINE_INLINE void SetRadius(TimeDelta _radius) {
			//MT::ScopedGuard guard(mutex);
			this->lightBD._ReComputeLumFlux(this->lightBD.lumFlux, this->lightBD.widthArea,
				this->lightBD.heightArea, this->lightBD.outerTmp, _radius);
		}

		/**/
		ENGINE_INLINE const Color& GetColor() const {
			//MT::ScopedGuard guard(mutex);
			return lightBD.color;
		}

		ENGINE_INLINE void SetColor(const Color &_color) {
			//MT::ScopedGuard guard(mutex);
			this->lightBD.color = _color;
		}

		/**/
		ENGINE_INLINE const Vec3 GetDirection() {
			//MT::ScopedGuard guard(mutex);
			return lightBD.direction;
		}
		ENGINE_INLINE void SetDirection(const Vec3 &direction) {
			//MT::ScopedGuard guard(mutex);
			this->lightBD.direction = direction;
		}

		/**/
		ENGINE_INLINE void SetEnable(bool flag) {
			//MT::ScopedGuard guard(mutex);
			lightBD.enabled = flag;
		}
		ENGINE_INLINE bool IsEnabled() {
			//MT::ScopedGuard guard(mutex);
			return lightBD.enabled;
		}

		ENGINE_INLINE void SetVisible(bool _s) {
			//MT::ScopedGuard guard(mutex);
			lightBD.visible = _s;
		}
		ENGINE_INLINE bool IsVisible() {
			//MT::ScopedGuard guard(mutex);
			return lightBD.visible;
		}

		ENGINE_INLINE bool IsOcclusedByFrustum() {
			//MT::ScopedGuard guard(mutex);
			return lightBD.m_Occluded;
		}
		ENGINE_INLINE void SetOcclusedByFrustum(bool _s) {
			//MT::ScopedGuard guard(mutex);
			lightBD.m_Occluded = _s;
		}

		ENGINE_INLINE bool IsCastShadows() {
			//MT::ScopedGuard guard(mutex);
			return lightBD.castShadows;
		}
		ENGINE_INLINE void SetCastShadows(bool shadows) {
			//MT::ScopedGuard guard(mutex);
			lightBD.castShadows = shadows;
		}

		ENGINE_INLINE bool IsDisableByDistance() {
			//MT::ScopedGuard guard(mutex);
			return lightBD.disableByDistance;
		}
		ENGINE_INLINE float IsDisableByDistanceValue() {
			//MT::ScopedGuard guard(mutex);
			return lightBD.disableByDistanceV;
		}
		ENGINE_INLINE void SetDisableByDistance(bool _disableByDistance) {
			//MT::ScopedGuard guard(mutex);
			lightBD.disableByDistance = _disableByDistance;
		}
		ENGINE_INLINE void SetDisableByDistanceValue(float _disableByDistanceV) {
			//MT::ScopedGuard guard(mutex);
			lightBD.disableByDistanceV = _disableByDistanceV;
		}

		ENGINE_INLINE float GetFOV() {
			//MT::ScopedGuard guard(mutex);
			return lightBD.fov;
		}
		ENGINE_INLINE void SetFOV(float fov) {
			//MT::ScopedGuard guard(mutex);
			this->lightBD.fov = fov;
		}

		ENGINE_INLINE float GetLightArea() const {
			//MT::ScopedGuard guard(mutex);
			return (lightBD.enabled ? lightBD.lightArea : -Math::ONEFLOAT);
		}
		ENGINE_INLINE void  SetlightArea(float _lightArea) {
			//MT::ScopedGuard guard(mutex);
			lightBD.lightArea = _lightArea;
		}
		ENGINE_INLINE void	SetLightBufferData(const LightBufferData& _ptr) {
			//MT::ScopedGuard guard(mutex);
			lightBD = _ptr;
		}
		ENGINE_INLINE const LightBufferData& GetLightBufferData() const {
			//MT::ScopedGuard guard(mutex);
			return lightBD;
		}

		/**
		Frustum culling part, will used in _CalculateVisibility
		Serialization: Yes
		*/
		ENGINE_INLINE void EnableFrustumCulling(bool enable) {
			//MT::ScopedGuard guard(mutex);
			lightBD.useFrustumCulling = enable;
		}

		ENGINE_INLINE bool IsFrustumCullingEnabled() const {
			//MT::ScopedGuard guard(mutex);
			return lightBD.useFrustumCulling;
		}

		/**
		Available only for SpotLight.
		x-inner,y-outer
		Serialization: Yes
		*/
		ENGINE_INLINE Vec2 GetSpotAngles() const {
			//MT::ScopedGuard guard(mutex);
			return lightBD.angles;
		}
		/**
		Setting Spot Angles, without check and without compute(used for Serialization)
		Serialization: Yes
		*/
		ENGINE_INLINE void SetSpotAngles(const Vec2&_angles) {
			//MT::ScopedGuard guard(mutex);
			lightBD.angles = _angles;
		}

		/**
		Used for lighting shader
		Serialization: No
		*/
		ENGINE_INLINE float GetSpotAngleScale() const {
			//MT::ScopedGuard guard(mutex);
			return Math::ONEFLOAT / Math::Max<float>(0.001f, lightBD.angles[0] - lightBD.angles[1]);
		}

		/**
		Used for lighting shader
		Serialization: No
		*/
		ENGINE_INLINE float GetSpotAngleOffset() const {
			//MT::ScopedGuard guard(mutex);
			return -lightBD.angles[1] * GetSpotAngleScale();
		}

		void SetShadowBox(const BBox& castersbox);

		void SetNewState(const LightBufferData&_data);
		void ResetSavedState();

		/*USED ONLY FOR SHADER EXPORT*/
		ENGINE_INLINE TimeDelta GetLuminuousIntensity() const {
			//MT::ScopedGuard guard(mutex);
			return this->lightBD.lumintensityTmp;
		}
		/*USED ONLY FOR SHADER EXPORT*/
		ENGINE_INLINE TimeDelta GetLuminance() const {
			//MT::ScopedGuard guard(mutex);
			return this->lightBD.luminanceTmp;
		}
	private:
		bool _CalculateVisability(Camera*);
		bool _CheckVisible(Camera* _camera);
		/*****
		Editor Part
		*****/
	protected:
		BBox			box;
		LightBufferData lightBD, lightBDSaved;
		friend LightBufferData;
	};
}