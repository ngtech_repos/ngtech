#pragma once

#include "Light.h"

namespace NGTech {
	class ENGINE_API LightArea : public Light
	{
		CLASS_AVALIBLE_IN_EDITOR(LightArea);
	public:
		LightArea();
		virtual ~LightArea() {}
	};
}