/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include "LightDirect.h"

namespace NGTech
{
	// 	@availability in script : NO, TODO : YES
	class ENGINE_API LightEnvProbe :public LightDirect
	{
		CLASS_AVALIBLE_IN_EDITOR(LightEnvProbe);
	public:
		LightEnvProbe();

		virtual ~LightEnvProbe();

		ENGINE_INLINE I_Texture* GetDiffuseIrrad() { return skyirraddiff; }
		ENGINE_INLINE I_Texture* GetSpecularIrrad() { return skyirradspec; }
		ENGINE_INLINE I_Texture* GetIntegratedBRDF() { return brdf; }

	private:
		void _Init();
	private:
		I_Texture * skyirraddiff = nullptr;
		I_Texture*	skyirradspec = nullptr;
		I_Texture*	brdf = nullptr;
		friend class RenderPipeline;
	};
}