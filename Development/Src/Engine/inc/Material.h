/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//***************************************************************************
#include "../../Common/StringHelper.h"
#include "../../Common/IRender.h"
#include "../../Common/IResource.h"
#include "EditableObject.h"
#include <vector>
//***************************************************************************
#include "MaterialPass.h"
//***************************************************************************

namespace NGTech {
	struct I_Texture;
	struct I_Shader;
	/**/
	class Material;
	struct PhysXMaterialSh;
	struct MaterialPassSh;

	/*��������� ��� ���������*/
	struct MaterialSettingsNode final
	{
		/*���������� ������������*/
		float Roughness = Math::ONEFLOAT;
		/*���������� ��������� ������ �������� ������*/
		float Metalness = Math::ONEFLOAT;
		/*
		������� ����
		White, see more colors http://developer.android.com/reference/android/graphics/Color.html
		Transparent 0x00000000
		*/
		Color BaseColor = Color::WHITE;

		/*IOR*/
		float IOR = 0.04f;// GOLD

		bool hasTexture = false;
		bool hasNormal = false;

		/* BRDF MODEL */
		enum class BRDF_MODEL
		{
			DEFAULT = 0,
			SUBSURFACE,
			MICRO
		} mBRDF = BRDF_MODEL::DEFAULT;

		MaterialSettingsNode() {}

		MaterialSettingsNode(const MaterialSettingsNode&_pbr) {
			_Swap(_pbr);
		}

		ENGINE_INLINE MaterialSettingsNode& operator=(const MaterialSettingsNode& other) {
			this->_Swap(other);
			return *this;
		}

		ENGINE_INLINE bool operator==(const MaterialSettingsNode&_mat) {
			return (Roughness == _mat.Roughness)
				&& (Metalness == _mat.Metalness)
				&& (BaseColor == _mat.BaseColor)
				&& (IOR == _mat.IOR)
				&& (hasTexture == _mat.hasTexture)
				&& (hasNormal == _mat.hasNormal)
				&& (mBRDF == _mat.mBRDF)
				&& (m_AttachedPhMatName == _mat.m_AttachedPhMatName)
				&& (m_AttachedPhMatPtr == _mat.m_AttachedPhMatPtr)
				&& (isTransparent == _mat.isTransparent);
		}

		void CleanResources();

		// Attached Physics Material
		[[Serialized, true]]
		String m_AttachedPhMatName = StringHelper::EMPTY_STRING;

		[[Serialized, false]]
		PhysXMaterialSh* m_AttachedPhMatPtr = nullptr;

		// �������� �� ��� ���������� ����������
		[[Serialized, true]]
		bool isTransparent = false;


	private:
		ENGINE_INLINE void _Swap(const MaterialSettingsNode&_mat) {
			if (this != &_mat) {
				Roughness = _mat.Roughness;
				Metalness = _mat.Metalness;
				BaseColor = _mat.BaseColor;
				IOR = _mat.IOR;
				hasTexture = _mat.hasTexture;
				hasNormal = _mat.hasNormal;
				mBRDF = _mat.mBRDF;
				m_AttachedPhMatName = _mat.m_AttachedPhMatName;
				m_AttachedPhMatPtr = _mat.m_AttachedPhMatPtr;
				isTransparent = _mat.isTransparent;
			}
		}
	};

	//---------------------------------------------------------------------------
	// Desc: Material class
	// 	@availability in script : NO, TODO : YES
	//---------------------------------------------------------------------------
	//!@todo REPLACE ON ENGINE OBJECT
	class ENGINE_API Material final :public IResource, public EditableObject
	{
		CLASS_AVALIBLE_IN_EDITOR(Material);
	public:
		Material();
		Material(const Material&_mat) { _Swap(_mat); }

		explicit Material(const String& path, bool _isTransparent, bool _load);
		explicit Material(const String& path, const Color& color, float roughness, float metalness, bool _transp, bool _load);

		virtual ~Material();

		bool SetPass(const String &name);

		void LoadingFinishedEvent();

		bool PassHasBlending(const String &name) const;
		void SetPassBlending(const String &name);

		void UnsetPassBlending();

		void UnsetPass();

		void SendFloat(const String &name, float value);
		void SendInt(const String &name, int value);
		void SendVec2(const String &name, const Vec2 & value);
		void SendVec3(const String &name, const Vec3 & value);
		void SendVec4(const String &name, const Vec4 & value);
		void SendMat4(const String &name, const Mat4 & value);

		ENGINE_INLINE virtual String GetResourceType() const override { return "Material"; }

		// user-defined copy assignment, copy-and-swap form
		ENGINE_INLINE Material& operator=(const Material& other) {
				this->_Swap(other);
			return *this;
		}

		ENGINE_INLINE bool operator==(const Material&_mat) {
			return (currentPass == _mat.currentPass)
				&& (mPBR == _mat.mPBR)
				&& (passes == _mat.passes);
		}

	public:
		ENGINE_INLINE bool IsTransparent() const { return mPBR.isTransparent; }
		ENGINE_INLINE bool IsHasTexture() const { return mPBR.hasTexture; }
		ENGINE_INLINE bool IsHasNormal() const { return mPBR.hasNormal; }

		ENGINE_INLINE float GetRoughness() const { return mPBR.Roughness; }
		ENGINE_INLINE float GetMetalness() const { return mPBR.Metalness; }
		ENGINE_INLINE Color GetBaseColor() const { return mPBR.BaseColor; }

		ENGINE_INLINE void SetRoughness(float _r) { mPBR.Roughness = _r; }
		ENGINE_INLINE void SetMetalness(float _r) { mPBR.Metalness = _r; }
		ENGINE_INLINE void SetBaseColor(const Color&_r) { mPBR.BaseColor = _r; }

		ENGINE_INLINE void SetTransparent(bool _v) { mPBR.isTransparent = _v; }
		ENGINE_INLINE void SetHasTexture(bool _v) { mPBR.hasTexture = _v; }
		ENGINE_INLINE void SetHasNormal(bool _v) { mPBR.hasNormal = _v; }

		ENGINE_INLINE size_t GetNumPasses() const { return passes.size(); }

		const std::shared_ptr<MaterialPassSh>& GetPass(size_t _i);

		ENGINE_INLINE void SetListOfPasses(std::vector<std::shared_ptr<MaterialPassSh>> _passes) {
			passes = _passes; 
		}

		ENGINE_INLINE std::vector<std::shared_ptr<MaterialPassSh>> GetListOfPasses() {
			return passes;
		}

		void AddPass(const std::shared_ptr<MaterialPassSh>& _pass);

		/**
		BRDF Model
		*/
		void SetBRDFModel(unsigned int _model);
		ENGINE_INLINE unsigned int GetBRDF() { return static_cast<unsigned int>(mPBR.mBRDF); }

		void AttachPhysXMaterialByPtr(PhysXMaterialSh * _phMat);
		ENGINE_INLINE void AttachPhysXMaterialByName(const String&_phMat) { mPBR.m_AttachedPhMatName = _phMat; }
		ENGINE_INLINE String GetPhysXMaterialShName() { return mPBR.m_AttachedPhMatName; }

		ENGINE_INLINE PhysXMaterialSh* _GetPhysXMaterialShPtr() { return mPBR.m_AttachedPhMatPtr; }
		ENGINE_INLINE bool IsPresentPhysicsMaterial() { return mPBR.m_AttachedPhMatPtr != nullptr; }

		// TODO: ���������� ������ � ��� �������(��������� � ��������� �������, ������� ��������)
		[[deprecated]]
		virtual void Load() override;
		ENGINE_INLINE void Load(const String& _path) { SetPath(_path); Load(); }
		ENGINE_INLINE virtual bool OnlyLoad() override;
	private:
		void _Swap(const Material&_mat);
	private:
		std::vector<std::shared_ptr<MaterialPassSh>> passes;

		// Used for Set/Unset functions
		[[Serialized, false]]
		std::shared_ptr<MaterialPassSh>	currentPass = nullptr;

		/*PBR*/
		[[Serialized, true]]
		MaterialSettingsNode mPBR;

		friend class RenderPipeline;
		friend class Serializer;
		friend class Deserializer;
		friend struct Cache;
		friend struct MaterialPassSh;
	};
}