/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

namespace NGTech {
	struct I_Texture;
	struct I_Shader;

	enum class MaterialsSceneParam {
		InCorrect = 0,
		matTime,
		matLightIRadius,

		matGlobalAmbient,
		/**/
		CamViewPosition,
		CamViewDirection,
		CamClipplanes,

		matMVP,
		matProj,
		matView,

		matWorld,
		matWorldInv,
		matViewportTransform,
		matSpotTransform,

		matShadowMap,
		matViewportMap,
		matSpotMap,
	};

	template<class T>
	struct MaterialSampler final
	{
		MaterialSampler():value(nullptr) {}

		MaterialSampler(const String& _t, const String& _sn, T _s, bool _n)
			:TextureName(_t), SamplerName(_sn), value(_s), needlyAddAtr(_n)
		{}

		MaterialSampler(const MaterialSampler& _smp) {
			_Swap(_smp);
		}

		ENGINE_INLINE MaterialSampler& operator=(const MaterialSampler& _smp) {
			_Swap(_smp);
			return *this;
		}

		ENGINE_INLINE bool operator==(const MaterialSampler& _smp) {
			return (TextureName == _smp.TextureName)
				&& (SamplerName == _smp.SamplerName)
				&& (value == _smp.value)
				&& (needlyAddAtr == _smp.needlyAddAtr);
		}

		T value;
		// ������������ ������ ��� u_sampler2D � u_samplerCube
		String TextureName = StringHelper::EMPTY_STRING;
		// ������������ ��� ���������
		String SamplerName = StringHelper::EMPTY_STRING;
		bool needlyAddAtr = false;

	private:
		ENGINE_INLINE void _Swap(const MaterialSampler& _smp) {
			if (this != &_smp) {
				TextureName = _smp.TextureName;
				SamplerName = _smp.SamplerName;
				value = _smp.value;
				needlyAddAtr = _smp.needlyAddAtr;
			}
		}
	};

	/**
	*/
	struct MaterialPassSh final
	{
		MaterialPassSh() {}

		MaterialPassSh(const MaterialPassSh& _smp) {
			_Swap(_smp);
		}

		~MaterialPassSh() {}


		ENGINE_INLINE MaterialPassSh& operator=(const MaterialPassSh& _smp) {
			_Swap(_smp);
			return *this;
		}

		ENGINE_INLINE bool operator==(const MaterialPassSh& _smp) {
			return (name == _smp.name)
				&& (shaderName == _smp.shaderName)
				&& (m_Shader == _smp.m_Shader)
				&& (u_sampler2D == _smp.u_sampler2D)
				&& (u_samplerCube == _smp.u_samplerCube)
				&& (u_float == _smp.u_float)
				&& (u_Vec2 == _smp.u_Vec2)
				&& (u_Vec3 == _smp.u_Vec3)
				&& (u_Vec4 == _smp.u_Vec4)
				&& (u_Mat4 == _smp.u_Mat4)
				&& (u_scene_params == _smp.u_scene_params)
				&& (hasBlending == _smp.hasBlending)
				&& (src == _smp.src)
				&& (dst == _smp.dst)
				&& (hasAlphaTest == _smp.hasAlphaTest)
				&& (alphaFunc == _smp.alphaFunc)
				&& (alphaRef == _smp.alphaRef)
				&& (castShadows == _smp.castShadows)
				&& (recieveShadows == _smp.recieveShadows)
				&& (defines == _smp.defines)
				&& (maxUnitTmp == _smp.maxUnitTmp)
				&& (shadowMapUnitTmp == _smp.shadowMapUnitTmp)
				&& (viewportMapUniTmp == _smp.viewportMapUniTmp)
				&& (spotMapUniTmp == _smp.spotMapUniTmp);
		}

		/**
		*/
		ENGINE_INLINE void AddDefines(const String &_define) {
			defines += _define + " \n";
		}
		/**
		*/
		ENGINE_INLINE void SetName(const String &_name) { name = _name; }

		ENGINE_INLINE String GetNameCopy() { return name; }
		ENGINE_INLINE String& GetNameForChange() { return name; }
	private:
		/*Called only in destructor*/
		void DeleteDataOnlyFromDestructor(const String& _path);

		ENGINE_INLINE void _Swap(const MaterialPassSh& _smp) {
			if (this == &_smp) {
				name = _smp.name;
				shaderName = _smp.shaderName;
				m_Shader = _smp.m_Shader;
				u_sampler2D = _smp.u_sampler2D;
				u_samplerCube = _smp.u_samplerCube;
				u_float = _smp.u_float;
				u_Vec2 = _smp.u_Vec2;
				u_Vec3 = _smp.u_Vec3;
				u_Vec4 = _smp.u_Vec4;
				u_Mat4 = _smp.u_Mat4;
				u_scene_params = _smp.u_scene_params;
				hasBlending = _smp.hasBlending;
				src = _smp.src;
				dst = _smp.dst;
				hasAlphaTest = _smp.hasAlphaTest;
				alphaFunc = _smp.alphaFunc;
				alphaRef = _smp.alphaRef;
				castShadows = _smp.castShadows;
				recieveShadows = _smp.recieveShadows;
				defines = _smp.defines;
				maxUnitTmp = _smp.maxUnitTmp;
				shadowMapUnitTmp = _smp.shadowMapUnitTmp;
				viewportMapUniTmp = _smp.viewportMapUniTmp;
				spotMapUniTmp = _smp.spotMapUniTmp;
			}
		}
	private:
		String		name = "Not defined";
		String		shaderName = "Not defined";
		I_Shader*	m_Shader = nullptr;

		// Format: TextureName, uniformName, TexturePTR
		std::vector<std::shared_ptr<MaterialSampler<I_Texture*>>>					u_sampler2D;
		std::vector<std::shared_ptr<MaterialSampler<I_Texture*>>>					u_samplerCube;
		std::vector<std::shared_ptr<MaterialSampler<float>>>						u_float;
		std::vector<std::shared_ptr<MaterialSampler<Vec2>>>							u_Vec2;
		std::vector<std::shared_ptr<MaterialSampler<Vec3>>>							u_Vec3;
		std::vector<std::shared_ptr<MaterialSampler<Vec4>>>							u_Vec4;
		std::vector<std::shared_ptr<MaterialSampler<Mat4>>>							u_Mat4;
		std::vector<std::shared_ptr<MaterialSampler<MaterialsSceneParam>>>			u_scene_params;

		bool hasBlending = false;
		BlendParam src = BlendParam::BLEND_NONE, dst = BlendParam::BLEND_NONE;

		bool hasAlphaTest = false;
		CompareType alphaFunc = CompareType::COMP_NONE;
		float alphaRef = Math::ZEROFLOAT;

		bool castShadows = true;
		bool recieveShadows = true;

		String defines = StringHelper::EMPTY_STRING;

	private:

		int maxUnitTmp = 0;
		int shadowMapUnitTmp = 0;
		int viewportMapUniTmp = 0;
		int spotMapUniTmp = 0;

		friend class Material;
		friend class Serializer;
		friend class Deserializer;
	};
}