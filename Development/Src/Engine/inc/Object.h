/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//**************************************
#include "../../Common/ScriptableObject.h"
//**************************************
#include "../../Core/inc/Mesh.h"
#include "../../Core/inc/SkinnedMesh.h"
//**************************************
#include "PhysBody.h"
#include "EngineObject.h"
//**************************************

namespace NGTech
{
	class Material;
	class RenderPipeline;
	class PhysBody;
	class ALSound;

	enum EulerAngle
	{
		ANGLE_0 = 0,
		ANGLE_1,
		ANGLE_COUNT
	};

	enum ObjectType {
		OBJECT,
		OBJECT_MESH,
		OBJECT_SKELETEAL_MESH,
		OBJECT_PARTICLE_SYSTEM,
		OBJECT_CAMERA,
		OBJECT_USERCUSTOM1,
		OBJECT_USERCUSTOM2,
		OBJECT_USERCUSTOM3,
		OBJECT_USERCUSTOM4
	};

	//---------------------------------------------------------------------------
	// Desc: base object class
	// 	@availability in script : only as subpart
	//---------------------------------------------------------------------------
	class ENGINE_API BaseEntity : public EngineObject
	{
		CLASS_AVALIBLE_IN_EDITOR(BaseEntity);
	public:
		BaseEntity();

		BaseEntity(const BaseEntity&ob) {
			_Swap(ob);
		}

		BaseEntity &operator =(const BaseEntity &a) {
			_Swap(a); return *this;
		}

		virtual ~BaseEntity();

		/**
		ScriptableObject
		*/
		virtual void AttachScript(const String& _name) override;
		virtual void AttachScript(ScriptInterface*_sc) override;
		virtual void AttachScript(const std::shared_ptr<ScriptInterface>& _sc) override;

		ENGINE_INLINE virtual void SetTransform(const Mat4 &_transform)
		{
			ObjectStateDesc newState(mState);
			newState.transform = _transform;
			SetNewState(newState);
		}

		ENGINE_INLINE virtual const Mat4& GetTransformForChange() {
			/*MT::ScopedGuard guard(mutex);*/
			return mState.transform;
		}

		ENGINE_INLINE virtual Mat4 GetTransform() {
			return GetTransformForChange();
		}

		/* !@Returned Position.*/
		ENGINE_INLINE virtual Vec3 GetPosition() const {
			return mState.transform.GetPosition();
		}

		ENGINE_INLINE float GetAngle() const {
			return mState.angle;
		}

		ENGINE_INLINE void SetAngle(float _angle) {
			/*MT::ScopedGuard guard(mutex);*/
			ObjectStateDesc newState(mState);
			newState.angle = _angle;
			SetNewState(newState);
		}

		/**
		*/
		ENGINE_INLINE virtual void SetPosition(const Vec3 &_pos) {
			//MT::ScopedGuard guard(mutex);
			SetTransform(Mat4::translate(_pos) * Mat4::rotate(mState.angle, Math::Z_AXIS));/*<- Pos*RotateZ*/
		}

		ENGINE_INLINE Vec3 GetScale() {
			return GetTransform().GetScale();
		}

		ENGINE_INLINE void SetScale(const Vec3& _scale) {
			Mat4 tmp = GetTransform();
			tmp.SetScale(_scale);
			SetTransform(tmp);
		}

		ENGINE_INLINE const Vec3& GetUpVector() const {
			return mState.upVector;
		}

		ENGINE_INLINE void SetUpVector(const Vec3&_upVector) {
			/*MT::ScopedGuard guard(mutex);*/
			ObjectStateDesc newState(mState);
			newState.upVector = _upVector;
			SetNewState(newState);
		}

		ENGINE_INLINE virtual ObjectType GetType() {
			/*MT::ScopedGuard guard(mutex);*/
			return OBJECT;
		}

		ENGINE_INLINE bool GetEnabled() const {
			/*MT::ScopedGuard guard(mutex);*/
			return enabled;
		}

		ENGINE_INLINE void SetEnabled(bool _v) {
			//MT::ScopedGuard guard(mutex);
			enabled = _v;
		}

		void _AddThisToContainerOnScene();
		void _DeleteThisToContainerFromScene();

	private:

		ENGINE_INLINE void _Swap(const BaseEntity&_other) {
			if (this != &_other) {
				InterfaceState::_Swap(_other);
				this->enabled = _other.enabled;

#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
				EditableObjectShared::_Swap(_other);
#endif
				//!@todo Call specific function
				// Script interface
				this->m_scripts = _other.m_scripts;
			}
		}

	private:
		bool enabled = true;
	};
}