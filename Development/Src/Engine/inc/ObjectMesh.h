/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include "Object.h"
#include "BasePhysicsObjectEntity.h"
#include "BaseVisualObjectEntity.h"

namespace NGTech
{
	//---------------------------------------------------------------------------
	//Desc: class of the scene object
	//	@availability in script : yes
	//---------------------------------------------------------------------------
	class ENGINE_API ObjectMesh : public BaseEntity, public BaseVisualObjectEntity
	{
		CLASS_AVALIBLE_IN_EDITOR(ObjectMesh);
	public:
		//!@todo �������� - ������ ��������� ������ ������ �� �����, � ������� �� ��� ��������� ������
		ObjectMesh() :ObjectMesh(true) {}

		explicit ObjectMesh(bool _addToSceneList);
		explicit ObjectMesh(const char* path);
		explicit ObjectMesh(const String& path);
		virtual ~ObjectMesh();

		virtual void LoadFromPath(const String &_path) override;

		void Scale(const Vec3&_v);

		//!@todo to protected: used only for render
		virtual void DrawSubset(size_t s, TimeDelta _delta = Math::ZERODELTA) override;

		virtual const Vec3&		GetMax()	override;
		virtual const Vec3&		GetMin()	override;

		virtual const BBox&		GetBBox()	override;
		virtual const BBox&		GetBBox(unsigned int subset) override;

		virtual const Vec3&		GetCenter()	override;
		virtual float			GetRadius()	override;

		virtual const Vec3&		GetMax(unsigned int s)			override;
		virtual const Vec3&		GetMin(unsigned int s)			override;
		virtual const Vec3&		GetCenter(unsigned int s)		override;
		virtual float			GetRadius(unsigned int s)		override;
		virtual const String&	GetSubsetName(unsigned int id)	override;
		virtual const BSphere&	GetBSphere() override;

		/**
		get BSphere of the object subset
		*/
		virtual const BSphere& GetBSphere(unsigned int subset) override;

		virtual Material* GetMaterial(unsigned int s)	const override;

		ENGINE_INLINE virtual BBox&			 GetTransformedBBox()		override { tempdata.Box = GetTransform() * GetBBox(); return tempdata.Box; }
		ENGINE_INLINE virtual BSphere&		 GetTransformedBSphere()	override { tempdata.Sphere = GetTransform() * GetBSphere(); return tempdata.Sphere; }

		virtual void SetMaterial(const String &path, const String &subsetName = "*") override;
		/*Setting material and loading Immediately*/
		virtual void SetMaterial(Material *mat, const String &subsetName = "*") override;

		virtual void SetTransform(const Mat4 &trans) override;
		//!@todo ��� ������ ���� ������ �����������
		virtual const Mat4& GetTransformForChange() override;
		virtual Mat4 GetTransform() override;

		// Avalible for OC check or not
		ENGINE_INLINE virtual bool HasQuery() const  override { return true; }

		/*Will call DrawSubset with OQ*/
		virtual bool RenderQuery(I_RenderLowLevel*, size_t _index, TimeDelta _delta) override;
		/*Will call DrawSubset only*/
		virtual bool RenderFast(I_RenderLowLevel*, size_t _index, TimeDelta _delta)	 override;

		ENGINE_INLINE virtual ObjectType GetType() override { return OBJECT_MESH; };

		/*Physics*/
		void SetPhysicsBox(const Vec3 &size, TimeDelta mass);
		void SetPhysicsSphere(TimeDelta rad, TimeDelta mass);

		void SetPhysicsCylinder(TimeDelta radius, TimeDelta width, TimeDelta mass);
		void SetPhysicsCapsule(TimeDelta radius, TimeDelta height, TimeDelta mass);

		void _SetPhysicsConvexHullImpl(TimeDelta mass);
		void _SetPhysicsStaticMeshImpl();
	public:
		BasePhysicsObjectEntity<ObjectMesh*> *m_PhysicsEntity = new BasePhysicsObjectEntity<ObjectMesh*>(this);
	private:
		void _AddToSceneList();
		void _SetPhysicsMaterial(Material* _mat);

		void _SetPhysicsMaterial(PhysXMaterialSh * _phmat);

		friend class RenderPipeline;
		friend class PhysBody;
	};
}