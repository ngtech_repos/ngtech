/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#ifndef PHYSBODY_H
#define PHYSBODY_H 1

//***************************************************************************
#include "../../Common/NonCopyable.h"
//***************************************************************************
#include "../../Core/inc/IncludesAndLibs.h"
//***************************************************************************
#include "ALSound.h"
#include "ALSoundSource.h"
//***************************************************************************
namespace physx
{
	class PxBase;
	class PxActor;
	class PxRigidActor;
	class PxRigidBody;
}
namespace NGTech {
	/**/
	struct PhysXMaterialSh;

	/**/
	struct ENGINE_API PhysBodyDescSh final
	{
		PhysBodyDescSh()
		{}

		~PhysBodyDescSh()
		{}

		bool operator==(const PhysBodyDescSh& _desc)
		{
			return
				(mCollisionType == _desc.mCollisionType)
				&& (LinearVelocity == _desc.LinearVelocity)
				&& (AngularVelocity == _desc.AngularVelocity)
				&& (CollisionSize == _desc.CollisionSize)
				&& (AppliedTorque == _desc.AppliedTorque)
				&& (AppliedForce == _desc.AppliedForce)
				&& (MassSpaceInertiaTensor == _desc.MassSpaceInertiaTensor)
				&& (Math::IsEqual(CollisionRadius, _desc.CollisionRadius))
				&& (Math::IsEqual(CollisionWidth, _desc.CollisionWidth))
				&& (Math::IsEqual(CollisionHeight, _desc.CollisionHeight))
				&& (Math::IsEqual(CollisionHeight, _desc.CollisionHeight))
				&& (Math::IsEqual(CollisionMass, _desc.CollisionMass))
				&& (Math::IsEqual(CollisionDensity, _desc.CollisionDensity))
				&& (Math::IsEqual(LinearDamping, _desc.LinearDamping))
				&& (Math::IsEqual(AngularDamping, _desc.AngularDamping))
				&& (m_AttachedPhysicsMaterial == _desc.m_AttachedPhysicsMaterial)
				&& (NumVertTmp == _desc.NumVertTmp)
				&& (NumFacesTmp == _desc.NumFacesTmp)
				&& (VerticesArrTmp == _desc.VerticesArrTmp)
				&& (IndicesArrTmp == _desc.IndicesArrTmp)
				&& (mActorTmp == _desc.mActorTmp)
				&& (TransformTmp == _desc.TransformTmp)

				&& (mSaveAsMeshNameTmp == _desc.mSaveAsMeshNameTmp)
				&& (mOriginalMeshNameTmp == _desc.mOriginalMeshNameTmp)
				&& (mImpactSoundPathTmp == _desc.mImpactSoundPathTmp);
		}

		ENGINE_INLINE void ComputeGlobalPose(const Mat4&_m) { this->TransformTmp = _m; }
		void CleanResources();

		/**/
		enum class CollisionType
		{
			CT_NONE = 0,
			CT_BOX,
			CT_SPHERE,
			CT_CYLINDER,
			CT_CAPSULE,
			CT_CONVEX,
			CT_STATIC
		} mCollisionType = CollisionType::CT_NONE;

		Vec3 LinearVelocity = Vec3::ZERO;
		Vec3 AngularVelocity = Vec3::ZERO;
		Vec3 CollisionSize = Vec3::ZERO;
		Vec3 AppliedTorque = Vec3::ZERO;
		Vec3 AppliedForce = Vec3::ZERO;
		Vec3 MassSpaceInertiaTensor = Vec3::ZERO;

		TimeDelta CollisionRadius = Math::ZEROFLOAT;
		TimeDelta CollisionWidth = Math::ZEROFLOAT;
		TimeDelta CollisionHeight = Math::ZEROFLOAT;
		TimeDelta CollisionMass = Math::ZEROFLOAT;
		TimeDelta CollisionDensity = DEFAULT_DENSITY;

		TimeDelta LinearDamping = Math::ZERODELTA;
		TimeDelta AngularDamping = Math::ZERODELTA;

		PhysXMaterialSh* m_AttachedPhysicsMaterial = nullptr;
		// TODO: ����� �������� ��������, ����� ���� ���������� ��������, �� ������ ����� ��������

		// Temp Data
		unsigned int NumVertTmp = 0;
		unsigned int NumFacesTmp = 0;

		void* VerticesArrTmp = nullptr;
		unsigned int * IndicesArrTmp = nullptr;

		physx::PxRigidActor *mActorTmp = nullptr;
		Mat4 TransformTmp = Mat4::IDENTITY;

		// todo: ser and implement this
		String mSaveAsMeshNameTmp = StringHelper::EMPTY_STRING;
		String mOriginalMeshNameTmp = StringHelper::EMPTY_STRING;
		[[deprecated]]
		String mImpactSoundPathTmp = StringHelper::EMPTY_STRING;

		static const TimeDelta DEFAULT_DENSITY;
	};

	//---------------------------------------------------------------------------
	//Desc: Rigid body struct
	//---------------------------------------------------------------------------
	class ENGINE_API PhysBody final
	{
	public:
		PhysBody() {}

		static PhysBody* CreateBox(const Vec3 &size, const Mat4 &_trans, TimeDelta mass, TimeDelta density, PhysXMaterialSh* _phMat = nullptr);
		static PhysBody* CreateSphere(TimeDelta radius, const Mat4 &_trans, TimeDelta mass, TimeDelta density, PhysXMaterialSh* _phMat = nullptr);

		static PhysBody* CreateCylinderConvex(TimeDelta radius, TimeDelta height, const Mat4 &_trans, TimeDelta mass, TimeDelta density, PhysXMaterialSh* _phMat = nullptr);
		static PhysBody* CreateCapsule(TimeDelta radius, TimeDelta height, const Mat4 &_trans, TimeDelta mass, TimeDelta density, PhysXMaterialSh* _phMat = nullptr);

		static PhysBody* CreateConvexHull(unsigned int _numVert, unsigned int _numFaces, const Mat4 &_trans, void*_vertices, unsigned int*_indices, TimeDelta _mass, TimeDelta _density, const String& _save_as, const String& originalName, PhysXMaterialSh* _phMat = nullptr);
		static PhysBody* CreateStaticMesh(unsigned int _numVert, unsigned int _numFaces, const Mat4 &_trans, void*_vertices, unsigned int*, TimeDelta _density, const String& _save_as, const String& originalName, PhysXMaterialSh* _phMat = nullptr);

		PhysBody(const PhysBodyDescSh& _d) :mDesc(_d) {}

		~PhysBody();

		void SetTransform(const Mat4 &trans);

		const Mat4 GetTransform();
		const Mat4& GetTransformForChange();

		void ComputeTransformFromPhysX();

		TimeDelta GetMass();
		void SetMass(TimeDelta mass);

		void AddForce(const Vec3 &force);
		void AddTorque(const Vec3 &torque);

		void SetLinearVelocity(const Vec3 &velocity);
		const Vec3 GetLinearVelocity() const;
		const Vec3 ComputeLinearVelicity();

		/**/
		void SetAngularVelocity(const Vec3 &velocity);
		const Vec3 GetAngularVelocity() const;
		const Vec3 ComputeAngularVelicity();

		ENGINE_INLINE PhysBodyDescSh::CollisionType GetCollisionType() const { return mDesc.mCollisionType; }

		void SetLinearDamping(TimeDelta _v);
		void SetAngularDamping(TimeDelta _v);
		void SetMassSpaceInertiaTensor(const Vec3&);

		void RecreateIfNeeded(const PhysBodyDescSh& _desc) { if (mDesc.mActorTmp != nullptr) _CreateFromDesc(mDesc); else mDesc = _desc; }

		ENGINE_INLINE const Vec3& GetCollisionSize() const { return mDesc.CollisionSize; }
		ENGINE_INLINE TimeDelta GetCollisionRad()  const { return mDesc.CollisionRadius; }
		ENGINE_INLINE TimeDelta GetCollisionWidth()  const { return mDesc.CollisionWidth; }
		ENGINE_INLINE TimeDelta GetCollisionHeight()  const { return mDesc.CollisionHeight; }

		ENGINE_INLINE PhysBodyDescSh GetDescCopy() { return mDesc; }
		ENGINE_INLINE PhysBodyDescSh &GetDescForChange() { return mDesc; }
	private:
		void _CreateFromDesc(const PhysBodyDescSh&_desc);
		void _SetPhysicsNone();
		void _CreatePhysicsBox();
		void _CreatePhysicsSphere();
		void _CreatePhysicsCylinder();
		void _CreatePhysicsCapsule();
		void _CreatePhysicsConvex();
		void _CreatePhysicsStaticMesh();

		void _Cleanup();

		PhysXMaterialSh* _GetPhysicsMaterial();
	private:
		PhysBodyDescSh mDesc;

		friend class PhysSystem;
		friend class PhysJoint;
		friend class PhysJointUpVector;
	};
}

#endif // PHYSBODY_H