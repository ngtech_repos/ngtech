/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include  <set>
//***************************************************************************
#include "PhysXForwardDecl.h"
//***************************************************************************
#include "Physics/PhysScene.h"
#include "PhysXSceneData.h"
//***************************************************************************

namespace NGTech {
	/**
	*/
	class	Engine;
	/**
	*/
	class	PhysXCharacterController;
	class	VehicleSceneQueryData;
	struct	CharacterControllerDesc;
	struct	PhysXMaterialSh;

	/**
	Engine`s main physics system. Created once
	*/
	ATTRIBUTE_ALIGNED16(class) ENGINE_API PhysSystem : public ALIGNED_AT16
	{
	public:
		PhysSystem(bool _singleThreading);
		virtual ~PhysSystem();

		void Initialise();
		void UpdateSingleThreaded(TimeDelta _delta);

		ENGINE_INLINE physx::PxFoundation* GetPxFoundation() const { return mFoundation; }
		ENGINE_INLINE physx::PxPhysics* GetPxPhysics() const { return mPhysics; }
		ENGINE_INLINE physx::PxCooking* GetPxCooking() const { return mCooking; }

		ENGINE_INLINE physx::PxDefaultCpuDispatcher* GetPxDefaultCpuDispatcher() const { return mCpuDispatcher; }
		ENGINE_INLINE physx::PxScene* GetActivePxScene() { return m_ActivePhysScene.m_SceneData.m_PhysXScene; }

		ENGINE_INLINE PhysScene& GetActivePhysScene() { return m_ActivePhysScene; }
		ENGINE_INLINE void SetActivePhysScene(const PhysScene& _physicsScene) { m_ActivePhysScene = _physicsScene; }

		// PCharacter
		physx::PxController* CreateCapsuleCharacterController(PhysXCharacterController*_ch, const CharacterControllerDesc* _desc);
		physx::PxController* CreateBoxCharacterController(PhysXCharacterController*_ch, const CharacterControllerDesc* _desc);

		void CreateSceneFromSceneDesc(const physx::PxSceneDesc*_sceneDesc, PhysXSceneData& _sceneDataForChange);
		void CreateCUDADispatcherUtil(physx::PxSceneDesc*_sceneDesc);

	private:
		PhysSystem();

		/*Threads*/
		void UpdateMultithreaded();

		void _InitThread();
		void _DestroyThread();
		static void _UpdateLoop(void*);
		void _CreateCPUDispatcher();
		void _CreateGPUDispatcher();

		void _InitVehicleSupport();
	private:
		MT::Thread						updateThread;
		physx::PxPvd*					mDebuggerConnection = nullptr;
		physx::PxFoundation*			mFoundation = nullptr;
		physx::PxPhysics*				mPhysics = nullptr;
		physx::PxCooking*				mCooking = nullptr;
		physx::PxDefaultCpuDispatcher*	mCpuDispatcher = nullptr;
#if PLATFORM_OS == PLATFORM_OS_WINDOWS
		physx::PxCudaContextManager*    mCudaContextManager = nullptr;
#endif
		bool							mNeedlyDestroyThread = false;
		bool							mPhysxExtensions = false;
		bool							mVehicleSDKInitialized = false;

		TimeDelta						_epeAccumilator = Math::ZERODELTA;
		TimeDelta						_epeTimeStep = Math::ZERODELTA;
		TimeDelta						_savedTimeStep = Math::ZERODELTA;
		TimeDelta						_timeStep = Math::ZERODELTA;

		uint32_t						_nSteps = 0;

		PhysScene						m_ActivePhysScene;
		bool							m_SingleThread = false;

		friend class					PhysBody;
		friend class					PhysJoint;
		friend class					PhysJointUpVector;
	};
}