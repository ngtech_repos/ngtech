/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include <PxSimulationEventCallback.h>
#include <PxContactModifyCallback.h>

namespace NGTech
{
	struct PhysXMaterialSh;

	/*
	Simulation Event Callback
	*/
	class PhysXCallback : public physx::PxSimulationEventCallback, public physx::PxContactModifyCallback
	{
	public:
		void Insert(const PhysBodyDescSh& _desc);

		virtual ~PhysXCallback() {}
	private:
		virtual void onConstraintBreak(physx::PxConstraintInfo* constraints, physx::PxU32 count) override;

		virtual void onWake(physx::PxActor** actors, physx::PxU32 count) override;

		virtual void onSleep(physx::PxActor** actors, physx::PxU32 count) override;

		virtual void onContact(const physx::PxContactPairHeader& pairHeader, const physx::PxContactPair* pairs, physx::PxU32 nbPairs) override;

		virtual void onTrigger(physx::PxTriggerPair* pairs, physx::PxU32 count) override;

		virtual void onContactModify(physx::PxContactModifyPair* const pairs, physx::PxU32 count) override;

		virtual void onAdvance(const physx::PxRigidBody*const* bodyBuffer, const physx::PxTransform* poseBuffer, const physx::PxU32 count) override {}

		void _PlaySound(physx::PxRigidActor*);
	private:
		std::map<physx::PxRigidActor*, PhysXMaterialSh*> m_actorToImpactSound;
	};
}