/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//***************************************************************************
#include "PxPhysicsAPI.h"
//***************************************************************************

namespace physx
{
	class PxController;
	struct PxExtendedVec3;
}

namespace NGTech
{
	/*
	@availability in script: yes
	*/
	struct CharacterControllerDesc
	{
		enum Mode
		{
			CCT_CAPSULE = 0,
			CCT_BOX,
			CCT_NUM
		};

		CharacterControllerDesc() {}
		CharacterControllerDesc(Mode _mMode) : mMode(_mMode) {	}

		float height = Math::ONEFLOAT;
		float radius = 5 * 0.4;
		float density = 0.1f;
		float contactOffset = 0.1f; // Originally had it lower, raising it didn't seem to do much. Must be positive
		float stepOffset = 0.1f;
		float invisibleWallHeight = Math::ZEROFLOAT;
		float maxJumpHeight = 2.46f; //������ ����,� ������,� ������
		float scaleCoeff = Math::ONEFLOAT;
		float slopeLimit = 90.0f; //���� �������
		float volumeGrowth = 1.5f;

		Vec3 position = Vec3::ZERO;
		Vec3 upDirection = Math::Y_AXIS;
		Mode mMode = CCT_CAPSULE;
	};

	/*
	@availability in script: yes
	*/
	class PhysXCharacterController
	{
	public:
		PhysXCharacterController();
		explicit PhysXCharacterController(const CharacterControllerDesc&desc);
		~PhysXCharacterController();

		void Move(float x, float y, float z, TimeDelta elapsedTime);

		/**/
		Vec3 GetPosition() const;
		/* Just teleport to new pos*/
		void SetPosition(const Vec3&_pos);

		void Jump(Vec3 &height);
		bool CanJump();
		const CharacterControllerDesc& GetDescription() const;
	private:
		class JumpAction
		{
		public:
			JumpAction();

			float		mV0;
			float		mJumpTime;
			bool		mJump;

			void		StartJump(float v0);
			void		StopJump();
			TimeDelta		GetHeight(TimeDelta elapsedTime, TimeDelta _descHeight);
		};
		JumpAction mJump;
	private:
		TimeDelta _JumpGetHeight(TimeDelta _time, TimeDelta _descHeight);
		void _CreateCapsuleCharacterController();
		void _InitCharacterController();
		void _CreateBoxCharacterController();
	private:
		physx::PxController* mController = nullptr;
		CharacterControllerDesc desc;
		Vec3 mPos;
	};
}