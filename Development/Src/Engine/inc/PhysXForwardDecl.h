#pragma once

namespace physx
{
	class PxPvd;
	class PxPhysics;
	class PxCooking;
	class PxDefaultCpuDispatcher;
	class PxFoundation;
	class PxScene;
	class PxMaterial;
	class PxCudaContextManager;
	class PxControllerManager;
	class PxSceneDesc;
	class PxActor;
	class PxShape;
	class PxRigidDynamic;
	class PxRigidStatic;
	class PxCapsuleControllerDesc;
	class PxController;
	class PxRigidActor;
	class PxTriangleMesh;
	class PxCollection;
	class PxBatchQuery;
	class PxVehicleDrivableSurfaceToTireFrictionPairs;

	class PxVehicleDrive;
	class PxVehicleDrive4W;
	class PxVehicleDriveTank;

	class PxVehicleNoDrive;
	class PxConvexMesh;
	class PxVehicleChassisData;
	class PxVehicleWheelsSimData;

	// Serialization
	class PxInputData;
	class PxOutputStream;
	class PxSerializationRegistry;
}