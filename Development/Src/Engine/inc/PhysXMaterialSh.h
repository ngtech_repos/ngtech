/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//***************************************************************************
#include "PhysXForwardDecl.h"
//***************************************************************************

namespace NGTech
{
	// !@Creation only from factors. Shared structure! Only for data-management!
	struct PhysXMaterialSh :public RefCount
	{
	public:
		PhysXMaterialSh(TimeDelta staticFriction, TimeDelta dynamicFriction, TimeDelta restitution,
			const String& materialName, bool vehicleDrivableSurface, const String& materialsMapKey);

		virtual ~PhysXMaterialSh();

		PhysXMaterialSh(const PhysXMaterialSh&_ph) {
			_Swap(_ph);
		}

		PhysXMaterialSh& operator = (const PhysXMaterialSh &_ph) {
			_Swap(_ph);
			return *this;
		}

		void SetStaticFriction(TimeDelta _fr);
		void SetDynamicFriction(TimeDelta _fr);
		void SetRestitution(TimeDelta _r);

		TimeDelta GetStaticFriction() { return m_StaticFriction; }
		TimeDelta GetDynamicFriction() { return m_DynamicFriction; }
		TimeDelta GetRestitution() { return m_Restitution; }

		bool IsVehicleDrivableSurface() { return m_VehicleDrivableSurface; }
		void SetVehicleDrivableSurface(bool _v) { m_VehicleDrivableSurface = _v; }

		String GetMaterialsMapKey() { return m_MaterialsMapKey; }
		void SetMaterialsMapKey(const String&_v) { m_MaterialsMapKey = _v; }

		physx::PxMaterial* GetPxMaterial() { ASSERT(m_PxMaterial, "mPxMaterial is Null"); return m_PxMaterial; }
		void SetPxMaterial(physx::PxMaterial*_phMat) { ASSERT(_phMat, "Provided invalid pointer on physx::PxMaterial* _phMat"); m_PxMaterial = _phMat; }

		void SetCombineModeMultiplyFriction();
		void SetCombineModeMultiplyRestitution();

		void SetImpactSoundForPhysBody(PhysBody* _pb, ALSound* snd);
		ENGINE_INLINE void SetImpactSound(ALSound* snd) { ASSERT(snd, "Invalid pointer on snd"); if (!snd) return; m_CollisionSound = snd; }

		ENGINE_INLINE ALSound* GetCollisionSound() const
		{
			// Without assert, impactSrc can be null!
			if (!m_CollisionSound)
				return nullptr;

			return m_CollisionSound;
		}

		static const TimeDelta frictionCoef;
	private:
		PhysXMaterialSh();

		void _Swap(const PhysXMaterialSh&_ph) {
			if (this != &_ph)
			{
				m_StaticFriction = _ph.m_StaticFriction;
				m_DynamicFriction = _ph.m_DynamicFriction;
				m_Restitution = _ph.m_Restitution;
				m_MaterialName = _ph.m_MaterialName;
				m_MaterialsMapKey = _ph.m_MaterialsMapKey;
				m_VehicleDrivableSurface = _ph.m_VehicleDrivableSurface;
				m_PxMaterial = _ph.m_PxMaterial;
				m_CollisionSound = _ph.m_CollisionSound;
			}
		}

	private:
		[[Serialized, true]]
		TimeDelta m_StaticFriction = Math::ZERODELTA;
		TimeDelta m_DynamicFriction = Math::ZERODELTA;
		TimeDelta m_Restitution = Math::ZERODELTA;
		bool m_VehicleDrivableSurface = false;

		// Only Raw pointers
		ALSound * m_CollisionSound = nullptr;

		[[Serialized, false]]
		String m_MaterialName = StringHelper::EMPTY_STRING;
		String m_MaterialsMapKey = StringHelper::EMPTY_STRING;

		physx::PxMaterial* m_PxMaterial = nullptr;

		friend struct PhysicsMaterialsManager;
	};
}