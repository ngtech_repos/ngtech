#pragma once

#include "PhysXForwardDecl.h"
#include "Physics/VehicleScene.h"

namespace NGTech
{
	class PhysXCallback;
	class VehicleSceneQueryData;
	/**/
	struct PhysXSceneData
	{
		PhysXSceneData();
		~PhysXSceneData();

		PhysXSceneData(const PhysXSceneData& _scene);

		PhysXSceneData& operator=(const PhysXSceneData&_other);

		bool IsValid() {
			return (m_PhysXScene != nullptr) && (m_ControllerManager != nullptr);
		}

		void Release() { delete this; }

		physx::PxScene* GetPxScene() { return m_PhysXScene; }

	private:
		physx::PxScene*	m_PhysXScene = nullptr;
		physx::PxControllerManager*	m_ControllerManager = nullptr;
		PhysXCallback*	m_PhCallback = nullptr;
		VehicleScene	m_Vehicle;

		enum class UpdateMethod
		{
			// Sub-step is lower
			UPDATEMETHOD_SUB_STEP = 0,
			UPDATEMETHOD_DELTA,
			UPDATEMETHOD_UNKNOW
		}m_UpdateMethod = UpdateMethod::UPDATEMETHOD_DELTA;

	private:
		void Cleanup();

		friend class PhysSystem;
		friend struct PhysScene;
		friend struct PhysXSystemBodys;
	};
}