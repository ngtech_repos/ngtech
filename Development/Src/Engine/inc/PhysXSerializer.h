#pragma once

#include "PhysXForwardDecl.h"

namespace NGTech
{
	using namespace physx;
	//!@todo DEPRECATED!
	struct ENGINE_API PhysXSerializer
	{
		PhysXSerializer(physx::PxPhysics* _P, physx::PxScene* _mActiveScene, bool _UseBinarySerialization) {
			_InitShP(_UseBinarySerialization, _P, _mActiveScene);
		}

		/**
		Calling _InitShP with false,  GetPhysics()->GetPxPhysics(), GetPhysics()->GetPxScene()
		*/
		PhysXSerializer();

		~PhysXSerializer();

		static void SerializeObjectFromWorld(PxShape* _shape, const String& _fileName);
		static void SerializeAllObjectsFromWorld(const String& _dir, const String& _sceneName);

	private:
		bool _InitShP(bool _UseBinarySerialization, physx::PxPhysics*_P, physx::PxScene*_ActiveScene);
		/**
		Create objects, add them to collections and serialize the collections to the steams gSharedStream and gActorStream
		This function doesn't setup the gPhysics global as the corresponding physics object is only used locally
		*/
		void _SerializeObjects(PxOutputStream* sharedStream, PxOutputStream* actorStream, PxShape* shape);
		void _SerializeEverything(const String& _dir, const String& _sceneName);
		/**
		Deserialize shared data and use resulting collection to deserialize and instance actor collections
		*/
		void _DeserializeObjects(PxInputData*& sharedData, PxInputData*& actorData);
	private:
		bool mUseBinarySerialization = false;

		// The physics SDK object
		// This will be cleaned on PhysicsModule
		physx::PxPhysics* mPhysics = nullptr;
		// This will be cleaned on PhysicsModule
		physx::PxScene*  mActiveScene = nullptr;
	};
}