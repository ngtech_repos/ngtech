#pragma once

namespace NGTech
{
	/**
	Utils for creation physics bodies
	*/
	struct PhysXSystemBodys
	{
		/**
		Creates PhysX box primitive
		\param[in] _trans Transformation matrix of physics body
		\param[in] _size Size of physics body
		\param[in] _mass Mass of physics body
		\param[in] _density Density of physics body
		\param[in] _pxCook ref on physx::PxCooking
		\param[in] _pxPhysics ref on physx::PxPhysics
		\param[in] _pxMaterial ref on physx::PxMaterial
		\param[in] _pxScene ref on physx::PxScene

		\return physx::PxRigidDynamic pointer
		*/
		static physx::PxRigidDynamic* CreateBox(const Mat4&_trans, const Vec3& _size, TimeDelta _mass, TimeDelta _density, physx::PxCooking& _pxCook, physx::PxPhysics& _pxPhysics, physx::PxMaterial& _pxMaterial, physx::PxScene& _pxScene);

		/**
		Creates PhysX sphere primitive
		\param[in] _trans Transformation matrix of physics body
		\param[in] _radius Radius of physics body
		\param[in] _mass Mass of physics body
		\param[in] _density Density of physics body
		\param[in] _pxCook ref on physx::PxCooking
		\param[in] _pxPhysics ref on physx::PxPhysics
		\param[in] _pxMaterial ref on physx::PxMaterial
		\param[in] _pxScene ref on physx::PxScene

		\return physx::PxRigidDynamic pointer
		*/
		static physx::PxRigidDynamic* CreateSphere(const Mat4&_trans, TimeDelta _radius, TimeDelta _mass, TimeDelta _density, physx::PxCooking& _pxCook, physx::PxPhysics& _pxPhysics, physx::PxMaterial& _pxMaterial, physx::PxScene& _pxScene);

		/**
		Creates PhysX cylinder primitive
		\param[in] _trans Transformation matrix of physics body
		\param[in] _radius Radius of physics body
		\param[in] _width Width of physics body
		\param[in] _mass Mass of physics body
		\param[in] _density Density of physics body
		\param[in] _pxCook ref on physx::PxCooking
		\param[in] _pxPhysics ref on physx::PxPhysics
		\param[in] _pxMaterial ref on physx::PxMaterial
		\param[in] _pxScene ref on physx::PxScene

		\return physx::PxRigidDynamic pointer
		*/
		static physx::PxRigidDynamic* CreateCylinderConvex(const Mat4&_trans, TimeDelta _radius, TimeDelta _width, TimeDelta _mass, TimeDelta _density, physx::PxCooking& _pxCook, physx::PxPhysics& _pxPhysics, physx::PxMaterial& _pxMaterial, physx::PxScene& _pxScene);

		/**
		Creates PhysX static mesh
		\param[in] _numVert number of vertixes of physics body
		\param[in] _numFaces number of faces of physics body
		\param[in] _trans Transformation matrix of physics body
		\param[in] _vertices Pointer on Vertices of physics body
		\param[in] _mass Mass of physics body
		\param[in] _density Density of physics body
		\param[in] _pxCook ref on physx::PxCooking
		\param[in] _pxPhysics ref on physx::PxPhysics
		\param[in] _pxMaterial ref on physx::PxMaterial
		\param[in] _pxScene ref on physx::PxScene

		\return physx::PxRigidStatic pointer
		*/
		static physx::PxRigidStatic*  CreateStaticMesh(unsigned int _numVert, unsigned int _numFaces, const Mat4 &_trans, void*_vertices, unsigned int*_indices, TimeDelta _density, physx::PxCooking* _pxCook, physx::PxPhysics* _pxPhysics, physx::PxMaterial* _pxMaterial, physx::PxScene* _pxScene);

		/**
		Creates ConvexHull PhysX primitive
		\param[in] _numVert number of vertixes of physics body
		\param[in] _numFaces number of faces of physics body
		\param[in] _trans Transformation matrix of physics body
		\param[in] _vertices Pointer on Vertices of physics body
		\param[in] _mass Mass of physics body
		\param[in] _density Density of physics body
		\param[in] _pxCook ref on physx::PxCooking
		\param[in] _pxPhysics ref on physx::PxPhysics
		\param[in] _pxMaterial ref on physx::PxMaterial
		\param[in] _pxScene ref on physx::PxScene

		\return physx::PxRigidDynamic pointer
		*/
		static physx::PxRigidDynamic* CreateConvexHull(unsigned int _numVert, unsigned int _numFaces, const Mat4 &_trans, void*_vertices, TimeDelta _mass, TimeDelta _density, physx::PxCooking* _pxCook, physx::PxPhysics* _pxPhysics, physx::PxMaterial* _pxMaterial, physx::PxScene* _pxScene);

		/**
		Creates Capsule PhysX primitive
		\param[in] _radius Radius of physics body
		\param[in] _height Height of physics body
		\param[in] _trans Transformation matrix of physics body
		\param[in] _mass Mass of physics body
		\param[in] _density Density of physics body
		\param[in] _pxCook ref on physx::PxCooking
		\param[in] _pxPhysics ref on physx::PxPhysics
		\param[in] _pxMaterial ref on physx::PxMaterial
		\param[in] _pxScene ref on physx::PxScene

		\return physx::PxRigidDynamic pointer
		*/
		static physx::PxRigidDynamic* CreateCapsule(TimeDelta _radius, TimeDelta _height, const Mat4 & _trans, TimeDelta _mass, TimeDelta _density, physx::PxCooking& _pxCook, physx::PxPhysics& _pxPhysics, physx::PxMaterial& _pxMaterial, physx::PxScene& _pxScene);

		/**
		Util for serialize physx bodies into xml/reb file.
		\param[in] _pxShape PhysX shape pointer
		\param[in] _cookedMeshFilename Filename of Cooked PhysX Mesh
		*/
		void SerializePhysXBody(physx::PxShape*_pxShape, const String& _cookedMeshFilename);
	};

#include "PhysXSystemBodys.inl"
}