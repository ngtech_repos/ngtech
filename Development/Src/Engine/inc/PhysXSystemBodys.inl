#pragma once

template <class T>
ENGINE_INLINE physx::PxConvexMesh* _CreateConvexMesh(const T* verts, const uint32_t numVerts,
	physx::PxPhysics& physics, physx::PxCooking& cooking);