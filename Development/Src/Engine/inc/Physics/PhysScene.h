/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//***************************************************************************
#include "PhysXForwardDecl.h"
#include "PhysXSceneData.h"
//***************************************************************************

namespace physx
{
	class PxBatchQuery;
	class PxVehicleDrivableSurfaceToTireFrictionPairs;
}

namespace NGTech
{
	/**
	*/
	class VehicleSceneQueryData;
	struct PhysBodyDescSh;

	/**
	����� ���������� �����
	�����, �.� � ��� ����� ���� ��������� ���������� ����(���������� ����� �� ������ �����/��������� ����� ������ � ���������� ���������)
	*/
	struct PhysScene
	{
		PhysXSceneData  m_SceneData;

		PhysScene() {}
		~PhysScene() {}

		void Create(PhysSystem*_phSystem);
		void CreateWithGetPhysicsPtr();

		void Release();
		void Clear();
		void SceneResetDynamicEntities();

		void SimulateScene(TimeDelta _delta);

		void _UpdateWithDelta(TimeDelta dTime);
		void _UpdateWithSubStep(TimeDelta dTime);

		void InsertPhActorToCollisionCallback(const PhysBodyDescSh& _desc);

		void SetGravity(const Vec3&_vec);
		Vec3 GetGravity();
	private:
		void _CreateSceneDesc();
	private:
		PhysSystem * m_PhysSystem = nullptr;

		friend struct PhysXSceneData;
	};
}