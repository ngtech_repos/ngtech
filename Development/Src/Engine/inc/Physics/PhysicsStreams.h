#pragma once

#include "PxPhysicsAPI.h"

namespace NGTech
{
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	class MyMemoryOutputStream : public physx::PxOutputStream
	{
	private:
		physx::PxU8* mData;
		physx::PxU32 mSize;
		physx::PxU32 mCapacity;

	public:
		MyMemoryOutputStream() : mData(NULL), mSize(0), mCapacity(0) {}

		MyMemoryOutputStream(int initialCapacity)
		{
			mCapacity = initialCapacity;
			mSize = 0;
			mData = new physx::PxU8[mCapacity];
		}

		virtual ~MyMemoryOutputStream()
		{
			if (mData)
				delete[] mData;
		}

		physx::PxU32 write(const void* src, physx::PxU32 size)
		{
			physx::PxU32 expectedSize = mSize + size;
			if (expectedSize > mCapacity)
			{
				if (mCapacity == 0)
					mCapacity = 4;
				while (expectedSize > mCapacity)
					mCapacity *= 2;

				physx::PxU8* newData = new physx::PxU8[mCapacity];
				if (mData)
				{
					memcpy(newData, mData, mSize);
					delete[] mData;
				}
				mData = newData;
			}
			memcpy(mData + mSize, src, size);
			mSize += size;
			return size;
		}

		physx::PxU32 getSize()	const { return mSize; }
		physx::PxU8* getData()	const { return mData; }
	};

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	class MyMemoryInputData : public physx::PxInputData
	{
	private:
		physx::PxU8* mData;
		physx::PxU32 mSize;
		physx::PxU32 mPosition;

	public:
		MyMemoryInputData(physx::PxU8* data, physx::PxU32 size)
		{
			this->mData = data;
			this->mSize = size;
			this->mPosition = 0;
		}

		physx::PxU32 read(void* dest, physx::PxU32 count)
		{
			physx::PxU32 length = physx::PxMin<physx::PxU32>(count, mSize - mPosition);
			memcpy(dest, mData + mPosition, length);
			mPosition += length;
			return length;
		}

		physx::PxU32 getLength() const
		{
			return mSize;
		}

		void seek(physx::PxU32 offset)
		{
			mPosition = physx::PxMin<physx::PxU32>(mSize, offset);
		}

		physx::PxU32 tell() const
		{
			return mPosition;
		}
	};
}