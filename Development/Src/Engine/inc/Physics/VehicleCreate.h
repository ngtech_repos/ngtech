/*
 * Copyright (c) 2008-2015, NVIDIA CORPORATION.  All rights reserved.
 *
 * NVIDIA CORPORATION and its licensors retain all intellectual property
 * and proprietary rights in and to this software, related documentation
 * and any modifications thereto.  Any use, reproduction, disclosure or
 * distribution of this software and related documentation without an express
 * license agreement from NVIDIA CORPORATION is strictly prohibited.
 */
 // Copyright (c) 2004-2008 AGEIA Technologies, Inc. All rights reserved.
 // Copyright (c) 2001-2004 NovodeX AG. All rights reserved.

#pragma once

#ifndef VEHICLE_COMMON_H
#define VEHICLE_COMMON_H

#include "PxPhysicsAPI.h"
#include "VehicleDescSh.h"

namespace NGTech
{
	using namespace physx;

	struct VehicleUtils
	{
		enum
		{
			DRIVABLE_SURFACE = 0xffff0000,
			UNDRIVABLE_SURFACE = 0x0000ffff
		};

		//Drivable surface types.
		enum
		{
			SURFACE_TYPE_TARMAC,
			MAX_NUM_SURFACE_TYPES
		};

		//Tire types.
		enum
		{
			TIRE_TYPE_NORMAL = 0,
			TIRE_TYPE_WORN,
			MAX_NUM_TIRE_TYPES
		};

		static PxVehicleDrive4W* CreateVehicle4W(const VehicleDescSh& vehDesc, PxPhysics* physics, PxCooking* cooking);

		static PxVehicleDriveTank* CreateVehicleTank(const VehicleDescSh& vehDesc, PxPhysics* physics, PxCooking* cooking);

		static PxVehicleNoDrive* CreateVehicleNoDrive(const VehicleDescSh& vehDesc, PxPhysics* physics, PxCooking* cooking);

		static physx::PxConvexMesh* CreateWheelMeshUtil(const TimeDelta width, const TimeDelta radius, physx::PxPhysics* physics, physx::PxCooking* cooking);

		static PxRigidDynamic* CreateVehicleActorUtil
		(const PxVehicleChassisData& chassisData,
			PxMaterial** wheelMaterials, PxConvexMesh** wheelConvexMeshes, const PxU32 numWheels,
			PxMaterial** chassisMaterials, PxConvexMesh** chassisConvexMeshes, const PxU32 numChassisMeshes,
			PxPhysics* physics);

		static PxConvexMesh* CreateChassisMeshUtil(const Vec3 dims, PxPhysics* physics, PxCooking* cooking);

		//  !@ 
		// * wheelsSimData - can't be null
		// * driveSimData - can be null
		static void CustomizeVehicleToLengthScaleUtil(const TimeDelta lengthScale, PxRigidDynamic* rigidDynamic, PxVehicleWheelsSimData* wheelsSimData, PxVehicleDriveSimData* driveSimData);

		//  !@ 
		// * wheelsSimData - can't be null
		// * driveSimData - can be null
		static void CustomizeVehicleToLengthScaleUtil(const TimeDelta lengthScale, PxRigidDynamic* rigidDynamic, PxVehicleWheelsSimData* wheelsSimData, PxVehicleDriveSimData4W* driveSimData);

		static PxVehicleDrivableSurfaceToTireFrictionPairs* CreateFrictionPairsUtil(const PxMaterial* defaultMaterial);

		static void SetupDrivableSurfaceUtil(PxFilterData& filterData);
		static void SetupNonDrivableSurfaceUtil(PxFilterData& filterData);
	};
}

#endif //VEHICLE_COMMON_H