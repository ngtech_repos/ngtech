#pragma once

#include "../PhysXForwardDecl.h"

namespace NGTech
{
	// ��������� ������� ��������� �������
	struct ENGINE_API VehicleDescSh final
	{
		TimeDelta chassisMass = Math::ONEDELTA;
		TimeDelta wheelMass = Math::ONEDELTA;
		TimeDelta wheelWidth = Math::ONEDELTA;
		TimeDelta wheelRadius = Math::ONEDELTA;
		TimeDelta wheelMOI = Math::ONEDELTA;
		TimeDelta numWheels = Math::ONEDELTA;

		Vec3 chassisDims = Vec3::ONE;
		Vec3 chassisMOI = Vec3::ONE;
		Vec3 chassisCMOffset = Vec3::ONE;

		// This tmp vars will got pointers from PhysXMaterialSh - not clean memory here
		PhysXMaterialSh* chassisMaterialTmp = nullptr;
		PhysXMaterialSh* wheelMaterialTmp = nullptr;

		VehicleDescSh() {}
		~VehicleDescSh() { CleanResources(); }

		VehicleDescSh(const VehicleDescSh&_desc) {
			_Swap(_desc);
		}

		VehicleDescSh& operator=(const VehicleDescSh&_desc) {
			_Swap(_desc);
			return *this;
		}

		bool operator==(const VehicleDescSh&_desc)
		{
			return (chassisMass == _desc.chassisMass)
				&& (wheelMass == _desc.wheelMass)
				&& (wheelWidth == _desc.wheelWidth)
				&& (wheelRadius == _desc.wheelRadius)
				&& (wheelMOI == _desc.wheelMOI)
				&& (numWheels == _desc.numWheels)

				&& (chassisDims == _desc.chassisDims)
				&& (chassisMOI == _desc.chassisMOI)
				&& (chassisCMOffset == _desc.chassisCMOffset)

				&& (chassisMaterialTmp == _desc.chassisMaterialTmp)
				&& (wheelMaterialTmp == _desc.wheelMaterialTmp);
		}

		void CleanResources();

	private:
		void _Swap(const VehicleDescSh&_desc) {
			if (this != &_desc)
			{
				chassisMass = _desc.chassisMass;
				wheelMass = _desc.wheelMass;
				wheelWidth = _desc.wheelWidth;
				wheelRadius = _desc.wheelRadius;
				wheelMOI = _desc.wheelMOI;
				numWheels = _desc.numWheels;

				chassisDims = _desc.chassisDims;
				chassisMOI = _desc.chassisMOI;
				chassisCMOffset = _desc.chassisCMOffset;

				chassisMaterialTmp = _desc.chassisMaterialTmp;
				wheelMaterialTmp = _desc.wheelMaterialTmp;
			}
		}
	};
}