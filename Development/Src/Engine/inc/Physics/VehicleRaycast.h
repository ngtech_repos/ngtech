/*
 * Copyright (c) 2008-2015, NVIDIA CORPORATION.  All rights reserved.
 *
 * NVIDIA CORPORATION and its licensors retain all intellectual property
 * and proprietary rights in and to this software, related documentation
 * and any modifications thereto.  Any use, reproduction, disclosure or
 * distribution of this software and related documentation without an express
 * license agreement from NVIDIA CORPORATION is strictly prohibited.
 */
 // Copyright (c) 2004-2008 AGEIA Technologies, Inc. All rights reserved.
 // Copyright (c) 2001-2004 NovodeX AG. All rights reserved.

#ifndef VEHICLE_RAYCAST_H
#define VEHICLE_RAYCAST_H

#include "PxPhysicsAPI.h"

using namespace physx;

namespace NGTech
{
	PxQueryHitType::Enum WheelRaycastPreFilter
	(PxFilterData filterData0, PxFilterData filterData1,
		const void* constantBlock, PxU32 constantBlockSize,
		PxHitFlags& queryFlags);

	//Data structure for quick setup of scene queries for suspension raycasts.
	class VehicleSceneQueryData
	{
	public:
		VehicleSceneQueryData();
		~VehicleSceneQueryData();

		//Allocate scene query data for up to maxNumVehicles and up to maxNumWheelsPerVehicle with numVehiclesInBatch per batch query.
		static VehicleSceneQueryData* Allocate(const PxU32 maxNumVehicles, const PxU32 maxNumWheelsPerVehicle, const PxU32 numVehiclesInBatch, PxAllocatorCallback& allocator);

		//Free allocated buffers.
		void Free(PxAllocatorCallback& allocator);

		//Create a PxBatchQuery instance that will be used for a single specified batch.
		static PxBatchQuery* SetUpBatchedSceneQuery(const PxU32 batchId, const VehicleSceneQueryData& vehicleSceneQueryData, PxScene* scene);

		//Return an array of scene query results for a single specified batch.
		PxRaycastQueryResult* GetRaycastQueryResultBuffer(const PxU32 batchId);

		//Get the number of scene query results that have been allocated for a single batch.
		PxU32 GetRaycastQueryResultBufferSize() const;

	private:

		//Number of raycasts per batch
		PxU32 mNumRaycastsPerBatch;

		//One result for each wheel.
		PxRaycastQueryResult* mSqResults = nullptr;

		//One hit for each wheel.
		PxRaycastHit* mSqHitBuffer = nullptr;

		//Filter shader used to filter drivable and non-drivable surfaces
		PxBatchQueryPreFilterShader mPreFilterShader;
	};
}
#endif //VEHICLE_RAYCAST_H