#pragma once

#include "PhysXForwardDecl.h"

namespace NGTech
{
	class VehicleSceneQueryData;
	struct PhysXSceneData;
	/**
	*/
	class VehicleScene
	{
		VehicleSceneQueryData*								mVehicleSceneQueryData = nullptr;
		physx::PxBatchQuery*								mBatchQuery = nullptr;
		physx::PxVehicleDrivableSurfaceToTireFrictionPairs* mFrictionPairs = nullptr;
		PhysXSceneData*										mSceneData = nullptr;
		bool												mInited = false;

		void _Init();
		void _Release();

	public:
		VehicleScene() { mInited = false; }
		VehicleScene(PhysXSceneData*_scene) :mSceneData(_scene) { _Init(); }
		VehicleScene(const VehicleScene&_vs)
		{
			this->mVehicleSceneQueryData = _vs.mVehicleSceneQueryData;
			this->mBatchQuery = _vs.mBatchQuery;
			this->mFrictionPairs = _vs.mFrictionPairs;
			this->mSceneData = _vs.mSceneData;
			this->mInited = _vs.mInited;
		}

		VehicleScene &operator=(const VehicleScene &_other) {
			if (this != &_other) {
				this->mVehicleSceneQueryData = _other.mVehicleSceneQueryData;
				this->mBatchQuery = _other.mBatchQuery;
				this->mFrictionPairs = _other.mFrictionPairs;
				this->mSceneData = _other.mSceneData;
				this->mInited = _other.mInited;
			}

			return *this;
		}

		~VehicleScene() { _Release(); }
	};
}