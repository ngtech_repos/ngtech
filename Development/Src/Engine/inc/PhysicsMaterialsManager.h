/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include  <set>
//***************************************************************************
#include "PhysXForwardDecl.h"
//***************************************************************************
#include "Physics/PhysScene.h"
#include "PhysXSceneData.h"
//***************************************************************************

namespace physx {
	class PxMaterial;
}

namespace NGTech {
	/**
	*/
	class	Engine;
	/**
	*/
	class	PhysXCharacterController;
	class	VehicleSceneQueryData;
	struct	CharacterControllerDesc;
	struct	PhysXMaterialSh;

	// !@ PhysicsMaterials manager and factory
	struct ENGINE_API PhysicsMaterialsManager
	{
		// !@ Setup default material...
		void Initialise(physx::PxPhysics* mPhysics);

		// !@ Delete default physics material
		void DeInitialise();

		// !@ Allocate Physics material (with search in cache)
		PhysXMaterialSh* AllocMaterial(physx::PxPhysics* mPhysics, TimeDelta staticFriction, TimeDelta dynamicFriction, TimeDelta restitution,
			const String& materialName, bool vehicleDrivableSurface);

		// !@ Allocate Physics material (with search in cache)
		PhysXMaterialSh* AllocMaterial(TimeDelta staticFriction, TimeDelta dynamicFriction, TimeDelta restitution,
			const String& materialName, bool vehicleDrivableSurface);

		void FreeMaterial(PhysXMaterialSh*material);

		void _IncrementVehicleDrivableMaterialsVersion();
		void _DecrementVehicleDrivableMaterialsVersion();
		PhysXMaterialSh* GetDefaultMaterial() const;

		// ��������� �������� �� ����������, ���� �������� �� ��������, �� ������ �����������
		static PhysXMaterialSh* CheckMatOnValid(PhysXMaterialSh*_ptr);
		static physx::PxMaterial* CheckPxMatOnValid(PhysXMaterialSh*_ptr);

	private:

		PhysXMaterialSh*	mMaterial = nullptr;
		typedef std::map<String, PhysXMaterialSh*> MaterialMap;

		MaterialMap materials;
		std::set<PhysXMaterialSh*> vehicleDrivableMaterials;
		unsigned int vehicleDrivableMaterialsVersionCounter = 1;

		friend struct PhysXMaterialSh;
	};

	ENGINE_API extern PhysicsMaterialsManager phMatManager;
}