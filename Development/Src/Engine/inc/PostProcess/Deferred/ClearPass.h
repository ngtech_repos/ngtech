#pragma once

namespace NGTech
{
	struct ClearPass {
		/*!@ Clean current RT*/
		void Do(const RenderPipeline&_rp, I_RenderLowLevel* _render) {
			ASSERT(_render, "Not exist render");
			if (_render == nullptr)
				return;

			_render->ClearDepthStencilColor();
		}
	};

	struct DepthPass {
		/*!@ Clean current RT*/
		void Do(const RenderPipeline&_rp, I_RenderLowLevel* _render) {
			ASSERT(_render, "Not exist render");
			if (_render == nullptr)
				return;

			BROFILER_CATEGORY("DepthPass", Brofiler::Color::LightGreen);
			PROFILE;

			//!@todo implement this
		}
	};
}