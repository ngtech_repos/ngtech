#pragma once

namespace NGTech
{
	struct DeferredShadingGeometryPass
	{
		void Do(GBuffer*gbuffer, const RenderPipeline&_rp, I_RenderLowLevel* _render)
		{
			ASSERT(_render, "Invalid pointer");
			ASSERT(gbuffer, "Invalid pointer");
			// if returned false
			ASSERT(gbuffer->IsInited(), "GBuffer not Inited");

			_render->EnableDepth();
			_render->DisableStencilTest();

			gbuffer->Bind();
			gbuffer->BindDepthTextureForWriting();
			{
				gbuffer->ClearDepthAndStencil();

				// enable depth buffer writes
				_render->DepthMask(true);

				// fill the z-buffer, or whatever
				_render->DepthFunc(CompareType::LESS);
				_rp._DeferredPass();
				_render->DepthFunc(CompareType::EQUAL);
			}
			gbuffer->UnBind();

			_render->DisableDepth();
			_render->DisableStencilTest();
		}
	};
}