#pragma once

namespace NGTech
{
	void LightEnvProbeShaderActions(Light* _light, uint32_t _diffIrad, uint32_t _specIrad, uint32_t _f0ScaleIrad, I_Shader* _shader);

	struct DeferredShadingLightPass
	{
		Mat4 texmat;
		size_t SavedCount = 0;
		Ref<I_Shader> m_LightPassShader = nullptr;

		//!@todo CHECK: Implement later
		static const auto MAGIC_CONST_FOR_ENVPROBE_SHADER = 2;

		enum EnvProbeText
		{
			Diffuse_Rad = 4,
			Specular_Rad = 5,
			F0_Scale_Bias = 6
		};

		DeferredShadingLightPass() : m_LightPassShader(nullptr)
		{
		}

		~DeferredShadingLightPass() {
			SAFE_RELEASE(m_LightPassShader);
		}

		void Init(I_RenderLowLevel* _render)
		{
			ASSERT(_render, "Invalid pointer on _render");

			if (!m_LightPassShader)
				m_LightPassShader = _render->ShaderCreate("deferredlp.glsl", StringHelper::EMPTY_STRING, false);

			ASSERT(m_LightPassShader, "Failed init m_LightPassShader");
		}

		/*!@ ������������ ��� ������ �� ������� �����*/
		template <class T>
		void DoFromAnother(GBuffer*_gb, const RenderPipeline&_rp, I_RenderLowLevel* _render, SceneManager&_sceneMgr, I_Drawable* _screenquad, T* _fxaa) {
			ASSERT(_fxaa, "Invalid pointer on _fxaa");

			_fxaa->PrePassStart(_rp, _render);
			Do(_gb, _rp, _render, _sceneMgr, _screenquad);
			_fxaa->Do(_rp, _render, _screenquad);
		}

		void Do(GBuffer*_gb, const RenderPipeline&_rp, I_RenderLowLevel* _render, SceneManager&_sceneMgr, I_Drawable* _screenquad) {
			ASSERT(_render, "Invalid pointer on _render");
			ASSERT(_gb, "Invalid pointer on _gb");

			Init(_render);

			if ((_render == nullptr) || (_sceneMgr.MeshesCount() == 0) || (_sceneMgr.LightsCount() == 0))
				return;

			ASSERT(m_LightPassShader, "Invalid pointer");

			ASSERT(_screenquad, "Invalid pointer");

			// if returned false
			ASSERT(_gb->IsInited(), "GBuffer not Inited");

			_gb->BindForReading();

			m_LightPassShader->SendInt("gRT0", GBuffer::GB_RT0);
			m_LightPassShader->SendInt("gRT1", GBuffer::GB_RT1);
			m_LightPassShader->SendInt("gRT2", GBuffer::GB_RT2);
			m_LightPassShader->SendInt("gRT3", GBuffer::GB_RT3);
			m_LightPassShader->SendInt("gRT4", GBuffer::GB_RT4);

			m_LightPassShader->SendInt("gDepth", GBuffer::GB_RT_DEPTH);
			m_LightPassShader->SendMat4("matTexture", texmat);

			auto lightsCount = _sceneMgr.LightsCount();

			if (lightsCount != SavedCount)
			{
				SavedCount = lightsCount;
				m_LightPassShader->Recompile("#define DNR_LIGHTS " + StringHelper::to_string(SavedCount));
			}
			SavedCount = lightsCount;

			_rp.UpdateUniforms(m_LightPassShader);

			// Also send light relevant uniforms
			unsigned int i = 0;
			for (auto light : _sceneMgr.GetAllLights())
			{
				ASSERT(light, "Invalid light");

				if (!light)
					continue;

				/**/
				/*matrixes.matShadowMap = light->GetShadowMap();*/

				/*Another*/
				m_LightPassShader->SendVec3("lights[" + StringHelper::to_string(i) + "].u_light_pos", light->GetPosition());
				m_LightPassShader->SendVec3("lights[" + StringHelper::to_string(i) + "].u_light_dir", light->GetDirection());

				m_LightPassShader->SendVec3("lights[" + StringHelper::to_string(i) + "].color", light->GetColor());

				m_LightPassShader->SendFloat("lights[" + StringHelper::to_string(i) + "].attIRadius", light->GetIRadius());
				/*Light type*/
				m_LightPassShader->SendInt("lights[" + StringHelper::to_string(i) + "].type", (int)light->GetType());
				/*SpotLight*/
				m_LightPassShader->SendVec2("lights[" + StringHelper::to_string(i) + "].spotangles", light->GetSpotAngles());
				m_LightPassShader->SendFloat("lights[" + StringHelper::to_string(i) + "].angleScale", light->GetSpotAngleScale());
				m_LightPassShader->SendFloat("lights[" + StringHelper::to_string(i) + "].angleOffset", light->GetSpotAngleOffset());

				/*PBR*/
				switch (light->GetType())
				{
				case LightBufferData::LIGHT_SPOT:
				case LightBufferData::LIGHT_OMNI:
				case LightBufferData::LIGHT_AREA:
				case LightBufferData::LIGHT_DIRECT:
					m_LightPassShader->SendFloat("lights[" + StringHelper::to_string(i) + "].lumIntensity", light->GetLuminuousIntensity());
					m_LightPassShader->SendFloat("lights[" + StringHelper::to_string(i) + "].luminance", light->GetLuminance());
					m_LightPassShader->SendVec3("lights[" + StringHelper::to_string(i) + "].lightRight", light->GetDirectionRight());
					m_LightPassShader->SendVec3("lights[" + StringHelper::to_string(i) + "].lightUp", light->GetDirectionUp());
					break;

				case LightBufferData::LIGHT_ENVPROBE:

					LightEnvProbeShaderActions(light, EnvProbeText::Diffuse_Rad + MAGIC_CONST_FOR_ENVPROBE_SHADER,
						EnvProbeText::Specular_Rad + MAGIC_CONST_FOR_ENVPROBE_SHADER,
						EnvProbeText::F0_Scale_Bias + MAGIC_CONST_FOR_ENVPROBE_SHADER, m_LightPassShader);

					_DrawLightPassShader(_render, _screenquad, _rp);
					break;

				case LightBufferData::LIGHT_ERROR:
				case LightBufferData::LIGHT_TYPE_COUNT:
					Warning("Invalid type of light. Passed LIGHT_ERROR or LIGHT_TYPE_COUNT");
					break;

				default:
					Warning("Invalid type of light");
					break;
				}

				i++;
			}

			_DrawLightPassShader(_render, _screenquad, _rp);
		}

		void _DrawLightPassShader(I_RenderLowLevel*_render, I_Drawable*_screenquad, const RenderPipeline&_rp)
		{
			// ��������
			_rp.UpdateUniforms(m_LightPassShader);
			//���������, �� ��������� depth test �� ��������������
			_render->DepthMask(false);
			{
				m_LightPassShader->Enable();
				{
					_screenquad->Draw();
				}
				m_LightPassShader->Disable();
			}
			_render->DepthMask(true);
		}
	};
}