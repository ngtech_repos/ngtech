/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include "Gbuffer.h"

namespace NGTech
{
	struct ClearPass;
	struct DeferredShadingGeometryPass;
	struct DeferredShadingLightPass;
	struct FXAA;

	struct DeferredShading {
		GBuffer*						m_GBuffer = nullptr;

		ClearPass*						m_ClearPass = nullptr;
		/*!@ Geometry pass of DS*/
		DeferredShadingGeometryPass*	m_DSGeometry = nullptr;
		/*!@ Light pass of DS*/
		DeferredShadingLightPass*		m_DSLightPass = nullptr;
		/*!@ FXAA*/
		FXAA*							m_FXAA = nullptr;

		Ref<I_Drawable>	m_Screenquad = nullptr;

		~DeferredShading();

		void Init(const Vec2& _windowSize, I_RenderLowLevel* _render);
		void RunPipeline(const RenderPipeline&_rp, I_RenderLowLevel* _render, SceneManager& _sceneMgr, bool _enabledWireframe);
		void ReSize(const Vec2& _windowSize, I_RenderLowLevel* _render);
	};
}