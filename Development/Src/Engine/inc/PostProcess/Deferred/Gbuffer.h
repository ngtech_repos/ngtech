/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

namespace NGTech
{
	struct GBuffer : public Noncopyable
	{
		enum GBStruct
		{
			GB_RT0 = 0,
			GB_RT1,
			GB_RT2,
			GB_RT3,
			GB_RT4,
			GB_RT_DEPTH,

			GB_NUM
		};

		GBuffer(const Vec2& _size);

		void ReInit(const Vec2&);
		void BindForReading() const;

		void Bind(bool _writeToColor = true) const;
		void UnBind() const;

		void ClearAll() const;
		void ClearDepthAndStencil() const;
		void ClearColorOnly() const;
		void ClearDepthOnly() const;

		void BindAndClear() const;

		void BindAlbedoTexture() const;
		void BindPositionTexture() const;
		void BindSpecularTexture() const;

		/**/
		void BindDepthTextureForWriting() const;
		void BindDepthTextureForReading() const;

		void Resolve(I_RenderTarget*_to, unsigned int _bitmask) const;

		ENGINE_INLINE I_RenderTarget* GetRT() const { return framebuffer; }

		virtual ~GBuffer() {
			_Clean();
		}

		bool IsInited() { return mInited; }
	private:

		GBuffer() { ReInit(size); }
		GBuffer(const GBuffer&_g) { this->ReInit(_g.size); }

		bool _Init(const Vec2&);
		void _Clean() {
			SAFE_RELEASE(framebuffer);
		}

		void _UnBindTexturesOnly() const;
	private:
		friend class DeferredShadingPipeline;
		Ref<I_RenderTarget> framebuffer = nullptr;
		Vec2 size = Vec2(1980, 1080);
		bool mInited = false;
	};
}