#pragma once

namespace NGTech
{
	/*
	!@ FXAA
	Called LAST
	*/
	struct FXAA
	{
		~FXAA();

		void PrePassStart(const RenderPipeline&_rp, I_RenderLowLevel* _render);
		void Do(const RenderPipeline&_rp, I_RenderLowLevel* _render, I_Drawable* m_Screenquad);
		void ReSize(I_RenderLowLevel* _render, const Vec2&_screenSize);

	private:
		void Init(const RenderPipeline&_rp, I_RenderLowLevel* _render, const Vec2&_screenSize);
		void ResultDraw(const RenderPipeline&_rp, I_RenderLowLevel* _render, I_Drawable* m_Screenquad);
		void PrePassEnd();

	private:
		I_Shader*			m_FXAAShader = nullptr;
		I_RenderTarget*		m_FXAA_RT = nullptr;
		bool				m_Inited = false;
	};
}