/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//**************************************
#include "Object.h"
#include "Camera.h"
#include <vector>
//**************************************

namespace NGTech
{
	class Light;
	class LightPoint;
	class LightDirect;
	class LightSpot;
	class BaseEntity;
	class Engine;
	class BaseVisualObjectEntity;

	struct DeferredShading;
	//---------------------------------------------------------------------------
	//Desc: Engine Render pipeline. Created once
	//---------------------------------------------------------------------------
	class RenderPipeline {
	public:
		explicit RenderPipeline(Engine*_engine);

		~RenderPipeline();

		void Initialise();
		/**
		main update function
		*/
		void Update(bool _paused, TimeDelta _delta);

		ENGINE_INLINE void EnableStats() { needStats = true; }

		bool			isUseAA() const;
		/**
		Revert CVARManager::AA_MODE value
		*/
		int	GetAAMode() const;

		/**
		*/
		ENGINE_INLINE const Vec2& GetScreenSize() const {
			return screensize;
		}

		void ReSize(const Vec2&_size);

		/*Updates uniforms*/
		void UpdateUniforms(I_Shader* _shd) const;

		/**
		*/
		void _RenderMeshesOnLayers(const char*_passname, bool blended, bool _transparent) const;

		/*
		!@ Drawing geometry on scene using pass: 'DeferredPass' from material
		*/
		void _DeferredPass() const;

		/*Forward pass*/
		void _ForwardPass() const;

		void RenderWithSpecificShader(I_Shader * _shader) const;

		/**
		Render Mesh with Occlusion Query
		*/
		bool _RenderObjectWithOcclusionQuery(BaseVisualObjectEntity* _obj, size_t _index) const;
		/**
		Render Mesh without Occlusion Query
		*/
		bool _RenderObjectFast(BaseVisualObjectEntity* _obj, size_t _index) const;

	private:
		void _InitMembers(Engine * _engine);
		void _UpdateActions() const;
		void _RenderUI() const;
		/**
		Function for high-level of rendering
		*/
		void _RenderComplex();
	private:
		Vec2 screensize = Vec2(1920, 1080);

		std::weak_ptr<CVARManager>	m_WeakCvars;
		I_RenderLowLevel* render = nullptr;
		Engine* engine = nullptr;

		DeferredShading* m_Pipeline = nullptr;

		// ��� ����� �������� ��� ������, ��� ���� �������� ������� ��������� ������
		TimeDelta deltaTime = Math::ZERODELTA;

		bool needStats = false;
		bool paused = false;
	private:

		friend class Light;
		friend class LightPoint;
		friend class LightSpot;
		friend class LightDirect;

		friend class Material;
	};
}