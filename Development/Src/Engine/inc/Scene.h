/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//**************************************
#include <vector>
#include <memory>
//**************************************
#include "Camera.h"
#include "LoadingScreen.h"
//**************************************
#include "SceneData.h"
//************************************

namespace NGTech
{
	/**/
	class Light;
	class LightPoint;
	class LightDirect;
	class LightSpot;

	class Camera;
	class ObjectMesh;
	class AnimatedMesh;

	ENGINE_INLINE void Shader_SetAmbient(const Vec3 &_color) { matrixes.matSceneAmbient = _color; }
	ENGINE_INLINE Vec3 Shader_GetAmbient() { return matrixes.matSceneAmbient; }

	struct ENGINE_API Scene
	{
		ENGINE_INLINE SceneData GetSceneDataCopy() {
			return m_SceneData;
		}
		ENGINE_INLINE SceneData& GetSceneData() {
			return m_SceneData;
		}

		ENGINE_INLINE void SetSceneData(const SceneData&_sceneData) {
			m_SceneData = _sceneData;
		}

		/* Constructor Functions */
		Scene(const String& name, unsigned int order) {
			m_SceneData.SetSceneName(name);
			m_SceneData.SetSceneOrder(order);
			m_SceneData.SetActive(false);
			/*
			���������: ������ ����������
			m_SceneData.m_State = SceneData::SceneState::Unintialised;
			m_SceneData.m_Active = false;
			m_SceneData.m_Loading = false;
			m_SceneData.m_SceneLoaded = false;
			m_SceneData.m_FontsLoaded = false;*/
		}

		Scene() { Clear(); }

		ENGINE_INLINE void Clear() {
			m_SceneData.Clear();
		}

		ENGINE_INLINE ~Scene() {
			Clear();
		}

		ENGINE_INLINE Scene(const Scene& _scene) {
			this->m_SceneData = _scene.m_SceneData;
		}

		ENGINE_INLINE Scene& operator=(const Scene&_other) {
			if (this != &_other)
			{
				this->m_SceneData = _other.m_SceneData;
			}

			return *this;
		}

		ENGINE_INLINE bool IsLoading() {
			return m_SceneData.GetLoadingStatus();
		}

		ENGINE_INLINE unsigned int GetOrder(void) {
			return m_SceneData.GetSceneOrder();
		}

		ENGINE_INLINE unsigned int GetGammaCorrectionMode() {
			return m_SceneData.GetGammaCorrMode();
		}

		void LoadSceneFinishedEventApply();

		/**
		Set and Get scene ambient color
		*/
		ENGINE_INLINE void SetSceneAmbient(const Color&_amb) { m_SceneData.SetSceneAmbient(_amb); }
		ENGINE_INLINE Color GetSceneAmbient() const { return m_SceneData.GetSceneAmbient(); }

		ENGINE_INLINE String GetSceneName() { return m_SceneData.GetSceneName(); }
		ENGINE_INLINE void SetSceneName(const String& _name) { m_SceneData.SetSceneName(_name); }

		/**
		Note: must be copy, used in multi threading
		*/
		Camera::BufferData GetCurrentCameraData();

		/**
		*/
		Camera* GetCurrentCamera();

		ENGINE_INLINE void AddCameraToList(Camera* _cam) { m_SceneData.GetCamerasListForChange().push_back(_cam); }

		void SetCameraAndDestroyOlder(Camera* _camera);

		/*! @brief Updates on loading stage. */
		void _DrawLoadingProgress(TimeDelta _delta, LoadingScreen* _screen);
		/*! @brief Updates game. Actors, dynamic actors and etc*/
		void UpdateGame(TimeDelta _delta);

		bool Create();
		void SetActive(bool active);
		void LoadScene(void);

		/* Get Functions*/
		ENGINE_INLINE Camera GetCamera(void) { return *m_SceneData.GetCurrentCamera(); }

		ENGINE_INLINE bool IsActive(void) { return m_SceneData.GetActive(); }

		ENGINE_INLINE String GetName(void) { return m_SceneData.GetSceneName(); }

		ENGINE_INLINE SceneData::SceneState GetState(void) { return m_SceneData.GetState(); }

		SceneData m_SceneData;
	private:
		void _CreateDefCameraAndSet();
	private:
		PlatformManager * m_PlatformManagerTmp = nullptr;
	};

	//---------------------------------------------------------------------------
	//Desc: class of the scene. Created one time
	//---------------------------------------------------------------------------
	class ENGINE_API SceneManager
	{
	public:
		Scene m_ActiveScene;

		std::vector<std::shared_ptr<Scene>> m_scenes;

		std::shared_ptr<Scene> m_currentScene;
		std::shared_ptr<Scene> m_preloadScene;
		std::shared_ptr<Scene> m_loadScene;

		explicit SceneManager(Engine*);
		virtual ~SceneManager();

		void Initialise();
		void Clear();

		ENGINE_INLINE Scene GetSceneCopy() { return m_ActiveScene; }
		ENGINE_INLINE Scene& GetSceneForChange() { return m_ActiveScene; }

		/**
		*/
		ENGINE_INLINE size_t MeshesCount() { return m_ActiveScene.m_SceneData.GetMeshesListForChange().size(); }
		ENGINE_INLINE size_t SkeletalCount() { return m_ActiveScene.m_SceneData.GetAnimMeshesListForChange().size(); }
		ENGINE_INLINE size_t LightsCount() { return m_ActiveScene.m_SceneData.GetLightListForChange().size(); }

		/**
		*/
		void AddLight(Light *_ptr);
		void AddMesh(ObjectMesh *_ptr);
		void AddSkeletal(AnimatedMesh *_ptr);
		/// ��������� ����������� ������ � ������ ���� ��������
		void AddObject(UpdatableObject*_ptr);
		void AddObject(BaseVisualObjectEntity * _ptr);
		void AddObject(BaseEntity* _ptr);

		void DeleteLightFromList(Light *light);
		void DeleteObjectMeshFromList(ObjectMesh *object);
		void DeleteSkeletalFromList(AnimatedMesh *ptr);
		void DeleteUpdatableObjectFromList(UpdatableObject*_ptr);
		void DeleteObject(BaseVisualObjectEntity * _ptr);
		void DeleteObject(BaseEntity*_ptr);

		/**
		*/
		ENGINE_INLINE ObjectMesh* GetMeshById(size_t i) { if (i <= m_ActiveScene.m_SceneData.GetMeshesListForChange().capacity()) return m_ActiveScene.m_SceneData.GetMeshesListForChange()[i]; return nullptr; }

		ENGINE_INLINE Light* GetLightById(size_t _id) { if (_id > m_ActiveScene.m_SceneData.GetLightListForChange().size()) return nullptr; return m_ActiveScene.m_SceneData.GetLightListForChange()[_id]; }
		ENGINE_INLINE std::vector<Light*>& GetAllLights() { return m_ActiveScene.m_SceneData.GetLightListForChange(); }

		/**
		*/
		ENGINE_INLINE bool EnabledEarlyZ() { return m_ActiveScene.m_SceneData.GetEnabledEarlyZ(); }
		ENGINE_INLINE void SetEnabledEarlyZ(bool _mEnabledEarlyZ) { m_ActiveScene.m_SceneData.SetEnableEarlyZ(_mEnabledEarlyZ); }

		/**
		*/
		void SetGammaCorrectionMode(unsigned int _mode);
		unsigned int GetGammaCorrectionMode();

		/**
		*/
		void NewScene();
		void LoadScene();

		/**
		*/
		void SetLoadingScreen(const String&_name);
		void SetLoadingScreen(LoadingScreen* _l);
		void DrawLoading(const EngineState& _state, TimeDelta _delta);
		void Update(TimeDelta _delta);

		String GetLoadingScreenName();

		ENGINE_INLINE String GetCurrentSceneName() { return m_ActiveScene.GetSceneName(); }
		ENGINE_INLINE void SetCurrentSceneName(const String& _name) { m_ActiveScene.SetSceneName(_name); }
		void SetActiveScene(const Scene& newScene);

		bool IsInited() const { return m_Inited; }
		/*****
		Editor Part
		*****/
		ENGINE_INLINE std::vector<EditorVar>& GetEditorVars() { return m_EditorVars; }
	private:
		/*****
		Editor Part
		*****/
		void _InitEditorVars();

		bool _CameraCheckValid();
		void _AddEntityToStats();
		void _AddLightToStats();
	private:
		/// Editable parameters displayed inside NGeD
		std::vector<EditorVar> m_EditorVars;
		Engine * engine = nullptr;
		std::weak_ptr<Cache> m_CacheWeak;
		std::shared_ptr<ScriptUpdateJob> m_SceneUpdateJob = nullptr;
		LoadingScreen * lcurLScreen = nullptr;
		bool m_Inited = false;
	private:
		friend class RenderPipeline;
		friend class DeferredShadingPipeline;
		friend class BaseEntity;
		friend class ObjectMesh;
		friend class AnimatedMesh;
		friend class Light;
	};
}