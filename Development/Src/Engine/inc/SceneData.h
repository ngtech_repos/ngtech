#pragma once

#include "../../Common/SceneMatrixes.h"

//************************************
namespace NGTech
{
	/**/
	class BaseVisualObjectEntity;
	/**/
	class Light;
	class LightPoint;
	class LightDirect;
	class LightSpot;
	/**/
	class Camera;
	class CameraFixed;
	class ObjectMesh;
	class AnimatedMesh;

	/**/
	struct ENGINE_API SceneData
	{
		enum SceneState
		{
			Unintialised = 0,
			Created,
			Loading,
			Game,
			LoadNextScene,
			LoadPreloadScene,
			Destroyed
		} m_State = SceneData::SceneState::Unintialised;

		SceneData();

		~SceneData();

		SceneData(const SceneData& _scene) {
			_Swap(_scene);
		}

		ENGINE_INLINE SceneData& operator=(const SceneData&_other) {
			_Swap(_other);
			return *this;
		}

		ENGINE_INLINE bool operator==(const SceneData&_other)
		{
			return (this->meshes == _other.meshes)
				&& (this->skeletals == _other.skeletals)
				&& (this->lights == _other.lights)
				&& (this->cameras == _other.cameras)
				&& (this->m_UpdatableObjects == _other.m_UpdatableObjects)
				&& (this->m_VisualObjects == _other.m_VisualObjects)
				&& (this->m_BaseEntity == _other.m_BaseEntity)

				&& (this->m_EnabledEarlyZ == _other.m_EnabledEarlyZ)
				&& (this->gammaCorrMode == _other.gammaCorrMode)

				&& (this->m_SceneLoaded == _other.m_SceneLoaded)
				&& (this->m_FontsLoaded == _other.m_FontsLoaded)
				&& (this->m_Loading == _other.m_Loading)
				&& (this->m_Active == _other.m_Active)

				&& (this->m_LoadingScreenName == _other.m_LoadingScreenName)
				&& (this->m_SceneName == _other.m_SceneName)
				&& (this->m_SceneGravity == _other.m_SceneGravity)
				&& (this->m_SceneAmbient == _other.m_SceneAmbient)

				&& (this->currentCamera == _other.currentCamera)
				&& (this->cameraDef == _other.cameraDef)

				&& (this->m_State == _other.m_State)
				&& (this->m_Order == _other.m_Order);
		}

		//!@ Cameras pointers will deleted in destructor
		void Clear();

		/*
		*/
		ENGINE_INLINE void SetEnableEarlyZ(bool _m_EnabledEarlyZ) { m_EnabledEarlyZ = _m_EnabledEarlyZ; }
		ENGINE_INLINE bool GetEnabledEarlyZ() const { return m_EnabledEarlyZ; }
		ENGINE_INLINE bool &GetEnabledEarlyZForChange() { return m_EnabledEarlyZ; }

		/*
		*/
		ENGINE_INLINE void SetSceneOrder(unsigned int _order) { m_Order = _order; }
		ENGINE_INLINE unsigned int GetSceneOrder() const { return m_Order; }

		/*
		*/
		ENGINE_INLINE void SetSceneName(const String& _name) { m_SceneName = _name; }
		ENGINE_INLINE String GetSceneName() const { return m_SceneName; }

		/*
		*/
		ENGINE_INLINE void SetActive(bool _active) { m_Active = _active; }
		ENGINE_INLINE bool GetActive() const { return m_Active; }

		/*
		*/
		ENGINE_INLINE void SetState(SceneData::SceneState _state) { m_State = _state; }
		ENGINE_INLINE SceneData::SceneState GetState() const { return m_State; }

		/*
		*/
		ENGINE_INLINE void SetLoadingStatus(bool _s) { m_Loading = _s; }
		ENGINE_INLINE bool GetLoadingStatus() const { return m_Loading; }

		/*
		*/
		ENGINE_INLINE void SetGammaCorrMode(unsigned int _gammaCorrMode) { gammaCorrMode = _gammaCorrMode; }
		ENGINE_INLINE unsigned int GetGammaCorrMode() const { return gammaCorrMode; }

		/*
		*/
		ENGINE_INLINE void SetSceneAmbient(const Color&_amb) { m_SceneAmbient = _amb; }
		ENGINE_INLINE Color GetSceneAmbient() const { return m_SceneAmbient; }

		/*
		*/
		ENGINE_INLINE Camera* GetCurrentCamera() const { return currentCamera; }
		void SetCurrentCamera(Camera* _cam);

		/*
		*/
		ENGINE_INLINE Camera* GetDefCamera() const { return cameraDef; }
		void SetCameraDef(Camera*_cam);

		/*
		*/
		ENGINE_INLINE void SetSceneLoaded(bool _l) { m_SceneLoaded = _l; }
		ENGINE_INLINE bool GetSceneLoaded() const { return m_SceneLoaded; }

		/*
		*/
		ENGINE_INLINE Vec3 GetSceneGravity() const { return m_SceneGravity; }
		ENGINE_INLINE void SetSceneGravity(const Vec3& _v) { m_SceneGravity = _v; }

		/*
		*/
		ENGINE_INLINE void SetLoadingScreenName(const String& _name) { m_LoadingScreenName = _name; }
		ENGINE_INLINE String GetLoadingScreenName() const { return m_LoadingScreenName; }

		ENGINE_INLINE String& GetLoadingScreenNameForChange() { return m_LoadingScreenName; }

		/*
		*/
		std::vector<ObjectMesh*>& GetMeshesListForChange() { return meshes; }
		std::vector<AnimatedMesh*>& GetAnimMeshesListForChange() { return skeletals; }
		std::vector<Light*>& GetLightListForChange() { return lights; }
		std::vector<Camera*>& GetCamerasListForChange() { return cameras; }
		std::vector<UpdatableObject*>& GetUpdatableObjectsForChange() { return m_UpdatableObjects; }
		std::vector<BaseVisualObjectEntity*>& GetObjectsForChange() { return m_VisualObjects; }

		std::vector<BaseEntity*>& GetBaseEntities() { return m_BaseEntity; }

	private:
		void _ResetCameras();

		ENGINE_INLINE void _Swap(const SceneData&_other)
		{
			if (this == &_other) return;

			this->meshes = _other.meshes;
			this->skeletals = _other.skeletals;
			this->lights = _other.lights;
			this->cameras = _other.cameras;
			this->m_UpdatableObjects = _other.m_UpdatableObjects;
			this->m_VisualObjects = _other.m_VisualObjects;
			this->m_BaseEntity = _other.m_BaseEntity;

			this->m_EnabledEarlyZ = _other.m_EnabledEarlyZ;
			this->gammaCorrMode = _other.gammaCorrMode;

			this->m_SceneLoaded = _other.m_SceneLoaded;
			this->m_FontsLoaded = _other.m_FontsLoaded;
			this->m_Loading = _other.m_Loading;
			this->m_Active = _other.m_Active;

			this->m_LoadingScreenName = _other.m_LoadingScreenName;
			this->m_SceneName = _other.m_SceneName;
			this->m_SceneGravity = _other.m_SceneGravity;
			this->m_SceneAmbient = _other.m_SceneAmbient;

			this->currentCamera = _other.currentCamera;
			this->cameraDef = _other.cameraDef;

			this->m_State = _other.m_State;
			this->m_Order = _other.m_Order;
		}

	private:
		std::vector<ObjectMesh*>					meshes;
		std::vector<AnimatedMesh*>					skeletals;
		std::vector<Light*>							lights;
		std::vector<Camera*>						cameras;
		std::vector<UpdatableObject*>				m_UpdatableObjects;
		std::vector<BaseVisualObjectEntity*>		m_VisualObjects;
		std::vector<BaseEntity*>					m_BaseEntity;

		bool m_EnabledEarlyZ = true;
		unsigned int gammaCorrMode = SceneMatrixes::GammaCorrectionMode::simpleGammaCorrection;

		bool m_SceneLoaded = false;
		bool m_FontsLoaded = false;
		bool m_Loading = false;
		bool m_Active = true;

		String m_LoadingScreenName = "Loading.dds";
		String m_SceneName = "EMPTY";

		Vec3 m_SceneGravity = CVARManager::m_DefaultGravity;

		Color m_SceneAmbient = Color::BLACK;

		//current state
		Camera* currentCamera = nullptr;
		[[deprecated]]
		Camera* cameraDef = nullptr;

		unsigned int m_Order = 1;
	};
}