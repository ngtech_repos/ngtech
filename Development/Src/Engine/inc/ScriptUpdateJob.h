/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

namespace NGTech
{
	/**
	*/
	class ScriptUpdateJob
	{
	public:
		enum class UPDATE_MODE
		{
			UPDATE = 0, // all updates will called in different thread
			UPDATE_INPUT,
			UPDATE_GUI,
			UPDATE_PHYSICS,
			UPDATE_RENDER,
			UPDATE_SCENEMANAGER,
			UPDATE_AI,
			LOADED, // called once TODO:CHECK ON IMPLEMENTATION
			LOADED_GUI, // called once TODO:CHECK ON IMPLEMENTATION
			UPDATE_MODE_NUM
		};

		struct State
		{
			UPDATE_MODE mode;
		};

		State m_State;

	public:
		ScriptUpdateJob(UPDATE_MODE _mode)
			:ScriptUpdateJob()
		{
			m_State.mode = _mode;
		}

		~ScriptUpdateJob() {}

		void Update(const ScriptManager&_sm);

		void Update();
	private:
		ScriptUpdateJob() {}
	};
}