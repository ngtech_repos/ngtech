/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

// Copy a file
#include <fstream>      // std::ifstream, std::ofstream

#include "rapidjson/include/rapidjson/writer.h"
#include "rapidjson/include/rapidjson/filewritestream.h"

namespace NGTech
{
	/**/
	using namespace rapidjson;
	using namespace std;
	/**/
	class SceneManager;
	class Camera;
	class ALSound;
	class ALSoundSource;
	class PhysXCharacterController;
	class Material;
	class Light;
	class BaseVisualObjectEntity;
	class PhysBody;
	class BaseEntity;
	class ObjectMesh;
	struct PhysXMaterialSh;
	struct MaterialPassSh;
	/**/

#ifdef DEBUG_SERIALIZER
#define DEBUG_SERIALIZER_TRACE; DebugM("[Debug Serializator] Called from %s, from file: %s, from line: %i", _function, __FILE__, __LINE__)
#else
#define DEBUG_SERIALIZER_TRACE
#endif

	class ENGINE_API Serializer
	{
	public:
		static const int UNUSED = -2;
	public:
		/**/
		Serializer();
		explicit Serializer(const char* _path);

	private:
		void InitFile(const char* _path);

		void StartArray();

		void EndArray();
	public:
		template<typename TPoint>
		void SerializeObj(TPoint _object, const char* _function, const char* calledInFile = __FILE__, int lineId = __LINE__);

		~Serializer();
	private:
		class MyOStreamWrapper
		{
		public:
			typedef char Ch;
			explicit MyOStreamWrapper(std::ostream& os) : os_(os) {
			}
			ENGINE_INLINE Ch Peek() const { ASSERT(false); return '\0'; }
			ENGINE_INLINE Ch Take() { ASSERT(false); return '\0'; }
			ENGINE_INLINE size_t Tell() const { return os_.tellp(); }
			ENGINE_INLINE Ch* PutBegin() { ASSERT(false); return 0; }
			ENGINE_INLINE void Put(Ch c) { os_.put(c); }
			ENGINE_INLINE void Flush() { os_.flush(); }
			ENGINE_INLINE size_t PutEnd(Ch*) { ASSERT(false); return 0; }
		private:
			MyOStreamWrapper(const MyOStreamWrapper&);
			MyOStreamWrapper& operator=(const MyOStreamWrapper&);
			std::ostream& os_;
		};
	private:
		std::ofstream m_outfile;
		MyOStreamWrapper *m_stream = nullptr;
		Writer<MyOStreamWrapper>* writer;
	};
}