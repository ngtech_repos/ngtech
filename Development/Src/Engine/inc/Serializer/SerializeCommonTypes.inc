/**
*/
template <> ENGINE_INLINE ENGINE_API
void Serializer::SerializeObj(float _value, const char* _function, const char* calledInFile, int lineId)
{
	ASSERT(writer, "Invalid obj");
	if (!writer)
	{
		Warning(__FUNCTION__);
		return;
	}

	DEBUG_SERIALIZER_TRACE;
	writer->Double((double)_value);
}

/**
*/
template <> ENGINE_INLINE ENGINE_API
void Serializer::SerializeObj(double _value, const char* _function, const char* calledInFile, int lineId)
{
	ASSERT(writer, "Invalid obj");
	if (!writer)
	{
		Warning(__FUNCTION__);
		return;
	}

	DEBUG_SERIALIZER_TRACE;
	writer->Double(_value);
}

/**
*/
template <> ENGINE_INLINE ENGINE_API
void Serializer::SerializeObj(bool _value, const char* _function, const char* calledInFile, int lineId)
{
	ASSERT(writer, "Invalid obj");
	if (!writer)
	{
		Warning(__FUNCTION__);
		return;
	}

	DEBUG_SERIALIZER_TRACE;
	writer->Bool(_value);
}

/**
*/
template <> ENGINE_INLINE ENGINE_API
void Serializer::SerializeObj(int _value, const char* _function, const char* calledInFile, int lineId)
{
	ASSERT(writer, "Invalid obj");
	if (!writer)
	{
		Warning(__FUNCTION__);
		return;
	}

	DEBUG_SERIALIZER_TRACE;
	writer->Int(_value);
}

/**
*/
template <> ENGINE_INLINE ENGINE_API
void Serializer::SerializeObj(unsigned int _value, const char* _function, const char* calledInFile, int lineId)
{
	if (!writer)
	{
		Warning(__FUNCTION__);
		return;
	}

	DEBUG_SERIALIZER_TRACE;
	writer->Uint(_value);
}

/**
*/
template <> ENGINE_INLINE ENGINE_API
void Serializer::SerializeObj(long long _value, const char* _function, const char* calledInFile, int lineId)
{
	ASSERT(writer, "Invalid obj");
	if (!writer)
	{
		Warning(__FUNCTION__);
		return;
	}

	DEBUG_SERIALIZER_TRACE;
	writer->Int64(_value);
}

/**
*/
template <> ENGINE_INLINE ENGINE_API
void Serializer::SerializeObj(const char* _value, const char* _function, const char* calledInFile, int lineId)
{
	ASSERT(writer, "Invalid obj");
	if ((!writer) || (strcmp(_value, "") == 0))
	{
		Warning(__FUNCTION__);
		return;
	}

	DEBUG_SERIALIZER_TRACE;
	writer->String(_value);
}

/**
*/
template <> ENGINE_INLINE ENGINE_API
void Serializer::SerializeObj(String _value, const char* _function, const char* calledInFile, int lineId)
{
	ASSERT(writer, "Invalid obj");
	// ��������� ������ ������ �� ����
	if ((!writer))
	{
		Warning(__FUNCTION__);
		return;
	}

	DEBUG_SERIALIZER_TRACE;
	writer->String(_value.c_str());
}