/**
*/
template <> ENGINE_INLINE ENGINE_API
void Serializer::SerializeObj(Vec2 _vec, const char* _function, const char* calledInFile, int lineId)
{
	ASSERT(writer, "Invalid obj");
	if (!writer)
	{
		Warning(__FUNCTION__);
		return;
	}

	DEBUG_SERIALIZER_TRACE
		StartArray();
	SerializeObj(_vec.x, __FUNCTION__, __FILE__, __LINE__);
	SerializeObj(_vec.y, __FUNCTION__, __FILE__, __LINE__);
	EndArray();
}

/**
*/
template <> ENGINE_INLINE ENGINE_API
void Serializer::SerializeObj(Vec3 _vec, const char* _function, const char* calledInFile, int lineId)
{
	ASSERT(writer, "Invalid obj");
	if (!writer)
	{
		Warning(__FUNCTION__);
		return;
	}

	DEBUG_SERIALIZER_TRACE
		StartArray();
	SerializeObj(_vec.x, __FUNCTION__, __FILE__, __LINE__);
	SerializeObj(_vec.y, __FUNCTION__, __FILE__, __LINE__);
	SerializeObj(_vec.z, __FUNCTION__, __FILE__, __LINE__);
	EndArray();
}

/**
*/
template <> ENGINE_INLINE ENGINE_API
void Serializer::SerializeObj(Vec4 _vec, const char* _function, const char* calledInFile, int lineId)
{
	ASSERT(writer, "Invalid obj");
	if (!writer)
	{
		Warning(__FUNCTION__);
		return;
	}

	DEBUG_SERIALIZER_TRACE
		StartArray();
	SerializeObj(_vec.x, __FUNCTION__, __FILE__, __LINE__);
	SerializeObj(_vec.y, __FUNCTION__, __FILE__, __LINE__);
	SerializeObj(_vec.z, __FUNCTION__, __FILE__, __LINE__);
	SerializeObj(_vec.w, __FUNCTION__, __FILE__, __LINE__);
	EndArray();
}

/**
*/
template <> ENGINE_INLINE ENGINE_API
void Serializer::SerializeObj(Color _vec, const char* _function, const char* calledInFile, int lineId)
{
	ASSERT(writer, "Invalid obj");
	if (!writer)
	{
		Warning(__FUNCTION__);
		return;
	}

	DEBUG_SERIALIZER_TRACE
		StartArray();
	SerializeObj(_vec.x, __FUNCTION__, __FILE__, __LINE__);
	SerializeObj(_vec.y, __FUNCTION__, __FILE__, __LINE__);
	SerializeObj(_vec.z, __FUNCTION__, __FILE__, __LINE__);
	SerializeObj(_vec.w, __FUNCTION__, __FILE__, __LINE__);
	EndArray();
}

/**
*/
template <> ENGINE_INLINE ENGINE_API
void Serializer::SerializeObj(Mat3 matrix, const char* _function, const char* calledInFile, int lineId)
{
	ASSERT(writer, "Invalid obj");
	if (!writer)
	{
		Warning(__FUNCTION__);
		return;
	}

	DEBUG_SERIALIZER_TRACE
		StartArray();
	for (int i = 0; i < 9; i++)
	{
		SerializeObj(matrix.e[i], __FUNCTION__, __FILE__, __LINE__);
	}
	EndArray();
}

/**
*/
template <> ENGINE_INLINE ENGINE_API
void Serializer::SerializeObj(Mat4 matrix, const char* _function, const char* calledInFile, int lineId)
{
	ASSERT(writer, "Invalid obj");
	if (!writer)
	{
		Warning(__FUNCTION__);
		return;
	}

	DEBUG_SERIALIZER_TRACE
		StartArray();
	for (int i = 0; i < 16; i++)
	{
		SerializeObj(matrix.e[i], __FUNCTION__, __FILE__, __LINE__);
	}
	EndArray();
}