/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

namespace NGTech
{
	struct ENGINE_API ThreadableObject
	{
	protected:
		MT::Mutex mutex;
	};

	struct ENGINE_API UpdatableObject :public ThreadableObject
	{
		UpdatableObject()
		{
		}

		virtual ~UpdatableObject()
		{
		}

		/// Physics Loop part
		/// Called first
		virtual bool FixedUpdate() {
			//MT::ScopedGuard guard(mutex);
			//	Warning("UpdatableObject::FixedUpdate");
			return true;
		}

		virtual bool WaitForFixedUpdate()
		{
			//MT::ScopedGuard guard(mutex);
			//	Warning("UpdatableObject::WaitForFixedUpdate");
			return true;
		}

		/// Update
		/// Called second
		virtual bool Update()
		{
			//MT::ScopedGuard guard(mutex);
			//	Warning("UpdatableObject::Update");
			return true;
		}
		virtual bool WaitForSeconds()
		{
			//MT::ScopedGuard guard(mutex);
			//	Warning("UpdatableObject::WaitForSeconds");
			return true;
		}

		/// Late Update
		/// Called third
		virtual bool LateUpdate()
		{
			//MT::ScopedGuard guard(mutex);
			//	Warning("UpdatableObject::WaitForSeconds");
			return true;
		}

	protected:
		/// ���������� ������ � �����, � ������ �����������
		void _AddToSceneList();
		/// �������� �� ������ �����������
		void _RemoveFromScene();
	};
}