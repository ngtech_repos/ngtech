#pragma once

//***************************************************************************
#include <string.h>
//***************************************************************************
#include "../../Externals/libtheora/include/theora/theora.h"
//***************************************************************************

namespace NGTech
{
	/*
	 */
	class VideoFileTheora {
	public:

		VideoFileTheora();
		virtual ~VideoFileTheora();

		// Load
		bool Load(const char *name);

		// Parameters
		uint32_t GetWidth();
		uint32_t GetHeight();
		float GetIFps();

		// time
		bool SetTime(float time);
		float GetTime();

		// read rgb frame from file
		bool ReadYUV(unsigned char *data);
		bool ReadRGB(unsigned char *data);

		// drop the fram
		bool Drop();

	private:

		bool ReadStream();
		bool ReadPacket();

		bool InitTheora();
		void ClearTheora();

		VFile *file = nullptr;

		ogg_sync_state oy;
		ogg_page og;
		ogg_packet op;
		ogg_stream_state to;
		theora_info ti;
		theora_comment tc;
		theora_state td;

		float ifps;
	};
}