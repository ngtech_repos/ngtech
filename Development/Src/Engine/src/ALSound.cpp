/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
//***************************************************************************
#include "string.h"//memset for gcc
//***************************************************************************
#include "../../Externals/libvorbis/include/vorbis/vorbisfile.h"
//***************************************************************************
#include "ALSystem.h"
#include "Log.h"
#include "FileHelper.h"
//***************************************************************************

namespace NGTech
{
	/*
	*/
	static ENGINE_INLINE size_t vorbis_read_function(void *dest, int size, int nmemb, void *_file) {
		return static_cast<VFile*>(_file)->ReadAsSizeT(dest, size, nmemb);
	}

	static ENGINE_INLINE int vorbis_seek_function(void *_file, ogg_int64_t offset, int whence) {
		/*
		whence can be
		SEEK_SET
		SEEK_END
		SEEK_CUR
		*/
		return static_cast<VFile*>(_file)->FSeek(offset, whence);
	}

	static ENGINE_INLINE long vorbis_tell_function(void *_file) {
		return (long)static_cast<VFile*>(_file)->FTell();
	}

	static ENGINE_INLINE int vorbis_close_function(void *_file) {
		return (long)static_cast<VFile*>(_file)->FTell();
	}

	/*
	*/
	ALSound::ALSound(const String &_path, bool _load)
	{
		// only for debug - ��� ���������, 1 ������
		ASSERT(this->RefCounter() >= 0, "incorrect count on construct %i", this->RefCounter()); //-V595

		Desc _desc;
		_desc.path = _path;

		ASSERT(this);
		SetDescAndLoad(_desc, _load);
	}

	ALSound::ALSound(Desc _desc, bool _load) : ALSound()
	{
		// only for debug - ��� ���������, 1 ������
		ASSERT(this->RefCounter() >= 0, "Desc incorrect count on construct %i", this->RefCounter()); //-V595

		ASSERT(this);
		SetDescAndLoad(_desc, _load);
	}

	ALSound::~ALSound() {
		alDeleteBuffers(1, &desc._buffID);
		if (desc._samples)
			free(desc._samples);
	}

	void ALSound::SetDescAndLoad(Desc _desc, bool _load)
	{
		MT::ScopedGuard guard(mutex);
		desc = _desc;

		this->SetPath(_desc.path);

		if (_load && !_LoadFile())
		{
			Warning("Failed loading audio file from path! %s .Check duplicates", GetPath().c_str());
		}
	}

	bool ALSound::_LoadFile() {
		MT::ScopedGuard guard(mutex);
		this->SetPath(String("sounds/") + desc.path);

		VFile file(GetPath().c_str(), FILE_MODE::READ_BIN);

		if (file.IsValid() == false || (file.IsValid() == true && _CheckFormatSupport(GetPath()) == false))
		{
			Warning("ALSound::_LoadFile error: sound file '%s'  is not a valid file", GetPath().c_str());
			return false;
		}

		OggVorbis_File vf;
		memset(&vf, 0, sizeof(vf));

		ov_callbacks callbacks = {
			(size_t(*)(void *, size_t, size_t, void *))  vorbis_read_function,
			(int(*)(void *, ogg_int64_t, int))              vorbis_seek_function,
			(int(*)(void *))                             vorbis_close_function,
			(long(*)(void *))                            vorbis_tell_function
		};

		if (ov_open_callbacks(&file, &vf, NULL, 0, callbacks) < 0) {
			Warning("ALSound::_LoadFile error: sound file '%s'  is not a valid file", GetPath().c_str());
			return false;
		}

		auto _vi = ov_info(&vf, -1);
		ASSERT(_vi, "Failed ov_info");

		desc._numSamples = (int)ov_pcm_total(&vf, -1);
		desc._numChannels = vf.vi->channels;

		switch (desc._numChannels)
		{
		case 1:
			desc._format = AL_FORMAT_MONO16;
			break;
		case 2:
			desc._format = AL_FORMAT_STEREO16;
			break;
		case 4:
			if (AudioExt::EXT_AL_FORMAT_QUAD16)
				desc._format = AudioExt::EXT_AL_FORMAT_QUAD16;
			else
			{
				Warning("ALSound::_LoadFile(): number of channels %d in \"%s\" file, but audio extenssion is not supported. Activated Mono \n", desc._numChannels, GetPath().c_str());
				desc._format = AL_FORMAT_MONO16;
			}
			break;
		case 6:
			if (AudioExt::EXT_AL_FORMAT_51CHN16)
				desc._format = AudioExt::EXT_AL_FORMAT_51CHN16;
			else
			{
				Warning("ALSound::_LoadFile(): number of channels %d in \"%s\" file, but audio extenssion is not supported. Activated Mono \n", desc._numChannels, GetPath().c_str());
				desc._format = AL_FORMAT_MONO16;
			}
			break;
		case 7:
			if (AudioExt::EXT_AL_FORMAT_61CHN16)
				desc._format = AudioExt::EXT_AL_FORMAT_61CHN16;
			else
			{
				Warning("ALSound::_LoadFile(): number of channels %d in \"%s\" file, but audio extenssion is not supported. Activated Mono \n", desc._numChannels, GetPath().c_str());
				desc._format = AL_FORMAT_MONO16;
			}
			break;
		case 8:
			if (AudioExt::EXT_AL_FORMAT_71CHN16)
				desc._format = AudioExt::EXT_AL_FORMAT_71CHN16;
			else
			{
				Warning("ALSound::_LoadFile(): number of channels %d in \"%s\" file, but audio extenssion is not supported. Activated Mono \n", desc._numChannels, GetPath().c_str());
				desc._format = AL_FORMAT_MONO16;
			}
			break;
		default:
			Warning("ALSound::_LoadFile(): bad number of channels %d in \"%s\" file\n", desc._numChannels, GetPath().c_str());
			SAFE_DELETE(_vi);
			return false;
		}

		desc._rate = vf.vi->rate;
		desc._size = desc._numSamples * desc._numChannels;

		desc._samples = new short[desc._size];
		desc._size *= sizeof(short);

		int samplePos = 0;
		while (samplePos < desc._size) {
			char *dest = (char *)desc._samples + samplePos;

			int bitStream, readBytes = ov_read(&vf, dest, this->desc._size - samplePos, 0, 2, 1, &bitStream);
			if (readBytes <= 0)
				break;
			samplePos += readBytes;
		}
		ov_clear(&vf);

		// Check errors
		alGetError();
		// Base Init
		alGenBuffers(1, &desc._buffID);

		if (AudioExt::CheckErrors()) {
			SAFE_DELETE(_vi);
			return false;
		}

		alBufferData(desc._buffID, desc._format, desc._samples, desc._size, desc._rate);

		int error = alGetError();
		if (error != AL_NO_ERROR) {
			SAFE_DELETE(_vi);
			ErrorWrite("ALSound::_LoadFile() error: sound file %s %s", GetPath().c_str(), " could not be loaded");
			return false;
		}

		SetLoaded(true);

		return true;
	}

	bool ALSound::_CheckFormatSupport(const String &path) {
		MT::ScopedGuard guard(mutex);
		String compare(FileUtil::GetExt(path.c_str()));
		if (compare == "ogg") {
			return true;
		}

		ErrorWrite("ALSound::_CheckFormatSupport() error: sound file %s %s", path.c_str(), " is not a OGG file");

		return false;
	}
}