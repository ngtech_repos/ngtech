/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
//***************************************************************************
#include "ALSystem.h"
#include "Log.h"
#include "Cache.h"
//***************************************************************************

namespace NGTech {
	/**
	*/
	ALSoundSource*
		ALSoundSource::CreateWithSoundSourcePreset(const String &_path)
	{
		auto ptr = new ALSoundSource(_path);
		ptr->LoadSoundSourcePreset();
		return ptr;
	}

	ALSoundSource*
		ALSoundSource::CreateWithBackGroundMusicPreset(const String &_path)
	{
		auto ptr = new ALSoundSource(_path);
		ptr->LoadBackgroundPreset();
		return ptr;
	}

	ALSoundSource*
		ALSoundSource::ConstructDesc(const Desc&_desc, const String&name)
	{
		ALSoundSource* sound = nullptr;

		ASSERT((name.empty() || name == "INVALID") && "Invalid name of sound");
		if (name.empty() || name == "INVALID")
		{
			Warning("Invalid name of sound");
			return sound;
		}

		auto ptr = new ALSoundSource(name);
		ptr->SetDescAndApply(_desc);
		return ptr;
	}

	ALSoundSource*
		ALSoundSource::CreateWithSoundSourcePreset(ALSound* _sound)
	{
		ASSERT(_sound, "Invalid pointer");
		_sound->AddRef();

		auto ptr = new ALSoundSource(_sound);
		ptr->LoadSoundSourcePreset();

		return ptr;
	}

	ALSoundSource*
		ALSoundSource::CreateWithBackGroundMusicPreset(ALSound* _sound)
	{
		ASSERT(_sound, "Invalid pointer");
		_sound->AddRef();

		auto ptr = new ALSoundSource(_sound);
		ptr->LoadBackgroundPreset();

		return ptr;
	}

	/**
	*/
	ALSoundSource::ALSoundSource(const String &sound)
	{
		AddRef();

		_CreateFromSound(Cache::LoadSound(sound));
	}

	ALSoundSource::ALSoundSource(ALSound*_sound)
	{
		AddRef();
		_CreateFromSound(_sound);
	}

	ALSoundSource::~ALSoundSource() {
		if (desc.IsValid())
			alDeleteSources(1, &desc.alID);
	}

	/**
	*/
	void ALSoundSource::Play() {
		MT::ScopedGuard guard(mutex);
		if (desc.IsValid())
			alSourcePlay(desc.alID);

		desc.Paused = false;
		desc.Stoped = false;
	}

	void ALSoundSource::Stop() {
		MT::ScopedGuard guard(mutex);
		if (desc.IsValid())
			alSourceStop(desc.alID);

		desc.Stoped = true;
		desc.Paused = false;
	}

	void ALSoundSource::Pause() {
		MT::ScopedGuard guard(mutex);
		if (desc.IsValid())
			alSourcePause(desc.alID);

		desc.Paused = true;
		desc.Stoped = false;
	}

	/**
	*/
	bool ALSoundSource::isPlaying() {
		MT::ScopedGuard guard(mutex);
		if (desc.alID == -1)
			return false;

		ALint state;
		alGetSourcei(desc.alID, AL_SOURCE_STATE, &state);
		return (state == AL_PLAYING);
	}

	/**
	*/
	void ALSoundSource::SetLooping(bool loop) {
		MT::ScopedGuard guard(mutex);
		if (desc.IsValid())
			alSourcei(desc.alID, AL_LOOPING, loop);

		desc.Looped = loop;
	}

	/**
	*/
	void ALSoundSource::SetRelative(bool relative) {
		MT::ScopedGuard guard(mutex);
		if (desc.IsValid())
			alSourcei(desc.alID, AL_SOURCE_RELATIVE, relative);

		desc.Relative = relative;
	}

	/**
	*/
	void ALSoundSource::SetGain(TimeDelta gain) {
		MT::ScopedGuard guard(mutex);
		auto volume = Math::ONEDELTA;
		//!@todo CHECK
		TODO("��������");
		if (desc.IsValid())
			alSourcef(desc.alID, AL_GAIN, gain*volume);

		desc.Gain = gain;
	}

	/**
	*/
	void ALSoundSource::SetPosition(const Vec3 &position) {
		MT::ScopedGuard guard(mutex);
		if (desc.IsValid())
			alSourcefv(desc.alID, AL_POSITION, position);

		desc.Position = position;
	}

	/**
	*/
	void ALSoundSource::SetRolloffFactor(TimeDelta rolloffFactor) {
		MT::ScopedGuard guard(mutex);
		if (desc.IsValid())
			alSourcef(desc.alID, AL_ROLLOFF_FACTOR, rolloffFactor);

		desc.RollOffFactor = rolloffFactor;
	}

	/**
	*/
	void ALSoundSource::SetMaxDistance(TimeDelta maxDistance) {
		MT::ScopedGuard guard(mutex);
		if (desc.IsValid())
			alSourcef(desc.alID, AL_MAX_DISTANCE, maxDistance);

		desc.MaxDistance_in_Meter = maxDistance;
	}

	void ALSoundSource::SetMinDistance(TimeDelta minDistance) {
		MT::ScopedGuard guard(mutex);
		if (desc.IsValid())
			alSourcef(desc.alID, AL_REFERENCE_DISTANCE, minDistance);

		desc.MinDistance_in_Meter = minDistance;
	}

	/**
	*/
	void ALSoundSource::LoadSoundSourcePreset()
	{
		MT::ScopedGuard guard(mutex);
		SetLooping(false);
		SetMinDistance(Math::ZEROFLOAT);
		SetMaxDistance(1000.0f);
		SetGain(Math::ONEFLOAT);
	}

	/**
	*/
	void ALSoundSource::LoadBackgroundPreset()
	{
		MT::ScopedGuard guard(mutex);
		SetLooping(true);
		SetGain(Math::ONEFLOAT);
	}

	/**
	*/
	void ALSoundSource::SetDescAndApply(const Desc& _desc)
	{
		MT::ScopedGuard guard(mutex);
		desc = _desc;

		SetLooping(desc.Looped);
		SetRelative(desc.Relative);

		SetGain(desc.Gain);
		SetRolloffFactor(desc.RollOffFactor);

		SetMaxDistance(desc.MaxDistance_in_Meter);
		SetMinDistance(desc.MinDistance_in_Meter);

		SetPosition(desc.Position);
	}

	void ALSoundSource::_CreateFromSound(ALSound* _sound)
	{
		MT::ScopedGuard guard(mutex);
		ASSERT(_sound, "Invalid pointer");

		desc.sound = _sound;
		alGenSources(1, &this->desc.alID);
		if (desc.IsValid())
			alSourcei(this->desc.alID, AL_BUFFER, desc.sound->desc._buffID);
		else
		{
			Error("Failed creation sound", true);
		}
	}
}