/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
//***************************************************************************
#include "Scene.h"
#include "ALSystem.h"
#include "AudioUpdateJob.h"
#include "Log.h"
//***************************************************************************

namespace NGTech
{
	/**
	*/
	ALSystem::ALSystem(bool _m_UseSingleThread, Engine* _engine)
		: m_UseSingleThread(_m_UseSingleThread),
		mEngine(_engine)
	{
		Debug(__FUNCTION__);
	}

	/**
	*/
	ALSystem::ALSystem()
		: alDevice(nullptr),
		alContext(nullptr),
		has_ext_efx(0),
		has_efx_reverb(0),
		has_efx_filter(0),
		has_eax_reverb(0),
		time(Math::ZERODELTA),
		total_time(Math::ZERODELTA),
		m_NeedlyDestroyThread(false)
	{
	}

	/**
	*/
	bool ALSystem::CheckExtension(const char*_name)
	{
#if !DISABLE_AUDIO
		int supported = AudioExt::CheckExtension(alDevice, _name);
		if (supported) LogPrintf("[ALSystem::CheckExtension] Found %s\n", _name);
		return supported;
#else
		return false;
#endif
	}

	/**
	*/
	void ALSystem::Initialise() {
#if !DISABLE_AUDIO
		Log::WriteHeader("-- ALSystem --");

		alDevice = alcOpenDevice(NULL);
		if (!alDevice) {
			Error("ALSystem::Initialise(): Can't open device\n", true);
			return;
		}

		// create attributes
		ALCint attribs[128];
		memset(attribs, 0, sizeof(attribs));
		ALCint *aptr = attribs;

		*aptr++ = ALC_MAX_AUXILIARY_SENDS;
		*aptr++ = 2;

		// create context
		alcGetError(alDevice);
		alContext = alcCreateContext(alDevice, attribs);
		if (!alContext)
		{
			ErrorWrite("ALSystem::Initialise(): Can't create context\n");
			alcCloseDevice(alDevice);
			alDevice = nullptr;
			return;
		}

		// set current context
		alcGetError(alDevice);
		auto result = alcMakeContextCurrent(alContext);
		if (result == AL_FALSE)
		{
			AudioExt::CheckContextErrors(alDevice);
			ErrorWrite("ALSystem::Initialise(): Can't make context current\n");
			alcDestroyContext(alContext);
			alcCloseDevice(alDevice);
			alContext = nullptr;
			alDevice = nullptr;
			return;
		}

		LogPrintf("Vendor:     %s", GetVendor().c_str());
		LogPrintf("Renderer:   %s", GetRenderer().c_str());
		LogPrintf("Version:    %s", GetVersion().c_str());
		LogPrintf("Extensions: %s", GetExtensions().c_str());

		// sound extensions
		has_ext_efx = 0;
		has_efx_filter = 0;
		has_efx_reverb = 0;
		has_eax_reverb = 0;

		// check efx extension
		has_ext_efx = CheckExtension(ALC_EXT_EFX_NAME);
		if (hasEXTEfx())
		{
			// check efx filter
			alGetError();
			ALuint filter_id = 0;
			alGenFilters(1, &filter_id);
			if (alGetError() == AL_NO_ERROR) {
				alFilteri(filter_id, AL_FILTER_TYPE, AL_FILTER_LOWPASS);
				has_efx_filter = (alGetError() == AL_NO_ERROR);
			}
			if (alIsFilter(filter_id)) alDeleteFilters(1, &filter_id);

			// check efx reverb
			alGetError();
			ALuint effect_id = 0;
			alGenEffects(1, &effect_id);
			if (alGetError() == AL_NO_ERROR) {
				alEffecti(effect_id, AL_EFFECT_TYPE, AL_EFFECT_REVERB);
				has_efx_reverb = (alGetError() == AL_NO_ERROR);
				alEffecti(effect_id, AL_EFFECT_TYPE, AL_EFFECT_EAXREVERB);
				has_eax_reverb = (alGetError() == AL_NO_ERROR);
			}
			if (alIsEffect(effect_id)) alDeleteEffects(1, &effect_id);
		}
		if (hasEFXFilter()) LogPrintf("[Audio Extension] Supported extension EFX Filter");
		if (hasEFXReverb()) LogPrintf("[Audio Extension] Supported extension EFX Reverb");
		if (hasEAXReverb()) LogPrintf("[Audio Extension] Supported extension EAX Reverb");

		// Advanced options
		audioExt.Init();

		// sound formats
		audioExt.ReportStatusExtensions();

		// set default options
		alDistanceModel(AL_LINEAR_DISTANCE_CLAMPED);

		//Enable Threading
		_InitThread();
#endif
	}

	/**
	*/
	ALSystem::~ALSystem()
	{
#if !DISABLE_AUDIO
		_DestroyThread();
		if (alDevice)
		{
			alcMakeContextCurrent(nullptr);

			alcDestroyContext(alContext);
			alcCloseDevice(alDevice);

			alDevice = nullptr;
			alContext = nullptr;
		}
#endif
	}

	/**
	*/
	String ALSystem::GetVendor() {
#if !DISABLE_AUDIO
		return (char *)alGetString(AL_VENDOR);
#else
		return "";
#endif
	}

	/**
	*/
	String ALSystem::GetRenderer() {
#if !DISABLE_AUDIO
		return (char *)alGetString(AL_RENDERER);
#else
		return "";
#endif
	}

	/**
	*/
	String ALSystem::GetVersion() {
#if !DISABLE_AUDIO
		return (char *)alGetString(AL_VERSION);
#else
		return "";
#endif
	}

	/**
	*/
	String ALSystem::GetExtensions() {
#if !DISABLE_AUDIO
		return (char *)alGetString(AL_EXTENSIONS);
#else
		return "";
#endif
	}

	/**
	*/
	void ALSystem::SetListener(Vec3 pos, Vec3 dir) {
#if !DISABLE_AUDIO
		float orientation[] = { dir.x, dir.y, dir.z, 0, 1, 0 };
		alListenerfv(AL_POSITION, pos);
		alListenerfv(AL_ORIENTATION, orientation);
#endif
	}

	void ALSystem::SetListener(Camera * _cam)
	{
		if (!_cam)
			return;

		auto pos = _cam->GetPosition();
		auto dir = _cam->GetDirection();

		SetListener(pos, dir);
	}

	/**
	*/
	void ALSystem::Update() {
		BROFILER_CATEGORY("UpdateAudio", Brofiler::Color::YellowGreen);

#ifndef DROP_EDITOR
		auto m_stat = GetStatistic();
		if (m_stat) {
			m_stat->SetAudioDeltaZero();

			static const TimeDelta timestep = Math::ONEDELTA / TimeDelta(UpdateConfig::Audio);

			static Timer timer;
			auto ftime = timer.GetElapsed();

			m_stat->SetAudioDelta(ftime);
			// update time
			{
				time += ftime;
				if (time < timestep)
					return;

				// ��������� ����������� �����
				ftime = timer.GetElapsed();
				// ����� �� ����� ���� �������������
				time = Math::mod(time, timestep);
			}
		}
#endif
		{
			if ((mEngine == nullptr) || (mEngine && mEngine->scene == nullptr)) //-V560
				return;

			auto curcam = mEngine->scene->m_ActiveScene.GetCurrentCameraData();
			this->SetListener(curcam._position, curcam.direction);
		}
	}

	/**
	*/
	void ALSystem::_InitThread()
	{
#if !DISABLE_AUDIO
		if (m_UseSingleThread == false)
			updateThread.Start(/*SMALLEST_STACK_SIZE*/32768, _UpdateLoop, this);
#endif
	}

	void ALSystem::_DestroyThread()
	{
#if !DISABLE_AUDIO
		if (m_UseSingleThread == false) {
			m_NeedlyDestroyThread = true;
			updateThread.Join();
		}
#endif
	}

	void ALSystem::_UpdateLoop(void*userData)
	{
#if !DISABLE_AUDIO
		BROFILER_THREAD("Audio Thread");
		static auto mAudio = static_cast<ALSystem*>(userData);
		ASSERT(mAudio, "Invalid pointer on mAudio. Failed cast from userData");

		static bool needlyDestroy = false;

		while (!needlyDestroy)
		{
			// Emulate "wait for events" message
			// Sleep ����������� �����,����� ��� ����� �����
			MT::SpinSleepMilliSeconds(1);
			if (mAudio)
			{
				mAudio->Update();

				needlyDestroy = mAudio->m_NeedlyDestroyThread;
			}
			// ��� ����� :)
		}
#endif
	}
}