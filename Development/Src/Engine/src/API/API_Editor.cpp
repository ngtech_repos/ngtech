/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"

#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
#include "EditorObjectManager.h"
#include "..\..\API\EditorAPI.h"

#include "Scene.h"

#include "Object.h"
#include "ObjectMesh.h"
#include "AnimatedMesh.h"

#include "Light.h"

#include <Rpc.h>
#include <Objbase.h>

namespace NGTech
{
	ENGINE_API EditableObject* API_Editor_FindActor(GUID guid)
	{
		auto manager = GetEditorObjectManager();
		ASSERT(manager, "Invalid pointer on manager");

		for (auto&ptr : manager->m_EditableObjects)
		{
			if (ptr && (ptr->IsSelectable && ptr->GetGUID() == guid))
				return ptr;
			else
				continue;
		}

		return nullptr;
	}

	ENGINE_API EditableObject* API_Editor_FindActorGUIDString(const char* sGuid)
	{
		GUID id;
		UuidFromString((unsigned char*)sGuid, &id);
		return API_Editor_FindActor(id);
	}

	ENGINE_API void API_Editor_DestroyActorGUIDString(const char * sGuid)
	{
		GUID id;
		UuidFromString((unsigned char*)sGuid, &id);

		auto actor = API_Editor_FindActor(id);
		if (actor)
		{
			SAFE_DELETE(actor);
		}
		else
			Warning("[API_Editor_DestroyActorGUIDString] Object with guid: %s not found on scene", sGuid);
	}
}
#endif