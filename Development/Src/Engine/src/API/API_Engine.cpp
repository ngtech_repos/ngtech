/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"

#include "Scene.h"
#include "Light.h"
#include "LightPoint.h"

namespace NGTech
{
	/**
	*/
	class Camera;
	class Light;
	/**
	*/
	void ENGINE_API API_LoadEngineModule(const String & _name)
	{
		Debug(__FUNCTION__);
		ASSERT(!_name.empty(), "Invalid name of module");

		if (_name.empty())
			return;

		auto engine = GetEngine();
		ASSERT(engine, "Invalid pointer on engine");

		if (!engine)
			return;

		engine->LoadEngineModule(_name.c_str());
	}

	void ENGINE_API API_LoadStartupModule(const String & _cfg, int argc, const char * const * argv)
	{
		Debug(__FUNCTION__);
		ASSERT(!_cfg.empty(), "Invalid name of module");
		if (_cfg.empty())
			return;

		auto engine = GetEngine();
		ASSERT(engine, "Invalid pointer on engine");

		if (!engine)
			return;

		engine->LoadStartupModule(_cfg.c_str(), argc, argv);
	}

	void ENGINE_API API_LoadStartupModuleEditor(const String & _cfg, int argc, const char * const * argv)
	{
		Debug(__FUNCTION__);
		ASSERT(!_cfg.empty(), "Invalid name of module");
		if (_cfg.empty())
			return;

		auto engine = GetEngine();
		ASSERT(engine, "Invalid pointer on engine");

		if (!engine)
			return;

		engine->LoadStartupModuleEditor(_cfg.c_str(), argc, argv);
	}

	void ENGINE_API API_EngineAutoStart(int argc, const char * const * argv)
	{
		// load the game dll
		API_LoadStartupModule("../system.ltx", argc, argv);
	}

	void ENGINE_API API_DestroyEngine()
	{
		auto engine = GetEngine();
		ASSERT(engine, "Invalid pointer on engine");
		if (!engine)
			return;

		engine->Quit();
	}

	ENGINE_API Camera* API_GetCurrentCamera() {
		auto scene = GetScene();
		ASSERT(scene, "Invalid pointer on engine");
		if (!scene)
			return nullptr;

		return scene->m_ActiveScene.GetCurrentCamera();
	}
}