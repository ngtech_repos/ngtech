/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"

//**************************************
#include <stdint.h>
#include <sys/stat.h>
//**************************************
#include "RenderPipeline.h"
//**************************************

namespace NGTech
{
	/**
	*/
	static void _R_GenerateScreenshotFilename(int &lastNumber, const char *base, String &fileName) {
		int	a, b, c, d, e;

		char buff[256];

		lastNumber++;
		if (lastNumber > 99999) {
			lastNumber = 99999;
		}
		for (; lastNumber < 99999; lastNumber++) {
			int	frac = lastNumber;

			a = frac / 10000;
			frac -= a * 10000;
			b = frac / 1000;
			frac -= b * 1000;
			c = frac / 100;
			frac -= c * 100;
			d = frac / 10;
			frac -= d * 10;
			e = frac;

			sprintf(buff, "%s%i%i%i%i%i.tga", base, a, b, c, d, e);
			fileName = buff;
			if (lastNumber == 99999) {
				break;
			}

			bool len = FileUtil::FileExist(fileName);
			if (!len) {
				break;
			}
			// check again...
		}
	}

	/**
	*/
#define	MAX_BLENDS	256	// to keep the accumulation in shorts
	ENGINE_API void API_Make_ScreenShot() {
		static int lastNumber = 0;
		String checkname;

		auto render = GetRender();
		ASSERT(render, "Not exist Render currently");

		FileUtil::CreateDirectoryIfNotExist("../userData/screenshots/");

		_R_GenerateScreenshotFilename(lastNumber, "../userData/screenshots/shot", checkname);

		render->WriteScreenshot(checkname.c_str());

		LogPrintf("Wrote %s\n", checkname.c_str());
	}

	/**
	*/
	ENGINE_API void API_ResizeWindow(int _w, int _h) {
		auto rp = GetRenderPipeline();
		ASSERT(rp, "Not exist RenderPipeline currently");

		rp->ReSize(Vec2(_w, _h));
	}

	/**
	*/
	ENGINE_API void API_EnableDisableWireframe(bool _use) {
		auto cvars = GetCvars();
		ASSERT(cvars, "Not exist CVARManager currently");

		cvars->r_wireframe = _use;
	}

	/**
	*/
	ENGINE_API void API_SetDebugDrawMode(unsigned int _dm) {
		if ((_dm >= SceneMatrixes::COUNT_VIS) || _dm < SceneMatrixes::FINAL_LIGHTING) {
			matrixes.debug_drawmode = SceneMatrixes::FINAL_LIGHTING;
			return;
		}

		matrixes.debug_drawmode = static_cast<SceneMatrixes::DrawMode>(_dm);
	}
}