/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"

#include "Scene.h"
#include "Object.h"
#include "ObjectMesh.h"

#include "LightsInc.h"

namespace NGTech
{
	void ENGINE_API API_NewScene()
	{
		auto ptr = GetScene();
		ASSERT(ptr, "Invalid pointer on scene");
		if (!ptr)
			return;

		ptr->NewScene();
	}

	void ENGINE_API API_SetLoadingScreen(const char * _name)
	{
		auto ptr = GetScene();
		ASSERT(ptr, "Invalid pointer on scene");
		if (!ptr)
			return;

		ptr->SetLoadingScreen(_name);
	}

	void ENGINE_API API_SetLoadingScreen(LoadingScreen *_screen)
	{
		auto ptr = GetScene();
		ASSERT(ptr, "Invalid pointer on scene");
		if (!ptr)
			return;

		ptr->SetLoadingScreen(_screen);
	}

	BBox CalculateShadowBox(Light* _light)
	{
		BBox shadowbox;

		auto scene = GetScene();
		ASSERT(scene, "Invalid pointer on scene");

		if (scene)
		{
			for (size_t j = 0; j < scene->MeshesCount(); ++j)
			{
				{
					const BBox& box = scene->GetMeshById(j)->GetBBox();

					shadowbox.AddBBox(box);
				}
			}
		}
		else
			ErrorWrite(__FUNCTION__);

		return shadowbox;
	}

	/**
	*/
	ENGINE_API void API_SetAmbient(const Vec3 &_amb)
	{
		auto scene = GetScene();
		ASSERT(scene, "Not exist RenderPipeline currently");

		Shader_SetAmbient(_amb);
	}

	ENGINE_API Light* API_CreateLight(unsigned int _type)
	{
		Light* ptr = nullptr;

		auto type = (LightBufferData::LightType)_type;
		switch (type)
		{
		case LightBufferData::LIGHT_OMNI:
			ptr = new LightPoint();
			break;
		case LightBufferData::LIGHT_SPOT:
			ptr = new LightSpot();
			break;
			//!@todo DEPRECATED
		case LightBufferData::LIGHT_DIRECT:
			ptr = new LightDirect();
			break;
		case LightBufferData::LIGHT_ENVPROBE:
			ptr = new LightEnvProbe();
			break;
		case LightBufferData::LIGHT_AREA:
			ASSERT("[API_CreateLight] Not Implemented");
			break;
		default:
			Error("[API_CreateLight] Incorrect value", true);
		}
		return ptr;
	}
}