/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"

#include "Scene.h"
#include "Material.h"
#include "Serializer.h"
#include "Deserializer.h"

#include "EditorObjectManager.h"

#include <future>

namespace NGTech
{
	template<typename T>
	static constexpr ENGINE_INLINE void AsyncSave(const char* _filename, T _ptr)
	{
		std::async(std::launch::async, [](const char* _p, T _sc)
		{
			try
			{
				Serializer serializer(_p);
				serializer.SerializeObj(_sc, __FUNCTION__, __FILE__, __LINE__);
			}
			catch (std::exception&e)
			{
				Warning(e.what());
			}
		}, _filename, _ptr);
	}

	/***/
	void ENGINE_API API_SaveScene(const String & path)
	{
		auto ptr = GetScene();
		ASSERT(ptr, "Invalid pointer on scene");
		ASSERT(path.empty() == false, "Invalid path");
		if ((!ptr) || (path.empty()))
			return;

		AsyncSave<SceneManager*>(path.c_str(), ptr);
	}

	void ENGINE_API API_LoadSceneNew(const String & path, bool isEditor)
	{
		DebugM("[API_LoadSceneNew] Loading file scene - %s", path.c_str());
		// ������������� ��������� - ��� ��������� ���-�� (������� ����� �����)
		SetEngineState(STATE_LOADING);

		auto _scene = GetScene();
		ASSERT(_scene, "Invalid pointer on _scene");
		ASSERT(!path.empty(), "Invalid path");

		if ((!_scene) || (path.empty())) return;

		try
		{
			Deserializer deserializer(path.c_str());
			deserializer.DeserializeModify<SceneManager&>(*_scene);
		}

		catch (std::exception&e)
		{
			ErrorWrite("API_LoadSceneNew %s", e.what());
		}
		catch (...)
		{
			throw;
		}

		// Editor has own PauseController
		if (isEditor == false) {
			SetEngineState(EngineState::STATE_GAME);
			Warning("[API_LoadSceneNew] STATE_GAME");
		}
		else {
			SetEngineState(EngineState::STATE_EDITOR_PAUSED);
			Warning("[API_LoadSceneNew] STATE_EDITOR_PAUSED");
		}
	}

	/***/
	void ENGINE_API API_SaveMaterial(Material* ptr, const String & path)
	{
		ASSERT(ptr, "Provided invalid pointer on ptr");
		ASSERT(!path.empty(), "Invalid path");

		if ((!ptr) || (path.empty()))
			return;

		AsyncSave<Material*>(path.c_str(), ptr);
	}

	/***/
	void ENGINE_API API_SaveMaterial(const String & path)
	{
		ASSERT("Material save not implemented");
		//!@todo Check
		/*if ((!ptr) || (path.empty()))
			return;

		AsyncSave<Material*>(path.c_str(), ptr);*/

		//!@todo Get from global resource manager material with name(����� ������� � ��������� �� �����)
	}

	void Util_LoadMaterial(Material* ptr, const String & path)
	{
		DebugM("[Util_LoadMaterial] Loading material file - %s", path.c_str());

		ASSERT(ptr, "Provided invalid pointer on ptr");
		ASSERT(!path.empty(), "Invalid path");

		if ((!ptr) || (path.empty()))
		{
			ErrorWrite("[%s] Incorrect material path: %s or incorrect ptr", __FUNCTION__, path.c_str());
			return;
		}

		try
		{
			Deserializer deserializer(path);
			deserializer.DeserializeModify<Material&>(*ptr);
		}
		catch (std::exception&e)
		{
			Warning(e.what());
		}
	}

	//!@todo ��������� ������ ��� ������������� ��������
	//!@todo DEPRECATED: ���������� ������� ������� �� 1 ���������
	size_t ENGINE_API API_CountofObjectsOnScene()
	{
		auto scene = GetScene();
		ASSERT(scene, "Invalid pointer on scene");

		if (!scene)
			return 0;

		return scene->LightsCount() + scene->MeshesCount() + scene->SkeletalCount();
	}

#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
	void ENGINE_API API_EditorCheckWhatWasModificated()
	{
		auto objManager = GetEditorObjectManager();

		ASSERT(objManager, "Invalid pointer on objManager");
		if (!objManager)
			return;

		for (auto &obj : objManager->m_EditableObjects)
		{
			obj->EditorCheckWhatWasModificated();
		}
	}
#endif
}