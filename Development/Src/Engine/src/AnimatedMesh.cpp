/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
//**************************************
#include "Cache.h"
#include "Scene.h"
//**************************************
#include "SkinnedMesh.h"
//**************************************
#include "AnimatedMesh.h"
//**************************************

namespace NGTech {
	AnimatedMesh::AnimatedMesh(const String &_path) :ObjectMesh(_path) {
		LoadFromPath(_path);
	}

	void AnimatedMesh::LoadFromPath(const String &_path)
	{
		auto cache = GetCache();
		auto _mLoadingPath = "meshes/" + _path;

		this->SetLoadingPath(_path);

		tempdata.mesh.m_AnimatedMesh = Cache::LoadSkinnedMesh(_path);

		if (!tempdata.mesh.m_AnimatedMesh) {
			Error("Failed loading of model", true);
			return;
		}

		tempdata.mNumSubsets = tempdata.mesh.m_AnimatedMesh->GetNumSubsets();
		if (tempdata.mNumSubsets <= 0)
		{
			ErrorWrite("Invalid subsets count");
			return;
		}

		ASSERT((tempdata.mNumSubsets > 0), "Invalid subsets count");

		tempdata.materials.resize(tempdata.mNumSubsets);
		tempdata.OldMaterials.resize(tempdata.mNumSubsets);

		if (m_PhysicsEntity)
			m_PhysicsEntity->SetOriginalMeshNamePhysX(_mLoadingPath);

		// standard materials
		SetMaterial(DefaultResources::Material_Default_Name, "*");

		// ����� ����, ��� ��������� ���, ������������� �������� - �.� ��� ���������
		// ����������, ��� ����� ����� ��������
		CLASS_INIT_EDTOR_VARS();
	}

	AnimatedMesh::~AnimatedMesh() {
		this->_RemoveFromScene();
	}

	void AnimatedMesh::DrawSubset(size_t subset, TimeDelta _delta) {
		ASSERT(tempdata.mesh.m_AnimatedMesh, "Invalid pointer on mesh.m_AnimatedMesh");

		if (tempdata.mesh.m_AnimatedMesh == nullptr || GetEnabled() == false)
		{
			DebugM("Mesh %s, is not exist pointer or disabled", this->GetLoadingPath().c_str()); //-V111
			return;
		}

		this->tempdata.mesh.m_AnimatedMesh->DrawSubsetShared(subset);
		PlayAnimationUpdate(_delta);
	}

	const Vec3& AnimatedMesh::GetMax() {
		ASSERT(tempdata.mesh.m_AnimatedMesh, "Invalid pointer on mesh.m_AnimatedMesh");

		this->tempdata.Max = tempdata.mesh.m_AnimatedMesh->bBox.maxes;
		return this->tempdata.Max;
	}

	const Vec3& AnimatedMesh::GetMin() {
		ASSERT(tempdata.mesh.m_AnimatedMesh, "Invalid pointer on mesh.m_AnimatedMesh");

		this->tempdata.Min = tempdata.mesh.m_AnimatedMesh->bBox.mins;
		return this->tempdata.Min;
	}

	const BBox& AnimatedMesh::GetBBox() {
		ASSERT(tempdata.mesh.m_AnimatedMesh, "Invalid pointer on mesh.m_AnimatedMesh");

		this->tempdata.Box = tempdata.mesh.m_AnimatedMesh->bBox;
		return this->tempdata.Box;
	}

	const Vec3& AnimatedMesh::GetCenter() {
		ASSERT(tempdata.mesh.m_AnimatedMesh, "Invalid pointer on mesh.m_AnimatedMesh");
		this->tempdata.Center = tempdata.mesh.m_AnimatedMesh->bSphere.center;
		return this->tempdata.Center;
	}

	float AnimatedMesh::GetRadius() {
		ASSERT(tempdata.mesh.m_AnimatedMesh, "Invalid pointer on mesh.m_AnimatedMesh");
		this->tempdata.Radius = tempdata.mesh.m_AnimatedMesh->bSphere.radius;
		return this->tempdata.Radius;
	}

	const Vec3& AnimatedMesh::GetMax(unsigned int s) {
		ASSERT(tempdata.mesh.m_AnimatedMesh, "Invalid pointer");
		ASSERT(tempdata.mesh.m_AnimatedMesh->GetSubset(s), "Invalid pointer");

		this->tempdata.Max = tempdata.mesh.m_AnimatedMesh->GetSubset(s)->bBox.maxes;
		return this->tempdata.Max;
	}

	const Vec3& AnimatedMesh::GetMin(unsigned int s)
	{
		ASSERT(tempdata.mesh.m_AnimatedMesh, "Invalid pointer");
		ASSERT(tempdata.mesh.m_AnimatedMesh->GetSubset(s), "Invalid pointer");

		this->tempdata.Min = tempdata.mesh.m_AnimatedMesh->GetSubset(s)->bBox.mins;
		return this->tempdata.Min;
	}

	const Vec3& AnimatedMesh::GetCenter(unsigned int s)
	{
		ASSERT(tempdata.mesh.m_AnimatedMesh, "Invalid pointer");
		ASSERT(tempdata.mesh.m_AnimatedMesh->GetSubset(s), "Invalid pointer");

		this->tempdata.Center = tempdata.mesh.m_AnimatedMesh->GetBSphere(s).center;
		return this->tempdata.Center;
	}

	float AnimatedMesh::GetRadius(unsigned int s)
	{
		ASSERT(tempdata.mesh.m_AnimatedMesh, "Invalid pointer");
		ASSERT(tempdata.mesh.m_AnimatedMesh->GetSubset(s), "Invalid pointer");

		this->tempdata.Radius = tempdata.mesh.m_AnimatedMesh->GetBSphere(s).radius;
		return this->tempdata.Radius;
	}

	/**
	*/
	bool AnimatedMesh::RenderQuery(I_RenderLowLevel* _render, size_t _index, TimeDelta _delta)
	{
		ASSERT(_render, "_render pointer not exist");

		if (!this->HasQuery()) {
			this->RenderFast(_render, _index, _delta);
			return false;
		}
		if (this->HasQuery() == 0) {
			return false;
		}

		if (!tempdata.query)
			tempdata.query = _render->GetOQ();

		_render->DepthMask(false);
		_render->ColorMask(false, false, false, false);

		{
			tempdata.query->BeginQuery();
			{
				//!@todo Replace on BBOX Mesh Render
				this->RenderFast(_render, _index, _delta);
			}
			tempdata.query->EndQuery();
		}

		_render->DepthMask(true);
		_render->ColorMask(true, true, true, true);

		tempdata.query->BeginConditionalRender();
		{
			this->RenderFast(_render, _index, _delta);
		}
		tempdata.query->EndConditionalRender();

		return true;
	}

	/**
	*/
	bool AnimatedMesh::RenderFast(I_RenderLowLevel* _render, size_t _index, TimeDelta _delta) {
		this->DrawSubset(_index, _delta);
		return true;
	}

	/**
	*/
	void AnimatedMesh::SetMaterial(const String &path, const String &name) {
		auto _cache = GetCache();
		ASSERT(_cache, "Invalid pointer on cache");

		ASSERT(tempdata.mesh.m_AnimatedMesh, "Invalid pointer");

		Material* material = Cache::LoadMaterial(path);
		ASSERT(material, "Invalid pointer on material");
		if (!material) return;

		if (name == "*")
		{
			for (auto&mat : tempdata.materials)
			{
				mat.material = material;
			}
		}

		tempdata.materials[tempdata.mesh.m_AnimatedMesh->GetSubset(name)].material = material;
	}

	/**
	*/
	const BBox& AnimatedMesh::GetBBox(unsigned int s)
	{
		ASSERT(tempdata.mesh.m_AnimatedMesh, "Invalid pointer");
		if (!tempdata.mesh.m_AnimatedMesh) return tempdata.Box;

		this->tempdata.Box = tempdata.mesh.m_AnimatedMesh->GetBBox(s);

		return this->tempdata.Box;
	}

	/**
	*/
	const BSphere& AnimatedMesh::GetBSphere(unsigned int s)
	{
		ASSERT(tempdata.mesh.m_AnimatedMesh, "Invalid pointer");
		if (!tempdata.mesh.m_AnimatedMesh) return tempdata.Sphere;
		this->tempdata.Sphere = tempdata.mesh.m_AnimatedMesh->GetBSphere(s);

		return this->tempdata.Sphere;
	}

	/**
	*/
	Material* AnimatedMesh::GetMaterial(unsigned int subset) const {
		ASSERT(tempdata.mesh.m_AnimatedMesh, "Invalid pointer");
		return tempdata.materials[subset].material;
	}

	/**
	*/
	void AnimatedMesh::SetAnimationFrame(int frame, int from, int to) {
		ASSERT(tempdata.mesh.m_AnimatedMesh, "Invalid pointer");
		if (!tempdata.mesh.m_AnimatedMesh) return;

		tempdata.mesh.m_AnimatedMesh->SetFrame(frame, from, to);
	}

	void AnimatedMesh::_AddToSceneList() {
		auto scene_ptr = GetScene();
		if (scene_ptr)
			scene_ptr->AddSkeletal(this);
		else
			ASSERT(scene_ptr, "Invalid pointer");

		BaseEntity::_AddToSceneList();
	}

	void AnimatedMesh::_RemoveFromScene()
	{
		if (m_PhysicsEntity)
			m_PhysicsEntity->SetPhysicsNone();

		auto cache = GetCache();

		if (tempdata.mesh.m_AnimatedMesh)
			Cache::DeleteResource(tempdata.mesh.m_AnimatedMesh);
		for (const auto& mat : tempdata.materials)
			Cache::DeleteResource(mat.material);

		// �������������� �������
		DeleteMatFromList(tempdata.materials, cache);
		DeleteMatFromList(tempdata.OldMaterials, cache);

		tempdata.materials.clear();
		tempdata.OldMaterials.clear();

		_RemoveFromSceneAnimatedMesh();
	}

	void AnimatedMesh::_RemoveFromSceneAnimatedMesh()
	{
		auto ptr = GetScene();
		if (ptr)
			ptr->DeleteSkeletalFromList(this);
		else
		{
			ASSERT(ptr, "Not exist scene pointer");
		}
		UpdatableObject::_RemoveFromScene();
	}

	void AnimatedMesh::PlayAnimationUpdate(TimeDelta _dtime) {
		//!@todo CHECK - WTF!? - 150!?
		//Math::ONEDELTA / TimeDelta(UpdateConfig::Animation)*_dtime
		static unsigned int i = 0;

		if (i > 150)
			i = 0;

		mIsPlaying = true;

		//!@todo check double on delta(use function "compare float")

		if (_dtime > Math::ZERODELTA)
			this->SetAnimationFrame(i);
		else
			Warning("[%s] Invalid delta time", __FUNCTION__);

		i++;
	}
}