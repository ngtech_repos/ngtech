/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
//**************************************
#include "ALSystem.h"
//**************************************

namespace NGTech
{
	// multi-channel formats
	unsigned int AudioExt::EXT_AL_FORMAT_QUAD16 = 0;
	unsigned int AudioExt::EXT_AL_FORMAT_51CHN16 = 0;
	unsigned int AudioExt::EXT_AL_FORMAT_61CHN16 = 0;
	unsigned int AudioExt::EXT_AL_FORMAT_71CHN16 = 0;

	bool AudioExt::Init()
	{
		// multi-channel formats
		EXT_AL_FORMAT_QUAD16 = alGetEnumValue("AL_FORMAT_QUAD16");
		EXT_AL_FORMAT_51CHN16 = alGetEnumValue("AL_FORMAT_51CHN16");
		EXT_AL_FORMAT_61CHN16 = alGetEnumValue("AL_FORMAT_61CHN16");
		EXT_AL_FORMAT_71CHN16 = alGetEnumValue("AL_FORMAT_71CHN16");

		return true;
	}

	/**
	*/
	void AudioExt::ReportStatusExtensions() {
		// sound formats
		if (EXT_AL_FORMAT_QUAD16) LogPrintf("[Audio Extension] Supported extension QUAD16");
		if (EXT_AL_FORMAT_51CHN16) LogPrintf("[Audio Extension] Supported extension 51CHN16");
		if (EXT_AL_FORMAT_61CHN16) LogPrintf("[Audio Extension] Supported extension 61CHN16");
		if (EXT_AL_FORMAT_71CHN16) LogPrintf("[Audio Extension] Supported extension 71CHN16");
	}

	int AudioExt::CheckExtension(ALCdevice* _device, const char* _name) {
		return alcIsExtensionPresent(_device, _name);
	}

	/**
	*/
	int AudioExt::CheckErrors()
	{
		int ret = 0;
		while (CheckErrors(alGetError())) ret = 1;
		return ret;
	}

	int AudioExt::CheckErrors(int _result)
	{
		if (_result == AL_NO_ERROR) return 0;

		if (_result == AL_INVALID_NAME) ErrorWrite("OpenAL error: invalid name\n");
		else if (_result == AL_INVALID_ENUM) ErrorWrite("OpenAL error: invalid enum\n");
		else if (_result == AL_INVALID_VALUE) ErrorWrite("OpenAL error: invalid value\n");
		else if (_result == AL_INVALID_OPERATION) ErrorWrite("OpenAL error: invalid operation\n");
		else if (_result == AL_OUT_OF_MEMORY) ErrorWrite("OpenAL error: out of memory\n");
		else ErrorWrite("OpenAL error: 0x%04X\n", _result);

		return 1;
	}

	/**
	*/
	int AudioExt::CheckContextErrors(ALCdevice*_device) {
		int ret = 0;
		while (CheckContextErrors(alcGetError(_device))) ret = 1;
		return ret;
	}

	int AudioExt::CheckContextErrors(int result)
	{
		if (result == ALC_NO_ERROR) return 0;

		if (result == ALC_INVALID_DEVICE) ErrorWrite("OpenAL context error: invalid device\n");
		else if (result == ALC_INVALID_CONTEXT) ErrorWrite("OpenAL context error: invalid context\n");
		else if (result == ALC_INVALID_ENUM) ErrorWrite("OpenAL context error: invalid enum\n");
		else if (result == ALC_INVALID_VALUE) ErrorWrite("OpenAL context error: invalid value\n");
		else if (result == ALC_OUT_OF_MEMORY) ErrorWrite("OpenAL context error: out of memory\n");
		else ErrorWrite("OpenAL context error: 0x%04X\n", result);

		return 1;
	}
}