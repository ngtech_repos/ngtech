/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"

#include "Cache.h"

#include "PhysBody.h"
#include "BasePhysicsObjectEntity.h"

namespace NGTech {

	void PhysicsObjectData::SetPhysicsNone() {
		// Only this check here, without asserts for when actor not exist collision
		if (m_AttachedPBody == nullptr) return;

		if (bMultipart) {
			Debug("Delete Multipart");
			SAFE_DELETE_ARRAY(m_AttachedPBody);
			bMultipart = false;
		}
		else {
			SAFE_DELETE(m_AttachedPBody);
		}

		this->m_PhysDesc.CleanResources();
	}

	void PhysicsObjectData::SetPhysXBodyTransform(const Mat4 & _trans) {
		ASSERT(m_AttachedPBody, "Invalid pointer on m_AttachedPBody");
		if (m_AttachedPBody == nullptr) return;
		m_AttachedPBody->SetTransform(_trans);
	}
}