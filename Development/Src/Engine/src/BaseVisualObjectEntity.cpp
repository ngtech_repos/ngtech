/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
#include "BaseVisualObjectEntity.h"

#include "Material.h"
#include "Cache.h"
#include "Scene.h"

namespace NGTech
{
	MaterialSubset::~MaterialSubset()
	{
		//!@todo CHECK
		// ��� ��������� ����� �������, ��������� � �������� ������ ����� �������������� � ������ �����
		//auto _cache = GetCache();
		//if (_cache && _cache->_useCacheSystem)
		//{
		//	Debug("Deletion material with subset");
		//	_cache->DeleteMaterial(material);
		//}
		//else
		//{
		//	ErrorWrite("[%s] Not exist cache system for material: %s, subsetName:%s ", __FUNCTION__, materialName.c_str(), subsetName.c_str());
		//}

		//subsetName.clear();
	}

	void DeleteMatFromList(std::vector<MaterialSubset> &materials, const std::shared_ptr<Cache> &_cache)
	{
		ASSERT(_cache, "Invalid pointer on _cache");
		if (!_cache) return;

		for (auto& mat : materials)
		{
			if (mat.material == nullptr)
			{
				Warning("Not exist pointer on mat.material");
				continue;
			}

			SAFE_RELEASE(mat.material)
		}
		materials.clear();
	}

	void BaseVisualObjectEntity::_AddToScene() { auto scene = GetScene(); ASSERT(scene, "Invalid scene"); scene->AddObject(this); }
	void BaseVisualObjectEntity::_DeleteFromScene() { auto scene = GetScene(); ASSERT(scene, "Invalid scene"); scene->DeleteObject(this); }

#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
	//!@todo ���� ������� ������� ������� ������ ������� �� �����������
	void BaseVisualObjectEntity::EditorCheckWhatWasModificated(void) {}
	void BaseVisualObjectEntity::_InitEditorVars(void) {}
	void BaseVisualObjectEntity::ReInitEditorVars(void) {}

	void BaseVisualObjectEntity::_AddEditorVars() {
		//MT::ScopedGuard guard(_bv_mutex);
		AddEditorVarsTo(m_EditorVars);
	}

	void BaseVisualObjectEntity::AddEditorVarsTo(std::vector<EditorVar>&_to) {
		//MT::ScopedGuard guard(_bv_mutex);
		_to.push_back(EditorVar("Layer", &tempdata.mRenderLayer, EditorVar::UNINT, "Rendering", "Used for render-priority:\
					0-SkyBox and etc.,\
					1-Default layer,\
					2-7-Engine Layers,\
					8-14-User Layers"));

		// Adds slots for materials
		try {
			for (unsigned int i = 0; i < tempdata.mNumSubsets; i++)
			{
				auto name = GetSubsetName(i);
				if (tempdata.materials[i].material == nullptr || name.empty())
				{
					Warning("[%s] not exist pointer on material or name is empty. subset number: %i", __FUNCTION__, i);
					continue;
				}

				tempdata.materials[i].subsetName = name;
				tempdata.materials[i].materialName = tempdata.materials[i].material->GetPath();

				_to.push_back(EditorVar(tempdata.materials[i].subsetName, &tempdata.materials[i].materialName, EditorVar::MATERIAL, "Materials", "Selection of material for preset"));
			}
		}
		catch (std::exception&e)
		{
			ErrorWrite(e.what());
		}
	}
#endif
}