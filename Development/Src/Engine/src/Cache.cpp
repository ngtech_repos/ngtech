/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"

//**************************************
#include <future>
#include <memory>
//**************************************
#include "Engine.h"
#include "Cache.h"
#include "Log.h"
#include "CvarManager.h"
#include "Material.h"
//**************************************
#include "../../Platform/inc/concurrentqueue/concurrentqueue.h"
#include "../../Platform/inc/concurrentqueue/blockingconcurrentqueue.h"
//**************************************

namespace NGTech {

	ALSound* Cache::LoadSound(const String& _path)
	{
		auto _cache = GetCache();
		if (_cache && _cache->m_useCacheSystem) return _cache->_LoadSoundUtil(_path);

		return new ALSound(_path, true);
	}

	Mesh* Cache::LoadModel(const String& _path)
	{
		auto _cache = GetCache();
		if (_cache && _cache->m_useCacheSystem) return _cache->_LoadModelUtil(_path);

		return new Mesh(_path, true);
	}

	SkinnedMesh * Cache::LoadSkinnedMesh(const String & _path)
	{
		auto _cache = GetCache();
		if (_cache && _cache->m_useCacheSystem) return _cache->_LoadSkinnedMeshUtil(_path);
		return new SkinnedMesh(_path, true);
	}


	I_Shader* Cache::LoadShader(const String& _path, const String& _defines)
	{
		auto _cache = GetCache();
		if (_cache && _cache->m_useCacheSystem)
			return _cache->_LoadShaderUtil(_path, _defines);

		LogPrintf("Loading shader without cache");
		auto _render = GetRender();
		ASSERT(_render, "Not exist _render pointer");

		if (!_render) return nullptr;

		return _render->ShaderCreate(_path, _defines);
	}

	void Cache::ReloadShaders() {
		String defines = "";

		// TODO: get defines from cvars
		// TODO: Implement on multithreading, in opengl - singlethread only
		for (const auto &all : shaders._CacheContainer)
		{
			auto ptr = all.second;
			if (!ptr) continue;

			ptr->Recompile(ptr->defines, defines);

		}
	}

	void Cache::ReloadTextures()
	{
		textures.LoadImmedly();
	}

	void Cache::ReloadMeshes()
	{
		models.LoadImmedly();
	}

	void Cache::ReloadAnimatedMeshes()
	{
		skinnedmodels.LoadImmedly();
	}

	void Cache::ReloadSounds()
	{
		sounds.LoadImmedly();
	}

	void Cache::ReloadAll()
	{
		ReloadShaders();
		ReloadTextures();
		ReloadMeshes();
		ReloadAnimatedMeshes();
		ReloadSounds();
	}

	void Cache::DestroyAllUnUsedResources()
	{
		models.DestroyAllUnUsedResources();
		skinnedmodels.DestroyAllUnUsedResources();
		sounds.DestroyAllUnUsedResources();
		materials.DestroyAllUnUsedResources();
		textures.DestroyAllUnUsedResources();
		shaders.DestroyAllUnUsedResources();
	}

	void Cache::DestroyAllResources()
	{
		models.Clean();
		skinnedmodels.Clean();
		sounds.Clean();
		materials.Clean();
		textures.Clean();
		shaders.Clean();
	}

	ALSound* Cache::_LoadSoundUtil(const String& _path) {
		return sounds.LoadObj(_path, nullptr, nullptr, false);
	}

	I_Shader* Cache::_LoadShaderUtil(const String &_path, const String& _defines) {
		return shaders.LoadObj(_path, _defines, /*DefaultResources::DefaultShader*/nullptr, false);
	}

	Mesh* Cache::_LoadModelUtil(const String& _path) {
		return models.LoadObj(_path, nullptr, static_cast<void*>(DefaultResources::ErrorMesh), false);
	}

	SkinnedMesh* Cache::_LoadSkinnedMeshUtil(const String& _path) {
		return skinnedmodels.LoadObj(_path, nullptr, static_cast<void*>(DefaultResources::AnimatedErrorMesh), false);
	}

	I_Texture* Cache::_LoadTextureUtil(const std::shared_ptr<TextureParamsTransfer>&_obj)
	{
		// TODO: implement on DefaultResources::DefaultTexture
		ASSERT(_obj, "Invalid pointer on _obj");
		if (!_obj) return nullptr;

		return textures.LoadObj(_obj->path, _obj, /*DefaultResources::DefaultTexture*/nullptr, false);
	}

	void Cache::DeleteResource(SkinnedMesh* _mesh)
	{
		if (!_mesh) return;

		auto _cache = GetCache();
		if (_cache && _cache->m_useCacheSystem)
			_cache->skinnedmodels.DeleteResource(_mesh);
		else
			SAFE_RELEASE(_mesh);
	}

	void Cache::DeleteResource(Mesh* _mesh)
	{
		if (!_mesh) return;
		auto _cache = GetCache();
		if (_cache && _cache->m_useCacheSystem)
			_cache->models.DeleteResource(_mesh);
		else
			SAFE_RELEASE(_mesh);
	}

	void Cache::DeleteResource(Material* _material)
	{
		if (!_material) return;

		auto _cache = GetCache();
		if (_cache && _cache->m_useCacheSystem)
			_cache->materials.DeleteResource(_material);
		else
			SAFE_RELEASE(_material);
	}


	template<>
	ENGINE_INLINE Material*
		CacheNode<Material*, void*, CacheReleaseCallback<Material*>>::LoadObj(const String & _path, void * _userData, void * _defaultResource, bool _LoadImmedly) {
		ASSERT(_defaultResource, "[Material] Invalid pointer on _defaultResource");
		if (!_defaultResource) return nullptr;

		auto cast = static_cast<Material*>(_defaultResource);
		ASSERT(cast, "Failed cast");
		if (!cast) return nullptr;

		if (_path.empty()) {
			ASSERT(!_path.empty(), "Provied empty path for resource. Can't load, will use default resource");
			cast->AddRef();
			return cast;
		}

		if (_path == "INVALID_PATH")
		{
			Warning("Invalid path requested. Can't load, will use default resource");
			cast->AddRef();
			return cast;
		}

		auto it = _CacheContainer.find(_path);

		if (it == _CacheContainer.end() || it->second == nullptr)
		{
			auto obj = new Material(_path, false, false);
			if (_LoadImmedly == false)
				AddToDequeDeferredLoading(obj);
			else
				AddToDequeImmedlyLoading(obj);

			_CacheContainer.insert(std::unordered_map<String, Material*>::value_type(_path, obj));
			return obj;
		}

		it->second->AddRef();
		return it->second;
	}

	Material* Cache::LoadMaterial(const String& _path) {
		ASSERT(!_path.empty(), "Invalid path name (empty) provided");
		/*ASSERT(DefaultResources::DefaultMaterial, "Invalid pointer");*/

		/*auto _cast = static_cast<Material*>(DefaultResources::DefaultMaterial);
		ASSERT(_cast, "Invalid cast");

		if (!_cast) return nullptr;*/

		//if (_path.empty()) return _cast;

		auto cache = GetCache();

		ASSERT(cache, "Invalid pointer on cache system");

		if (cache && cache->m_useCacheSystem) {
			return cache->materials.LoadObj(_path, nullptr, static_cast<void*>(DefaultResources::DefaultMaterial), false);
		}
		else // TODO: �������� �� ������� ��������� - ����������� �� ���� ��� ���, ���� ����������� ������ ��������� � �������
			return new Material(_path, false, true);
	}

	I_Texture* Cache::LoadTexture(const std::shared_ptr<TextureParamsTransfer>&_obj) {
		ASSERT(_obj, "Invalid pointer on _obj");
		if (!_obj) return nullptr;

		auto cache = GetCache();
		ASSERT(cache, "Invalid pointer on cache system");

		if (cache && cache->m_useCacheSystem) {
			return cache->_LoadTextureUtil(_obj);
		}
		else {
			auto _render = GetRender();
			ASSERT(_render, "Invalid pointer on _render");
			if (!_render) return nullptr;

			return _render->TextureCreate(_obj);
		}

		return nullptr;
	}

	void Cache::DeleteResource(I_Texture* _texture)
	{
		if (!_texture) return;

		auto _cache = GetCache();
		if (_cache && _cache->m_useCacheSystem)
			_cache->textures.DeleteResource(_texture);
		else
		{
			LogPrintf("Delete texture from material from pass without caching");
			SAFE_RELEASE(_texture);
		}
	}

	void Cache::DeleteResource(I_Shader* _shader)
	{
		if (!_shader) return;

		auto _cache = GetCache();
		if (_cache && _cache->m_useCacheSystem)
			_cache->shaders.DeleteResource(_shader);
		else
			SAFE_RELEASE(_shader);
	}
}