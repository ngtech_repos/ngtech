#include "EnginePrivate.h"

#include "Cache.h"

namespace NGTech
{

	//TODO: implement 
	template<>
	ENGINE_INLINE I_Shader*
		CacheNode<I_Shader*, const String &, CacheReleaseCallback<I_Shader*>>::LoadObj(const String & _path, const String & _defines, void * _defaultResource, bool _LoadImmedly)
	{
		//ASSERT(_defaultResource, "Invalid pointer on _defaultResource");
		//if (!_defaultResource) return nullptr;

		//auto cast = static_cast<I_Shader*>(_defaultResource);
		//ASSERT(cast, "Failed cast");
		//if (!cast) return nullptr;

		ASSERT(!_path.empty(), "Provied empty path for resource. Can't load, will use default resource");

		if (_path.empty()) {
			//cast->AddRef();
			//return cast;
			return nullptr;
		}

		if (_path == "INVALID_PATH")
		{
			Warning("Invalid path requested. Can't load, will use default resource");
			//cast->AddRef();
			//return cast;
			return nullptr;
		}

		auto it = _CacheContainer.find(_path);
		if (it == _CacheContainer.end() || it->second == nullptr) {
			String defines = _defines;

			//!@todo: Add define with selected QUALITY_PRESET(EXTREAME, LOW and ETC)

			auto _render = GetRender();
			ASSERT(_render, "Invalid pointer on _render");
			if ((_render == nullptr) || (_render && _render->GetRenderName() == "NULLDrv"))
				//return cast;
				return nullptr;

			// todo: add call on deferred call
			auto obj = _render->ShaderCreate(_path, defines);
			if (obj == nullptr) {
				Warning("[%s] Failed creation of shader on path: %s", _path.c_str());
				//return cast;
				return nullptr;
			}

			obj->AddDefines(defines);

			/*TODO: uncomment*/
			/*if (_LoadImmedly == false)
				AddToDequeDeferredLoading(obj);
			else
				AddToDequeImmedlyLoading(obj);*/

			_CacheContainer.insert(std::unordered_map<String, I_Shader*>::value_type(_path, obj));

			return obj;
		}

		it->second->AddRef();
		return it->second;

	}

	template<>
	ENGINE_INLINE SkinnedMesh*
		CacheNode<SkinnedMesh*, void*, CacheReleaseCallback<SkinnedMesh*>>::LoadObj(const String & _path, void * _userData, void * _defaultResource, bool _LoadImmedly) {
		ASSERT(_defaultResource, "[SkinnedMesh] Invalid pointer on _defaultResource");
		if (!_defaultResource) return nullptr;

		auto cast = static_cast<SkinnedMesh*>(_defaultResource);
		ASSERT(cast, "Failed cast");
		if (!cast) return nullptr;

		ASSERT(!_path.empty(), "Provied empty path for resource. Can't load, will use default resource");

		if (_path.empty()) {
			cast->AddRef();
			return cast;
		}

		if (_path == "INVALID_PATH")
		{
			Warning("Invalid path requested. Can't load, will use default resource");
			cast->AddRef();
			return cast;
		}

		auto it = _CacheContainer.find(_path);

		if (it == _CacheContainer.end() || it->second == nullptr)
		{
			auto obj = new SkinnedMesh(_path, false);
			if (_LoadImmedly == false)
				AddToDequeDeferredLoading(obj);
			else
				AddToDequeImmedlyLoading(obj);

			_CacheContainer.insert(std::unordered_map<String, SkinnedMesh*>::value_type(_path, obj));
			return obj;
		}

		it->second->AddRef();
		return it->second;
	}

	template<>
	ENGINE_INLINE Mesh*
		CacheNode<Mesh*, void*, CacheReleaseCallback<Mesh*>>::LoadObj(const String & _path, void * _userData, void * _defaultResource, bool _LoadImmedly) {
		ASSERT(_defaultResource, "[Mesh] Invalid pointer on _defaultResource");
		if (!_defaultResource) return nullptr;

		auto cast = static_cast<Mesh*>(_defaultResource);
		ASSERT(cast, "Failed cast");
		if (!cast) return nullptr;

		ASSERT(!_path.empty(), "Provied empty path for resource. Can't load, will use default resource");

		if (_path.empty()) {
			cast->AddRef();
			return cast;
		}

		if (_path == "INVALID_PATH")
		{
			Warning("Invalid path requested. Can't load, will use default resource");
			cast->AddRef();
			return cast;
		}

		auto it = _CacheContainer.find(_path);

		if (it == _CacheContainer.end() || it->second == nullptr)
		{
			auto obj = new Mesh(_path, false);
			if (_LoadImmedly == false)
				AddToDequeDeferredLoading(obj);
			else
				AddToDequeImmedlyLoading(obj);

			_CacheContainer.insert(std::unordered_map<String, Mesh*>::value_type(_path, obj));
			return obj;
		}

		it->second->AddRef();
		return it->second;
	}

	template<>
	ENGINE_INLINE ALSound*
		CacheNode<ALSound*, void*, CacheReleaseCallback<ALSound*>>::LoadObj(const String & _path, void* _desc/*can be null. not used*/, void * _defaultResource, bool _LoadImmedly) {
		ASSERT(_defaultResource, "[ALSound] Invalid pointer on _defaultResource");
		if (!_defaultResource) return nullptr;

		auto cast = static_cast<ALSound*>(_defaultResource);
		ASSERT(cast, "Failed cast");
		if (!cast) return nullptr;

		ASSERT(!_path.empty(), "Provied empty path for resource. Can't load, will use default resource");

		if (_path.empty()) {
			cast->AddRef();
			return cast;
		}

		if (_path == "INVALID_PATH")
		{
			Warning("Invalid path requested. Can't load, will use default resource");
			cast->AddRef();
			return cast;
		}

		auto it = _CacheContainer.find(_path);

		if (it == _CacheContainer.end() || it->second == nullptr)
		{
			ALSound* obj = new ALSound(_path, false);

			if (_LoadImmedly == false)
				AddToDequeDeferredLoading(obj);
			else
				AddToDequeImmedlyLoading(obj);

			_CacheContainer.insert(std::unordered_map<String, ALSound*>::value_type(_path, obj));
			return obj;
		}

		it->second->AddRef();
		return it->second;
	}

	template<>
	ENGINE_INLINE I_Texture*
		CacheNode<I_Texture*, const std::shared_ptr<TextureParamsTransfer>&, CacheReleaseCallback<I_Texture*>>::LoadObj(const String & _path, const std::shared_ptr<TextureParamsTransfer>& texP, void * _defaultResource, bool _LoadImmedly) {
		// TODO: uncomment this
		//ASSERT(_defaultResource, "[I_Texture] Invalid pointer on _defaultResource");
		//if (!_defaultResource) return nullptr;

		//auto cast = static_cast<I_Texture*>(_defaultResource);
		//ASSERT(cast, "Failed cast");
		//if (!cast) return nullptr;

		ASSERT(!_path.empty(), "Provied empty path for resource. Can't load, will use default resource");

		if (_path.empty()) {
			//cast->AddRef();
			return /*cast*/nullptr;
		}

		if (_path == "INVALID_PATH")
		{
			Warning("Invalid path requested. Can't load, will use default resource");
			//cast->AddRef();
			return nullptr;
		}

		ASSERT(texP, "Provided incorrect TextureParamsTransfer");
		if (!texP) return nullptr;

		auto render = GetRender();
		ASSERT(render, "Invalid render pointer");
		if (render == nullptr) return nullptr;

		auto it = _CacheContainer.find(_path);

		if (it == _CacheContainer.end() || it->second == nullptr)
		{
			if ((texP->path != _path) && (!_path.empty()))
				texP->path = _path;

			I_Texture* obj = render->TextureCreate(texP);
			ASSERT(obj, "Invalid creation texture");
			if (!obj) return nullptr;

			if (_LoadImmedly == false)
				AddToDequeDeferredLoading(obj);
			else
				AddToDequeImmedlyLoading(obj);

			_CacheContainer.insert(std::unordered_map<String, I_Texture*>::value_type(_path, obj));
			return obj;
		}

		it->second->AddRef();
		return it->second;
	}

#if 0
	template<>
	ENGINE_INLINE Material*
		CacheNode<Material*, void*, CacheReleaseCallback<Material*>>::LoadObj(const String & _path, void * _userData, void * _defaultResource, bool _LoadImmedly) {
		ASSERT(_defaultResource, "[Material] Invalid pointer on _defaultResource");
		if (!_defaultResource) return nullptr;

		auto cast = static_cast<Material*>(_defaultResource);
		ASSERT(cast, "Failed cast");
		if (!cast) return nullptr;

		if (_path.empty()) {
			ASSERT(!_path.empty(), "Provied empty path for resource. Can't load, will use default resource");
			cast->AddRef();
			return cast;
		}

		if (_path == "INVALID_PATH")
		{
			Warning("Invalid path requested. Can't load, will use default resource");
			cast->AddRef();
			return cast;
		}

		auto it = _CacheContainer.find(_path);

		if (it == _CacheContainer.end() || it->second == nullptr)
		{
			auto obj = new Material(_path, false, false);
			if (_LoadImmedly == false)
				AddToDequeDeferredLoading(obj);
			else
				AddToDequeImmedlyLoading(obj);

			_CacheContainer.insert(std::unordered_map<String, Material*>::value_type(_path, obj));
			return obj;
		}

		it->second->AddRef();
		return it->second;
	}
#else
	//template<> Material* CacheNode<Material*, void*, CacheReleaseCallback<Material*>>::LoadObj(const String& _path, void* _userData, void* _defaultResource, bool _loadImmdly)
	//{
	//	return nullptr;
	//}
#endif

	//template<class T, class Desc, class _Callback>
	//ENGINE_INLINE T CacheNode<T, Desc, _Callback>::LoadObj(const String & _path, Desc _userData, void * _defaultResource, bool _LoadImmedly)
	//{
	//	ASSERT(_defaultResource, "Invalid pointer on _defaultResource");
	//	if (!_defaultResource) return nullptr;

	//	auto cast = static_cast<T>(_defaultResource);
	//	ASSERT(cast, "Failed cast");
	//	if (!cast) return nullptr;

	//	if (_path.empty()) {
	//		ASSERT(!_path.empty(), "Provied empty path for resource. Can't load, will use default resource");
	//		cast->AddRef();
	//		return cast;
	//	}

	//	if (_path == "INVALID_PATH")
	//	{
	//		Warning("Invalid path requested. Can't load, will use default resource");
	//		cast->AddRef();
	//		return cast;
	//	}

	//	auto it = _CacheContainer.find(_path);

	//	if (it == _CacheContainer.end() || it->second == nullptr)
	//	{
	//		T* obj = new T(_path, false);
	//		if (_LoadImmedly == false)
	//			AddToDequeDeferredLoading(obj);
	//		else
	//			AddToDequeImmedlyLoading(obj);

	//		_CacheContainer.insert(std::unordered_map<String, T>::value_type(_path, obj));
	//		return obj;
	//	}

	//	it->second->AddRef();
	//	return it->second;
	//}

}