/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
//**************************************
#include "Camera.h"
#include "Scene.h"
#include "Frustum.h"
//**************************************

namespace NGTech {
	Camera::Camera(bool _automaticSwitch) {
		if (GetScene() && _automaticSwitch)
			GetScene()->m_ActiveScene.SetCameraAndDestroyOlder(this);

		this->mState.upVector = Vec3(Math::ZEROFLOAT, Math::ONEFLOAT, Math::ZEROFLOAT);
		this->_RecalculateView();

		this->fov = 45.0f;

		auto cvars = GetCvars();
		bufferData.aspectRatio = cvars->r_width / cvars->r_height;

		bufferData.clipDistance = Vec2(1e4, 0.1);

		// Eiler Angles, used in CameraFPS, CameraFree
		this->angle[0] = 0;
		this->angle[1] = 0;

		this->_RecalculateProjection();

		this->frustum = new Frustum();
		this->_RecalculateFrustum();

#ifndef DROP_EDITOR
		// ������� ����������
		GetStatistic()->AddCamerasCount();
#endif
	}

	Camera::~Camera() {
		Debug("Destroying Camera");

#ifndef DROP_EDITOR
		// ������� ����������
		GetStatistic()->ReduceCamerasCount();
#endif
		SAFE_DELETE(frustum);
	}

	const Mat4& Camera::GetTransformForChange() {
		//MT::ScopedGuard guard(mutex);
		bufferData._position = BaseEntity::GetPosition();
		transform = Mat4::lookAt(bufferData._position, bufferData._position + bufferData.direction, Math::Y_AXIS);
		return transform;
	}

	Mat4 Camera::GetTransform() {
		return GetTransformForChange();
	}

	void Camera::_RecalculateProjection(const Vec2&_v) {
		//MT::ScopedGuard guard(mutex);
		this->bufferData.clipDistance = _v;
		_RecalculateProjection();
	}

	void Camera::_RecalculateProjection() {
		//MT::ScopedGuard guard(mutex);
		this->bufferData.projMatrix = Mat4::perspective(fov, bufferData.aspectRatio, bufferData.clipDistance.y, bufferData.clipDistance.x);
		this->bufferData._bakedData = Vec4(bufferData.clipDistance.x / (bufferData.clipDistance.x - bufferData.clipDistance.y),
			bufferData.clipDistance.x * bufferData.clipDistance.y / (bufferData.clipDistance.y - bufferData.clipDistance.x),
			tanf(Math::DegreesToRadians(fov * 0.5f)) * (bufferData.aspectRatio), tanf(Math::DegreesToRadians(fov * 0.5f))
		);
	}

	void Camera::_RecalculateFrustum() {
		//MT::ScopedGuard guard(mutex);
		ASSERT(frustum, "invalid pointer");
		frustum->Build(this->bufferData.projMatrix * this->bufferData.viewMatrix);
	}

	void Camera::_RecalculateView() {
		//MT::ScopedGuard guard(mutex);
		bufferData._position = BaseEntity::GetPosition();
		// See GLM::LookAt - second param must be position+direction
		this->bufferData.viewMatrix = Mat4::lookAt(bufferData._position, bufferData._position + bufferData.direction, mState.upVector);
	}

	void Camera::SetView(const Mat4 &view) {
		//MT::ScopedGuard guard(mutex);
		this->bufferData.viewMatrix = view;
		auto position = Mat4::inverse(view) * Vec3(Math::ZEROFLOAT, Math::ZEROFLOAT, Math::ZEROFLOAT);
		this->SetPosition(position);
	}

	void Camera::SetFOV(float _fov) {
		//MT::ScopedGuard guard(mutex);
		fov = _fov;
		_RecalculateProjection();
	}

	void Camera::Update(const Vec2&_v) {
		//MT::ScopedGuard guard(mutex);
		_RecalculateProjection(_v);
		_RecalculateView();
		_RecalculateFrustum();
	}

	void Camera::SetPhysics(const Vec3&_phSize) { Warning(__FUNCTION__);/*MT::ScopedGuard guard(mutex);*/ phSize = _phSize; }
}