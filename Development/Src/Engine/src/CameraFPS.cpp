/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
//**************************************
#include "Engine.h"
#include "CameraFPS.h"
//**************************************
#include "PhysXCharacterController.h"
//**************************************
//see http://physx3.googlecode.com/svn/trunk/PhysX-3.2_PC_SDK_Core/Samples/SampleCCTSharedCode/SampleCCTActor.cpp

namespace NGTech {
	/**
	*/
	CameraFPS::CameraFPS(bool _autoswitch)
		:Camera(_autoswitch) {
		this->maxVelocity = 1500.f;
		pBody = nullptr;
		jumpSnd = nullptr;
		jumpSrc = nullptr;
		pJoint = nullptr;
	}

	CameraFPS::~CameraFPS() {
		Debug("Destroying CameraFPS");
		SAFE_DELETE(pBody);
	}

	/**
	*/
	void CameraFPS::SetPhysics(const Vec3 &_size) {
		//MT::ScopedGuard guard(mutex);
		Camera::SetPhysics(_size);
		auto position = this->GetPosition();

		CharacterControllerDesc desc;

		TimeDelta sizeY = _size.y;
		if (Math::IsEqual(sizeY, Math::ZERODELTA)) {
			Warning("[%s] Invalid size.y, it's must be >0", __FUNCTION__);
			sizeY = Math::ONEDELTA;
		}
		desc.height = sizeY;

		desc.position = position;

		_SetPhysics(desc);
	}

	void CameraFPS::_SetPhysics(const CharacterControllerDesc &_desc) {
		//MT::ScopedGuard guard(mutex);
		pBody = new PhysXCharacterController(_desc);
	}

	/**
	*/
	void CameraFPS::SetPosition(const Vec3 &_pos) {
		//MT::ScopedGuard guard(mutex);
		if (pBody) {
			this->pBody->SetPosition(_pos);
			Camera::SetPosition(this->pBody->GetPosition());
			return;
		}
		Camera::SetPosition(_pos);
	}

	Vec3 CameraFPS::GetPosition() const {
		//MT::ScopedGuard guard(mutex);
		if (pBody) return pBody->GetPosition();
		return Camera::GetPosition();
	}

	void CameraFPS::Move(const Vec3& _pos) {
		//MT::ScopedGuard guard(mutex);
		auto _engine = GetEngine();
		if (this->pBody && _engine) {
			this->pBody->Move(_pos.x, _pos.y, _pos.z, _engine->GetElapsedTimeFromStart());
			Camera::SetPosition(this->pBody->GetPosition());
			return;
		}
		else {
			ErrorWrite("_engine pointer not exist");
		}

		SetPosition(_pos);
	}

	void CameraFPS::MoveToPoint(const Vec3 & _pos) {
		//MT::ScopedGuard guard(mutex);
		auto _engine = GetEngine();
		auto difference = _pos - this->GetPosition();
		if (this->pBody && _engine) {
			this->pBody->Move(difference.x, difference.y, difference.z, _engine->GetElapsedTimeFromStart());
			Camera::SetPosition(this->pBody->GetPosition());
			return;
		}
		else {
			ErrorWrite("_engine pointer not exist");
		}

		SetPosition(_pos);
	}

	/**
	*/
	void CameraFPS::UpdateWCD() {
		//MT::ScopedGuard guard(mutex);
		TODO("Check on code duplication");
		// Mouse sensitivity as degrees per pixel
		static const float MOUSE_SENSITIVITY = 0.1f;//TODO:��������� ����������������
		static const TimeDelta timeStep = Math::ONEDELTA / TimeDelta(UpdateConfig::Physics);

		auto _engine = GetEngine();
		ASSERT(_engine, "Not exist _engine pointer");

		const TimeDelta frameTime = _engine->GetElapsedTimeFromStart();
		if (pBody)
		{
			Camera::SetPosition(pBody->GetPosition());
		}

		if (GetWindow()->IsMouseMoved() && GetWindow()->IsMouseGrabbed()) {
			angle[0] = -MOUSE_SENSITIVITY * GetWindow()->GetMouseX();
			angle[1] = -MOUSE_SENSITIVITY * GetWindow()->GetMouseY();
		}

		if (angle[1] > 80) angle[1] = 75;
		if (angle[1] < -80) angle[1] = -75;

		Vec3 forwardVec = Vec3(sinf(Math::DegreesToRadians(angle[0])) * cosf(Math::DegreesToRadians(angle[1])),
			sinf(Math::DegreesToRadians(angle[1])),
			cosf(Math::DegreesToRadians(angle[0])) * cosf(Math::DegreesToRadians(angle[1])));

		Vec3 leftVec = Vec3(sinf(Math::DegreesToRadians(angle[0] + 90)), 0, cosf(Math::DegreesToRadians(angle[0] + 90)));
		Vec3 movement(0, 0, 0);

		bufferData.direction.x = sinf(Math::DegreesToRadians(angle[0])) * cosf(Math::DegreesToRadians(angle[1]));
		bufferData.direction.y = sinf(Math::DegreesToRadians(angle[1]));
		bufferData.direction.z = cosf(Math::DegreesToRadians(angle[0])) * cosf(Math::DegreesToRadians(angle[1]));
		bufferData.direction = Vec3::normalize(bufferData.direction);

		if (GetWindow()->IsKeyPressed("W")) {
			movement += forwardVec * timeStep * frameTime;
		}
		if (GetWindow()->IsKeyPressed("S")) {
			movement -= forwardVec * timeStep * frameTime;
		}
		if (GetWindow()->IsKeyPressed("A")) {
			movement += leftVec * timeStep * frameTime;
		}
		if (GetWindow()->IsKeyPressed("D")) {
			movement -= leftVec * timeStep * frameTime;
		}

		if (movement.length() > EPSILON) {
			movement = Vec3::normalize(movement);
		}

		if (pBody)
		{
			//!@todo CHECK
			/*static bool jump = false;

			if (GetWindow()->isKeyPressed("SPACE"))
			{
				jump = true;
				pBody->Jump(movement);
			}
			else
			{
				if (!jump)
				{
					float dy = GetPhysics()->GetGravity().y * physxDelta;
					movement.y += dy;
				}
				else
					jump = false;
			}*/
			this->Move(movement * maxVelocity);
		}
		else
		{
			bufferData._position = BaseEntity::GetPosition();
			this->SetPosition(bufferData._position + movement * maxVelocity);
		}

		bufferData._position = BaseEntity::GetPosition();

		TODO("SCALE SUPPORT");
		transform = Mat4::translate(bufferData._position) * Mat4::rotate(90, bufferData.direction);

		this->bufferData.viewMatrix = Mat4::lookAt(bufferData._position, bufferData._position + bufferData.direction, Math::Y_AXIS);
	}

	void CameraFPS::Update(const Vec2&_v) {
		//MT::ScopedGuard guard(mutex);
		Camera::Update(_v);

		// Mouse sensitivity as degrees per pixel
		static const float MOUSE_SENSITIVITY = 0.1f;//TODO:��������� ����������������
		static const TimeDelta timeStep = Math::ONEDELTA / TimeDelta(UpdateConfig::Physics);
		const TimeDelta frameTime = GetEngine()->GetElapsedTimeFromStart();
		if (pBody)
		{
			Camera::SetPosition(pBody->GetPosition());
		}

		if (GetWindow()->IsMouseMoved() && GetWindow()->IsMouseGrabbed()) {
			angle[0] = -MOUSE_SENSITIVITY * GetWindow()->GetMouseX();
			angle[1] = -MOUSE_SENSITIVITY * GetWindow()->GetMouseY();
		}

		if (angle[1] > 80) angle[1] = 75;
		if (angle[1] < -80) angle[1] = -75;

		Vec3 forwardVec = Vec3(sinf(Math::DegreesToRadians(angle[0])) * cosf(Math::DegreesToRadians(angle[1])),
			sinf(Math::DegreesToRadians(angle[1])),
			cosf(Math::DegreesToRadians(angle[0])) * cosf(Math::DegreesToRadians(angle[1])));

		Vec3 leftVec = Vec3(sinf(Math::DegreesToRadians(angle[0] + 90)), 0, cosf(Math::DegreesToRadians(angle[0] + 90)));
		Vec3 movement = Vec3::ZERO;

		bufferData.direction.x = sinf(Math::DegreesToRadians(angle[0])) * cosf(Math::DegreesToRadians(angle[1]));
		bufferData.direction.y = sinf(Math::DegreesToRadians(angle[1]));
		bufferData.direction.z = cosf(Math::DegreesToRadians(angle[0])) * cosf(Math::DegreesToRadians(angle[1]));
		bufferData.direction = Vec3::normalize(bufferData.direction);

		if (GetWindow()->IsKeyPressed("W")) {
			movement += forwardVec * timeStep * frameTime;
		}
		if (GetWindow()->IsKeyPressed("S")) {
			movement -= forwardVec * timeStep * frameTime;
		}
		if (GetWindow()->IsKeyPressed("A")) {
			movement += leftVec * timeStep * frameTime;
		}
		if (GetWindow()->IsKeyPressed("D")) {
			movement -= leftVec * timeStep * frameTime;
		}

		if (movement.length() > EPSILON) {
			movement = Vec3::normalize(movement);
		}

		if (pBody)
		{
			//!@todo CHECK
			/*static bool jump = false;

			if (GetWindow()->isKeyPressed("SPACE")) {
				jump = true;
				pBody->Jump(movement);
			}
			else
			{
				if (!jump)
				{
					float dy = GetPhysics()->GetGravity().y * physxDelta;
					movement.y += dy;
				}
				else
					jump = false;
			}*/
			this->Move(movement * maxVelocity);
		}
		else
		{
			bufferData._position = BaseEntity::GetPosition();
			this->SetPosition(bufferData._position + movement * maxVelocity);
		}

		TODO("SCALE SUPPORT");
		//!@todo CHECK
		transform = Mat4::translate(bufferData._position) * Mat4::rotate(90, bufferData.direction);

		this->bufferData.viewMatrix = Mat4::lookAt(bufferData._position, bufferData._position + bufferData.direction, Math::Y_AXIS);
	}
}