/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
//**************************************
#include "CameraFixed.h"
//**************************************

namespace NGTech {
	/**
	*/
	CameraFixed::CameraFixed() :Camera() {
		this->maxVelocity = 1500;
	}

	/**
	*/
	CameraFixed::~CameraFixed() {
		Debug("Destroying CameraFixed");
	}

	/**
	*/
	void CameraFixed::UpdateWCD() {
		//MT::ScopedGuard guard(mutex);
		bufferData._position = BaseEntity::GetPosition();
		transform = Mat4::translate(bufferData._position) * Mat4::rotate(90, bufferData.direction);
		this->bufferData.viewMatrix = Mat4::lookAt(bufferData._position, bufferData._position + bufferData.direction, Math::Y_AXIS);
	}

	/**
	*/
	void CameraFixed::Update(const Vec2&_v) {
		//MT::ScopedGuard guard(mutex);
		Camera::Update(_v);

		bufferData._position = BaseEntity::GetPosition();

		transform = Mat4::translate(bufferData._position)*Mat4::rotate(90, bufferData.direction);
		this->bufferData.viewMatrix = Mat4::lookAt(bufferData._position, bufferData._position + bufferData.direction, Math::Y_AXIS);
	}

	/**
	*/
	void CameraFixed::Move(const Vec3& _pos) {
		//MT::ScopedGuard guard(mutex);
		SetPosition(_pos);
	}

	/**
	*/
	void CameraFixed::MoveToPoint(const Vec3 & _pos) {
		//MT::ScopedGuard guard(mutex);
		auto difference = _pos - this->GetPosition();
		SetPosition(_pos);
	}
}