/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
//**************************************
#include "Engine.h"
#include "CameraFree.h"
//**************************************
#include "PhysXCharacterController.h"
//**************************************

namespace NGTech {
	/**
	*/
	CameraFree::CameraFree(bool _autoswitch)
		:Camera(_autoswitch)
	{
		this->maxVelocity = 1500.f;
		pBody = nullptr;
	}

	/**
	*/
	CameraFree::~CameraFree() {
		Debug("Destroying CameraFree");
		SAFE_DELETE(pBody);
	}

	/**
	*/
	void CameraFree::SetPhysics(const Vec3 &_size) {
		//MT::ScopedGuard guard(mutex);
		Camera::SetPhysics(_size);
		auto position = this->GetPosition();// TODO:CHECK:BUG: ��������� ���� � ������� ���������� �� transform

		CharacterControllerDesc desc;

		TimeDelta sizeY = _size.y;
		if (Math::IsEqual(sizeY, Math::ZERODELTA)) {
			Warning("[%s] Invalid size.y, it's must be >0", __FUNCTION__);
			sizeY = Math::ONEDELTA;
		}
		desc.height = sizeY;

		desc.position = position;

		_SetPhysics(desc);
	}

	/**
	*/
	void CameraFree::_SetPhysics(const CharacterControllerDesc &_desc) {
		//MT::ScopedGuard guard(mutex);
		pBody = new PhysXCharacterController(_desc);
	}

	/**
	*/
	void CameraFree::SetPosition(const Vec3 &_pos) {
		//MT::ScopedGuard guard(mutex);
		if (pBody) {
			this->pBody->SetPosition(_pos);
			Camera::SetPosition(this->pBody->GetPosition());
			return;
		}

		Camera::SetPosition(_pos);
	}

	Vec3 CameraFree::GetPosition() const {
		//MT::ScopedGuard guard(mutex);
		if (pBody) return pBody->GetPosition();
		return Camera::GetPosition();
	}

	void CameraFree::Move(const Vec3& _pos) {
		//MT::ScopedGuard guard(mutex);

		auto _engine = GetEngine();
		if (this->pBody && _engine != nullptr) {
			this->pBody->Move(_pos.x, _pos.y, _pos.z, _engine->GetElapsedTimeFromStart());
			Camera::SetPosition(this->pBody->GetPosition());
			return;
		}
		else {
			ErrorWrite("Not exist pBody or _engine");
		}

		SetPosition(_pos);
	}

	void CameraFree::MoveToPoint(const Vec3& _pos) {
		//MT::ScopedGuard guard(mutex);

		auto difference = _pos - this->GetPosition();
		auto _engine = GetEngine();
		if (this->pBody && _engine != nullptr) {
			this->pBody->Move(difference.x, difference.y, difference.z, _engine->GetElapsedTimeFromStart());
			Camera::SetPosition(this->pBody->GetPosition());
			return;
		}
		else {
			ErrorWrite("Not exist pBody or _engine");
		}
		SetPosition(_pos);
	}

	/**
	*/
	void CameraFree::UpdateWCD() {
		//MT::ScopedGuard guard(mutex);

		//!@todo CHECK
		TODO("Check on code duplication");
		// Mouse sensitivity as degrees per pixel
		static const float MOUSE_SENSITIVITY = 0.1f;//TODO:��������� ����������������
		static const TimeDelta timeStep = Math::ONEDELTA / TimeDelta(UpdateConfig::Physics);

		auto _engine = GetEngine();
		ASSERT(_engine, "Not exist _engine pointer");

		const TimeDelta frameTime = _engine->GetElapsedTimeFromStart();

		if (pBody) {
			Camera::SetPosition(pBody->GetPosition());
		}

		if (GetWindow()->IsMouseMoved() && GetWindow()->IsMouseGrabbed()) {
			angle[0] = -MOUSE_SENSITIVITY * GetWindow()->GetMouseX();
			angle[1] = -MOUSE_SENSITIVITY * GetWindow()->GetMouseY();
		}

		if (angle[1] > 80) angle[1] = 75;
		if (angle[1] < -80) angle[1] = -75;

		Vec3 forwardVec = Vec3(sinf(Math::DegreesToRadians(angle[0])) * cosf(Math::DegreesToRadians(angle[1])),
			sinf(Math::DegreesToRadians(angle[1])),
			cosf(Math::DegreesToRadians(angle[0])) * cosf(Math::DegreesToRadians(angle[1])));

		Vec3 leftVec = Vec3(sinf(Math::DegreesToRadians(angle[0] + 90)), 0, cosf(Math::DegreesToRadians(angle[0] + 90)));
		Vec3 movement(0, 0, 0);

		bufferData.direction.x = sinf(Math::DegreesToRadians(angle[0])) * cosf(Math::DegreesToRadians(angle[1]));
		bufferData.direction.y = sinf(Math::DegreesToRadians(angle[1]));
		bufferData.direction.z = cosf(Math::DegreesToRadians(angle[0])) * cosf(Math::DegreesToRadians(angle[1]));
		bufferData.direction = Vec3::normalize(bufferData.direction);

		if (GetWindow()->IsKeyPressed("W")) {
			movement += forwardVec * timeStep * frameTime;
		}
		if (GetWindow()->IsKeyPressed("S")) {
			movement -= forwardVec * timeStep * frameTime;
		}
		if (GetWindow()->IsKeyPressed("A")) {
			movement += leftVec * timeStep * frameTime;
		}
		if (GetWindow()->IsKeyPressed("D")) {
			movement -= leftVec * timeStep * frameTime;
		}

		if (movement.length() > EPSILON) {
			movement = Vec3::normalize(movement);
		}

		if (pBody)
		{
			this->Move(movement * maxVelocity);
		}
		else
		{
			bufferData._position = BaseEntity::GetPosition();
			this->SetPosition(bufferData._position + movement * maxVelocity);
		}

		bufferData._position = BaseEntity::GetPosition();

		TODO("SCALE SUPPORT");
		transform = Mat4::translate(bufferData._position) * Mat4::rotate(90, bufferData.direction);

		this->bufferData.viewMatrix = Mat4::lookAt(bufferData._position, bufferData._position + bufferData.direction, Math::Y_AXIS);
	}

	/**
	*/
	void CameraFree::Update(const Vec2&_v) {
		//MT::ScopedGuard guard(mutex);
		Camera::Update(_v);

		// Mouse sensitivity as degrees per pixel
		static const float MOUSE_SENSITIVITY = 0.1f;//TODO:��������� ����������������
		static const TimeDelta timeStep = Math::ONEDELTA / TimeDelta(UpdateConfig::Physics);
		auto _engine = GetEngine();
		ASSERT(_engine, "Not exist _engine pointer");

		const TimeDelta frameTime = _engine->GetElapsedTimeFromStart();

		if (pBody) {
			Camera::SetPosition(pBody->GetPosition());
		}

		if (GetWindow()->IsMouseMoved() && GetWindow()->IsMouseGrabbed()) {
			angle[0] = -MOUSE_SENSITIVITY * GetWindow()->GetMouseX();
			angle[1] = -MOUSE_SENSITIVITY * GetWindow()->GetMouseY();
		}

		if (angle[1] > 80) angle[1] = 75;
		if (angle[1] < -80) angle[1] = -75;

		Vec3 forwardVec = Vec3(sinf(Math::DegreesToRadians(angle[0])) * cosf(Math::DegreesToRadians(angle[1])),
			sinf(Math::DegreesToRadians(angle[1])),
			cosf(Math::DegreesToRadians(angle[0])) * cosf(Math::DegreesToRadians(angle[1])));

		Vec3 leftVec = Vec3(sinf(Math::DegreesToRadians(angle[0] + 90)), 0, cosf(Math::DegreesToRadians(angle[0] + 90)));
		Vec3 movement = Vec3::ZERO;

		bufferData.direction.x = sinf(Math::DegreesToRadians(angle[0])) * cosf(Math::DegreesToRadians(angle[1]));
		bufferData.direction.y = sinf(Math::DegreesToRadians(angle[1]));
		bufferData.direction.z = cosf(Math::DegreesToRadians(angle[0])) * cosf(Math::DegreesToRadians(angle[1]));
		bufferData.direction = Vec3::normalize(bufferData.direction);

		if (GetWindow()->IsKeyPressed("W")) {
			movement += forwardVec * timeStep * frameTime;
		}
		if (GetWindow()->IsKeyPressed("S")) {
			movement -= forwardVec * timeStep * frameTime;
		}
		if (GetWindow()->IsKeyPressed("A")) {
			movement += leftVec * timeStep * frameTime;
		}
		if (GetWindow()->IsKeyPressed("D")) {
			movement -= leftVec * timeStep * frameTime;
		}

		if (movement.length() > EPSILON) {
			movement = Vec3::normalize(movement);
		}

		if (pBody)
		{
			this->Move(movement * maxVelocity);
		}
		else
		{
			bufferData._position = BaseEntity::GetPosition();
			this->SetPosition(bufferData._position + movement * maxVelocity);
		}

		TODO("SCALE SUPPORT");
		//!@todo CHECK
		transform = Mat4::translate(bufferData._position) * Mat4::rotate(90, bufferData.direction);

		this->bufferData.viewMatrix = Mat4::lookAt(bufferData._position, bufferData._position + bufferData.direction, Math::Y_AXIS);
	}
}