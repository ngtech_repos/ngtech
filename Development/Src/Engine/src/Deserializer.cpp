/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"

//!@todo REPLACE ON https://github.com/simonask/reflect/blob/master/serialization/deserialize_object.cpp

//**************************************
#include "SerializerSharedIncls.h"
//**************************************
#include "Deserializer.h"
//**************************************
#include <stdint.h>
//**************************************
#include "PhysSystem.h"
#include "PhysicsMaterialsManager.h"
//**************************************
// NOTE
// val.Empty() - used only for arrays
// val.IsNull() - used only for objects

//
//#ifndef ALLOW_DEBUG_DESERIALIZER
//#define ALLOW_DEBUG_DESERIALIZER 1
//#endif

namespace NGTech {
	template<> void Deserializer::DeserializeModify<ObjectMesh&>(const Value& val, ObjectMesh& ptr);
	template<> void Deserializer::DeserializeModify<Light&>(const Value& val, Light& ptr);
	template<> void Deserializer::DeserializeModify<PhysXMaterialSh&>(const Value& val, PhysXMaterialSh& ptr);
	template<> PhysXMaterialSh* Deserializer::Deserialize<PhysXMaterialSh*>(const Value& val, PhysXMaterialSh* defV);
	template<> PhysXMaterialSh* Deserializer::DeserializeFromFile<PhysXMaterialSh*>(const String& _name, PhysXMaterialSh* _defV);
	template <>	PhysBodyDescSh Deserializer::Deserialize<PhysBodyDescSh>(const Value&val, PhysBodyDescSh _defV);

	void _DeserializeNewCamera(const Value& val, Deserializer& _des);

	Deserializer::Deserializer(const String & path) {
#if ALLOW_DEBUG_DESERIALIZER
		mDebugOut = debugger_present() ? true : false;
#else
		mDebugOut = false;
#endif
		filename = path;
		VFileStream file(path);
		if (!file.IsValid())
		{
			Warning("[%s] INVALID FILE %s", __FUNCTION__, filename.c_str());
			return;
		}

		StringStream stream(file.LoadFile());
		doc.ParseStream(stream);
		file.Close();

		if (doc.HasParseError())
			ErrorWrite("Detected error in parsing file: %s", filename.c_str());
	}

	//---------------------------Common--------------------------
	/**
	*/
#if PLATFORM_MEMORY_ADDRESS_SPACE == PLATFORM_MEMORY_ADDRESS_SPACE_64BIT
	/**
	*/
	template <> ENGINE_INLINE ENGINE_API
		int Deserializer::DeserializeUtil<int>(const Value& val, const char* key, int defV) {
		if (val.IsNull())
		{
			ErrorWrite("Main val is NULL");
			return defV;
		}

		if (!val.HasMember(key))
		{
			if (mDebugOut)
				ErrorWrite("Not exist key %s", key);
			return defV;
		}

		if (val[key].IsNull() || (val[key].IsInt() == false)) {
			ErrorWrite("Requested value for key: %s is null or not Int", key);
			return defV;
		}

		int d = val[key].GetInt();
		return d;
	}

	/**
	*/
	template <> ENGINE_INLINE ENGINE_API
		unsigned int Deserializer::DeserializeUtil<unsigned int>(const Value& val, const char* key, unsigned int defV) {
		if (val.IsNull())
		{
			ErrorWrite("Main val is NULL");
			return defV;
		}

		if (!val.HasMember(key))
		{
			if (mDebugOut)
				ErrorWrite("Not exist key %s", key);
			return defV;
		}

		if (val[key].IsNull() || (val[key].IsUint() == false)) {
			ErrorWrite("Requested value for key: %s is null or not Uint", key);
			return defV;
		}

		auto d = val[key].GetUint();
		return d;
	}
#endif

	/**
	*/
	template <> ENGINE_INLINE ENGINE_API
		intptr_t Deserializer::DeserializeUtil<intptr_t>(const Value& val, const char* key, intptr_t defV) {
		return DeserializeUtil<int>(val, key, defV);
	}

	/**
	*/
	template <> ENGINE_INLINE ENGINE_API
		uintptr_t Deserializer::DeserializeUtil<uintptr_t>(const Value& val, const char* key, uintptr_t defV) {
		return DeserializeUtil<unsigned int>(val, key, defV);
	}

	/**
	*/
	template <> ENGINE_INLINE ENGINE_API
		TimeDelta Deserializer::DeserializeUtil(const Value& val, const char* key, TimeDelta defV) {
		if (val.IsNull())
		{
			ErrorWrite("Main val is NULL");
			return defV;
		}

		if (!val.HasMember(key))
		{
			if (mDebugOut)
				ErrorWrite("Not exist key %s", key);
			return defV;
		}

		if (val[key].IsNull() || (val[key].IsDouble() == false)) {
			ErrorWrite("Requested value for key: %s is null or not double", key);
			return defV;
		}

		auto d = val[key].GetDouble();
		return d;
	}

	/**
	*/
	template<> ENGINE_INLINE ENGINE_API
		TimeDelta Deserializer::DeserializeWithNull<TimeDelta>(const Value& val, void* userdata) {
		if (val.IsNull()) {
			return Math::ZERODELTA;
		}

		if (val.IsDouble() == false) {
			ErrorWrite("[DeserializeWithNull<TimeDelta>] INVALID DATA: val.IsDouble() = false ");
			return Math::ZERODELTA;
		}

		TimeDelta d = val.GetDouble();
		return d;
	}

	/**
	*/
	template<> ENGINE_INLINE ENGINE_API
		bool Deserializer::DeserializeUtil<bool>(const Value& val, const char* key, bool defV) {
		if (val.IsNull())
		{
			ErrorWrite("Main val is NULL");
			return defV;
		}

		if (!val.HasMember(key))
		{
			if (mDebugOut)
				ErrorWrite("Not exist key %s", key);
			return defV;
		}

		if (val[key].IsNull() || (val[key].IsBool() == false)) {
			ErrorWrite("Requested value for key: %s is null or not boolean", key);
			return defV;
		}

		auto d = val[key].GetBool();
		return d;
	}

	/**
	*/
	template<> ENGINE_INLINE ENGINE_API
		String Deserializer::Deserialize<String>(const Value& val, String defV) {
		if (val.IsNull()) {
			ErrorWrite("[DeserializeWithNull<String>] is Null");
			return defV;
		}

		if (val.IsString() == false) {
			ErrorWrite("[DeserializeWithNull<String>] is IsString = false");
			return defV;
		}

		String d = val.GetString();
		return d;
	}

	/**
	*/
	template<> ENGINE_INLINE ENGINE_API
		String Deserializer::DeserializeWithNull<String>(const Value& val, void* userdata) {
		if (val.IsNull()) {
			ErrorWrite("[DeserializeWithNull<String>] is Null");
			return nullptr;
		}

		if (val.IsString() == false) {
			ErrorWrite("[DeserializeWithNull<String>] is IsString = false");
			return nullptr;
		}

		auto d = val.GetString();
		return d;
	}

	/**
	*/
	template<> ENGINE_INLINE ENGINE_API
		const char* Deserializer::Deserialize<const char*>(const Value& val, const char* defV) {
		return Deserialize<String>(val, defV).c_str();
	}

	/**
	*/
	template<> ENGINE_INLINE ENGINE_API
		const char* Deserializer::DeserializeWithNull<const char*>(const Value& val, void* userdata) {
		return DeserializeWithNull<String>(val, userdata).c_str();
	}

#if PLATFORM_MEMORY_ADDRESS_SPACE == PLATFORM_MEMORY_ADDRESS_SPACE_64BIT
	/**
	*/
	template<> ENGINE_INLINE ENGINE_API
		int Deserializer::DeserializeWithNull<int>(const Value& val, void* userdata) {
		if (val.IsNull()) {
			return -1;
		}

		int d = val.GetInt();
		return d;
	}

	/**
	*/
	template<> ENGINE_INLINE ENGINE_API
		unsigned int Deserializer::DeserializeWithNull<unsigned int>(const Value& val, void* userdata) {
		if (val.IsNull()) {
			return (unsigned int)-1;
		}
		unsigned int d = val.GetUint();
		return d;
	}
#endif

	/**
	*/
	template<> ENGINE_INLINE ENGINE_API
		bool Deserializer::DeserializeWithNull<bool>(const Value& val, void* userdata) {
		if (val.IsNull()) {
			return false;
		}
		bool d = val.GetBool();
		return d;
	}

	/**
	*/
	template<> ENGINE_INLINE ENGINE_API
		uintptr_t Deserializer::DeserializeWithNull<uintptr_t>(const Value& val, void* userdata) {
		if (val.IsNull()) {
			return (uintptr_t)-1;
		}
		uintptr_t d = val.GetUint(); //-V101
		return d;
	}

	/**
	*/
	template<> ENGINE_INLINE ENGINE_API
		intptr_t Deserializer::DeserializeWithNull<intptr_t>(const Value& val, void* userdata) {
		if (val.IsNull()) {
			return (intptr_t)-1;
		}
		intptr_t d = val.GetInt(); //-V101
		return d;
	}
	//---------------------------MATH----------------------------
	/**
	*/
	template<> ENGINE_INLINE ENGINE_API
		Color Deserializer::DeserializeUtil<Color>(const Value& val, const char* key, Color defV) {
		if (val.IsNull())
		{
			ErrorWrite("Main val is NULL");
			return defV;
		}

		if (!val.HasMember(key))
		{
			if (mDebugOut)
				ErrorWrite("Not exist key %s", key);
			return defV;
		}

		if (val[key].IsNull() || (val[key].IsArray() == false)) {
			ErrorWrite("Requested value for key: %s is null or not IsArray", key);
			return defV;
		}

		Color vec;

		vec.x = val[key][0].GetDouble();
		vec.y = val[key][1].GetDouble();
		vec.z = val[key][2].GetDouble();
		vec.w = val[key][3].GetDouble();
		return vec;
	}

	/**
	*/
	template<> ENGINE_INLINE ENGINE_API
		Vec4 Deserializer::Deserialize<Vec4>(const Value& val, Vec4 defV) {
		Vec4 vec;
		if (val.IsNull()) {
			return defV;
		}

		vec.x = (float)val[0].GetDouble();
		vec.y = (float)val[1].GetDouble();
		vec.z = (float)val[2].GetDouble();
		vec.w = (float)val[3].GetDouble();
		return vec;
	}

	/**
	*/
	template<> ENGINE_INLINE ENGINE_API
		Vec2 Deserializer::DeserializeUtil<Vec2>(const Value& val, const char* key, Vec2 defV) {
		if (val.IsNull())
		{
			ErrorWrite("Main val is NULL");
			return defV;
		}

		if (!val.HasMember(key))
		{
			if (mDebugOut)
				ErrorWrite("Not exist key %s", key);
			return defV;
		}

		if (val[key].IsNull() || (val[key].IsArray() == false)) {
			ErrorWrite("Requested value for key: %s is null or not IsArray", key);
			return defV;
		}

		Vec2 vec;

		vec.x = val[key][0].GetDouble();
		vec.y = val[key][1].GetDouble();
		return vec;
	}

	template<> ENGINE_INLINE ENGINE_API
		Vec3 Deserializer::DeserializeUtil<Vec3>(const Value& val, const char* key, Vec3 defV) {
		if (val.IsNull())
		{
			ErrorWrite("Main val is NULL");
			return defV;
		}

		if (!val.HasMember(key))
		{
			if (mDebugOut)
				ErrorWrite("Not exist key %s", key);
			return defV;
		}

		if (val[key].IsNull() || (val[key].IsArray() == false)) {
			ErrorWrite("Requested value for key: %s is null or not IsArray", key);
			return defV;
		}

		Vec3 vec;

		vec.x = val[key][0].GetDouble();
		vec.y = val[key][1].GetDouble();
		vec.z = val[key][2].GetDouble();
		return vec;
	}

	template<> ENGINE_INLINE ENGINE_API
		Vec4 Deserializer::DeserializeUtil<Vec4>(const Value& val, const char* key, Vec4 defV) {
		if (val.IsNull())
		{
			ErrorWrite("Main val is NULL");
			return defV;
		}

		if (!val.HasMember(key))
		{
			if (mDebugOut)
				ErrorWrite("Not exist key %s", key);
			return defV;
		}

		if (val[key].IsNull() || (val[key].IsArray() == false)) {
			ErrorWrite("Requested value for key: %s is null or not IsArray", key);
			return defV;
		}

		Vec4 vec;

		vec.x = val[key][0].GetDouble();
		vec.y = val[key][1].GetDouble();
		vec.z = val[key][2].GetDouble();
		vec.w = val[key][3].GetDouble();
		return vec;
	}

	template<> ENGINE_INLINE ENGINE_API
		Quat Deserializer::DeserializeUtil<Quat>(const Value& val, const char* key, Quat defV) {
		if (val.IsNull())
		{
			ErrorWrite("Main val is NULL");
			return defV;
		}

		if (!val.HasMember(key))
		{
			if (mDebugOut)
				ErrorWrite("Not exist key %s", key);
			return defV;
		}

		if (val[key].IsNull() || (val[key].IsArray() == false)) {
			ErrorWrite("Requested value for key: %s is null or not IsArray", key);
			return defV;
		}

		Quat vec;

		vec.x = val[key][0].GetDouble();
		vec.y = val[key][1].GetDouble();
		vec.z = val[key][2].GetDouble();
		vec.w = val[key][3].GetDouble();
		return vec;
	}

	/**
	*/
	template<> ENGINE_INLINE ENGINE_API
		String Deserializer::DeserializeUtil<String>(const Value& val, const char* key, String defV) {
		if (val.IsNull())
		{
			ErrorWrite("Main val is NULL");
			return defV;
		}

		if (!val.HasMember(key))
		{
			if (mDebugOut)
				ErrorWrite("Not exist key %s", key);
			return defV;
		}

		if (val[key].IsNull() || (val[key].IsString() == false)) {
			ErrorWrite("Requested value for key: %s is null or not IsString", key);
			return defV;
		}

		String str = val[key].GetString();
		return str;
	}

	template<> ENGINE_INLINE ENGINE_API
		const char* Deserializer::DeserializeUtil<const char*>(const Value& val, const char* key, const char* defV) {
		return DeserializeUtil<String>(val, key, defV).c_str();
	}

	/**
	*/
	template<> ENGINE_INLINE ENGINE_API
		Vec2 Deserializer::Deserialize<Vec2>(const Value& val, Vec2 defV) {
		Vec2 vec;
		if (val.IsNull()) {
			return defV;
		}

		vec.x = (float)val[0].GetDouble();
		vec.y = (float)val[1].GetDouble();
		return vec;
	}

	/**
	*/
	template<> ENGINE_INLINE ENGINE_API
		Mat3 Deserializer::Deserialize<Mat3>(const Value& val, Mat3 defV) {
		Mat3 mat;
		if (val.IsNull()) {
			ErrorWrite("[Deserialize<Mat3>(const Value& val, Mat3 defV)] value is NULL");
			return defV;
		}

		for (SizeType i = 0; i < val.Size(); i++) {
			mat.e[i] = DeserializeWithNull<TimeDelta>(val[i], NULL);
		}
		return mat;
	}

	/**
	*/
	template<> ENGINE_INLINE ENGINE_API
		Mat4 Deserializer::Deserialize<Mat4>(const Value& val, Mat4 defV) {
		Mat4 mat;
		if (val.IsNull()) {
			ErrorWrite("[Deserialize<Mat4>(const Value& val, Mat4 defV)] value is NULL");
			return defV;
		}

		for (SizeType i = 0; i < val.Size(); i++) {
			mat.e[i] = DeserializeWithNull<TimeDelta>(val[i], NULL);
		}
		return mat;
	}
	//---------------------------Physics-------------------------

	template <>
	PhysBodyDescSh Deserializer::Deserialize<PhysBodyDescSh>(const Value&val, PhysBodyDescSh _defV) {
		PhysBodyDescSh mDesc;
		if (val.IsNull()) {
			Warning("DeserializeModify([val[object],PhysBodyDesc) is null - check call");
			return mDesc;
		}

		mDesc.AngularVelocity = DeserializeUtil(val, "AngularVelocity", Vec3::ZERO);
		mDesc.CollisionSize = DeserializeUtil(val, "CollisionSize", Vec3::ZERO);

		mDesc.AppliedTorque = DeserializeUtil(val, "AppliedTorque", Vec3::ZERO);
		mDesc.AppliedForce = DeserializeUtil(val, "AppliedForce", Vec3::ZERO);
		mDesc.MassSpaceInertiaTensor = DeserializeUtil(val, "MassSpaceInertiaTensor", Vec3::ZERO);

		mDesc.CollisionRadius = DeserializeUtil(val, /*"CollisionRadius"*/"CollisionRad", Math::ZERODELTA);
		mDesc.CollisionWidth = DeserializeUtil(val, "CollisionWidth", Math::ZERODELTA);
		mDesc.CollisionHeight = DeserializeUtil(val, "CollisionHeight", Math::ZERODELTA);

		mDesc.CollisionMass = DeserializeUtil(val, "CollisionMass", Math::ONEDELTA);
		mDesc.CollisionDensity = DeserializeUtil(val, "CollisionDensity", Math::ZERODELTA);

		mDesc.LinearDamping = DeserializeUtil(val, "LinearDamping", Math::ZERODELTA);
		mDesc.AngularDamping = DeserializeUtil(val, "AngularDamping", Math::ZERODELTA);

		// TODO: Renmae param
		mDesc.mCollisionType = static_cast<PhysBodyDescSh::CollisionType>(DeserializeUtil(val, /*"CollisionType"*/"phType", static_cast<unsigned int>(PhysBodyDescSh::CollisionType::CT_NONE)));

		// TODO: Implement this
		// mDesc.m_PhysicsMaterial;
		//// IMPACT SOUND
			//{
			//	String str = DeserializeUtil<String>(physBodyDescBlock, "ImpactSound", "");
			//	if (str.empty() == false)
			//		ptr.SetImpactSound(str);
			//}

		return mDesc;
	}

	template<> ENGINE_INLINE ENGINE_API
		PhysBody* Deserializer::DeserializeWithNull<PhysBody*>(const Value& val, void* userData) {
		if (val.IsNull()) {
			return nullptr;
		}

		if (!userData)
		{
			ErrorWrite("Invalid userData");
			return nullptr;
		}

		return new PhysBody(Deserialize(val, PhysBodyDescSh()));
	}

	//---------------------------Audio---------------------------
	/**
	*/
	template<> ENGINE_INLINE ENGINE_API
		void Deserializer::DeserializeModify<ALSound&>(const Value& val, ALSound& _sound) {
		if (val.IsNull())
			return;

		if (val.HasMember("desc.path") == false) {
			Warning("Not exist key desc.path");
			return;
		}

		ALSound::Desc desc;
		desc.path = Deserialize<const char*>(val["desc.path"], "");

		// Setup description, and immediately loading
		_sound.SetDescAndLoad(desc, true);
	}

	/**
	*/
	template<> ENGINE_INLINE ENGINE_API
		void Deserializer::DeserializeModify<ALSoundSource&>(const Value& val, ALSoundSource& _sound) {
		if (val.IsNull())
			return;

		ALSoundSource::Desc desc;
		desc.Paused = DeserializeUtil<bool>(val, "desc.Paused", false);
		desc.Stoped = DeserializeUtil<bool>(val, "desc.Stoped", true);
		desc.Looped = DeserializeUtil<bool>(val, "desc.Looped", false);
		desc.Relative = DeserializeUtil<bool>(val, "desc.Relative", false);

		desc.Gain = DeserializeUtil(val, "desc.Gain", Math::ZERODELTA);
		desc.RollOffFactor = DeserializeUtil(val, "desc.RollOffFactor", Math::ZERODELTA);
		desc.MaxDistance_in_Meter = DeserializeUtil(val, "desc.MaxDistance", Math::ZERODELTA);
		desc.MinDistance_in_Meter = DeserializeUtil(val, "desc.MinDistance", Math::ZERODELTA);

		desc.Position = DeserializeUtil(val, "desc.Position", Vec3::ZERO);

		desc.preset = (ALSoundSource::Desc::Sound_Presets)DeserializeUtil<unsigned int>(val, "desc.preset", 0);

		_sound.SetDescAndApply(desc);
	}
	//---------------------------Scene---------------------------

	/**
	�������������� �����-��������������� ����� �����
	Status: 20%
	// TODO:
	*/
	template<> ENGINE_INLINE ENGINE_API
		void Deserializer::DeserializeModify<SceneManager&>(const Value& val, SceneManager& scene) {
		if (val.IsNull()) {
			Warning("Empty file scene");
			return;
		}

		scene.Clear();

		// Scene Name
		scene.SetCurrentSceneName(filename);

		// Scene Params
		{
			auto useEarlyZ = DeserializeUtil<bool>(val, "useEarlyZ", true);
			scene.SetEnabledEarlyZ(useEarlyZ);

			auto gammaCorrMode = DeserializeUtil<unsigned int>(val, "gammaCorrMode", 0);
			scene.SetGammaCorrectionMode(gammaCorrMode);

			auto ambient = DeserializeUtil<Color>(val, "sceneAmbient", Color::BLACK);
			scene.m_ActiveScene.SetSceneAmbient(ambient);

			if (val.HasMember("currentCamera") == true) {
				_DeserializeNewCamera(val["currentCamera"], *this);
			}

			auto loadingScreen = DeserializeUtil(val, "loadingScreen", DefaultResources::Texture_DefaultDiffuse_Name);
			scene.SetLoadingScreen(loadingScreen);

			//Warning("Finished LoadingScreen");
		}

		// Meshes
		{
			if (val.HasMember("meshes") == true) {
				if (val["meshes"].IsNull()) {
					Warning("Block meshes invalid");
				}

				for (SizeType i = 0; i < val["meshes"].Size(); i++)
				{
					// Memory will be cleared in Scene::Clear
					// TODO:Des Refactor this. : ������� ����� ������ � �������� �����
					auto mesh = new ObjectMesh(true);
					DeserializeModify<ObjectMesh&>(val["meshes"][i], *mesh); //-V108
				}
			}
		}
		// TODO:Des: Animated Meshes �� �����������

		// Lights
		{
			if (val.HasMember("lights") == true) {
				if (val["lights"].IsNull()) {
					Warning("Block Lights Invalid");
				}

				for (SizeType i = 0; i < val["lights"].Size(); i++)
				{
					auto type = DeserializeUtil<unsigned int>(val["lights"][i], "lightType", 0);

					// Memory will be cleared in Scene::Clear
					auto light = API_CreateLight(type);

					DeserializeModify<Light&>(val["lights"][i], *light);

					auto _data = light->GetLightBufferData();
					light->SetNewState(_data);
				}
			}
		}

		scene.m_ActiveScene.LoadSceneFinishedEventApply();
	}

	//---------------------------Light---------------------------
	/**
	*/
	template<> ENGINE_INLINE ENGINE_API
		void Deserializer::DeserializeModify<Light&>(const Value& val, Light& ptr) {
		if (val.IsNull()) {
			Warning("DeserializeModify([val[object],Light&) is NULL - check call");
			return;
		}

		//!@todo lightType

		auto DirRight = DeserializeUtil(val, "dirRight", Vec3::ZERO);
		ptr.SetDirectionRight(DirRight);

		auto _dirup = DeserializeUtil(val, "dirUp", Vec3::ZERO);
		ptr.SetDirectionUp(_dirup);

		auto LumFlux = DeserializeUtil(val, "lumFlux", 5000.0);
		ptr.SetLumFlux(LumFlux);

		auto Position = DeserializeUtil(val, "position", Vec3::ZERO);
		ptr.SetPosition(Position);

		auto radius = DeserializeUtil(val, "radius", 500.0);
		ptr.SetRadius(radius);

		auto Color = DeserializeUtil(val, "color", Color::WHITE);
		ptr.SetColor(Color);

		auto Direction = DeserializeUtil(val, "direction", Vec3::ZERO);
		ptr.SetDirection(Direction);

		auto IsEnable = DeserializeUtil<bool>(val, "isEnable", true);
		ptr.SetEnable(IsEnable);

		auto IsVisible = DeserializeUtil<bool>(val, "isVisible", true);
		ptr.SetVisible(IsVisible);

		auto IsCastShadows = DeserializeUtil<bool>(val, "isCastShadows", true);
		ptr.SetCastShadows(IsCastShadows);

		auto IsDisableByDistance = DeserializeUtil<bool>(val, "isDisableByDistance", false);
		ptr.SetDisableByDistance(IsDisableByDistance);

		auto disableByDistanceV = DeserializeUtil(val, "isDisableByDistanceValue", 1000.0);
		ptr.SetDisableByDistanceValue(disableByDistanceV);

		//!@todo ADD DEFAULT FOV CONSTANT
		auto fov = DeserializeUtil(val, "fov", 60.0);
		ptr.SetFOV(fov);

		auto LightArea = DeserializeUtil(val, "lightArea", Math::ZERODELTA);
		ptr.SetlightArea(LightArea);

		auto isFrustumCullingEnabled = DeserializeUtil<bool>(val, "isFrustumCullingEnabled", true);
		ptr.EnableFrustumCulling(isFrustumCullingEnabled);

		auto Scale = DeserializeUtil(val, "scale", Vec3::ONE);
		ptr.SetScale(Scale);

		//!@todo DES Refactor: ������ MagicConst
		auto spotAngles = DeserializeUtil(val, "spotAngles", Vec2(1.0, 0.7193397879600525));
		ptr.SetSpotAngles(spotAngles);

		auto orientation = DeserializeUtil(val, "orientation", Quat::ZERO);
		ptr.SetOrientation(orientation);

		/*Bool*/
		{
		}
		/*Float*/
		{
			auto lightArea = DeserializeUtil(val, "lightArea", Math::ZERODELTA);
			ptr.SetlightArea(lightArea);

			auto lumFlux = DeserializeUtil(val, "lumFlux", Math::ONEDELTA);
			ptr.SetLumFlux(lumFlux);
			//!@todo Check ��� ��������� ������ ��� �������� � ���������� �� � ����, ��� �� �� �������� ��, � �� ID
						//auto diffirrad = DeserializeUtil<unsigned int>(val, "diffirrad", 0);
						//ptr.SetDiffirrad_Env_and_Area(diffirrad);

						//auto specirrad = DeserializeUtil<unsigned int>(val, "specirrad", 0);
						//ptr.SetSpecirrad_Env_and_Area(specirrad);
		}

		/*��������� ��������� ���������*/
		ptr.ResetSavedState();
	}
	//---------------------------Object--------------------------
	/**
	*/
	template<> ENGINE_INLINE ENGINE_API
		void Deserializer::DeserializeModify<BaseEntity&>(const Value& val, BaseEntity& ptr) {
		if (val.IsNull()) {
			Warning("DeserializeModify([val[object],BaseEntity&) is NULL - check call");
			return;
		}

		if (val.HasMember("transform") == true) {
			ptr.SetTransform(Deserialize<Mat4>(val["transform"], Mat4::IDENTITY));
		}
		ptr.SetUpVector(DeserializeUtil(val, "upVector", Vec3::ZERO));
		ptr.SetAngle(DeserializeUtil(val, "angle", Math::ZERODELTA));
	}

	//---------------------------BaseVisualObjectEntity--------------------------
	template<> ENGINE_INLINE ENGINE_API
		void Deserializer::DeserializeModify<BaseVisualObjectEntity&>(const Value& val, BaseVisualObjectEntity& ptr)
	{
		if (val.IsNull()) {
			Warning("DeserializeModify([val[object],BaseVisualObjectEntity&) is NULL - check call");
			return;
		}

		/*Specific BaseVisualObjectEntity data started*/
		if (val.HasMember("LoadingPath") == true) {
			String path = Deserialize<String>(val["LoadingPath"], "");
			{
				if (path.empty())
				{
					ErrorWrite("[DeserializeModify BaseVisualObjectEntity] empty path. Mesh not loaded");
					return;
				}
				ptr.LoadFromPath(path);
			}
		}

		ptr.SetRenderLayer((RenderLayer)DeserializeUtil<uintptr_t>(val, "renderLayer", RenderLayer::LAYER_1));
		ptr.UseHWOcclusionQuery(DeserializeUtil<bool>(val, "UseHWQuery", false));

		/*Materials*/
		{
			if (val.HasMember("materials") == true) {
				const rapidjson::Value& materials = val["materials"];
				for (SizeType i = 0; i < materials.Size(); i++)
				{
					if (materials[i].IsNull())
					{
						ErrorWrite("[DeserializeModify BaseVisualObjectEntity-Mesh] empty subsetName or matName. Material not applied(materials[i].IsNull())");
						continue;
					}

					const Value& matValue = materials[i]["material"];
					const Value& subValue = materials[i]["subset"];

					if (matValue.IsNull() || subValue.IsNull())
					{
						ErrorWrite("[DeserializeModify BaseVisualObjectEntity-Mesh] empty subsetName or matName. Material not applied(matValue.IsNull() || subValue.IsNull())");
						continue;
					}

					auto matName = Deserialize<String>(matValue, "");
					auto subsetName = Deserialize<String>(subValue, "");

					if ((subsetName.empty()) || (matName.empty()))
					{
						ErrorWrite("[DeserializeModify BaseVisualObjectEntity-Mesh] empty subsetName or matName. Material not applied");
						continue;
					}

					ptr.SetMaterial(matName, subsetName);
				}
			}
		}
	}

	//---------------------------BasePhysicsObjectEntity--------------------------
	template<> ENGINE_INLINE ENGINE_API
		void Deserializer::DeserializeModify<BasePhysicsObjectEntity<ObjectMesh*>&>(const Value& val, BasePhysicsObjectEntity<ObjectMesh*>& ptr)
	{
		if (val.IsNull()) {
			Warning("DeserializeModify([val[object],BasePhysicsObjectEntity&) IsNull - check fail");
			return;
		}

		/*Collision*/
		if (val.HasMember("PhysBodyDesc") == false)
		{
			Warning("DeserializeModify([val[object],BasePhysicsObjectEntity&) HasMember PhysBodyDesc - check fail");
			return;
		}

		{
			const rapidjson::Value& physBodyDescBlock = val["PhysBodyDesc"];
			if (physBodyDescBlock.IsNull()) {
				Warning("DeserializeModify(const Value& val, BasePhysicsObjectEntity& ptr) physBodyBlock IsNull");
				return;
			}

			ptr.SetPhysicsByDesc(Deserialize<PhysBodyDescSh>(physBodyDescBlock, PhysBodyDescSh()));

			// TODO: uncomment this and implement
			//// IMPACT SOUND
						//{
						//	String str = DeserializeUtil<String>(physBodyDescBlock, "ImpactSound", "");
						//	if (str.empty() == false)
						//		ptr.SetImpactSound(str);
						//}
		}
	}

	//---------------------------Camera--------------------------
	template<> ENGINE_INLINE ENGINE_API
		void Deserializer::DeserializeModify<Camera*>(const Value& val, Camera* _cam) {
		if (val.IsNull()) {
			Warning("DeserializeModify([val[object],Camera&) is null - check call");
			return;
		}

		auto _scene = GetScene();
		ASSERT(_cam, "Not exist _cam");
		ASSERT(_scene, "Not exist _scene");

		auto type = static_cast<Camera::CameraType>(DeserializeUtil<unsigned int>(val, "type", Camera::CAMERA_INVALID)); //-V205

		if (_cam->GetCameraType() != type)
		{
			Warning("Constructed: %i Requested: %i", _cam->GetCameraType(), type);
			Error("[Deserializer::DeserializeModify] camera - invalid created type", true);
			return;
		}

		_cam->SetPhysics(DeserializeUtil(val, "phSize", Vec3::ZERO));
		_cam->SetUpVector(DeserializeUtil(val, "upVector", Vec3::ZERO));
		_cam->SetPosition(DeserializeUtil(val, "position", Vec3::ZERO));
		_cam->SetDirection(DeserializeUtil(val, "direction", Vec3::ZERO));
		_cam->SetMaxVelocity(DeserializeUtil(val, "maxVelocity", Math::ZERODELTA));
		_cam->SetFOV(DeserializeUtil(val, "fov", Math::ZERODELTA));
		// ClipPlanes
		_cam->SetZNear(DeserializeUtil(val, "ZNear", 0.1));
		_cam->SetZFar(DeserializeUtil(val, "ZFar", 1e4));
		// Euler Angles
		{
			if (val.HasMember("Angles") == true) {
				auto angles = Deserialize<Vec2>(val["Angles"], Vec2::ZERO);
				{
					_cam->SetAngle(EulerAngle::ANGLE_0, angles.x);
					_cam->SetAngle(EulerAngle::ANGLE_1, angles.y);
				}
			}
		}

		//TODO:And Object state needly des

		/*������ �������� �������*/
		_scene->m_ActiveScene.SetCameraAndDestroyOlder(_cam);
	}

	ENGINE_INLINE void _DeserializeNewCamera(const Value& val, Deserializer& _des) {
		if (val.IsNull())
			return;

		auto type = static_cast<Camera::CameraType>(_des.DeserializeUtil<unsigned int>(val, "type", Camera::CAMERA_INVALID)); //-V205

		Camera* cam = nullptr;

		switch (type)
		{
		case Camera::CAMERA_FPS:
#ifdef DEBUG_SERIALIZER
			Debug("Creation CAMERA_FPS cam");
#endif
			cam = new CameraFPS(false);
			break;
		case Camera::CAMERA_FREE:
#ifdef DEBUG_SERIALIZER
			Debug("Creation CAMERA_FREE cam");
#endif
			cam = new CameraFree(false);
			break;
		case Camera::CAMERA_FIXED:
#ifdef DEBUG_SERIALIZER
			Debug("Creation CAMERA_FIXED cam");
#endif
			cam = new CameraFixed();
			break;

		default:
#ifdef DEBUG_SERIALIZER
			Debug("Creation CAMERA cam");
#endif
			break;
		}

		if (!cam)
		{
			Error("[Deserializer::_DeserializeNewCamera] camera - invalid creation", true);
			return;
		}

		_des.DeserializeModify(val, cam);
	}

	template<> ENGINE_INLINE ENGINE_API
		void Deserializer::DeserializeModify<MaterialPassSh&>(const Value& val, MaterialPassSh& pass) {
		if (val.IsNull())
		{
			Warning("INCORRECT FILE");
			return;
		}

		pass.SetName(Deserialize<String>(val["name"], "INVALID").c_str());
		{
			pass.castShadows = (DeserializeUtil<bool>(val, "castShadows", true));
			pass.recieveShadows = (DeserializeUtil<bool>(val, "recieveShadows", true));
		}
		pass.defines = (Deserialize<String>(val["defines"], ""));
		pass.hasAlphaTest = (DeserializeUtil<bool>(val, "hasAlphaTest", false));
		{
			pass.alphaFunc = static_cast<CompareType>(DeserializeUtil<uintptr_t>(val, "alphaFunc", static_cast<uintptr_t>(CompareType::COMP_NONE)));
			pass.alphaRef = DeserializeUtil(val, "alphaRef", Math::ZERODELTA);
		}

		pass.hasBlending = (DeserializeUtil<bool>(val, "hasBlending", false));
		{
			pass.src = static_cast<BlendParam>(DeserializeUtil<uintptr_t>(val, "blendSrc", static_cast<uintptr_t>(BlendParam::BLEND_NONE)));
			pass.dst = static_cast<BlendParam>(DeserializeUtil<uintptr_t>(val, "blendDst", static_cast<uintptr_t>(BlendParam::BLEND_NONE)));
		}

		pass.shaderName = (Deserialize<String>(val["shaderName"], "INCORRECT"));

		if (pass.shaderName.empty() || pass.shaderName == "INCORRECT")
		{
			ErrorWrite("[%s] shaderName is empty or INCORRECT. For material: %s", __FUNCTION__, filename.c_str());
			return;
		}

			//LogPrintf("Loading shader from cache");
		pass.m_Shader = Cache::LoadShader(pass.shaderName, pass.defines);

		if (pass.m_Shader == nullptr) {
				ErrorWrite("Not exist _render pointer %s %s %s", this->filename.c_str(), pass.shaderName.c_str(), pass.defines.c_str());
				return;
			}

		// Samplers
		{
			// samplers 2D
			{
				const rapidjson::Value& V = val["samplers2D"];
				for (SizeType i = 0; i < V.Size(); i++)
				{
					String SamplerName = "INVALID";
					String TextureName = "INVALID";
					bool needlyAddAtr = false;

					SamplerName = Deserialize<String>(V[i]["SamplerName"], "INVALID");
					TextureName = Deserialize<String>(V[i]["TextureName"], "INVALID");
					// todo: replace on needed generate normals
					needlyAddAtr = DeserializeUtil<bool>(V[i], "NeedlyAddAtr", false);
					// Value UNUSED

					if (TextureName.empty()
						|| SamplerName.empty()
						|| TextureName.compare("INVALID") == 0
						|| SamplerName.compare("INVALID") == 0)
					{
						ErrorWrite("[samplers2D] Someone empty");
						continue;
					}

					I_Texture* TexSampler = nullptr;

					auto texP = std::make_shared<TextureParamsTransfer>();
					texP->m_Type = TextureParamsTransfer::Type::TEXTURE_2D;
					texP->path = TextureName;

					if (needlyAddAtr)
					{
						texP->generateNormal = true;
						//!@todo � ��������� �������� �������
						texP->normalLevel = 4;
						texP->path += "_NORMAL_MAP";

						auto sampler = GetCache()->LoadTexture(texP);
						if (!sampler)
						{
							ErrorWrite("Not created sampler");
							continue;
						}
						TexSampler = sampler;
					}
					else
					{
						auto sampler = GetCache()->LoadTexture(texP);
						if (!sampler)
						{
							ErrorWrite("Not created sampler");
							continue;
						}
						TexSampler = sampler;
					}

					auto s1 = std::make_shared<MaterialSampler<I_Texture*>>(TextureName.c_str(), SamplerName.c_str(), TexSampler, needlyAddAtr);
					pass.u_sampler2D.push_back(s1);

#ifdef DEBUG_SERIALIZER
					DebugM("[samplers2D] Finished");
#endif
				}
			}
			// samplers Cube
			{
				const rapidjson::Value& V = val["samplersCube"];
				for (SizeType i = 0; i < V.Size(); i++)
				{
					String SamplerName = "INVALID";
					String TextureName = "INVALID";
					bool needlyAddAtr = false;
					// Value UNUSED

					SamplerName = Deserialize<String>(V[i]["SamplerName"], "INVALID");
					TextureName = Deserialize<String>(V[i]["TextureName"], "INVALID");
					needlyAddAtr = DeserializeUtil<bool>(V[i], "NeedlyAddAtr", false);

					if (TextureName.empty()
						|| SamplerName.empty()
						|| TextureName.compare("INVALID") == 0
						|| SamplerName.compare("INVALID") == 0)
					{
						ErrorWrite("[samplersCube] Someone empty");
						continue;
					}

					auto texP = std::make_shared<TextureParamsTransfer>();
					texP->m_Type = TextureParamsTransfer::Type::TEXTURE_CUBE;
					texP->path = TextureName;

					auto TexValue = GetCache()->LoadTexture(texP);

					if (!TexValue)
						continue;

					auto s1 = std::make_shared<MaterialSampler<I_Texture*>>(TextureName.c_str(), SamplerName.c_str(), TexValue, needlyAddAtr);
					pass.u_samplerCube.push_back(s1);

#ifdef DEBUG_SERIALIZER
					DebugM("[samplersCube] Finished");
#endif
				}
			}
			// samplers float
			{
				const rapidjson::Value& V = val["samplersFloat"];
				for (SizeType i = 0; i < V.Size(); i++)
				{
					String SamplerName = "INVALID";
					String TextureName = "INVALID";
					bool needlyAddAtr = false;
					auto value = Math::ZERODELTA;

					SamplerName = Deserialize<String>(V[i]["SamplerName"], "INVALID");
					TextureName = Deserialize<String>(V[i]["TextureName"], "INVALID");
					needlyAddAtr = DeserializeUtil<bool>(V[i], "NeedlyAddAtr", false);
					value = DeserializeUtil(V[i], "Value", Math::ZERODELTA);

					if (TextureName.empty()
						|| SamplerName.empty()
						|| TextureName.compare("INVALID") == 0
						|| SamplerName.compare("INVALID") == 0)
					{
						ErrorWrite("[samplersFloat] Someone empty");
						continue;
					}

					auto s1 = std::make_shared<MaterialSampler<float>>(TextureName.c_str(), SamplerName.c_str(), value, needlyAddAtr);
					pass.u_float.push_back(s1);

#ifdef DEBUG_SERIALIZER
					DebugM("[samplersFloat] Finished");
#endif
				}
			}
			// samplers vec2
			{
				const rapidjson::Value& V = val["samplersVec2"];
				for (SizeType i = 0; i < V.Size(); i++)
				{
					String SamplerName = "INVALID";
					String TextureName = "INVALID";
					bool needlyAddAtr = false;
					Vec2 value = Vec2::ZERO;

					SamplerName = Deserialize<String>(V[i]["SamplerName"], "INVALID");
					TextureName = Deserialize<String>(V[i]["TextureName"], "INVALID");
					needlyAddAtr = DeserializeUtil<bool>(V[i], "NeedlyAddAtr", false);
					value = Deserialize<Vec2>(V[i]["Value"], Vec2::ZERO);

					if (TextureName.empty()
						|| SamplerName.empty()
						|| TextureName.compare("INVALID") == 0
						|| SamplerName.compare("INVALID") == 0)
					{
						ErrorWrite("[samplersVec2] Someone empty");
						continue;
					}

					// Format: TextureName, uniformName, TexturePTR
					auto s1 = std::make_shared<MaterialSampler<Vec2>>(TextureName.c_str(), SamplerName.c_str(), value, needlyAddAtr);
					pass.u_Vec2.push_back(s1);

#ifdef DEBUG_SERIALIZER
					DebugM("[samplersVec2] Finished");
#endif
				}
			}

			// samplers vec3
			{
				const rapidjson::Value& V = val["samplersVec3"];
				for (SizeType i = 0; i < V.Size(); i++)
				{
					String SamplerName = "INVALID";
					String TextureName = "INVALID";
					bool needlyAddAtr = false;
					Vec3 value = Vec3::ZERO;

					SamplerName = Deserialize<String>(V[i]["SamplerName"], "INVALID");
					TextureName = Deserialize<String>(V[i]["TextureName"], "INVALID");
					needlyAddAtr = DeserializeUtil<bool>(V[i], "NeedlyAddAtr", false);
					value = DeserializeUtil(V[i], "Value", Vec3::ZERO);

					if (TextureName.empty()
						|| SamplerName.empty()
						|| TextureName.compare("INVALID") == 0
						|| SamplerName.compare("INVALID") == 0)
					{
						ErrorWrite("[samplersVec3] Someone empty");
						continue;
					}

					// Format: TextureName, uniformName, TexturePTR
					auto s1 = std::make_shared<MaterialSampler<Vec3>>(TextureName.c_str(), SamplerName.c_str(), value, needlyAddAtr);
					pass.u_Vec3.push_back(s1);

#ifdef DEBUG_SERIALIZER
					DebugM("[samplersVec3] Finished");
#endif
				}
			}
			// samplers vec4
			{
				const rapidjson::Value& V = val["samplersVec4"];
				for (SizeType i = 0; i < V.Size(); i++)
				{
					String SamplerName = "INVALID";
					String TextureName = "INVALID";
					bool needlyAddAtr = false;
					Vec4 value = Vec4::ZERO;

					SamplerName = Deserialize<String>(V[i]["SamplerName"], "INVALID");
					TextureName = Deserialize<String>(V[i]["TextureName"], "INVALID");
					needlyAddAtr = DeserializeUtil<bool>(V[i], "NeedlyAddAtr", false);
					value = Deserialize<Vec4>(V[i]["Value"], Vec4::ZERO);

					if (TextureName.empty()
						|| SamplerName.empty()
						|| TextureName.compare("INVALID") == 0
						|| SamplerName.compare("INVALID") == 0)
					{
						ErrorWrite("[samplersVec4] Someone empty");
						continue;
					}

					// Format: TextureName, uniformName, TexturePTR
					auto s1 = std::make_shared<MaterialSampler<Vec4>>(TextureName.c_str(), SamplerName.c_str(), value, needlyAddAtr);
					pass.u_Vec4.push_back(s1);

#ifdef DEBUG_SERIALIZER
					DebugM("[samplersVec3] Finished");
#endif
				}
			}
			// samplers Mat4
			{
				const rapidjson::Value& V = val["samplersMat4"];
				for (SizeType i = 0; i < V.Size(); i++)
				{
					String SamplerName = "INVALID";
					String TextureName = "INVALID";
					bool needlyAddAtr = false;
					Mat4 value = Mat4::ZERO;

					SamplerName = Deserialize<String>(V[i]["SamplerName"], "INVALID");
					TextureName = Deserialize<String>(V[i]["TextureName"], "INVALID");
					needlyAddAtr = DeserializeUtil<bool>(V[i], "NeedlyAddAtr", false);
					value = Deserialize<Mat4>(V[i]["Value"], Mat4::ZERO);

					if (TextureName.empty()
						|| SamplerName.empty()
						|| TextureName.compare("INVALID") == 0
						|| SamplerName.compare("INVALID") == 0)
					{
						ErrorWrite("[samplersMat4] Someone empty");
						continue;
					}

					// Format: TextureName, uniformName, TexturePTR
					auto s1 = std::make_shared<MaterialSampler<Mat4>>(TextureName.c_str(), SamplerName.c_str(), value, needlyAddAtr);
					pass.u_Mat4.push_back(s1);

#ifdef DEBUG_SERIALIZER
					DebugM("[samplersMat4] Finished");
#endif
				}
			}
			// samplers SceneParam
			{
				const rapidjson::Value& V = val["samplersSceneParams"];
				for (SizeType i = 0; i < V.Size(); i++)
				{
					String SamplerName = "INVALID";
					String TextureName = "INVALID";
					bool needlyAddAtr = false;
					uintptr_t value = static_cast<uintptr_t>(MaterialsSceneParam::InCorrect);

					SamplerName = Deserialize<String>(V[i]["SamplerName"], "INVALID");
					TextureName = Deserialize<String>(V[i]["TextureName"], "INVALID");
					needlyAddAtr = DeserializeUtil<bool>(V[i], "NeedlyAddAtr", false);
					value = DeserializeUtil<uintptr_t>(V[i], "Value", static_cast<uintptr_t>(MaterialsSceneParam::InCorrect));

					if (TextureName.empty()
						|| SamplerName.empty()
						|| TextureName.compare("INVALID") == 0
						|| SamplerName.compare("INVALID") == 0
						|| value == static_cast<uintptr_t>(MaterialsSceneParam::InCorrect))
					{
						ErrorWrite("[samplersSceneParams] Someone empty");
						continue;
					}

					// Format: TextureName, uniformName, TexturePTR
					auto s1 = std::make_shared<MaterialSampler<MaterialsSceneParam>>(TextureName, SamplerName, static_cast<MaterialsSceneParam>(value), needlyAddAtr);
					pass.u_scene_params.push_back(s1);

#ifdef DEBUG_SERIALIZER
					DebugM("[samplersSceneParams] Finished");
#endif
				}
			}
		}
	}

	template<> ENGINE_INLINE ENGINE_API
		void Deserializer::DeserializeModify<Material&>(const Value& val, Material& material)
	{
		if (val.IsNull())
		{
			ErrorWrite("DeserializeModify Material is NULL");
			return;
		}

		/*������������� ������� ���� - �����*/
		material.SetBaseColor(DeserializeUtil<Color>(val, "BaseColor", Color::WHITE));
		/*������������� ��������� �� ���������*/
		material.SetRoughness(DeserializeUtil(val, "Roughness", Math::ONEDELTA));
		material.SetMetalness(DeserializeUtil(val, "Metalness", Math::ZERODELTA));

		material.SetHasTexture(DeserializeUtil<bool>(val, "hasTexture", false));
		material.SetHasNormal(DeserializeUtil<bool>(val, "hasNormal", false));
		material.SetTransparent(DeserializeUtil<bool>(val, "isTransparent", false));
		/*������������� BRDF ������*/
		material.SetBRDFModel(DeserializeUtil<uintptr_t>(val, "BRDF", static_cast<uintptr_t>(MaterialSettingsNode::BRDF_MODEL::DEFAULT)));
		/*������������� ��� �����*/
		material.SetPath(filename);

		const rapidjson::Value& data = val["passes"];
#ifdef DEBUG_SERIALIZER
		DebugM("Count of passes %i", data.Size());
#endif
		/*������ ��������*/
		std::vector<std::shared_ptr<MaterialPassSh>> _passes;

		/*�������� � ��������� ������*/
		for (SizeType i = 0; i < data.Size(); i++)
		{
#ifdef DEBUG_SERIALIZER
			DebugM("Pass with id:%i starter", i);
#endif
			/*Memory will cleaned in destructor*/
			auto pass = std::make_shared<MaterialPassSh>();
			DeserializeModify<MaterialPassSh&>(data[SizeType(i)], *pass);
			_passes.push_back(pass);
		}

		/*����������� ������*/
		material.SetListOfPasses(_passes);
		material.LoadingFinishedEvent();
	}

	/**
	*/
	template<> ENGINE_INLINE ENGINE_API
		void Deserializer::DeserializeModify<ObjectMesh&>(const Value& val, ObjectMesh& ptr) {
		if (val.IsNull()) {
			Warning("DeserializeModify([val[object],ObjectMesh&) is null - check call");
			return;
		}

#ifdef DEBUG_SERIALIZER
		Debug("ObjectMesh - started");
#endif

		/*Visual BaseEntity*/
		{
			const rapidjson::Value& baseVisualObjectEntityBlock = val["BaseVisualObjectEntity"];
			if (baseVisualObjectEntityBlock.IsNull())
			{
				ErrorWrite("[DeserializeModify ObjectMesh-Collision] NULL baseVisualObjectEntityBlock. Collision not applied");
				return;
			}

			DeserializeModify<BaseVisualObjectEntity&>(baseVisualObjectEntityBlock, *(BaseVisualObjectEntity*)&ptr);
		}
#ifdef DEBUG_SERIALIZER
		Debug("ObjectMesh - finished  Visual object");
#endif

		/**/
		ptr.SetEnabled(DeserializeUtil<bool>(val, "Enabled", true));
#ifdef DEBUG_SERIALIZER
		Debug("ObjectMesh - finished  Enabled object");
#endif

		/*Collision*/
		{
			const rapidjson::Value& physBodyBlock = val["BasePhysicsObjectEntity"];
			if (physBodyBlock.IsNull())
			{
				ErrorWrite("[DeserializeModify ObjectMesh-Collision] NULL physBodyBlock. Collision not applied");
				return;
			}

			
			DeserializeModify<BasePhysicsObjectEntity<ObjectMesh*>&>(physBodyBlock, *ptr.m_PhysicsEntity);
		}
		/*BaseEntity*/
		{
#ifdef DEBUG_SERIALIZER
			Debug("ObjectMesh - Object - started");
#endif

			/*default object value*/
			DeserializeModify<BaseEntity&>(val["object"], *(BaseEntity*)&ptr);

#ifdef DEBUG_SERIALIZER
			Debug("ObjectMesh - Object - finished object");
#endif
		}

		/*��������� ��������� ���������*/
		ptr.ResetSavedState();
#ifdef DEBUG_SERIALIZER
		Debug("ObjectMesh - finished BasePhysicsObjectEntity object");
#endif
	}

	/**
	*/
	template<> ENGINE_INLINE ENGINE_API
		void Deserializer::DeserializeModify<PhysXMaterialSh&>(const Value& val, PhysXMaterialSh& ptr) {
		if (val.IsNull()) {
			Warning("PhysXMaterialSh - IsNull");
			return;
		}

#ifdef DEBUG_SERIALIZER
		Debug("DeserializeModify(const Value& val, PhysXMaterialSh& ptr) - started");
#endif

		auto staticFriction = DeserializeUtil(val, "staticFriction", Math::ONEDELTA);
		auto dynamicFriction = DeserializeUtil(val, "dynamicFriction", Math::ONEDELTA);
		auto restitution = DeserializeUtil(val, "restitution", Math::ONEDELTA);

		ptr.SetDynamicFriction(dynamicFriction);
		ptr.SetStaticFriction(staticFriction);
		ptr.SetRestitution(restitution);

#ifdef DEBUG_SERIALIZER
		Debug("DeserializeModify(const Value& val, PhysXMaterialSh& ptr) - finished");
#endif
	}

	template<> ENGINE_INLINE ENGINE_API
		PhysXMaterialSh*
		Deserializer::Deserialize<PhysXMaterialSh*>(const Value& val, PhysXMaterialSh* defV)
	{
		if (val.IsNull()) {
			return defV;
		}

		auto materialName = Deserialize<String>(val["materialName"], false);
		auto staticFriction = DeserializeUtil(val, "staticFriction", Math::ONEDELTA);
		auto dynamicFriction = DeserializeUtil(val, "dynamicFriction", Math::ONEDELTA);
		auto restitution = DeserializeUtil(val, "restitution", Math::ONEDELTA);
		auto vehicleDrivableSurface = DeserializeUtil<bool>(val, "vehicleDrivableSurface", false);

		auto ph = GetPhysics();
		ASSERT(ph, "Invalid pointer on Physics system");
		if (!ph)
			return nullptr;

		auto pxPhysics = ph->GetPxPhysics();
		ASSERT(pxPhysics, "Invalid pointer on pxPhysics");
		if (!pxPhysics)
			return nullptr;

		return phMatManager.AllocMaterial(pxPhysics, staticFriction, dynamicFriction, restitution, materialName, vehicleDrivableSurface);
	}

	template<> ENGINE_INLINE ENGINE_API
		PhysXMaterialSh*
		Deserializer::DeserializeFromFile<PhysXMaterialSh*>(const String& _name, PhysXMaterialSh* _defV) {
		if (_name.empty()) {
			Warning("[Deserializer::DeserializeFromFile::PhysXMaterialSh] Invalid file name");
			return _defV;
		}

#ifdef DEBUG_SERIALIZER
		Debug("[Deserializer::DeserializeFromFile::PhysXMaterialSh] Invalid file name");
#endif

		Deserializer des(_name);
		auto res = des.Deserialize(des.doc, _defV);

#ifdef DEBUG_SERIALIZER
		Debug("[Deserializer::DeserializeFromFile::PhysXMaterialSh] Invalid file name - finished");
#endif
		return res;
	}
}