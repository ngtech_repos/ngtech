/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"

#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
//**************************************
#include "..\inc\EditableObject.h"
//**************************************
#include <Rpc.h>
#include <Objbase.h>
#pragma comment(lib, "Rpcrt4.lib")
//**************************************
#include "..\inc\EditorObjectManager.h"
//**************************************

namespace NGTech
{
	EditableObject::EditableObject()
	{
		/**/
		_InitEditorVars();
		GenerateNewGUID();

		bExportable = true;
		IsSelected = false;
		IsSelectable = true;
		IsLight = false;
		IsPrefab = false;
		IsNeedlyManualUpdate = false;

		/**/
		GetEditorObjectManager()->AddEditableObject(this);
	}

	EditableObject::EditableObject(const EditableObject&_other) {
		_Swap(_other);
	}

	EditableObject::~EditableObject()
	{
		/**/
		GetEditorObjectManager()->DeleteObject(this);
	}

	const char * EditableObject::GetGUIDString()
	{
#if IS_OS_WINDOWS
		//TODO: Implement check("CROSSPLATFORM");
		BYTE* actorGUID;
		UuidToStringA(&m_GUID, &actorGUID);
		return (char*)actorGUID;
#else
		return nullptr;
#endif
	}

	void EditableObject::GenerateNewGUID()
	{
#if IS_OS_WINDOWS
		//TODO: Implement check("Do check");
		if (true)//(Editor::Instance()->GetEditorMode())
			CoCreateGuid(&m_GUID);// Editor-spawned actors get GUIDs
		else
			ZeroMemory(&m_GUID, sizeof(GUID));
#endif
	}

	void EditableObject::ReInitEditorVars()
	{
		m_EditorVars.clear();
		_InitEditorVars();
	}

	void EditableObjectExtension::_InitEditorVars() {}
	void EditableObjectExtension::_AddEditorVars() {}
	void EditableObjectExtension::_AddEditorVars(const std::vector<EditorVar>&_vars) {
		for (auto &obj : _vars) {
			m_EditorVars.push_back(obj);
		}
	}
}

#endif