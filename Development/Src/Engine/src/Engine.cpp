/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
//***************************************************
#include "IGame.h"
//***************************************************
#include "../../Core/inc/CvarManager.h"
//***************************************************
#include "Engine.h"
#include "ALSystem.h"
#include "Cache.h"
#include "Config.h"
#include "Scene.h"
#include "GUI.h"
#include "VFS.h"
#include "RenderPipeline.h"
#include "EngineScriptInterp.h"
#include "../Platform/inc/WindowSystem_GLFW.h"
#include "../Platform/inc/WindowSystem.h"
#include "PhysSystem.h"
#include "Material.h"
//***************************************************
#include "GameUpdateJob.h"
#include "ScriptUpdateJob.h"

namespace NGTech
{
	/**
	*/
	I_RenderLowLevel* CreateRenderFabric(RenderDrv _drv);

	/**
	*/
	static constexpr ENGINE_INLINE bool _IsWindowFocusedUtil(I_Window* _iWindow, bool _isEditor)
	{
		ASSERT(_iWindow, __FUNCTION__);

		if (!_iWindow) return false;

		if (_isEditor)
			return true;

		// ����������, ���� �� ����� ������������ PVD
#if !defined(ENGINE_RELEASE) && (!defined(_FINALRELEASE) || defined(EMULATE_DEVELOPER_FINAL_RELEASE)) && !defined(NGTECH_STATIC_LIBS) && !IS_OS_ANDROID
		return true;
#else
		return _iWindow->IsWindowFocused();
#endif
	}

	/**
	*/
	static ENGINE_INLINE int _CalcucateBuildNumber()
	{
		using namespace std;
		const int startDay = 17;
		const int startMonth = 4;
		const int startYear = 2012;
		const char* monthId[12] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
		const int daysInMonth[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		auto buildDate = __DATE__;
		int days;
		int months = 0;
		int years;

		char month[16];
		char buffer[256];
		strcpy(buffer, buildDate);
		sscanf(buffer, "%s %d %d", month, &days, &years);
		for (int i = 0; i < 12; i++)
		{
			if (_stricmp(monthId[i], month)) continue;
			months = i;
			break;
		}
		auto buildId = (years - startYear) * 365 + days - startDay;
		for (int i = 0; i < months; i++)
			buildId += daysInMonth[i];
		for (int i = 0; i < startMonth - 1; i++)
			buildId -= daysInMonth[i];

		return buildId;
	}

	/**
	*/
	void Engine::_PreInitFast()
	{
		static bool _mInited = false;

		SetEngineState(STATE_LOADING);

		if (!_mInited)
		{
			SetCore(this);

			ASSERT(log && "[Engine::_PreInitF] Failed Init log");

			LogPrintf("Engine Version: %s build %d, Build Date : %s : %s", ENGINE_VERSION_STRING, _CalcucateBuildNumber(), __DATE__, __TIME__);

			cvars = std::make_shared<CVARManager>();
			if (!cvars)
				Warning("[Init] CVARManager Failed");

			ASSERT(cvars, "[Engine::_PreInitF] Failed Init CVARManager");

			plugins = new EnginePlugins();
			ASSERT(plugins, "[Init] EnginePlugins Failed");

			vfs = new FileSystem();
			ASSERT(vfs, "[Init] FileSystem Failed");

			// create threads
			gameUpdJob = new GameUpdateJob(this);
			ASSERT(gameUpdJob, "[Init] GameUpdateJob Failed");

			_mInited = true;

			// Scriptable
			m_ScriptUpdateJob = std::make_shared<ScriptUpdateJob>(ScriptUpdateJob::UPDATE_MODE::UPDATE);
			m_renderUpdateJob = std::make_shared<ScriptUpdateJob>(ScriptUpdateJob::UPDATE_MODE::UPDATE_RENDER);
			m_AIUpdateJob = std::make_shared<ScriptUpdateJob>(ScriptUpdateJob::UPDATE_MODE::UPDATE_AI);
			//!@todo ��������� � Physics, ������ ��� ��������� lua_state
			m_PhysicsUpdateJob = std::make_shared<ScriptUpdateJob>(ScriptUpdateJob::UPDATE_MODE::UPDATE_PHYSICS);
		}
		else
			ASSERT(_mInited == 1);
	}

	/**
	*/
	void Engine::_UpdateTimer() {
		elapsedTimeFromStart = _timer.GetElapsed();
	}

	/**
	*/
	void Engine::_PostUpdate()
	{
		ASSERT(this->platformM, "Invalid pointer on this->platformM");

		static const TimeDelta updateInterval = Math::ONEDELTA / TimeDelta(UpdateConfig::GlobalFrameRate);
		TimeDelta remainingTime = updateInterval - elapsedTimeFromStart;
		if (remainingTime > Math::ZERODELTA)
		{
			if (this->platformM)
				this->platformM->WaitAllTasks(remainingTime);

			std::this_thread::sleep_for(std::chrono::duration<TimeDelta>(remainingTime));
		}

		if (this->platformM)
			this->platformM->UpdatePlatform();
	}

	void Engine::_PreInit()
	{
		Debug("[Init] Engine::_preInit()");
		DebugM("debugger: %s\n", debugger_present() ? "ATTACHED" : "NOT ATTACHED");

		{
			_CreateWindow();
			ASSERT(iWindow, "[Init] Window Failed");
		}
		{
			iRender = CreateRenderFabric(RenderDrv::OpenGLDrv);
			ASSERT(iRender, "[Init] Render Failed");
		}
		{
			alSystem = new ALSystem(m_UseSingleThread, this);
			ASSERT(alSystem, "[Init] Audio Failed");
		}
		{
			physSystem = new PhysSystem(m_UseSingleThread);
			ASSERT(physSystem, "[Init] Physics Failed");
		}
		{
			cache = std::make_shared<Cache>();
			ASSERT(cache, "[Init] Cache Failed");
		}

		// initialize GUI
		{
			gui = new GUI(this);
			ASSERT(gui, "[Init] GUI Failed");
		}

		// initialize SceneManager
		{
			scene = new SceneManager(this);
			ASSERT(scene, "[Init] SceneManager Failed");
		}
		// initialize RenderPipeline
		{
			rpipeline = new RenderPipeline(this);
			ASSERT(rpipeline, "[Init] RenderPipeline Failed");
		}
		// initialize Script
		{
			RegisterLuaBindings(std::make_shared<EngineScriptInterp>());
		}
	}

	void Engine::_CreateWindow()
	{
		Debug(__FUNCTION__);
		ASSERT(cvars, "Invalid pointer on cvars");

		I_Window::PlatformDependVars m_Vars;
		m_Vars.width = this->cvars->r_width;
		m_Vars.height = this->cvars->r_height;
		m_Vars.bpp = this->cvars->r_bpp;

		m_Vars.zdepth = this->cvars->r_zdepth;
		m_Vars.fullscreen = this->cvars->r_fullscreen;
		m_Vars.withoutBorder = this->cvars->w_withoutBorder;

		WindowCallbacks m_WindowCallbacks;

#if IS_OS_WINDOWS
#ifndef DROP_EDITOR
		if (this->IsEditor()) {
			iWindow = new WindowSystem(this, m_Vars, m_WindowCallbacks);
			//!@todo ADD call CreateWindowImpl
			return;
		}
#endif
#endif

		iWindow = new WindowSystemGLFW(this, m_Vars, m_WindowCallbacks);
		iWindow->CreateWindowImpl();
	}

	/**
	*/
	void Engine::SetGame(IGame*_game) {
		ASSERT(_game, "Invalid pointer");
		ASSERT(vfs, "Invalid pointer");

		Debug("[Init] Engine::setGame()");
		if ((!_game) || (!vfs))
			return;

		game = std::shared_ptr<IGame>(_game);

		vfs->AddResourceLocation(app_path + "../" + _game->ResourceDirectory() + "/", true);
	}

	/**
	*/
	void Engine::Initialise(void* _hwnd)
	{
		Debug("[Init] Engine::Initialise()");
		static bool _mInited = false;
		if (!_mInited)
		{
			_PreInit();

			if (iWindow) {
				iWindow->Initialise(_hwnd);
				Debug("[Init] Window Finished");
			}
			else
				ASSERT(false, "Failed Initialise iWindow");

			if (iRender) {
				iRender->Initialise();
				Debug("[Init] Render Finished");
			}
			else
				ASSERT(false, "Failed Initialise iRender");

			if (alSystem) {
				alSystem->Initialise();
				Debug("[Init] Audio Finished");
			}
			else
				ASSERT(false, "Failed Initialise Audio");

			if (physSystem) {
				physSystem->Initialise();
				Debug("[Init] Physics Finished");
			}
			else
				ASSERT(false, "Failed Initialise Physics");

			_SetCriticalResources();
			Debug("[Init] FileSystem Finished");

			//initialize GUI
			if (gui) {
				gui->Initialise();
				Debug("[Init] GUI Finished");
			}
			else
				ASSERT(false, "Failed Initialise GUI");

			if (scene) {
				scene->Initialise();
				Debug("[Init] SceneManager Finished");
			}
			else
				ASSERT(false, "Failed Initialise SceneManager");

			//initialize RenderPipeline
			if (rpipeline) {
				rpipeline->Initialise();
				Debug("[Init] RenderPipeline Finished");
			}
			else
				ASSERT(false, "Failed Initialise RenderPipeline");

			//initialize reflection
			_InitReflection();

			//initialize all scripts bindings
			InitAllLuaBindings();

			//initialize Game
			if (game) {
				game->Initialise();
				Debug("[Init] Game Finished");
			}
			else
			{
				//!@todo FIX ME! : �� ��������
				if (IsEditor())
					Warning("Failed Initialise Game");
				else
					ASSERT(false, "Failed Initialise Game");
			}

			this->running = true;

			Debug("[Init] All Systems Initialised");

			_mInited = true;
		}
		else
			ASSERT(_mInited == 1);

		// �������� ������ �������
		_timer.Reset();
		_gameFrameInterval = Math::ONEDELTA / UpdateConfig::GlobalFrameRate;
		_gameFrameTime = Math::ZERODELTA;
	}

	/**
	*/
	Engine::~Engine() {
		_Destroy();
	}

	/**
	*/
	void Engine::EditorLoop()
	{
		if (IsRunning()) {
			_Loop();
		}
	}

	/**
	*/
	void Engine::MainLoop()
	{
		while (IsRunning())
		{
			_Loop();
		}
	}

	void Engine::_Loop()
	{
		BROFILER_NEXT_FRAME();
		BROFILER_FRAME("MainThread");

		ASSERT(iWindow, "Invalid pointer on iWindow");

		if (!iWindow)
			return;

		// �������� ������� �� ����� ��������
		// ��� ������ ��� �����

		this->iWindow->Update();

		// ���� ������ ��� ������ �� �����
		if (this->_LoopImpl() == false)
		{
			this->Quit();
			return;
		}
	}

	/**
	*/
	void Engine::DoUpdate(TimeDelta _f)
	{
		if (!IsRunning())
			return;

		BROFILER_CATEGORY("UpdateLogic", Brofiler::Color::Orchid);
		PROFILE;

		if (_IsWindowFocusedUtil(this->iWindow, this->IsEditor()))
		{
			// info:https://stackoverflow.com/questions/18591924/how-to-use-bitmask
			bool _paused = ((GetEngineState() & (STATE_PAUSED | STATE_PHYSICS_PAUSED | STATE_EDITOR_PAUSED | STATE_LOADING)) == (STATE_PAUSED | STATE_PHYSICS_PAUSED | STATE_EDITOR_PAUSED | STATE_LOADING));

			if (_paused == false)
			{
				{
					ASSERT(this->m_ScriptUpdateJob, "Not exist pointer on m_ScriptUpdateJob");

					if (this->m_ScriptUpdateJob)
						this->m_ScriptUpdateJob->Update();

					ASSERT(this->scene, "Not exist pointer on scene");

					if (this->scene)
						this->scene->Update(_f);

					ASSERT(this->m_AIUpdateJob, "Not exist pointer on m_AIUpdateJob");

					if (this->m_AIUpdateJob)
						this->m_AIUpdateJob->Update();

					ASSERT(this->m_PhysicsUpdateJob, "Not exist pointer on m_PhysicsUpdateJob");

					if (this->m_PhysicsUpdateJob)
						this->m_PhysicsUpdateJob->Update();
				}

				//!@todo Check this
				//!@todo DEPRECATED: MT not works
				/*ASSERT(this->platformM && this->gameUpdJob, "Not exist pointer on platformM or gameUpdJob");
				if (this->platformM && this->gameUpdJob)
				{
					this->platformM->RunTask(this->gameUpdJob, 1);
					this->platformM->WaitTask(this->gameUpdJob, 1);
				}*/

				if (this->game)
					this->game->Update();
			}
		}
	}

	/**
	*/
	void Engine::DoRender()
	{
		if (!IsRunning() || !(_IsWindowFocusedUtil(this->iWindow, this->IsEditor())))
			return;

		BROFILER_CATEGORY("Render", Brofiler::Color::Salmon);

		if ((_IsWindowFocusedUtil(this->iWindow, this->IsEditor())))
		{
			// ��� ��� ��� ������ ��������������, ����� �� ��������
			if (this->game && this->game->ec)
				this->game->RunEventsCallback();

			this->_UpdateTimer();

			// info:https://stackoverflow.com/questions/18591924/how-to-use-bitmask
			bool _pausedGameLogic = ((GetEngineState() & (STATE_PAUSED | STATE_PHYSICS_PAUSED | STATE_EDITOR_PAUSED)) == (STATE_PAUSED | STATE_PHYSICS_PAUSED | STATE_EDITOR_PAUSED));
			bool _loading = ((GetEngineState() & (STATE_LOADING)) == (STATE_LOADING));

			// ������ ����������, ����-�������� �� ����������, ��� ���� ����, ������� ��������� ��� ����������,
			// ������ ��� � IRender ����� ������������ ������ �� scene
			if (!this->scene)
				return;


			if (_loading)
			{
				Warning("Loading in progress");
				this->scene->DrawLoading(GetEngineState(), this->GetElapsedTimeFromStart());
				return;
			}

			if (this->iRender)
				this->iRender->BeginFrame();


			if (this->rpipeline)
				this->rpipeline->Update(_pausedGameLogic, this->GetElapsedTimeFromStart());

			if (this->m_renderUpdateJob)
				this->m_renderUpdateJob->Update();

			// Game update
			{
				if (this->game && this->game->rc)
					this->game->RunRenderCallback();

				if (this->game)
					this->game->Render();
			}

			if (this->iRender)
				this->iRender->EndFrame();

			// run single-threaded code
			if (m_UseSingleThread)
			{
				if (this->alSystem)
					this->alSystem->Update();
				else
					ASSERT("Not exist pointer on alSystem");

				if (this->physSystem)
					this->physSystem->UpdateSingleThreaded(GetElapsedTimeFromStart());
				else
					ASSERT("Not exist pointer on physSystem");
			}

			this->_PostUpdate();
		}
	}

	/**
	*/
	void Engine::Quit() {
		running = false;
		SetEngineState(STATE_PAUSED);
		_Destroy();
	}

	/**
	*/
	void Engine::_Destroy()
	{
		if (cvars)
			cvars->SaveConfig();

		if (platformM)
			platformM->StopJobs();

		SAFE_DELETE(gameUpdJob); //-V809
		SAFE_rESET(game);

		DestroyAllLuaBindings();
		SAFE_DELETE(rpipeline); //-V809
		SAFE_DELETE(gui); //-V809
		SAFE_DELETE(scene); //-V809

		SAFE_DELETE(physSystem); //-V809
		SAFE_DELETE(alSystem); //-V809

		SAFE_RELEASE(iRender);
		// We must use delete, because if call release, we got heap corruption
		SAFE_DELETE(iWindow);
	}

	/**
	*/
	void Engine::_GetAppDir() {
		app_path = Sys_EXEPath();
		app_path += "/";
	}

	/**
	*/
	void Engine::SetResources(const char* _name)
	{
		ASSERT(vfs, "Invalid pointer");

		if (!vfs) return;
		vfs->AddResourceLocation(app_path + _name, true);
	}

	/**
	*/
	void Engine::_SetCriticalResources() {
		ASSERT(vfs, "Invalid pointer");

		if (!vfs) return;
		vfs->AddResourceLocation(app_path + "../Engine/", true);
		//!@todo ADD CHECK AND CONFIG
		DefaultResources::InitDefaultResources(new Material("defmat.mat", false, true));
	}

	/**
	*/
	void Engine::LoadEngineModule(const char*_name) {
		ASSERT(plugins, "Invalid pointer");

		if (!plugins) return;
		plugins->LoadEngineModule(_name);
	}

	void Engine::LoadEngineModuleEditor(const char*_name) {
		ASSERT(plugins, "Invalid pointer");

		if (!plugins) return;
		plugins->LoadEngineModuleEditor(_name);
	}

	/**
	*/
	void Engine::LoadStartupModule(const char*_cfg, int argc, const char * const * argv)
	{
		Debug(__FUNCTION__);
		_LoadStartupModule(_cfg, argc, argv, false);
	}

	void Engine::_LoadStartupModule(const char*_cfg, int argc, const char * const * argv, bool isEditor)
	{
		Debug(__FUNCTION__);
#ifndef NGTECH_STATIC_LIBS
		Config config(_cfg);
		auto sys_game_dll = config.GetString("System", "sys_game_dll");
		auto sys_game_folder = config.GetString("System", "sys_game_folder");
		if (sys_game_dll.empty() || sys_game_folder.empty())
		{
			Error("Not valid value if game dll in main system config", true);
			return;
		}
		cvars->sys_game_dll = sys_game_dll.c_str();
		cvars->sys_game_folder = sys_game_folder.c_str();

		ParseCommandLine(argc, argv);

		SetResources((app_path + "../" + cvars->sys_game_folder + "/").c_str());
		if (isEditor)
		{
			LogPrintf("[Engine] Loading engine module: %s in editor mode", cvars->sys_game_dll.c_str());
			LoadEngineModuleEditor(cvars->sys_game_dll.c_str());
		}
		else
		{
			LogPrintf("[Engine] Loading engine module: %s in game mode", cvars->sys_game_dll.c_str());
			LoadEngineModule(cvars->sys_game_dll.c_str());
		}
#else
		Config config(_cfg);
		auto sys_game_folder = config.GetString("System", "sys_game_folder");
		if (sys_game_folder.empty())
		{
			Error("Not valid value if game folder in main system config", true);
			return;
		}
		cvars->sys_game_folder = sys_game_folder.c_str();

		ParseCommandLine(argc, argv);

		SetResources((app_path + "../" + cvars->sys_game_folder + "/").c_str());

		if (isEditor)
		{
			void GameEntryPointEditor();
			GameEntryPointEditor();
		}
		else
		{
			void GameEntryPoint();
			GameEntryPoint();
		}
#endif
	}

	/**
	*/
	void Engine::LoadStartupModuleEditor(const char*_cfg, int argc, const char * const * argv) {
		Debug(__FUNCTION__);
		_LoadStartupModule(_cfg, argc, argv, true);
	}

	// see https://martalex.gitbooks.io/gameprogrammingpatterns/content/chapter-3/3.2-game-loop.html
	//!@todo see https://web.archive.org/web/20140319005648/http://www.richardfine.co.uk/junk/unity%20lifetime.png
	bool Engine::_LoopImpl()
	{
		TimeDelta frametime = _timer.Frametime();

		_gameFrameTime += frametime;

		//!@todo DEPRECATED: It's not works, so ue directly call
		//while (_gameFrameTime >= _gameFrameInterval)
		{
			this->DoUpdate(_gameFrameInterval);

			if (m_onUpdate_callback)
				m_onUpdate_callback->_FixedUpdate(_gameFrameInterval);

			_gameFrameTime -= _gameFrameInterval;
		}

		this->DoRender();

		if (m_onUpdate_callback)
			m_onUpdate_callback->_VariableUpdate(frametime);

		_timer.UpdateFrametime();

		return true;
	}
}