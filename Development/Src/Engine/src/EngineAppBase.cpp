/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
#include "EngineAppBase.h"
#include "../../Common/IGame.h"

namespace NGTech {
	/**
	*/
	EngineAppBase::EngineAppBase()
		:engine(nullptr)
	{
		engine = GetEngine();
		ASSERT(engine, "engine pointer is null");
	}

	/**
	*/
	EngineAppBase::EngineAppBase(void* _hwnd, bool _isEditor, IGame*_game, ICallback* rc, ICallback* ev)
		:EngineAppBase()
	{
		ASSERT(engine, "engine pointer is null");
		engine->RunEditor(_isEditor);
		engine->SetGame(_game);
		engine->Initialise(_hwnd);

		auto game = GetGame();
		if (rc&&game)
			game->SetRenderCallback(rc);

		if (ev&&game)
			game->SetEventsCallback(ev);

		Update();
	}

	/**
	*/
	EngineAppBase::EngineAppBase(void* _hwnd, bool _isEditor)
		:EngineAppBase()
	{
		ASSERT(engine, "engine pointer is null");
		engine->RunEditor(_isEditor);
		engine->Initialise(_hwnd);

		Update();
	}

	/**
	*/
	EngineAppBase::EngineAppBase(IGame*_game, ICallback* rc, ICallback* ev)
		:EngineAppBase(0, false, _game, rc, ev)
	{}

	/**
	*/
	void EngineAppBase::Update()
	{
		if (!engine) return;

		if (engine->IsEditor())
			engine->EditorLoop();
		else
			engine->MainLoop();
	}

	void EngineAppBase::Run()
	{
		if (!engine) return;
		engine->MainLoop();
	}
}