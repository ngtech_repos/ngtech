/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"

#include "EditorObjectManager.h"

namespace NGTech {
	static Engine* engine = nullptr;

	Engine* GetEngine() {
		if (!engine) {
			engine = new Engine();
		}
		return engine;
	}

	void DestroyEngine() {
		delete engine;
		engine = nullptr;
	}

#ifndef DROP_EDITOR
	const std::shared_ptr<EditorObjectManager>& GetEditorObjectManager()
	{
		static auto m_editorManager = std::make_shared<EditorObjectManager>();
		ASSERT(m_editorManager, "m_editorManager is null");

		return m_editorManager;
	}
#endif
}