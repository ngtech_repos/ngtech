/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"

namespace NGTech
{
	//-----------------------------------------------------------------------------
	// For plugin system...
	typedef void(*DLL_START_PLUGIN)(void);
	typedef void(*DLL_STOP_PLUGIN)(void);
	//-----------------------------------------------------------------------------
	void EnginePlugins::LoadEngineModule(const char *modulename)
	{
		LoadEngineModuleFuncName(modulename, "dllStartPlugin");
	}

	void EnginePlugins::LoadEngineModuleEditor(const char *modulename)
	{
		LoadEngineModuleFuncName(modulename, "dllStartPluginEditor");
	}

	void EnginePlugins::LoadEngineModuleFuncName(const char *modulename, const char* _funcName)
	{
		// Load plugin library
		DynLib module(modulename);
		module.Load();

		// Call startup function
		DLL_START_PLUGIN pFunc = (DLL_START_PLUGIN)module.GetSymbol(_funcName);

		if (!pFunc)
		{
			Warning("Cannot find symbol dllStartPluginEditor in library %s", modulename);
			return;
		}
		else
		{
			String str = "Found symbol dllStartPluginEditor in library ";
			str += modulename;
			Debug(str);
			pFunc();
		}

		// Store for later unload
		modules.push_back(&module);
	}
}