#include "EnginePrivate.h"

#include "EditorObjectManager.h"

// Lights
#include "LightPoint.h"
#include "LightDirect.h"
#include "LightEnvProbe.h"
#include "LightSpot.h"
#include "LightArea.h"
// ObjectMesh
#include "ObjectMesh.h"
#include "AnimatedMesh.h"

namespace NGTech
{
#if REFLECTION_USE_DEBUG_GLOBAL_CONTAINER
	ENGINE_API ReflectionCreatorsContainer* _g_ReflectionCreatorsContainer = new ReflectionCreatorsContainer();
#endif

	void Engine::_InitReflection() {
#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
		auto edM = GetEditorObjectManager();
		ASSERT(edM, "Editor manager is null");
		if (!edM)
			return;

		edM->MakeAvalibileForReflection<LightPoint>();
		edM->MakeAvalibileForReflection<LightDirect>();
		edM->MakeAvalibileForReflection<LightEnvProbe>();
		edM->MakeAvalibileForReflection<LightSpot>();
		edM->MakeAvalibileForReflection<LightArea>();
		edM->MakeAvalibileForReflection<ObjectMesh>();
#endif
	}
}