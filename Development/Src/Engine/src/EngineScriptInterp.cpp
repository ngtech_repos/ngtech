/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
//***************************************************
#include "EngineScriptInterp.h"
//***************************************************
#include "lua/luabind.hpp"
#include "lua/operator.hpp"
#include "lua/lua.hpp"
//***************************************************
#include "Light.h"
#include "LoadingScreen.h"
//***************************************************
#include "../Core/inc/lua/out_value_policy.hpp"
//***************************************************
#include "../../API/NGTechEngineAPI.h"
//***************************************************
#include "../inc/PhysXCharacterController.h"
//***************************************************

namespace NGTech {
	EngineScriptInterp::EngineScriptInterp() {}

	bool EngineScriptInterp::Initialise() {
		if (!CoreScriptInterp::Initialise())
		{
			ErrorWrite("failed CoreScriptInterp::Initialise()");
			return false;
		}

		bindEngineAPI();

		return true;
	}

	void EngineScriptInterp::bindEngineAPI() {
		using namespace luabind;
		using namespace NGTech;

		try
		{
			auto state = GetLuaState();
			if (!state)
			{
				ErrorWrite("failed EngineScriptInterp::bindEngineAPI()");
				return;
			}

			// pointer for API_LoadScene(String); with optional param
			typedef void(*LoadConstStringFunctionBoolFalse)(const String&);
			typedef void(*LoadingConstCharFunction)(const char*);

			//////////////////////////////////////////////////////////////////////////
			///////////////////////////////Engine API/////////////////////////////////
			//////////////////////////////////////////////////////////////////////////
			module(state)
				[
					def("API_LoadEngineModule", &API_LoadEngineModule),
					def("API_LoadStartupModule", &API_LoadStartupModule),
					def("API_GetCurrentCamera", &API_GetCurrentCamera),
					def("API_SaveScene", &API_SaveScene),
					def("API_LoadScene", &API_LoadSceneNew),
					def("API_NewScene", &API_NewScene),
					def("API_SetLoadingScreen", (LoadingConstCharFunction)&API_SetLoadingScreen),//!CHECK
					def("API_SetGravity", &API_SetGravity),//!CHECK
					def("API_SaveMaterial", (LoadConstStringFunctionBoolFalse)&API_SaveMaterial),//!
					// TODO:Engine:Scripting: uncomment this!
					//def("API_LoadMaterial", (LoadConstStringFunctionBoolFalse)&API_LoadMaterial),//!
					def("API_CreateLight", &API_CreateLight)//!
				];

			/*WINDOW*/
			module(state)
				[
					def("API_SetWindowTitle", &API_SetWindowTitle),
					def("API_Window_GetTicks", &API_Window_GetTicks)
				];

			//RENDER
			module(state)
				[
					def("API_Make_ScreenShot", &API_Make_ScreenShot),
					def("API_SetAmbient", &API_SetAmbient),
					def("API_ResizeWindow", &API_ResizeWindow)
				];

			//////////////////////////////////////////////////////////////////////////
			//////////////////////////////Engine Clases///////////////////////////////
			//////////////////////////////////////////////////////////////////////////
			//TODO:Engine:Script:Audio not implemented currently
			module(state)
				[
					//////////////////////////////////////////////////////////////////////////
					/////////////////////////////////LoadingScreen////////////////////////////
					//////////////////////////////////////////////////////////////////////////
					//{
					class_<LoadingScreen>("LoadingScreen")
					.def(constructor<const String&>())
				.def("Show", &LoadingScreen::Show)

				//}

				//////////////////////////////////////////////////////////////////////////
				/////////////////////////////////Frustum//////////////////////////////////
				//////////////////////////////////////////////////////////////////////////
				//{
				, class_<Frustum>("Frustum")
				.def("Get", &Frustum::Get)
				.def("Build", &Frustum::Build)

				//.def("isInside", &Frustum::isInside)
				//.def("isInside", &Frustum::isInside)
				//.def("isInside", &Frustum::isInside)
				//.def("isInside", &Frustum::isInside)
				//.def("isInside", &Frustum::isInside)

				//}

					//////////////////////////////////////////////////////////////////////////
					///////////////////////////////CameraFixed////////////////////////////////
					//////////////////////////////////////////////////////////////////////////
					//{
				, class_<CameraFixed>("CameraFixed")
				// Object specific methods

				.enum_("ObjectType")
				[
					value("OBJECT", ObjectType::OBJECT),
					value("OBJECT_MESH", ObjectType::OBJECT_MESH),
					value("OBJECT_SKELETEAL_MESH", ObjectType::OBJECT_SKELETEAL_MESH),
					value("OBJECT_PARTICLE_SYSTEM", ObjectType::OBJECT_PARTICLE_SYSTEM),
					value("OBJECT_CAMERA", ObjectType::OBJECT_CAMERA),
					value("OBJECT_USERCUSTOM1", ObjectType::OBJECT_USERCUSTOM1),
					value("OBJECT_USERCUSTOM2", ObjectType::OBJECT_USERCUSTOM2),
					value("OBJECT_USERCUSTOM3", ObjectType::OBJECT_USERCUSTOM3),
					value("OBJECT_USERCUSTOM4", ObjectType::OBJECT_USERCUSTOM4)
				]
			.enum_("EulerAngle")
				[
					value("ANGLE_0", EulerAngle::ANGLE_0),
					value("ANGLE_1", EulerAngle::ANGLE_1),
					value("ANGLE_COUNT", EulerAngle::ANGLE_COUNT)
				]

			//.def("AttachScript", &CameraFixed::AttachScript)
			//.def("AttachScript", &CameraFixed::AttachScript)
			//.def("AttachScript", &CameraFixed::AttachScript)

			.def("SetTransform", &CameraFixed::SetTransform)
				.def("GetTransform", &CameraFixed::GetTransform)

				.def("GetPosition", &CameraFixed::GetPosition)

				.def("GetAngle", &CameraFixed::GetAngle)
				.def("SetAngle", &CameraFixed::SetAngle)

				.def("SetPosition", &CameraFixed::SetPosition)

				.def("GetUpVector", &CameraFixed::GetUpVector)
				.def("SetUpVector", &CameraFixed::SetUpVector)

				.def("GetType", &CameraFixed::GetType)

				.def("GetEnabled", &CameraFixed::GetEnabled)
				.def("SetEnabled", &CameraFixed::SetEnabled)

				//.def("DestroyFromScene", &CameraFixed::DestroyFromScene)

				// Camera specific methods
				.enum_("CameraType")
				[
					value("CAMERA_INVALID", Camera::CameraType::CAMERA_INVALID),
					value("CAMERA_FREE", Camera::CameraType::CAMERA_FREE),
					value("CAMERA_FPS", Camera::CameraType::CAMERA_FPS),
					value("CAMERA_FIXED", Camera::CameraType::CAMERA_FIXED)
				]
			.def("GetType", &CameraFixed::GetType)
				.def("GetCameraType", &CameraFixed::GetCameraType)

				.def("GetDirection", &CameraFixed::GetDirection)
				.def("SetDirection", &CameraFixed::SetDirection)

				.def("SetPosition", &CameraFixed::SetPosition)
				.def("GetPosition", &CameraFixed::GetPosition)

				.def("GetMaxVelocity", &CameraFixed::GetMaxVelocity)
				.def("SetMaxVelocity", &CameraFixed::SetMaxVelocity)

				/*.def("getMax", &CameraFixed::getMax)
				.def("getMin", &CameraFixed::getMin)
				.def("getCenter", &CameraFixed::getCenter)
				.def("getMax", &CameraFixed::getMax)
				.def("getMin", &CameraFixed::getMin)
				.def("getCenter", &CameraFixed::getCenter)*/

				.def("GetFOV", &CameraFixed::GetFOV)
				.def("SetFOV", &CameraFixed::SetFOV)

				/*.def("getAspect", &CameraFixed::getAspect)
				.def("setAspect", &CameraFixed::setAspect)

				.def("getZNear", &CameraFixed::getZNear)
				.def("setZNear", &CameraFixed::setZNear)

				.def("setZFar", &CameraFixed::setZFar)
				.def("getZFar", &CameraFixed::getZFar)*/

				.def("GetClipDistance", &CameraFixed::GetClipDistance)

				.def("GetBakedData", &CameraFixed::GetBakedData)
				.def("GetTransform", &CameraFixed::GetTransform)
				.def("SetProjection", &CameraFixed::SetProjection)
				.def("GetProjMatrix", &CameraFixed::GetProjMatrix)

				//.def("RecalculateProjection", &CameraFixed::RecalculateProjection)
				//.def("RecalculateProjection", &CameraFixed::RecalculateProjection)

				//.def("RecalculateView", &CameraFixed::RecalculateView)
				//.def("RecalculateFrustum", &CameraFixed::RecalculateFrustum)
				.def("SetView", &CameraFixed::SetView)
				.def("GetViewMatrix", &CameraFixed::GetViewMatrix)

				.def("Update", &CameraFixed::Update)
				.def("UpdateWCD", &CameraFixed::UpdateWCD)

				.def("GetAngle", &CameraFixed::GetAngle)
				.def("SetAngle", &CameraFixed::SetAngle)
				.def("LookAt", &CameraFixed::LookAt)

				// TODO:Engine:Scripting: not implemented frustum currently - depend from frustum
				//.def("GetFrustum", &CameraFixed::GetFrustum)

				.def("MoveToPoint", &CameraFixed::MoveToPoint)
				.def("getPhysicsSize", &CameraFixed::getPhysicsSize)
				//def("SetPhysics", &CameraFixed::SetPhysics).
				.def("GetBufferData", &CameraFixed::GetBufferData)

				// CameraFixed specific methods
				.def(constructor<>())
				.def("GetCameraType", &CameraFixed::GetCameraType)
				.def("Move", &CameraFixed::Move)

				//def("SetPhysics", &CameraFixed::SetPhysics).
				//def("SetPhysics", &CameraFixed::SetPhysics).
		//}

				//////////////////////////////////////////////////////////////////////////
				////////////////////////////////CameraFPS/////////////////////////////////
				//////////////////////////////////////////////////////////////////////////
				//{
				, class_<CameraFPS>("CameraFPS")
				// Object specific methods

				.enum_("ObjectType")
				[
					value("OBJECT", ObjectType::OBJECT),
					value("OBJECT_MESH", ObjectType::OBJECT_MESH),
					value("OBJECT_SKELETEAL_MESH", ObjectType::OBJECT_SKELETEAL_MESH),
					value("OBJECT_PARTICLE_SYSTEM", ObjectType::OBJECT_PARTICLE_SYSTEM),
					value("OBJECT_CAMERA", ObjectType::OBJECT_CAMERA),
					value("OBJECT_USERCUSTOM1", ObjectType::OBJECT_USERCUSTOM1),
					value("OBJECT_USERCUSTOM2", ObjectType::OBJECT_USERCUSTOM2),
					value("OBJECT_USERCUSTOM3", ObjectType::OBJECT_USERCUSTOM3),
					value("OBJECT_USERCUSTOM4", ObjectType::OBJECT_USERCUSTOM4)
				]
			.enum_("EulerAngle")
				[
					value("ANGLE_0", EulerAngle::ANGLE_0),
					value("ANGLE_1", EulerAngle::ANGLE_1),
					value("ANGLE_COUNT", EulerAngle::ANGLE_COUNT)
				]

			//.def("AttachScript", &CameraFPS::AttachScript)
			//.def("AttachScript", &CameraFPS::AttachScript)
			//.def("AttachScript", &CameraFPS::AttachScript)

			.def("SetTransform", &CameraFPS::SetTransform)
				.def("GetTransform", &CameraFPS::GetTransform)

				.def("GetPosition", &CameraFPS::GetPosition)

				.def("GetAngle", &CameraFPS::GetAngle)
				.def("SetAngle", &CameraFPS::SetAngle)

				.def("SetPosition", &CameraFPS::SetPosition)

				.def("GetUpVector", &CameraFPS::GetUpVector)
				.def("SetUpVector", &CameraFPS::SetUpVector)

				.def("GetType", &CameraFPS::GetType)

				.def("GetEnabled", &CameraFPS::GetEnabled)
				.def("SetEnabled", &CameraFPS::SetEnabled)

				//.def("DestroyFromScene", &CameraFPS::DestroyFromScene)

				// Camera specific methods
				.enum_("CameraType")
				[
					value("CAMERA_INVALID", Camera::CameraType::CAMERA_INVALID),
					value("CAMERA_FREE", Camera::CameraType::CAMERA_FREE),
					value("CAMERA_FPS", Camera::CameraType::CAMERA_FPS),
					value("CAMERA_FIXED", Camera::CameraType::CAMERA_FIXED)
				]
			.def("GetType", &CameraFPS::GetType)
				.def("GetCameraType", &CameraFPS::GetCameraType)

				.def("GetDirection", &CameraFPS::GetDirection)
				.def("SetDirection", &CameraFPS::SetDirection)

				.def("SetPosition", &CameraFPS::SetPosition)
				.def("GetPosition", &CameraFPS::GetPosition)

				.def("GetMaxVelocity", &CameraFPS::GetMaxVelocity)
				.def("SetMaxVelocity", &CameraFPS::SetMaxVelocity)

				/*.def("getMax", &CameraFPS::getMax)
				.def("getMin", &CameraFPS::getMin)
				.def("getCenter", &CameraFPS::getCenter)
				.def("getMax", &CameraFPS::getMax)
				.def("getMin", &CameraFPS::getMin)
				.def("getCenter", &CameraFPS::getCenter)*/

				.def("GetFOV", &CameraFPS::GetFOV)
				.def("SetFOV", &CameraFPS::SetFOV)

				/*.def("getAspect", &CameraFPS::getAspect)
				.def("setAspect", &CameraFPS::setAspect)

				.def("getZNear", &CameraFPS::getZNear)
				.def("setZNear", &CameraFPS::setZNear)

				.def("setZFar", &CameraFPS::setZFar)
				.def("getZFar", &CameraFPS::getZFar)*/

				.def("GetClipDistance", &CameraFPS::GetClipDistance)

				.def("GetBakedData", &CameraFPS::GetBakedData)
				.def("GetTransform", &CameraFPS::GetTransform)
				.def("SetProjection", &CameraFPS::SetProjection)
				.def("GetProjMatrix", &CameraFPS::GetProjMatrix)

				//.def("RecalculateProjection", &CameraFPS::RecalculateProjection)
				//.def("RecalculateProjection", &CameraFPS::RecalculateProjection)

				//.def("RecalculateView", &CameraFPS::RecalculateView)
				//.def("RecalculateFrustum", &CameraFPS::RecalculateFrustum)
				.def("SetView", &CameraFPS::SetView)
				.def("GetViewMatrix", &CameraFPS::GetViewMatrix)

				.def("Update", &CameraFPS::Update)
				.def("UpdateWCD", &CameraFPS::UpdateWCD)

				.def("GetAngle", &CameraFPS::GetAngle)
				.def("SetAngle", &CameraFPS::SetAngle)
				.def("LookAt", &CameraFPS::LookAt)

				// TODO:Engine:Scripting: not implemented frustum currently - depend from frustum
				//.def("GetFrustum", &CameraFPS::GetFrustum)

				.def("MoveToPoint", &CameraFPS::MoveToPoint)
				.def("getPhysicsSize", &CameraFPS::getPhysicsSize)
				//.def("SetPhysics", &CameraFPS::SetPhysics)
				.def("GetBufferData", &CameraFPS::GetBufferData)

				// CameraFPS specific methods
				.def(constructor<bool>())
				.def("GetCameraType", &CameraFPS::GetCameraType)
				.def("Move", &CameraFPS::Move)
				//.def("SetPhysics", &CameraFPS::SetPhysics)
				//.def("SetPhysics", &CameraFPS::SetPhysics)
				.def("GetJumpSnd", &CameraFPS::GetJumpSnd)
				//}

				//////////////////////////////////////////////////////////////////////////
				////////////////////////////////CameraFree////////////////////////////////
				//////////////////////////////////////////////////////////////////////////
				//{
				, class_<CameraFree>("CameraFree")
				// Object specific methods

				.enum_("ObjectType")
				[
					value("OBJECT", ObjectType::OBJECT),
					value("OBJECT_MESH", ObjectType::OBJECT_MESH),
					value("OBJECT_SKELETEAL_MESH", ObjectType::OBJECT_SKELETEAL_MESH),
					value("OBJECT_PARTICLE_SYSTEM", ObjectType::OBJECT_PARTICLE_SYSTEM),
					value("OBJECT_CAMERA", ObjectType::OBJECT_CAMERA),
					value("OBJECT_USERCUSTOM1", ObjectType::OBJECT_USERCUSTOM1),
					value("OBJECT_USERCUSTOM2", ObjectType::OBJECT_USERCUSTOM2),
					value("OBJECT_USERCUSTOM3", ObjectType::OBJECT_USERCUSTOM3),
					value("OBJECT_USERCUSTOM4", ObjectType::OBJECT_USERCUSTOM4)
				]
			.enum_("EulerAngle")
				[
					value("ANGLE_0", EulerAngle::ANGLE_0),
					value("ANGLE_1", EulerAngle::ANGLE_1),
					value("ANGLE_COUNT", EulerAngle::ANGLE_COUNT)
				]

			//.def("AttachScript", &CameraFree::AttachScript)
			//.def("AttachScript", &CameraFree::AttachScript)
			//.def("AttachScript", &CameraFree::AttachScript)

			.def("SetTransform", &CameraFree::SetTransform)
				.def("GetTransform", &CameraFree::GetTransform)

				.def("GetPosition", &CameraFree::GetPosition)

				.def("GetAngle", &CameraFree::GetAngle)
				.def("SetAngle", &CameraFree::SetAngle)

				.def("SetPosition", &CameraFree::SetPosition)

				.def("GetUpVector", &CameraFree::GetUpVector)
				.def("SetUpVector", &CameraFree::SetUpVector)

				.def("GetType", &CameraFree::GetType)

				.def("GetEnabled", &CameraFree::GetEnabled)
				.def("SetEnabled", &CameraFree::SetEnabled)

				//.def("DestroyFromScene", &CameraFree::DestroyFromScene)

				// Camera specific methods
				.enum_("CameraType")
				[
					value("CAMERA_INVALID", Camera::CameraType::CAMERA_INVALID),
					value("CAMERA_FREE", Camera::CameraType::CAMERA_FREE),
					value("CAMERA_FPS", Camera::CameraType::CAMERA_FPS),
					value("CAMERA_FIXED", Camera::CameraType::CAMERA_FIXED)
				]
			.def("GetType", &CameraFree::GetType)
				.def("GetCameraType", &CameraFree::GetCameraType)

				.def("GetDirection", &CameraFree::GetDirection)
				.def("SetDirection", &CameraFree::SetDirection)

				.def("SetPosition", &CameraFree::SetPosition)
				.def("GetPosition", &CameraFree::GetPosition)

				.def("GetMaxVelocity", &CameraFree::GetMaxVelocity)
				.def("SetMaxVelocity", &CameraFree::SetMaxVelocity)

				/*.def("getMax", &CameraFree::getMax)
				.def("getMin", &CameraFree::getMin)
				.def("getCenter", &CameraFree::getCenter)
				.def("getMax", &CameraFree::getMax)
				.def("getMin", &CameraFree::getMin)
				.def("getCenter", &CameraFree::getCenter)*/

				.def("GetFOV", &CameraFree::GetFOV)
				.def("SetFOV", &CameraFree::SetFOV)

				/*.def("getAspect", &CameraFree::getAspect)
				.def("setAspect", &CameraFree::setAspect)

				.def("getZNear", &CameraFree::getZNear)
				.def("setZNear", &CameraFree::setZNear)

				.def("setZFar", &CameraFree::setZFar)
				.def("getZFar", &CameraFree::getZFar)*/

				.def("GetClipDistance", &CameraFree::GetClipDistance)

				.def("GetBakedData", &CameraFree::GetBakedData)
				.def("GetTransform", &CameraFree::GetTransform)
				.def("SetProjection", &CameraFree::SetProjection)
				.def("GetProjMatrix", &CameraFree::GetProjMatrix)

				//.def("RecalculateProjection", &CameraFree::RecalculateProjection)
				//.def("RecalculateProjection", &CameraFree::RecalculateProjection)

				//.def("RecalculateView", &CameraFree::RecalculateView)
				//.def("RecalculateFrustum", &CameraFree::RecalculateFrustum)
				.def("SetView", &CameraFree::SetView)
				.def("GetViewMatrix", &CameraFree::GetViewMatrix)

				.def("Update", &CameraFree::Update)
				.def("UpdateWCD", &CameraFree::UpdateWCD)

				.def("GetAngle", &CameraFree::GetAngle)
				.def("SetAngle", &CameraFree::SetAngle)
				.def("LookAt", &CameraFree::LookAt)

				// TODO:Engine:Scripting: not implemented frustum currently - depend from frustum
				//.def("GetFrustum", &CameraFree::GetFrustum)

				.def("MoveToPoint", &CameraFree::MoveToPoint)
				.def("getPhysicsSize", &CameraFree::getPhysicsSize)
				//.def("SetPhysics", &CameraFree::SetPhysics)
				.def("GetBufferData", &CameraFree::GetBufferData)

				// CameraFree specific methods
				.def(constructor<bool>())
				.def("GetCameraType", &CameraFree::GetCameraType)
				.def("Move", &CameraFree::Move)
				//.def("SetPhysics", &CameraFree::SetPhysics)
				//.def("SetPhysics", &CameraFree::SetPhysics)

				// this not implemented for CameraFree
				//.def("GetJumpSnd", &CameraFree::GetJumpSnd)
		//}

					//////////////////////////////////////////////////////////////////////////
					///////////////////////////////ObjectMesh/////////////////////////////////
					//////////////////////////////////////////////////////////////////////////

					//{
				, class_<ObjectMesh>("ObjectMesh")
				// Object specific methods

				.enum_("ObjectType")
				[
					value("OBJECT", ObjectType::OBJECT),
					value("OBJECT_MESH", ObjectType::OBJECT_MESH),
					value("OBJECT_SKELETEAL_MESH", ObjectType::OBJECT_SKELETEAL_MESH),
					value("OBJECT_PARTICLE_SYSTEM", ObjectType::OBJECT_PARTICLE_SYSTEM),
					value("OBJECT_CAMERA", ObjectType::OBJECT_CAMERA),
					value("OBJECT_USERCUSTOM1", ObjectType::OBJECT_USERCUSTOM1),
					value("OBJECT_USERCUSTOM2", ObjectType::OBJECT_USERCUSTOM2),
					value("OBJECT_USERCUSTOM3", ObjectType::OBJECT_USERCUSTOM3),
					value("OBJECT_USERCUSTOM4", ObjectType::OBJECT_USERCUSTOM4)
				]
			.enum_("EulerAngle")
				[
					value("ANGLE_0", EulerAngle::ANGLE_0),
					value("ANGLE_1", EulerAngle::ANGLE_1),
					value("ANGLE_COUNT", EulerAngle::ANGLE_COUNT)
				]

			//.def("AttachScript", &ObjectMesh::AttachScript)
			//.def("AttachScript", &ObjectMesh::AttachScript)
			//.def("AttachScript", &ObjectMesh::AttachScript)

			.def("SetTransform", &ObjectMesh::SetTransform)
				.def("GetTransform", &ObjectMesh::GetTransform)

				.def("GetPosition", &ObjectMesh::GetPosition)

				.def("GetAngle", &ObjectMesh::GetAngle)
				.def("SetAngle", &ObjectMesh::SetAngle)

				.def("SetPosition", &ObjectMesh::SetPosition)

				.def("GetUpVector", &ObjectMesh::GetUpVector)
				.def("SetUpVector", &ObjectMesh::SetUpVector)

				.def("GetType", &ObjectMesh::GetType)

				.def("GetEnabled", &ObjectMesh::GetEnabled)
				.def("SetEnabled", &ObjectMesh::SetEnabled)

				//.def("DestroyFromScene", &ObjectMesh::DestroyFromScene)

				// BasePhysicsObjectEntity specific methods
				/*.def("GetMultipart", &ObjectMesh::GetMultipart)
				.def("SetMultipart", &ObjectMesh::SetMultipart)

				.def("GetCollisionSize", &ObjectMesh::GetCollisionSize)
				.def("GetCollisionRad", &ObjectMesh::GetCollisionRad)
				.def("GetCollisionWidth", &ObjectMesh::GetCollisionWidth)
				.def("GetCollisionHeight", &ObjectMesh::GetCollisionHeight)
				.def("GetCollisionMass", &ObjectMesh::GetCollisionMass)

				.def("SetPhysicsByDesc", &ObjectMesh::SetPhysicsByDesc)

				.def("GetPhysBody", &ObjectMesh::GetPhysBody)
				.def("SetPhysBody", &ObjectMesh::SetPhysBody)*/

				//.def("SetImpactSound", &ObjectMesh::SetImpactSound)

				/*.def("SetPhysicsConvexHull", &ObjectMesh::SetPhysicsConvexHull)
				.def("SetPhysicsStaticMesh", &ObjectMesh::SetPhysicsStaticMesh)
				.def("SetPhysicsNone", &ObjectMesh::SetPhysicsNone)*/

				//.def("GetCollisionType", &ObjectMesh::GetCollisionType)

				//.def("DeleteImpactSound", &ObjectMesh::DeleteImpactSound)

				// BaseVisualObjectEntity specific methods
				.enum_("RenderLayer")
				[
					value("LAYER_0", RenderLayer::LAYER_0),
					value("LAYER_1", RenderLayer::LAYER_1),
					value("LAYER_2", RenderLayer::LAYER_2),
					value("LAYER_3", RenderLayer::LAYER_3),
					value("LAYER_4", RenderLayer::LAYER_4),
					value("LAYER_5", RenderLayer::LAYER_5),
					value("LAYER_6", RenderLayer::LAYER_6),
					value("LAYER_7", RenderLayer::LAYER_7),
					value("USER_LAYER_8", RenderLayer::USER_LAYER_8),
					value("USER_LAYER_9", RenderLayer::USER_LAYER_9),
					value("USER_LAYER_10", RenderLayer::USER_LAYER_10),
					value("USER_LAYER_11", RenderLayer::USER_LAYER_11),
					value("USER_LAYER_12", RenderLayer::USER_LAYER_12),
					value("USER_LAYER_13", RenderLayer::USER_LAYER_13),
					value("USER_LAYER_14", RenderLayer::USER_LAYER_14)
				]

			//
			// ObjectMesh/BaseVisualObjectEntity specific methods
			//
			.def("GetNumSubsets", &ObjectMesh::GetNumSubsets)
				.def("GetMaterials", &ObjectMesh::GetMaterials)
				.def("GetMaterialsAsString", &ObjectMesh::GetMaterialsAsString)

				.def("GetLoadingPath", &ObjectMesh::GetLoadingPath)
				.def("SetLoadingPath", &ObjectMesh::SetLoadingPath)

				.def("GetRenderLayer", &ObjectMesh::GetRenderLayer)
				.def("SetRenderLayer", &ObjectMesh::SetRenderLayer)

				// override LoadFromPath from 	BaseVisualObjectEntity
				.def("LoadFromPath", &ObjectMesh::LoadFromPath)

				//.def("GetMax", &ObjectMesh::GetMax)
				//.def("GetMin", &ObjectMesh::GetMin)

				//.def("GetBBox", &ObjectMesh::GetBBox)
				//.def("GetBBox", &ObjectMesh::GetBBox)

				//.def("GetCenter", &ObjectMesh::GetCenter)
				//.def("GetRadius", &ObjectMesh::GetRadius)

				//.def("GetMax", &ObjectMesh::GetMax)
				//.def("GetMin", &ObjectMesh::GetMin)

				//.def("GetBBox", &ObjectMesh::GetBBox)
				//.def("GetBBox", &ObjectMesh::GetBBox)

				//.def("GetCenter", &ObjectMesh::GetCenter)
				//.def("GetRadius", &ObjectMesh::GetRadius)

				.def("GetSubsetName", &ObjectMesh::GetSubsetName)

				//.def("GetBSphere", &ObjectMesh::GetBSphere)
				//.def("GetBSphere", &ObjectMesh::GetBSphere)

				//!@todo Engine:scripting: Material dependency
				.def("GetMaterial", &ObjectMesh::GetMaterial)

				.def("GetTransformedBBox", &ObjectMesh::GetTransformedBBox)
				.def("GetTransformedBSphere", &ObjectMesh::GetTransformedBSphere)

				//.def("SetMaterial", &ObjectMesh::SetMaterial)
				//.def("SetMaterial", &ObjectMesh::SetMaterial)

				.def("GetTransform", &ObjectMesh::GetTransform)

				.def("HasQuery", &ObjectMesh::HasQuery)

				.def("SetPhysicsBox", &ObjectMesh::SetPhysicsBox)
				.def("SetPhysicsSphere", &ObjectMesh::SetPhysicsSphere)

				.def("SetPhysicsCylinder", &ObjectMesh::SetPhysicsCylinder)
				.def("SetPhysicsCapsule", &ObjectMesh::SetPhysicsCapsule)

			/*	.def("SetPhysicsConvexHull", &ObjectMesh::SetPhysicsConvexHull)
				.def("SetPhysicsStaticMesh", &ObjectMesh::SetPhysicsStaticMesh)*/
				//			}

				//////////////////////////////////////////////////////////////////////////
				///////////////////////////////AnimatedMesh///////////////////////////////
				//////////////////////////////////////////////////////////////////////////
			//{
				, class_<AnimatedMesh>("AnimatedMesh")
				// Object specific methods

				.enum_("ObjectType")
				[
					value("OBJECT", ObjectType::OBJECT),
					value("OBJECT_MESH", ObjectType::OBJECT_MESH),
					value("OBJECT_SKELETEAL_MESH", ObjectType::OBJECT_SKELETEAL_MESH),
					value("OBJECT_PARTICLE_SYSTEM", ObjectType::OBJECT_PARTICLE_SYSTEM),
					value("OBJECT_CAMERA", ObjectType::OBJECT_CAMERA),
					value("OBJECT_USERCUSTOM1", ObjectType::OBJECT_USERCUSTOM1),
					value("OBJECT_USERCUSTOM2", ObjectType::OBJECT_USERCUSTOM2),
					value("OBJECT_USERCUSTOM3", ObjectType::OBJECT_USERCUSTOM3),
					value("OBJECT_USERCUSTOM4", ObjectType::OBJECT_USERCUSTOM4)
				]
			.enum_("EulerAngle")
				[
					value("ANGLE_0", EulerAngle::ANGLE_0),
					value("ANGLE_1", EulerAngle::ANGLE_1),
					value("ANGLE_COUNT", EulerAngle::ANGLE_COUNT)
				]

			//.def("AttachScript", &AnimatedMesh::AttachScript)
			//.def("AttachScript", &ObAnimatedMeshjectMesh::AttachScript)
			//.def("AttachScript", &AnimatedMesh::AttachScript)

			.def("SetTransform", &AnimatedMesh::SetTransform)
				.def("GetTransform", &AnimatedMesh::GetTransform)

				.def("GetPosition", &AnimatedMesh::GetPosition)

				.def("GetAngle", &AnimatedMesh::GetAngle)
				.def("SetAngle", &AnimatedMesh::SetAngle)

				.def("SetPosition", &AnimatedMesh::SetPosition)

				.def("GetUpVector", &AnimatedMesh::GetUpVector)
				.def("SetUpVector", &AnimatedMesh::SetUpVector)

				.def("GetType", &AnimatedMesh::GetType)

				.def("GetEnabled", &AnimatedMesh::GetEnabled)
				.def("SetEnabled", &AnimatedMesh::SetEnabled)

				//.def("DestroyFromScene", &AnimatedMesh::DestroyFromScene)

				// BasePhysicsObjectEntity specific methods
				//.def("GetImpactSound", &AnimatedMesh::GetImpactSound)
				/*.def("GetMultipart", &AnimatedMesh::GetMultipart)
				.def("SetMultipart", &AnimatedMesh::SetMultipart)

				.def("GetCollisionSize", &AnimatedMesh::GetCollisionSize)
				.def("GetCollisionRad", &AnimatedMesh::GetCollisionRad)
				.def("GetCollisionWidth", &AnimatedMesh::GetCollisionWidth)
				.def("GetCollisionHeight", &AnimatedMesh::GetCollisionHeight)
				.def("GetCollisionMass", &AnimatedMesh::GetCollisionMass)

				.def("SetPhysicsByDesc", &AnimatedMesh::SetPhysicsByDesc)

				.def("GetPhysBody", &AnimatedMesh::GetPhysBody)
				.def("SetPhysBody", &AnimatedMesh::SetPhysBody)*/

				//.def("SetImpactSound", &AnimatedMesh::SetImpactSound)

				/*.def("SetPhysicsConvexHull", &AnimatedMesh::SetPhysicsConvexHull)
				.def("SetPhysicsStaticMesh", &AnimatedMesh::SetPhysicsStaticMesh)
				.def("SetPhysicsNone", &AnimatedMesh::SetPhysicsNone)*/

				//.def("GetCollisionType", &AnimatedMesh::GetCollisionType)

				//.def("DeleteImpactSound", &AnimatedMesh::DeleteImpactSound)

				// BaseVisualObjectEntity specific methods
				.enum_("RenderLayer")
				[
					value("LAYER_0", RenderLayer::LAYER_0),
					value("LAYER_1", RenderLayer::LAYER_1),
					value("LAYER_2", RenderLayer::LAYER_2),
					value("LAYER_3", RenderLayer::LAYER_3),
					value("LAYER_4", RenderLayer::LAYER_4),
					value("LAYER_5", RenderLayer::LAYER_5),
					value("LAYER_6", RenderLayer::LAYER_6),
					value("LAYER_7", RenderLayer::LAYER_7),
					value("USER_LAYER_8", RenderLayer::USER_LAYER_8),
					value("USER_LAYER_9", RenderLayer::USER_LAYER_9),
					value("USER_LAYER_10", RenderLayer::USER_LAYER_10),
					value("USER_LAYER_11", RenderLayer::USER_LAYER_11),
					value("USER_LAYER_12", RenderLayer::USER_LAYER_12),
					value("USER_LAYER_13", RenderLayer::USER_LAYER_13),
					value("USER_LAYER_14", RenderLayer::USER_LAYER_14)
				]

			//
			// ObjectMesh/BaseVisualObjectEntity specific methods
			//
			.def("GetNumSubsets", &AnimatedMesh::GetNumSubsets)
				.def("GetMaterials", &AnimatedMesh::GetMaterials)
				.def("GetMaterialsAsString", &AnimatedMesh::GetMaterialsAsString)

				.def("GetLoadingPath", &AnimatedMesh::GetLoadingPath)
				.def("SetLoadingPath", &AnimatedMesh::SetLoadingPath)

				.def("GetRenderLayer", &AnimatedMesh::GetRenderLayer)
				.def("SetRenderLayer", &AnimatedMesh::SetRenderLayer)

				// override LoadFromPath from 	BaseVisualObjectEntity
				.def("LoadFromPath", &AnimatedMesh::LoadFromPath)

				//.def("GetMax", &AnimatedMesh::GetMax)
				//.def("GetMin", &AnimatedMesh::GetMin)

				//.def("GetBBox", &AnimatedMesh::GetBBox)
				//.def("GetBBox", &AnimatedMesh::GetBBox)

				//.def("GetCenter", &AnimatedMesh::GetCenter)
				//.def("GetRadius", &AnimatedMesh::GetRadius)

				//.def("GetMax", &AnimatedMesh::GetMax)
				//.def("GetMin", &AnimatedMesh::GetMin)

				//.def("GetBBox", &AnimatedMesh::GetBBox)
				//.def("GetBBox", &AnimatedMesh::GetBBox)

				//.def("GetCenter", &AnimatedMesh::GetCenter)
				//.def("GetRadius", &AnimatedMesh::GetRadius)

				.def("GetSubsetName", &AnimatedMesh::GetSubsetName)

				//.def("GetBSphere", &AnimatedMesh::GetBSphere)
				//.def("GetBSphere", &AnimatedMesh::GetBSphere)

				//!@todo Engine:scripting: Material dependency
				.def("GetMaterial", &AnimatedMesh::GetMaterial)

				.def("GetTransformedBBox", &AnimatedMesh::GetTransformedBBox)
				.def("GetTransformedBSphere", &AnimatedMesh::GetTransformedBSphere)

				//.def("SetMaterial", &AnimatedMesh::SetMaterial)
				//.def("SetMaterial", &AnimatedMesh::SetMaterial)

				.def("GetTransform", &AnimatedMesh::GetTransform)

				.def("HasQuery", &AnimatedMesh::HasQuery)

				.def("SetPhysicsBox", &AnimatedMesh::SetPhysicsBox)
				.def("SetPhysicsSphere", &AnimatedMesh::SetPhysicsSphere)

				.def("SetPhysicsCylinder", &AnimatedMesh::SetPhysicsCylinder)
				.def("SetPhysicsCapsule", &AnimatedMesh::SetPhysicsCapsule)

			/*	.def("SetPhysicsConvexHull", &AnimatedMesh::SetPhysicsConvexHull)
				.def("SetPhysicsStaticMesh", &AnimatedMesh::SetPhysicsStaticMesh)*/
				//			}

				// Object specific methods

				.enum_("ObjectType")
				[
					value("OBJECT", ObjectType::OBJECT),
					value("OBJECT_MESH", ObjectType::OBJECT_MESH),
					value("OBJECT_SKELETEAL_MESH", ObjectType::OBJECT_SKELETEAL_MESH),
					value("OBJECT_PARTICLE_SYSTEM", ObjectType::OBJECT_PARTICLE_SYSTEM),
					value("OBJECT_CAMERA", ObjectType::OBJECT_CAMERA),
					value("OBJECT_USERCUSTOM1", ObjectType::OBJECT_USERCUSTOM1),
					value("OBJECT_USERCUSTOM2", ObjectType::OBJECT_USERCUSTOM2),
					value("OBJECT_USERCUSTOM3", ObjectType::OBJECT_USERCUSTOM3),
					value("OBJECT_USERCUSTOM4", ObjectType::OBJECT_USERCUSTOM4)
				]
			.enum_("EulerAngle")
				[
					value("ANGLE_0", EulerAngle::ANGLE_0),
					value("ANGLE_1", EulerAngle::ANGLE_1),
					value("ANGLE_COUNT", EulerAngle::ANGLE_COUNT)
				]

			//.def("AttachScript", &AnimatedMesh::AttachScript)
			//.def("AttachScript", &AnimatedMesh::AttachScript)
			//.def("AttachScript", &AnimatedMesh::AttachScript)

			.def("SetTransform", &AnimatedMesh::SetTransform)
				.def("GetTransform", &AnimatedMesh::GetTransform)

				.def("GetPosition", &AnimatedMesh::GetPosition)

				.def("GetAngle", &AnimatedMesh::GetAngle)
				.def("SetAngle", &AnimatedMesh::SetAngle)

				.def("SetPosition", &AnimatedMesh::SetPosition)

				.def("GetUpVector", &AnimatedMesh::GetUpVector)
				.def("SetUpVector", &AnimatedMesh::SetUpVector)

				.def("GetType", &AnimatedMesh::GetType)

				.def("GetEnabled", &AnimatedMesh::GetEnabled)
				.def("SetEnabled", &AnimatedMesh::SetEnabled)

				//.def("DestroyFromScene", &AnimatedMesh::DestroyFromScene)

				// BasePhysicsObjectEntity specific methods
				//.def("GetImpactSound", &AnimatedMesh::GetImpactSound)
				//.def("GetMultipart", &AnimatedMesh::GetMultipart)
				//.def("SetMultipart", &AnimatedMesh::SetMultipart)

				//.def("GetCollisionSize", &AnimatedMesh::GetCollisionSize)
				//.def("GetCollisionRad", &AnimatedMesh::GetCollisionRad)
				//.def("GetCollisionWidth", &AnimatedMesh::GetCollisionWidth)
				//.def("GetCollisionHeight", &AnimatedMesh::GetCollisionHeight)
				//.def("GetCollisionMass", &AnimatedMesh::GetCollisionMass)

				//.def("SetPhysicsByDesc", &AnimatedMesh::SetPhysicsByDesc)

				//.def("GetPhysBody", &AnimatedMesh::GetPhysBody)
				//.def("SetPhysBody", &AnimatedMesh::SetPhysBody)

				////.def("SetImpactSound", &AnimatedMesh::SetImpactSound)

				//.def("SetPhysicsConvexHull", &AnimatedMesh::SetPhysicsConvexHull)
				//.def("SetPhysicsStaticMesh", &AnimatedMesh::SetPhysicsStaticMesh)
				//.def("SetPhysicsNone", &AnimatedMesh::SetPhysicsNone)

				//.def("GetCollisionType", &AnimatedMesh::GetCollisionType)

				//.def("DeleteImpactSound", &AnimatedMesh::DeleteImpactSound)

				// BaseVisualObjectEntity specific methods
				.enum_("RenderLayer")
				[
					value("LAYER_0", RenderLayer::LAYER_0),
					value("LAYER_1", RenderLayer::LAYER_1),
					value("LAYER_2", RenderLayer::LAYER_2),
					value("LAYER_3", RenderLayer::LAYER_3),
					value("LAYER_4", RenderLayer::LAYER_4),
					value("LAYER_5", RenderLayer::LAYER_5),
					value("LAYER_6", RenderLayer::LAYER_6),
					value("LAYER_7", RenderLayer::LAYER_7),
					value("USER_LAYER_8", RenderLayer::USER_LAYER_8),
					value("USER_LAYER_9", RenderLayer::USER_LAYER_9),
					value("USER_LAYER_10", RenderLayer::USER_LAYER_10),
					value("USER_LAYER_11", RenderLayer::USER_LAYER_11),
					value("USER_LAYER_12", RenderLayer::USER_LAYER_12),
					value("USER_LAYER_13", RenderLayer::USER_LAYER_13),
					value("USER_LAYER_14", RenderLayer::USER_LAYER_14)
				]

			//
			// AnimatedMesh/BaseVisualObjectEntity specific methods
			//
			.def("GetNumSubsets", &AnimatedMesh::GetNumSubsets)
				.def("GetMaterials", &AnimatedMesh::GetMaterials)
				.def("GetMaterialsAsString", &AnimatedMesh::GetMaterialsAsString)

				.def("GetLoadingPath", &AnimatedMesh::GetLoadingPath)
				.def("SetLoadingPath", &AnimatedMesh::SetLoadingPath)

				.def("GetRenderLayer", &AnimatedMesh::GetRenderLayer)
				.def("SetRenderLayer", &AnimatedMesh::SetRenderLayer)

				// override LoadFromPath from 	BaseVisualObjectEntity
				.def("LoadFromPath", &AnimatedMesh::LoadFromPath)

				//.def("GetMax", &AnimatedMesh::GetMax)
				//.def("GetMin", &AnimatedMesh::GetMin)

				//.def("GetBBox", &AnimatedMesh::GetBBox)
				//.def("GetBBox", &AnimatedMesh::GetBBox)

				//.def("GetCenter", &AnimatedMesh::GetCenter)
				//.def("GetRadius", &AnimatedMesh::GetRadius)

				//.def("GetMax", &AnimatedMesh::GetMax)
				//.def("GetMin", &AnimatedMesh::GetMin)

				//.def("GetBBox", &AnimatedMesh::GetBBox)
				//.def("GetBBox", &AnimatedMesh::GetBBox)

				//.def("GetCenter", &AnimatedMesh::GetCenter)
				//.def("GetRadius", &AnimatedMesh::GetRadius)

				.def("GetSubsetName", &AnimatedMesh::GetSubsetName)

				//.def("GetBSphere", &AnimatedMesh::GetBSphere)
				//.def("GetBSphere", &AnimatedMesh::GetBSphere)

				//!@todo Engine:scripting: Material dependency
				.def("GetMaterial", &AnimatedMesh::GetMaterial)

				.def("GetTransformedBBox", &AnimatedMesh::GetTransformedBBox)
				.def("GetTransformedBSphere", &AnimatedMesh::GetTransformedBSphere)

				//.def("SetMaterial", &AnimatedMesh::SetMaterial)
				//.def("SetMaterial", &AnimatedMesh::SetMaterial)

				.def("GetTransform", &AnimatedMesh::GetTransform)

				.def("HasQuery", &AnimatedMesh::HasQuery)

				.def("SetPhysicsBox", &AnimatedMesh::SetPhysicsBox)
				.def("SetPhysicsSphere", &AnimatedMesh::SetPhysicsSphere)

				.def("SetPhysicsCylinder", &AnimatedMesh::SetPhysicsCylinder)
				.def("SetPhysicsCapsule", &AnimatedMesh::SetPhysicsCapsule)

				//.def("SetPhysicsConvexHull", &AnimatedMesh::SetPhysicsConvexHull)
				//.def("SetPhysicsStaticMesh", &AnimatedMesh::SetPhysicsStaticMesh)
				//			}
				//////////////////////////////////////////////////////////////////////////
				///////////////////////////////ObjectSky///////////////////////////////
				//////////////////////////////////////////////////////////////////////////
				//{
				, class_<ObjectSky>("ObjectSky")
				// Object specific methods

				.enum_("ObjectType")
				[
					value("OBJECT", ObjectType::OBJECT),
					value("OBJECT_MESH", ObjectType::OBJECT_MESH),
					value("OBJECT_SKELETEAL_MESH", ObjectType::OBJECT_SKELETEAL_MESH),
					value("OBJECT_PARTICLE_SYSTEM", ObjectType::OBJECT_PARTICLE_SYSTEM),
					value("OBJECT_CAMERA", ObjectType::OBJECT_CAMERA),
					value("OBJECT_USERCUSTOM1", ObjectType::OBJECT_USERCUSTOM1),
					value("OBJECT_USERCUSTOM2", ObjectType::OBJECT_USERCUSTOM2),
					value("OBJECT_USERCUSTOM3", ObjectType::OBJECT_USERCUSTOM3),
					value("OBJECT_USERCUSTOM4", ObjectType::OBJECT_USERCUSTOM4)
				]
			.enum_("EulerAngle")
				[
					value("ANGLE_0", EulerAngle::ANGLE_0),
					value("ANGLE_1", EulerAngle::ANGLE_1),
					value("ANGLE_COUNT", EulerAngle::ANGLE_COUNT)
				]

			//.def("AttachScript", &ObjectSky::AttachScript)
			//.def("AttachScript", &ObObjectSkyjectMesh::AttachScript)
			//.def("AttachScript", &ObjectSky::AttachScript)

			.def("SetTransform", &ObjectSky::SetTransform)
				.def("GetTransform", &ObjectSky::GetTransform)

				.def("GetPosition", &ObjectSky::GetPosition)

				.def("GetAngle", &ObjectSky::GetAngle)
				.def("SetAngle", &ObjectSky::SetAngle)

				.def("SetPosition", &ObjectSky::SetPosition)

				.def("GetUpVector", &ObjectSky::GetUpVector)
				.def("SetUpVector", &ObjectSky::SetUpVector)

				.def("GetType", &ObjectSky::GetType)

				.def("GetEnabled", &ObjectSky::GetEnabled)
				.def("SetEnabled", &ObjectSky::SetEnabled)

				//.def("DestroyFromScene", &ObjectSky::DestroyFromScene)

				// BasePhysicsObjectEntity specific methods
				//.def("GetImpactSound", &ObjectSky::GetImpactSound)
				//.def("GetMultipart", &ObjectSky::GetMultipart)
				//.def("SetMultipart", &ObjectSky::SetMultipart)

				//.def("GetCollisionSize", &ObjectSky::GetCollisionSize)
				//.def("GetCollisionRad", &ObjectSky::GetCollisionRad)
				//.def("GetCollisionWidth", &ObjectSky::GetCollisionWidth)
				//.def("GetCollisionHeight", &ObjectSky::GetCollisionHeight)
				//.def("GetCollisionMass", &ObjectSky::GetCollisionMass)

				//.def("SetPhysicsByDesc", &ObjectSky::SetPhysicsByDesc)

				//.def("GetPhysBody", &ObjectSky::GetPhysBody)
				//.def("SetPhysBody", &ObjectSky::SetPhysBody)

				////.def("SetImpactSound", &ObjectSky::SetImpactSound)

				//.def("SetPhysicsConvexHull", &ObjectSky::SetPhysicsConvexHull)
				//.def("SetPhysicsStaticMesh", &ObjectSky::SetPhysicsStaticMesh)
				//.def("SetPhysicsNone", &ObjectSky::SetPhysicsNone)

				//.def("GetCollisionType", &ObjectSky::GetCollisionType)

				//.def("DeleteImpactSound", &ObjectSky::DeleteImpactSound)

				// BaseVisualObjectEntity specific methods
				.enum_("RenderLayer")
				[
					value("LAYER_0", RenderLayer::LAYER_0),
					value("LAYER_1", RenderLayer::LAYER_1),
					value("LAYER_2", RenderLayer::LAYER_2),
					value("LAYER_3", RenderLayer::LAYER_3),
					value("LAYER_4", RenderLayer::LAYER_4),
					value("LAYER_5", RenderLayer::LAYER_5),
					value("LAYER_6", RenderLayer::LAYER_6),
					value("LAYER_7", RenderLayer::LAYER_7),
					value("USER_LAYER_8", RenderLayer::USER_LAYER_8),
					value("USER_LAYER_9", RenderLayer::USER_LAYER_9),
					value("USER_LAYER_10", RenderLayer::USER_LAYER_10),
					value("USER_LAYER_11", RenderLayer::USER_LAYER_11),
					value("USER_LAYER_12", RenderLayer::USER_LAYER_12),
					value("USER_LAYER_13", RenderLayer::USER_LAYER_13),
					value("USER_LAYER_14", RenderLayer::USER_LAYER_14)
				]

			//
			// ObjectMesh/BaseVisualObjectEntity specific methods
			//
			.def("GetNumSubsets", &ObjectSky::GetNumSubsets)
				.def("GetMaterials", &ObjectSky::GetMaterials)
				.def("GetMaterialsAsString", &ObjectSky::GetMaterialsAsString)

				.def("GetLoadingPath", &ObjectSky::GetLoadingPath)
				.def("SetLoadingPath", &ObjectSky::SetLoadingPath)

				.def("GetRenderLayer", &ObjectSky::GetRenderLayer)
				.def("SetRenderLayer", &ObjectSky::SetRenderLayer)

				// override LoadFromPath from 	BaseVisualObjectEntity
				.def("LoadFromPath", &ObjectSky::LoadFromPath)

				//.def("GetMax", &ObjectSky::GetMax)
				//.def("GetMin", &ObjectSky::GetMin)

				//.def("GetBBox", &ObjectSky::GetBBox)
				//.def("GetBBox", &ObjectSky::GetBBox)

				//.def("GetCenter", &ObjectSky::GetCenter)
				//.def("GetRadius", &ObjectSky::GetRadius)

				//.def("GetMax", &ObjectSky::GetMax)
				//.def("GetMin", &ObjectSky::GetMin)

				//.def("GetBBox", &ObjectSky::GetBBox)
				//.def("GetBBox", &ObjectSky::GetBBox)

				//.def("GetCenter", &ObjectSky::GetCenter)
				//.def("GetRadius", &ObjectSky::GetRadius)

				.def("GetSubsetName", &ObjectSky::GetSubsetName)

				//.def("GetBSphere", &ObjectSky::GetBSphere)
				//.def("GetBSphere", &ObjectSky::GetBSphere)

				//!@todo Engine:scripting: Material dependency
				.def("GetMaterial", &ObjectSky::GetMaterial)

				.def("GetTransformedBBox", &ObjectSky::GetTransformedBBox)
				.def("GetTransformedBSphere", &ObjectSky::GetTransformedBSphere)

				//.def("SetMaterial", &ObjectSky::SetMaterial)
				//.def("SetMaterial", &ObjectSky::SetMaterial)

				.def("GetTransform", &ObjectSky::GetTransform)

				.def("HasQuery", &ObjectSky::HasQuery)

				.def("SetPhysicsBox", &ObjectSky::SetPhysicsBox)
				.def("SetPhysicsSphere", &ObjectSky::SetPhysicsSphere)

				.def("SetPhysicsCylinder", &ObjectSky::SetPhysicsCylinder)
				.def("SetPhysicsCapsule", &ObjectSky::SetPhysicsCapsule)

				//.def("SetPhysicsConvexHull", &ObjectSky::SetPhysicsConvexHull)
				//.def("SetPhysicsStaticMesh", &ObjectSky::SetPhysicsStaticMesh)
				//			}

				// Object specific methods

				.enum_("ObjectType")
				[
					value("OBJECT", ObjectType::OBJECT),
					value("OBJECT_MESH", ObjectType::OBJECT_MESH),
					value("OBJECT_SKELETEAL_MESH", ObjectType::OBJECT_SKELETEAL_MESH),
					value("OBJECT_PARTICLE_SYSTEM", ObjectType::OBJECT_PARTICLE_SYSTEM),
					value("OBJECT_CAMERA", ObjectType::OBJECT_CAMERA),
					value("OBJECT_USERCUSTOM1", ObjectType::OBJECT_USERCUSTOM1),
					value("OBJECT_USERCUSTOM2", ObjectType::OBJECT_USERCUSTOM2),
					value("OBJECT_USERCUSTOM3", ObjectType::OBJECT_USERCUSTOM3),
					value("OBJECT_USERCUSTOM4", ObjectType::OBJECT_USERCUSTOM4)
				]
			.enum_("EulerAngle")
				[
					value("ANGLE_0", EulerAngle::ANGLE_0),
					value("ANGLE_1", EulerAngle::ANGLE_1),
					value("ANGLE_COUNT", EulerAngle::ANGLE_COUNT)
				]

			//.def("AttachScript", &ObjectSky::AttachScript)
			//.def("AttachScript", &ObjectSky::AttachScript)
			//.def("AttachScript", &ObjectSky::AttachScript)

			.def("SetTransform", &ObjectSky::SetTransform)
				.def("GetTransform", &ObjectSky::GetTransform)

				.def("GetPosition", &ObjectSky::GetPosition)

				.def("GetAngle", &ObjectSky::GetAngle)
				.def("SetAngle", &ObjectSky::SetAngle)

				.def("SetPosition", &ObjectSky::SetPosition)

				.def("GetUpVector", &ObjectSky::GetUpVector)
				.def("SetUpVector", &ObjectSky::SetUpVector)

				.def("GetType", &ObjectSky::GetType)

				.def("GetEnabled", &ObjectSky::GetEnabled)
				.def("SetEnabled", &ObjectSky::SetEnabled)

				//.def("DestroyFromScene", &ObjectSky::DestroyFromScene)

				// BasePhysicsObjectEntity specific methods
				//.def("GetImpactSound", &ObjectSky::GetImpactSound)
				//.def("GetMultipart", &ObjectSky::GetMultipart)
				//.def("SetMultipart", &ObjectSky::SetMultipart)

				//.def("GetCollisionSize", &ObjectSky::GetCollisionSize)
				//.def("GetCollisionRad", &ObjectSky::GetCollisionRad)
				//.def("GetCollisionWidth", &ObjectSky::GetCollisionWidth)
				//.def("GetCollisionHeight", &ObjectSky::GetCollisionHeight)
				//.def("GetCollisionMass", &ObjectSky::GetCollisionMass)

				//.def("SetPhysicsByDesc", &ObjectSky::SetPhysicsByDesc)

				//.def("GetPhysBody", &ObjectSky::GetPhysBody)
				//.def("SetPhysBody", &ObjectSky::SetPhysBody)

				////.def("SetImpactSound", &ObjectSky::SetImpactSound)

				//.def("SetPhysicsConvexHull", &ObjectSky::SetPhysicsConvexHull)
				//.def("SetPhysicsStaticMesh", &ObjectSky::SetPhysicsStaticMesh)
				//.def("SetPhysicsNone", &ObjectSky::SetPhysicsNone)

				//.def("GetCollisionType", &ObjectSky::GetCollisionType)

				//.def("DeleteImpactSound", &ObjectSky::DeleteImpactSound)

				// BaseVisualObjectEntity specific methods
				.enum_("RenderLayer")
				[
					value("LAYER_0", RenderLayer::LAYER_0),
					value("LAYER_1", RenderLayer::LAYER_1),
					value("LAYER_2", RenderLayer::LAYER_2),
					value("LAYER_3", RenderLayer::LAYER_3),
					value("LAYER_4", RenderLayer::LAYER_4),
					value("LAYER_5", RenderLayer::LAYER_5),
					value("LAYER_6", RenderLayer::LAYER_6),
					value("LAYER_7", RenderLayer::LAYER_7),
					value("USER_LAYER_8", RenderLayer::USER_LAYER_8),
					value("USER_LAYER_9", RenderLayer::USER_LAYER_9),
					value("USER_LAYER_10", RenderLayer::USER_LAYER_10),
					value("USER_LAYER_11", RenderLayer::USER_LAYER_11),
					value("USER_LAYER_12", RenderLayer::USER_LAYER_12),
					value("USER_LAYER_13", RenderLayer::USER_LAYER_13),
					value("USER_LAYER_14", RenderLayer::USER_LAYER_14)
				]

			//
			// ObjectSky/BaseVisualObjectEntity specific methods
			//
			.def("GetNumSubsets", &ObjectSky::GetNumSubsets)
				.def("GetMaterials", &ObjectSky::GetMaterials)
				.def("GetMaterialsAsString", &ObjectSky::GetMaterialsAsString)

				.def("GetLoadingPath", &ObjectSky::GetLoadingPath)
				.def("SetLoadingPath", &ObjectSky::SetLoadingPath)

				.def("GetRenderLayer", &ObjectSky::GetRenderLayer)
				.def("SetRenderLayer", &ObjectSky::SetRenderLayer)

				// override LoadFromPath from 	BaseVisualObjectEntity
				.def("LoadFromPath", &ObjectSky::LoadFromPath)

				//.def("GetMax", &ObjectSky::GetMax)
				//.def("GetMin", &ObjectSky::GetMin)

				//.def("GetBBox", &ObjectSky::GetBBox)
				//.def("GetBBox", &ObjectSky::GetBBox)

				//.def("GetCenter", &ObjectSky::GetCenter)
				//.def("GetRadius", &ObjectSky::GetRadius)

				//.def("GetMax", &ObjectSky::GetMax)
				//.def("GetMin", &ObjectSky::GetMin)

				//.def("GetBBox", &ObjectSky::GetBBox)
				//.def("GetBBox", &ObjectSky::GetBBox)

				//.def("GetCenter", &ObjectSky::GetCenter)
				//.def("GetRadius", &ObjectSky::GetRadius)

				.def("GetSubsetName", &ObjectSky::GetSubsetName)

				//.def("GetBSphere", &ObjectSky::GetBSphere)
				//.def("GetBSphere", &ObjectSky::GetBSphere)

				//!@todo Engine:scripting: Material dependency
				.def("GetMaterial", &ObjectSky::GetMaterial)

				.def("GetTransformedBBox", &ObjectSky::GetTransformedBBox)
				.def("GetTransformedBSphere", &ObjectSky::GetTransformedBSphere)

				//.def("SetMaterial", &ObjectSky::SetMaterial)
				//.def("SetMaterial", &ObjectSky::SetMaterial)

				.def("GetTransform", &ObjectSky::GetTransform)

				.def("HasQuery", &ObjectSky::HasQuery)

				.def("SetPhysicsBox", &ObjectSky::SetPhysicsBox)
				.def("SetPhysicsSphere", &ObjectSky::SetPhysicsSphere)

				.def("SetPhysicsCylinder", &ObjectSky::SetPhysicsCylinder)
				.def("SetPhysicsCapsule", &ObjectSky::SetPhysicsCapsule)

				//.def("SetPhysicsConvexHull", &ObjectSky::SetPhysicsConvexHull)
				//.def("SetPhysicsStaticMesh", &ObjectSky::SetPhysicsStaticMesh)
				//			}

				//////////////////////////////////////////////////////////////////////////
				///////////////////////////////PhysXMaterialSh////////////////////////////
				//////////////////////////////////////////////////////////////////////////
				//{
				/*, class_<PhysXMaterialSh>("PhysXMaterialSh")
				.def(constructor<float, float, float>())
				.def(constructor<>())

				.def("AttachScript", (void(PhysXMaterialSh::*)(const String&)) &PhysXMaterialSh::AttachScript)
				.def("ApplyState", (void(PhysXMaterialSh::*)(float, float, float)) &PhysXMaterialSh::ApplyState)

				.def("DestroyFromScene", &PhysXMaterialSh::DestroyFromScene)*/
				//}
				//////////////////////////////////////////////////////////////////////////
				///////////////////////////////CharacterControllerDesc////////////////////
				//////////////////////////////////////////////////////////////////////////
				//{
				, class_<CharacterControllerDesc>("CharacterControllerDesc")
				.def(constructor<>())
				.def(constructor<CharacterControllerDesc::Mode>())
				.enum_("Mode")
				[
					value("CCT_CAPSULE", CharacterControllerDesc::CCT_CAPSULE),
					value("CCT_BOX", CharacterControllerDesc::CCT_BOX)
				]
			.def_readwrite("height", &CharacterControllerDesc::height)
				.def_readwrite("radius", &CharacterControllerDesc::radius)
				.def_readwrite("density", &CharacterControllerDesc::density)
				.def_readwrite("contactOffset", &CharacterControllerDesc::contactOffset)
				.def_readwrite("stepOffset", &CharacterControllerDesc::stepOffset)
				.def_readwrite("invisibleWallHeight", &CharacterControllerDesc::invisibleWallHeight)
				.def_readwrite("maxJumpHeight", &CharacterControllerDesc::maxJumpHeight)
				.def_readwrite("scaleCoeff", &CharacterControllerDesc::scaleCoeff)
				.def_readwrite("slopeLimit", &CharacterControllerDesc::slopeLimit)
				.def_readwrite("volumeGrowth", &CharacterControllerDesc::volumeGrowth)
				.def_readwrite("position", &CharacterControllerDesc::position)
				.def_readwrite("upDirection", &CharacterControllerDesc::upDirection)
				.def_readwrite("mMode", &CharacterControllerDesc::mMode)

				//}
				//////////////////////////////////////////////////////////////////////////
				///////////////////////////////PhysXCharacterController///////////////////
				//////////////////////////////////////////////////////////////////////////
				//{
				, class_<PhysXCharacterController>("PhysXCharacterController")
				.def(constructor<const PhysXCharacterController&>())
				.def("move", &PhysXCharacterController::Move)

				.def("GetPosition", &PhysXCharacterController::GetPosition)
				.def("SetPosition", &PhysXCharacterController::SetPosition)

				.def("Jump", &PhysXCharacterController::Jump)
				.def("CanJump", &PhysXCharacterController::CanJump)

				.def("GetDescription", &PhysXCharacterController::GetDescription)
				//}
				];
		}
		catch (std::exception& e)
		{
			ErrorWrite("[s] Failed binding %s", __FUNCTION__, e.what());
		}
	}
}