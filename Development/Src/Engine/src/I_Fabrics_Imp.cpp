/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"

//***************************************************
#include "../../OGLDrv/inc/GLSystem.h"
#include "../../NULLDrv/NullDrv.h"
//***************************************************

namespace NGTech
{
	I_RenderLowLevel* CreateRenderFabric(RenderDrv _drv)
	{
		Debug(__FUNCTION__);
		auto _engine = GetEngine();
		ASSERT(_engine, "Invalid pointer on _engine");

		if (_drv == RenderDrv::OpenGLDrv)
			return new GLSystem(_engine);
		else if (_drv == RenderDrv::NUM_DRV)
			return new NULLDrv(_engine);

		ASSERT(false, "Incorrect renderDrv value passed");

		return nullptr;
	}
}