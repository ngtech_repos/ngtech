/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/

#include "EnginePrivate.h"
//**************************************
#include <algorithm>
//**************************************
#include "Light.h"
#include "Scene.h"
#include "Frustum.h"
//**************************************

namespace NGTech
{
	/**
	*/
	Light::Light()
	{
		auto scene = GetScene();
		ASSERT(scene, "Not exist pointer on scene");

		if (scene) {
			scene->AddLight(this);
		}

#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
		IsLight = true;
		CLASS_INIT_EDTOR_VARS();
#endif
	}

	/*
	*/
	Light::~Light() {
#ifdef HEAVY_DEBUG
		Debug(__FUNCTION__);
#endif

		auto scene = GetScene();
		ASSERT(scene, "Not exist pointer on scene");

		if (scene) {
			scene->DeleteLightFromList(this);
		}
	}

	/**
	*/
	bool Light::_CalculateVisability(Camera* _cam)
	{
		//MT::ScopedGuard guard(mutex);
		ASSERT(_cam, "not exist _cam");

		lightBD.lightArea = Math::ZEROFLOAT;
		lightBD.mins = { -Math::ONEFLOAT, -Math::ONEFLOAT, -Math::ONEFLOAT, Math::ZEROFLOAT };
		lightBD.maxes = { -Math::ONEFLOAT, -Math::ONEFLOAT, -Math::ONEFLOAT, Math::ZEROFLOAT };

		lightBD.m_Occluded = false;

		// check, if light volume is inside camera frustum
		if (lightBD.useFrustumCulling && (!_cam->GetFrustum()->IsInside(BSphere(GetPosition(), GetRadius()))))
		{
			lightBD.m_Occluded = true;
			return false;
		}

		// calculate bounding box of light sphere in view-space
		Vec3 centerVS = _cam->GetViewMatrix()*this->GetPosition();
		Vec3 corners[8];
		corners[0] = centerVS + Vec3(-lightBD.radius, lightBD.radius, lightBD.radius);
		corners[1] = centerVS + Vec3(-lightBD.radius, -lightBD.radius, lightBD.radius);
		corners[2] = centerVS + Vec3(lightBD.radius, -lightBD.radius, lightBD.radius);
		corners[3] = centerVS + Vec3(lightBD.radius, lightBD.radius, lightBD.radius);
		corners[4] = centerVS + Vec3(-lightBD.radius, lightBD.radius, -lightBD.radius);
		corners[5] = centerVS + Vec3(-lightBD.radius, -lightBD.radius, -lightBD.radius);
		corners[6] = centerVS + Vec3(lightBD.radius, -lightBD.radius, -lightBD.radius);
		corners[7] = centerVS + Vec3(lightBD.radius, lightBD.radius, -lightBD.radius);

		// calculate bounding box in clip-space
		BBox boundingBox;
		boundingBox.Clear();

		for (auto i = 0; i < 8; i++)
		{
			if (corners[i].z > Math::ZEROFLOAT)
				corners[i].z = Math::ZEROFLOAT;

			corners[i] = _cam->GetProjMatrix()*corners[i];
			corners[i] = (corners[i] * 0.5f) + Vec3(0.5f, 0.5f, 0.5f);
			boundingBox.Inflate(corners[i]);
		}

		auto const cvars = GetCvars();
		ASSERT(cvars, "Invalid pointer");
		if (!cvars) return false;

		// calculate light area in screen-space
		auto width = (boundingBox.maxes.x - boundingBox.mins.x) * cvars->r_width;
		auto height = (boundingBox.maxes.y - boundingBox.mins.y) * cvars->r_height;
		lightBD.lightArea = std::max(width, height);

		// clip bounding box
		boundingBox.mins = Vec3(Math::ZEROFLOAT, Math::ZEROFLOAT, Math::ZEROFLOAT);
		boundingBox.maxes = Vec3(Math::ZEROFLOAT, Math::ZEROFLOAT, Math::ZEROFLOAT);

		lightBD.mins = boundingBox.mins;
		lightBD.maxes = boundingBox.maxes;

		return true;
	}

	// TODO:cHECK IMPLEMENT THIS
	void Light::SetNewState(const LightBufferData&_newState)
	{
		if (lightBD == _newState) return;

		auto tmpLightDB = _newState;

		switch (tmpLightDB.mType)
		{
		case LightBufferData::LIGHT_OMNI:
			//Debug("LIGHT_OMNI");
			tmpLightDB.SetOmniParams(tmpLightDB.lumFlux);
			break;
		case LightBufferData::LIGHT_SPOT:
			//Debug("LIGHT_SPOT");
			tmpLightDB.SetupAsSpot(tmpLightDB.lumFlux, this->GetPosition(), tmpLightDB.direction, tmpLightDB.radius, Math::ZEROFLOAT, tmpLightDB.outerTmp);
			break;
		case LightBufferData::LIGHT_DIRECT:
			//Warning("LIGHT_DIRECT NOT IMPLEMENTED");
			break;
		case LightBufferData::LIGHT_ENVPROBE:
			//Debug("LIGHT_ENVPROBE");
			tmpLightDB.SetupAsEnvProbe();
			break;
		case LightBufferData::LIGHT_AREA:
			//Debug("LIGHT_AREA");
			tmpLightDB.SetupAsEnvProbe();
			break;

		default:
			Warning("Invalid LightDB, Invalid  [%s]", __FUNCTION__);
			break;
		}

		lightBDSaved = lightBD;
		lightBD = _newState;
	}

	/*��������� ��������� ���������*/
	void Light::ResetSavedState() {
		lightBDSaved = lightBD;
	}

	bool Light::_CheckVisible(Camera* _camera) {
		//MT::ScopedGuard guard(mutex);
		ASSERT(_camera, "Invalid pointer on _camera");

		//frustum visibility
		if (!_CalculateVisability(_camera))
			return false;

		if (lightBD.disableByDistance && ((this->GetPosition() - _camera->GetPosition()).length() > lightBD.disableByDistanceV)) {
			return false;
		}

		return true;
	}

	void Light::SetShadowBox(const BBox& castersbox)
	{
		//MT::ScopedGuard guard(mutex);
		box = castersbox;

		auto position = GetPosition();
		Vec3 look = position + GetDirection();
		box.FitToBox(lightBD.clipplanes.x, lightBD.clipplanes.y, position, look);
	}

	LightBufferData::LightBufferData()
	{
		/**/
		this->angles.x = cosf(Math::DegreesToRadians(30));
		this->angles.y = cosf(Math::DegreesToRadians(45));
	}

	void LightBufferData::SetOmniParams(TimeDelta _LumFlux)
	{
		this->mType = LightBufferData::LIGHT_OMNI;
		this->fov = 60.0;

		this->innerTmp = 30.0;
		this->outerTmp = 45.0;

		this->angles.x = cosf(Math::DegreesToRadians(this->innerTmp));
		this->angles.y = cosf(Math::DegreesToRadians(this->outerTmp));

		_ReComputeLumFlux(_LumFlux, 0, this->innerTmp, this->outerTmp, this->radius);
	}

	void LightBufferData::_ReComputeLumFlux(TimeDelta _LumFlux, TimeDelta _width, TimeDelta _height, TimeDelta _outer, TimeDelta _radius)
	{
		this->lumFlux = _LumFlux;
		this->widthArea = _width;
		this->heightArea = _height;
		this->outerTmp = _outer;
		this->radius = _radius;

		switch (mType)
		{
		case LIGHT_OMNI:
			// Nick: ���� �� ���� ��� ��� � ������ ���� Flux/4PI
			this->lumintensityTmp = this->lumFlux / (4 * M_PI);
			// Nick: ���������� ������� �� �������� � ������, ��� ������ ���� ����� 1,
			// �.� ����� ��� ������� �� dist*dist
			this->luminanceTmp = 1;

			break;

		case LIGHT_AREA:
			if (this->widthArea == 0 && this->heightArea == 0)
			{
				// sphere
			}
			else if (this->widthArea != 0 && this->heightArea != 0)
			{
				// rectangle
				dirright.Scale(this->widthArea * 0.5f);
				dirup.Scale(this->heightArea * 0.5f);

				lumintensityTmp = 0;
				luminanceTmp = this->lumFlux / (M_PI * this->widthArea * this->heightArea);
			}
			else
			{
				// tube
				float length = Math::Max(this->widthArea, this->heightArea);

				dirright.Scale(length * 0.5f);
				dirup.Scale(this->radius);

				lumintensityTmp = 0;
				luminanceTmp = this->lumFlux / (2 * this->radius * M_PI * M_PI * (length + 2 * this->radius));
			}

			break;

		case LIGHT_ENVPROBE:
			this->lumintensityTmp = 1;

			break;

		case LIGHT_SPOT:
			this->lumintensityTmp = _LumFlux / (2 * M_PI * (1 - cosf(this->outerTmp)));
			this->luminanceTmp = _LumFlux / (2 * M_PI * M_PI * (1 - cosf(this->outerTmp)) * 1e-4f); // assume 1 cm

			break;
		}
	}

	void LightBufferData::SetupAsArea(TimeDelta lumflux, Quat orient, TimeDelta _width, TimeDelta _height, TimeDelta _radius)
	{
		this->mType = LightBufferData::LIGHT_AREA;

		this->orientation = orient;

		this->dirright = Math::X_AXIS;
		this->dirup = Math::Y_AXIS;

		orient.Rotate(dirright, dirright);
		orient.Rotate(dirup, dirup);

		_ReComputeLumFlux(lumflux, _width, _height, 0, _radius);
	}

	void LightBufferData::SetupAsEnvProbe()
	{
		this->mType = LIGHT_ENVPROBE;

		_ReComputeLumFlux(0, 0, 0, 0, 0);
	}

	void LightBufferData::SetupAsSpot(TimeDelta _lumflux, Vec3 _pos, Vec3 _dir, TimeDelta _radius, TimeDelta _inner, TimeDelta _outer)
	{
		this->fov = 60.f;

		this->mType = LIGHT_SPOT;

		this->direction = _dir;

		this->angles.x = cosf(Math::DegreesToRadians(_inner / 2));
		this->angles.y = cosf(Math::DegreesToRadians(_outer * 0.5f));

		_ReComputeLumFlux(_lumflux, 0, _inner, _outer, _radius);
	}

#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
	void Light::_InitEditorVars()
	{
		//MT::ScopedGuard guard(mutex);
#ifdef HEAVY_DEBUG
		Debug(__FUNCTION__);
#endif

		//m_EditorVars.push_back(EditorVar("orientation", &lightBD.orientation, "General", ""));

		// General
		{
			m_EditorVars.push_back(EditorVar("LumFlux", &lightBD.lumFlux, "General", ""));
			m_EditorVars.push_back(EditorVar("�astShadows", &lightBD.castShadows, "General", ""));
			m_EditorVars.push_back(EditorVar("Radius", &lightBD.radius, "General", ""));
			m_EditorVars.push_back(EditorVar("isEnable", &lightBD.enabled, "General", ""));
			m_EditorVars.push_back(EditorVar("isVisible", &lightBD.visible, "General", ""));
		}

		// Color
		{
			m_EditorVars.push_back(EditorVar("color.r", &lightBD.color.x, "Color", ""));
			m_EditorVars.push_back(EditorVar("color.g", &lightBD.color.y, "Color", ""));
			m_EditorVars.push_back(EditorVar("color.b", &lightBD.color.z, "Color", ""));
			m_EditorVars.push_back(EditorVar("color.a", &lightBD.color.w, "Color", ""));
		}

		// Direction
		{
			m_EditorVars.push_back(EditorVar("direction.X", &lightBD.direction.x, "Direction", ""));
			m_EditorVars.push_back(EditorVar("direction.Y", &lightBD.direction.y, "Direction", ""));
			m_EditorVars.push_back(EditorVar("direction.Z", &lightBD.direction.z, "Direction", ""));
		}

		//Optimizations
		{
			m_EditorVars.push_back(EditorVar("isDisableByDistance", &lightBD.disableByDistance, "Optimizations", ""));
			m_EditorVars.push_back(EditorVar("DisableDistanceValue", &lightBD.disableByDistanceV, "Optimizations", ""));
			m_EditorVars.push_back(EditorVar("isFrustumCullingEnabled", &lightBD.useFrustumCulling, "Optimizations", ""));
		}

		BaseEntity::_InitEditorVars();
	}

	void Light::EditorCheckWhatWasModificated()
	{
		//MT::ScopedGuard guard(mutex);
		if (lightBDSaved != lightBD)
		{
			Warning("Apply new state");
			this->SetNewState(lightBD);
		}
	}

	void Light::_AddEditorVars(void) {}
	void Light::AddEditorVarsTo(std::vector<EditorVar>&_vars) {}
	void Light::ReInitEditorVars(void) {}

#endif
}