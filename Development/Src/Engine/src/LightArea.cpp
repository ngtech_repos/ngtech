/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
#include "LightArea.h"

namespace NGTech 
{
	LightArea::LightArea()
	{
		this->lightBD.mType = LightBufferData::LIGHT_AREA;

		this->SetColor(Color(0.5f, 0.5f, 0.5f, 0.0f));
		this->SetPosition(Vec3(0, 0, 0));

#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
		CLASS_INIT_EDTOR_VARS();
#endif
	}

#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)

	void LightArea::AddEditorVarsTo(std::vector<EditorVar>&_vars) {}
	void LightArea::_AddEditorVars() {}

	void LightArea::ReInitEditorVars() {
		//MT::ScopedGuard guard(mutex);
		Light::ReInitEditorVars();
		_InitEditorVars();
	}

	void LightArea::_InitEditorVars() {
		m_EditorVars.push_back(EditorVar("dirright", &lightBD.dirright, "Light Area", ""));
		m_EditorVars.push_back(EditorVar("dirup", &lightBD.dirup, "Light Area", ""));
		m_EditorVars.push_back(EditorVar("widthArea", &lightBD.widthArea, "Light Area", ""));
		m_EditorVars.push_back(EditorVar("heightArea", &lightBD.heightArea, "Light Area", ""));
	}

	void LightArea::EditorCheckWhatWasModificated() {
		Light::EditorCheckWhatWasModificated();
	}
#endif
}