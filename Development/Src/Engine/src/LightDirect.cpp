/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"

#include "LightDirect.h"
#include "Material.h"

namespace NGTech
{
	/**
	*/
	LightDirect::LightDirect()
	{
		this->lightBD.mType = LightBufferData::LIGHT_DIRECT;

		this->SetColor(Color(0.5f, 0.5f, 0.5f, 0.0f));
		this->SetPosition(Vec3(0, 0, 0));

#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
		CLASS_INIT_EDTOR_VARS();
#endif
	}

	/**
	*/
	LightDirect::~LightDirect() {
	}

#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
	void LightDirect::_AddEditorVars() {}

	void LightDirect::AddEditorVarsTo(std::vector<EditorVar>&_vars) {}
	void LightDirect::_InitEditorVars() {}

	void LightDirect::ReInitEditorVars() {
		//MT::ScopedGuard guard(mutex);
		Light::ReInitEditorVars();
		_InitEditorVars();
	}

	void LightDirect::EditorCheckWhatWasModificated() {
		Light::EditorCheckWhatWasModificated();
	}
#endif
}