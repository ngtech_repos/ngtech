/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"

#include "LightEnvProbe.h"

namespace NGTech
{
	static constexpr ENGINE_INLINE void ValidateTextureAndSetToShader(I_Texture* _texture, uint32_t id)
	{
		if (_texture == nullptr)
		{
			Warning("Invalid pointer on for one sampler with id: %i", id);
			return;
		}

		_texture->Set(id);
	}

	void LightEnvProbeShaderActions(Light* _light, uint32_t _diffIrad, uint32_t _specIrad, uint32_t _f0ScaleIrad, I_Shader* _shader)
	{
		ASSERT(_shader, "Invalid pointer on _shader");
		ASSERT(_light, "Invalid pointer on _light");

		auto lightProbeFromLight = static_cast<LightEnvProbe*>(_light);
		ASSERT(lightProbeFromLight, "Invalid cast");

		/*���������� ��������, ��� ���������� - ������ ������*/
		ValidateTextureAndSetToShader(lightProbeFromLight->GetDiffuseIrrad(), _diffIrad);
		ValidateTextureAndSetToShader(lightProbeFromLight->GetSpecularIrrad(), _specIrad);
		ValidateTextureAndSetToShader(lightProbeFromLight->GetIntegratedBRDF(), _specIrad);

		/*ENVPROBE. �������� � ������*/
		_shader->SendInt("envprobe_diffuse_rad", _diffIrad);
		_shader->SendInt("envprobe_specular_rad", _specIrad);
		_shader->SendInt("envprobe_f0_scale_bias", _f0ScaleIrad);

		/*�������� ���������*/
		//_shader->CommitChanges();
	}

	/**
	*/
	LightEnvProbe::LightEnvProbe() {
		_Init();
	}

	void LightEnvProbe::_Init() 
	{
		this->lightBD.mType = LightBufferData::LIGHT_ENVPROBE;

		auto _render = GetRender();
		ASSERT(_render, "Invalid pointer on _render");
		if (!_render) return;

		auto tex = std::make_shared<TextureParamsTransfer>();
		tex->path = "uffizi_diff_irrad.dds";
		tex->m_Type = TextureParamsTransfer::Type::TEXTURE_CUBE;

		skyirraddiff = _render->TextureCreate(tex);
		ASSERT(skyirraddiff, "Invalid pointer on skyirraddiff");
		if (!skyirraddiff) return;

		tex = std::make_shared<TextureParamsTransfer>();
		tex->path = "uffizi_spec_irrad.dds";
		tex->m_Type = TextureParamsTransfer::Type::TEXTURE_CUBE;

		skyirradspec = _render->TextureCreate(tex);
		ASSERT(skyirradspec, "Invalid pointer on skyirradspec");
		if (!skyirradspec) return;

		tex = std::make_shared<TextureParamsTransfer>();
		tex->path = "brdf.png";

		brdf = _render->TextureCreate(tex);
		ASSERT(brdf, "Invalid pointer on brdf");
		if (!brdf) return;

		// dds r16g16 not works
		//!@todo FIXME dds r16g16 not works

		this->lightBD.SetupAsEnvProbe();
		this->SetPosition(Vec3(0, 0, 0));
	}

	/**
	*/
	LightEnvProbe::~LightEnvProbe() {
		SAFE_RELEASE(skyirraddiff);
		SAFE_RELEASE(skyirradspec);
		SAFE_RELEASE(brdf);
	}

#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)

	//!@todo �������� �������������� ���������� ���� �������

	void LightEnvProbe::_AddEditorVars(void) {}
	void LightEnvProbe::AddEditorVarsTo(std::vector<EditorVar>&_vars) {}
	void LightEnvProbe::_InitEditorVars() {}

	void LightEnvProbe::ReInitEditorVars(void) {
		//MT::ScopedGuard guard(mutex);
		Light::ReInitEditorVars();
		_InitEditorVars();
	}

	void LightEnvProbe::EditorCheckWhatWasModificated() {
		Light::EditorCheckWhatWasModificated();
	}

#endif
}