/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"

#include "LightPoint.h"
#include "Material.h"

namespace NGTech
{
	/*
	*/
	LightPoint::LightPoint()
	{
		// ~40 W
		this->lightBD.SetOmniParams(20);

#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
		CLASS_INIT_EDTOR_VARS();
#endif
	}

#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
	void LightPoint::_AddEditorVars(void) {}
	void LightPoint::AddEditorVarsTo(std::vector<EditorVar>&_vars) {}

	void LightPoint::_InitEditorVars(void) {
		m_EditorVars.push_back(EditorVar("FOV", &lightBD.fov, "Point Light", ""));
		m_EditorVars.push_back(EditorVar("Inner angle (Radians)", &lightBD.angles.x, "Point Light", ""));
		m_EditorVars.push_back(EditorVar("Outer angle (Radians)", &lightBD.angles.y, "Point Light", ""));
	}

	void LightPoint::ReInitEditorVars(void) {
		//MT::ScopedGuard guard(mutex);
		Light::ReInitEditorVars();
		CLASS_REINIT_EDITOR_VARS();
	}

	void LightPoint::EditorCheckWhatWasModificated(void) {
		Light::EditorCheckWhatWasModificated();
	}

#endif
}