/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"

#include "LightSpot.h"

namespace NGTech
{
	LightSpot::LightSpot(float _lumflux)
	{
		this->lightBD.mType = LightBufferData::LIGHT_SPOT;

		lightBD.InitAsSpot(_lumflux);
		SetShadowBox(CalculateShadowBox(this));

		CLASS_REINIT_EDITOR_VARS();
	}

#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)

	void LightSpot::AddEditorVarsTo(std::vector<EditorVar>&_vars) {}
	void LightSpot::_AddEditorVars() {}
	void LightSpot::_InitEditorVars() {}

	void LightSpot::ReInitEditorVars()
	{
		//MT::ScopedGuard guard(mutex);
		Light::ReInitEditorVars();
		_InitEditorVars();
	}

	void LightSpot::EditorCheckWhatWasModificated() {
		Light::EditorCheckWhatWasModificated();
	}

#endif
}