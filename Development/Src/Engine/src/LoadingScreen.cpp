/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
//***************************************************************************
#include "LoadingScreen.h"
//***************************************************************************

namespace NGTech {
	/*
	*/
	LoadingScreen::LoadingScreen(const String &_path)
		:path(_path)
	{
		render = GetRender();
		ASSERT(render, "Invalid pointer on render");

		if (render->GetRenderName() == "NULLDrv")
			return;

		auto texTransfer = std::make_shared<TextureParamsTransfer>();
		texTransfer->path = _path;

		background = render->TextureCreate(texTransfer);
		ASSERT(background, "Invalid pointer on background");

		background->SetFilter(Filter::LINEAR);

		screenquad = render->CreateReact();
		basic2D = render->ShaderCreate("basic2d.glsl");
	}

	LoadingScreen::~LoadingScreen() {
		SAFE_RELEASE(basic2D);
		SAFE_RELEASE(screenquad);

		SAFE_RELEASE(background);
	}

	void LoadingScreen::Show()
	{
		ASSERT(render, "Invalid pointer on render");

		if ((!render) || (render && (render->GetRenderName() == "NULLDrv")))
			return;

		ASSERT(screenquad, "Invalid pointer on screenquad");
		ASSERT(basic2D, "Invalid pointer on basic2D");
		ASSERT(background, "Invalid pointer on background");

		if ((!background) || (!screenquad) || (!basic2D) || (!render))
			return;

		render->BeginFrame();
		{
			render->DisableDepth();
			render->DepthMask(false);
			background->Set(0);

			basic2D->Enable();
			basic2D->SendInt("u_albedo_texture", 0);
			basic2D->SendMat4("matTexture", texmat);
			basic2D->CommitChanges();
			{
				screenquad->Draw();
			}
			basic2D->Disable();
			background->Unset(0);

			render->EnableDepth();
			render->DepthMask(true);

			render->ProcessQueue();
		}
		render->EndFrame();
	}
}