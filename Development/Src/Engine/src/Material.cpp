/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
//***************************************************************************
#include "Engine.h"
#include "Material.h"
#include "Log.h"
#include "Config.h"
#include "Cache.h"
#include "Scene.h"
#include "RenderPipeline.h"
#include "CvarManager.h"
//***************************************************************************
#include "PhysXMaterialSh.h"
#include "PhysicsMaterialsManager.h"
//***************************************************************************

namespace NGTech {
	static const String AlbedoSamplerName = "u_albedo_texture";
	static const String NormalSamplerName = "u_normal_texture";
	static const String MetallnessSamplerName = "u_metallic_texture";
	static const String RoughnessSamplerName = "u_roughness_texture";
	static const String HeightMapSamplerName = "u_heightmap_texture";
	static const String AOMapSamplerName = "u_aomap_texture";
	static const String CubeMapSamplerName = "u_cubemap_texture";
	static const String BaseColorMapSamplerName = "u_basecolor_texture";

	// Uniforms
	static const String _AlbedoNotNull = "_AlbedoNotNull";
	static const String _NormalNotNull = "_NormalNotNull";
	static const String _MetallnessNotNull = "_MetallnessNotNull";
	static const String _RoughnessNotNull = "_RoughnessNotNull";
	static const String _HeightMapNotNull = "_HeightMapNotNull";
	static const String _AONotNull = "_AONotNull";
	static const String _CubeMapNotNull = "_CubeMapNotNull";
	static const String _BaseColorNotNull = "_BaseColorNotNull";

	static const String _INVERT_Y_UV = "INVERT_Y_UV";

	/*��������� ���� �� ���������� �� ������������� ���������, ���� ���� ������ ������ ����������, ���
	�� null. ��� �����, ��� ��� � GLSL ������ ���������, ��� sampler �� null.*/
	static constexpr ENGINE_INLINE void _CheckSamplers(const String& _SamplerName, I_Shader* _shader, Material* _mat)
	{
		ASSERT(_shader, "Invalid pointer on _shader");
		if (!_shader) return;

		ASSERT(_mat, "Invalid pointer on _mat");
		if (!_mat) return;

		// ���������� ���������, ��� �� �� �������� � ���������
		if (_SamplerName == AlbedoSamplerName)/*Albedo*/
			_shader->SendInt(_AlbedoNotNull, 1);
		else if (_SamplerName == NormalSamplerName)/*Normal*/
			_shader->SendInt(_NormalNotNull, 1);
		else if (_SamplerName == MetallnessSamplerName)/*Metallness*/
			_shader->SendInt(_MetallnessNotNull, 1);
		else if (_SamplerName == RoughnessSamplerName)/*Roughness*/
			_shader->SendInt(_RoughnessNotNull, 1);
		else if (_SamplerName == HeightMapSamplerName)/*HeightMap*/
			_shader->SendInt(_HeightMapNotNull, 1);
		else if (_SamplerName == AOMapSamplerName)/*AO Map*/
			_shader->SendInt(_AONotNull, 1);
		else if (_SamplerName == BaseColorMapSamplerName)/*BaseColor Texture*/
			_shader->SendInt(_BaseColorNotNull, 1);

		else if (_SamplerName == CubeMapSamplerName)/*CubeMap Texture*/
			_shader->SendInt(_CubeMapNotNull, 1);
		else
			Warning("Invalid samplerName: %s. For shader: %s, for material: %s", _SamplerName.c_str(), _shader->GetPath().c_str(), _mat->GetPath().c_str());
	}

	void Util_LoadMaterial(Material* ptr, const String & path);

	void MaterialSettingsNode::CleanResources() {
		SAFE_RELEASE(m_AttachedPhMatPtr);
	}

	//***************************************************************************
	Material::Material(const String &path, bool _transp, bool _load)
		:Material() {
		if (path == StringHelper::EMPTY_STRING) {
			Warning("[Material::Material] Material loading from blank path,automatic deletion.Check your material list");
			return;
		}

		mPBR.isTransparent = _transp;

		SetPath(path);

		if (_load)
			Load();
	}

	Material::Material()
		:EditableObject()
	{
		passes.clear();

		SetLoaded(false);

		// Default pass
		this->AddPass(std::make_shared<MaterialPassSh>());
	}

	/**
	*/
	Material::Material(const String &path, const Color& color, float roughness, float metalness, bool _transp, bool _load)
		:Material(path, _transp, _load) {
		if (path.empty()) {
			Warning("[Material::Material] Material loading from blank path,automatic deletion.Check your material list");
			return;
		}

		mPBR.isTransparent = _transp;
		mPBR.Roughness = roughness;
		mPBR.Metalness = metalness;
		mPBR.BaseColor = color;

		SetPath(path);

		if (_load)
			Load();
	}

	/**
	*/
	void Material::Load() {
		//MT::ScopedGuard guard(mutex);
		// ������� ������ ��������
		passes.clear();

		if (!OnlyLoad())
		{
			Warning("Failed loading material %s", GetPath().c_str());
			return;
		}

		// ����� ����, ��� ��������� ���, ������������� �������� - �.� ��� ���������
		// ����������, ��� ����� ����� ��������
		CLASS_REINIT_EDITOR_VARS();
	}

	bool Material::OnlyLoad() {
		//MT::ScopedGuard guard(mutex);

		/*if (IsLoaded())
		{
			Warning("[Material::OnlyLoad] Material %s is already loaded", this->GetPath().c_str());
			return false;
		}*/

		//!@todo CLEAN
		//Material mat(*this);

		////begin loading
		//// ��� ����� ��� ����������� mLoaded = true;
		//API_LoadMaterial(&mat, this->GetPath());

		//this->CopyParamsFrom(mat);
		Util_LoadMaterial(this, this->GetPath());

		return true;
	}

	Material::~Material()
	{
		mPBR.CleanResources();
	
		for (const auto &pass : passes)
		{
			if (!pass)
			{
				Warning("Invalid pointer on pass");
				continue;
			}

			/**/
			pass->DeleteDataOnlyFromDestructor(GetPath());
		}

		//Cache::RemoveFromCacheList(this);
	}

	bool Material::SetPass(const String &_passname) {
		//MT::ScopedGuard guard(mutex);
		auto p = SearchFirstElementByNameInContainerUtil<std::shared_ptr<MaterialPassSh>, std::vector<std::shared_ptr<MaterialPassSh>>>(passes, _passname);

		if ((!p) || (p && !p->m_Shader))
		{
			Warning("Failed set pass %s, %s, %i", __FUNCTION__, __FILE__, __LINE__);
			return false;
		}

		currentPass = p;

		p->maxUnitTmp = 0;

		p->m_Shader->Enable();

		for (const auto &it : p->u_float)
		{
			if (!it)
			{
				Warning("Failed push float sampler(for shader:%s) pass %s, %s, %i", GetPath().c_str(), __FUNCTION__, __FILE__, __LINE__);
				continue;
			}
			p->m_Shader->SendFloat(it->SamplerName, it->value);
		}

		for (const auto &it : p->u_Vec2)
		{
			if (!it)
			{
				Warning("Failed push vec2 sampler(for shader:%s) pass %s, %s, %i", GetPath().c_str(), __FUNCTION__, __FILE__, __LINE__);
				continue;
			}
			p->m_Shader->SendVec2(it->SamplerName, it->value);
		}

		for (const auto &it : p->u_Vec3)
		{
			if (!it)
			{
				Warning("Failed push vec3 sampler(for shader:%s) pass %s, %s, %i", GetPath().c_str(), __FUNCTION__, __FILE__, __LINE__);
				continue;
			}
			p->m_Shader->SendVec3(it->SamplerName, it->value);
		}

		for (const auto &it : p->u_Vec4)
		{
			if (!it)
			{
				Warning("Failed push vec4 sampler(for shader:%s) pass %s, %s, %i", GetPath().c_str(), __FUNCTION__, __FILE__, __LINE__);
				continue;
			}
			p->m_Shader->SendVec4(it->SamplerName, it->value);
		}

		for (const auto &it : p->u_Mat4)
		{
			if (!it)
			{
				Warning("Failed push vec4 sampler(for shader:%s) pass %s, %s, %i", GetPath().c_str(), __FUNCTION__, __FILE__, __LINE__);
				continue;
			}
			p->m_Shader->SendMat4(it->SamplerName, it->value);
		}

		/*������� ������������ ��� ��������� � range-based*/
		unsigned int itCounter = 0;
		/**/
		for (const auto &it : p->u_sampler2D)
		{
			if (!it || (it && !it->value))
			{
				Warning("Failed push u_sampler2D sampler(for shader:%s) pass %s, %s, %i", GetPath().c_str(), __FUNCTION__, __FILE__, __LINE__);
				continue;
			}

			p->m_Shader->SendInt(it->SamplerName, p->maxUnitTmp + itCounter);
			it->value->Set(p->maxUnitTmp + itCounter);

			itCounter++;

			/**/
			_CheckSamplers(it->SamplerName, p->m_Shader, this);
		}
		p->maxUnitTmp += p->u_sampler2D.size();

		/**/
		itCounter = 0;
		for (const auto &it : p->u_samplerCube)
		{
			if (!it || (it && !it->value))
			{
				Warning("Failed push u_samplerCube sampler(for shader:%s) pass %s, %s, %i", GetPath().c_str(), __FUNCTION__, __FILE__, __LINE__);
				continue;
			}

			p->m_Shader->SendInt(it->SamplerName, p->maxUnitTmp + itCounter);
			it->value->Set(p->maxUnitTmp + itCounter);

			itCounter++;

			/**/
			_CheckSamplers(it->SamplerName, p->m_Shader, this);
		}
		p->maxUnitTmp += p->u_samplerCube.size();

		/**/
		itCounter = 0;
		for (const auto it : p->u_scene_params)
		{
			if (!it || (it && it->value == MaterialsSceneParam::InCorrect))
			{
				Warning("Failed push u_scene_params sampler(for shader:%s) pass %s, %s, %i", GetPath().c_str(), __FUNCTION__, __FILE__, __LINE__);
				continue;
			}

			auto type = it->value;
			String name = it->SamplerName;

			if (type == MaterialsSceneParam::matTime) {
				p->m_Shader->SendFloat(name, matrixes.matTime);
			}

			/**/
			if (type == MaterialsSceneParam::CamViewPosition) {
				p->m_Shader->SendVec3(name, matrixes.CamViewPosition);
			}

			if (type == MaterialsSceneParam::CamViewDirection) {
				p->m_Shader->SendVec3(name, matrixes.CamViewDirection);
			}

			if (type == MaterialsSceneParam::matGlobalAmbient) {
				p->m_Shader->SendVec4(name, matrixes.matSceneAmbient);
			}

			if (type == MaterialsSceneParam::matMVP) {
				p->m_Shader->SendMat4(name, matrixes.matMVP);
			}

			if (type == MaterialsSceneParam::matProj) {
				p->m_Shader->SendMat4(name, matrixes.matProj);
			}

			if (type == MaterialsSceneParam::matView) {
				p->m_Shader->SendMat4(name, matrixes.matView);
			}

			if (type == MaterialsSceneParam::matWorld) {
				p->m_Shader->SendMat4(name, matrixes.matWorld);
			}

			if (type == MaterialsSceneParam::matWorldInv) {
				p->m_Shader->SendMat4(name, matrixes.matWorldInv);
			}

			if (type == MaterialsSceneParam::matSpotTransform) {
				p->m_Shader->SendMat4(name, matrixes.matSpotTransform);
			}

			if (type == MaterialsSceneParam::matViewportTransform) {
				p->m_Shader->SendMat4(name, matrixes.matViewportTransform);
			}

			if (type == MaterialsSceneParam::matShadowMap && (GetCvars()->r_shadowtype > 0)) {
				if (matrixes.matShadowMap) {
					// TODO:Engine:Material:Shadow mapping: Replace this - needed for implementation of SM
					p->m_Shader->SendInt(name, p->maxUnitTmp);
					matrixes.matShadowMap->Set(p->maxUnitTmp);
				}
				else
				{
#ifdef HEAVY_DEBUG
					DebugM("Can't set matShadowMap for pass: %s, for variable: %s", _passname.c_str(), name.c_str());
#endif
				}
			}

			if (type == MaterialsSceneParam::matViewportMap) {
				if (matrixes.matViewportMap) {
					p->m_Shader->SendInt(name, p->maxUnitTmp + 1);
					matrixes.matViewportMap->Set(p->maxUnitTmp + 1);
				}
				else
				{
#ifdef HEAVY_DEBUG
					DebugM("Can't set matViewportMap for pass: %s, for variable: %s", _passname.c_str(), name.c_str());
#endif
				}
			}

			if (type == MaterialsSceneParam::matSpotMap) {
				if (matrixes.matSpotMap) {
					p->m_Shader->SendInt(name, p->maxUnitTmp + 2);
					matrixes.matSpotMap->Set(p->maxUnitTmp + 2);
				}
				else
				{
#ifdef HEAVY_DEBUG
					DebugM("Can't set MatSpotMap for pass: %s, for variable: %s", _passname.c_str(), name.c_str());
#endif
				}
			}
		}
		//AUTO PARAMS
		{
			// always export
			matrixes.UpdateUniforms(p->m_Shader);

			// Scene
			auto scene = GetScene();
			ASSERT(scene, "Invalid pointer on scene");
			if (scene)
			{
				auto cam = scene->m_ActiveScene.GetCurrentCamera();
				ASSERT(cam, "pointer on cam is invalid");

				p->m_Shader->SendVec3("viewPos", cam->GetPosition());
				p->m_Shader->SendVec3("viewDir", cam->GetDirection());
				p->m_Shader->SendVec2("clipplanes", cam->GetClipDistance());
			}

			// PBR
			{
				p->m_Shader->SendVec4("baseColor", this->mPBR.BaseColor);

				Vec4 matparams(this->mPBR.Roughness, this->mPBR.Metalness, mPBR.hasTexture ? 1 : 0, mPBR.hasNormal ? 1 : 0);

				p->m_Shader->SendVec4("matParams", matparams);

				p->m_Shader->SendFloat("uIOR", mPBR.IOR);
			}
		}

		p->m_Shader->CommitChanges();

		return true;
	}

	void Material::LoadingFinishedEvent() {
		//MT::ScopedGuard guard(mutex);
		CLASS_REINIT_EDITOR_VARS();

		SetLoaded(true);
	}

	/**
	*/
	void Material::UnsetPass() {
		//MT::ScopedGuard guard(mutex);
		std::shared_ptr<MaterialPassSh> p = currentPass;
		if (!p) return;

		p->maxUnitTmp = 0;
		p->m_Shader->Disable();

		/**/
		unsigned int m_Counter = 0;
		for (auto sampler : p->u_sampler2D)
		{
			if ((sampler == nullptr) || (sampler && (sampler->value == nullptr)))
			{
				Warning("sampler is null: %b or sampler->value is null: %b", sampler == nullptr, (sampler && (sampler->value == nullptr)));
				continue;
			}
			sampler->value->Unset(p->maxUnitTmp + m_Counter);
			m_Counter++;
		}

		p->maxUnitTmp += p->u_sampler2D.size();

		/**/
		m_Counter = 0;
		for (auto sampler : p->u_samplerCube) {
			if ((sampler == nullptr) || (sampler && (sampler->value == nullptr)))
			{
				Warning("sampler is null: %b or sampler->value is null: %b", sampler == nullptr, (sampler && (sampler->value == nullptr)));
				continue;
			}
			sampler->value->Unset(p->maxUnitTmp + m_Counter);
			m_Counter++;
		}

		p->maxUnitTmp += p->u_samplerCube.size();

		/**/
		if (matrixes.matShadowMap && (GetCvars()->r_shadowtype > 0)) matrixes.matShadowMap->Unset(p->maxUnitTmp);

		if (matrixes.matViewportMap) matrixes.matViewportMap->Unset(p->maxUnitTmp + 1);
		if (matrixes.matSpotMap) matrixes.matSpotMap->Unset(p->maxUnitTmp + 2);
	}

	/**
	*/
	bool Material::PassHasBlending(const String &name) const {
		//MT::ScopedGuard guard(mutex);
		std::shared_ptr<MaterialPassSh> p = nullptr;

		//!@todo Replace on std::find , ��� ���� ����� ������������ std::set
		for (size_t i = 0; i < passes.size(); i++) {
			if (!passes[i])
				continue;

			if (passes[i]->name == name) {
				p = passes[i];
				return p->hasBlending;
			}
		}
		if (!p) return false;

		return p->hasBlending;
	}

	/**
	*/
	void Material::SetPassBlending(const String &name) {
		//MT::ScopedGuard guard(mutex);
		auto p = SearchFirstElementByNameInContainerUtil<std::shared_ptr<MaterialPassSh>, std::vector<std::shared_ptr<MaterialPassSh>>>(passes, name);

		if (!p) {
			Warning("[%s] Invalid pointer on current pass", __FUNCTION__);
			return;
		}

		auto _render = GetRender();
		ASSERT(_render, "Invalid pointer on Render");

		if (!_render)
			return;

		if (p->hasBlending) {
			_render->DepthMask(false);
			_render->EnableBlending(p->src, p->dst);
		}
	}

	/**
	*/
	void Material::UnsetPassBlending() {
		//MT::ScopedGuard guard(mutex);
		std::shared_ptr<MaterialPassSh> p = currentPass;
		if (!p)
		{
			Warning("[%s] Invalid pointer on current pass", __FUNCTION__);
			return;
		}

		auto _render = GetRender();
		ASSERT(_render, "Invalid pointer on Render");

		if (!_render)
			return;

		if (p->hasBlending) {
			_render->DisableBlending();
			_render->DepthMask(true);
		}
	}

	/**
	*/
	void Material::SendFloat(const String &name, float value) {
		//MT::ScopedGuard guard(mutex);
		if ((!currentPass) || (currentPass && !currentPass->m_Shader))
		{
			Warning("[%s] Invalid pointer on current pass or invalid shader", __FUNCTION__);
			return;
		}

		ASSERT(currentPass, "currentPass not exist");
		ASSERT(currentPass->m_Shader, "currentPass->m_Shader not exist");

		currentPass->m_Shader->SendFloat(name, value);
	}

	/**
	*/
	void Material::SendInt(const String &name, int value) {
		//MT::ScopedGuard guard(mutex);
		if ((!currentPass) || (currentPass && !currentPass->m_Shader))
		{
			Warning("[%s] Invalid pointer on current pass or invalid shader", __FUNCTION__);
			return;
		}

		ASSERT(currentPass, "currentPass not exist");
		ASSERT(currentPass->m_Shader, "currentPass->m_Shader not exist");

		currentPass->m_Shader->SendInt(name, value);
	}

	/**
	*/
	void Material::SendVec2(const String &name, const Vec2 & value) {
		//MT::ScopedGuard guard(mutex);
		if ((!currentPass) || (currentPass && !currentPass->m_Shader))
		{
			Warning("[%s] Invalid pointer on current pass or invalid shader", __FUNCTION__);
			return;
		}

		ASSERT(currentPass, "currentPass not exist");
		ASSERT(currentPass->m_Shader, "currentPass->m_Shader not exist");

		currentPass->m_Shader->SendVec2(name, value);
	}

	/**
	*/
	void Material::SendVec3(const String &name, const Vec3 & value) {
		//MT::ScopedGuard guard(mutex);
		if ((!currentPass) || (currentPass && !currentPass->m_Shader)) {
			Warning("[%s] Invalid pointer on current pass or invalid shader", __FUNCTION__);
			return;
		}

		ASSERT(currentPass, "currentPass not exist");
		ASSERT(currentPass->m_Shader, "currentPass->m_Shader not exist");

		currentPass->m_Shader->SendVec3(name, value);
	}

	/**
	*/
	void Material::SendVec4(const String &name, const Vec4 & value) {
		//MT::ScopedGuard guard(mutex);
		if ((!currentPass) || (currentPass && !currentPass->m_Shader)) {
			Warning("[%s] Invalid pointer on current pass or invalid shader", __FUNCTION__);
			return;
		}

		ASSERT(currentPass, "currentPass not exist");
		ASSERT(currentPass->m_Shader, "currentPass->m_Shader not exist");

		currentPass->m_Shader->SendVec4(name, value);
	}

	/**
	*/
	void Material::SendMat4(const String &name, const Mat4 & value) {
		//MT::ScopedGuard guard(mutex);
		if ((!currentPass) || (currentPass && !currentPass->m_Shader)) {
			Warning("[%s] Invalid pointer on current pass or invalid shader", __FUNCTION__);
			return;
		}

		ASSERT(currentPass, "currentPass not exist");
		ASSERT(currentPass->m_Shader, "currentPass->m_Shader not exist");

		currentPass->m_Shader->SendMat4(name, value);
	}

	void Material::_Swap(const Material&_other) {
		//MT::ScopedGuard guard(mutex);
		if (this != &_other) {
			this->currentPass = _other.currentPass;
			this->mPBR = _other.mPBR;
			this->passes = _other.passes;

			EditableObjectShared::_Swap(_other);
		}
	}

	const std::shared_ptr<MaterialPassSh>& Material::GetPass(size_t _i) {
		//MT::ScopedGuard guard(mutex);
		//Debug(StringHelper::StickString("Requested pass number: %i", _i).c_str());

		if (_i < passes.size())
			return passes[_i];

		Warning("[%s] Invalid size", __FUNCTION__);
		return nullptr;
	}

	void Material::AddPass(const std::shared_ptr<MaterialPassSh>& _pass) {
		//MT::ScopedGuard guard(mutex);
		passes.push_back(_pass);
	}

	void Material::SetBRDFModel(unsigned int _model) {
		//MT::ScopedGuard guard(mutex);
		//DebugM("[Material::SetBRDFModel] Called with %i", _model);
		mPBR.mBRDF = static_cast<MaterialSettingsNode::BRDF_MODEL>(_model);
	}

	void Material::AttachPhysXMaterialByPtr(PhysXMaterialSh* _phMat)
	{
		ASSERT(_phMat, "Invalid pointer on _phMat provided");
		if (!_phMat) return;

		if (mPBR.m_AttachedPhMatPtr == _phMat) return;

		auto old = mPBR.m_AttachedPhMatPtr;
		mPBR.m_AttachedPhMatPtr = _phMat;

		if (!old)
			return;

		phMatManager.FreeMaterial(old);
	}

#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
	void Material::_InitEditorVars() {
		//MT::ScopedGuard guard(mutex);
		// ��������� �� ���������� ������� ��������
		auto DefPass = GetPass(0);

		if (!DefPass) {
			Warning("[%s] Invalid material %s. Passes is null", __FUNCTION__, this->GetPath().c_str());
			return;
		}

		// �������� ��� �������
		for (const auto &pass : passes)
		{
			m_EditorVars.push_back(EditorVar("defines", &pass->defines, pass->name, ""));
			////m_EditorVars.push_back(EditorVar("alphaFunc", &pass->alphaFunc, pass->name, "Path to loading screen"));
			m_EditorVars.push_back(EditorVar("alphaRef", &pass->alphaRef, pass->name, ""));
			m_EditorVars.push_back(EditorVar("hasAlphaTest", &pass->hasAlphaTest, pass->name, ""));
			m_EditorVars.push_back(EditorVar("hasBlending", &pass->hasBlending, pass->name, ""));
			m_EditorVars.push_back(EditorVar("castShadows", &pass->castShadows, pass->name, ""));
			m_EditorVars.push_back(EditorVar("recieveShadows", &pass->recieveShadows, pass->name, ""));
		}

		// Physics Material
		// TODO:Implement this
		static bool empty = false;
		{
			m_EditorVars.push_back(EditorVar("Physics Material", &empty, "Physics Material", ""));
			m_EditorVars.push_back(EditorVar("Physics Material Mask", &empty, "Physics Material", ""));
			m_EditorVars.push_back(EditorVar("Physics Material Mask UVChannel", &empty, "Physics Material", ""));
			m_EditorVars.push_back(EditorVar("Black Physics Material", &empty, "Physics Material", ""));
			m_EditorVars.push_back(EditorVar("White Physics Material", &empty, "Physics Material", ""));
		}

		// Rendering
		{
			m_EditorVars.push_back(EditorVar("Enable Subsurface Scattering", &empty, "Rendering", ""));
			m_EditorVars.push_back(EditorVar("Tessellation Mode", &empty, "Rendering", ""));
			m_EditorVars.push_back(EditorVar("Use Image Based Reflections", &empty, "Rendering", ""));
		}

		// PBR
		{
			m_EditorVars.push_back(EditorVar("ForwardPass", &mPBR.isTransparent, "PBR", ""));

			m_EditorVars.push_back(EditorVar("BaseColor", &mPBR.BaseColor, EditorVar::COLOR, "PBR", "Base Color simply defines the overall color of the Material. It takes in a Vector3 (RGB) value and each channel is automatically clamped between 0 and 1."));
			m_EditorVars.push_back(EditorVar("Roughness", &mPBR.Roughness, EditorVar::FLOAT, "PBR", "The Roughness input literally controls how rough the Material is. A rough Material will scatter reflected light in more directions than a smooth Material. This can be seen in how blurry or sharp the reflection is or in how broad or tight the specular highlight is. Roughness of 0 (smooth) is a mirror reflection and roughness of 1 (rough) is completely matte or diffuse.", true, true, 100, 0, 100));
			m_EditorVars.push_back(EditorVar("Metalness", &mPBR.Metalness, EditorVar::FLOAT, "PBR", "The Metallic input literally controls how 'metal - like' your surface will be. Nonmetals have Metallic values of 0, metals have Metallic values of 1. For pure surfaces, such as pure metal, pure stone, pure plastic, etc. this value will be 0 or 1, not anything in between. When creating hybrid surfaces like corroded, dusty, or rusty metals, you may find that you need some value between 0 and 1.", true, true, 100, 0, 100));

			//!@todo ���� �������� ��� �������
			m_EditorVars.push_back(EditorVar("HasTexture", &mPBR.hasTexture, "PBR", ""));
			m_EditorVars.push_back(EditorVar("HasNormal", &mPBR.hasNormal, "PBR", ""));

			m_EditorVars.push_back(EditorVar("IOR", &mPBR.IOR, "PBR", ""));

			//!@todo: uncomment this
			//m_EditorVars.push_back(EditorVar("BRDF Model", &mPBR.mBRDF, "PBR", ""));
		}
	}

	void Material::EditorCheckWhatWasModificated(void) {}
	void Material::_AddEditorVars(void) {}
	void Material::AddEditorVarsTo(std::vector<EditorVar>&_vars) {}
	void Material::ReInitEditorVars(void) {}
#endif
}