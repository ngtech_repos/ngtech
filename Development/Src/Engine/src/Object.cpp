/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
//**************************************
#include "Scene.h"
//**************************************

namespace NGTech {
	BaseEntity::BaseEntity()
	{
		SetEnabled(true);

		_AddToSceneList();
		this->_AddThisToContainerOnScene();

#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
		/*EditableObject*/
		IsPrefab = true;

		_InitEditorVars();
#endif
	}

	BaseEntity::~BaseEntity() {
#ifdef HEAVY_DEBUG
		Debug(__FUNCTION__);
#endif
		this->_DeleteThisToContainerFromScene();
	}

	void BaseEntity::AttachScript(const String & _name)
	{
		//MT::ScopedGuard guard(mutex);
		Warning("%s (WithName) %s", __FUNCTION__, _name.c_str());
		auto sm = GetScriptManager();
		if (!sm)
		{
			Warning("[%s] Not exist script manager", __FUNCTION__);
			return;
		}

		auto script = sm->GetScriptByName(_name);
		if (script == nullptr)
			script = new Script(_name);

		ScriptableObject::AttachScript(script);
	}

	void BaseEntity::AttachScript(ScriptInterface * _script)
	{
		//MT::ScopedGuard guard(mutex);
		if (!_script)
		{
			Warning("[%s] Not exist pointer, failed attach to script manager", __FUNCTION__);
			return;
		}

		AttachScript(std::shared_ptr<ScriptInterface>(_script));
	}

	void BaseEntity::AttachScript(const std::shared_ptr<ScriptInterface>& _sc)
	{
		//MT::ScopedGuard guard(mutex);

		ASSERT(_sc, "Not exist pointer on _sc");

		if (!_sc)
			return;

		DebugM("%s %s", __FUNCTION__, _sc->GetName().c_str());

		auto sm = GetScriptManager();
		if (!sm)
		{
			Warning("[%s] Not exist script manager", __FUNCTION__);
			return;
		}

		auto script = sm->GetScriptByName(_sc->GetName());
		if (script == nullptr)
			sm->AttachScript(_sc);

		ScriptableObject::AttachScript(_sc);
	}

#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
	void BaseEntity::_InitEditorVars()
	{
		ReInitEditorVars();
	}

	void BaseEntity::EditorCheckWhatWasModificated() {
		if (mSavedState != mState)
			SetNewState(mState);
	}

	void BaseEntity::_AddEditorVars() {}

	void BaseEntity::AddEditorVarsTo(std::vector<class NGTech::EditorVar> &) {}

	void BaseEntity::ReInitEditorVars() {
		//MT::ScopedGuard guard(mutex);
		TODO("��� �� UPVector? �������� ��������");
		// Up Vector
		m_EditorVars.push_back(EditorVar("upVector.X", &mState.upVector.x, "Up Vector", "Path to loading screen"));
		m_EditorVars.push_back(EditorVar("upVector.Y", &mState.upVector.y, "Up Vector", "Path to loading screen"));
		m_EditorVars.push_back(EditorVar("upVector.Z", &mState.upVector.z, "Up Vector", "Path to loading screen"));
		// Position
		m_EditorVars.push_back(EditorVar("Pos.X", &mState.transform.e[12], "Position", "World Position.x"));
		m_EditorVars.push_back(EditorVar("Pos.Y", &mState.transform.e[13], "Position", "World Position.y"));
		m_EditorVars.push_back(EditorVar("Pos.Z", &mState.transform.e[14], "Position", "World Position.z"));
		// Scale
		//m_EditorVars.push_back(EditorVar("Scale1.X", &mState.m_Scale.x, "Scale", "Scale.x"));
		//m_EditorVars.push_back(EditorVar("Scale1.Y", &mState.m_Scale.y, "Scale", "Scale.y"));
		//m_EditorVars.push_back(EditorVar("Scale.Z", &mState.m_Scale.z, "Scale", "Scale.z"));

		//m_EditorVars.push_back(EditorVar("ScaleNEW.X", &mState.transform.GetScaleForChange_X(), "Scale", "Scale.x"));
		//m_EditorVars.push_back(EditorVar("ScaleNEW.Y", &mState.transform.GetScaleForChange_Y(), "Scale", "Scale.y"));
		//m_EditorVars.push_back(EditorVar("ScaleNEW.Z", &mState.transform.GetScaleForChange_Z(), "Scale", "Scale.z"));

		m_EditorVars.push_back(EditorVar("scaleOLD.X", &mState.m_Scale.x, "Scale", "Scale.x"));
		m_EditorVars.push_back(EditorVar("scaleOLD.Y", &mState.m_Scale.y, "Scale", "Scale.y"));
		m_EditorVars.push_back(EditorVar("scaleOLD.Z", &mState.m_Scale.z, "Scale", "Scale.z"));

		//!@todo CHECK
		// Direction
	/*	m_EditorVars.push_back(EditorVar("direction.X", &transform.e[0], "Scale", "Path to loading screen"));
		m_EditorVars.push_back(EditorVar("direction.Y", &transform.e[5], "Scale", "Path to loading screen"));
		m_EditorVars.push_back(EditorVar("direction.Z", &transform.e[10], "Scale", "Path to loading screen"));*/

		m_EditorVars.push_back(EditorVar("Angle", &mState.angle, "General", "Angle of orientations"));
		m_EditorVars.push_back(EditorVar("Enabled", &enabled, "General", "Is enabled this object for rendering"));

		//!@todo CHECK: ��� �� ����� �������� �����
		m_EditorVars.push_back(EditorVar("Use HW Occlusion Query", &mState.useHWOQuery, "Rendering", "Optimization, can be slow on different hardware."));
	}

#endif

	void BaseEntity::_AddThisToContainerOnScene() { auto scene = GetScene(); ASSERT(scene, "Invalid scene"); if (!scene) return;  scene->AddObject(this); }
	void BaseEntity::_DeleteThisToContainerFromScene() { auto scene = GetScene(); ASSERT(scene, "Invalid scene");  if (!scene) return; scene->DeleteObject(this); }
}