/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
//**************************************
#include "ObjectMesh.h"
#include "Log.h"
#include "Cache.h"
#include "Scene.h"
#include "Material.h"
//**************************************
#include "BasePhysicsObjectEntity.h"
//**************************************
#include "../../inc/PhysicsMaterialsManager.h"
//**************************************
//#define DISABLE_PHYSICS 1

namespace NGTech
{
	/**
	*/
	static constexpr ENGINE_INLINE void _ApplyMaterialUtil(std::vector<MaterialSubset> &materials, size_t s, Material *material)
	{
		ASSERT(material, "Invalid pointer");
		if (!material) return;

		materials[s].material = material;
		materials[s].materialName = material->GetPath();
	}

	static constexpr ENGINE_INLINE void _ApplyMaterialUtil(std::vector<MaterialSubset> &materials, Material* material)
	{
		ASSERT(material, "Invalid pointer");
		if (!material) return;

		for (auto& mat : materials)
		{
			if (!material)
			{
				ErrorWrite("Not exist material pointer");
				continue;
			}

			mat.material = material;
			mat.materialName = material->GetPath();
		}
	}

	static ENGINE_INLINE void _CleanResourcesUtil(
		const std::shared_ptr<Cache>& _cache,
		SceneManager* _scene,
		I_OcclusionQuery* _query,
		Mesh* _m_StaticMesh,
		std::vector<MaterialSubset>& materials,
		std::vector<MaterialSubset>& OldMaterials,
		const char* _loadingPatch
	)
	{
		ASSERT(_cache, "Cache not exist");
		if (!_cache) return;
		ASSERT(_scene, "Not exist scene pointer");
		if (!_scene) return;

		SAFE_RELEASE(_query);

		// already called
		// !@bug
		//SetPhysicsNone(); //��������

		if (_m_StaticMesh)
			Cache::DeleteResource(_m_StaticMesh);
		for (const auto& matRow : materials)
			Cache::DeleteResource(matRow.material);

		// �������������� �������
		DeleteMatFromList(materials, _cache);
		DeleteMatFromList(OldMaterials, _cache);

		//this->DeleteImpactSound();

		//if (scene)
		//	scene->DeleteObjectMeshFromList(this);

		//UpdatableObject::_RemoveFromScene();
	}

	ObjectMesh::ObjectMesh(bool _addToSceneList)
	{
		tempdata.mesh.m_StaticMesh = nullptr;

		if (_addToSceneList)
			_AddToSceneList();

		UseHWOcclusionQuery(false);
	}

	ObjectMesh::ObjectMesh(const String& _path)
		:ObjectMesh(true)
	{
		LoadFromPath(String(_path));
	}

	ObjectMesh::ObjectMesh(const char*_c)
		: ObjectMesh(String(_c))
	{}

	void ObjectMesh::LoadFromPath(const String &_path)
	{
		//MT::ScopedGuard guard(mutex);
		auto cache = GetCache();
		auto _mLoadingPath = "meshes/" + _path;

		this->SetLoadingPath(_path);

		tempdata.mesh.m_StaticMesh = Cache::LoadModel(_mLoadingPath);

		if (!tempdata.mesh.m_StaticMesh) {
			ErrorWrite("[ObjectMesh::LoadFromPath] Failed loading of model %s", _mLoadingPath.c_str());
			return;
		}

		tempdata.mNumSubsets = tempdata.mesh.m_StaticMesh->GetNumSubsets();
		if (tempdata.mNumSubsets <= 0)
		{
			ErrorWrite("[ObjectMesh::LoadFromPath] Invalid subsets count %i", tempdata.mNumSubsets);
			return;
		}

		ASSERT((tempdata.mNumSubsets > 0), "[ObjectMesh::LoadFromPath] Invalid subsets count");

		tempdata.materials.resize(tempdata.mNumSubsets);
		tempdata.OldMaterials.resize(tempdata.mNumSubsets);

		if (m_PhysicsEntity)
			m_PhysicsEntity->SetOriginalMeshNamePhysX(_mLoadingPath);

		// standard materials
		SetMaterial(DefaultResources::Material_Default_Name, "*");

		// ����� ����, ��� ��������� ���, ������������� �������� - �.� ��� ���������
		// ����������, ��� ����� ����� ��������
		CLASS_INIT_EDTOR_VARS();
	}

	ObjectMesh::~ObjectMesh()
	{
		//Warning("~ObjectMesh");
		//_CleanResourcesUtil(GetCache(), GetScene(), query, mesh.m_StaticMesh, materials, OldMaterials, this->GetLoadingPath().c_str());
		SAFE_DELETE(m_PhysicsEntity);
	}

	void ObjectMesh::DrawSubset(size_t subset, TimeDelta _delta) {
		//MT::ScopedGuard guard(mutex);
		ASSERT(tempdata.mesh.m_StaticMesh, "Invalid pointer on mesh.m_StaticMesh");

		if (!tempdata.mesh.m_StaticMesh || !GetEnabled())
			return;

		this->tempdata.mesh.m_StaticMesh->DrawSubsetShared(static_cast<unsigned int>(subset));
	}

	Material* ObjectMesh::GetMaterial(unsigned int s) const {
		//MT::ScopedGuard guard(mutex);
		ASSERT(tempdata.mesh.m_StaticMesh, "Invalid pointer on mesh.m_StaticMesh");
		// ��������� ����� materials.size() < s
		ASSERT(tempdata.materials.size() > s, "Not exist materials[s]");
		ASSERT(tempdata.materials.size() != 0, "Not exist materials[s]");

		//Warning("GetMaterial size: %i, req: %i", materials.size(), s);

		//ASSERT(materials[s].material, "Not exist materials[s].material");

		/*
		weak_ptr doesn't have any constructors that take a nullptr_t or raw pointers, so you cannot construct one with nullptr as an argument. To obtain an empty weak_ptr just default construct one.
		https://stackoverflow.com/questions/22720692/why-cant-i-return-a-nullptr-stdweak-ptr?lq=1
		*/
		if (!tempdata.mesh.m_StaticMesh)
			return nullptr;

		if (tempdata.materials.empty() || tempdata.materials.size() < s || tempdata.materials.size() == 0)
			return nullptr;

		return tempdata.materials[s].material;
	}

	void ObjectMesh::SetMaterial(const String &_path, const String &_name) {
		//MT::ScopedGuard guard(mutex);

		ASSERT(tempdata.mesh.m_StaticMesh, "Invalid pointer");

		if (tempdata.mesh.m_StaticMesh == nullptr) {
			Warning("[ObjectMesh::SetMaterial] Invalid pointer on mesh: %s. Can't set material: %s, for preset: %s", GetLoadingPath().c_str(), _path.c_str(), _name.c_str());
			return;
		}

		Material* material = Cache::LoadMaterial(_path);

		ASSERT(material, "Invalid pointer on material");

		if (!material)
		{
			ErrorWrite("Failed loading material with name: %s for subset with name: %s", _path.c_str(), _name.c_str());
			return;
		}

		if (_name == "*")
		{
			_ApplyMaterialUtil(tempdata.materials, material);
			_ApplyMaterialUtil(tempdata.OldMaterials, material);
		}

		auto subsetID = tempdata.mesh.m_StaticMesh->GetSubset(_name);

		if (subsetID < tempdata.materials.size())
		{
			// ���� ������ ���������� ������, ��� �� ������ � �� ���������� ��������� ��� ������ ������
			if (tempdata.materials.empty() || (!tempdata.materials.empty() && tempdata.materials[subsetID].material == nullptr))
			{
				Warning("[%s] Invalid subsetID %i for subset name - %s", __FUNCTION__, subsetID, _name.c_str());
				return;
			}

			ASSERT(material, "Invalid pointer");

			tempdata.materials[subsetID].material = material;
			tempdata.materials[subsetID].subsetName = _name;
			tempdata.materials[subsetID].materialName = material->GetPath();

			tempdata.OldMaterials[subsetID].material = material;
			tempdata.OldMaterials[subsetID].subsetName = _name;
			tempdata.OldMaterials[subsetID].materialName = material->GetPath();
		}
		else
			Warning("[%s] Invalid subset name - %s", __FUNCTION__, _name.c_str());

		/**/
		_SetPhysicsMaterial(material);
	}

	void ObjectMesh::SetMaterial(Material *_material, const String &name) {
		//MT::ScopedGuard guard(mutex);

		ASSERT(tempdata.mesh.m_StaticMesh, "Invalid pointer");

		ASSERT(_material, "Not exist shared pointer on material");

		if (tempdata.mesh.m_StaticMesh == nullptr || _material == nullptr)
			return;

		if (name == "*")
		{
			for (size_t s = 0; s < tempdata.mesh.m_StaticMesh->GetNumSubsets(); s++)
			{
				_ApplyMaterialUtil(tempdata.materials, s, _material);
				_ApplyMaterialUtil(tempdata.OldMaterials, s, _material);
			}
		}
		auto size = tempdata.mesh.m_StaticMesh->GetSubset(name);
		if (size <= tempdata.materials.size())
		{
			_ApplyMaterialUtil(tempdata.materials, size, _material);
			_ApplyMaterialUtil(tempdata.OldMaterials, size, _material);
		}
		else
			Warning("[%s] Invalid subset name - %s", __FUNCTION__, name.c_str()); //-V111

		/*Loading material Immediately*/
		_material->Load();

		/**/
		_SetPhysicsMaterial(_material);
	}

	void ObjectMesh::SetTransform(const Mat4 &_trans) {
		//MT::ScopedGuard guard(mutex); //!@todo CHECK!
		BaseEntity::SetTransform(_trans);

		// info:https://stackoverflow.com/questions/18591924/how-to-use-bitmask
		bool _paused = ((GetEngineState() & (STATE_PAUSED | STATE_PHYSICS_PAUSED | STATE_EDITOR_PAUSED | STATE_LOADING)) == (STATE_PAUSED | STATE_PHYSICS_PAUSED | STATE_EDITOR_PAUSED | STATE_LOADING));

		if (m_PhysicsEntity && (_paused == false) && m_PhysicsEntity->IsValidPhysXBody() == true)
			m_PhysicsEntity->SetPhysXBodyTransform(mState.transform);
		else if (m_PhysicsEntity && (m_PhysicsEntity->IsValidPhysXBody() == false))
		{
			//Debug("Invalid pointer on mPhysicsObjectData.pBody");
		}
	}

	const Mat4& ObjectMesh::GetTransformForChange() {
		//MT::ScopedGuard guard(mutex);

		bool _paused = ((GetEngineState() & (STATE_PAUSED | STATE_PHYSICS_PAUSED | STATE_EDITOR_PAUSED | STATE_LOADING)) == (STATE_PAUSED | STATE_PHYSICS_PAUSED | STATE_EDITOR_PAUSED | STATE_LOADING));

		if (mSavedState.transform.GetScale() != this->mState.m_Scale)
		{
			this->Scale(this->mState.m_Scale);
			return mState.transform;
		}

		if (m_PhysicsEntity && (m_PhysicsEntity->IsValidPhysXBody() == true) && (_paused == false)) {
			return m_PhysicsEntity->mPhysicsObjectData->m_AttachedPBody->GetTransformForChange();
		}

		return mState.transform;
	}

	Mat4 ObjectMesh::GetTransform() {

		bool _paused = ((GetEngineState() & (STATE_PAUSED | STATE_PHYSICS_PAUSED | STATE_EDITOR_PAUSED | STATE_LOADING)) == (STATE_PAUSED | STATE_PHYSICS_PAUSED | STATE_EDITOR_PAUSED | STATE_LOADING));

		if (mSavedState.transform.GetScale() != this->mState.m_Scale)
		{
			this->Scale(this->mState.m_Scale);
			return mState.transform;
		}

		if (m_PhysicsEntity && (m_PhysicsEntity->IsValidPhysXBody() == true) && (_paused == false)) {
			return m_PhysicsEntity->mPhysicsObjectData->m_AttachedPBody->GetTransform();
		}

		return mState.transform;
	}

	void ObjectMesh::Scale(const Vec3&_m_Scale) {
		//MT::ScopedGuard guard(mutex);

		/*if (mSavedState.transform.GetScale() == _m_Scale)
		{
			Warning("����������");
			return;
		}
*/
		ObjectStateDesc newState(mState);
		newState.transform.SetScale(_m_Scale);

		SetNewState(newState);

		if (m_PhysicsEntity && m_PhysicsEntity->mPhysicsObjectData)
		{
			m_PhysicsEntity->mPhysicsObjectData->m_PhysDesc.TransformTmp.SetScale(_m_Scale);
			//this->SetPhysicsNone();
			m_PhysicsEntity->ReplacePhysicsDesc(m_PhysicsEntity->mPhysicsObjectData->m_PhysDesc);
		}

		SetTransform(newState.transform);

		Warning("test");
	}

	void ObjectMesh::SetPhysicsBox(const Vec3 &size, TimeDelta mass) {
		//MT::ScopedGuard guard(mutex);

		ASSERT(m_PhysicsEntity, "Invalid pointer on m_PhysicsEntity");
		if (!m_PhysicsEntity) return;

		m_PhysicsEntity->SetPhysBody(nullptr);
		m_PhysicsEntity->_setPhysicsBox(size, mass, mState.transform);
	}

	void ObjectMesh::SetPhysicsSphere(TimeDelta rad, TimeDelta mass) {
		//MT::ScopedGuard guard(mutex);

		ASSERT(m_PhysicsEntity, "Invalid pointer on m_PhysicsEntity");
		if (!m_PhysicsEntity) return;

		m_PhysicsEntity->SetPhysBody(nullptr);
		m_PhysicsEntity->_setPhysicsSphere(rad, mass, mState.transform);
	}

	void ObjectMesh::SetPhysicsCylinder(TimeDelta radius, TimeDelta width, TimeDelta mass) {
		//MT::ScopedGuard guard(mutex);

		ASSERT(m_PhysicsEntity, "Invalid pointer on m_PhysicsEntity");
		if (!m_PhysicsEntity) return;

		m_PhysicsEntity->SetPhysBody(nullptr);
		m_PhysicsEntity->_setPhysicsCylinder(radius, width, mass, mState.transform);
	}

	void ObjectMesh::SetPhysicsCapsule(TimeDelta radius, TimeDelta height, TimeDelta mass) {
		//MT::ScopedGuard guard(mutex);

		ASSERT(m_PhysicsEntity, "Invalid pointer on m_PhysicsEntity");
		if (!m_PhysicsEntity) return;

		m_PhysicsEntity->SetPhysBody(nullptr);
		m_PhysicsEntity->_setPhysicsCapsule(radius, height, mass, mState.transform);
	}

	void ObjectMesh::_SetPhysicsConvexHullImpl(TimeDelta mass) {
		//MT::ScopedGuard guard(mutex);

		ASSERT(m_PhysicsEntity, "Invalid pointer on m_PhysicsEntity");
		if (!m_PhysicsEntity) return;

		m_PhysicsEntity->SetPhysBody(nullptr);
		Util_SetPhysicsConvexHull(tempdata.mesh.m_StaticMesh, mass, this, "ObjectMesh");
	}

	void ObjectMesh::_SetPhysicsStaticMeshImpl() {
		//MT::ScopedGuard guard(mutex);

		ASSERT(m_PhysicsEntity, "Invalid pointer on m_PhysicsEntity");
		if (!m_PhysicsEntity) return;

#if !DISABLE_PHYSICS
		m_PhysicsEntity->SetPhysBody(nullptr);

		size_t numIndices = 0;
		size_t numVertices = 0;
		for (size_t i = 0; i < tempdata.mesh.m_StaticMesh->GetNumSubsets(); i++) {
			numIndices += tempdata.mesh.m_StaticMesh->subsets[i]->numIndices;
			numVertices += tempdata.mesh.m_StaticMesh->subsets[i]->numVertices;
		}

		auto size = tempdata.mesh.m_StaticMesh->GetNumSubsets();

		// ����� ����������� ����� size<=0
		ASSERT(size >= 0 && "[ObjectMesh::SetPhysicsStaticMesh] Incorrect NumSubsets.");

		if (size <= 0) {
			Error("[ObjectMesh::SetPhysicsStaticMesh] Incorrect NumSubsets.", false);
			return;
		}

		PhysBody* pb = new PhysBody[size]();

		// ����������� ����, ��� �� ������������ PhysX Cooking
		auto _mLoadingPath = this->GetLoadingPath();
		{
			if (m_PhysicsEntity->SaveAsMeshNameEmptyPhysX())
				m_PhysicsEntity->SetSaveAsMeshNamePhysX(_mLoadingPath);

			if (m_PhysicsEntity->OriginalMeshNameEmptyPhysX())
				m_PhysicsEntity->SetOriginalMeshNamePhysX(_mLoadingPath);
		}

		for (size_t i = 0; i < size; i++)
		{
			// ��������� ����� size < i
			ASSERT(size > i, "Size: %i, req: %i", size, i);
			String subsetItName = " " + std::to_string(i);

			pb[i] = *PhysBody::CreateStaticMesh
			(
				tempdata.mesh.m_StaticMesh->subsets[i]->numVertices,
				tempdata.mesh.m_StaticMesh->subsets[i]->numIndices,
				mState.transform,
				tempdata.mesh.m_StaticMesh->subsets[i]->vertices,
				tempdata.mesh.m_StaticMesh->subsets[i]->indices,
				PhysBodyDescSh::DEFAULT_DENSITY, // todo: implement this

				m_PhysicsEntity->GetSaveAsMeshNamePhysX() + subsetItName,
				m_PhysicsEntity->GetOriginalMeshNamePhysX() + subsetItName
			);
		}

		m_PhysicsEntity->SetPhysBody(pb);
		m_PhysicsEntity->SetMultipart(true);
#endif
	}

	void ObjectMesh::_AddToSceneList() {
		//MT::ScopedGuard guard(mutex);
		auto scenePtr = GetScene();
		if (scenePtr)
			scenePtr->AddMesh(this);
		else
			ASSERT(scenePtr, "Invalid pointer");

		UpdatableObject::_AddToSceneList();
	}

	bool ObjectMesh::RenderQuery(I_RenderLowLevel* _render, size_t _index, TimeDelta _delta)
	{
		//MT::ScopedGuard guard(mutex);
		ASSERT(_render, "Invalid _render pointer");

		if (!_render) return false;

		if (!this->UseQuery()) {
			this->RenderFast(_render, _index, _delta);
			return false;
		}
		if (this->HasQuery() == 0) {
			return false;
		}

		if (!tempdata.query)
			tempdata.query = _render->GetOQ();

		_render->DepthMask(false);
		_render->ColorMask(false, false, false, false);

		tempdata.query->BeginQuery();
		{
			// TODO:RENDER bounding box
			this->RenderFast(_render, _index, _delta);
		}
		tempdata.query->EndQuery();

		_render->ColorMask(true, true, true, true);
		_render->DepthMask(true);

		tempdata.query->BeginConditionalRender();
		{
			this->RenderFast(_render, _index, _delta);
		}
		tempdata.query->EndConditionalRender();

		return true;
	}

	bool ObjectMesh::RenderFast(I_RenderLowLevel* _render, size_t _index, TimeDelta _delta) {
		ASSERT(_render, "Invalid _render pointer on render");
		if (!_render) return false;

		//MT::ScopedGuard guard(mutex);

		this->DrawSubset(_index, _delta);

		return true;
	}

	const Vec3& ObjectMesh::GetMax() {
		//MT::ScopedGuard guard(mutex);
		ASSERT(tempdata.mesh.m_StaticMesh, "Invalid pointer");
		if (!tempdata.mesh.m_StaticMesh)
			return Vec3::ZERO;

		this->tempdata.Max = tempdata.mesh.m_StaticMesh->bBox.maxes;
		return this->tempdata.Max;
	}

	const Vec3& ObjectMesh::GetMin() {
		//MT::ScopedGuard guard(mutex);
		ASSERT(tempdata.mesh.m_StaticMesh, "Invalid pointer");
		if (!tempdata.mesh.m_StaticMesh)
			return Vec3::ZERO;

		this->tempdata.Min = tempdata.mesh.m_StaticMesh->bBox.mins;
		return this->tempdata.Min;
	}

	const BBox& ObjectMesh::GetBBox() {
		//MT::ScopedGuard guard(mutex);
		ASSERT(tempdata.mesh.m_StaticMesh, "Invalid pointer");

		this->tempdata.Box = tempdata.mesh.m_StaticMesh->bBox;
		return this->tempdata.Box;
	}

	const BBox& ObjectMesh::GetBBox(unsigned int subset) {
		//MT::ScopedGuard guard(mutex);
		ASSERT(tempdata.mesh.m_StaticMesh, "Invalid pointer");
		ASSERT(tempdata.mesh.m_StaticMesh->subsets, "Invalid pointer");
		ASSERT(tempdata.mesh.m_StaticMesh->subsets[subset], "Invalid pointer"); //-V108

		this->tempdata.Box = tempdata.mesh.m_StaticMesh->subsets[subset]->bBox; //-V108
		return this->tempdata.Box;
	}

	const Vec3& ObjectMesh::GetCenter() {
		//MT::ScopedGuard guard(mutex);
		ASSERT(tempdata.mesh.m_StaticMesh, "Invalid pointer");
		this->tempdata.Center = tempdata.mesh.m_StaticMesh->bSphere.center;
		return this->tempdata.Center;
	}

	float ObjectMesh::GetRadius() {
		//MT::ScopedGuard guard(mutex);
		ASSERT(tempdata.mesh.m_StaticMesh, "Invalid pointer");
		this->tempdata.Radius = tempdata.mesh.m_StaticMesh->bSphere.radius;
		return this->tempdata.Radius;
	}

	const Vec3&	ObjectMesh::GetMax(unsigned int s) {
		//MT::ScopedGuard guard(mutex);
		ASSERT(tempdata.mesh.m_StaticMesh, "Invalid pointer");
		ASSERT(tempdata.mesh.m_StaticMesh->subsets, "Invalid pointer");
		ASSERT(tempdata.mesh.m_StaticMesh->subsets[s], "Invalid pointer"); //-V108

		this->tempdata.Max = tempdata.mesh.m_StaticMesh->GetMax(s);
		return this->tempdata.Max;
	}

	const Vec3&	ObjectMesh::GetMin(unsigned int s) {
		//MT::ScopedGuard guard(mutex);
		ASSERT(tempdata.mesh.m_StaticMesh, "Invalid pointer");
		ASSERT(tempdata.mesh.m_StaticMesh->subsets, "Invalid pointer");
		ASSERT(tempdata.mesh.m_StaticMesh->subsets[s], "Invalid pointer"); //-V108

		this->tempdata.Min = tempdata.mesh.m_StaticMesh->GetMin(s);
		return this->tempdata.Min;
	}

	const Vec3&	ObjectMesh::GetCenter(unsigned int s) {
		//MT::ScopedGuard guard(mutex);
		ASSERT(tempdata.mesh.m_StaticMesh, "Invalid pointer");
		ASSERT(tempdata.mesh.m_StaticMesh->subsets, "Invalid pointer");

		this->tempdata.Center = tempdata.mesh.m_StaticMesh->GetCenter(s);
		return this->tempdata.Center;
	}

	float ObjectMesh::GetRadius(unsigned int s) {
		//MT::ScopedGuard guard(mutex);
		ASSERT(tempdata.mesh.m_StaticMesh, "Invalid pointer");
		ASSERT(tempdata.mesh.m_StaticMesh->subsets, "Invalid pointer");
		ASSERT(tempdata.mesh.m_StaticMesh->subsets[s], "Invalid pointer"); //-V108

		this->tempdata.Radius = tempdata.mesh.m_StaticMesh->GetRadius(s);
		return this->tempdata.Radius;
	}

	const String& ObjectMesh::GetSubsetName(unsigned int id) {
		//MT::ScopedGuard guard(mutex);
		ASSERT(tempdata.mesh.m_StaticMesh, "Invalid pointer");
		ASSERT(tempdata.mesh.m_StaticMesh->subsets, "Invalid pointer");

		this->tempdata.SubsetName = tempdata.mesh.m_StaticMesh->GetSubsetName(id);
		return this->tempdata.SubsetName;
	}

	const BSphere& ObjectMesh::GetBSphere() {
		//MT::ScopedGuard guard(mutex);
		ASSERT(tempdata.mesh.m_StaticMesh, "Invalid pointer");

		this->tempdata.Sphere = tempdata.mesh.m_StaticMesh->bSphere;
		return this->tempdata.Sphere;
	}

	const BSphere& ObjectMesh::GetBSphere(unsigned int s) {
		//MT::ScopedGuard guard(mutex);
		ASSERT(tempdata.mesh.m_StaticMesh, "Invalid pointer");
		ASSERT(tempdata.mesh.m_StaticMesh->subsets, "Invalid pointer");
		ASSERT(tempdata.mesh.m_StaticMesh->subsets[s], "Invalid pointer"); //-V108

		this->tempdata.Sphere = tempdata.mesh.m_StaticMesh->subsets[s]->bSphere; //-V108
		return this->tempdata.Sphere;
	}

	void ObjectMesh::_SetPhysicsMaterial(Material* _mat)
	{
		ASSERT(_mat, "Provided invalid pointer on _mat");
		if (_mat == nullptr)
			return;

		_SetPhysicsMaterial(_mat->_GetPhysXMaterialShPtr());
	}

	void ObjectMesh::_SetPhysicsMaterial(PhysXMaterialSh*_phmat)
	{
		ASSERT(m_PhysicsEntity, "Invalid pointer on m_PhysicsEntity");
		if (!m_PhysicsEntity) return;

		auto phMat = _phmat;

		if (phMat == nullptr)
			phMat = phMatManager.GetDefaultMaterial();

		ASSERT(phMat, "Invalid pointer on _phMat and invalid pointer on default physx material");
		if (!phMat) return;

		if (m_PhysicsEntity->IsValidPhysXBody() == false)
			return;

		auto pb = m_PhysicsEntity->GetPhysBody();

		auto desc = pb->GetDescForChange();
		desc.m_AttachedPhysicsMaterial = _phmat;

		pb->RecreateIfNeeded(desc);
	}

#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
	void ObjectMesh::_InitEditorVars()
	{
		BaseEntity::_InitEditorVars();
		BaseVisualObjectEntity::AddEditorVarsTo(this->GetEditorVars());
	}

	void ObjectMesh::EditorCheckWhatWasModificated()
	{
		/*MT::ScopedGuard guard(mutex);*/
		/**/
		// !@todo it's maybe deprecated
		//auto oldScale = mState.transform.GetScale();
		//if (oldScale != mState.m_Scale)
		//	Scale(mState.m_Scale);

		if (tempdata.materials.size() == tempdata.OldMaterials.size())
		{
			for (size_t i = 0; i < tempdata.materials.size(); i++)
			{
				auto ptr = tempdata.materials[i];
				auto ptr2 = tempdata.OldMaterials[i];

				if (tempdata.materials.size() != tempdata.OldMaterials.size()
					|| ptr.material != ptr2.material
					|| ptr.materialName != ptr2.materialName
					|| ptr.subsetName != ptr2.subsetName)
				{
					tempdata.OldMaterials = tempdata.materials;
					//DebugM("�� ���������� ptr1: %s ptr2: %s", ptr.materialName.c_str(), ptr2.materialName.c_str());
					SetMaterial(ptr.materialName, ptr.subsetName);
					return;
				}
			}
		}

		tempdata.OldMaterials = tempdata.materials;

		for (const auto &material : tempdata.materials)
		{
			auto subset = material;
			auto ptr = subset.material;
			if (ptr == nullptr)
			{
				Error("Invalid ptr", false);
				continue;
			}

			//DebugM("%s - %s", ptr.material->GetPath(), ptr.subsetName);
			SetMaterial(ptr->GetPath(), subset.subsetName);
		}

		// ��������������� ������ �������, �.� �� �������� - Simulate, ��������� ������, �������� ��� ��� - ������� ������ �������
		// ��� �� ���� ������� ��� ��������� ������ ���������, � ���������(���� �� ��������� ��� �������� � �� ����������� mstate
		// �����, �� � ��� ��������, ��� � ��������� ������ ����� �������� ������� �������
		if (mSavedState != mState) {
			SetTransform(mState.transform);
			//Scale(mState.transform.GetScale());
			SetNewState(mState);
		}
		else {
			SetTransform(mSavedState.transform);
			//Scale(mState.transform.GetScale());
			SetNewState(mSavedState);
		}
	}

	void ObjectMesh::_AddEditorVars(void) {}
	void ObjectMesh::AddEditorVarsTo(std::vector<EditorVar>&_vars) {}
	void ObjectMesh::ReInitEditorVars(void) {}

#endif
}