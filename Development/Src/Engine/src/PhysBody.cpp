/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//***************************************************************************
#include "EnginePrivate.h"
//***************************************************************************
#include "PhysSystem.h"
#include "PhysBody.h"
#include "Log.h"
//***************************************************************************
#include "PxPhysicsAPI.h"
//***************************************************************************
#include "EngineMathToPhysx.inl"
#include "PhysXUtils.h"
#include "PhysXSystemBodys.h"
#include "PhysXMaterialSh.h"
#include "PhysicsMaterialsManager.h"
//***************************************************************************

namespace NGTech {
	//***************************************************************************
	void PhysX_DeleteActor(physx::PxRigidActor* _actor);
	//***************************************************************************
	void PhysBodyDescSh::CleanResources() {
		if (mActorTmp)
			PhysX_DeleteActor(mActorTmp);
		//SAFE_DELETE(impactSrc); //-V809
	}
	//***************************************************************************
	const TimeDelta PhysBodyDescSh::DEFAULT_DENSITY(2000.0f);
	//***************************************************************************
	using namespace physx;
	
	PhysBody* PhysBody::CreateBox(const Vec3 &size, const Mat4 &_trans, TimeDelta _mass, TimeDelta _density, PhysXMaterialSh* _phMat) {
		PhysBodyDescSh desc;
		desc.CollisionMass = _mass;
		desc.CollisionSize = size;
		desc.CollisionDensity = _density;
		desc.TransformTmp = _trans;
		desc.mCollisionType = PhysBodyDescSh::CollisionType::CT_BOX;
		desc.m_AttachedPhysicsMaterial = _phMat;

		PhysBody *body = new PhysBody(desc);
		body->_CreateFromDesc(desc);

		return body;
	}

	PhysBody* PhysBody::CreateSphere(TimeDelta radius, const Mat4 &_trans, TimeDelta _mass, TimeDelta _density, PhysXMaterialSh* _phMat) {
		PhysBodyDescSh desc;
		desc.CollisionMass = _mass;
		desc.CollisionRadius = radius;
		desc.CollisionDensity = _density;
		desc.TransformTmp = _trans;
		desc.mCollisionType = PhysBodyDescSh::CollisionType::CT_SPHERE;
		desc.m_AttachedPhysicsMaterial = _phMat;

		PhysBody *body = new PhysBody(desc);
		body->_CreateFromDesc(desc);

		return body;
	}

	//NOTE:����� �������� �����,��� �� � �������� ���� ������ �� ���� � ��������� ��������
	PhysBody* PhysBody::CreateCylinderConvex(TimeDelta radius, TimeDelta width, const Mat4 &_trans, TimeDelta _mass, TimeDelta _density, PhysXMaterialSh* _phMat)
	{
		PhysBodyDescSh desc;
		desc.CollisionMass = _mass;
		desc.CollisionRadius = radius;
		desc.CollisionWidth = width;
		desc.CollisionDensity = _density;
		desc.TransformTmp = _trans;
		desc.mCollisionType = PhysBodyDescSh::CollisionType::CT_CYLINDER;
		desc.m_AttachedPhysicsMaterial = _phMat;

		PhysBody *body = new PhysBody(desc);
		body->_CreateFromDesc(desc);

		return body;
	}

	PhysBody* PhysBody::CreateCapsule(TimeDelta radius, TimeDelta height, const Mat4 &_trans, TimeDelta _mass, TimeDelta _density, PhysXMaterialSh* _phMat)
	{
		PhysBodyDescSh desc;
		desc.CollisionMass = _mass;
		desc.CollisionRadius = radius;
		desc.CollisionHeight = height;
		desc.CollisionDensity = _density;
		desc.TransformTmp = _trans;
		desc.mCollisionType = PhysBodyDescSh::CollisionType::CT_CAPSULE;
		desc.m_AttachedPhysicsMaterial = _phMat;

		PhysBody *body = new PhysBody(desc);
		body->_CreateFromDesc(desc);

		return body;
	}

	PhysBody* PhysBody::CreateConvexHull(unsigned int _numVert, unsigned int _numFaces, const Mat4 &_trans, void*_vertices, unsigned int*_indices, TimeDelta _mass,
		TimeDelta _density,
		const String& _save_as,
		const String& originalName,
		PhysXMaterialSh* _phMat)
	{
		PhysBodyDescSh desc;
		desc.CollisionMass = _mass;
		desc.CollisionDensity = _density;
		desc.TransformTmp = _trans;
		desc.NumVertTmp = _numVert;
		desc.NumFacesTmp = _numFaces;
		desc.VerticesArrTmp = _vertices;
		desc.IndicesArrTmp = _indices;
		desc.mCollisionType = PhysBodyDescSh::CollisionType::CT_CONVEX;
		desc.m_AttachedPhysicsMaterial = _phMat;

		PhysBody *body = new PhysBody(desc);
		body->_CreateFromDesc(desc);

		return body;
	}

	PhysBody* PhysBody::CreateStaticMesh(unsigned int _numVert, unsigned int _numFaces, const Mat4 &_trans, void*_vertices, unsigned int*_indices, TimeDelta _density,
		const String& _save_as,
		const String& originalName,
		PhysXMaterialSh* _phMat)
	{
		PhysBodyDescSh desc;
		desc.CollisionDensity = _density;
		desc.TransformTmp = _trans;
		desc.NumVertTmp = _numVert;
		desc.NumFacesTmp = _numFaces;
		desc.VerticesArrTmp = _vertices;
		desc.IndicesArrTmp = _indices;
		desc.mCollisionType = PhysBodyDescSh::CollisionType::CT_STATIC;
		desc.m_AttachedPhysicsMaterial = _phMat;

		PhysBody *body = new PhysBody(desc);
		body->_CreateFromDesc(desc);

		return body;
	}

	PhysBody::~PhysBody() {
		//Debug(__FUNCTION__);
		_Cleanup();
	}

	/**
	*/
	void PhysBody::SetTransform(const Mat4 &_transform)
	{
		if (mDesc.TransformTmp == _transform)
			return;

		mDesc.TransformTmp = _transform;
		ASSERT(this->mDesc.mActorTmp && this->mDesc.mActorTmp->getScene(), "Not exist mActor, or not exist actorScene");

		if (this->mDesc.mActorTmp && this->mDesc.mActorTmp->getScene())
		{
			physx::PxSceneWriteLock lock(*this->mDesc.mActorTmp->getScene(), __FUNCTION__, __LINE__);
			this->mDesc.mActorTmp->setGlobalPose(EngineMathToPhysX(_transform));
		}
		else
			Debug("PhysBody::setTransform:-mActor is not exist");
	}

	void PhysBody::ComputeTransformFromPhysX()
	{
		ASSERT(this->mDesc.mActorTmp && this->mDesc.mActorTmp->getScene(), "Not exist mActor, or not exist actorScene");
		if (this->mDesc.mActorTmp && this->mDesc.mActorTmp->getScene())
		{
			physx::PxSceneReadLock lock(*this->mDesc.mActorTmp->getScene(), __FUNCTION__, __LINE__);
			auto pos = this->mDesc.mActorTmp->getGlobalPose();
			mDesc.TransformTmp = PhysXToEngineMath(pos);
		}
	}

	const Mat4 PhysBody::GetTransform() {
		ComputeTransformFromPhysX();
		return mDesc.TransformTmp;
	}

	const Mat4& PhysBody::GetTransformForChange() {
		ComputeTransformFromPhysX();
		return mDesc.TransformTmp;
	}

	/**
	*/
	void PhysBody::AddTorque(const Vec3 &_torque) {
		if (mDesc.AppliedTorque == _torque)
			return;

		mDesc.AppliedTorque = _torque;

		auto ph = GetPhysics();

		ASSERT(this->mDesc.mActorTmp && ph && this->mDesc.mActorTmp->getScene(), "Not exist mActor, or not exist actorScene");
		if (this->mDesc.mActorTmp && ph && this->mDesc.mActorTmp->getScene())
		{
			auto cA = this->mDesc.mActorTmp->is<PxRigidBody>();
			if (cA)
			{
				physx::PxSceneWriteLock lock(*this->mDesc.mActorTmp->getScene(), __FUNCTION__, __LINE__);
				cA->addTorque(PxVec3(_torque.x, _torque.y, _torque.z));
			}
		}
	}

	/**
	*/
	void PhysBody::AddForce(const Vec3 &force) {
		if (mDesc.AppliedForce == force)
			return;

		mDesc.AppliedForce = force;

		ASSERT(this->mDesc.mActorTmp && this->mDesc.mActorTmp->getScene(), "Not exist mActor, or not exist actorScene");
		if (this->mDesc.mActorTmp && this->mDesc.mActorTmp->getScene())
		{
			auto cA = this->mDesc.mActorTmp->is<PxRigidBody>();
			if (cA)
			{
				physx::PxSceneWriteLock lock(*this->mDesc.mActorTmp->getScene(), __FUNCTION__, __LINE__);
				cA->addForce(PxVec3(force.x, force.y, force.z));
			}
		}
	}

	/**
	*/
	void PhysBody::SetLinearVelocity(const Vec3 &velocity) {
		if (mDesc.LinearVelocity == velocity)
			return;

		mDesc.LinearVelocity = velocity;

		ASSERT(this->mDesc.mActorTmp, "Not exist mActor, or not exist actorScene");
		auto _scene = this->mDesc.mActorTmp->getScene(); //-V595

		ASSERT(_scene, "Not exist actorScene");
		if (this->mDesc.mActorTmp && _scene)
		{
			//Debug("[PhysBody::SetLinearVelocity] mActor->isRigidBody()");
			auto cA = this->mDesc.mActorTmp->is<PxRigidBody>();
			if (cA)
			{
				physx::PxSceneWriteLock lock(*_scene, __FUNCTION__, __LINE__);
				cA->setLinearVelocity(
					EngineMathToPhysX(mDesc.LinearVelocity)
				);
			}
		}
	}

	/**
	*/
	const Vec3 PhysBody::ComputeAngularVelicity() {
		ASSERT(this->mDesc.mActorTmp && this->mDesc.mActorTmp->getScene(), "Not exist mActorTmp, or not exist actorScene");
		if (this->mDesc.mActorTmp && this->mDesc.mActorTmp->getScene())
		{
			auto cA = this->mDesc.mActorTmp->is<PxRigidBody>();
			if (cA)
			{
				physx::PxSceneReadLock lock(*this->mDesc.mActorTmp->getScene(), __FUNCTION__, __LINE__);
				auto mPxVec = cA->getAngularVelocity();
				Vec3  _vec = { mPxVec.x, mPxVec.y, mPxVec.z };
				if (mDesc.AngularVelocity != _vec)
					mDesc.AngularVelocity = _vec;
			}
			else
				Warning("Failed casting");
		}
		return mDesc.AngularVelocity;
	}

	const Vec3 PhysBody::GetAngularVelocity() const {
		return mDesc.AngularVelocity;
	}

	/**
	*/
	void PhysBody::SetAngularVelocity(const Vec3 &velocity) {
		if (mDesc.AngularVelocity == velocity)
			return;

		mDesc.AngularVelocity = velocity;

		ASSERT(this->mDesc.mActorTmp && this->mDesc.mActorTmp->getScene() && "Not exist mActorTmp, or not exist actorScene");
		if (this->mDesc.mActorTmp && this->mDesc.mActorTmp->getScene())
		{
			//Debug("[PhysBody::SetAngularVelocity] mActorTmp->isRigidBody()");
			auto cA = this->mDesc.mActorTmp->is<PxRigidBody>();
			if (cA)
			{
				physx::PxSceneWriteLock lock(*this->mDesc.mActorTmp->getScene(), __FUNCTION__, __LINE__);
				cA->setAngularVelocity(
					EngineMathToPhysX(mDesc.AngularVelocity)
				);
			}
		}
	}

	/**
	*/
	const Vec3 PhysBody::ComputeLinearVelicity() {
		ASSERT(this->mDesc.mActorTmp && this->mDesc.mActorTmp->getScene(), "Not exist mActorTmp, or not exist actorScene");
		if (this->mDesc.mActorTmp && this->mDesc.mActorTmp->getScene())
		{
			auto cA = this->mDesc.mActorTmp->is<PxRigidBody>();
			ASSERT(cA, "Failed casting");
			physx::PxSceneReadLock lock(*this->mDesc.mActorTmp->getScene(), __FUNCTION__, __LINE__);
			auto mPxVec = cA->getLinearVelocity();
			Vec3 _vec = { mPxVec.x, mPxVec.y, mPxVec.z };
			if (this->mDesc.LinearVelocity != _vec)
				mDesc.LinearVelocity = _vec;
		}
		return mDesc.LinearVelocity;
	}

	const Vec3 PhysBody::GetLinearVelocity() const {
		return mDesc.LinearVelocity;
	}

	/**
	*/
	TimeDelta PhysBody::GetMass() {
		ASSERT(this->mDesc.mActorTmp && this->mDesc.mActorTmp->getScene(), "Not exist mActorTmp, or not exist actorScene");
		if (this->mDesc.mActorTmp && this->mDesc.mActorTmp->getScene())
		{
			auto cA = this->mDesc.mActorTmp->is<PxRigidBody>();
			if (cA)
			{
				physx::PxSceneReadLock lock(*this->mDesc.mActorTmp->getScene(), __FUNCTION__, __LINE__);
				auto nmass = cA->getMass();
				if (this->mDesc.CollisionMass != nmass)
					this->mDesc.CollisionMass = nmass;
			}
		}
		return mDesc.CollisionMass;
	}

	/**
	*/
	void PhysBody::SetMass(TimeDelta _mass) {
		if (Math::IsEqual(mDesc.CollisionMass, _mass))
			return;

		this->mDesc.CollisionMass = _mass;

		ASSERT(this->mDesc.mActorTmp && this->mDesc.mActorTmp->getScene(), "Not exist mActorTmp, or not exist actorScene");
		if (this->mDesc.mActorTmp && this->mDesc.mActorTmp->getScene())
		{
			auto cA = this->mDesc.mActorTmp->is<PxRigidBody>();
			if (cA)
			{
				physx::PxSceneWriteLock lock(*this->mDesc.mActorTmp->getScene(), __FUNCTION__, __LINE__);
				cA->setMass(mDesc.CollisionMass);
			}
		}
	}

	/**
	*/
	void PhysBody::SetLinearDamping(TimeDelta _v) {
		if (Math::IsEqual(mDesc.LinearDamping, _v))
			return;

		mDesc.LinearDamping = _v;

		ASSERT(this->mDesc.mActorTmp && this->mDesc.mActorTmp->getScene(), "Not exist mActorTmp, or not exist actorScene");
		if (this->mDesc.mActorTmp && this->mDesc.mActorTmp->getScene())
		{
			PxRigidDynamic* mNActor = this->mDesc.mActorTmp->is<PxRigidDynamic>();
			if (mNActor)
			{
				physx::PxSceneWriteLock lock(*this->mDesc.mActorTmp->getScene(), __FUNCTION__, __LINE__);
				mNActor->setLinearDamping(_v);
			}
		}
	}

	/**
	*/
	void PhysBody::SetAngularDamping(TimeDelta _v) {
		if (Math::IsEqual(mDesc.AngularDamping, _v))
			return;

		mDesc.AngularDamping = _v;

		ASSERT(this->mDesc.mActorTmp && this->mDesc.mActorTmp->getScene(), "Not exist mActorTmp, or not exist actorScene");
		if (this->mDesc.mActorTmp && this->mDesc.mActorTmp->getScene())
		{
			PxRigidDynamic* mNActor = this->mDesc.mActorTmp->is<PxRigidDynamic>();
			if (mNActor)
			{
				physx::PxSceneWriteLock lock(*this->mDesc.mActorTmp->getScene(), __FUNCTION__, __LINE__);
				mNActor->setAngularDamping(_v);
			}
		}
	}

	/**
	*/
	void PhysBody::SetMassSpaceInertiaTensor(const Vec3&_vec) {
		if (mDesc.MassSpaceInertiaTensor == _vec)
			return;

		mDesc.MassSpaceInertiaTensor = _vec;

		ASSERT(this->mDesc.mActorTmp && this->mDesc.mActorTmp->getScene(), "Not exist mActorTmp, or not exist actorScene");
		if (this->mDesc.mActorTmp && this->mDesc.mActorTmp->getScene())
		{
			PxRigidDynamic* mNActor = this->mDesc.mActorTmp->is<PxRigidDynamic>();
			if (mNActor)
			{
				physx::PxSceneWriteLock lock(*this->mDesc.mActorTmp->getScene(), __FUNCTION__, __LINE__);
				mNActor->setMassSpaceInertiaTensor(PxVec3(_vec.x, _vec.y, _vec.z));
			}
		}
	}

	void PhysBody::_CreateFromDesc(const PhysBodyDescSh&_desc) {
		if (mDesc == _desc)
			return;

		mDesc = _desc;

		switch (_desc.mCollisionType)
		{
		case PhysBodyDescSh::CollisionType::CT_NONE:
			_SetPhysicsNone();
			break;

		case PhysBodyDescSh::CollisionType::CT_BOX:
			_CreatePhysicsBox();
			break;

		case PhysBodyDescSh::CollisionType::CT_SPHERE:
			_CreatePhysicsSphere();
			break;

		case PhysBodyDescSh::CollisionType::CT_CYLINDER:
			_CreatePhysicsCylinder();
			break;

		case PhysBodyDescSh::CollisionType::CT_CAPSULE:
			_CreatePhysicsCapsule();
			break;

		case PhysBodyDescSh::CollisionType::CT_CONVEX:
			_CreatePhysicsConvex();
			break;

		case PhysBodyDescSh::CollisionType::CT_STATIC:
			_CreatePhysicsStaticMesh();
			break;
		default:
			ErrorWrite("[ObjectMesh::SetPhysicsByDesc] Invalid type. Collision not applied");
		}
	}

	void PhysBody::_SetPhysicsNone() {
		_Cleanup();
	}

	void PhysBody::_CreatePhysicsBox() {
		auto ph = GetPhysics();

		if (!ph) {
			Error(__FUNCTION__, true);
			return;
		}

		// Initialize Cube Actor
		this->mDesc.mActorTmp = PhysXSystemBodys::CreateBox(mDesc.TransformTmp, mDesc.CollisionSize, mDesc.CollisionMass, mDesc.CollisionDensity, *ph->GetPxCooking(), *ph->GetPxPhysics(), *_GetPhysicsMaterial()->GetPxMaterial(), *ph->GetActivePxScene());
		ASSERT(this->mDesc.mActorTmp, "create actor failed!");

		SetAngularDamping(0.5f);
	}

	void PhysBody::_CreatePhysicsSphere() {
		auto phModule = GetPhysics();

		ASSERT(phModule, "Not exist phModule");

		if (!phModule) {
			Error(__FUNCTION__, true);
			return;
		}

		// Initialize Sphere Actor
		this->mDesc.mActorTmp = PhysXSystemBodys::CreateSphere(mDesc.TransformTmp, mDesc.CollisionRadius, mDesc.CollisionMass, mDesc.CollisionDensity, *phModule->GetPxCooking(), *phModule->GetPxPhysics(), *_GetPhysicsMaterial()->GetPxMaterial(), *phModule->GetActivePxScene());
		ASSERT(this->mDesc.mActorTmp, "create actor failed!");

		SetAngularDamping(0.5f);
		SetLinearDamping(Math::ONEFLOAT);
	}

	void PhysBody::_CreatePhysicsCylinder() {
		auto phModule = GetPhysics();

		ASSERT(phModule, "Not exist phModule");

		if (!phModule) {
			return;
		}

		// Initialize Cube Actor
		this->mDesc.mActorTmp = PhysXSystemBodys::CreateCylinderConvex(mDesc.TransformTmp, mDesc.CollisionRadius, mDesc.CollisionWidth, mDesc.CollisionMass, mDesc.CollisionDensity, *phModule->GetPxCooking(), *phModule->GetPxPhysics(), *_GetPhysicsMaterial()->GetPxMaterial(), *phModule->GetActivePxScene());
		ASSERT(this->mDesc.mActorTmp, "create actor failed!");

		SetAngularDamping(0.5f);
	}

	void PhysBody::_CreatePhysicsCapsule() {
		auto phModule = GetPhysics();
		ASSERT(phModule, "Not exist phModule");

		if (phModule == nullptr) {
			return;
		}

		// Initialize Cube Actor
		this->mDesc.mActorTmp = PhysXSystemBodys::CreateCapsule(mDesc.CollisionRadius, mDesc.CollisionHeight, mDesc.TransformTmp, mDesc.CollisionMass, mDesc.CollisionDensity, *phModule->GetPxCooking(), *phModule->GetPxPhysics(), *_GetPhysicsMaterial()->GetPxMaterial(), *phModule->GetActivePxScene());
		ASSERT(this->mDesc.mActorTmp, "create actor failed!");

		SetAngularDamping(0.5f);
	}

	void PhysBody::_CreatePhysicsConvex() {
		auto phModule = GetPhysics();
		ASSERT(phModule, "Not exist phModule");

		if (phModule == nullptr) {
			return;
		}

		//!@todo �������� ����������� ��������� ��������� ���������� �����
		this->mDesc.mActorTmp = PhysXSystemBodys::CreateConvexHull(mDesc.NumVertTmp, mDesc.NumFacesTmp, mDesc.TransformTmp, mDesc.VerticesArrTmp, mDesc.CollisionMass, mDesc.CollisionDensity, phModule->GetPxCooking(), phModule->GetPxPhysics(), _GetPhysicsMaterial()->GetPxMaterial(), phModule->GetActivePxScene());
		ASSERT(this->mDesc.mActorTmp, "create actor failed!");

		SetAngularDamping(0.5f);
	}

	void PhysBody::_CreatePhysicsStaticMesh()
	{
		auto phModule = GetPhysics();
		ASSERT(phModule, "Not exist phModule");
		if (phModule == nullptr) {
			return;
		}

		//!@todo �������� ����������� ��������� ��������� ���������� �����
		this->mDesc.mActorTmp = PhysXSystemBodys::CreateStaticMesh(mDesc.NumVertTmp, mDesc.NumFacesTmp, mDesc.TransformTmp, mDesc.VerticesArrTmp, mDesc.IndicesArrTmp, mDesc.CollisionDensity, phModule->GetPxCooking(), phModule->GetPxPhysics(), _GetPhysicsMaterial()->GetPxMaterial(), phModule->GetActivePxScene());
		ASSERT(this->mDesc.mActorTmp, "create actor failed!");
	}

	void PhysBody::_Cleanup()
	{
		this->mDesc.CleanResources();
	}

	PhysXMaterialSh* PhysBody::_GetPhysicsMaterial()
	{
		if ((mDesc.m_AttachedPhysicsMaterial == nullptr) || (mDesc.m_AttachedPhysicsMaterial && mDesc.m_AttachedPhysicsMaterial->GetPxMaterial() == nullptr))
			return phMatManager.GetDefaultMaterial();

		return mDesc.m_AttachedPhysicsMaterial;
	}
}