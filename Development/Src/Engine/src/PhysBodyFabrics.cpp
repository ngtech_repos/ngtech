/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
#if !DISABLE_PHYSICS
//***************************************************************************
#include "PhysSystem.h"
#include "PhysBody.h"
#include "Log.h"
//***************************************************************************
#include "PxPhysicsAPI.h"
//***************************************************************************
#include "EngineMathToPhysx.inl"
#include "PhysXUtils.h"
//***************************************************************************
#include "PhysXCharacterController.h"
#include "PhysXMaterialSh.h"
#include "PhysXSystemBodys.h"
#include "PhysXSerializer.h"
//***************************************************************************
#include "Physics/PhysicsStreams.h"
#include "PhysicsMaterialsManager.h"
//***************************************************************************

namespace NGTech {
	using namespace physx;

	template <class T>
	ENGINE_INLINE physx::PxConvexMesh* _CreateConvexMesh(const T* verts, const uint32_t numVerts,
		physx::PxPhysics& physics, physx::PxCooking& cooking)
	{
		ASSERT(verts, "Invalid poitner");

		// Create descriptor for convex mesh
		PxConvexMeshDesc convexDesc;
		convexDesc.points.count = numVerts;
		convexDesc.points.stride = sizeof(T);
		convexDesc.points.data = verts;
		convexDesc.flags = PxConvexFlag::eCOMPUTE_CONVEX;

		PxConvexMesh* convexMesh = nullptr;
		MyMemoryOutputStream buffer;
		if (cooking.cookConvexMesh(convexDesc, buffer))
		{
			MyMemoryInputData id(buffer.getData(), buffer.getSize());
			convexMesh = physics.createConvexMesh(id);
		}

		ASSERT(convexMesh, "[Physics] _CreateConvexMesh. Invalid pointer.");

		return convexMesh;
	}

	static ENGINE_INLINE physx::PxConvexMesh* _CreateCylinderConvexMesh100(const PxF32 width, const PxF32 radius, const PxU32 numCirclePoints, physx::PxPhysics& physics, physx::PxCooking& cooking)
	{
#ifndef MAX_NUM_VERTS_IN_CIRCLE
#define MAX_NUM_VERTS_IN_CIRCLE 100
#endif
		ASSERT(numCirclePoints < MAX_NUM_VERTS_IN_CIRCLE);
		PxVec3 verts[2 * MAX_NUM_VERTS_IN_CIRCLE];
		PxU32 numVerts = 2 * numCirclePoints;
		const PxF32 dtheta = 2 * PxPi / (Math::ONEDELTA * numCirclePoints);
		for (PxU32 i = 0; i < MAX_NUM_VERTS_IN_CIRCLE; i++)
		{
			const PxF32 theta = dtheta * i;
			const PxF32 cosTheta = radius * PxCos(theta);
			const PxF32 sinTheta = radius * PxSin(theta);
			verts[2 * i + 0] = PxVec3(-0.5f*width, cosTheta, sinTheta);
			verts[2 * i + 1] = PxVec3(+0.5f*width, cosTheta, sinTheta);
		}

#ifdef MAX_NUM_VERTS_IN_CIRCLE
#undef MAX_NUM_VERTS_IN_CIRCLE
#endif

		return _CreateConvexMesh(verts, numVerts, physics, cooking);
	}

	static ENGINE_INLINE physx::PxConvexMesh* _CreateCylinderConvexMesh16(const PxF32 width, const PxF32 radius, const PxU32 numCirclePoints, physx::PxPhysics& physics, physx::PxCooking& cooking)
	{
#ifndef MAX_NUM_VERTS_IN_CIRCLE
#define MAX_NUM_VERTS_IN_CIRCLE 16
#endif
		ASSERT(numCirclePoints < MAX_NUM_VERTS_IN_CIRCLE);
		PxVec3 verts[2 * MAX_NUM_VERTS_IN_CIRCLE];
		PxU32 numVerts = 2 * numCirclePoints;
		const PxF32 dtheta = 2 * PxPi / (Math::ONEDELTA * numCirclePoints);
		for (PxU32 i = 0; i < MAX_NUM_VERTS_IN_CIRCLE; i++)
		{
			const PxF32 theta = dtheta * i;
			const PxF32 cosTheta = radius * PxCos(theta);
			const PxF32 sinTheta = radius * PxSin(theta);
			verts[2 * i + 0] = PxVec3(-0.5f*width, cosTheta, sinTheta);
			verts[2 * i + 1] = PxVec3(+0.5f*width, cosTheta, sinTheta);
		}
#ifdef MAX_NUM_VERTS_IN_CIRCLE
#undef MAX_NUM_VERTS_IN_CIRCLE
#endif

		return _CreateConvexMesh(verts, numVerts, physics, cooking);
	}

	static ENGINE_INLINE PxConvexMesh* CreateWheelConvexMesh(const PxVec3* verts, const PxU32 numVerts, physx::PxPhysics& physics, physx::PxCooking& cooking)
	{
		//Extract the wheel radius and width from the aabb of the wheel convex mesh.
		PxVec3 wheelMin(PX_MAX_F32, PX_MAX_F32, PX_MAX_F32);
		PxVec3 wheelMax(-PX_MAX_F32, -PX_MAX_F32, -PX_MAX_F32);
		for (PxU32 i = 0; i < numVerts; i++)
		{
			wheelMin.x = PxMin(wheelMin.x, verts[i].x);
			wheelMin.y = PxMin(wheelMin.y, verts[i].y);
			wheelMin.z = PxMin(wheelMin.z, verts[i].z);
			wheelMax.x = PxMax(wheelMax.x, verts[i].x);
			wheelMax.y = PxMax(wheelMax.y, verts[i].y);
			wheelMax.z = PxMax(wheelMax.z, verts[i].z);
		}
		const PxF32 wheelWidth = wheelMax.x - wheelMin.x;
		const PxF32 wheelRadius = PxMax(wheelMax.y, wheelMax.z);

		return _CreateCylinderConvexMesh16(wheelWidth, wheelRadius, 8, physics, cooking);
	}

	//***************************************************************************
	physx::PxController*
		PhysSystem::CreateBoxCharacterController(PhysXCharacterController*_ch,
			const CharacterControllerDesc* _desc)
	{
		ASSERT(_desc, "Invalid _desc provided");
		ASSERT(this->m_ActivePhysScene.m_SceneData.m_PhysXScene, "Invalid this->m_ActivePhysScene.m_SceneData.m_PhysXScene");
		if (!this->m_ActivePhysScene.m_SceneData.m_PhysXScene || _desc == nullptr)
			return nullptr;

		physx::PxSceneWriteLock lock(*this->m_ActivePhysScene.m_SceneData.m_PhysXScene, __FUNCTION__, __LINE__);

		// Initialize Cube Actor
		PxBoxControllerDesc pdesc;
		pdesc.halfForwardExtent = _desc->radius;
		pdesc.halfSideExtent = _desc->radius;
		pdesc.halfHeight = _desc->height / 2;

		pdesc.material = phMatManager.GetDefaultMaterial()->GetPxMaterial();
		pdesc.density = _desc->density;

		pdesc.contactOffset = _desc->contactOffset;

		pdesc.stepOffset = _desc->stepOffset;

		pdesc.invisibleWallHeight = _desc->invisibleWallHeight;
		pdesc.maxJumpHeight = _desc->maxJumpHeight;
		pdesc.scaleCoeff = _desc->scaleCoeff;
		pdesc.slopeLimit = _desc->slopeLimit;
		pdesc.volumeGrowth = _desc->volumeGrowth;

		pdesc.position = EngineMathToPhysXDouble(_desc->position);
		pdesc.upDirection = EngineMathToPhysX(_desc->upDirection);

		if (pdesc.isValid() == false)
		{
			Warning("[PhysSystem::CreateBoxCharacterController] Invalid physics desc");
			return nullptr;
		}

		auto mController = this->m_ActivePhysScene.m_SceneData.m_ControllerManager->createController(pdesc);
		ASSERT(mController, "Failed creation mController");

		return mController;
	}

	physx::PxController*
		PhysSystem::CreateCapsuleCharacterController(PhysXCharacterController*_ch,
			const CharacterControllerDesc* _desc)
	{
		ASSERT(_desc, "Invalid _desc provided");

		ASSERT(this->m_ActivePhysScene.m_SceneData.m_PhysXScene, "Invalid this->m_ActivePhysScene.m_SceneData.m_PhysXScene");
		if (!this->m_ActivePhysScene.m_SceneData.m_PhysXScene || _desc == nullptr)
			return nullptr;

		physx::PxSceneWriteLock lock(*this->m_ActivePhysScene.m_SceneData.m_PhysXScene, __FUNCTION__, __LINE__);

		// Initialize Capsule Actor
		PxCapsuleControllerDesc pdesc;

		pdesc.height = _desc->height;
		pdesc.radius = _desc->radius;
		pdesc.density = _desc->density;

		pdesc.material = phMatManager.GetDefaultMaterial()->GetPxMaterial();

		pdesc.contactOffset = _desc->contactOffset;
		pdesc.stepOffset = _desc->stepOffset;

		pdesc.invisibleWallHeight = _desc->invisibleWallHeight;
		pdesc.maxJumpHeight = _desc->maxJumpHeight;
		pdesc.scaleCoeff = _desc->scaleCoeff;

		pdesc.climbingMode = PxCapsuleClimbingMode::eEASY;
		pdesc.slopeLimit = _desc->slopeLimit;
		pdesc.volumeGrowth = _desc->volumeGrowth;

		pdesc.position = EngineMathToPhysXDouble(_desc->position);
		pdesc.upDirection = EngineMathToPhysX(_desc->upDirection);

		if (pdesc.isValid() == false)
		{
			Warning("[PhysSystem::CreateCapsuleCharacterController] Invalid physics desc");
			return nullptr;
		}

		auto mController = this->m_ActivePhysScene.m_SceneData.m_ControllerManager->createController(pdesc);
		ASSERT(mController, "Failed creation mController");

		return mController;
	}
	//***************************************************************************

	physx::PxRigidDynamic* PhysXSystemBodys::CreateBox(const Mat4&_trans, const Vec3& _size, TimeDelta _mass, TimeDelta _density,
		physx::PxCooking& _pxCook,
		physx::PxPhysics& _pxPhysics,
		physx::PxMaterial& _pxMaterial,
		physx::PxScene& _pxScene)
	{
		// Initialize Cube Actor
		auto mActor = PxCreateDynamic(_pxPhysics, EngineMathToPhysX(_trans), PxBoxGeometry(_size.x / 2, _size.y / 2, _size.z / 2), _pxMaterial, _density);
		if (!mActor) {
			Warning("create actor failed!");
			return nullptr;
		}

		if (mActor) //-V547
		{
			physx::PxSceneWriteLock lock(_pxScene, __FUNCTION__, __LINE__);

			if (_mass > Math::ZEROFLOAT)
				mActor->setMass(_mass);

			auto rb = mActor->is<PxRigidBody>();
			PxRigidBodyExt::updateMassAndInertia(*rb, _density);

			_pxScene.addActor(*mActor);
		}

		return mActor;
	}

	physx::PxRigidDynamic* PhysXSystemBodys::CreateSphere(const Mat4&_trans, TimeDelta _radius, TimeDelta _mass, TimeDelta _density,
		physx::PxCooking& _pxCook,
		physx::PxPhysics& _pxPhysics,
		physx::PxMaterial& _pxMaterial,
		physx::PxScene& _pxScene)
	{
		// Initialize Cube Actor
		auto mActor = PxCreateDynamic(_pxPhysics, EngineMathToPhysX(_trans), PxSphereGeometry(_radius), _pxMaterial, _density);
		if (!mActor) {
			Warning("create actor failed!");
			return nullptr;
		}

		if (mActor)
		{
			physx::PxSceneWriteLock lock(_pxScene, __FUNCTION__, __LINE__);

			if (_mass > Math::ZEROFLOAT)
				mActor->setMass(_mass);

			auto rb = mActor->is<PxRigidBody>();
			PxRigidBodyExt::updateMassAndInertia(*rb, _density);

			_pxScene.addActor(*mActor);
		}

		return mActor;
	}

	physx::PxRigidDynamic* PhysXSystemBodys::CreateCylinderConvex(const Mat4&_trans, TimeDelta _radius, TimeDelta width, TimeDelta _mass, TimeDelta _density,
		physx::PxCooking& _pxCook,
		physx::PxPhysics& _pxPhysics,
		physx::PxMaterial& _pxMaterial,
		physx::PxScene& _pxScene)
	{
		PxConvexMesh* mesh = _CreateCylinderConvexMesh100(width, _radius, 60, _pxPhysics, _pxCook);

		// Initialize Cube Actor
		auto mActor = PxCreateDynamic(_pxPhysics, EngineMathToPhysX(_trans), PxConvexMeshGeometry(mesh), _pxMaterial, _density);
		if (!mActor) {
			Warning("create actor failed!");
			return nullptr;
		}

		if (mActor) //-V547
		{
			physx::PxSceneWriteLock lock(_pxScene, __FUNCTION__, __LINE__);
			SetupDefaultRigidDynamic(*mActor);

			if (_mass > Math::ZEROFLOAT)
				mActor->setMass(_mass);

			auto rb = mActor->is<PxRigidBody>();
			PxRigidBodyExt::updateMassAndInertia(*rb, _density);

			_pxScene.addActor(*mActor);
		}

		return mActor;
	}

	physx::PxRigidDynamic * PhysXSystemBodys::CreateCapsule(TimeDelta radius, TimeDelta height, const Mat4 & _trans, TimeDelta _mass, TimeDelta _density,
		physx::PxCooking& _pxCook,
		physx::PxPhysics& _pxPhysics,
		physx::PxMaterial& _pxMaterial,
		physx::PxScene& _pxScene)
	{
		// Initialize Cube Actor
		auto mActor = PxCreateDynamic(_pxPhysics, EngineMathToPhysX(_trans), PxCapsuleGeometry(radius, height / 2), _pxMaterial, _density);
		if (!mActor) {
			Warning("create actor failed!");
			return nullptr;
		}

		if (mActor)
		{
			physx::PxSceneWriteLock lock(_pxScene, __FUNCTION__, __LINE__);

			if (_mass > Math::ZEROFLOAT)
				mActor->setMass(_mass);

			auto rb = mActor->is<PxRigidBody>();
			PxRigidBodyExt::updateMassAndInertia(*rb, _density);

			_pxScene.addActor(*mActor);
		}

		return mActor;
	}

	physx::PxRigidStatic*
		PhysXSystemBodys::CreateStaticMesh(unsigned int _numVert, unsigned int _numFaces, const Mat4 &_trans, void*_vertices, unsigned int*_indices, TimeDelta _density,
			physx::PxCooking* _pxCook,
			physx::PxPhysics* _pxPhysics,
			physx::PxMaterial* _pxMaterial,
			physx::PxScene* _pxScene
		)
	{
		ASSERT(_pxCook && "Invalid _pxCook pointer");
		ASSERT(_pxPhysics && "Invalid _pxPhysics pointer");
		ASSERT(_pxMaterial && "Invalid _pxMaterial pointer");
		ASSERT(_pxScene && "Invalid _pxScene pointer");

		PxTriangleMeshDesc description;

		description.points.count = _numVert;
		description.points.stride = sizeof(Vertex);
		description.points.data = static_cast<Vertex*>(_vertices);

		description.triangles.count = _numFaces / 3;
		description.triangles.stride = sizeof(unsigned int) * 3;
		description.triangles.data = _indices;

		ASSERT(description.points.data && description.triangles.data, "Not exist description.points.data or description.triangles.data");

		//!@todo CHECK
		//!@todo ������� ��������� �������
		//String cookedMeshFilename;
		//String rawNameAndPath = StringHelper::CutExt(originalName);

		//if (_save_as.empty() == false) {
		//	rawNameAndPath = StringHelper::CutExt(_save_as);
		//}

		//cookedMeshFilename = rawNameAndPath;
		//cookedMeshFilename += ".pmcf";

		//DebugM("[PhysXSystemBodys::CreateStaticMesh] Created physics collision with name: %s", cookedMeshFilename.c_str());

		if (!_pxCook->validateTriangleMesh(description)) {
#ifdef HEAVY_DEBUG
			Debug("[Physics]Is not valid triangle mesh");
#endif
		}

		MyMemoryOutputStream buffer;
		bool status = _pxCook->cookTriangleMesh(description, buffer);

		if (!status) {
			Warning("[Physics]Cooking failed");
			return nullptr;
		}

		// Allocate input buffer
		MyMemoryInputData input(buffer.getData(), buffer.getSize());

		PxTriangleMesh* triangleMesh = _pxPhysics->createTriangleMesh(input);
		ASSERT(triangleMesh && "Failed creation of triangleMesh");

		if (!triangleMesh)
		{
			Warning("Failed creation of triangleMesh");
			return nullptr;
		}

		// Note: if will shared_ptr -> crash
		auto mActor = _pxPhysics->createRigidStatic(EngineMathToPhysX(_trans));
		ASSERT(mActor && "Not exist physics actor");

		if (!mActor)
		{
			Warning("Not exist physics actor");
			return nullptr;
		}

		// ReScale
		Mat4 trans = _trans;
		// create a shape instancing a triangle mesh at the given scale
		PxMeshScale scale(EngineMathToPhysX(trans.GetScale()), PxQuat(Math::ONEFLOAT));

		// Creates shapes. Note: It's not memory leak, shape will deleted when will destroyed actor
		// is shape = null, collision not exist
		auto mShape = PxRigidActorExt::createExclusiveShape(*mActor, PxTriangleMeshGeometry(triangleMesh, scale), *_pxMaterial);

		ASSERT((mShape || _pxScene) && "Invalid creation of shape or invalid pointer on _pxScene");

		if (mShape == nullptr || _pxScene == nullptr) {
			Warning("[Physics] !mShape || !_pxScene failed");
			SAFE_rELEASE(triangleMesh);
			return nullptr;
		}

		//!@todo ������� ���� ��������������� �� ��������� ����
		mShape->setFlag(PxShapeFlag::eVISUALIZATION, true);

		if (_pxScene && mActor)
		{
			physx::PxSceneWriteLock lock(*_pxScene, __FUNCTION__, __LINE__);
			_pxScene->addActor(*mActor);
		}
		else
			Warning("Can't add actor on scene. Scene is %i ", _pxScene == nullptr);

		// Delete older pointers
		SAFE_rELEASE(triangleMesh);

		return mActor;
	}

	void PhysXSystemBodys::SerializePhysXBody(physx::PxShape* mShape, const String& cookedMeshFilename) {
		ASSERT(mShape && "[PhysXSystemBodys::SerializePhysXBody] Not exist mShape");
		if (!mShape)
			return;

		PhysXSerializer::SerializeObjectFromWorld(mShape, cookedMeshFilename);
	}

	physx::PxRigidDynamic * PhysXSystemBodys::CreateConvexHull(unsigned int _numVert, unsigned int _numFaces, const Mat4 &_trans, void*_vertices, TimeDelta _mass, TimeDelta _density,
		physx::PxCooking* _pxCook,
		physx::PxPhysics* _pxPhysics,
		physx::PxMaterial* _pxMaterial,
		physx::PxScene* _pxScene
	)
	{
		ASSERT(_pxCook, "[Physics] Invalid _pxCook pointer");
		ASSERT(_pxPhysics, "[Physics] Invalid _pxPhysics pointer");
		ASSERT(_pxMaterial, "[Physics] Invalid _pxMaterial pointer");
		ASSERT(_pxScene, "[Physics] Invalid _pxScene pointer");

		Vertex* _vertexPtr = static_cast<Vertex*>(_vertices);
		ASSERT(_vertexPtr, "[Physics] VertexPtr is null");

		PxConvexMesh* convex = _CreateConvexMesh(_vertexPtr, _numVert, *_pxPhysics, *_pxCook);
		ASSERT(convex, "[Physics] Cooking failed");

		PxRigidDynamic* mActor = _pxPhysics->createRigidDynamic(PxTransform(PxIdentity));
		ASSERT(mActor, "[Physics] create actor failed!");

		// Initialize Convex Actor
		auto mShape = PxRigidActorExt::createExclusiveShape(*mActor, PxConvexMeshGeometry(convex), *_pxMaterial); //-V595
		ASSERT(mShape, "[Physics] create actor failed!");
		ASSERT(mActor, "[Physics] create actor failed!");

		//!@todo CHECK
		//!@todo ������� ��������� �������
		/*String cookedMeshFilename;
		String rawNameAndPath = StringHelper::CutExt(_originalName);

		if (_save_as.empty() == false) {
			rawNameAndPath = StringHelper::CutExt(_save_as);
		}*/

		//cookedMeshFilename = rawNameAndPath;
		//cookedMeshFilename += ".pmcf";

		//DebugM("[PhysXSystemBodys::CreateStaticMesh] Created physics collision with name: %s", cookedMeshFilename.c_str());

		if (mActor && _pxScene)
		{
			physx::PxSceneWriteLock lock(*_pxScene, __FUNCTION__, __LINE__);

			auto rb = mActor->is<PxRigidBody>();

			if (rb && _mass > Math::ZEROFLOAT)
				rb->setMass(_mass);

			PxRigidBodyExt::updateMassAndInertia(*mActor, _density);
			_pxScene->addActor(*mActor);
		}

		return mActor;
	}

	static constexpr ENGINE_INLINE void SimulatePaused(physx::PxScene * phScene)
	{
		ASSERT(phScene, "Not exist phScene");
		if (!phScene) return;

		phScene->simulate(Math::ZEROFLOAT);
		phScene->flushQueryUpdates();
		phScene->flushSimulation(true);
		phScene->fetchResults(true);
	}

	void PhysX_DeleteActor(physx::PxRigidActor* _actor)
	{
		ASSERT(_actor, __FUNCTION__);

		// Also releases any shapes associated with the actor.
		SAFE_rELEASE(_actor);
	}
}
#endif