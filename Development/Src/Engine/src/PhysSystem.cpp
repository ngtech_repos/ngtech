/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"

//***************************************************************************
#include "PhysSystem.h"
#include "PhysBody.h"
#include "EngineMathToPhysx.inl"
#include "PhysXCallbacks.h"
#include "PhysXMaterialSh.h"
#include "../inc/Physics/VehicleRaycast.h"
//***************************************************************************
#include "PxPhysicsAPI.h"
#include "PxPhysXConfig.h"
#include "PxFiltering.h"
#include "foundation/PxMemory.h"
#include "PsThread.h"

#include "extensions/PxDefaultStreams.h"

#include "PxPvd.h"
#include "cudamanager/PxCudaContextManager.h"
//***************************************************************************
#include "../../inc/Physics/VehicleTireFriction.h"
#include "../../inc/PhysicsMaterialsManager.h"
//***************************************************************************

#ifdef _DEBUG
//#define ENABLE_PVD 1
#endif

namespace NGTech {
	/**
	*/
	using namespace physx;

	/**
	*/
	PxDefaultAllocator gDefaultAllocatorCallback;
	static PxDefaultErrorCallback gDefaultErrorCallback;
	static PxSimulationFilterShader gDefaultFilterShader = PxDefaultSimulationFilterShader;

	PhysSystem::PhysSystem(bool _m_SingleThread) :
		m_SingleThread(_m_SingleThread)
	{
		Debug(__FUNCTION__);
	}

	/**
	*/
	PhysSystem::PhysSystem() :
		mFoundation(nullptr),
		mDebuggerConnection(nullptr),
		mPhysics(nullptr),
		mCooking(nullptr),
		mCpuDispatcher(nullptr),
#if PLATFORM_OS == PLATFORM_OS_WINDOWS
		mCudaContextManager(nullptr),
#endif

		mNeedlyDestroyThread(false),
		mPhysxExtensions(false),
		mVehicleSDKInitialized(false),

		_epeAccumilator(Math::ZERODELTA),
		_epeTimeStep(Math::ZERODELTA),
		_savedTimeStep(Math::ZERODELTA),
		_timeStep(Math::ZERODELTA),
		_nSteps(0)
	{
		Debug(__FUNCTION__);
	}

	void PhysSystem::_UpdateLoop(void*userData)
	{
		BROFILER_THREAD("Physics Thread");
		auto ph = (PhysSystem*)userData;
		ASSERT(ph && "Not exist userdata");

		static bool needlyDestroy = false;

		while (!needlyDestroy)
		{
			// Emulate "wait for events" message
			MT::Thread::Sleep(5);

			// Sleep ����������� �����,����� ��� ����� �����
			if (ph)
			{
				ph->UpdateMultithreaded();

				needlyDestroy = ph->mNeedlyDestroyThread;
			}
			// ��� ����� :)
			//MT::SpinSleepMilliSeconds(1);
			// Nick:Note: This was changed 16.04.2017(Sleep(5) and commented ^. It's got is better results
		}
	}

	/**
	*/
	void PhysSystem::Initialise()
	{
		Log::WriteHeader("-- PhysSystem --");
		Debug("PhysSystem::Initialise");

		mFoundation = PxCreateFoundation(PX_PHYSICS_VERSION, gDefaultAllocatorCallback, gDefaultErrorCallback);
		ASSERT(mFoundation, "PhysSystem::Initialise()-PxCreateFoundation failed!");

#ifdef ENABLE_PVD
		//!@todo NEW FUNCTION
		mDebuggerConnection = PxCreatePvd(*mFoundation);
		PxPvdTransport* transport = PxDefaultPvdSocketTransportCreate("127.0.0.1", 5425, 10);
		mDebuggerConnection->connect(*transport, PxPvdInstrumentationFlag::eALL);
#endif

		// Mass of every object is multiplied by this factor (so the EPE simulation is more stable)
		PxTolerancesScale scale;
		scale.length = 1;
		scale.speed = 10;

		mPhysics = PxCreatePhysics(PX_PHYSICS_VERSION, *mFoundation, scale, true, mDebuggerConnection);
		ASSERT(mPhysics, "PhysSystem::Initialise()-PxCreatePhysics failed!");

		mPhysxExtensions = PxInitExtensions(*mPhysics, mDebuggerConnection);
		ASSERT(mPhysxExtensions, "PhysSystem::Initialise()-PxInitExtensions failed!");

		PxCookingParams params(scale);
		params.meshWeldTolerance = Math::ZEROFLOAT;
		// We need to disable the mesh cleaning part so that the vertex mapping remains untouched.
		params.meshPreprocessParams = PxMeshPreprocessingFlag::eWELD_VERTICES;
		params.meshPreprocessParams |= PxMeshPreprocessingFlag::eDISABLE_CLEAN_MESH;

		// Deformable meshes are only supported with PxMeshMidPhase::eBVH33.
		// We will use eBVH34 (SSE2 processors only)
		params.midphaseDesc.setToDefault(PxMeshMidPhase::eBVH34);
		params.convexMeshCookingType = PxConvexMeshCookingType::eQUICKHULL;

		mCooking = PxCreateCooking(PX_PHYSICS_VERSION, *mFoundation, params);
		ASSERT(mCooking, "PxCreateCooking failed!");

		if (!mCooking) {
			Error("PxCreateCooking failed!", true);
			return;
		}

		// setup default material...
		phMatManager.Initialise(mPhysics);

		_InitVehicleSupport();

		_CreateCPUDispatcher();
		_CreateGPUDispatcher();
		m_ActivePhysScene.Create(this);

		_InitThread();
	}

	PhysSystem::~PhysSystem()
	{
		Debug("PhysSystem::~PhysSystem()");
		_DestroyThread();

		// Delete Vehicle controller
		if (mVehicleSDKInitialized)
		{
			physx::PxCloseVehicleSDK();
			LogPrintf("VehicleSDK closed");
		}

		LogPrintf("Prepare for release of defPhysMat");

		// Delete default physics material
		phMatManager.DeInitialise();

		LogPrintf("Released of defPhysMat. Prepare for release ActivePhysScene");

		// Delete default scene
		m_ActivePhysScene.Release();

		LogPrintf("ActivePhysScene released");

		if (mPhysxExtensions) {
			PxCloseExtensions();
			LogPrintf("PhysX extensions is closed");
		}

		SAFE_rELEASE(mCpuDispatcher);
		LogPrintf("PhysX CPU Dispatcher is closed");

#if PLATFORM_OS == PLATFORM_OS_WINDOWS
		// we works with modificated version of physx, GPU acceleration is not avalible
	/*	SAFE_rELEASE(mCudaContextManager);
		LogPrintf("PhysX CUDA Dispatcher is closed");*/
#endif

		//!@todo Fixme
		// FIXME: code clean
		// crash
		//SAFE_rELEASE(mPhysics);
		//LogPrintf("PhysX mPhysics is released");

#if ENABLE_PVD
		PxPvdTransport* transport = mDebuggerConnection->getTransport();
		SAFE_rELEASE(mDebuggerConnection);
		SAFE_rELEASE(transport);
		LogPrintf("PhysX Debugger Connection is closed");
#endif
		// crash
		//SAFE_rELEASE(mFoundation);
		//LogPrintf("PhysX mFoundation is released");

		//SAFE_rELEASE(mCooking);
		//LogPrintf("PhysX mCooking is released");
	}

	void PhysSystem::_InitVehicleSupport()
	{
		mVehicleSDKInitialized = PxInitVehicleSDK(*mPhysics);
		PxVehicleSetBasisVectors(PxVec3(0, 1, 0)/*up*/, PxVec3(0, 0, 1)/*forward*/);

		//Set the vehicle update mode to be immediate velocity changes.
		PxVehicleSetUpdateMode(PxVehicleUpdateMode::eVELOCITY_CHANGE);

		if (mVehicleSDKInitialized)
			LogPrintf("PxInitVehicleSDK is successful");
		else
			ErrorWrite("PxInitVehicleSDK is failed");
	}

	void PhysSystem::UpdateSingleThreaded(TimeDelta _delta) {
		auto gEngineState = GetEngineState();
		// info:https://stackoverflow.com/questions/18591924/how-to-use-bitmask
		bool _paused = ((gEngineState & (STATE_PAUSED)) == (STATE_PAUSED)) || ((gEngineState & (STATE_PHYSICS_PAUSED)) == (STATE_PHYSICS_PAUSED));

		if (_paused) {
			//Debug("[PhysSystem::UpdateSingleThreaded] engine paused");
			return;
		}
		else {
			//Warning("State %i", gEngineState);
		}

		m_ActivePhysScene.SimulateScene(_delta);
	}

	void PhysSystem::UpdateMultithreaded()
	{
		auto gEngineState = GetEngineState();
		// info:https://stackoverflow.com/questions/18591924/how-to-use-bitmask
		bool _paused = ((gEngineState & (STATE_PAUSED)) == (STATE_PAUSED)) || ((gEngineState & (STATE_PHYSICS_PAUSED)) == (STATE_PHYSICS_PAUSED));

		if (_paused) {
			//Debug("[PhysSystem::UpdateMultithreaded] engine paused");
			return;
		}
		else {
			//Warning("State %i", gEngineState);
		}

		static Timer timer;
		TimeDelta dtime = timer.GetElapsed();

		m_ActivePhysScene.SimulateScene(dtime);

		// ��������� ����������� ����� (� getElapsed ������� �������� LastTime)
		timer.UpdateElapsed();
	}

	void PhysSystem::CreateSceneFromSceneDesc(const physx::PxSceneDesc*_sceneDesc, PhysXSceneData& _sceneDataForChange)
	{
		ASSERT(_sceneDesc, "_sceneDesc is nullptr");

		_sceneDataForChange.m_PhysXScene = mPhysics->createScene(*_sceneDesc);
		ASSERT(_sceneDataForChange.m_PhysXScene, "PhysSystem::Initialise()-create m_PhysXScene failed!");

		_sceneDataForChange.m_ControllerManager = PxCreateControllerManager(*_sceneDataForChange.m_PhysXScene);
		ASSERT(_sceneDataForChange.m_ControllerManager, "PhysSystem::Initialise()-create mCCManager failed!");
	}

	void PhysSystem::CreateCUDADispatcherUtil(physx::PxSceneDesc*_sceneDesc)
	{
		ASSERT(_sceneDesc, "_sceneDesc is null");

#if PLATFORM_OS == PLATFORM_OS_WINDOWS
		// We use modificated PhysX version, GPU Support is not avalible
		// ����� ���� �� ������� ��� ���������� ������������������� CUDA - ��������� ��� ����� ���������� ��������, �� ���� ����������
		//try {
		//	LogPrintf("[Hardware PhysX] Trying to initialize Hardware PhysX");
		//	if (mCudaContextManager != nullptr)
		//	{
		//		// create GPU dispatcher
		//		if (_sceneDesc->gpuDispatcher == nullptr)
		//		{
		//			_sceneDesc->gpuDispatcher = mCudaContextManager->getGpuDispatcher();
		//			if (_sceneDesc->gpuDispatcher == nullptr)
		//				LogPrintf("[Hardware PhysX] Hardware PhysX is not available - not GPU dispatcher");
		//			else
		//			{
		//				LogPrintf("[Hardware PhysX] Hardware PhysX is enabled");
		//			}
		//		}
		//	}
		//	else
		//		LogPrintf("[Hardware PhysX] Hardware PhysX is not available");
		//}
		//catch (...)
		{
			LogPrintf("[Hardware PhysX] Hardware PhysX is not available");
		}
#endif
	}

	void PhysSystem::_InitThread()
	{
		if (m_SingleThread == false)
			updateThread.Start(/*SMALLEST_STACK_SIZE*/32768, _UpdateLoop, this);
	}

	void PhysSystem::_DestroyThread()
	{
		if (m_SingleThread == false) {
			mNeedlyDestroyThread = true;
			updateThread.Join();
		}
		Debug("PhysSystem::_DestroyThread()");
	}

	void PhysSystem::_CreateCPUDispatcher()
	{
		auto numWorkers = PxMax(PxI32(shdfnd::Thread::getNbPhysicalCores()) - 1, 0);

		mCpuDispatcher = PxDefaultCpuDispatcherCreate(numWorkers);
		ASSERT(mCpuDispatcher, "PhysSystem::Initialise()-PxDefaultCpuDispatcherCreate failed!");
	}

	void PhysSystem::_CreateGPUDispatcher()
	{
		PxCudaContextManagerDesc cudaContextManagerDesc;
		mCudaContextManager = PxCreateCudaContextManager(*mFoundation, cudaContextManagerDesc);

		if (mCudaContextManager)
		{
			if (!mCudaContextManager->contextIsValid())
			{
				SAFE_rELEASE(mCudaContextManager);
				LogPrintf("[Hardware PhysX] Hardware PhysX is not available - Invalid CUDA context");
			}
		}
		else
			LogPrintf("[Hardware PhysX] Hardware PhysX is not available - not available CUDA context");
	}
}