/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
#if !DISABLE_PHYSICS
//***************************************************************************
#include "PhysXCallbacks.h"
#include "EngineMathToPhysx.inl"
//***************************************************************************
#include "PhysXMaterialSh.h"
//***************************************************************************

namespace NGTech
{
	using namespace physx;
	void PhysXCallback::onConstraintBreak(PxConstraintInfo* constraints, PxU32 count) {
	}

	void PhysXCallback::onWake(PxActor** actors, PxU32 count) {
	}
	void PhysXCallback::onSleep(PxActor** actors, PxU32 count) {
	}
	void PhysXCallback::onContact(const PxContactPairHeader& pairHeader, const PxContactPair* pairs, PxU32 nbPairs)
	{
		if (pairHeader.actors[0] == nullptr || pairHeader.actors[1] == nullptr)
			return;

		auto actor0 = pairHeader.actors[0];
		auto actor1 = pairHeader.actors[1];

		_PlaySound(actor0);
		_PlaySound(actor1);
	}
	void PhysXCallback::onTrigger(PxTriggerPair* pairs, PxU32 count) {
	}

	void PhysXCallback::onContactModify(PxContactModifyPair* const pairs, PxU32 count) {
	}

	void PhysXCallback::Insert(const PhysBodyDescSh& _desc) {
		ASSERT(_desc.mActorTmp, "Invalid pointer on pb");

		if (_desc.mActorTmp == nullptr)
			return;

		ASSERT(_desc.m_AttachedPhysicsMaterial, "Invalid pointer on m_AttachedPhysicsMaterial");
		if (_desc.m_AttachedPhysicsMaterial == nullptr)
			return;

		m_actorToImpactSound.insert(std::pair<physx::PxRigidActor*, PhysXMaterialSh*>(_desc.mActorTmp, _desc.m_AttachedPhysicsMaterial));
	}

	void PhysXCallback::_PlaySound(physx::PxRigidActor*_actor)
	{
		ASSERT(_actor, "Invalid pointer on _actor");

		try {
			if (!_actor)
				return;

			if (_actor && _actor->getScene() == nullptr)
				return;

			auto it = m_actorToImpactSound.find(_actor);
			if (it == m_actorToImpactSound.end())
				return;

			if (it->second == nullptr)
				return;

			auto ptr = ALSoundSource::CreateWithSoundSourcePreset(it->second->GetCollisionSound());

			if (ptr == nullptr)
				return;

			// ��� ��� ����� �� ���������, ��� �� �������������
			if (ptr && (ptr->isPlaying() == false))
			{
				// Crash on release x64 if run under debugger and do exit(press esc)
				physx::PxSceneReadLock lock(*_actor->getScene(), __FUNCTION__, __LINE__);

				ptr->SetPosition(PhysXToEngineMath(_actor->getGlobalPose().p));
				ptr->Play();
			}

			SAFE_RELEASE(ptr);
		}
		catch (std::exception&e)
		{
			Warning(e.what());
		}
	}
}
#endif