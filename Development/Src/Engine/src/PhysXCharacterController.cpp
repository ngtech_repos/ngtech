/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
//***************************************************************************
#include "PxPhysicsAPI.h"
//***************************************************************************
#include "PhysSystem.h"
#include "PhysBody.h"
#include "Log.h"
//***************************************************************************
#include "EngineMathToPhysx.inl"
#include "PhysXCharacterController.h"
//***************************************************************************

namespace NGTech
{
	/**
	*/
	using namespace physx;

	PhysXCharacterController::PhysXCharacterController() :
		mController(nullptr) {
		_InitCharacterController();
	}

	PhysXCharacterController::PhysXCharacterController(const CharacterControllerDesc & _desc)
		: desc(_desc),
		mController(nullptr)
	{
		_InitCharacterController();
	}

	PhysXCharacterController::~PhysXCharacterController() {
		SAFE_rELEASE(mController);
	}

	void PhysXCharacterController::_CreateCapsuleCharacterController() {
		auto ph = GetPhysics();
		ASSERT(ph, "Not exist Physics module");

		if (!ph) {
			Warning("Character Controller not exist"); return;
		}
		mController = ph->CreateCapsuleCharacterController(this, &this->desc);
	}

	void PhysXCharacterController::_InitCharacterController()
	{
		//Warning("[%s] Desc: contactOffset %f, density %f, height %f, invisibleWallHeight %f, maxJumpHeight %f, mMode %i, position: (%f, %f, %f), radius: %f, scaleCoeff: %f, slopeLimit: %f, stepOffset: %f, upDirection: (%f, %f, %f), volumeGrowth: %f"
		//	, __FUNCTION__,
		//	desc.contactOffset,
		//	desc.density,
		//	desc.height,
		//	desc.invisibleWallHeight,
		//	desc.maxJumpHeight,
		//	static_cast<unsigned int>(desc.mMode),
		//	desc.position.x, desc.position.y, desc.position.z,
		//	desc.radius,
		//	desc.scaleCoeff,
		//	desc.slopeLimit,
		//	desc.stepOffset,
		//	desc.upDirection,
		//	desc.volumeGrowth);

		if (desc.mMode == desc.CCT_CAPSULE)
			_CreateCapsuleCharacterController();
		else
			_CreateBoxCharacterController();

		if (mController == nullptr) {
			ErrorWrite("[%s] Failed physics desc", __FUNCTION__);
			ErrorWrite("[%s] Desc: contactOffset %f, density %f, height %f, invisibleWallHeight %f, maxJumpHeight %f, mMode %i, position: (%f, %f, %f), radius: %f, scaleCoeff: %f, slopeLimit: %f, stepOffset: %f, upDirection: (%s), volumeGrowth: %f"
				, __FUNCTION__,
				desc.contactOffset,
				desc.density,
				desc.height,
				desc.invisibleWallHeight,
				desc.maxJumpHeight,
				static_cast<unsigned int>(desc.mMode),
				desc.position.x, desc.position.y, desc.position.z,
				desc.radius,
				desc.scaleCoeff,
				desc.slopeLimit,
				desc.stepOffset,
				desc.upDirection,
				desc.volumeGrowth);
		}
	}

	void PhysXCharacterController::_CreateBoxCharacterController() {
		auto ph = GetPhysics();
		ASSERT(ph, "Not exist Physics module");
		if (!ph) return;

		mController = ph->CreateBoxCharacterController(this, &this->desc);
	}

	void PhysXCharacterController::Move(float x, float y, float z, TimeDelta elapsedTime)
	{
		ASSERT(mController, "Not exist mController");

		auto scene = mController->getScene();

		if (scene) {
			physx::PxSceneWriteLock lock(*scene, __FUNCTION__, __LINE__);
			static PxControllerFilters filters(0);
			{
				mController->move(EngineMathToPhysX(Vec3(x, y, z)), EPSILON, elapsedTime, filters);
			}
		}
	}

	Vec3 PhysXCharacterController::GetPosition() const
	{
		Vec3 res = Vec3::ZERO;

		ASSERT(mController, "Not exist mController");
		if (mController == nullptr)
			return res;

		auto scene = mController->getScene();

		ASSERT(scene, "Not exist scene");

		if (scene) {
			physx::PxSceneReadLock lock(*scene, __FUNCTION__, __LINE__);
			res = PhysXToEngineMath(mController->getPosition());
		}

		return res;
	}

	void PhysXCharacterController::SetPosition(const Vec3&_pos)
	{
		ASSERT(mController, "Not exist mController");

		if (!mController)	return;

		auto scene = mController->getScene();
		ASSERT(scene, "Not exist mController");

		if (scene) {
			physx::PxSceneWriteLock lock(*scene, __FUNCTION__, __LINE__);
			mController->setPosition(EngineMathToPhysXDouble(_pos));
		}
	}

	void PhysXCharacterController::Jump(Vec3 &movement)
	{
		if (CanJump())
		{
			mJump.StartJump(desc.maxJumpHeight);

			//!@todo Unactual Jump, needly rewrite
			TODO("Unactual Jump, needly rewrite")
				//!@todo CHECK
				/*	float dy;
					const float heightDelta = _JumpGetHeight(dtime, desc.maxJumpHeight);
					if (heightDelta != Math::ZEROFLOAT)
					{
						dy = heightDelta;
						movement.y += dy;
					}*/

				mJump.StopJump();
		}
	}

	bool PhysXCharacterController::CanJump()
	{
		PxControllerState cctState;

		ASSERT(mController, "Not exist mController");

		if (!mController) return false;

		mController->getState(cctState);
		return (cctState.collisionFlags & PxControllerCollisionFlag::eCOLLISION_DOWN) != 0;
	}

	TimeDelta PhysXCharacterController::_JumpGetHeight(TimeDelta _time, TimeDelta _descHeight) {
		return mJump.GetHeight(_time, _descHeight);
	}

	const CharacterControllerDesc& PhysXCharacterController::GetDescription() const {
		return desc;
	}
}