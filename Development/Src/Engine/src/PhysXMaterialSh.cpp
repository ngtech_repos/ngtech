/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
//***************************************************************************
#include "PhysSystem.h"
#include "PhysXMaterialSh.h"
//***************************************************************************
#include "PxMaterial.h"
#include "PxPhysicsAPI.h"
//***************************************************************************

namespace NGTech
{
	using namespace physx;
	const TimeDelta PhysXMaterialSh::frictionCoef = TimeDelta(4.7);

	PhysXMaterialSh::PhysXMaterialSh() {}

	PhysXMaterialSh::PhysXMaterialSh(TimeDelta _staticFriction, TimeDelta _dynamicFriction, TimeDelta _restitution, const String& _materialName, bool _vehicleDrivableSurface, const String& _materialsMapKey)
		:
		m_StaticFriction(_staticFriction),
		m_DynamicFriction(_dynamicFriction),
		m_Restitution(_restitution),
		m_MaterialName(_materialName),
		m_VehicleDrivableSurface(_vehicleDrivableSurface),
		m_MaterialsMapKey(_materialsMapKey)
	{}

	PhysXMaterialSh::~PhysXMaterialSh() {
		SAFE_rELEASE(m_PxMaterial);
	}

	void PhysXMaterialSh::SetStaticFriction(TimeDelta _fr) {
		ASSERT(m_PxMaterial, "Invalid pointer on mMaterial");
		if (!m_PxMaterial) return;
		m_PxMaterial->setStaticFriction(_fr * frictionCoef);
	}

	void PhysXMaterialSh::SetDynamicFriction(TimeDelta _fr) {
		ASSERT(m_PxMaterial, "Invalid pointer on mMaterial");
		if (!m_PxMaterial) return;
		m_PxMaterial->setDynamicFriction(_fr * frictionCoef);
	}

	void PhysXMaterialSh::SetRestitution(TimeDelta _r) {
		ASSERT(m_PxMaterial, "Invalid pointer on mMaterial");
		if (!m_PxMaterial) return;
		m_PxMaterial->setRestitution(_r * frictionCoef);
	}

	void PhysXMaterialSh::SetCombineModeMultiplyFriction() {
		ASSERT(m_PxMaterial, "Invalid pointer on mMaterial");
		if (!m_PxMaterial) return;
		m_PxMaterial->setFrictionCombineMode(PxCombineMode::eMULTIPLY);
	}

	void PhysXMaterialSh::SetCombineModeMultiplyRestitution() {
		ASSERT(m_PxMaterial, "Invalid pointer on mMaterial");
		if (!m_PxMaterial) return;
		m_PxMaterial->setRestitutionCombineMode(PxCombineMode::eMULTIPLY);
	}

	void PhysXMaterialSh::SetImpactSoundForPhysBody(PhysBody* _pb, ALSound* _snd)
	{
		ASSERT(_snd, "Invalid pointer on _snd");
		if (!_snd) return;

		ASSERT(_pb, "Invalid pointer on _pb");
		if (!_pb) return;

		m_CollisionSound = _snd;
		ASSERT(m_CollisionSound, "Invalid pointer on Collision Sound");
		if (!m_CollisionSound) return;

		auto ph = GetPhysics();
		ASSERT(ph, "Not exist physics pointer on physSystem");
		if (!ph) return;

		ph->GetActivePhysScene().InsertPhActorToCollisionCallback(_pb->GetDescCopy());
	}
}