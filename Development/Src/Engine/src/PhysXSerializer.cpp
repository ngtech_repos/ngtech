#include "EnginePrivate.h"

//***************************************************************************
#include "PxPhysicsAPI.h"
#include "extensions/PxCollectionExt.h"
//***************************************************************************
#include "PhysSystem.h"
#include "PhysXSerializer.h"
//***************************************************************************

namespace NGTech {
	/**
	*/
	PhysXSerializer::PhysXSerializer() {
		auto ph = GetPhysics();
		ASSERT(ph, "Invalid ph module");
		if (!ph) return;

		auto phP = ph->GetPxPhysics();
		ASSERT(phP, "Invalid GetPxPhysics module");

		if (!phP) return;

		auto aPS = ph->GetActivePxScene();
		ASSERT(aPS, "Invalid GetActivePxScene module");

		if (!aPS) return;

		_InitShP(mUseBinarySerialization, phP, aPS);
	}

	/**
	*/
	PhysXSerializer::~PhysXSerializer() {}

	/**
	*/
	void PhysXSerializer::SerializeObjectFromWorld(PxShape* _shape, const String& _fileName)
	{
		ASSERT(_shape, "[PhysXSerializer::SerializeObjectsFromWorld] Not exist _shape");

		DebugM("Serialize object to: %s", _fileName.c_str());

		/*PxDefaultMemoryOutputStream*/
		PxDefaultFileOutputStream	sharedOutputStream(_fileName.c_str());
		/*PxDefaultMemoryOutputStream*/
		PxDefaultFileOutputStream actorOutputStream(_fileName.c_str());

		PhysXSerializer ser;
		//!@todo Check Possible Memleak
		ser._SerializeObjects(&sharedOutputStream, &actorOutputStream, _shape);
	}

	/**
	*/
	void PhysXSerializer::SerializeAllObjectsFromWorld(const String& _dir, const String& _sceneName)
	{
		PhysXSerializer ser;

		ASSERT(ser.mPhysics, "[PhysXSerializer::SerializeAllObjectsFromWorld] Not exist ser.mPhysics");
		if (!ser.mPhysics) return;
		ASSERT(ser.mActiveScene, "[PhysXSerializer::SerializeAllObjectsFromWorld] Not exist ser.mActiveScene");
		if (!ser.mActiveScene) return;

		String _cookedSceneName = StringHelper::CutExt(_sceneName);
		_cookedSceneName += ".pscf"; //!@todo FORMAT FROM CONFIG

		ser._SerializeEverything(_dir, _cookedSceneName);
	}

	/**
	*/
	void PhysXSerializer::_SerializeEverything(const String& _dir, const String& _sceneName)
	{
		ASSERT(mPhysics, "mPhysics not exist");
		if (!mPhysics) return;
		ASSERT(mActiveScene, "mActiveScene not exist");
		if (!mActiveScene) return;

		// Registry for serializable types
		PxSerializationRegistry* registry = PxSerialization::createSerializationRegistry(*mPhysics);

		FileUtil::CreateDirectoryIfNotExist(_dir);

		PxDefaultFileOutputStream outStream((_dir + _sceneName).c_str());          // The user stream doing the actual write to disk

											// 1) Create a collection from the set of all objects in the physics SDK that are shareable across
											//    multiple scenes.
		PxCollection* everythingCollection = PxCollectionExt::createCollection(*mPhysics);

		ASSERT(everythingCollection, "[PhysXSerializer::_SerializeEverything] everythingCollection is null");

		if (!everythingCollection) return;

		// 2) Create a collection from all objects in the scene and add it
		//    to everythingCollection.
		PxCollection* collectionScene = PxCollectionExt::createCollection(*mActiveScene);

		ASSERT(collectionScene, "[PhysXSerializer::_SerializeEverything] collectionScene is null");
		if (!collectionScene) return;
		everythingCollection->add(*collectionScene);
		collectionScene->release();

		ASSERT(registry, "[PhysXSerializer::_SerializeEverything] registry is null");

		if (!registry) return;

		// 3) Complete collection
		PxSerialization::complete(*everythingCollection, *registry);

		// 4) serialize collection and release it

		if (mUseBinarySerialization) {
			// Binary
			PxSerialization::serializeCollectionToBinary(outStream, *everythingCollection, *registry);
		}
		else {
			// RepX
			PxSerialization::serializeCollectionToXml(outStream, *everythingCollection, *registry);
		}

		SAFE_rELEASE(everythingCollection);
		SAFE_rELEASE(registry);
	}

	/**
	Creates two example collections:
	- collection with actors and joints that can be instantiated multiple times in the scene
	- collection with shared objects
	*/
	static ENGINE_INLINE void _CreateCollections(PxCollection* sharedCollection, PxCollection* actorCollection, PxSerializationRegistry* sr, PxShape* shape)
	{
		ASSERT(sr, "Invalid PxSerializationRegistry");
		if (!sr) return;
		ASSERT(shape, "Invalid PxShape");
		if (!shape) return;

		sharedCollection = PxCreateCollection();		// collection for all the shared objects //-V763
		actorCollection = PxCreateCollection();			// collection for all the nonshared objects //-V763

		PxSerialization::complete(*sharedCollection, *sr);									// chases the pointer from shape to material, and adds it
		PxSerialization::createSerialObjectIds(*sharedCollection, PxSerialObjectId(77));	// arbitrary choice of base for references to shared objects

		actorCollection->add(*shape);
		PxSerialization::complete(*actorCollection, *sr, sharedCollection, true);			// chases all pointers and recursively adds actors and joints
	}

	/**
	Create objects, add them to collections and serialize the collections to the steams gSharedStream and gActorStream
	This function doesn't setup the gPhysics global as the corresponding physics object is only used locally
	*/
	void PhysXSerializer::_SerializeObjects(PxOutputStream* sharedStream, PxOutputStream* actorStream, PxShape* _shape)
	{
		if (!mPhysics) return;

		PxSerializationRegistry* sr = PxSerialization::createSerializationRegistry(*mPhysics);

		ASSERT(actorStream, "Invalid PxOutputStream actorStream");
		if (!actorStream) return;
		ASSERT(sharedStream, "Invalid PxOutputStream sharedStream");
		if (!sharedStream) return;
		ASSERT(_shape, "Invalid PxShape");
		if (!_shape) return;

		PxCollection* sharedCollection = nullptr;
		PxCollection* actorCollection = nullptr;

		_CreateCollections(sharedCollection, actorCollection, sr, _shape);

		// Alternatively to using PxDefaultMemoryOutputStream it would be possible to serialize to files using
		// PxDefaultFileOutputStream or a similar implementation of PxOutputStream.
		if (mUseBinarySerialization)
		{
			PxSerialization::serializeCollectionToBinary(*sharedStream, *sharedCollection, *sr); //-V522
			PxSerialization::serializeCollectionToBinary(*actorStream, *actorCollection, *sr, sharedCollection); //-V522
		}
		else
		{
			PxSerialization::serializeCollectionToXml(*sharedStream, *sharedCollection, *sr);
			PxSerialization::serializeCollectionToXml(*actorStream, *actorCollection, *sr, NULL, sharedCollection);
		}

		SAFE_rELEASE(actorCollection);
		SAFE_rELEASE(sharedCollection);
		SAFE_rELEASE(sr);
	}

	/**
	TODO: Uncomment this
	*/
	void PhysXSerializer::_DeserializeObjects(PxInputData*& sharedData, PxInputData*& actorData)
	{
		//!@todo CHECK
		//PxSerializationRegistry* sr = PxSerialization::createSerializationRegistry(*gPhysics);

		//PxCollection* sharedCollection = NULL;
		//{
		//	if (gUseBinarySerialization)
		//	{
		//		void* alignedBlock = createAlignedBlock(sharedData.getLength());
		//		sharedData.read(alignedBlock, sharedData.getLength());
		//		sharedCollection = PxSerialization::createCollectionFromBinary(alignedBlock, *sr);
		//	}
		//	else
		//	{
		//		sharedCollection = PxSerialization::createCollectionFromXml(sharedData, *gCooking, *sr);
		//	}
		//}

		//// Deserialize collection and instantiate objects twice, each time with a different transform
		//PxTransform transforms[2] = { PxTransform(PxVec3(-5.0f, 0.0f, 0.0f)), PxTransform(PxVec3(5.0f, 0.0f, 0.0f)) };

		//for (PxU32 i = 0; i < 2; i++)
		//{
		//	PxCollection* collection = NULL;

		//	// If the PxInputData actorData would refer to a file, it would be better to avoid reading from it twice.
		//	// This could be achieved by reading the file once to memory, and then working with copies.
		//	// This is particulary practical when using binary serialization, where the data can be directly
		//	// converted to physics objects.
		//	actorData.seek(0);

		//	if (gUseBinarySerialization)
		//	{
		//		void* alignedBlock = createAlignedBlock(actorData.getLength());
		//		actorData.read(alignedBlock, actorData.getLength());
		//		collection = PxSerialization::createCollectionFromBinary(alignedBlock, *sr, sharedCollection);
		//	}
		//	else
		//	{
		//		collection = PxSerialization::createCollectionFromXml(actorData, *gCooking, *sr, sharedCollection);
		//	}

		//	for (PxU32 o = 0; o < collection->getNbObjects(); o++)
		//	{
		//		PxRigidActor* rigidActor = collection->getObject(o).is<PxRigidActor>();
		//		if (rigidActor)
		//		{
		//			PxTransform globalPose = rigidActor->getGlobalPose();
		//			globalPose = globalPose.transform(transforms[i]);
		//			rigidActor->setGlobalPose(globalPose);
		//		}
		//	}

		//	gScene->addCollection(*collection);
		//	collection->release();
		//}
		//sharedCollection->release();

		//PxMaterial* material;
		//gPhysics->getMaterials(&material, 1);
		//PxRigidStatic* groundPlane = PxCreatePlane(*gPhysics, PxPlane(0, 1, 0, 0), *material);
		//gScene->addActor(*groundPlane);
		//sr->release();
	}

	/**
	*/
	bool PhysXSerializer::_InitShP(bool _UseBinarySerialization, physx::PxPhysics*_P, physx::PxScene*_ActiveScene)
	{
		ASSERT(_P, "Not exist PxPhysics pointer");
		if (!_P) return false;

		ASSERT(_ActiveScene, "Not exist _ActiveScene pointer");
		if (!_ActiveScene) return false;

		mPhysics = _P;
		mActiveScene = _ActiveScene;

		mUseBinarySerialization = _UseBinarySerialization;

		return true;
	}
}