/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
//***************************************************************************
#include "PhysSystem.h"
#include "PhysBody.h"
#include "Log.h"
//***************************************************************************
#include "PxPhysicsAPI.h"
#include "EngineMathToPhysx.inl"
//***************************************************************************

namespace NGTech {
	using namespace physx;

	/**
	*/
	void SetupDefaultRigidDynamic(PxRigidDynamic& body, bool kinematic)
	{
		auto _scene = body.getScene();
		ASSERT(_scene, "Invalid scene");

		if (_scene)
		{
			physx::PxSceneWriteLock lock(*_scene, __FUNCTION__, __LINE__);
			body.setContactReportThreshold(0);
			body.setActorFlag(PxActorFlag::eVISUALIZATION, true);
			body.setAngularDamping(0.5f);
			body.setRigidBodyFlag(PxRigidBodyFlag::eKINEMATIC, kinematic);
		}
	}

	/**
	*/
	void addForceAtLocalPos(PxRigidBody& body, const PxVec3& force, const PxVec3& pos,
		PxForceMode::Enum mode, bool wakeup)
	{
		auto _scene = body.getScene();
		ASSERT(_scene, "Invalid scene");

		if (_scene)
		{
			physx::PxSceneReadLock lock(*_scene, __FUNCTION__, __LINE__);

			const PxVec3 globalForcePos = body.getGlobalPose().transform(pos);  // to world space
			const PxTransform globalPose = body.getGlobalPose();
			const PxVec3 centerOfMass = globalPose.transform(body.getCMassLocalPose().p);
			const PxVec3 torque = (globalForcePos - centerOfMass).cross(force);
			body.addForce(force, mode, wakeup);
			body.addTorque(torque, mode, wakeup);
		}
	}
}