#include "EnginePrivate.h"

//***************************************************************************
#include "PxPhysicsAPI.h"
//***************************************************************************
#include "PhysSystem.h"
#include "Physics/PhysScene.h"
#include "PhysXCallbacks.h"
#include "EngineMathToPhysx.inl"
//***************************************************************************

namespace NGTech {
	using namespace physx;

	/**
	*/
	static ENGINE_INLINE physx::PxFilterFlags SampleFilterShader(
		physx::PxFilterObjectAttributes attributes0, physx::PxFilterData filterData0,
		physx::PxFilterObjectAttributes attributes1, physx::PxFilterData filterData1,
		physx::PxPairFlags& pairFlags, const void* constantBlock, physx::PxU32 constantBlockSize)
	{
		// let triggers through
		if (physx::PxFilterObjectIsTrigger(attributes0) || physx::PxFilterObjectIsTrigger(attributes1))
		{
			pairFlags = physx::PxPairFlag::eTRIGGER_DEFAULT;
			return physx::PxFilterFlag::eDEFAULT;
		}
		// generate contacts for all that were not filtered above
		pairFlags = physx::PxPairFlag::eCONTACT_DEFAULT | physx::PxPairFlag::eTRIGGER_DEFAULT;

		// trigger the contact callback for pairs (A,B) where
		// the filter mask of A contains the ID of B and vice versa.
		if ((filterData0.word0 & filterData1.word1) && (filterData1.word0 & filterData0.word1))
		{
			pairFlags |= physx::PxPairFlag::eNOTIFY_TOUCH_FOUND;
		}

		return physx::PxFilterFlag::eDEFAULT;
	}

	static ENGINE_INLINE void _SceneResetEntities(physx::PxScene* scene, physx::PxActorTypeFlags flags)
	{
		ASSERT(scene, "Not exist scene");

		if (!scene) return;

		physx::PxSceneWriteLock lock(*scene, __FUNCTION__, __LINE__);

		PxU32 nActors = scene->getNbActors(flags);
		PxActor** actors = new PxActor*[nActors];
		scene->getActors(flags, actors, nActors);
		scene->removeActors(actors, nActors);
		delete[] actors;
	}

	void PhysScene::Create(PhysSystem*_phSystem)
	{
		ASSERT(_phSystem, "Invalid _phSystem pointer");
		if (!_phSystem) return;
		m_PhysSystem = _phSystem;
		_CreateSceneDesc();
	}

	void PhysScene::CreateWithGetPhysicsPtr() {
		auto ptr = GetPhysics();
		ASSERT(ptr, "Not exist physics pointer");
		if (!ptr) return;

		Create(ptr);
	}

	void PhysScene::Release()
	{
		Clear();
		m_SceneData.Cleanup();
	}

	void PhysScene::_CreateSceneDesc()
	{
		ASSERT(m_PhysSystem, "m_PhysSystem is null");
		ASSERT(m_PhysSystem->GetPxPhysics(), "m_PhysSystem->GetPxPhysics() is null"); //-V595
		if (m_PhysSystem == nullptr || (m_PhysSystem && m_PhysSystem->GetPxPhysics() == nullptr))
			return;

		PxSceneDesc sceneDesc(m_PhysSystem->GetPxPhysics()->getTolerancesScale());
		sceneDesc.gravity = EngineMathToPhysX(CVARManager::m_DefaultGravity);

		ASSERT(m_SceneData.m_PhCallback, "m_SceneData.m_PhCallback is null");

		if (sceneDesc.simulationEventCallback == nullptr)
			sceneDesc.simulationEventCallback = m_SceneData.m_PhCallback;

		// eREQUIRE_RW_LOCK - If we're frame lagging the async scene (truly running it async) then use the scene lock
		// eENABLE_ACTIVETRANSFORMS - We want to use 'active transforms'
		sceneDesc.flags = PxSceneFlag::eENABLE_KINEMATIC_PAIRS | PxSceneFlag::eENABLE_KINEMATIC_STATIC_PAIRS
			| PxSceneFlag::eENABLE_ACTIVE_ACTORS | PxSceneFlag::eREQUIRE_RW_LOCK | PxSceneFlag::eENABLE_CCD | PxSceneFlag::eENABLE_STABILIZATION | PxSceneFlag::eENABLE_ENHANCED_DETERMINISM;

		// Do this to improve loading times, esp. for streaming in sub levels
		sceneDesc.staticStructure = PxPruningStructureType::eDYNAMIC_AABB_TREE;
		sceneDesc.dynamicStructure = PxPruningStructureType::eDYNAMIC_AABB_TREE;

		if (sceneDesc.cpuDispatcher == nullptr)
			sceneDesc.cpuDispatcher = m_PhysSystem->GetPxDefaultCpuDispatcher();

		ASSERT(sceneDesc.cpuDispatcher, "Invalid cpuDispatcher");
		if (sceneDesc.cpuDispatcher == nullptr)
			return;

		m_PhysSystem->CreateCUDADispatcherUtil(&sceneDesc);

		if (!sceneDesc.filterShader)
			sceneDesc.filterShader = SampleFilterShader;

		m_PhysSystem->CreateSceneFromSceneDesc(&sceneDesc, m_SceneData);
	}

	void PhysScene::Clear()
	{
		ASSERT(m_SceneData.m_PhysXScene, "mScene not exist");

		_SceneResetEntities(m_SceneData.m_PhysXScene, PxActorTypeFlag::eRIGID_STATIC | PxActorTypeFlag::eRIGID_DYNAMIC);
	}

	void PhysScene::SceneResetDynamicEntities()
	{
		ASSERT(m_SceneData.m_PhysXScene, "mScene not exist");

		_SceneResetEntities(m_SceneData.m_PhysXScene, PxActorTypeFlag::eRIGID_DYNAMIC);
	}

	void PhysScene::SimulateScene(TimeDelta _delta)
	{
		BROFILER_CATEGORY("UpdatePhysics", Brofiler::Color::Yellow);
		// Sub-step is lower
		if (m_SceneData.m_UpdateMethod == PhysXSceneData::UpdateMethod::UPDATEMETHOD_SUB_STEP)
			_UpdateWithSubStep(_delta);
		else if (m_SceneData.m_UpdateMethod == PhysXSceneData::UpdateMethod::UPDATEMETHOD_DELTA)
			_UpdateWithDelta(_delta);
		else
			ASSERT("Incorrect update method");
	}

	void PhysScene::_UpdateWithDelta(TimeDelta dTime)
	{
		//!@todo ������� ��� �������� � ������
		static const TimeDelta m_fixedDeltaTime = Math::ONEDELTA / TimeDelta(UpdateConfig::Physics);
		static const TimeDelta m_maxDeltaTime = 2 * m_fixedDeltaTime;
		static TimeDelta mAcc = Math::ZERODELTA;
		dTime = Math::Clamp(dTime, 0.0, m_maxDeltaTime);

#ifndef DROP_EDITOR
		GetStatistic()->SetPhysicsDelta(dTime);
#endif

		if (m_fixedDeltaTime != 0.0f)
		{
			mAcc += dTime;

			while (mAcc >= m_fixedDeltaTime)
			{
				m_SceneData.m_PhysXScene->simulate(m_fixedDeltaTime);
				m_SceneData.m_PhysXScene->fetchResults(true);
				mAcc -= m_fixedDeltaTime;
			}
		}
		else
		{
			m_SceneData.m_PhysXScene->simulate(dTime);
			m_SceneData.m_PhysXScene->fetchResults(true);
		}
	}

	void PhysScene::_UpdateWithSubStep(TimeDelta dTime)
	{
#ifndef DROP_EDITOR
		GetStatistic()->SetPhysicsDelta(dTime);
#endif

		static const TimeDelta substepSize = Math::ONEDELTA / TimeDelta(UpdateConfig::Physics);
		static const uint32_t numMaxSubsteps = 8;

		static TimeDelta accumulator = Math::ZERODELTA;
		accumulator += dTime;
		if (accumulator > substepSize*numMaxSubsteps)
			accumulator = substepSize * numMaxSubsteps;

		int numStepsReq = (int)floorf(accumulator / substepSize);
		if (numStepsReq > 1)
		{
			for (unsigned int i = 0; i < numStepsReq - 1; ++i)
			{
				accumulator -= substepSize;
#if VEHICLES_ENABLED
				m_VehicleManager->Update(substepSize);
#endif

				m_SceneData.m_PhysXScene->simulate(substepSize);
				{
					physx::PxSceneWriteLock lock(*m_SceneData.m_PhysXScene, __FUNCTION__, __LINE__);
					m_SceneData.m_PhysXScene->fetchResults(true);
				}
				m_SceneData.m_PhysXScene->flushQueryUpdates();
			}
		}
		if (accumulator >= substepSize)
		{
			accumulator -= substepSize;
#if VEHICLES_ENABLED
			m_VehicleManager->Update(substepSize);
#endif

			{
				physx::PxSceneWriteLock lock(*m_SceneData.m_PhysXScene, __FUNCTION__, __LINE__);
				m_SceneData.m_PhysXScene->simulate(substepSize);
			}
			{
				physx::PxSceneReadLock lock(*m_SceneData.m_PhysXScene, __FUNCTION__, __LINE__);
				m_SceneData.m_PhysXScene->fetchResults(true);
			}
		}
	}

	void PhysScene::InsertPhActorToCollisionCallback(const PhysBodyDescSh& _desc)
	{
		ASSERT(m_SceneData.m_PhCallback, "Invalid pointer on m_SceneData.m_PhCallback");
		if (!m_SceneData.m_PhCallback) return;
		m_SceneData.m_PhCallback->Insert(_desc);
	}

	void PhysScene::SetGravity(const Vec3&_vec)
	{
		ASSERT(m_SceneData.m_PhysXScene, "Not exist physics scene");
		if (!m_SceneData.m_PhysXScene) return;

		physx::PxSceneWriteLock lock(*m_SceneData.m_PhysXScene, __FUNCTION__, __LINE__);
		m_SceneData.m_PhysXScene->setGravity(PxVec3(_vec.x, _vec.y, _vec.z));
	}

	Vec3 PhysScene::GetGravity() {
		ASSERT(m_SceneData.m_PhysXScene, "Invalid pointer on m_SceneData.m_PhysXScene");
		if (m_SceneData.m_PhysXScene == nullptr)
			return Vec3::ZERO;

		return PhysXToEngineMath(m_SceneData.m_PhysXScene->getGravity());
	}
}