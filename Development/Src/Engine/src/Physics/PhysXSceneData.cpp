#include "EnginePrivate.h"

//***************************************************************************
#include "PxPhysicsAPI.h"
//***************************************************************************
#include "PhysXSceneData.h"
#include "PhysXCallbacks.h"
//***************************************************************************

namespace NGTech
{
	PhysXSceneData::PhysXSceneData(const PhysXSceneData& _scene)
	{
		this->m_PhysXScene = _scene.m_PhysXScene;
		this->m_ControllerManager = _scene.m_ControllerManager;
		this->m_PhCallback = _scene.m_PhCallback;
		this->m_Vehicle = _scene.m_Vehicle;
	}

	PhysXSceneData::~PhysXSceneData() {
		Cleanup();
	}

	void PhysXSceneData::Cleanup()
	{
		SAFE_DELETE(m_PhCallback);
		//!@todo FIX IT
#if 0//crashes
		// crash		SAFE_rELEASE(m_ControllerManager);
// crash		SAFE_rELEASE(m_PhysXScene);
		// ���������� ��������������� �� �� ������
#else
#endif
	}

	PhysXSceneData& PhysXSceneData::operator=(const PhysXSceneData&_other)
	{
		if (this != &_other) {
			this->m_PhysXScene = _other.m_PhysXScene;
			this->m_ControllerManager = _other.m_ControllerManager;
			this->m_PhCallback = _other.m_PhCallback;
			this->m_Vehicle = _other.m_Vehicle;
		}

		return *this;
	}

	PhysXSceneData::PhysXSceneData()
	{
		m_PhCallback = new PhysXCallback();
	}
}