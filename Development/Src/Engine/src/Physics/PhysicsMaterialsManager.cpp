/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"

//***************************************************************************
#include "PhysSystem.h"
#include "PhysBody.h"
#include "EngineMathToPhysx.inl"
#include "PhysXCallbacks.h"
#include "PhysXMaterialSh.h"
#include "../inc/Physics/VehicleRaycast.h"
//***************************************************************************
#include "PxPhysicsAPI.h"
#include "PxPhysXConfig.h"
#include "PxFiltering.h"
#include "foundation/PxMemory.h"
#include "PsThread.h"

#include "extensions/PxDefaultStreams.h"

#include "PxPvd.h"
#include "cudamanager/PxCudaContextManager.h"
//***************************************************************************
#include "../../inc/Physics/VehicleTireFriction.h"
#include "../../inc/PhysicsMaterialsManager.h"
#include "../../inc/PhysXMaterialSh.h"
//***************************************************************************

namespace NGTech {
	ENGINE_API PhysicsMaterialsManager phMatManager;

	/**
	Creation using PhysX was moved in defferent function for exception safe
	*/
	static void _CreateMaterialUtil(physx::PxPhysics*_physics, PhysXMaterialSh*_to)
	{
		ASSERT(_physics, "Provided invalid pointer on physx::PxPhysics _physics");
		ASSERT(_to, "Provided invalid ref on PhysXMaterialSh _to");
		if (!_physics || !_to)
			return;

		_to->SetPxMaterial(_physics->createMaterial(_to->GetStaticFriction() * PhysXMaterialSh::frictionCoef, _to->GetDynamicFriction() * PhysXMaterialSh::frictionCoef, _to->GetRestitution()));
		_to->SetCombineModeMultiplyFriction();
		_to->SetCombineModeMultiplyRestitution();
	}

	/**
	*/
	static ENGINE_INLINE String GetMaterialKey(float staticFriction, float dynamicFriction, float restitution, const String& materialName,
		bool vehicleDrivableSurface)
	{
		std::ostringstream ss;
		ss << staticFriction << " " << dynamicFriction << " " << restitution << " " << materialName << " " <<
			(vehicleDrivableSurface ? "T" : "F");
		return ss.str();
	}

	void PhysicsMaterialsManager::Initialise(physx::PxPhysics* mPhysics) {
		mMaterial = AllocMaterial(mPhysics, Math::ONEDELTA, Math::ONEDELTA, Math::ONEDELTA, "default.phmat", true);
		ASSERT(mMaterial, "createMaterial failed!");
	}

	void PhysicsMaterialsManager::DeInitialise() {
		SAFE_RELEASE(mMaterial);
	}

	PhysXMaterialSh* PhysicsMaterialsManager::AllocMaterial
	(
		physx::PxPhysics* mPhysics,
		TimeDelta staticFriction, TimeDelta dynamicFriction, TimeDelta restitution,
		const String& materialName,
		bool vehicleDrivableSurface)
	{
		ASSERT(mPhysics, "Can't use this function, because pointer on mPhysics is invalid");
		if (!mPhysics)
			return nullptr;

		String key = GetMaterialKey(staticFriction, dynamicFriction, restitution, materialName, vehicleDrivableSurface);

		PhysXMaterialSh* material = nullptr;
		MaterialMap::iterator it = materials.find(key);

		if (it == materials.end()) {
			material = new PhysXMaterialSh(staticFriction, dynamicFriction, restitution, materialName,
				vehicleDrivableSurface, key);

			_CreateMaterialUtil(mPhysics, material);

			materials[key] = material;

			if (material->IsVehicleDrivableSurface())
			{
				vehicleDrivableMaterials.insert(material);
				_IncrementVehicleDrivableMaterialsVersion();
			}
		}
		else
			material = it->second;

		ASSERT(material, "Failed creation or finded value is NULL");

		return material;
	}

	PhysXMaterialSh * PhysicsMaterialsManager::AllocMaterial(TimeDelta staticFriction, TimeDelta dynamicFriction, TimeDelta restitution, const String & materialName, bool vehicleDrivableSurface)
	{
		return AllocMaterial(GetPhysics()->GetPxPhysics(), staticFriction, dynamicFriction, restitution, materialName, vehicleDrivableSurface);
	}

	void PhysicsMaterialsManager::FreeMaterial(PhysXMaterialSh* material)
	{
		ASSERT(material, "Invalid pointer");

		if (material->IsVehicleDrivableSurface())
		{
			vehicleDrivableMaterials.erase(material);
			_DecrementVehicleDrivableMaterialsVersion();
		}

		SAFE_RELEASE(material);

		if (material->RefCounter() == 0)
		{
			MaterialMap::iterator it = materials.find(material->GetMaterialsMapKey());
			if (it == materials.end())
				ASSERT(NULL, "PhysXWorld: FreeMaterial: it == materials.end().");
			else
				materials.erase(it);
		}
	}

	void PhysicsMaterialsManager::_IncrementVehicleDrivableMaterialsVersion()
	{
		vehicleDrivableMaterialsVersionCounter++;
		if (vehicleDrivableMaterialsVersionCounter >= 2100000000)
			vehicleDrivableMaterialsVersionCounter = 1;
	}

	void PhysicsMaterialsManager::_DecrementVehicleDrivableMaterialsVersion()
	{
		vehicleDrivableMaterialsVersionCounter--;
		if (vehicleDrivableMaterialsVersionCounter < 0)
			vehicleDrivableMaterialsVersionCounter = 0;
	}

	PhysXMaterialSh* PhysicsMaterialsManager::GetDefaultMaterial() const {
		ASSERT(mMaterial, "Not exist mMaterial");
		if (!mMaterial) return nullptr;

		mMaterial->AddRef();
		return mMaterial;
	}

	PhysXMaterialSh* PhysicsMaterialsManager::CheckMatOnValid(PhysXMaterialSh*_ptr) {
		if (_ptr == nullptr)
			return phMatManager.GetDefaultMaterial();

		return _ptr;
	}

	physx::PxMaterial* PhysicsMaterialsManager::CheckPxMatOnValid(PhysXMaterialSh*_ptr) {
		if ((_ptr == nullptr) || (_ptr && _ptr->GetPxMaterial() == nullptr))
			return phMatManager.GetDefaultMaterial()->GetPxMaterial();

		return _ptr->GetPxMaterial();
	}
}