#include "EnginePrivate.h"

#include "../../inc/Physics/VehicleDescSh.h"
#include "PhysXMaterialSh.h"

namespace NGTech
{
	void VehicleDescSh::CleanResources() {
		SAFE_RELEASE(chassisMaterialTmp);
		SAFE_RELEASE(wheelMaterialTmp);
	}
}