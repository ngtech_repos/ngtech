/*
 * Copyright (c) 2008-2015, NVIDIA CORPORATION.  All rights reserved.
 *
 * NVIDIA CORPORATION and its licensors retain all intellectual property
 * and proprietary rights in and to this software, related documentation
 * and any modifications thereto.  Any use, reproduction, disclosure or
 * distribution of this software and related documentation without an express
 * license agreement from NVIDIA CORPORATION is strictly prohibited.
 */
 // Copyright (c) 2004-2008 AGEIA Technologies, Inc. All rights reserved.
 // Copyright (c) 2001-2004 NovodeX AG. All rights reserved.

#include "EnginePrivate.h"

#include "../../inc/Physics/VehicleCreate.h"
#include "../../inc/Physics/VehicleTireFriction.h"
#include "../../inc/Physics/VehicleRaycast.h"
#include "../../inc/EngineMathToPhysx.inl"

#include "../../inc/PhysicsMaterialsManager.h"

namespace NGTech
{
	namespace NoDrive
	{
		static constexpr ENGINE_INLINE void _ComputeWheelCenterActorOffsets
		(const PxF32 wheelFrontZ, const PxF32 wheelRearZ, const PxVec3& chassisDims, const PxF32 wheelWidth, const PxF32 wheelRadius, const PxU32 numWheels, PxVec3* wheelCentreOffsets)
		{
			ASSERT(wheelCentreOffsets, "Invalid pointer on wheelCentreOffsets");
			if (!wheelCentreOffsets) return;

			//chassisDims.z is the distance from the rear of the chassis to the front of the chassis.
			//The front has z = 0.5*chassisDims.z and the rear has z = -0.5*chassisDims.z.
			//Compute a position for the front wheel and the rear wheel along the z-axis.
			//Compute the separation between each wheel along the z-axis.
			const PxF32 numLeftWheels = numWheels / 2.0f;
			const PxF32 deltaZ = (wheelFrontZ - wheelRearZ) / (numLeftWheels - 1.0f);
			//Set the outside of the left and right wheels to be flush with the chassis.
			//Set the top of the wheel to be just touching the underside of the chassis.
			for (PxU32 i = 0; i < numWheels; i += 2)
			{
				//Left wheel offset from origin.
				wheelCentreOffsets[i + 0] = PxVec3((-chassisDims.x + wheelWidth)*0.5f, -(chassisDims.y / 2 + wheelRadius), wheelRearZ + i * deltaZ*0.5f);
				//Right wheel offsets from origin.
				wheelCentreOffsets[i + 1] = PxVec3((+chassisDims.x - wheelWidth)*0.5f, -(chassisDims.y / 2 + wheelRadius), wheelRearZ + i * deltaZ*0.5f);
			}
		}

		static ENGINE_INLINE void _SetupWheelsSimulationData
		(const PxF32 wheelMass, const PxF32 wheelMOI, const PxF32 wheelRadius, const PxF32 wheelWidth,
			const PxU32 numWheels, const PxVec3* wheelCenterActorOffsets,
			const PxVec3& chassisCMOffset, const PxF32 chassisMass,
			PxVehicleWheelsSimData* wheelsSimData)
		{
			ASSERT(wheelsSimData, "invalid pointer on wheelsSimData");
			if (!wheelsSimData) return;

			//Set up the wheels.
			PxVehicleWheelData wheels[PX_MAX_NB_WHEELS];
			{
				//Set up the wheel data structures with mass, moi, radius, width.
				for (PxU32 i = 0; i < numWheels; i++)
				{
					wheels[i].mMass = wheelMass;
					wheels[i].mMOI = wheelMOI;
					wheels[i].mRadius = wheelRadius;
					wheels[i].mWidth = wheelWidth;
				}
			}

			//Set up the tires.
			PxVehicleTireData tires[PX_MAX_NB_WHEELS];
			{
				//Set up the tires.
				for (PxU32 i = 0; i < numWheels; i++)
				{
					tires[i].mType = VehicleUtils::TIRE_TYPE_NORMAL;
				}
			}

			//Set up the suspensions
			PxVehicleSuspensionData suspensions[PX_MAX_NB_WHEELS];
			{
				//Compute the mass supported by each suspension spring.
				PxF32 suspSprungMasses[PX_MAX_NB_WHEELS];
				PxVehicleComputeSprungMasses(numWheels, wheelCenterActorOffsets, chassisCMOffset, chassisMass, 1, suspSprungMasses);

				//Set the suspension data.
				for (PxU32 i = 0; i < numWheels; i++)
				{
					suspensions[i].mMaxCompression = 0.3f;
					suspensions[i].mMaxDroop = 0.1f;
					suspensions[i].mSpringStrength = 35000.0f;
					suspensions[i].mSpringDamperRate = 4500.0f;
					suspensions[i].mSprungMass = suspSprungMasses[i];
				}

				//Set the camber angles.
				const PxF32 camberAngleAtRest = 0.0;
				const PxF32 camberAngleAtMaxDroop = 0.01f;
				const PxF32 camberAngleAtMaxCompression = -0.01f;
				for (PxU32 i = 0; i < numWheels; i += 2)
				{
					suspensions[i + 0].mCamberAtRest = camberAngleAtRest;
					suspensions[i + 1].mCamberAtRest = -camberAngleAtRest;
					suspensions[i + 0].mCamberAtMaxDroop = camberAngleAtMaxDroop;
					suspensions[i + 1].mCamberAtMaxDroop = -camberAngleAtMaxDroop;
					suspensions[i + 0].mCamberAtMaxCompression = camberAngleAtMaxCompression;
					suspensions[i + 1].mCamberAtMaxCompression = -camberAngleAtMaxCompression;
				}
			}

			//Set up the wheel geometry.
			PxVec3 suspTravelDirections[PX_MAX_NB_WHEELS];
			PxVec3 wheelCentreCMOffsets[PX_MAX_NB_WHEELS];
			PxVec3 suspForceAppCMOffsets[PX_MAX_NB_WHEELS];
			PxVec3 tireForceAppCMOffsets[PX_MAX_NB_WHEELS];
			{
				//Set the geometry data.
				for (PxU32 i = 0; i < numWheels; i++)
				{
					//Vertical suspension travel.
					suspTravelDirections[i] = PxVec3(0, -1, 0);

					//Wheel center offset is offset from rigid body center of mass.
					wheelCentreCMOffsets[i] = wheelCenterActorOffsets[i] - chassisCMOffset;

					//Suspension force application point 0.3 metres below rigid body center of mass.
					suspForceAppCMOffsets[i] = PxVec3(wheelCentreCMOffsets[i].x, -0.3f, wheelCentreCMOffsets[i].z);

					//Tire force application point 0.3 metres below rigid body center of mass.
					tireForceAppCMOffsets[i] = PxVec3(wheelCentreCMOffsets[i].x, -0.3f, wheelCentreCMOffsets[i].z);
				}
			}

			//Set up the filter data of the raycast that will be issued by each suspension.
			PxFilterData qryFilterData;
			VehicleUtils::SetupNonDrivableSurfaceUtil(qryFilterData);

			//Set the wheel, tire and suspension data.
			//Set the geometry data.
			//Set the query filter data
			for (PxU32 i = 0; i < numWheels; i++)
			{
				wheelsSimData->setWheelData(i, wheels[i]);
				wheelsSimData->setTireData(i, tires[i]);
				wheelsSimData->setSuspensionData(i, suspensions[i]);
				wheelsSimData->setSuspTravelDirection(i, suspTravelDirections[i]);
				wheelsSimData->setWheelCentreOffset(i, wheelCentreCMOffsets[i]);
				wheelsSimData->setSuspForceAppPointOffset(i, suspForceAppCMOffsets[i]);
				wheelsSimData->setTireForceAppPointOffset(i, tireForceAppCMOffsets[i]);
				wheelsSimData->setSceneQueryFilterData(i, qryFilterData);
				wheelsSimData->setWheelShapeMapping(i, i);
			}
		}
	}// namespace NoDrive

	PxVehicleNoDrive* VehicleUtils::CreateVehicleNoDrive(const VehicleDescSh& vehicleDesc, PxPhysics* physics, PxCooking* cooking)
	{
		ASSERT(physics, "Invalid pointer on physics");
		if (!physics) return nullptr;
		ASSERT(cooking, "Invalid pointer on cooking");
		if (!cooking) return nullptr;

		const PxVec3 chassisDims = EngineMathToPhysX(vehicleDesc.chassisDims);
		const PxF32 wheelWidth = vehicleDesc.wheelWidth;
		const PxF32 wheelRadius = vehicleDesc.wheelRadius;
		const PxU32 numWheels = vehicleDesc.numWheels;

		//Construct a physx actor with shapes for the chassis and wheels.
		//Set the rigid body mass, moment of inertia, and center of mass offset.
		PxRigidDynamic* vehActor = NULL;
		{
			//Construct a convex mesh for a cylindrical wheel.
			PxConvexMesh* wheelMesh = CreateWheelMeshUtil(wheelWidth, wheelRadius, physics, cooking);
			ASSERT(wheelMesh, "invalid pointer on wheelMesh");
			if (!wheelMesh) return nullptr;

			//Assume all wheels are identical for simplicity.
			PxConvexMesh* wheelConvexMeshes[PX_MAX_NB_WHEELS];
			PxMaterial* wheelMaterials[PX_MAX_NB_WHEELS];

			//Set the meshes and materials for the driven wheels.
			for (PxU32 i = 0; i < numWheels; i++)
			{
				wheelConvexMeshes[i] = wheelMesh;
				wheelMaterials[i] = PhysicsMaterialsManager::CheckPxMatOnValid(vehicleDesc.wheelMaterialTmp);
			}

			//Chassis just has a single convex shape for simplicity.
			PxConvexMesh* chassisConvexMesh = CreateChassisMeshUtil(vehicleDesc.chassisDims, physics, cooking);
			PxConvexMesh* chassisConvexMeshes[1] = { chassisConvexMesh };
			PxMaterial* chassisMaterials[1] = { PhysicsMaterialsManager::CheckPxMatOnValid(vehicleDesc.chassisMaterialTmp) };

			//Rigid body data.
			PxVehicleChassisData rigidBodyData;
			rigidBodyData.mMOI = EngineMathToPhysX(vehicleDesc.chassisMOI);
			rigidBodyData.mMass = vehicleDesc.chassisMass;
			rigidBodyData.mCMOffset = EngineMathToPhysX(vehicleDesc.chassisCMOffset);

			vehActor = CreateVehicleActorUtil
			(rigidBodyData,
				wheelMaterials, wheelConvexMeshes, numWheels,
				chassisMaterials, chassisConvexMeshes, 1,
				physics);

			ASSERT(vehActor, "Failed creation vehActor");
			if (!vehActor) return nullptr;
		}

		//Set up the sim data for the wheels.
		PxVehicleWheelsSimData* wheelsSimData = PxVehicleWheelsSimData::allocate(numWheels);
		ASSERT(wheelsSimData, "invalid pointer on wheelsSimData");
		if (!wheelsSimData) return nullptr;

		{
			//Compute the wheel center offsets from the origin.
			PxVec3 wheelCentreActorOffsets[PX_MAX_NB_WHEELS];
			const PxF32 frontZ = chassisDims.z*0.3f;
			const PxF32 rearZ = -chassisDims.z*0.3f;
			NoDrive::_ComputeWheelCenterActorOffsets(frontZ, rearZ, chassisDims, wheelWidth, wheelRadius, numWheels, wheelCentreActorOffsets);

			NoDrive::_SetupWheelsSimulationData
			(vehicleDesc.wheelMass, vehicleDesc.wheelMOI, wheelRadius, wheelWidth,
				numWheels, wheelCentreActorOffsets,
				EngineMathToPhysX(vehicleDesc.chassisCMOffset), vehicleDesc.chassisMass,
				wheelsSimData);
		}

		//Create a vehicle from the wheels and drive sim data.
		PxVehicleNoDrive* vehDriveNoDrive = PxVehicleNoDrive::allocate(numWheels);
		ASSERT(vehDriveNoDrive, "Invalid pointer on vehDriveNoDrive");
		if (!vehDriveNoDrive) return nullptr;

		vehDriveNoDrive->setup(physics, vehActor, *wheelsSimData);

		//Free the sim data because we don't need that any more.
		wheelsSimData->free();

		return vehDriveNoDrive;
	}
}