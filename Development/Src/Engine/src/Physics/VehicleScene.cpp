#include "EnginePrivate.h"

//***************************************************************************
#include "../../inc/PhysSystem.h"
#include "../../inc/PhysXMaterialSh.h"
#include "../../inc/PhysicsMaterialsManager.h"
//***************************************************************************
#include "../../inc/Physics/VehicleScene.h"
#include "../../inc/Physics/VehicleRaycast.h"
#include "../../inc/Physics/VehicleTireFriction.h"
#include "../../inc/Physics/VehicleCreate.h"
#include "../../inc/PhysXSceneData.h"
//***************************************************************************

namespace NGTech
{
	extern PxDefaultAllocator gDefaultAllocatorCallback;

	void VehicleScene::_Init()
	{
		ASSERT(mSceneData, "mSceneData is not exist");

		//Create the batched scene queries for the suspension raycasts.
		mVehicleSceneQueryData = VehicleSceneQueryData::Allocate(1, PX_MAX_NB_WHEELS, 1, gDefaultAllocatorCallback);

		ASSERT(mVehicleSceneQueryData, "mVehicleSceneQueryData is nullptr");
		if (!mVehicleSceneQueryData) return;

		mBatchQuery = VehicleSceneQueryData::SetUpBatchedSceneQuery(0, *mVehicleSceneQueryData, mSceneData->GetPxScene());

		ASSERT(mBatchQuery, "mBatchQuery is nullptr");
		if (!mBatchQuery) return;

		auto mMaterial = phMatManager.GetDefaultMaterial()->GetPxMaterial();

		// TODO: �������� ����� ����� ��������� ��������
		ASSERT(mMaterial, "mMaterial is null");
		if (!mMaterial) return;

		mFrictionPairs = VehicleUtils::CreateFrictionPairsUtil(mMaterial);

		ASSERT(mFrictionPairs, "mFrictionPairs is nullptr");
		if (!mFrictionPairs) return;

		mInited = true;
	}

	void VehicleScene::_Release()
	{
		// ���� ������������� �� ���� �����������, ������ ������� ���
		if (mInited == false)
			return;

		ASSERT(mInited, "mInited is false");

		SAFE_rELEASE(mBatchQuery);

		if (mVehicleSceneQueryData)
			mVehicleSceneQueryData->Free(gDefaultAllocatorCallback);

		SAFE_rELEASE(mFrictionPairs);
	}
}