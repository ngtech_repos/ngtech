/*
 * Copyright (c) 2008-2015, NVIDIA CORPORATION.  All rights reserved.
 *
 * NVIDIA CORPORATION and its licensors retain all intellectual property
 * and proprietary rights in and to this software, related documentation
 * and any modifications thereto.  Any use, reproduction, disclosure or
 * distribution of this software and related documentation without an express
 * license agreement from NVIDIA CORPORATION is strictly prohibited.
 */
 // Copyright (c) 2004-2008 AGEIA Technologies, Inc. All rights reserved.
 // Copyright (c) 2001-2004 NovodeX AG. All rights reserved.

#include "EnginePrivate.h"

#include "../../inc/Physics/VehicleCreate.h"
#include "../../inc/Physics/VehicleTireFriction.h"
#include "../../inc/Physics/VehicleRaycast.h"
#include "../../inc/EngineMathToPhysx.inl"

#include "../../inc/PhysicsMaterialsManager.h"

namespace NGTech
{
	namespace Tank
	{
		static constexpr ENGINE_INLINE void _ComputeWheelCenterActorOffsets
		(const PxF32 wheelFrontZ, const PxF32 wheelRearZ, const PxVec3& chassisDims, const PxF32 wheelWidth, const PxF32 wheelRadius, const PxU32 numWheels, PxVec3* wheelCentreOffsets)
		{
			//chassisDims.z is the distance from the rear of the chassis to the front of the chassis.
			//The front has z = 0.5*chassisDims.z and the rear has z = -0.5*chassisDims.z.
			//Compute a position for the front wheel and the rear wheel along the z-axis.
			//Compute the separation between each wheel along the z-axis.
			const PxF32 numLeftWheels = numWheels / 2.0f;
			const PxF32 deltaZ = (wheelFrontZ - wheelRearZ) / (numLeftWheels - 1.0f);
			//Set the outside of the left and right wheels to be flush with the chassis.
			//Set the top of the wheel to be just touching the underside of the chassis.
			for (PxU32 i = 0; i < numWheels; i += 2)
			{
				//Left wheel offset from origin.
				wheelCentreOffsets[i + 0] = PxVec3((-chassisDims.x + wheelWidth)*0.5f, -(chassisDims.y / 2 + wheelRadius), wheelRearZ + i * deltaZ*0.5f);
				//Right wheel offsets from origin.
				wheelCentreOffsets[i + 1] = PxVec3((+chassisDims.x - wheelWidth)*0.5f, -(chassisDims.y / 2 + wheelRadius), wheelRearZ + i * deltaZ*0.5f);
			}
		}

		static ENGINE_INLINE void _SetupWheelsSimulationData
		(const PxF32 wheelMass, const PxF32 wheelMOI, const PxF32 wheelRadius, const PxF32 wheelWidth,
			const PxU32 numWheels, const PxVec3* wheelCenterActorOffsets,
			const PxVec3& chassisCMOffset, const PxF32 chassisMass,
			PxVehicleWheelsSimData* wheelsSimData)
		{
			//Set up the wheels.
			PxVehicleWheelData wheels[PX_MAX_NB_WHEELS];
			{
				//Set up the wheel data structures with mass, moi, radius, width.
				//Increase the damping on the wheel.
				for (PxU32 i = 0; i < numWheels; i++)
				{
					wheels[i].mMass = wheelMass;
					wheels[i].mMOI = wheelMOI;
					wheels[i].mRadius = wheelRadius;
					wheels[i].mWidth = wheelWidth;
					wheels[i].mDampingRate = 2.0f;
				}
			}

			//Set up the tires.
			PxVehicleTireData tires[PX_MAX_NB_WHEELS];
			{
				//Set all tire types to "normal" type.
				for (PxU32 i = 0; i < numWheels; i++)
				{
					tires[i].mType = VehicleUtils::TIRE_TYPE_NORMAL;
				}
			}

			//Set up the suspensions
			PxVehicleSuspensionData suspensions[PX_MAX_NB_WHEELS];
			{
				//Compute the mass supported by each suspension spring.
				PxF32 suspSprungMasses[PX_MAX_NB_WHEELS];
				PxVehicleComputeSprungMasses(numWheels, wheelCenterActorOffsets, chassisCMOffset, chassisMass, 1, suspSprungMasses);

				//Set the suspension data.
				for (PxU32 i = 0; i < numWheels; i++)
				{
					suspensions[i].mMaxCompression = 0.3f;
					suspensions[i].mMaxDroop = 0.1f;
					suspensions[i].mSpringStrength = 10000.0f;
					suspensions[i].mSpringDamperRate = 1500.0f;
					suspensions[i].mSprungMass = suspSprungMasses[i];
				}
			}

			//Set up the wheel geometry.
			PxVec3 suspTravelDirections[PX_MAX_NB_WHEELS];
			PxVec3 wheelCentreCMOffsets[PX_MAX_NB_WHEELS];
			PxVec3 suspForceAppCMOffsets[PX_MAX_NB_WHEELS];
			PxVec3 tireForceAppCMOffsets[PX_MAX_NB_WHEELS];
			{
				for (PxU32 i = 0; i < numWheels; i++)
				{
					//Vertical suspension travel.
					suspTravelDirections[i] = PxVec3(0, -1, 0);

					//Wheel center offset is offset from rigid body center of mass.
					wheelCentreCMOffsets[i] = wheelCenterActorOffsets[i] - chassisCMOffset;

					//Suspension force application point 0.3 metres below rigid body center of mass.
					suspForceAppCMOffsets[i] = PxVec3(wheelCentreCMOffsets[i].x, -0.3f, wheelCentreCMOffsets[i].z);

					//Tire force application point 0.3 metres below rigid body center of mass.
					tireForceAppCMOffsets[i] = PxVec3(wheelCentreCMOffsets[i].x, -0.3f, wheelCentreCMOffsets[i].z);
				}
			}

			//Set up the filter data of the raycast that will be issued by each suspension.
			PxFilterData qryFilterData;
			VehicleUtils::SetupNonDrivableSurfaceUtil(qryFilterData);

			//Set the wheel, tire and suspension data.
			//Set the geometry data.
			//Set the query filter data
			for (PxU32 i = 0; i < numWheels; i++)
			{
				wheelsSimData->setWheelData(i, wheels[i]);
				wheelsSimData->setTireData(i, tires[i]);
				wheelsSimData->setSuspensionData(i, suspensions[i]);
				wheelsSimData->setSuspTravelDirection(i, suspTravelDirections[i]);
				wheelsSimData->setWheelCentreOffset(i, wheelCentreCMOffsets[i]);
				wheelsSimData->setSuspForceAppPointOffset(i, suspForceAppCMOffsets[i]);
				wheelsSimData->setTireForceAppPointOffset(i, tireForceAppCMOffsets[i]);
				wheelsSimData->setSceneQueryFilterData(i, qryFilterData);
				wheelsSimData->setWheelShapeMapping(i, i);
			}
		}
	}// namespace tank

	PxVehicleDriveTank* VehicleUtils::CreateVehicleTank(const VehicleDescSh& tankDesc, PxPhysics* physics, PxCooking* cooking)
	{
		ASSERT(physics, "invalid pointer on physics");
		if (!physics) return nullptr;

		ASSERT(cooking, "invalid pointer on cooking");
		if (!cooking) return nullptr;

		using namespace Tank;

		const PxVec3 chassisDims = EngineMathToPhysX(tankDesc.chassisDims);
		const PxF32 wheelWidth = tankDesc.wheelWidth;
		const PxF32 wheelRadius = tankDesc.wheelRadius;
		const PxU32 numWheels = tankDesc.numWheels;

		//Construct a physx actor with shapes for the chassis and wheels.
		//Set the rigid body mass, moment of inertia, and center of mass offset.
		PxRigidDynamic* tankActor = NULL;
		{
			//Construct a convex mesh for a cylindrical wheel.
			PxConvexMesh* wheelMesh = VehicleUtils::CreateWheelMeshUtil(wheelWidth, wheelRadius, physics, cooking);
			//Assume all wheels are identical for simplicity.
			PxConvexMesh* wheelConvexMeshes[PX_MAX_NB_WHEELS];
			PxMaterial* wheelMaterials[PX_MAX_NB_WHEELS];
			for (PxU32 i = 0; i < numWheels; i++)
			{
				wheelConvexMeshes[i] = wheelMesh;
				wheelMaterials[i] = PhysicsMaterialsManager::CheckPxMatOnValid(tankDesc.wheelMaterialTmp);
			}

			//Chassis just has a single convex shape for simplicity.
			PxConvexMesh* chassisConvexMesh = VehicleUtils::CreateChassisMeshUtil(tankDesc.chassisDims, physics, cooking);
			PxConvexMesh* chassisConvexMeshes[1] = { chassisConvexMesh };
			PxMaterial* chassisMaterials[1] = { PhysicsMaterialsManager::CheckPxMatOnValid(tankDesc.chassisMaterialTmp) };

			//Rigid body data.
			PxVehicleChassisData rigidBodyData;
			rigidBodyData.mMOI = EngineMathToPhysX(tankDesc.chassisMOI);
			rigidBodyData.mMass = tankDesc.chassisMass;
			rigidBodyData.mCMOffset = EngineMathToPhysX(tankDesc.chassisCMOffset);

			tankActor = VehicleUtils::CreateVehicleActorUtil
			(rigidBodyData,
				wheelMaterials, wheelConvexMeshes, numWheels,
				chassisMaterials, chassisConvexMeshes, 1,
				physics);
		}

		//Set up the sim data for the wheels.
		PxVehicleWheelsSimData* wheelsSimData = PxVehicleWheelsSimData::allocate(numWheels);
		{
			//Compute the wheel center offsets from the origin.
			PxVec3 wheelCentreActorOffsets[PX_MAX_NB_WHEELS];
			const PxF32 frontZ = chassisDims.z*0.35f;
			const PxF32 rearZ = -chassisDims.z*0.35f;
			_ComputeWheelCenterActorOffsets(frontZ, rearZ, chassisDims, wheelWidth, wheelRadius, numWheels, wheelCentreActorOffsets);

			_SetupWheelsSimulationData
			(tankDesc.wheelMass, tankDesc.wheelMOI, wheelRadius, wheelWidth,
				numWheels, wheelCentreActorOffsets,
				EngineMathToPhysX(tankDesc.chassisCMOffset), tankDesc.chassisMass,
				wheelsSimData);
		}

		//Set up the sim data for the tank drive model.
		PxVehicleDriveSimData driveSimData;
		{
			//Set up the engine to be more powerful but also more damped than the default engine.
			PxVehicleEngineData engineData = driveSimData.getEngineData();
			engineData.mPeakTorque *= 2.0f;
			engineData.mDampingRateZeroThrottleClutchEngaged = 2.0f;
			engineData.mDampingRateZeroThrottleClutchDisengaged = 0.5f;
			engineData.mDampingRateFullThrottle = 0.5f;
			driveSimData.setEngineData(engineData);
		}

		//Create a tank from the wheels and drive sim data.
		PxVehicleDriveTank* vehDriveTank = PxVehicleDriveTank::allocate(numWheels);
		vehDriveTank->setup(physics, tankActor, *wheelsSimData, driveSimData, numWheels);

		//Free the sim data because we don't need that any more.
		wheelsSimData->free();

		return vehDriveTank;
	}
}