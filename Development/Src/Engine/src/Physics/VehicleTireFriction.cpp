/*
* Copyright (c) 2008-2015, NVIDIA CORPORATION.  All rights reserved.
*
* NVIDIA CORPORATION and its licensors retain all intellectual property
* and proprietary rights in and to this software, related documentation
* and any modifications thereto.  Any use, reproduction, disclosure or
* distribution of this software and related documentation without an express
* license agreement from NVIDIA CORPORATION is strictly prohibited.
*/
// Copyright (c) 2004-2008 AGEIA Technologies, Inc. All rights reserved.
// Copyright (c) 2001-2004 NovodeX AG. All rights reserved.

#include "EnginePrivate.h"

#include "../../inc/Physics/VehicleTireFriction.h"
#include "../../inc/Physics/VehicleCreate.h"

namespace NGTech
{
	using namespace physx;

	//Tire model friction for each combination of drivable surface type and tire type.
	static PxF32 gTireFrictionMultipliers[VehicleUtils::MAX_NUM_SURFACE_TYPES][VehicleUtils::MAX_NUM_TIRE_TYPES] =
	{
		//NORMAL,	WORN
		{ 1.00f,		0.1f }//TARMAC
	};

	PxVehicleDrivableSurfaceToTireFrictionPairs* VehicleUtils::CreateFrictionPairsUtil(const PxMaterial* defaultMaterial)
	{
		ASSERT(defaultMaterial, "invalid pointer on defaultMaterial");
		if (!defaultMaterial) return nullptr;

		PxVehicleDrivableSurfaceType surfaceTypes[1];
		surfaceTypes[0].mType = SURFACE_TYPE_TARMAC;

		const PxMaterial* surfaceMaterials[1];
		surfaceMaterials[0] = defaultMaterial;

		PxVehicleDrivableSurfaceToTireFrictionPairs* surfaceTirePairs =
			PxVehicleDrivableSurfaceToTireFrictionPairs::allocate(VehicleUtils::MAX_NUM_TIRE_TYPES, VehicleUtils::MAX_NUM_SURFACE_TYPES);

		ASSERT(surfaceTirePairs, "invalid pointer on surfaceTirePairs");
		if (!surfaceTirePairs) return nullptr;

		surfaceTirePairs->setup(VehicleUtils::MAX_NUM_TIRE_TYPES, VehicleUtils::MAX_NUM_SURFACE_TYPES, surfaceMaterials, surfaceTypes);

		for (PxU32 i = 0; i < VehicleUtils::MAX_NUM_SURFACE_TYPES; i++)
		{
			for (PxU32 j = 0; j < VehicleUtils::MAX_NUM_TIRE_TYPES; j++)
			{
				surfaceTirePairs->setTypePairFriction(i, j, gTireFrictionMultipliers[i][j]);
			}
		}
		return surfaceTirePairs;
	}
}