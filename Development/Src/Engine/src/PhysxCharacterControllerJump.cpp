/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
#if !DISABLE_PHYSICS
//***************************************************************************
#include "PxPhysicsAPI.h"
//***************************************************************************
#include "PhysSystem.h"
#include "PhysBody.h"
#include "Log.h"
//***************************************************************************
#include "EngineMathToPhysx.inl"
#include "PhysXCharacterController.h"
//***************************************************************************

namespace NGTech
{
	using namespace physx;

	PhysXCharacterController::JumpAction::JumpAction() :
		mV0(Math::ZEROFLOAT),
		mJumpTime(Math::ZEROFLOAT),
		mJump(false)
	{
	}

	void PhysXCharacterController::JumpAction::StartJump(float v0)
	{
		if (mJump)	return;
		mJumpTime = Math::ZEROFLOAT;
		mV0 = v0;
		mJump = true;
	}

	void PhysXCharacterController::JumpAction::StopJump()
	{
		if (!mJump)	return;
		mJump = false;
		mJumpTime = Math::ZEROFLOAT;
		mV0 = Math::ZEROFLOAT;
	}

	TimeDelta PhysXCharacterController::JumpAction::GetHeight(TimeDelta elapsedTime, TimeDelta _descHeight)
	{
		auto ph = GetPhysics();
		ASSERT(ph, "Not exist physics pointer");

		if (!mJump)	return Math::ZEROFLOAT;
		mJumpTime += elapsedTime;

		auto gJumpGravity = -(ph->GetActivePhysScene().GetGravity().y);

		float h = gJumpGravity * mJumpTime*mJumpTime + mV0 * mJumpTime;
		if (h > _descHeight)
			h = _descHeight;
		if (h <= 0)
			Warning("In: %s:%i height: %f of jump <0,it's correct?", __FUNCTION__, __LINE__, h);
		return h * elapsedTime;
	}
}
#endif