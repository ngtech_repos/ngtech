/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
/**
*/
#include "RenderPipeline.h"
#include "Scene.h"
#include "Light.h"
/**
*/
#include "PostProcess/FXAA.h"
#include "PostProcess/Deferred/DeferredShadingPipeline.h"
#include "PostProcess/Deferred/ClearPass.h"
#include "PostProcess/Deferred/DeferredShadingGeometryPass.h"
#include "PostProcess/Deferred/DeferredShadingLightPass.h"

namespace NGTech
{
	void LightEnvProbeShaderActions(Light* _light, uint32_t _diffIrad, uint32_t _specIrad, uint32_t _f0ScaleIrad, I_Shader* _shader);

	DeferredShading::~DeferredShading() {
		SAFE_RELEASE(m_Screenquad);
		SAFE_DELETE(m_DSLightPass);
		SAFE_DELETE(m_DSGeometry);
		SAFE_DELETE(m_ClearPass);
		SAFE_DELETE(m_GBuffer);
	}

	void DeferredShading::Init(const Vec2& _windowSize, I_RenderLowLevel* _render) {
		m_GBuffer = new GBuffer(_windowSize);
		m_ClearPass = new ClearPass();
		m_DSGeometry = new DeferredShadingGeometryPass();
		m_DSLightPass = new DeferredShadingLightPass();
		m_FXAA = new FXAA();

		ReSize(_windowSize, _render);
	}

	void DeferredShading::RunPipeline(const RenderPipeline&_rp, I_RenderLowLevel* _render, SceneManager& _sceneMgr, bool _enabledWireframe) {
		ASSERT(_render, "Invalid pointer on _render");
		if (!_render) return;
		if (_render->GetRenderName() == "NULLDrv")
			return;

		ASSERT(m_ClearPass, "Invalid pointer on m_ClearPass");
		if (!m_ClearPass) return;
		ASSERT(m_DSGeometry, "Invalid pointer on m_DSGeometry");
		if (!m_DSGeometry) return;
		ASSERT(m_DSLightPass, "Invalid pointer on m_DSLightPass");
		if (!m_DSLightPass) return;
		ASSERT(m_Screenquad, "Invalid pointer on m_Screenquad");
		if (!m_Screenquad) return;
		ASSERT(m_FXAA, "Invalid pointer on m_FXAA");
		if (!m_FXAA) return;

		//draw wire-frame
		if (_enabledWireframe)
			_render->EnableWireframeMode();

		m_ClearPass->Do(_rp, _render);
		m_DSGeometry->Do(m_GBuffer, _rp, _render);

		// we must disable it this, because will draw in quad
		if (_enabledWireframe)
			_render->DisableWireframeMode();

		if (_rp.GetAAMode() != CVARManager::AA_NONE) {
			m_DSLightPass->DoFromAnother(m_GBuffer, _rp, _render, _sceneMgr, m_Screenquad, m_FXAA);
		}
		else {
			m_DSLightPass->Do(m_GBuffer, _rp, _render, _sceneMgr, m_Screenquad);
		}
	}

	void DeferredShading::ReSize(const Vec2& _windowSize, I_RenderLowLevel* _render)
	{
		ASSERT(_render, "Invalid pointer on _render");
		if (!_render) return;

		if (_render->GetRenderName() == "NULLDrv") return;

		ASSERT(m_GBuffer, "Invalid pointer on m_GBuffer");
		if (!m_GBuffer) return;

		auto old_Screenquad = m_Screenquad;
		m_GBuffer->ReInit(_windowSize);

		m_Screenquad = _render->CreateReact();

		// Gamma Correction

		//!@todo Расскоментировать -
	/*	if (!m_LightPassShader)
			Error("[FinalProcessorDeferred::_Create()] deferredlp shader is null", true);
		else
		{
			texmat.Identity();
		}*/

		SAFE_RELEASE(old_Screenquad);
	}

	//struct GTAO
	//{
	//	GTAO(I_RenderLowLevel* _render) { Init(_render); }
	//	~GTAO() { SAFE_RELEASE(_shader); }

	//	void Do(GBuffer*gbuffer, const RenderPipeline&_rp, I_RenderLowLevel* _render) {
	//		ASSERT(_render, "Invalid pointer");
	//		ASSERT(gbuffer, "Invalid pointer");
	//		// if returned false
	//		ASSERT(gbuffer->IsInited(), "GBuffer not Inited");
	//		ASSERT(m_Inited, "m_Inited = false");
	//		ASSERT(_shader, "_shader is null");

	//		_rp.RenderWithSpecificShader(_shader);
	//	}

	//	void Init(I_RenderLowLevel* _render) {
	//		_shader = _render->ShaderCreate("GTAO.glsl");
	//		m_Inited = true;
	//	}

	//private:
	//	GTAO() = delete;
	//private:
	//	I_Shader* _shader = nullptr;
	//	bool m_Inited = false;
	//};
}