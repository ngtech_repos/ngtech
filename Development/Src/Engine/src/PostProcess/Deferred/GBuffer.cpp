/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"

#include "..\..\..\inc\PostProcess\Deferred\GBuffer.h"

namespace NGTech
{
	GBuffer::GBuffer(const Vec2& _size) : size(_size),
		framebuffer(nullptr),
		mInited(false)
	{
		this->ReInit(size);
	}

	void GBuffer::ReInit(const Vec2&_size)
	{
		this->mInited = false;
		this->_Init(_size);
	}

	bool GBuffer::_Init(const Vec2& _size)
	{
		// Warning(__FUNCTION__);
		ASSERT(this->mInited == false, "Already Inited");
		size = _size;

		auto render = GetRender();

		ASSERT(render, "Not exist render. Invalid pointer");
		if (!render) return false;

		if (render->GetRenderName() == "NULLDrv")
			return false;

		auto oldFrameBuffer = framebuffer;

		framebuffer = render->CreateFBO(_size.x, _size.y);
		ASSERT(framebuffer, "Invalid pointer on framebuffer");
		if (!framebuffer) return false;

		if (!framebuffer->AttachColorTexture(GBStruct::GB_RT0, Format::GLFMT_B16G16R16F))
		{
			ErrorWrite("[%s] AttachColorTexture(GB_RT0)", __FUNCTION__);
			return false;
		}
		if (!framebuffer->Validate())
		{
			ErrorWrite("[%s] Validate", __FUNCTION__);
			return false;
		}

		if (!framebuffer->AttachColorTexture(GBStruct::GB_RT1, Format::GLFMT_A16B16G16R16F))
		{
			ErrorWrite("[%s] AttachColorTexture(GB_RT1)", __FUNCTION__);
			return false;
		}
		if (!framebuffer->Validate())
		{
			ErrorWrite("[%s] Validate", __FUNCTION__);
			return false;
		}

		if (!framebuffer->AttachColorTexture(GBStruct::GB_RT2, Format::GLFMT_B16G16R16F))
		{
			ErrorWrite("[%s] AttachColorTexture(GB_RT2)", __FUNCTION__);
			return false;
		}
		if (!framebuffer->Validate())
		{
			ErrorWrite("[%s] Validate", __FUNCTION__);
			return false;
		}

		// RT3
		if (!framebuffer->AttachColorTexture(GBStruct::GB_RT3, Format::GLFMT_A16B16G16R16F))
		{
			ErrorWrite("[%s] AttachColorTexture(GB_RT3)", __FUNCTION__);
			return false;
		}
		if (!framebuffer->Validate())
		{
			ErrorWrite("[%s] Validate", __FUNCTION__);
			return false;
		}

		// RT4
		/*if (!framebuffer->AttachColorTexture(GBStruct::GB_RT4, Format::GLFMT_A32B32G32R32F))
		{
			ErrorWrite("[%s] AttachColorTexture(GB_RT5)", __FUNCTION__);
			return false;
		}*/

		if (!framebuffer->Validate())
		{
			ErrorWrite("[%s] Validate", __FUNCTION__);
			return false;
		}

		if (!framebuffer->AttachDepthTexture())
		{
			ErrorWrite("[%s] AttachDepthTexture", __FUNCTION__);
			return false;
		}
		if (!framebuffer->Validate())
		{
			ErrorWrite("[%s] Validate", __FUNCTION__);
			return false;
		}

		//DebugM("Created framebuffer. Links: %i", framebuffer->RefCounter());
		mInited = true;

		SAFE_RELEASE(oldFrameBuffer);

		return true;
	}

	void GBuffer::Bind(bool _writeToColor) const
	{
		// Warning(__FUNCTION__);
		// ��������� ����� mInited = false
		ASSERT(this->mInited, "Not inited");
		if (!mInited) return;

		ASSERT(framebuffer, "framebuffer is lost");
		if (!framebuffer) return;

		framebuffer->Set();

		if (_writeToColor)
			this->BindForReading();
	}

	void GBuffer::UnBind() const
	{
		// Warning(__FUNCTION__);
		// ��������� ����� mInited = false
		ASSERT(this->mInited, "Not inited");
		if (!mInited) return;

		ASSERT(framebuffer, "Not exist framebuffer");
		if (!framebuffer) return;

		this->_UnBindTexturesOnly();
		framebuffer->Unset();
	}

	void GBuffer::_UnBindTexturesOnly() const
	{
		// Warning(__FUNCTION__);
		// ��������� ����� mInited = false
		ASSERT(this->mInited, "Not inited");
		if (!mInited) return;

		ASSERT(framebuffer, "Not exist framebuffer");
		if (!framebuffer) return;

		//!@todo CHECK
		//framebuffer->UnBindActiveTexture(0);
	}

	void GBuffer::ClearAll() const
	{
		// Warning(__FUNCTION__);
		// ��������� ����� mInited = false
		ASSERT(this->mInited, "Not inited");
		if (!mInited) return;

		ASSERT(framebuffer, "Not exist framebuffer");
		if (!framebuffer) return;

		framebuffer->Clear();
	}

	void GBuffer::ClearDepthAndStencil() const
	{
		// Warning(__FUNCTION__);
		// ��������� ����� mInited = false
		ASSERT(this->mInited, "Not inited");
		if (!mInited) return;

		ASSERT(framebuffer, "Not exist framebuffer");
		if (!framebuffer) return;

		framebuffer->ClearDepthAndStencil();
	}

	void GBuffer::ClearColorOnly() const
	{
		// Warning(__FUNCTION__);
		// ��������� ����� mInited = false
		ASSERT(this->mInited, "Not inited");
		if (!mInited) return;

		ASSERT(framebuffer, "Not exist framebuffer");
		if (!framebuffer)
			return;

		framebuffer->ClearColorOnly();
	}

	void GBuffer::ClearDepthOnly() const
	{
		// Warning(__FUNCTION__);
		// ��������� ����� mInited = false
		ASSERT(this->mInited, "Not inited");
		if (!mInited) return;

		ASSERT(framebuffer, "Not exist framebuffer");
		if (!framebuffer) return;

		framebuffer->ClearDepthOnly();
	}

	void GBuffer::BindAndClear() const
	{
		// Warning(__FUNCTION__);
		// ��������� ����� mInited = false
		ASSERT(this->mInited, "Not inited");
		if (!mInited) return;

		ASSERT(framebuffer, "Not exist framebuffer");
		if (!framebuffer) return;

		framebuffer->Set();
		framebuffer->Clear();
		framebuffer->Unset();
	}

	void GBuffer::BindAlbedoTexture() const
	{
		// Warning(__FUNCTION__);
		// ��������� ����� mInited = false
		ASSERT(this->mInited, "Not inited");
		if (!mInited) return;

		ASSERT(framebuffer, "Not exist framebuffer");
		if (!framebuffer) return;

		framebuffer->BindColorTexture(GBStruct::GB_RT1);
	}

	void GBuffer::BindPositionTexture() const
	{
		// Warning(__FUNCTION__);
		// ��������� ����� mInited = false
		ASSERT(this->mInited, "Not inited");
		if (!mInited) return;

		ASSERT(framebuffer, "Not exist framebuffer");
		if (!framebuffer) return;

		framebuffer->BindColorTexture(GBStruct::GB_RT0);
	}

	void GBuffer::BindSpecularTexture() const
	{
		// Warning(__FUNCTION__);
		// ��������� ����� mInited = false
		ASSERT(this->mInited, "Not inited");
		if (!mInited) return;

		ASSERT(framebuffer, "Not exist framebuffer");
		if (!framebuffer) return;

		framebuffer->BindColorTexture(GBStruct::GB_RT2);
	}

	void GBuffer::BindDepthTextureForWriting() const
	{
		// Warning(__FUNCTION__);
		// ��������� ����� mInited = false
		ASSERT(this->mInited, "Not inited");
		ASSERT(framebuffer, "Not exist framebuffer");

		if (!framebuffer) return;

		framebuffer->BindDepthTexture(GBStruct::GB_RT_DEPTH);
	}

	void GBuffer::BindDepthTextureForReading() const
	{
		// Warning(__FUNCTION__);
		// ��������� ����� mInited = false
		ASSERT(this->mInited, "Not inited");
		if (!mInited) return;

		ASSERT(framebuffer, "Not exist framebuffer");
		if (!framebuffer) return;

		framebuffer->BindDepthTexture(GBStruct::GB_RT_DEPTH);
	}

	void GBuffer::Resolve(I_RenderTarget*_to, unsigned int _bitmask) const
	{
		ASSERT(this->mInited, "Not inited");
		if (!mInited) return;

		ASSERT(framebuffer, "Not exist framebuffer pointer");
		if (!framebuffer) return;

		ASSERT(_to, "Not exist _to pointer");
		if (!_to) return;


		framebuffer->Resolve(_to, _bitmask);
	}

	void GBuffer::BindForReading() const
	{
		// Warning(__FUNCTION__);
		// ��������� ����� mInited = false
		ASSERT(this->mInited, "Not inited");
		if (!mInited) return;
		ASSERT(framebuffer, "Not exist framebuffer");

		if (!framebuffer) return;

		framebuffer->BindColorTexture(GBStruct::GB_RT0);
		framebuffer->BindColorTexture(GBStruct::GB_RT1);
		framebuffer->BindColorTexture(GBStruct::GB_RT2);
		framebuffer->BindColorTexture(GBStruct::GB_RT3);
		//framebuffer->BindColorTexture(GBStruct::GB_RT4);
	}
}