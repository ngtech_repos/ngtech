/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"

#include "RenderPipeline.h"
#include "..\..\inc\PostProcess\FXAA.h"

namespace NGTech
{
	FXAA::~FXAA()
	{
		SAFE_RELEASE(m_FXAAShader);
		SAFE_RELEASE(m_FXAA_RT);
	}

	void FXAA::Init(const RenderPipeline&_rp, I_RenderLowLevel* _render, const Vec2&_screenSize)
	{
		ASSERT(_render, "Invalid pointer");

		ASSERT(m_Inited == false, "already inited");

		m_FXAAShader = _render->ShaderCreate("fxaa.glsl");

		ASSERT(m_FXAAShader, "m_FXAAShader is null");

		ReSize(_render, _screenSize);
	}

	void FXAA::Do(const RenderPipeline&_rp, I_RenderLowLevel* _render, I_Drawable* _screenquad)
	{
		PrePassEnd();
		ResultDraw(_rp, _render, _screenquad);
	}

	void FXAA::PrePassEnd()
	{
		ASSERT(m_FXAA_RT, "Invalid pointer");
		m_FXAA_RT->Unset();
		m_FXAA_RT->BindColorTexture(0);
	}

	void FXAA::ReSize(I_RenderLowLevel* _render, const Vec2&_screenSize)
	{
		ASSERT(_render, "Invalid pointer");
		auto old_m_FXAA_RT = m_FXAA_RT;

		m_FXAA_RT = _render->CreateFBO(_screenSize.x, _screenSize.y);

		// Подключаем их к фреймбуфферу
		if (!m_FXAA_RT->AttachColorTexture(0, Format::GLFMT_B16G16R16F))
		{
			ErrorWrite("[%s] Failed AttachColorTexture(0) - FXAA", __FUNCTION__);
			return;
		}

		ASSERT(m_FXAA_RT->Validate(), "Failed Validate() - FXAA");

		SAFE_RELEASE(old_m_FXAA_RT);
		m_Inited = true;
	}

	void FXAA::ResultDraw(const RenderPipeline&_rp, I_RenderLowLevel* _render, I_Drawable* _screenquad) {
		ASSERT(_render, "Invalid pointer on _render");
		ASSERT(m_FXAA_RT, "Invalid pointer on m_FXAA_RT");

		ASSERT(m_FXAAShader, "Invalid pointer on m_FXAAShader");
		ASSERT(_screenquad, "Invalid pointer on _screenquad");

		m_FXAA_RT->BindColorTexture(0);

		_rp.UpdateUniforms(m_FXAAShader);

		m_FXAAShader->SendInt("intexture", 0);
		m_FXAAShader->AddDefines("#define FXAA_PRESET " + _rp.GetAAMode());

		m_FXAAShader->Enable();
		{
			_screenquad->Draw();
		}
		m_FXAAShader->Disable();
	}

	void FXAA::PrePassStart(const RenderPipeline&_rp, I_RenderLowLevel* _render)
	{
		if (m_Inited == false)
			Init(_rp, _render, _rp.GetScreenSize());

		ASSERT(m_FXAA_RT, "Invalid pointer on m_FXAA_RT");
		m_FXAA_RT->Set();
		{
			m_FXAA_RT->BindColorTexture(0);
			//m_FXAA_RT->Clear();
		}
	}
}