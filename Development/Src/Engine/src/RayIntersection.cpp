#include "EnginePrivate.h"

//***************************************************
#include "../../../Externals/glfw/include/GLFW/glfw3.h"
#include "../../../Externals/glfw/include/GLFW/glfw3native.h"
//***************************************************
#include "../inc/WindowSystem_GLFW.h"
//***************************************************

namespace NGTech
{
	// https://github.com/SaschaWillems/openglcpp/blob/master/raypicking/glRenderer.cpp
	// https://www.scratchapixel.com/lessons/3d-basic-rendering/ray-tracing-rendering-a-triangle/moller-trumbore-ray-triangle-intersection
	bool RayIntersectsTriangle(const Vec3& origin, const Vec3& dir, const Vec3& v0, const Vec3& v1, const Vec3& v2, float* intersection)
	{
		// Triangle edges
		Vec3 e1(v1 - v0);
		Vec3 e2(v2 - v0);

		const float epsilon = 0.000001f;

		Vec3 P, Q;
		float t;

		// Calculate determinant
		P = Vec3::cross(dir, e2);
		float det = Vec3::dot(e1, P);
		// If determinant is (close to) zero, the ray lies in the plane of the triangle or parallel it's plane
		if ((det > -epsilon) && (det < epsilon))
		{
			return 0;
		}
		float invDet = 1.0f / det;

		// Distance from first vertex to ray origin
		Vec3 T = origin - v0;

		// Calculate u parameter
		float u = Vec3::dot(T, P) * invDet;
		// Intersection point lies outside of the triangle
		if ((u < 0.0f) || (u > 1.0f))
		{
			return 0;
		}

		//Prepare to test v parameter
		Q = Vec3::cross(T, e1);

		// Calculate v parameter
		float v = Vec3::dot(dir, Q) * invDet;
		// Intersection point lies outside of the triangle
		if (v < 0.f || u + v  > 1.f) return 0;

		// Calculate t
		t = Vec3::dot(e2, Q) * invDet;

		if (t > epsilon)
		{
			// Triangle interesected
			if (intersection)
			{
				*intersection = t;
			}
			return true;
		}

		// No intersection
		return false;
	}

	// http://dreamstatecoding.blogspot.com/2018/03/opengl-4-with-opentk-in-c-part-15.html
	void CheckIntesectionWithSubset(const Subset*_subset, TimeDelta _mouseX, TimeDelta _mouseY, TimeDelta _windowX, TimeDelta _windowY)
	{
		ASSERT(_subset, "_subset is null");

		struct
		{
			int32_t index = -1;
			float lastPos = std::numeric_limits<float>::max();
		} intersection;

		uint32_t numIntersections = 0;

		//glfwGetCursorPos(window, &mx, &my);
		//glfwGetWindowSize(window, &w, &h);

		Vec4 viewport = Vec4(0.0f, 0.0f, _windowX, _windowY);

		Mat4 inverse = Mat4::inverse(matrixes.matMVP);
		//glm::mat4 inverse = glm::inverse(uboVS.projection * uboVS.view * uboVS.model);

		// Mouse world pos on near plane
		Vec4 worldNear(float(_mouseX), float(_windowY - _mouseY), 0.0f, 1.0f);
		worldNear.x = ((worldNear.x - viewport[0]) / viewport[2]);
		worldNear.y = ((worldNear.y - viewport[1]) / viewport[3]);
		worldNear = worldNear * 2.0f - Vec4(1.0, 1.0, 1.0, 1.0);
		worldNear = inverse * worldNear;
		worldNear = worldNear / worldNear.w;

		// Mouse world pos on far plane
		Vec4 worldFar(float(_mouseX), float(_windowY - _mouseY), 1.0f, 1.0f);
		worldFar.x = ((worldFar.x - viewport[0]) / viewport[2]);
		worldFar.y = ((worldFar.y - viewport[1]) / viewport[3]);
		worldNear = worldNear * 2.0f - Vec4(1.0, 1.0, 1.0, 1.0);
		worldFar = inverse * worldFar;
		worldNear = worldNear / worldNear.w;

		// Get ray between pos on near and far plane
		Vec3 rayDir = Vec3::normalize(worldFar - worldNear);

		for (uint32_t i = 0; i < _subset->numIndices / 3; i++)
		{
			int ind0 = _subset->indices[i * 3 + 0];
			int ind1 = _subset->indices[i * 3 + 1];
			int ind2 = _subset->indices[i * 3 + 2];

			float currIntersectionPos;
			if (RayIntersectsTriangle(worldNear, rayDir, _subset->vertices[ind0].position/*triangleData[i * 3]*/, _subset->vertices[ind1].position/* triangleData[i * 3 + 1]*/, _subset->vertices[ind2].position/*triangleData[i * 3 + 2]*/, &currIntersectionPos))
			{
				if (currIntersectionPos < intersection.lastPos)
				{
					intersection.lastPos = currIntersectionPos;
					intersection.index = i;
				}
				std::cout << "\tIntersection " << numIntersections << ": Triangle " << i << " intersected at ray pos " << currIntersectionPos << std::endl;
				numIntersections++;
			}
		}
	}
}