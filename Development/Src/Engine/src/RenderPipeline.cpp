/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
/*
TODO:RenderPipeline: NOTES:*neeeded refactoring
*/
//**************************************
#include "EnginePrivate.h"
//**************************************
#include "Scene.h"
#include "Cache.h"
//**************************************
#include "Light.h"
#include "LightPoint.h"
//**************************************
#include "RenderPipeline.h"
#include "Frustum.h"
//**************************************
#include <iostream>
#include <algorithm>
#include <vector>
//**************************************
#include "../inc/PostProcess/Deferred/DeferredShadingPipeline.h"
//**************************************
#include "GUI.h"
//**************************************
#include "ObjectMesh.h"
#include "AnimatedMesh.h"
//**************************************

namespace NGTech
{
	template<class Tcontainer>
	static constexpr ENGINE_INLINE void _RenderObjects(const char* _passName, bool _blended, bool _transparent, Camera*_camera, RenderLayer _layer, Tcontainer&container, const RenderPipeline* rp) {
		ASSERT(_camera, "not exist camera");

		if (!_camera)
			return;

		for (const auto &object : container)
		{
			if (!object) {
				Debug("[_RenderMeshesShared] Invalid pointer in container :( ");
				continue;
			}

			// ��������� ����, ���� ���� �������� ������� �� ����� ���������,
			// ��������� ���
			if (object->GetRenderLayer() != _layer)
				continue;

			Mat4 objTrans = object->GetTransform();

			matrixes.matProj = _camera->GetProjMatrix();
			matrixes.matProjInv = matrixes.matProj.inverse(matrixes.matProjInv);
			matrixes.matView = _camera->GetTransform();
			matrixes.matView = matrixes.matView*objTrans;

			unsigned int s = 0;
			for (auto &subset : object->GetMaterials())
			{
				// frustum culling
				auto distance = (object->GetCenter(s) - _camera->GetPosition()).length();
				if (!_camera->GetFrustum()->IsInside(object->GetCenter(s), distance)) {
					continue; /*��� �����*/
				}

				ASSERT(subset.material, "Not exist subset.material. Subset %s, material %s", subset.subsetName.c_str(), subset.materialName.c_str());
				if (subset.material == nullptr)
					continue;

				if (subset.material->IsTransparent() && _transparent)
				{
					//!@todo Implement this
					Warning("[TODO:]_transparent ������");
				}

				if (subset.material->IsTransparent() != _transparent)
					continue;

				matrixes.ComputeMVPWorld(objTrans, _camera->GetPosition(), _camera->GetDirection(), _camera->GetClipDistance());

				if (_blended)
				{
					if (subset.material->PassHasBlending(_passName) == false) continue;
					subset.material->SetPassBlending(_passName);
				}
				else
				{
					if (subset.material->PassHasBlending(_passName)) continue;
				}
				//!@todo Check: cleaning
				//mtr->SetPassAlphaTest();

				if (!subset.material->SetPass(_passName)) {
					ErrorWrite("Failed set pass %s", _passName); //-V111
					continue;
				}

				/*Occlusion Query*/
				if (object->UseQuery())
				{
					if (rp->_RenderObjectWithOcclusionQuery(object, s) == false) {
						ErrorWrite("_RenderMeshWithOcclusionQuery failed");
						continue;
					}
				}
				// �������� ��� OQ
				else
				{
					/*Render Mesh*/
					if (rp->_RenderObjectFast(object, s) == false) {
						ErrorWrite("_RenderMeshFast failed");
						continue;
					}
				}

				//!@todo Check: cleaning
				//mtr->UnsetPassAlphaTest();

				if (subset.material->PassHasBlending(_passName) && _blended) {
					subset.material->UnsetPassBlending();
				}
				subset.material->UnsetPass();

				s++;
			}
		}

		_camera->_RecalculateFrustum();
	}

	/*
	������������ ��������� ��� ��������, ������ � ��������
	������������ �������� - frustum culling
	��� Occlusion Query
	*/
	template<class Tcontainer>
	static constexpr ENGINE_INLINE void _RenderObjectsWithShader(I_Shader* _shader, Camera*_camera, bool _UseFrustumCulling, Tcontainer&container, const RenderPipeline* rp) {
		ASSERT(_camera, "not exist camera");
		ASSERT(_shader, "not exist _shader");

		if (!_camera)
			return;

		for (const auto &object : container)
		{
			if (!object) {
				Debug("[_RenderMeshesSharedWithShader] Invalid pointer in container :( ");
				continue;
			}

			Mat4 objTrans = object->GetTransform();

			matrixes.matProj = _camera->GetProjMatrix();
			matrixes.matProjInv = matrixes.matProj.inverse(matrixes.matProjInv);
			matrixes.matView = _camera->GetTransform();
			matrixes.matView = matrixes.matView*objTrans;

			for (size_t s = 0; s < object->GetNumSubsets(); s++)
			{
				if (_UseFrustumCulling) {
					// frustum culling
					auto distance = (object->GetCenter(s) - _camera->GetPosition()).length();
					if (!_camera->GetFrustum()->IsInside(object->GetCenter(s), distance))
					{
						// ��� �����
						continue;
					}
				}

				matrixes.ComputeMVPWorld(objTrans, _camera->GetPosition(), _camera->GetDirection(), _camera->GetClipDistance());

				_shader->Enable();

				/*Occlusion Query*/
				{
					/*Render Mesh*/
					if (rp->_RenderObjectFast(object, s) == false) {
						ErrorWrite("_RenderMeshFast failed");
						continue;
					}
				}

				_shader->Enable();
			}
		}

		_camera->_RecalculateFrustum();
	}

	template<class T>
	static constexpr void ENGINE_INLINE Util_UpdateRenderObjects(T _container) {
		//Debug("[Util_UpdateRenderObjects] Call");
		// ���� ��������� ������, �� ������� �������� �� ���������
		if (_container.empty())
		{
			//Debug("[Util_UpdateRenderObjects] Empty Container");
			return;
		}
		//else
		//	Debug("[Util_UpdateRenderObjects] Not empty");

		for (const auto &obj : _container)
		{
			if (!obj)
			{
				Debug("[Util_UpdateRenderObjects] Invalid pointer in _container");
				continue;
			}
			//ErrorWrite("[Util_UpdateRenderObjects] valid pointer");

			obj->Update();
			obj->WaitForSeconds();
		}
	}

	template<class T>
	static constexpr void ENGINE_INLINE Util_FixedUpdateObjects(T _container) {
		//Debug("[Util_FixedUpdateObjects] Call");
		// ���� ��������� ������, �� ������� �������� �� ���������
		if (_container.empty())
		{
			//Debug("[Util_FixedUpdateObjects] Empty Container");
			return;
		}
		//else
			//Debug("[Util_FixedUpdateObjects] Not empty");

		for (const auto & obj : _container)
		{
			if (!obj)
			{
				Debug("[Util_FixedUpdateObjects] Invalid pointer in _container");
				continue;
			}
			//ErrorWrite("[Util_FixedUpdateObjects] valid pointer");

			obj->FixedUpdate();
			obj->WaitForFixedUpdate();
		}
	}

	template<class T>
	static constexpr void ENGINE_INLINE Util_LateUpdateObjects(T _container) {
		//Debug("[Util_LateUpdateObjects] Call");

		// ���� ��������� ������, �� ������� �������� �� ���������
		if (_container.empty())
		{
			//Debug("[Util_LateUpdateObjects] Empty Container");
			return;
		}
		//else
		//	Debug("[Util_LateUpdateObjects] Not empty");

		for (const auto &obj : _container)
		{
			if (!obj)
			{
				Debug("[Util_LateUpdateObjects] Invalid pointer in _container");
				continue;
			}
			else
			{
				//ErrorWrite("[Util_LateUpdateObjects] check pointer");

				obj->LateUpdate();
			}
		}
	}

	/**
	*/
	RenderPipeline::RenderPipeline(Engine*_engine)
		:needStats(false)
		, paused(false)
	{
		Debug(__FUNCTION__);
		deltaTime = Math::ZERODELTA;
		_InitMembers(_engine);
	}

	void RenderPipeline::_InitMembers(Engine* _engine) {
		engine = _engine;
		ASSERT(_engine, "Invalid pointer");
		render = _engine->iRender;
		ASSERT(render, "Invalid pointer");
		m_WeakCvars = _engine->cvars;
		ASSERT(_engine->cvars, "Invalid pointer");
		screensize = Vec2(engine->cvars->r_width, engine->cvars->r_height);
	}

	/**
	*/
	void RenderPipeline::Initialise() {
		Log::WriteHeader("-- Scene --");
		Debug("RenderPipeline::Initialise");

		auto cvars = m_WeakCvars.lock();
		ASSERT(cvars, "Cvars is expired");

		screensize = Vec2(cvars->r_width, cvars->r_height);

		m_Pipeline = new DeferredShading();
		m_Pipeline->Init(screensize, engine->iRender);
	}

	/**
	*/
	RenderPipeline::~RenderPipeline() {
		SAFE_DELETE(m_Pipeline); //-V809
	}

	bool RenderPipeline::isUseAA() const {
		auto cvars = m_WeakCvars.lock();
		ASSERT(cvars, "Cvars is expired");

		if (!cvars)
			return false;

		return cvars->r_aa_mode != CVARManager::AA_NONE;
	}

	int RenderPipeline::GetAAMode() const {
		auto cvars = m_WeakCvars.lock();
		ASSERT(cvars, "Cvars is expired");

		if (!cvars)
			return CVARManager::AA_NONE;

		return cvars->r_aa_mode;
	}

	/**
	*/
	void RenderPipeline::Update(bool _paused, TimeDelta _delta) {
		ASSERT(engine, "Invalid pointer engine");
		if (!engine) return;
		ASSERT(engine->scene, "Invalid pointer scene");
		if (!engine->scene) return;
		ASSERT(engine->cache, "Invalid pointer cache");
		if (!engine->cache) return;

		BROFILER_CATEGORY("RenderPipeline", Brofiler::Color::LightGoldenRodYellow);
		PROFILE;
		if (paused != _paused)
			paused = _paused;

		// If state not changed
		if (_delta == Math::ZERODELTA) return;

		deltaTime = _delta;

		auto cam = engine->scene->m_ActiveScene.GetCurrentCamera();

		if (!cam) {
			Debug("invalid pointer on cam");
			return;
		}

#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
		auto stats = GetStatistic();
		// The update statistics of Render
		if (stats) {
			stats->SetRenderDelta();
		}
		else
			Debug("invalid pointer on stats");
#endif

		//---------update-camera-----------------------------------
		cam->UpdateWCD();

		_UpdateActions();

		_RenderComplex();

#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
		// The update statistics of Render
		if (stats) {
			stats->PrepareToNewFrame();
		}
		else
			Debug("invalid pointer on stats");
#endif

		_RenderUI();
	}

	void RenderPipeline::_RenderUI() const {
		ASSERT(engine, "Invalid pointer engine");
		if (!engine) return;

		ASSERT(engine->gui, "Invalid pointer on engine->gui");
		if (!engine->gui) return;

		engine->gui->Render();
	}

	void RenderPipeline::_RenderComplex() {
		ASSERT(engine, "Invalid pointer engine");
		if (!engine) return;
		ASSERT(engine->scene, "Invalid pointer on engine->scene");
		if (!engine->scene) return;
		ASSERT(engine->cache, "Invalid pointer on engine->cache");
		if (!engine->cache) return;
		ASSERT(engine->cvars, "Invalid pointer on engine->cvars");
		if (!engine->cvars) return;

		//---------draw-scene--------------------------------
		matrixes.matTime = engine->GetElapsedTimeFromStart();

		if (matrixes.matTime <= Math::ZERODELTA) return;

		m_Pipeline->RunPipeline(*this, engine->iRender, *engine->scene, engine->cvars->r_wireframe);

		needStats = false;
	}

	/**
	*/
	void RenderPipeline::_RenderMeshesOnLayers(const char*_passname, bool blended, bool _transparent) const {
		ASSERT(engine, "Invalid pointer on engine");
		if (!engine) return;
		ASSERT(engine->scene, "Invalid pointer on scene");
		if (!engine->scene) return;

		auto cam = engine->scene->m_ActiveScene.GetCurrentCamera();
		ASSERT(cam, "Not exist cam ptr");

		if (cam == nullptr) return;

		// ������ �� �����
		for (unsigned int layer = RenderLayer::LAYER_0; layer <= RenderLayer::USER_LAYER_14; ++layer)
		{
			_RenderObjects(_passname, blended, _transparent, cam, (RenderLayer)layer, engine->scene->m_ActiveScene.m_SceneData.GetObjectsForChange(), this);
		}
	}

	/**
	*/
	bool RenderPipeline::_RenderObjectWithOcclusionQuery(BaseVisualObjectEntity*_object, size_t _index) const {
		ASSERT(_object, "Invalid pointer on _object");
		if (!_object) return false;

		return _object->RenderQuery(render, _index, deltaTime);
	}

	/**
	*/
	bool RenderPipeline::_RenderObjectFast(BaseVisualObjectEntity*_object, size_t _index) const {
		ASSERT(_object, "Invalid pointer on _object");
		if (!_object) return false;

		return _object->RenderFast(render, _index, deltaTime);
	}

	/**
	*/
	void RenderPipeline::ReSize(const Vec2&_size) {
		ASSERT(render, "Invalid pointer on render");
		if (!render) return;
		ASSERT(m_Pipeline, "Invalid pointer on m_Pipeline");
		if (!m_Pipeline) return;
		ASSERT(engine, "Invalid pointer on engine");
		if (!engine) return;
		ASSERT(engine->cvars, "Invalid pointer on engine->cvars");
		if (!engine->cvars) return;

		engine->cvars->r_width = _size.x;
		engine->cvars->r_height = _size.y;

		render->Reshape(_size.x, _size.y);
		m_Pipeline->ReSize(_size, render);
	}

	/**
	*/
	void RenderPipeline::UpdateUniforms(I_Shader * _shd) const {
		ASSERT(_shd, "Invalid pointer on engine->cvars");
		if (!_shd) return;

		ASSERT(engine, "Invalid pointer on engine");
		if (!engine) return;
		ASSERT(engine->cvars, "Invalid pointer on engine->cvars");
		if (!engine->cvars) return;

		matrixes.UpdateUniforms(_shd);


		_shd->SendVec2("u_ScreenSize", this->screensize);
		/*will used in gamma correction*/
		_shd->SendFloat("u_gammaf", Math::ONEFLOAT / engine->cvars->cl_gamma);
		_shd->SendVec4("auto_bakedCameraData", engine->scene->m_ActiveScene.GetCurrentCamera()->GetBakedData());
	}

	void RenderPipeline::_UpdateActions() const {
		ASSERT(engine, "Invalid pointer on engine");
		if (!engine) return;
		ASSERT(engine->scene, "Invalid pointer on engine->scene");
		if (!engine->scene) return;

		// ��������� ��� ���������, ��� � ���� �������.(������������ �������� � ���� �� ������)
		// ��������� ��� �������
		Util_FixedUpdateObjects(engine->scene->m_ActiveScene.m_SceneData.GetUpdatableObjectsForChange());
		Util_UpdateRenderObjects(engine->scene->m_ActiveScene.m_SceneData.GetUpdatableObjectsForChange());
		Util_LateUpdateObjects(engine->scene->m_ActiveScene.m_SceneData.GetUpdatableObjectsForChange());
	}

	/**
	*/
	void RenderPipeline::_DeferredPass() const {
		this->_RenderMeshesOnLayers("DeferredPass", false, false);
	}

	void RenderPipeline::_ForwardPass() const {
		this->_RenderMeshesOnLayers("ForwardPass", true, true);
	}

	void RenderPipeline::RenderWithSpecificShader(I_Shader*_shader) const {
		ASSERT(_shader, "Invalid pointer on _shader");
		if (!_shader) return;
		ASSERT(engine, "Invalid pointer on engine");
		if (!engine) return;
		ASSERT(engine->scene, "Invalid pointer on engine->scene");
		if (!engine->scene) return;

		auto cam = engine->scene->m_ActiveScene.GetCurrentCamera();
		ASSERT(cam, "Not exist cam ptr");

		if (cam == nullptr) return;

		_RenderObjectsWithShader(_shader, cam, true, engine->scene->m_ActiveScene.m_SceneData.GetObjectsForChange(), this);
	}
}