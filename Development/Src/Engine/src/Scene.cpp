/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//***************************************************
#include "EnginePrivate.h"
#include "Scene.h"
#include "Cache.h"

#include <algorithm>

//***************************************************
#include "Serializer.h"
//***************************************************
#include "LoadingScreen.h"
//***************************************************
#include "ScriptUpdateJob.h"
//***************************************************
#include "CameraFixed.h"
//***************************************************
#include "Light.h"
//***************************************************
#include "ObjectMesh.h"
#include "AnimatedMesh.h"
//***************************************************
#include "Scene.h"
//***************************************************
#include "CameraFixed.h"
//***************************************************

namespace NGTech
{
	ATTRIBUTE_ALIGNED16(class) AnimateObjectJob : public ThreadTask
	{
	public:
		AnimateObjectJob() {}

		virtual void Do(MT::FiberContext&ctx) override {
			ASSERT(mScene, "Invalid pointer");
			ASSERT(animTaskCount, "Invalid pointer");

			if (animatedMesh)
			{
				//animatedMesh->PlayAnimationUpdate()
			}

			animTaskCount->IncFetch();
			MT::SpinSleepMilliSeconds(1);
		}

		void SetSceneData(Scene*_scene, MT::Atomic32<int32>* _taskCount, AnimatedMesh* _animMesh)
		{
			mScene = _scene;
			animTaskCount = _taskCount;
			animatedMesh = _animMesh;
		}

		MT_DECLARE_TASK(AnimateObjectJob, MT::StackRequirements::STANDARD, MT::TaskPriority::NORMAL, MT::Color::Blue);
	private:
		Scene * mScene = nullptr;
		MT::Atomic32<int32>* animTaskCount = nullptr;
		AnimatedMesh* animatedMesh = nullptr;
	};

	//!todo implement this in structure
	template<class T>
	void RunAnimTask(PlatformManager*_platformM, Scene* _scene, T _container)
	{
		if (_container.empty())
			return;

		Warning("Created container size: %i", _container.size());

		ASSERT(_platformM, "Not exist pointer on _platformM");
		ASSERT(_scene, "Not exist pointer on _scene");

		MT::Atomic32<int32> counter;

		auto tasks = new AnimateObjectJob[_container.size()];

		unsigned int i = 0;

		for (const auto&p : _container)
		{
			tasks[i].SetSceneData(_scene, &counter, p);
			i++;
		}
		_platformM->RunTask(&tasks[0], i);

		Warning("[RunAnimTask] Created:  %i", i);

		// Wait for the tasks to complete
		_platformM->WaitTask(&tasks[0], i);

		delete[] tasks;
	}

	/* Public Functions*/
	bool Scene::Create()
	{
		LogPrintf("Scene: %s %s", m_SceneData.GetSceneName().c_str(), " has been created.");

		m_SceneData.SetState(SceneData::SceneState::Created);

		return true;
	}
	/**
	*/
	void Scene::SetCameraAndDestroyOlder(Camera* _camera) {
		Debug(__FUNCTION__);
		this->m_SceneData.SetCurrentCamera(_camera);
	}

	void Scene::_DrawLoadingProgress(TimeDelta _delta, LoadingScreen* _screen)
	{
		if (!_screen) return;

		_screen->Show();

		/*if (m_SceneData.GetSceneLoaded() == false)
		{
			Warning("Scene: %s %s", m_SceneData.GetSceneName().c_str(), " has loaded.");
			m_SceneData.SetSceneLoaded(true);
		}*/
	}

	void Scene::UpdateGame(TimeDelta _delta) {
		//Warning("Scene::UpdateGame");

		auto engine = GetEngine();
		ASSERT(engine, "Engine not exist");

		auto _platformM = engine->platformM;
		ASSERT(_platformM, "Platform not exist");

		RunAnimTask(_platformM, this, this->m_SceneData.GetAnimMeshesListForChange());
	}

	void Scene::_CreateDefCameraAndSet() {
		Debug(__FUNCTION__);
		this->SetCameraAndDestroyOlder(new CameraFixed());
	}

	Camera::BufferData Scene::GetCurrentCameraData()
	{
		auto cam = GetCurrentCamera();
		if (!cam)
		{
			Warning("[ERROR] GetCurrentCameraData-GetCurrentCamera is nullptr");
			return Camera::BufferData();
		}

		return cam->GetBufferData();
	}

	Camera* Scene::GetCurrentCamera()
	{
		if (m_SceneData.GetCurrentCamera() == nullptr)
		{
			if (m_SceneData.GetDefCamera() == nullptr)
			{
				ErrorWrite("Not exist m_SceneData.cameraDef. Creation ...");
				m_SceneData.SetCameraDef(new Camera());

				return m_SceneData.GetDefCamera();
			}

			return m_SceneData.GetDefCamera();
		}
		return m_SceneData.GetCurrentCamera();
	}

	void Scene::LoadSceneFinishedEventApply()
	{
		m_SceneData.SetLoadingStatus(false);
		m_SceneData.SetState(SceneData::SceneState::Game);

		API_SetGravity(m_SceneData.GetSceneGravity());
		Shader_SetAmbient(GetSceneAmbient());
	}

	void Scene::SetActive(bool active) {
		m_SceneData.SetActive(active);
	}

	void Scene::LoadScene(void)
	{
		if (m_SceneData.GetSceneLoaded() == true) return;

		m_SceneData.SetLoadingStatus(true);
		m_SceneData.SetState(SceneData::SceneState::Loading);

	}
}