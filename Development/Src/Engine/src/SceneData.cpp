//***************************************************
#include "EnginePrivate.h"
//***************************************************
#include "Scene.h"
//***************************************************
#include "CameraFixed.h"
#include "Light.h"
#include "ObjectMesh.h"
#include "AnimatedMesh.h"
//***************************************************

namespace NGTech
{
	/**
	*/
	template <class T>
	static constexpr ENGINE_INLINE void _ReleaseAllObjects(T& _objL)
	{
		if (_objL.empty()) {
			//Debug("List is empty");
			return;
		}

		//Debug(__FUNCTION__);
		// we will use older method for increment able, instead iterator and for each
		// because else we get vector iterator not increment able
		for (auto &ptr : _objL)
		{
			if (!ptr)
				continue;

			SAFE_RELEASE(ptr);
		}
		_objL.clear();
	}

	template <class T>
	static constexpr ENGINE_INLINE void _DeleteAllObjects(T& _objL)
	{
		if (_objL.empty()) {
			//Debug("List is empty");
			return;
		}

		// �� ������� �� �������!
		_objL.clear();
	}

	/**
	*/
	SceneData::SceneData() {
		Clear();
	}

	SceneData::~SceneData()
	{
		Clear();
		_ResetCameras();
	}

	void SceneData::Clear()
	{
		_ReleaseAllObjects(meshes);
		_ReleaseAllObjects(skeletals);
		_ReleaseAllObjects(lights);
		_ReleaseAllObjects(cameras);

		_DeleteAllObjects(m_UpdatableObjects);
		_DeleteAllObjects(m_VisualObjects);

#ifndef DROP_EDITOR
		auto m_Stat = GetStatistic();
		ASSERT(m_Stat, "Not exist Statistic pointer");

		if (!m_Stat) return;

		m_Stat->SetLightsCount(lights.size());
		m_Stat->SetEntityCount(meshes.size() + skeletals.size());
		m_Stat->SetCamerasCount(cameras.size());
#endif
	}

	void SceneData::SetCurrentCamera(Camera*_cam)
	{
		auto oldercamera = currentCamera;
		currentCamera = _cam;
		SAFE_DELETE(oldercamera); //-V809
	}

	void SceneData::SetCameraDef(Camera*_cam)
	{
		auto oldercamera = cameraDef;
		cameraDef = _cam;
		SAFE_DELETE(oldercamera); //-V809
	}

	void SceneData::_ResetCameras()
	{
		SAFE_DELETE(currentCamera); //-V809

		//!@todo MEMLEAK, FIX IT - CRASH
		//SAFE_DELETE(cameraDef);
	}
}