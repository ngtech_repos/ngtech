/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
#include "Scene.h"
#include "Cache.h"

#include <algorithm>

#include "Scene.h"
//***************************************************
#include "Serializer.h"
//***************************************************
#include "LoadingScreen.h"
//***************************************************
#include "ScriptUpdateJob.h"
//***************************************************
#include "CameraFixed.h"
//***************************************************
#include "Light.h"
//***************************************************
#include "ObjectMesh.h"
#include "AnimatedMesh.h"
//***************************************************

namespace NGTech
{
	/**
	*/
	SceneManager::SceneManager(Engine*_engine)
		:engine(_engine),
		lcurLScreen(nullptr)
	{
		Debug(__FUNCTION__);

		ASSERT(engine, "Not exist engine");

		m_CacheWeak = engine->cache;

		_InitEditorVars();

		m_SceneUpdateJob = std::make_shared<ScriptUpdateJob>(ScriptUpdateJob::UPDATE_MODE::UPDATE_SCENEMANAGER);
	}

	SceneManager::~SceneManager() {
		Debug(__FUNCTION__);
		m_SceneUpdateJob.reset();
		Clear();
	}

	void SceneManager::Initialise()
	{
		m_Inited = true;
		Clear();
		SetLoadingScreen(m_ActiveScene.m_SceneData.GetLoadingScreenName());
	}

	void SceneManager::Clear() {
		//Debug(__FUNCTION__);
		NewScene();
	}

	void SceneManager::Update(TimeDelta _delta) {
		ASSERT(IsInited(), "IsInited() false");

		if (IsInited() == false)
			return;

		//!@todo CHECK
		//if (m_currentScene->getState() == Scene::SceneState::Created)
		//{
		//	m_currentScene->loadScene();
		//}
		//else if (m_currentScene->getState() == Scene::SceneState::Game)
		//{
		//	m_currentScene->updateGame(delta);
		//}
		//else if (m_currentScene->getState() == Scene::SceneState::Loading)
		//{
		//	if (m_currentScene->isLoading())
		//	{
		//		m_currentScene->updateLoading(delta);
		//	}
		//}

		//if (m_currentScene->getState() == Scene::SceneState::Destroyed ||
		//	m_currentScene->getState() == Scene::SceneState::LoadNextScene ||
		//	m_currentScene->getState() == Scene::SceneState::LoadPreloadScene)
		//{
		//	if (m_currentScene->getState() == Scene::SceneState::Unintialised)
		//	{
		//		// Work out next scene

		//		m_currentScene->create(m_scriptManager->getScriptsForScene(m_currentScene->getName()));
		//		m_currentScene->loadScene();
		//	}

		//	if (m_currentScene->getState() == Scene::SceneState::Destroyed &&
		//		!m_currentScene->isLoading())
		//	{
		//		m_currentScene->destroy();
		//		m_currentScene = m_preloadScene;
		//	}
		//}

		//Debug(__FUNCTION__);

		//Warning("SceneManager::Update");
		m_ActiveScene.UpdateGame(_delta);

		ASSERT(m_SceneUpdateJob, "Not exist pointer on m_SceneUpdateJob");

		if (!m_SceneUpdateJob) return;
		m_SceneUpdateJob->Update();
	}

	//!@todo Fix bug: ��������� ��� � ����������� ������� �� �����, ���� ��� ������ �� 160 �����, � ������� � ������ ������.
	// ���� ������ � �������� ���������, � �� � ���������
	void SceneManager::NewScene() {
		//Warning(__FUNCTION__);

		ASSERT(IsInited(), "IsInited() false");

		if (IsInited() == false)
			return;

		auto cache = m_CacheWeak.lock();

		ASSERT(cache, "Cache is expired");

		if (cache)
			cache->DestroyAllUnUsedResources();

		SetActiveScene(Scene());
		LoadScene();
	}

	void SceneManager::LoadScene()
	{
#if 1
		ASSERT(IsInited(), "IsInited() false");

		if (IsInited() == false)
			return;

		//!@todo CHECK
		// Loading scene
		//if (m_ActiveScene.m_SceneData.GetSceneName() == "EMPTY")
		//	return;

		//if (m_ActiveScene.m_SceneData.GetSceneLoaded() == false)
		//{
		//	LogPrintf("Scene: %s %s", m_ActiveScene.m_SceneData.GetSceneName(), " is loading.");

		//	m_ActiveScene.m_SceneData.SetLoadingStatus(true);
		//	m_ActiveScene.m_SceneData.GetResourceManager().load();

		SetEngineState(EngineState::STATE_LOADING);

		//}
#endif
	}

	void SceneManager::AddObject(UpdatableObject*_ptr)
	{
		ASSERT(IsInited(), "IsInited() false");

		if (IsInited() == false)
			return;

		ASSERT(_ptr, "Invalid pointer on _ptr");
		if (!_ptr) return;

		m_ActiveScene.m_SceneData.GetUpdatableObjectsForChange().push_back(_ptr);

		_AddEntityToStats();
	}

	void SceneManager::AddObject(BaseVisualObjectEntity* _ptr)
	{
		ASSERT(IsInited(), "IsInited() false");

		if (IsInited() == false)
			return;

		ASSERT(_ptr, "Invalid pointer on _ptr");
		if (!_ptr) return;

		m_ActiveScene.m_SceneData.GetObjectsForChange().push_back(_ptr);

		_AddEntityToStats();
	}

	void SceneManager::AddObject(BaseEntity* _ptr)
	{
		ASSERT(IsInited(), "IsInited() false");

		if (IsInited() == false)
			return;

		ASSERT(_ptr, "Invalid pointer on _ptr");
		if (!_ptr) return;

		m_ActiveScene.m_SceneData.GetBaseEntities().push_back(_ptr);

		_AddEntityToStats();
	}

	void SceneManager::AddMesh(ObjectMesh *_ptr)
	{
		ASSERT(IsInited(), "IsInited() false");

		if (IsInited() == false)
			return;

		ASSERT(_ptr, "Invalid pointer on _ptr");
		if (!_ptr) return;

		m_ActiveScene.m_SceneData.GetMeshesListForChange().push_back(_ptr);

		_AddEntityToStats();
	}

	void SceneManager::AddSkeletal(AnimatedMesh *_ptr)
	{
		ASSERT(IsInited(), "IsInited() false");

		if (IsInited() == false)
			return;

		ASSERT(_ptr, "Invalid pointer on _ptr");
		if (!_ptr) return;

		m_ActiveScene.m_SceneData.GetAnimMeshesListForChange().push_back(_ptr);

		_AddEntityToStats();
	}

	void SceneManager::AddLight(Light *_ptr)
	{
		ASSERT(IsInited(), "IsInited() false");

		if (IsInited() == false)
			return;

		ASSERT(_ptr, "Invalid pointer on _ptr");
		if (!_ptr) return;

		m_ActiveScene.m_SceneData.GetLightListForChange().push_back(_ptr);

		_AddEntityToStats();
		_AddLightToStats();
	}

	/**
	*/
	void SceneManager::SetLoadingScreen(const String&_path) {
		//Debug(__FUNCTION__);

		ASSERT(IsInited(), "IsInited() false");

		if (IsInited() == false)
			return;

		if (_path.empty())
			return;

		m_ActiveScene.m_SceneData.SetLoadingScreenName(_path);

		//initializing loading screen
		SetLoadingScreen(new LoadingScreen(_path));
	}

	void SceneManager::SetLoadingScreen(LoadingScreen*_l)
	{
		//Debug(__FUNCTION__);

		ASSERT(IsInited(), "IsInited() false");
		ASSERT(_l, "Invalid pointer");

		if (IsInited() == false)
			return;

		auto old = lcurLScreen;
		lcurLScreen = _l;
		SAFE_DELETE(old); //-V809
	}

	void SceneManager::DrawLoading(const EngineState& _state, TimeDelta _delta)
	{
		ASSERT(IsInited(), "IsInited() false");

		if (IsInited() == false)
			return;

		bool _isLoading = ((_state & (STATE_LOADING)) == (STATE_LOADING));

		if (_isLoading && m_ActiveScene.m_SceneData.GetSceneLoaded() == false)
		{
			if (this->lcurLScreen == nullptr)
			{
				Warning("[%s] not exist loadingScreen pointer", __FUNCTION__);
				return;
			}

			m_ActiveScene._DrawLoadingProgress(_delta, this->lcurLScreen);
		}
		else {
			//Warning("[%s] %i", " SceneManager::UpdateLoading", _state);
		}
	}

	String SceneManager::GetLoadingScreenName() {
		if (m_ActiveScene.m_SceneData.GetLoadingScreenName().empty()) {
			m_ActiveScene.m_SceneData.SetLoadingScreenName(DefaultResources::Texture_DefaultDiffuse_Name);
		}
		return m_ActiveScene.m_SceneData.GetLoadingScreenName();
	}

	/**
	*/
	void SceneManager::DeleteLightFromList(Light *object) {
		ASSERT(object, "Invalid ptr");
		Debug(__FUNCTION__);

		_DeleteAllObjectsFromListWithoutCallingDesCtorForObj<std::vector<Light*>, Light*>(m_ActiveScene.m_SceneData.GetLightListForChange(), object);

#ifndef DROP_EDITOR
		GetStatistic()->ReduceEntityCount();
		GetStatistic()->ReduceLightsCount();
#endif
	}

	void SceneManager::DeleteObjectMeshFromList(ObjectMesh *object) {
		ASSERT(object, "Invalid ptr");
		Debug(__FUNCTION__);

		_DeleteAllObjectsFromListWithoutCallingDesCtorForObj<std::vector<ObjectMesh*>, ObjectMesh*>(m_ActiveScene.m_SceneData.GetMeshesListForChange(), object);

#ifndef DROP_EDITOR
		GetStatistic()->ReduceEntityCount();
#endif
	}

	void SceneManager::DeleteSkeletalFromList(AnimatedMesh *object) {
		ASSERT(object, "Invalid ptr");
#ifdef HEAVY_DEBUG
		Debug(__FUNCTION__);
#endif

		_DeleteAllObjectsFromListWithoutCallingDesCtorForObj<std::vector<AnimatedMesh*>, AnimatedMesh*>(m_ActiveScene.m_SceneData.GetAnimMeshesListForChange(), object);

#ifndef DROP_EDITOR
		GetStatistic()->ReduceEntityCount();
#endif
	}

	void SceneManager::DeleteUpdatableObjectFromList(UpdatableObject *object) {
		ASSERT(object, "Invalid ptr");
#ifdef HEAVY_DEBUG
		Debug(__FUNCTION__);
#endif

		_DeleteAllObjectsFromListWithoutCallingDesCtorForObj(m_ActiveScene.m_SceneData.GetUpdatableObjectsForChange(), object);

#ifndef DROP_EDITOR
		GetStatistic()->ReduceEntityCount();
#endif
	}

	void SceneManager::DeleteObject(BaseVisualObjectEntity*_ptr)
	{
		ASSERT(_ptr, "Invalid ptr");
#ifdef HEAVY_DEBUG
		Debug(__FUNCTION__);
#endif

		_DeleteAllObjectsFromListWithoutCallingDesCtorForObj(m_ActiveScene.m_SceneData.GetObjectsForChange(), _ptr);

#ifndef DROP_EDITOR
		GetStatistic()->ReduceEntityCount();
#endif
	}

	void SceneManager::DeleteObject(BaseEntity*_ptr)
	{
		ASSERT(_ptr, "Invalid ptr");
#ifdef HEAVY_DEBUG
		Debug(__FUNCTION__);
#endif

		_DeleteAllObjectsFromListWithoutCallingDesCtorForObj(m_ActiveScene.m_SceneData.GetBaseEntities(), _ptr);

#ifndef DROP_EDITOR
		GetStatistic()->ReduceEntityCount();
#endif
	}

	bool SceneManager::_CameraCheckValid()
	{
#ifdef HEAVY_DEBUG
		Debug(__FUNCTION__);
#endif

		if (!m_ActiveScene.m_SceneData.GetCurrentCamera())
		{
			Warning("Current camera is not valid.Loosing ptr?.Activated Fixed camera");
			return false;
		}

		return true;
}

	void SceneManager::_AddEntityToStats()
	{
#ifdef HEAVY_DEBUG
		Debug(__FUNCTION__);
#endif
#ifndef DROP_EDITOR
		GetStatistic()->AddEntityCount();
#endif
	}

	void SceneManager::_AddLightToStats()
	{
#ifdef HEAVY_DEBUG
		Debug(__FUNCTION__);
#endif

#ifndef DROP_EDITOR
		GetStatistic()->AddLightsCount();
#endif
	}

	void SceneManager::_InitEditorVars()
	{
		Debug(__FUNCTION__);

#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)
		// Rendering
		m_EditorVars.push_back(EditorVar("Use Early Z Pass", &(this->m_ActiveScene.m_SceneData.GetEnabledEarlyZForChange()), "Rendering", "Early Z rejection"));
		m_EditorVars.push_back(EditorVar("Gamma Correction Mode", &matrixes.u_gammaMode, EditorVar::UNINT, "Rendering",
			"Gamma correction modes: 0 - simple, 1 - linear, 2 - simple Reinhard, 3 - luma based Reinhard,\
		 4 - white preserving luma based Reinhard, 5 - RomBinDaHouse, 6 - Uncharted2, 7 - Filmic"));

		// Physics
		TODO("Add Functional!!");
		//!@todo CHECK
		//static bool mIgnorePhDelta = false;
		//m_EditorVars.push_back(EditorVar("Physics Ignore Delta Time", &mIgnorePhDelta, "Physics",
		//	"If TRUE,physics simulation will ignore time elapsed between frames, and use(0.033 TimeDilation)"));

		// Scene
		m_EditorVars.push_back(EditorVar("Loading Screen filename", &this->m_ActiveScene.m_SceneData.GetLoadingScreenNameForChange(), EditorVar::TEXTURE, "Scene", "Path to loading screen"));
		m_EditorVars.push_back(EditorVar("Scene Ambient", &matrixes.matSceneAmbient, EditorVar::FLOAT3, "Scene", "Path to loading screen"));
#endif
	}

	void SceneManager::SetGammaCorrectionMode(unsigned int _mode)
	{
		Debug(__FUNCTION__);

		auto mode = static_cast<SceneMatrixes::GammaCorrectionMode>(_mode);
		if (mode > SceneMatrixes::GammaCorrectionMode::GAMMACORRECTION_MAX || mode < SceneMatrixes::GammaCorrectionMode::simpleGammaCorrection)
			mode = SceneMatrixes::GammaCorrectionMode::Uncharted2ToneMapping;

		matrixes.u_gammaMode = mode;
	}

	unsigned int SceneManager::GetGammaCorrectionMode()
	{
		Debug(__FUNCTION__);

		auto mode = static_cast<SceneMatrixes::GammaCorrectionMode>(matrixes.u_gammaMode);
		if (mode > SceneMatrixes::GammaCorrectionMode::GAMMACORRECTION_MAX || mode < SceneMatrixes::GammaCorrectionMode::simpleGammaCorrection)
			mode = SceneMatrixes::GammaCorrectionMode::Uncharted2ToneMapping;

		return mode;
	}

	void SceneManager::SetActiveScene(const Scene& newScene) {
		// ��������� ���������� �����
		auto olderScene = m_ActiveScene;
		// ������� ����� �����
		m_ActiveScene = newScene;
		// ������������ ����� �� ����� �����
		this->SetGammaCorrectionMode(m_ActiveScene.GetGammaCorrectionMode());
		// ������� ������ �����
		olderScene.Clear();
	}
}