/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
//***************************************************
#include "ScriptUpdateJob.h"
#include "ScriptManager.h"
//***************************************************

namespace NGTech
{
	void ScriptUpdateJob::Update(const ScriptManager&_sm)
	{
		switch (m_State.mode)
		{
		case UPDATE_MODE::UPDATE:
			_sm.Update();
			break;
		case UPDATE_MODE::UPDATE_INPUT:
			_sm.UpdateInput();
			break;
		case UPDATE_MODE::UPDATE_GUI:
			_sm.UpdateGUI();
			break;
		case UPDATE_MODE::UPDATE_PHYSICS:
			_sm.UpdatePhysics();
			break;
		case UPDATE_MODE::UPDATE_RENDER:
			_sm.UpdateRender();
			break;
		case UPDATE_MODE::UPDATE_SCENEMANAGER:
			_sm.UpdateSceneManager();
			break;
		case UPDATE_MODE::UPDATE_AI:
			_sm.UpdateAI();
			break;
		case UPDATE_MODE::LOADED:
			_sm.Loaded();
			break;
		case UPDATE_MODE::LOADED_GUI:
			_sm.LoadedGUI();
			break;
		default:
			Warning("INCORRECT UPDATE");
		}
	}

	void ScriptUpdateJob::Update()
	{
		Update(*GetScriptManager());
	}
}