/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
//**************************************
#include "SerializerSharedIncls.h"
#include "Serializer.h"
//**************************************
#include "Deserializer.h"

namespace NGTech {
	template <>
	void Serializer::SerializeObj(BasePhysicsObjectEntity<ObjectMesh*>* object, const char* _function, const char* calledInFile, int lineId);

	template <>
	void Serializer::SerializeObj(BaseVisualObjectEntity*object, const char* _function, const char* calledInFile /*= __FILE__*/, int lineId /*= __LINE__*/);

	template <>
	void Serializer::SerializeObj(ALSound*object, const char* _function, const char* calledInFile /*= __FILE__*/, int lineId /*= __LINE__*/);

	//---------------------------Common---------------------------
	/*Float,bool,int,unsigned int,size_t, String, const char**/
#include "Serializer/SerializeCommonTypes.inc"

// Math
/*Vec2,Vec3,Vec4,Mat3,Mat4,Color*/
#include "Serializer/SerializeMathTypes.inc"
	/**
	*/
	Serializer::Serializer()
	{
		m_stream = nullptr;
		writer = nullptr;
	}

	template <> ENGINE_INLINE ENGINE_API
		void Serializer::SerializeObj(BaseEntity* object, const char* _function, const char* calledInFile /*= __FILE__*/, int lineId /*= __LINE__*/)
	{
		ASSERT(object, "Invalid obj");
		ASSERT(writer, "Invalid writer");

		if ((!object) || (!writer))
			return;

		DEBUG_SERIALIZER_TRACE;
		writer->StartObject();
		{
			writer->Key("transform");
			SerializeObj(object->GetTransform(), __FUNCTION__, __FILE__, __LINE__);
			writer->Key("upVector");
			SerializeObj(object->GetUpVector(), __FUNCTION__, __FILE__, __LINE__);
			writer->Key("angle");
			SerializeObj(object->GetAngle(), __FUNCTION__, __FILE__, __LINE__);
		}
		writer->EndObject();
	}

	template <> ENGINE_INLINE ENGINE_API
		void Serializer::SerializeObj(PhysBodyDescSh *object, const char* _function, const char* calledInFile /*= __FILE__*/, int lineId /*= __LINE__*/)
	{
		ASSERT(writer, "Invalid writer");

		if (object == nullptr) {
			Debug("Not exist PhysbodyDesc pointer");
			return;
		}

		// Vec3
		{
			writer->Key("LinearVelocity");
			SerializeObj(object->LinearVelocity, __FUNCTION__, __FILE__, __LINE__);

			writer->Key("AngularVelocity");
			SerializeObj(object->AngularVelocity, __FUNCTION__, __FILE__, __LINE__);

			writer->Key("CollisionSize");
			SerializeObj(object->CollisionSize, __FUNCTION__, __FILE__, __LINE__);

			writer->Key("AppliedTorque");
			SerializeObj(object->AppliedTorque, __FUNCTION__, __FILE__, __LINE__);

			writer->Key("MassSpaceInertiaTensor");
			SerializeObj(object->MassSpaceInertiaTensor, __FUNCTION__, __FILE__, __LINE__);
		}

		//float
		{
			writer->Key("CollisionRad");
			SerializeObj(object->CollisionRadius, __FUNCTION__, __FILE__, __LINE__);

			writer->Key("CollisionWidth");
			SerializeObj(object->CollisionWidth, __FUNCTION__, __FILE__, __LINE__);

			writer->Key("CollisionHeight");
			SerializeObj(object->CollisionHeight, __FUNCTION__, __FILE__, __LINE__);

			writer->Key("CollisionMass");
			SerializeObj(object->CollisionMass, __FUNCTION__, __FILE__, __LINE__);

			writer->Key("CollisionDensity");
			SerializeObj(object->CollisionDensity, __FUNCTION__, __FILE__, __LINE__);

			writer->Key("LinearDamping");
			SerializeObj(object->LinearDamping, __FUNCTION__, __FILE__, __LINE__);

			writer->Key("AngularDamping");
			SerializeObj(object->AngularDamping, __FUNCTION__, __FILE__, __LINE__);
		}
	}

	template <> ENGINE_INLINE ENGINE_API
		void Serializer::SerializeObj(PhysBody* object, const char* _function, const char* calledInFile /*= __FILE__*/, int lineId /*= __LINE__*/)
	{
		ASSERT(writer, "Invalid writer");

		if (writer == nullptr)
			return;

		// ������ ��� ������ ����, ��� ��� �������� ������ ����� ��� ����������� ����
		// (����������� ������, ����� ���������� ���� ������������� ���������� ����, ���� ��� �� ���������)
		if (object == nullptr)
		{
			Warning("Not exist physbody");
			return;
		}

		DEBUG_SERIALIZER_TRACE;
		writer->StartObject();
		{
			writer->Key("phType");
			SerializeObj(static_cast<unsigned int>(object->GetCollisionType()), __FUNCTION__, __FILE__, __LINE__);

			SerializeObj(&object->GetDescCopy(), __FUNCTION__, __FILE__, __LINE__);

			//writer->Key("ImpactSound");
			//SerializeObj(object->GetImpactSound(), __FUNCTION__, __FILE__, __LINE__);
		}
		writer->EndObject();
	}

	template <> ENGINE_INLINE ENGINE_API
		void Serializer::SerializeObj(ObjectMesh* object, const char* _function, const char* calledInFile /*= __FILE__*/, int lineId /*= __LINE__*/)
	{
		ASSERT(object, "Invalid obj");
		ASSERT(writer, "Invalid writer");

		if ((!object) || (!writer))
			return;

		DEBUG_SERIALIZER_TRACE;
		writer->StartObject();
		{
			writer->Key("object");
			SerializeObj((BaseEntity*)object, __FUNCTION__, __FILE__, __LINE__);

			/*VisualObject*/
			{
				writer->Key("BaseVisualObjectEntity");
				SerializeObj(static_cast<BaseVisualObjectEntity*>(object), __FUNCTION__, __FILE__, __LINE__);
			}

			writer->Key("Enabled");
			SerializeObj(object->GetEnabled(), __FUNCTION__, __FILE__, __LINE__);

			/*Collision*/
			{
				writer->Key("BasePhysicsObjectEntity");
				SerializeObj(object->m_PhysicsEntity, __FUNCTION__, __FILE__, __LINE__);
			}
		}
		writer->EndObject();
	}

	template <> ENGINE_INLINE ENGINE_API
		void Serializer::SerializeObj(Light* object, const char* _function, const char* calledInFile /*= __FILE__*/, int lineId /*= __LINE__*/)
	{
		ASSERT(object, "Invalid obj");
		ASSERT(writer, "Invalid writer");

		if ((!object) || (!writer))
			return;

		DEBUG_SERIALIZER_TRACE;
		writer->StartObject();
		writer->Key("lightType");
		SerializeObj(static_cast<unsigned int>(object->GetType()), __FUNCTION__, __FILE__, __LINE__);
		writer->Key("dirRight");
		SerializeObj(object->GetDirectionRight(), __FUNCTION__, __FILE__, __LINE__);
		writer->Key("dirUp");
		SerializeObj(object->GetDirectionUp(), __FUNCTION__, __FILE__, __LINE__);
		writer->Key("lumFlux");
		SerializeObj(object->GetLumFlux(), __FUNCTION__, __FILE__, __LINE__);
		writer->Key("position");
		SerializeObj(object->GetPosition(), __FUNCTION__, __FILE__, __LINE__);
		writer->Key("radius");
		SerializeObj(object->GetRadius(), __FUNCTION__, __FILE__, __LINE__);
		writer->Key("iRadius");
		SerializeObj(object->GetIRadius(), __FUNCTION__, __FILE__, __LINE__);

		writer->Key("color");
		SerializeObj(object->GetColor(), __FUNCTION__, __FILE__, __LINE__);

		writer->Key("direction");
		SerializeObj(object->GetDirection(), __FUNCTION__, __FILE__, __LINE__);
		writer->Key("isEnable");
		SerializeObj(object->IsEnabled(), __FUNCTION__, __FILE__, __LINE__);
		writer->Key("isVisible");
		SerializeObj(object->IsVisible(), __FUNCTION__, __FILE__, __LINE__);

		writer->Key("isCastShadows");
		SerializeObj(object->IsCastShadows(), __FUNCTION__, __FILE__, __LINE__);

		writer->Key("isDisableByDistance");
		SerializeObj(object->IsDisableByDistance(), __FUNCTION__, __FILE__, __LINE__);
		writer->Key("IsDisableByDistanceValue");
		SerializeObj(object->IsDisableByDistanceValue(), __FUNCTION__, __FILE__, __LINE__);

		writer->Key("fov");
		SerializeObj(object->GetFOV(), __FUNCTION__, __FILE__, __LINE__);

		writer->Key("lightArea");
		SerializeObj(object->GetLightArea(), __FUNCTION__, __FILE__, __LINE__);

		writer->Key("isFrustumCullingEnabled");
		SerializeObj(object->IsFrustumCullingEnabled(), __FUNCTION__, __FILE__, __LINE__);

		writer->Key("scale");
		SerializeObj(object->GetScale(), __FUNCTION__, __FILE__, __LINE__);

		/*Spot*/
		writer->Key("spotAngles");
		SerializeObj(object->GetSpotAngles(), __FUNCTION__, __FILE__, __LINE__);

		writer->EndObject();
	}

	template <> ENGINE_INLINE ENGINE_API
		void Serializer::SerializeObj(PhysXCharacterController* object, const char* _function, const char* calledInFile /*= __FILE__*/, int lineId /*= __LINE__*/)
	{
		ASSERT(object, "Invalid obj");
		ASSERT(writer, "Invalid writer");

		if ((!object) || (!writer))
			return;

		DEBUG_SERIALIZER_TRACE;
		writer->StartObject();
		{
			writer->Key("mode");
			auto desc = object->GetDescription();
			writer->Uint(desc.mMode);
			writer->Key("position");
			SerializeObj(desc.position, __FUNCTION__, __FILE__, __LINE__);
			writer->Key("upDirection");
			SerializeObj(desc.upDirection, __FUNCTION__, __FILE__, __LINE__);
		}
		writer->EndObject();
	}

	template <> ENGINE_INLINE ENGINE_API
		void Serializer::SerializeObj(ALSoundSource* object, const char* _function, const char* calledInFile /*= __FILE__*/, int lineId /*= __LINE__*/)
	{
		ASSERT(object, "Invalid obj");
		ASSERT(writer, "Invalid writer");

		if ((!object) || (!writer))
		{
			ErrorWrite("Not exist sound or file or writer");
			return;
		}

		DEBUG_SERIALIZER_TRACE;
		auto decsPtr = object->GetDesc();

		writer->StartObject();
		{
			writer->Key("desc.preset");
			SerializeObj(static_cast<unsigned int>(decsPtr.preset), __FUNCTION__, __FILE__, __LINE__);

			writer->Key("desc.path");
			SerializeObj(decsPtr.GetPath(), __FUNCTION__, __FILE__, __LINE__);

			writer->Key("desc.Paused");
			SerializeObj(decsPtr.Paused, __FUNCTION__, __FILE__, __LINE__);
			writer->Key("desc.Stoped");
			SerializeObj(decsPtr.Stoped, __FUNCTION__, __FILE__, __LINE__);
			writer->Key("desc.Looped");
			SerializeObj(decsPtr.Looped, __FUNCTION__, __FILE__, __LINE__);
			writer->Key("desc.Relative");
			SerializeObj(decsPtr.Relative, __FUNCTION__, __FILE__, __LINE__);
			writer->Key("desc.Gain");
			SerializeObj(decsPtr.Gain, __FUNCTION__, __FILE__, __LINE__);
			writer->Key("desc.RollOffFactor");
			SerializeObj(decsPtr.RollOffFactor, __FUNCTION__, __FILE__, __LINE__);
			writer->Key("desc.MaxDistance");
			SerializeObj(decsPtr.MaxDistance_in_Meter, __FUNCTION__, __FILE__, __LINE__);
			writer->Key("desc.MinDistance");
			SerializeObj(decsPtr.MinDistance_in_Meter, __FUNCTION__, __FILE__, __LINE__);
			writer->Key("desc.Position");
			SerializeObj(decsPtr.Position, __FUNCTION__, __FILE__, __LINE__);
		}
		writer->EndObject();
	}

	template <> ENGINE_INLINE ENGINE_API
		void Serializer::SerializeObj(ALSound* object, const char* _function, const char* calledInFile /*= __FILE__*/, int lineId /*= __LINE__*/)
	{
		ASSERT(writer, "Invalid writer");

		DEBUG_SERIALIZER_TRACE;

		writer->StartObject();
		{ // TODO: ��� ���������� - ������ �������� ������� ������ ������ ��� ���
			writer->Key("desc.path");
			if (object == nullptr) {
				SerializeObj(StringHelper::EMPTY_STRING, __FUNCTION__, __FILE__, __LINE__);
			}
			else
				SerializeObj(object->desc.path.c_str(), __FUNCTION__, __FILE__, __LINE__);
		}
		writer->EndObject();
	}

	template <> ENGINE_INLINE ENGINE_API
		void Serializer::SerializeObj(Camera* object, const char* _function, const char* calledInFile /*= __FILE__*/, int lineId /*= __LINE__*/)
	{
		ASSERT(object, "Invalid obj");
		ASSERT(writer, "Invalid writer");

		if ((!object) || (!writer))
			return;

		DEBUG_SERIALIZER_TRACE;
		writer->StartObject();
		/**/
		writer->Key("type");
		SerializeObj(static_cast<unsigned int>(object->GetCameraType()), __FUNCTION__, __FILE__, __LINE__);
		/**/
		writer->Key("phSize");
		SerializeObj(object->getPhysicsSize(), __FUNCTION__, __FILE__, __LINE__);

		/**/
		writer->Key("upVector");
		SerializeObj(object->GetUpVector(), __FUNCTION__, __FILE__, __LINE__);

		/**/
		writer->Key("position");
		SerializeObj(object->GetPosition(), __FUNCTION__, __FILE__, __LINE__);

		/**/
		writer->Key("direction");
		SerializeObj(object->GetDirection(), __FUNCTION__, __FILE__, __LINE__);

		/**/
		writer->Key("maxVelocity");
		SerializeObj(object->GetMaxVelocity(), __FUNCTION__, __FILE__, __LINE__);

		/**/
		writer->Key("fov");
		SerializeObj(object->GetFOV(), __FUNCTION__, __FILE__, __LINE__);

		/**/
		writer->Key("ZNear");
		SerializeObj(object->GetZNear(), __FUNCTION__, __FILE__, __LINE__);

		/**/
		writer->Key("ZFar");
		SerializeObj(object->GetZFar(), __FUNCTION__, __FILE__, __LINE__);

		/**/
		writer->Key("Angles");
		SerializeObj(Vec2(object->GetAngle(EulerAngle::ANGLE_0), object->GetAngle(EulerAngle::ANGLE_1)), __FUNCTION__, __FILE__, __LINE__);

		/**/
		//TODO: ("Add GetTransform(instead position and direction)");

		writer->EndObject();
	}

	template <> ENGINE_INLINE ENGINE_API
		void Serializer::SerializeObj(SceneManager*object, const char* _function, const char* calledInFile /*= __FILE__*/, int lineId /*= __LINE__*/)
	{
		ASSERT(object, "Invalid obj");
		ASSERT(writer, "Invalid writer");

		if ((!object) || (!writer))
			return;

		DEBUG_SERIALIZER_TRACE;
		writer->StartObject();
		// Scene Params
		{
			writer->Key("useEarlyZ");
			this->SerializeObj(object->EnabledEarlyZ(), __FUNCTION__, __FILE__, __LINE__);

			writer->Key("gammaCorrMode");
			this->SerializeObj(object->GetGammaCorrectionMode(), __FUNCTION__, __FILE__, __LINE__);

			writer->Key("sceneAmbient");
			this->SerializeObj(object->m_ActiveScene.GetSceneAmbient(), __FUNCTION__, __FILE__, __LINE__);

			writer->Key("currentCamera");
			this->SerializeObj(object->m_ActiveScene.GetCurrentCamera(), __FUNCTION__, __FILE__, __LINE__);//TODO:������� ���� currentcamera, � ������ ���� �����

			writer->Key("loadingScreen");
			this->SerializeObj(object->GetLoadingScreenName(), __FUNCTION__, __FILE__, __LINE__);
		}
		// Meshes
		{
			writer->Key("meshes");

			// Meshes
			StartArray();
			{
				for (auto i = 0; i < object->MeshesCount(); i++)
				{
					auto mesh = object->GetMeshById(i);
					if (!mesh)
					{
						ErrorWrite("[SerializeObj Scene meshes] Failed GetMeshById method");
						continue;
					}
					this->SerializeObj(mesh, __FUNCTION__, __FILE__, __LINE__);
				}
			}
			EndArray();
		}
		// Lights
		{
			writer->Key("lights");
			// Lights
			StartArray();
			{
				for (auto i = 0; i < object->LightsCount(); i++)
				{
					auto light = object->GetLightById(i);
					if (!light)
						continue;
					this->SerializeObj(light, __FUNCTION__, __FILE__, __LINE__);
				}
			}
			EndArray();
		}
		// Sounds
		TODO("Background Sounds");
		//TODO Background Sounds

		writer->EndObject();
	}

	template <> ENGINE_INLINE ENGINE_API
		void Serializer::SerializeObj(BaseVisualObjectEntity*object, const char* _function, const char* calledInFile /*= __FILE__*/, int lineId /*= __LINE__*/)
	{
		ASSERT(object, "Invalid obj");
		ASSERT(writer, "Invalid writer");

		if ((!object) || (!writer))
			return;

		DEBUG_SERIALIZER_TRACE;
		writer->StartObject();
		{
			writer->Key("RenderLayer");
			SerializeObj(static_cast<unsigned int>(object->GetRenderLayer()), __FUNCTION__, __FILE__, __LINE__);

			writer->Key("LoadingPath");
			SerializeObj(object->GetLoadingPath(), __FUNCTION__, __FILE__, __LINE__);

			writer->Key("UseHWQuery");
			SerializeObj(object->UseQuery(), __FUNCTION__, __FILE__, __LINE__);

			// Materials List
			{
				writer->Key("materials");
				StartArray();
				for (unsigned int i = 0; i < object->GetNumSubsets(); i++)
				{
					writer->StartObject();
					{
						// ���� ��������� ������, �� �������� �������� �� ���������
						{
							writer->Key("material");
							auto mat = object->GetMaterial(i);

							if (mat == nullptr)
								SerializeObj(DefaultResources::Material_Default_Name, __FUNCTION__, __FILE__, __LINE__);
							else
								SerializeObj(mat->GetPath(), __FUNCTION__, __FILE__, __LINE__);
						}

						{
							writer->Key("subset");
							SerializeObj(object->GetSubsetName(i).c_str(), __FUNCTION__, __FILE__, __LINE__);
						}
					}
					writer->EndObject();
				}
				EndArray();
			}
		}
		writer->EndObject();
	}

	template <> ENGINE_INLINE ENGINE_API
		void Serializer::SerializeObj(BasePhysicsObjectEntity<ObjectMesh*>* object, const char* _function, const char* calledInFile, int lineId)
	{
		ASSERT(object, "Invalid obj");
		ASSERT(writer, "Invalid writer");

		if ((!object) || (!writer))
			return;

		DEBUG_SERIALIZER_TRACE;
		writer->StartObject();
		{
			auto pb = object->GetPhysBody();
			if (pb) {
				writer->Key("PhysBodyDesc");
				this->SerializeObj(pb, __FUNCTION__, __FILE__, __LINE__);
			}
		}
		writer->EndObject();
	}

	Serializer::~Serializer()
	{
		SAFE_DELETE(writer); //-V809
		SAFE_DELETE(m_stream); //-V809
	}

	Serializer::Serializer(const char* _path)
	{
		InitFile(_path);
	}

	void Serializer::InitFile(const char* _path)
	{
		m_outfile.open(_path);
		m_stream = new MyOStreamWrapper(m_outfile);
		ASSERT(m_stream, "Invalid obj");

		writer = new Writer<MyOStreamWrapper>(*m_stream);
		ASSERT(writer, "Invalid writer");
	}

	void Serializer::StartArray()
	{
		ASSERT(writer, "Invalid writer");
		if (!writer)
			return;

		writer->StartArray();
	}

	void Serializer::EndArray()
	{
		ASSERT(writer, "Invalid writer");
		if (!writer)
			return;

		writer->EndArray();
	}

	template <> ENGINE_INLINE ENGINE_API
		void Serializer::SerializeObj(PhysXMaterialSh* object, const char* _function, const char* calledInFile, int lineId)
	{
		ASSERT(object, "Invalid obj");
		ASSERT(writer, "Invalid writer");

		if ((!object) || (!writer))
			return;

		DEBUG_SERIALIZER_TRACE;

		writer->StartObject();
		{
			writer->Key("staticFriction");
			SerializeObj(object->GetStaticFriction(), __FUNCTION__, __FILE__, __LINE__);
			writer->Key("dynamicFriction");
			SerializeObj(object->GetDynamicFriction(), __FUNCTION__, __FILE__, __LINE__);
			writer->Key("restitution");
			SerializeObj(object->GetRestitution(), __FUNCTION__, __FILE__, __LINE__);
			writer->Key("vehicleDrivableSurface");
			SerializeObj(object->IsVehicleDrivableSurface(), __FUNCTION__, __FILE__, __LINE__);
		}
		writer->EndObject();
	}

	//===============================================MATERIALS================================================================
	/**
	Passes
	*/
	template <> ENGINE_INLINE ENGINE_API
		void Serializer::SerializeObj(MaterialPassSh* object, const char* _function, const char* calledInFile, int lineId)
	{
		ASSERT(object, "Invalid obj");
		ASSERT(writer, "Invalid writer");

		if ((!object) || (!writer))
			return;

		writer->StartObject();
		writer->Key("name");
		SerializeObj(object->name, __FUNCTION__, __FILE__, __LINE__);

		{
			writer->Key("castShadows");
			SerializeObj(object->castShadows, __FUNCTION__, __FILE__, __LINE__);
			writer->Key("recieveShadows");
			SerializeObj(object->castShadows, __FUNCTION__, __FILE__, __LINE__);
		}

		writer->Key("defines");
		SerializeObj(object->defines, __FUNCTION__, __FILE__, __LINE__);

		writer->Key("hasAlphaTest");
		SerializeObj(object->hasAlphaTest, __FUNCTION__, __FILE__, __LINE__);
		{
			writer->Key("alphaFunc");
			SerializeObj(static_cast<unsigned int>(object->alphaFunc), __FUNCTION__, __FILE__, __LINE__);

			writer->Key("alphaRef");
			SerializeObj(object->alphaRef, __FUNCTION__, __FILE__, __LINE__);
		}

		writer->Key("hasBlending");
		SerializeObj(object->hasBlending, __FUNCTION__, __FILE__, __LINE__);
		{
			writer->Key("blendSrc");
			SerializeObj(static_cast<unsigned int>(object->src), __FUNCTION__, __FILE__, __LINE__);

			writer->Key("blendDst");
			SerializeObj(static_cast<unsigned int>(object->dst), __FUNCTION__, __FILE__, __LINE__);
		}

		writer->Key("shaderName");
		SerializeObj(object->shaderName, __FUNCTION__, __FILE__, __LINE__);

		// Samplers
		{
			// samplers 2D
			{
				writer->Key("samplers2D");
				StartArray();
				for (size_t i = 0; i < object->u_sampler2D.size(); i++)
				{
					writer->StartObject();
					auto obj = *object->u_sampler2D[i];
					// Format: SamplerName-TextureName-NeedlyAddAtr-Value
					writer->Key("SamplerName");
					writer->String(obj.SamplerName.c_str());
					writer->Key("TextureName");
					writer->String(obj.TextureName.c_str());
					writer->Key("NeedlyAddAtr");
					writer->Bool(obj.needlyAddAtr);
					writer->Key("Value");
					SerializeObj(Serializer::UNUSED, __FUNCTION__, __FILE__, __LINE__);
					writer->EndObject();
				}
				EndArray();
			}
			// samplers Cube
			{
				writer->Key("samplersCube");
				StartArray();
				for (size_t i = 0; i < object->u_samplerCube.size(); i++)
				{
					writer->StartObject();
					auto obj = *object->u_samplerCube[i];
					// Format: SamplerName-TextureName-NeedlyAddAtr-Value
					writer->Key("SamplerName");
					SerializeObj(obj.SamplerName.c_str(), __FUNCTION__, __FILE__, __LINE__);
					writer->Key("TextureName");
					SerializeObj(obj.TextureName.c_str(), __FUNCTION__, __FILE__, __LINE__);
					writer->Key("NeedlyAddAtr");
					SerializeObj(obj.needlyAddAtr, __FUNCTION__, __FILE__, __LINE__);
					writer->Key("Value");
					SerializeObj(Serializer::UNUSED, __FUNCTION__, __FILE__, __LINE__);
					writer->EndObject();
				}
				EndArray();
			}
			// samplers float
			{
				writer->Key("samplersFloat");
				StartArray();
				for (size_t i = 0; i < object->u_float.size(); i++)
				{
					writer->StartObject();
					auto obj = *object->u_float[i];
					// Format: SamplerName-value(float)
					writer->Key("SamplerName");
					SerializeObj(obj.SamplerName.c_str(), __FUNCTION__, __FILE__, __LINE__);
					writer->Key("TextureName");
					SerializeObj(obj.TextureName.c_str(), __FUNCTION__, __FILE__, __LINE__);
					writer->Key("NeedlyAddAtr");
					SerializeObj(obj.needlyAddAtr, __FUNCTION__, __FILE__, __LINE__);
					writer->Key("Value");
					SerializeObj(obj.value, __FUNCTION__, __FILE__, __LINE__);
					writer->EndObject();
				}
				EndArray();
			}
			// samplers vec2
			{
				writer->Key("samplersVec2");
				StartArray();
				for (size_t i = 0; i < object->u_Vec2.size(); i++)
				{
					writer->StartObject();
					auto obj = *object->u_Vec2[i];
					// Format: SamplerName-value(vec2)
					writer->Key("SamplerName");
					SerializeObj(obj.SamplerName.c_str(), __FUNCTION__, __FILE__, __LINE__);
					writer->Key("TextureName");
					SerializeObj(obj.TextureName.c_str(), __FUNCTION__, __FILE__, __LINE__);
					writer->Key("NeedlyAddAtr");
					SerializeObj(obj.needlyAddAtr, __FUNCTION__, __FILE__, __LINE__);
					writer->Key("Value");
					SerializeObj(obj.value, __FUNCTION__, __FILE__, __LINE__);
					writer->EndObject();
				}
				EndArray();
			}
			// samplers vec3
			{
				writer->Key("samplersVec3");
				StartArray();
				for (size_t i = 0; i < object->u_Vec3.size(); i++)
				{
					writer->StartObject();
					auto obj = *object->u_Vec3[i];
					// Format: SamplerName-value(vec3)
					writer->Key("SamplerName");
					SerializeObj(obj.SamplerName.c_str(), __FUNCTION__, __FILE__, __LINE__);
					writer->Key("TextureName");
					SerializeObj(obj.TextureName.c_str(), __FUNCTION__, __FILE__, __LINE__);
					writer->Key("NeedlyAddAtr");
					SerializeObj(obj.needlyAddAtr, __FUNCTION__, __FILE__, __LINE__);
					writer->Key("Value");
					SerializeObj(obj.value, __FUNCTION__, __FILE__, __LINE__);
					writer->EndObject();
				}
				EndArray();
			}
			// samplers vec4
			{
				writer->Key("samplersVec4");
				StartArray();
				for (size_t i = 0; i < object->u_Vec4.size(); i++)
				{
					writer->StartObject();
					auto obj = *object->u_Vec4[i];
					// Format: SamplerName-value(vec4)
					writer->Key("SamplerName");
					SerializeObj(obj.SamplerName.c_str(), __FUNCTION__, __FILE__, __LINE__);
					writer->Key("TextureName");
					SerializeObj(obj.TextureName.c_str(), __FUNCTION__, __FILE__, __LINE__);
					writer->Key("NeedlyAddAtr");
					SerializeObj(obj.needlyAddAtr, __FUNCTION__, __FILE__, __LINE__);
					writer->Key("Value");
					SerializeObj(obj.value, __FUNCTION__, __FILE__, __LINE__);
					writer->EndObject();
				}
				EndArray();
			}
			// samplers Mat4
			{
				writer->Key("samplersMat4");
				StartArray();
				for (size_t i = 0; i < object->u_Mat4.size(); i++)
				{
					writer->StartObject();
					auto obj = *object->u_Mat4[i];
					// Format: SamplerName-value(mat4)
					writer->Key("SamplerName");
					SerializeObj(obj.SamplerName.c_str(), __FUNCTION__, __FILE__, __LINE__);
					writer->Key("TextureName");
					SerializeObj(obj.TextureName.c_str(), __FUNCTION__, __FILE__, __LINE__);
					writer->Key("NeedlyAddAtr");
					SerializeObj(obj.needlyAddAtr, __FUNCTION__, __FILE__, __LINE__);
					writer->Key("Value");
					SerializeObj(obj.value, __FUNCTION__, __FILE__, __LINE__);
					writer->EndObject();
				}
				EndArray();
			}
			// samplers SceneParam
			{
				writer->Key("samplersSceneParams");
				StartArray();
				for (size_t i = 0; i < object->u_scene_params.size(); i++)
				{
					writer->StartObject();
					auto obj = *object->u_scene_params[i];
					// Format: SamplerName-value(mat4)
					writer->Key("SamplerName");
					SerializeObj(obj.SamplerName.c_str(), __FUNCTION__, __FILE__, __LINE__);
					writer->Key("TextureName");
					SerializeObj(obj.TextureName.c_str(), __FUNCTION__, __FILE__, __LINE__);
					writer->Key("NeedlyAddAtr");
					SerializeObj(obj.needlyAddAtr, __FUNCTION__, __FILE__, __LINE__);
					writer->Key("Value");
					SerializeObj(static_cast<unsigned int>(obj.value), __FUNCTION__, __FILE__, __LINE__);
					writer->EndObject();
				}
				EndArray();
			}
		}

		writer->EndObject();
	}

	/**
	*/
	template <> ENGINE_INLINE ENGINE_API
		void Serializer::SerializeObj(Material* object, const char* _function, const char* calledInFile, int lineId)
	{
		ASSERT(object, "Invalid obj");
		ASSERT(writer, "Invalid writer");

		if ((!object) || (!writer))
			return;

		writer->StartObject();
		{
			writer->Key("Roughness");
			SerializeObj(object->GetRoughness(), __FUNCTION__, __FILE__, __LINE__);
			writer->Key("Metalness");
			SerializeObj(object->GetMetalness(), __FUNCTION__, __FILE__, __LINE__);
			writer->Key("hasTexture");
			SerializeObj(object->IsHasTexture(), __FUNCTION__, __FILE__, __LINE__);
			writer->Key("hasNormal");
			SerializeObj(object->IsHasNormal(), __FUNCTION__, __FILE__, __LINE__);
			writer->Key("isTransparent");
			SerializeObj(object->IsTransparent(), __FUNCTION__, __FILE__, __LINE__);
			writer->Key("BaseColor");
			SerializeObj(object->GetBaseColor(), __FUNCTION__, __FILE__, __LINE__);

			// Serialize BRDF Model
			writer->Key("BRDF");
			SerializeObj(object->GetBRDF(), __FUNCTION__, __FILE__, __LINE__);

			// Passes
			{
				writer->Key("passes");

				StartArray();

				for (auto &pass : object->GetListOfPasses())
				{
					SerializeObj(pass.get(), __FUNCTION__, __FILE__, __LINE__);
				}

				EndArray();
			}

			writer->EndObject();
		}
	}
	//========================================================================================================================
}