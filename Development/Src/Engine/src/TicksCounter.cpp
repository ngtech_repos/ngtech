/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//***************************************************
#include "EnginePrivate.h"
//***************************************************
#if  PLATFORM_OS == PLATFORM_OS_WINDOWS
#include <windows.h>
#else
#include <sys/time.h>
#endif

namespace NGTech {
	/**
	Gets frames ticks
	\return ulTicks
	*/
	ENGINE_API unsigned long API_Window_GetTicks()
	{
#if  PLATFORM_OS == PLATFORM_OS_WINDOWS
		return GetTickCount();
#else
		struct timeval now;
		gettimeofday(&now, NULL);
		return (now.tv_sec) * 1000 + (now.tv_usec) / 1000;
#endif
	}
}