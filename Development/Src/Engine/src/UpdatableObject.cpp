#include "EnginePrivate.h"

#include "UpdatableObject.h"

#include "Scene.h"

namespace NGTech
{
	void UpdatableObject::_AddToSceneList()
	{
		MT::ScopedGuard guard(mutex);
		//Debug("UpdatableObject _AddToScene");
		auto scene = GetScene();
		ASSERT(scene, "Invalid scene");

		if (scene)
			scene->AddObject(this);
	}

	void UpdatableObject::_RemoveFromScene()
	{
		MT::ScopedGuard guard(mutex);
		//Debug("UpdatableObject::_RemoveFromScene");
		auto scene = GetScene();
		ASSERT(scene, "Invalid scene");

		if (scene)
			scene->DeleteUpdatableObjectFromList(this);
	}
}