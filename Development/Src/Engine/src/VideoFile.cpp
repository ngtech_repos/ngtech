#include "EnginePrivate.h"

#include "VideoFile.h"

namespace NGTech
{
	/******************************************************************************\
	*
	* YUV converters
	*
	\******************************************************************************/

	/*
	 */
	static void yuv_to_yuv(unsigned char *dest, const unsigned char *src_y, int src_y_stride, const unsigned char *src_u, const unsigned char *src_v, int src_uv_stride, int width, int height) {
		unsigned char *d = dest;

		for (int y = 0; y < height; y++) {
			const unsigned char *sy = src_y + src_y_stride * y;
			const unsigned char *su = src_u + src_uv_stride * (y >> 1);
			const unsigned char *sv = src_v + src_uv_stride * (y >> 1);

			for (int x = 0; x < width; x += 2) {
				*d++ = *sy++;
				*d++ = *su;
				*d++ = *sv;

				*d++ = *sy++;
				*d++ = *su++;
				*d++ = *sv++;
			}
		}
	}

	/*
	 */
	static void yuv_to_rgb(unsigned char *dest, const unsigned char *src_y, int src_y_stride, const unsigned char *src_u, const unsigned char *src_v, int src_uv_stride, int width, int height) {
		static int initialized = 0;
		static int y_table[256];
		static int rv_table[256];
		static int gv_table[256];
		static int gu_table[256];
		static int bu_table[256];
		static unsigned char clamp_table[256 * 3];

		if (initialized == 0) {
			initialized = 1;
			for (int i = 0; i < 256; i++) {
				y_table[i] = (int)(297.984f * i);
				rv_table[i] = (int)(408.576f * i + 8388.608f);
				gv_table[i] = (int)(-208.128f * i + 100532.224f);
				gu_table[i] = (int)(-100.352f * i);
				bu_table[i] = (int)(516.352f * i - 5439.488f);
			}
			for (int i = 0; i < 256; i++) {
				clamp_table[i + 0] = 0;
				clamp_table[i + 256] = (unsigned char)i;
				clamp_table[i + 512] = 255;
			}
		}

		unsigned char *d = dest;

		for (int y = 0; y < height; y++) {
			const unsigned char *sy = src_y + src_y_stride * y;
			const unsigned char *su = src_u + src_uv_stride * (y >> 1);
			const unsigned char *sv = src_v + src_uv_stride * (y >> 1);

			for (int x = 0; x < width; x += 2) {
				int y1 = y_table[*sy];
				int rv = rv_table[*sv];
				int gv = gv_table[*sv];
				int gu = gu_table[*su];
				int bu = bu_table[*su];

				int r = (y1 + rv) >> 8;
				int g = (y1 + gv + gu) >> 8;
				int b = (y1 + bu) >> 8;

				*d++ = clamp_table[r];
				*d++ = clamp_table[g];
				*d++ = clamp_table[b];

				sy++;

				y1 = y_table[*sy];

				r = (y1 + rv) >> 8;
				g = (y1 + gv + gu) >> 8;
				b = (y1 + bu) >> 8;

				*d++ = clamp_table[r];
				*d++ = clamp_table[g];
				*d++ = clamp_table[b];

				sy++;
				su++;
				sv++;
			}
		}
	}

	/*
	 */
	VideoFileTheora::VideoFileTheora() : file(NULL) {
		memset(&oy, 0, sizeof(oy));
		memset(&og, 0, sizeof(og));
		memset(&op, 0, sizeof(op));
		memset(&to, 0, sizeof(to));
		memset(&ti, 0, sizeof(ti));
		memset(&tc, 0, sizeof(tc));
		memset(&td, 0, sizeof(td));
	}

	VideoFileTheora::~VideoFileTheora() {
		ClearTheora();

		if (file) {
			file->Close();
			SAFE_DELETE(file); //-V809
		}
	}

	/*
	 */
	bool VideoFileTheora::ReadStream() {
		char *buf = ogg_sync_buffer(&oy, 4096);
		int size = (int)file->ReadAsSizeT(buf, sizeof(char), 4096);
		ogg_sync_wrote(&oy, size);
		return size;
	}

	bool VideoFileTheora::ReadPacket() {
		while (ogg_stream_packetout(&to, &op) <= 0) {
			if (ReadStream() == 0) {
				return false;
			}
			while (ogg_sync_pageout(&oy, &og) > 0) {
				ogg_stream_pagein(&to, &og);
			}
		}
		return true;
	}

	/*
	 */
	bool VideoFileTheora::InitTheora() {
		ogg_sync_init(&oy);
		ogg_stream_clear(&to);
		theora_info_init(&ti);
		theora_comment_init(&tc);

		int state = 0;
		int theora_p = 0;

		// parse headers
		while (state == 0) {
			if (ReadStream() == 0) {
				ErrorWrite("VideoFileTheora::init_theora(): can't read stream\n");
				ClearTheora();
				return 0;
			}
			while (ogg_sync_pageout(&oy, &og) > 0) {
				if (ogg_page_bos(&og) == 0) {
					if (theora_p) ogg_stream_pagein(&to, &og);
					state = 1;
					break;
				}
				ogg_stream_state stream;
				memset(&stream, 0, sizeof(stream));
				ogg_stream_init(&stream, ogg_page_serialno(&og));
				ogg_stream_pagein(&stream, &og);
				ogg_stream_packetout(&stream, &op);
				if (theora_p == 0 && theora_decode_header(&ti, &tc, &op) >= 0) {
					to = stream;
					theora_p = 1;
				}
				else {
					ogg_stream_clear(&stream);
				}
			}
		}

		// expecting more header packets
		while (theora_p && theora_p < 3) {
			int ret = ogg_stream_packetout(&to, &op);
			if (ret < 0) {
				ErrorWrite("VideoFileTheora::init_theora(): error parsing theora stream headers\n");
				ClearTheora();
				return 0;
			}
			if (ret > 0) {
				if (theora_decode_header(&ti, &tc, &op)) {
					ErrorWrite("VideoFileTheora::init_theora(): error decoding theora stream headers\n");
					ClearTheora();
					return 0;
				}
				if (++theora_p == 3) break;
			}
			if (ogg_sync_pageout(&oy, &og) > 0) {
				ogg_stream_pagein(&to, &og);
			}
			else if (ReadStream() == 0) {
				ErrorWrite("VideoFileTheora::init_theora(): can't read stream\n");
				ClearTheora();
				return 0;
			}
		}

		// check theora packets
		if (theora_p != 3) {
			ErrorWrite("VideoFileTheora::init_theora(): can't find theora stream\n");
			ClearTheora();
			return 0;
		}

		theora_decode_init(&td, &ti);

		ifps = (float)ti.fps_denominator / ti.fps_numerator;

		return true;
	}

	void VideoFileTheora::ClearTheora() {
		ogg_sync_clear(&oy);
		ogg_stream_clear(&to);
		theora_clear(&td);
		theora_info_clear(&ti);
		theora_comment_clear(&tc);

		ifps = Math::ZEROFLOAT;
	}

	/*
	 */
	bool VideoFileTheora::Load(const char *name) {
		file = new VFile(name);
		if (file->IsDataExist() == false)
		{
			ErrorWrite("VideoFileTheora::load(): can't open \"%s\" file\n", name); //-V111
			file->Close();
			SAFE_DELETE(file);
			return false;
		}

		if (InitTheora() == 0) {
			ErrorWrite("VideoFileTheora::load(): init_theora() == 0 \"%s\" file\n", name);
			file->Close();
			SAFE_DELETE(file);
			return false;
		}

		return true;
	}

	/*
	 */
	uint32_t VideoFileTheora::GetWidth() {
		return ti.width;
	}

	uint32_t VideoFileTheora::GetHeight() {
		return ti.height;
	}

	float VideoFileTheora::GetIFps() {
		return ifps;
	}

	/*
	 */
	bool VideoFileTheora::SetTime(float time) {
		float delta = GetTime() - time;
		if (delta < Math::ZEROFLOAT) delta = -delta;
		if (delta < ifps * 2.0f) return true;

		ClearTheora();

		// TODO:CHECK
		file->FSeek(0, SEEK_CUR); // Nick: SEEK_CUR ������� �

		if (InitTheora() == 0) return false;

		while (time > GetTime()) {
			if (Drop() == 0) return false;
		}

		return true;
	}

	float VideoFileTheora::GetTime() {
		return (float)theora_granule_time(&td, td.granulepos);
	}

	/*
	 */
	bool VideoFileTheora::ReadYUV(unsigned char *data) {
		if (ReadPacket() == 0) return false;

		if (theora_decode_packetin(&td, &op) == 0) {
			yuv_buffer yuv;
			if (theora_decode_YUVout(&td, &yuv) != 0) return false;
			yuv_to_yuv(data, yuv.y, yuv.y_stride, yuv.u, yuv.v, yuv.uv_stride, GetWidth(), GetHeight());
		}

		return true;
	}

	bool VideoFileTheora::ReadRGB(unsigned char *data) {
		if (ReadPacket() == 0) return false;

		if (theora_decode_packetin(&td, &op) == 0) {
			yuv_buffer yuv;
			if (theora_decode_YUVout(&td, &yuv) != 0) return false;
			yuv_to_rgb(data, yuv.y, yuv.y_stride, yuv.u, yuv.v, yuv.uv_stride, GetWidth(), GetHeight());
		}

		return true;
	}

	/*
	 */
	bool VideoFileTheora::Drop() {
		if (ReadPacket() == 0) return false;

		theora_decode_packetin(&td, &op);

		return true;
	}
}