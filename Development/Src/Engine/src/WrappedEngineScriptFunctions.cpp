/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "EnginePrivate.h"
#include "Scene.h"
#include "Cache.h"

#include "Object.h"
#include "ObjectMesh.h"
#include "AnimatedMesh.h"

#include "WrappedScriptFunctions.h"

namespace NGTech
{
	/*
	*/
	ENGINE_API void API_SetActiveCameraFOV(float _fov)
	{
		auto scene = GetScene();
		ASSERT(scene, "Not exist sceneManager");

		//Update client fov in config
		auto cvars = GetCvars();
		ASSERT(cvars && "Not exist Cvars");

		if (!cvars)
			return;

		cvars->cl_fov = _fov;

		if (!scene)
			return;

		auto cam = scene->m_ActiveScene.GetCurrentCamera();
		if (cam)
			cam->SetFOV(_fov);
	}

	ENGINE_API void API_EditorPauseEngine(bool _s)
	{
		if (_s)
		{
			SetEngineState(STATE_EDITOR_PAUSED);
			Debug("Editor paused");
		}
		else
		{
			SetEngineState(STATE_EDITOR_RUNNING);
			Debug("Editor running");
		}
	}

	/*
	*/
	static ENGINE_INLINE void Util_LoadModel(const String&_p)
	{
		auto ptr = std::make_shared<ObjectMesh>(_p);
		ptr->SetMaterial("defmat.mat");
		ptr->SetTransform(Mat4::translate(Vec3::ZERO));
	}

	static ENGINE_INLINE void Util_LoadMaterial(const String&_p)
	{
		auto cache = GetCache();
		ASSERT(cache, "Invalid pointer");
		if (cache)
			cache->LoadMaterial(_p);
	}

	/**
	*/
	ENGINE_API void API_EditorLoadEngineFormat(const String&_p) {
		DebugM("API_LoadEngineFormat:File %s", _p.c_str(), __FILE__, __LINE__);

		if (_p.empty())
		{
			Error("Invalid path", false);
			return;
		}

		String out;
		String in(_p);
		StringHelper::getExtension(out, in);

		if (out == "nggf")
			Util_LoadModel(_p);
		else if (out == "scene")
			API_LoadSceneNew(_p, true);
		else if (out == "mat") {
			Util_LoadMaterial(_p);
		}
		else
			Warning("[%s] Unknown  extension: %s nothing be loaded", __FUNCTION__, out.c_str());

		out.clear();
		in.clear();
	}

	ENGINE_API void API_EditorSaveEngineFormat(const char*_p) {
		DebugM("API_EditorSaveEngineFormat:File %s", _p, __FILE__, __LINE__);

		String out;
		String in(_p);
		StringHelper::getExtension(out, in);

		if (out == "scene" || out == "bak")
			API_SaveScene(_p);
		if (out == "mat")
			API_SaveMaterial(_p);
		else
			Warning("[%s] Unknown  extension: %s nothing be saved", __FUNCTION__, out.c_str());

		out.clear();
		in.clear();
	}
}