/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include "../Common/IGame.h"
#include "DLLx.h"

namespace NGTech
{
	class ObjectMesh;
	class AnimatedMesh;
	class Camera;
	class CameraFPS;
	class CameraFree;
	class CameraFixed;
	class LightPoint;
	class LuaScript;
}

namespace ExampleGameModule
{
	using namespace NGTech;
	//------------------------------------------------------------
	class GAME_API ExampleGame : public IGame
	{
	public:
		virtual void Initialise() override;
		virtual void Update() override;
		virtual void Render() override {}
		virtual const char* ResourceDirectory() override { return "ExampleGame"; }
		virtual void StartGamePlay() override;
	private:
		LuaScript* m_StartUpScript = nullptr;
		class GameGUIEvents*events = nullptr;
		bool m_GameplayRunned = false;
	};
	//------------------------------------------------------------
	class GAME_API RenderCallback : public NGTech::ICallback
	{
	public:
		virtual void Body() override {  }
	};
	//------------------------------------------------------------
	class GAME_API EventsCallback : public NGTech::ICallback
	{
	public:
		EventsCallback();
		virtual void Body() override;
	};
}