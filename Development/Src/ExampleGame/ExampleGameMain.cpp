/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "stdafx.h"

#include "../Common/IGame.h"
#include "../Engine/inc/DLLDef.h"
#include "../Engine/inc/EngineAppBase.h"
#include "ExampleGame.h"

GAME_API void ExampleGameStart();
GAME_API void ExampleGameStartEditor();

namespace NGTech {
	struct ICallback;
	GAME_API void EngineStart(IGame*_game = nullptr, ICallback *rc = nullptr, ICallback *ev = nullptr) {
		EngineAppBase(_game, rc, ev);
	}

	GAME_API void GameEntryPoint()
	{
		ExampleGameStart();
	}

	GAME_API void GameEntryPointEditor()
	{
		ExampleGameStartEditor();
	}
}

GAME_API void ExampleGameStart() {
	using namespace ExampleGameModule;
	// Enable run-time memory check for debug builds.
	EngineStart(new ExampleGame(), new RenderCallback(), new EventsCallback());
}

GAME_API void ExampleGameStartEditor() {
	using namespace ExampleGameModule;
	using namespace NGTech;

	auto engine = GetEngine();
	ASSERT(engine, __FUNCTION__);

	auto _game = new ExampleGame();
	auto _rc = new RenderCallback();
	auto _ev = new EventsCallback();

	_game->Initialise();

	if (_rc&&_game)
		_game->SetRenderCallback(_rc);

	if (_ev&&_game)
		_game->SetEventsCallback(_ev);

	engine->SetGame(_game);
}