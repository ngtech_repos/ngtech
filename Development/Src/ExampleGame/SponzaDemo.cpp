/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "stdafx.h"
#include "../../API/NGTechEngineAPI.h"

#define SPONZA_DEMO

#ifdef SPONZA_DEMO

using namespace NGTech;

#include "GameGUI.h"
#include "../Core/inc/LuaScript.h"

#include "../Engine/inc/PhysicsMaterialsManager.h"
#include "../Engine/inc/PhysXMaterialSh.h"

namespace ExampleGameModule
{
	void ExampleGame::Update() {
		/*BROFILER_CATEGORY("UpdateGame", Brofiler::Color::LightYellow);
		PROFILE;*/

		//Debug("ExampleGame::Update");

		if (m_GameplayRunned == false)
			StartGamePlay();
	}

	void ExampleGame::Initialise()
	{
		events = new GameGUIEvents();

		GetWindow()->GrabMouse(true);

		m_StartUpScript = LuaScript::RunGameEntryPoint("_G.lua");
	}
	//------------------------------------------------------------

	void ExampleGame::StartGamePlay() {
		//Debug("ExampleGame::StartGamePlay");

		ASSERT(m_StartUpScript, "m_StartUpScript is null");
		m_StartUpScript->CallSpecificFunction("StartGamePlay", "_G.lua");
		m_GameplayRunned = true;
	}

	//------------------------------------------------------------
	EventsCallback::EventsCallback() {}
	//------------------------------------------------------------

	void EventsCallback::Body() {
		auto engine = GetEngine();
		auto window = engine->iWindow;

		if (!window)
			return;

		if (window->IsKeyDown("G"))
		{
			static auto res = ALSoundSource::CreateWithBackGroundMusicPreset(new ALSound("stream_stereo_00.ogg"));
			res->Play();
		}

		if (window->IsKeyDown("ESC")) {
			engine->Quit();
			return;
		}

		if (window->IsKeyDown("F12"))
			API_Make_ScreenShot();

		//======================================
		if (window->IsKeyDown("C")) {
			auto mat = Cache::GetMaterialNew("gold_pbr.mat");
			PhysXMaterialSh* phm = phMatManager.AllocMaterial(Math::ONEDELTA, Math::ONEDELTA, Math::ONEDELTA, "gold_pbr.phmat", true);
			phm->SetImpactSound(new ALSound("impact.ogg", true));
			mat->AttachPhysXMaterialByPtr(phm);

			auto mesh = new ObjectMesh("cube.nggf");
			mesh->SetMaterial(mat, "*");
			mesh->SetPhysicsBox(Vec3::ONE, 100);
			mesh->SetPosition(Vec3(0, 200, 0));
		}

		if (window->IsKeyDown("Z"))
		{
			auto light = new LightEnvProbe();
			light->SetDirection(Vec3(-0.2f, -1.0f, -0.3f));
			light->SetColor(Color(2.f, 2.f, 2.f, 0.0f));
		}
	}
	//------------------------------------------------------------
}
#endif