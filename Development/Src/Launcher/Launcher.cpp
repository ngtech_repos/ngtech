/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "stdafx.h"
#include "Launcher.h"

// only for developer mode
#if !defined(ENGINE_RELEASE) && !defined(NGTECH_STATIC_LIBS) && (!defined(_FINALRELEASE) || defined(EMULATE_DEVELOPER_FINAL_RELEASE))
#define NGTECH_STATIC_LIBS 1
#endif

#ifdef NGTECH_STATIC_LIBS
extern "C"
{
	GAME_API void dllStartPlugin(void) throw();
}
#endif

int RunGame(int argc, char **argv);

namespace NGTech
{
	void API_EngineAutoStart(int argc, const char * const * argv);
}

#if PLATFORM_OS == PLATFORM_OS_WINDOWS && (!defined(_DEBUG))
#include <windows.h>
#include <shellapi.h>

/**
*/
class LiveCreateMutex
{
private:
	HANDLE m_hMutex;

public:
	LiveCreateMutex(bool bCreateMutex)
		: m_hMutex(NULL)
	{
		if (bCreateMutex)
		{
			// create PC only mutex that signals that the game is running
			// this is used by LiveCreate to make sure only one instance of game is running
			const char* szMutexName = "Global\\LiveCreatePCMutex";
			m_hMutex = ::CreateMutexA(NULL, FALSE, szMutexName);
			if (NULL == m_hMutex)
			{
				// mutex may already exist, then just open it
				m_hMutex = ::OpenMutexA(MUTEX_ALL_ACCESS, FALSE, szMutexName);
			}
		}
	}

	~LiveCreateMutex()
	{
		if (NULL != m_hMutex)
		{
			::CloseHandle(m_hMutex);
			m_hMutex = NULL;
		}
	}
};

int WINAPI WinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPTSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	const bool bCreateMutex = (NULL == strstr(lpCmdLine, "-nomutex"));
	LiveCreateMutex liveCreateMutex(bCreateMutex);

	int argCount = 0;
	{
		LPWSTR *szArgList = nullptr;
		szArgList = CommandLineToArgvW(GetCommandLineW(), &argCount);
		LocalFree(szArgList);
	}

	return RunGame(argCount, &lpCmdLine);
}
#endif

//-------------------------------------------------------------
int main(int argc, char **argv) {
	return RunGame(argc, argv);
}

int RunGame(int argc, char **argv)
{
	using namespace NGTech;
	// load the game dll
	API_EngineAutoStart(argc, argv);
	return 0;
}