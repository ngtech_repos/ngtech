#pragma once

//***************************************************************************
#include "../Common/MathLib.h"
#include "../Common/IRender.h"
//***************************************************************************
namespace NGTech
{
	struct CoreManager;
	/**/
	class NULLDrv  final :public I_RenderLowLevel
	{
	public:
		/**
		*/
		virtual String GetRenderName() override { return "NULLDrv"; }

		virtual void EnableWireframeMode() override {}
		virtual void DisableWireframeMode() override {}
	public:
		explicit NULLDrv(CoreManager*) {}
		virtual ~NULLDrv() {}

		virtual void Initialise() override {}

		virtual void Reshape(uint32_t width, uint32_t height, Mat4&proj) override {}
		virtual void Reshape(uint32_t width, uint32_t height) override {}
		virtual void GetViewport(int *viewport) override {}

		virtual void ClearColor(Vec3 color) override {}

		virtual void ColorMask(bool r, bool g, bool b, bool a) override {}

		virtual void Clear(RenderBuffer buffers) override {}

		virtual void SetViewport(unsigned int w, unsigned int h) override {}
		virtual void SetViewport(unsigned int x, unsigned int y, unsigned int w, unsigned int h) override {}

		//----------2D/3D-mode----
		virtual I_Drawable* CreateReact() override { return nullptr; }

		//---Blending-------------------------------
		virtual void BlendFunc(BlendParam src, BlendParam dst) override {}
		virtual void EnableBlending() override {}
		virtual void EnableBlending(BlendParam src, BlendParam dst) override {}
		virtual void DisableBlending() override {}

		/**
		Depth-Buffer
		*/
		virtual void DepthFunc(CompareType type) override {}
		virtual void EnableDepth(CompareType type) override {}
		virtual void EnableDepth() override {}
		virtual void DisableDepth() override {}
		virtual void DepthMask(bool mask) override {}
		/**
		Stencil test
		*/
		virtual void DisableStencilTest() override {}
		virtual void EnableStencilTest() override {}
		/**
		Z-Buffer
		*/
		virtual void PolygonOffsetFill(float a, float b) override {}
		virtual void EnablePolygonOffsetFill(float a, float b) override {}
		virtual void EnablePolygonOffsetFill() override {}
		virtual void DisablePolygonOffsetFill() override {}
		/**
		Culling
		*/
		virtual void SetPolygonFront(CullType type) override {}
		virtual void SetPolygonCull(CullFace face) override {}
		virtual void EnableCulling(CullType type) override {}
		virtual void EnableCulling(CullFace face) override {}
		virtual void EnableCulling() override {}
		virtual void DisableCulling() override {}

		//---Clip-plains--------------------------
		virtual void ClipPlane(const Vec4 &plain, int plainNum) override {}
		virtual void EnableClipPlane(int plainNum) override {}
		virtual void EnableClipPlane(const Vec4 &plain, int plainNum) override {}
		virtual void DisableClipPlane(int plainNum) override {}

		virtual void WriteScreenshot(const char* path) const override {}

		/**
		Compute shader support check
		*/
		virtual bool IsComputeSupported() override { return false; }

		/*FRAMES*/
		virtual void BeginFrame() override {}
		virtual void EndFrame() override {}

		virtual void ClearDepthStencilColor() override {}
		virtual void ClearDepthColor() override {}
		// clear depths to 1.0
		virtual void ClearDepthOnly() override {}
		virtual void ClearColorOnly() override {}
		/*
		Says driver end of frame
		*/
		virtual void FlushGPU() override {}

	private:

		/**
		��������� �������
		*/
		virtual void ProcessQueue() override {}

		//FABRICS
		virtual I_OcclusionQuery* GetOQ() override { return nullptr; }

		virtual I_Texture* TextureCreate(const std::shared_ptr<TextureParamsTransfer>&_obj) override { return nullptr; }

		virtual I_Texture* TextureCreate2DUtil(const String &path, const std::shared_ptr<TextureParamsTransfer>&textP) override { return nullptr; }
		virtual I_Texture* TextureCreateCubeUtil(const String &path) override { return nullptr; }
		virtual I_Texture* TextureCreateCubeFromFileUtil(const String&path) override { return nullptr; }

		virtual I_Texture *TextureCreate2DUtil(uint32_t width, uint32_t height, const Format& format, const String& path) override { return nullptr; }
		virtual I_Texture *TextureCreate3DUtil(uint32_t width, uint32_t height, uint32_t depth, const Format& format, const String& path) override { return nullptr; }
		virtual I_Texture *TextureCreateCubeUtil(uint32_t width, uint32_t height, const Format& format, const String& path) override { return nullptr; }

		virtual I_Shader *ShaderCreate(const String &path, const String &defines = "", bool allowShaderCache = true) override { return nullptr; }
		virtual I_VBManager *CreateIBO(void *data, uint32_t numElements, DataType dataType) override { return nullptr; }
		virtual I_VBManager *CreateVBO(void *data, uint32_t numElements, uint32_t elemSize, DataType dataType, TypeDraw drawType) override { return nullptr; }
		virtual I_RenderTarget *CreateFBO(unsigned int x, unsigned int y) override { return nullptr; }

		virtual I_ModelHelper* GetModelHelper() override { return nullptr; }
	};
}