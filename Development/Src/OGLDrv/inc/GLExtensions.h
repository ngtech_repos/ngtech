/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#if !IS_OS_ANDROID

#include "../../../Externals/RenderAdditions/glew/GL/glew.h"
#if PLATFORM_OS == PLATFORM_OS_WINDOWS
#include "../../../Externals/RenderAdditions/glew/gl/wglew.h"
#else
#include "../../../Externals/RenderAdditions/glew/GL/glxew.h"
#endif

#else

#include "../../../Externals/RenderAdditions/EGL/egl.h"
#include "../../../Externals/RenderAdditions/GLES3/gl3.h"
#include "../../../Externals/RenderAdditions/GLES3/gl31.h"
#include "../../../Externals/RenderAdditions/GLES3/gl32.h"

#endif
//***************************************************************************
#include "RenderDLL.h"
#include "VersionLayer.h"
//***************************************************************************

namespace NGTech
{	/**
	*/
	struct OpenGLVersionManager :public Singleton<OpenGLVersionManager>
	{
		ENGINE_INLINE OpenGLVersionBase GetSelectedProfile() {
			return m_SeletectedProfile;
		}

		ENGINE_INLINE void ChangeMode(uint32_t _major, uint32_t _minor, bool openGLES)
		{
			if (_major == 4 && _minor == 6 && openGLES == false) //-V112
				m_SeletectedProfile = OpenGL46();
			if (_major == 4 && _minor == 5 && openGLES == false) //-V112
				m_SeletectedProfile = OpenGL45();
			else if (_major == 4 && _minor == 4 && openGLES == false) //-V112
				m_SeletectedProfile = OpenGL44();
			else if (_major == 4 && _minor == 3 && openGLES == false) //-V112
				m_SeletectedProfile = OpenGL43();
			else if (_major == 4 && _minor == 2 && openGLES == false) //-V112
				m_SeletectedProfile = OpenGL42();

			if (openGLES)
				m_SeletectedProfile = OpenGL_ES31();
		}

		// Default value is OpenGL 3.3
		OpenGLVersionBase m_SeletectedProfile;
	};

	/**
	OpenGL extensions managing class
	*/
	struct HardwareRenderFeatureManager
	{
		/**
		Is GL extension supported
		\param name extension name
		\return true if supported
		*/
		static bool IsExtSupported(const char*name);
		/**
		Init extensions
		*/
		static void InitExtensions(I_RenderLowLevel* _sys);

		/**
		Checking support of extensions
		*/
		static void DeterminateRenderMode(I_RenderLowLevel* _sys);
	};
}