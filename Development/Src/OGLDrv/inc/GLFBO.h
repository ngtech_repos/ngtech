/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//***************************************************************************
#include "MathLib.h"
//***************************************************************************

namespace NGTech {
	class GLFBO final :public I_RenderTarget
	{
	public:
		static GLFBO *Create(unsigned int width, unsigned int height);
		virtual ~GLFBO();

		virtual void Set() override;

		/**
		unset GLFBO
		*/
		virtual void Unset() override;

		/**
		Clears the frame buffer
		*/
		virtual void Clear() override;

		/**
		Clears the frame buffer only depth
		*/
		virtual void ClearDepthOnly() override;

		/**
		Clears the frame color only depth
		*/
		virtual void ClearColorOnly() override;
		/**
		Clears the frame depth and stencil
		*/
		virtual void ClearDepthAndStencil() override;

		// TODO:DEPRECATED
		virtual bool AttachColorTexture(unsigned int _index) override;
		virtual bool AttachColorTexture(unsigned int _index, const Format& format) override;
		virtual void BindColorTexture(unsigned int _index) override;
		virtual void BindColorTexture(unsigned int _as, unsigned int _index) override;

		// TODO:DEPRECATED
		virtual bool AttachDepthTexture() override;

		/**
		*/
		virtual void BindDepthTexture() override;
		virtual void BindDepthTexture(unsigned int _index) override;

		virtual void UnBindActiveTexture(unsigned int tex_unit) override;
		virtual bool Validate() override;

		virtual void ReattachColor(unsigned int target, int face, int level) override;
		virtual void ReattachColor(unsigned int target, int level) override;

		virtual void ReattachDepth(int face, int level) override;
		virtual void ReattachDepth(int level) override;
		virtual void ReattachDepthStencil(int face, int level) override;
		virtual void ReattachDepthStencil(int level) override;

		/* !@ Get blit depth from current frame-buffer to main frame-buffer */
		/* !@ ����� ������ ������ BlitDepth*/
		virtual void Resolve(I_RenderTarget* to, unsigned int bitmask) override;

		virtual void ReadBuffer(unsigned int _texIndex) override;
		virtual void WriteBuffer(unsigned int _texIndex) override;
	private:
		GLFBO();
		bool _AttachRenderbuffer(unsigned int target, const Format& _format);
		bool _AttachTexture(unsigned int target, const Format& format, const Filter& filter = Filter::NEAREST);
		void _Reattach(unsigned int target, int face, int level);
		void _Reattach(unsigned int target, int level);

		ENGINE_INLINE GLuint GetColorAttachment(int index) const {
			return rendertargets[index].id;
		}

		ENGINE_INLINE GLuint GetDepthAttachment() const {
			return depthstencil.id;
		}

		virtual unsigned int GetUID() const override { return fboid; }

		/*If fboid = -1(my defined default value), when this buffer is not correct*/
		bool _IsInvalid();

		bool IsAlreadyBound();
		bool IsAlreadyBindedForRead(int _check);
		bool IsAlreadyBindedForWrite(int _check);
	private:
		struct Attachment
		{
			GLuint id;
			int type;

			Attachment()
				: id(0), type(0) {}
		};
	private:
		GLuint fboid = (GLuint)-1;

		/*Actual*/
		Attachment	rendertargets[8];
		Attachment	depthstencil;
	};
}