/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//***************************************************************************
#include "MathLib.h"
//***************************************************************************

namespace NGTech {
	/**
	����� ��������, ����� ���� ��� Occlusion Query, ����� ���� ��� Timer(opengl4.5)
	*/
	class GLQuery
	{
	public:
		GLQuery(GLuint target);
		~GLQuery();

		// clear query
		void Clear();

		// render destroy
		void Destroy();

		// query begin/end
		void BeginQuery();
		void EndQuery();

		// query result
		bool IsCompleted();
		unsigned int GetResult();
		long long GetResultLong();

		// get query
		GLuint GetTypeQuery() const;
		GLuint GetQueryID() const { return query_id; }

		GLQuery() :type(0), query_id(0), result(0) {}
	private:
		GLuint type;
		GLuint query_id;
		unsigned int result;
	};

	/**
	Implementation of Hardware Occlusion Query
	*/
	class GLOcclusionQuery final :public I_OcclusionQuery
	{
	public:
		GLOcclusionQuery();

		/**
		*/
		virtual ~GLOcclusionQuery() {}

		/**
		Begins rendering to query
		*/
		virtual void BeginQuery() override;

		/**
		End rendering to query
		*/
		virtual void EndQuery() override;

		/**
		Draw only if one sample went through the tests,
		\we don't need to get the query result which prevent the rendering pipeline to stall.
		This function is starts conditional render
		*/
		virtual void BeginConditionalRender() override;

		/**
		Draw only if one sample went through the tests,
		\we don't need to get the query result which prevent the rendering pipeline to stall.
		This function is ends conditional render
		*/
		virtual void EndConditionalRender() override;

		/**
		Returns is completed query
		\return bool
		*/
		bool IsCompleted();
	private:
		GLQuery query;
	};
}