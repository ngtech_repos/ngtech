/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//***************************************************************************
#include "RenderDLL.h"
#include "ShaderContainer.h"
//***************************************************************************

namespace NGTech
{
	/**
	Used for contains shader texts
	\and for manipulations with versions
	*/
	struct ShaderContainer;

	/**
	Desc: GLSL Shader
	*/
	class RENDER_API GLShader final : public I_Shader
	{
	public:

		/**
		Creates new GLShader
		\param path shader file path
		\defines shader code defines
		\return pointer to the new GLShader
		*/
		static GLShader *Create(const String &path, const String &defines = "", bool allowSaveShaderCache = true);

		/**
		������� ������ �� ����
		*/
		GLShader(const String& _path)
			:GLShader() {
			SetPath(_path);
		}

		/**
		*/
		GLShader();

		/**
		Destroys  GLShader
		*/
		virtual ~GLShader();

		/**
		Sets shader
		*/
		virtual void Enable() override;

		/**
		Unsets shader
		*/
		virtual void Disable() override;

		/**
		Recompile shader:
		Deletion older shader cache
		Compile new shader
		Saving cache
		*/
		virtual void Recompile(const String& _shaderSpecificDefs, const String& _sceneSpecificDefs) override;

		/**
		Sends uniform param to shader
		\param name parameter name
		\param value param value
		*/
		virtual void SendMat4(const String &name, const Mat4 &value) override;

		/**
		Sends uniform param to shader
		\param name parameter name
		\param value param value
		*/
		virtual void SendVec4(const String &name, const Vec4 &value) override;

		/**
		Sends uniform param to shader
		\param name parameter name
		\param value param value
		*/
		virtual void SendVec3(const String &name, const Vec3 &value) override;

		/**
		Sends uniform param to shader
		\param name parameter name
		\param value param value
		*/
		virtual void SendVec2(const String &name, const Vec2 &value) override;

		/**
		Sends uniform param to shader
		\param name parameter name
		\param value param value
		*/
		virtual void SendFloat(const String &name, float value) override;

		/**
		Sends uniform param to shader
		\param name parameter name
		\param value param value
		*/
		virtual void SendInt(const String &name, int value) override;

		/**
		*/
		virtual void CommitChanges() override;

		/**
		*/
		virtual void DispatchCompute(unsigned int num_groups_x, unsigned int num_groups_y, unsigned int num_groups_z) override;

		/**
		*/
		virtual void AddDefines(const String &_defines) override;

		GLuint GetProgramID() { return this->program; }

		void SetProgramID(GLuint _programId) {
			this->program = _programId;
		}

		GLuint GetPipelineNameID() {
			return PipelineName;
		}

		void SetPipelineNameID(GLuint _PipelineName) {
			PipelineName = _PipelineName;
		}

		bool IsAllowShaderCache() {
			return allowShaderCache;
		}

		/**
		��������� ��������� �������� �������, � ������������� ��� �������
		*/
		bool CheckLinked(GLuint _programId, GLuint _PipelineName, const char*path, bool _showError);
		/**
		�������� ���������, ��������������� ������, ��������� ���������, � ������������� ��� �������
		*/
		bool CheckLinkedAndBind(GLuint _programId, GLuint _PipelineName, const char*path, bool _showError);

		void SetLoaded(bool _loaded) {
			bLoaded = _loaded;
		}

		GLuint GetShader_GS_ID() const {
			return gs;
		}

		void SetShader_GS_ID(GLuint _gs) {
			gs = _gs;
		}

		GLuint GetShader_VS_ID() const {
			return vs;
		}

		void SetShader_VS_ID(GLuint _vs) {
			vs = _vs;
		}

		GLuint GetShader_FS_ID() const {
			return fs;
		}

		void SetShader_FS_ID(GLuint _fs) {
			fs = _fs;
		}

		GLuint GetShader_CS_ID() const {
			return cs;
		}

		void SetShader_CS_ID(GLuint _cs) {
			cs = _cs;
		}

		GLuint GetShader_TCS_ID() const {
			return tcs;
		}

		void SetShader_TCS_ID(GLuint _tcs) {
			tcs = _tcs;
		}

		GLuint GetShader_TES_ID() const {
			return tes;
		}

		void SetShader_TES_ID(GLuint _tes) {
			tes = _tes;
		}

		/**
		Saving shader cache in user directory
		*/
		bool SaveToShaderCache(const char* path);

		const std::shared_ptr<ShaderContainer>& GetContainer() const { return contaner; }

	private:

		void _AddUniform(const char* name, GLuint location, GLuint count, GLenum type);
		void _QueryUniforms(GLuint _program);
		/**
		An indexer that returns the location of the attribute/uniform
		*/
		GLuint operator[](const String& attribute);

		/**
		An indexer that returns the location of the attribute/uniform
		*/
		GLuint operator[](const char* attribute);

		/**
		Creation shader functions
		*/
		bool _CreateShader(const String &path, const String &_shaderSpecificDefs = "", const String &_sceneSpecificDefs = "", bool save = true);

		/**
		*/
		ENGINE_INLINE bool _IsLoaded() { return bLoaded; }

		/**
		������������ ��� ����������:
		glDeleteProgram(pr);
		glDeleteProgramPipelines(1, &pipe);
		*/
		void _DeleteProgramAndPipeline(GLuint pr, GLuint pipe);

		void _Clean();
		/**
		*/
	private:

		/**
		*/
		void _BindAttributes(GLuint _programId);

	private:
		struct Uniform
		{
			char	Name[32];
			GLint	StartRegister;
			GLint	RegisterCount;
			GLint	Location;
			GLenum	Type;

			mutable bool Changed;

			ENGINE_INLINE bool operator <(const Uniform& other) const {
				return (0 > strcmp(Name, other.Name));
			}
		};

		typedef orderedarray<Uniform>		uniformtable;
		uniformtable						uniforms;
		float*								floatvalues = nullptr;
		int*								intvalues = nullptr;
		std::shared_ptr<ShaderContainer>	contaner = nullptr;

		GLuint								floatcap = 0, floatsize = 0, intcap = 0,intsize = 0, PipelineName = 0, gs = 0, vs = 0, fs = 0, cs = 0, tcs = 0, tes = 0, program = 0, mShaderId = 0;
		bool								allowShaderCache = false, bLoaded = false, bLinked = false;
		std::map<String, GLuint> _attributeList;
	private:
		friend struct ShaderUtils;
	};
}