/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include <vector>
//***************************************************************************
#include "MathLib.h"
#include "GLExtensions.h"
#include "GLTexture.h"
#include "GLShader.h"
#include "GLFBO.h"
#include "GLCommons.h"
#include "GLOcclusionQuery.h"
#include "RenderDLL.h"
#include "GL_VertexBuffer.h"
#include "GL_IndexBuffer.h"
#include "GL_StorageBuffer.h"
#include "GL_AtomicBuffer.h"
#include "RenderContextBase.h"
//***************************************************************************

namespace NGTech
{
	class Engine;
	class ModelHelper;
	class OGL_ScreenQuad;
	//---------------------------------------------------------------------------
	//Desc: Engine`s main video system. Created one time
	//---------------------------------------------------------------------------
	class RENDER_API GLSystem final :public I_RenderLowLevel
	{
	public:
		/**
		*/
		virtual String GetRenderName() override { return "OpenGL"; }

		void EnableMultiSample(bool);
		void EnableVSync(bool);

		void EnableDebugOutput();
		void DisableDebugOutput();
		virtual void EnableWireframeMode() override;
		virtual void DisableWireframeMode() override;
	public:
		explicit GLSystem(CoreManager*);
		virtual ~GLSystem();

		virtual void Initialise() override;

		virtual void Reshape(uint32_t width, uint32_t height, Mat4&proj) override;
		virtual void Reshape(uint32_t width, uint32_t height) override;
		virtual void GetViewport(int *viewport) override;

		virtual void ClearColor(Vec3 color) override;

		virtual void ColorMask(bool r, bool g, bool b, bool a) override;

		virtual void Clear(RenderBuffer buffers) override;

		virtual void SetViewport(unsigned int w, unsigned int h) override;
		virtual void SetViewport(unsigned int x, unsigned int y, unsigned int w, unsigned int h) override;

		//----------2D/3D-mode----
		virtual I_Drawable* CreateReact() override;

		//---Blending-------------------------------
		virtual void BlendFunc(BlendParam src, BlendParam dst) override;
		virtual void EnableBlending() override;
		virtual void EnableBlending(BlendParam src, BlendParam dst) override;
		virtual void DisableBlending() override;

		/**
		Depth-Buffer
		*/
		virtual void DepthFunc(CompareType type) override;
		virtual void EnableDepth(CompareType type) override;
		virtual void EnableDepth() override;
		virtual void DisableDepth() override;
		virtual void DepthMask(bool mask) override;
		/**
		Stencil test
		*/
		virtual void DisableStencilTest() override;
		virtual void EnableStencilTest() override;
		/**
		Z-Buffer
		*/
		virtual void PolygonOffsetFill(float a, float b) override;
		virtual void EnablePolygonOffsetFill(float a, float b) override;
		virtual void EnablePolygonOffsetFill() override;
		virtual void DisablePolygonOffsetFill() override;
		/**
		Culling
		*/
		virtual void SetPolygonFront(CullType type) override;
		virtual void SetPolygonCull(CullFace face) override;
		virtual void EnableCulling(CullType type) override;
		virtual void EnableCulling(CullFace face) override;
		virtual void EnableCulling() override;
		virtual void DisableCulling() override;

		//---Clip-plains--------------------------
		virtual void ClipPlane(const Vec4 &plain, int plainNum) override;
		virtual void EnableClipPlane(int plainNum) override;
		virtual void EnableClipPlane(const Vec4 &plain, int plainNum) override;
		virtual void DisableClipPlane(int plainNum) override;

		virtual void WriteScreenshot(const char* path) const override;

		ENGINE_INLINE bool IsDebugOutputEnabled() const { return debugOutputEnabled; }

		/**
		Compute shader support check
		*/
		virtual bool IsComputeSupported() override;

		/*FRAMES*/
		virtual void BeginFrame() override;
		virtual void EndFrame() override;

		virtual void ClearDepthStencilColor() override;
		virtual void ClearDepthColor() override;
		// clear depths to 1.0
		virtual void ClearDepthOnly() override;
		virtual void ClearColorOnly() override;
		/*
		Says driver end of frame
		*/
		virtual void FlushGPU() override;

	private:

		/**
		��������� �������
		*/
		virtual void ProcessQueue() override;

		bool _CreateContext(I_Window*_toWindow);
		/*
		OpenGL info
		*/
		const char * _GetVendor();
		const char * _GetRenderer();
		const char * _GetVersion();
		const char* _GetGLSLVersion();
		/*
		Set default options
		*/
		void _RenderSetDefaults();

		//---Draw-Geom-Stream---------------------
		void _DrawElements(DrawMode _dmMode, void *_indices, unsigned int _indexCount,
			unsigned int _instance_count = 1,
			unsigned int _base_instance = 0);
		void _DrawArrays(DrawMode _dmMode, unsigned int _vertexCount,
			unsigned int _instance_count = 1,
			unsigned int _base_instance = 0);

		//FABRICS
		virtual GLOcclusionQuery* GetOQ() override;

		virtual GLTexture* TextureCreate(const std::shared_ptr<TextureParamsTransfer>&_obj) override;

		virtual GLTexture* TextureCreate2DUtil(const String &path, const std::shared_ptr<TextureParamsTransfer>&textP) override;
		[[deprecated]]
		virtual GLTexture* TextureCreateCubeUtil(const String &path) override;
		virtual GLTexture* TextureCreateCubeFromFileUtil(const String&path) override;

		virtual GLTexture *TextureCreate2DUtil(uint32_t width, uint32_t height, const Format& format, const String& path) override;
		virtual GLTexture *TextureCreate3DUtil(uint32_t width, uint32_t height, uint32_t depth, const Format& format, const String& path) override;
		virtual GLTexture *TextureCreateCubeUtil(uint32_t width, uint32_t height, const Format& format, const String& path) override;

		virtual GLShader *ShaderCreate(const String &path, const String &defines = "", bool allowShaderCache = true) override;
		virtual OGL_IndexBuffer *CreateIBO(void *data, uint32_t numElements, DataType dataType) override;
		virtual OGL_VertexBuffer *CreateVBO(void *data, uint32_t numElements, uint32_t elemSize, DataType dataType, TypeDraw drawType) override;
		virtual GLFBO *CreateFBO(unsigned int x, unsigned int y) override;

		virtual I_ModelHelper* GetModelHelper() override;
	private:
		friend class GLTexture;
		friend class GLVBO;
		friend class GLShader;
		friend class ModelHelper;
	private:
		//!@todo Refactor this
		Vec4 mCurrentViewport;
		CullFace polygon_cull;
		CullType polygon_front;
		CompareType	depth_func;
		bool debugOutputEnabled;

		OpenGLVersionManager m_APIVersionLayer;
	private:
		CoreManager * engine = nullptr;
		std::weak_ptr<CVARManager>			m_WeakCvars;
		std::shared_ptr<RenderContextBase>	m_RenderContext = nullptr;
	};
}