/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//***************************************************************************
#include "../Common/IRender.h"
//***************************************************************************
#include "ILImage.h"
//***************************************************************************

namespace NGTech {
	/*��������� ������� ������ ������ �� ��������*/
	// ��� ������������ ������ � ������� - �����
	struct GLTextureData final
	{
		/*Texture target enum*/
		TextureTarget selectedTarget = TextureTarget::TEXTURE_ZERO;

		/*Texture anisotropy*/
		Aniso aniso = Aniso::ANISO_X0;

		uint32_t Width = 0;
		uint32_t Height = 0;
		uint32_t Depth = 0;

		uint32_t glID = 0;
		uint32_t Target = 0;

		//!@todo ��� �� ������?
		uint32_t minFilter = 0;
		uint32_t magFilter = 0;

		uint32_t _minMipLevel = 0;
		uint32_t _maxMipLevel = 0;

		uint32_t Wrap = 0;

		//!@todo ��� �� ������?
		uint32_t internalFormat = 0;
		uint32_t srcFormat = 0;

		uint32_t dataType = 0;
	};

	/**
	Texture class
	*/
	class GLTexture final :public I_Texture
	{
	public:
		GLTexture()
			: m_texData(new GLTextureData()) {
		}

		GLTexture(const GLTexture&_t) {
			_Swap(_t);
		}

		GLTexture(const String&path) {
			SetPath(path);
		}
		/**
		*/
		virtual ~GLTexture();
		/**
		*/
		virtual void SetWrap(WrapType _wrap) override;
		/**
		*/
		virtual void SetFilter(Filter _filter) override;
		/**
		*/
		virtual void SetAniso(Aniso _aniso) override;
		/**
		*/
		virtual void Set() override;
		virtual void UnSet() override;
		/**
		*/
		virtual void Set(unsigned int tex_unit) override;
		virtual void Unset(unsigned int tex_unit) override;
		/**
		*/
		virtual void Copy(int face = -1) override;
		/**
		*/
		virtual void SetMinMipLevel(unsigned int level) override;
		virtual void SetMaxMipLevel(unsigned int level) override;

		ENGINE_INLINE void SetTextureData(GLTextureData*_texData) {
			ASSERT(_texData, "Pointer on _texData is nullptr");
			m_texData = _texData;
		}

		virtual	unsigned int GetMinMipLevel() const override { ASSERT(m_texData, "Pointer on m_texData is nullptr"); return m_texData->_minMipLevel; }
		virtual	unsigned int GetMaxMipLevel() const override { ASSERT(m_texData, "Pointer on m_texData is nullptr"); return m_texData->_maxMipLevel; }

		ENGINE_INLINE GLTexture & operator=(const GLTexture & _t) {
			if (this != &_t) {
				_Swap(_t);
			}

			return *this;
		}
	private:
		/**
		*/
		static GLTexture *Create(unsigned int width, unsigned int height, unsigned int depth, TextureTarget target, Format format, void **data, const String &path);
		/**
		*/
		static GLTexture *Create2d(const String &path, const std::shared_ptr<TextureParamsTransfer>&_Tpp);
		static GLTexture *CreateCube(const String &path);

		static GLTexture *GLCreateCubeTextureFromFile(const String &path);
		/**
		*/
		static GLTexture *Create2d(unsigned int width, unsigned int height, Format format, const String&path);
		static GLTexture *Create3d(unsigned int width, unsigned int height, unsigned int depth, Format format, const String&path);
		static GLTexture *CreateCube(unsigned int width, unsigned int height, Format format, const String&path);

		/**
		*/
		static GLTexture *Create2d(Image *image, const String &path);
		static GLTexture *Create3d(Image *image, const String &path);
		static GLTexture *CreateCube(Image **image, const String &path);
		/**
		*/
		void _ActivateTarget(void **data);
		void _ActivateFormat(Format _format);

		/**
		*/
		void _SetWrap(unsigned int _wrap);

		ENGINE_INLINE void _Swap(const GLTexture &_t) { _t.CheckOnValidData(); m_texData = _t.m_texData; }

		bool CheckOnValidData() const { ASSERT(m_texData, "Invalid pointer on m_texData"); return m_texData != nullptr; }
		ENGINE_INLINE GLTextureData* GetTextureData() { return m_texData; }

	private:
		GLTextureData* m_texData = new GLTextureData();

		friend class GLFBO;
		friend class GLSystem;
	};
}