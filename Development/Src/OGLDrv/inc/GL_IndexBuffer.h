/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//***************************************************************************
#include "MathLib.h"
#include "NonCopyable.h"
//***************************************************************************

namespace NGTech {
	//---------------------------------------------------------------------------
	//Desc: Vertex buffer object class
	//---------------------------------------------------------------------------
	class OGL_IndexBuffer final :public I_VBManager, Noncopyable
	{
	public:
		OGL_IndexBuffer();
		static OGL_IndexBuffer *Create(void *data, int numElements, DataType dataType);
		virtual ~OGL_IndexBuffer();

		// ������������� ��������� VBO ��� �������
		virtual void Bind() const;
		virtual void UnBind() const;
		virtual void BindIndex(unsigned int idx) const;
		virtual void UnbindIndex(unsigned int idx) const;
		virtual void Allocate(const void *_data, size_t _size, const TypeDraw& _usage);

		virtual void FillBuffer(size_t offset);

		ENGINE_INLINE unsigned int GetID() { return glID; }
		/**
		Destroy
		*/
		virtual void Destroy() { delete this; }
	public:
		virtual void _Create();
	private:
		void DeleteBuffers();
		void *_ResizeBuffer(locked_data, int _offset = 0, void** _data = nullptr);
		void DeleteBuffer(locked_data);

		//!@todo ������� � ��������� �����
	private:

		size_t _size;
		int num_indices, buffer_size, buffer_offset;
		uint32_t frameNumber;
		GLsync buffer_sync[3];
		void*data;
		unsigned int glID;
		int numElements;
		int elementSize;

		unsigned int dataType;
		unsigned int drawType;
	};
}