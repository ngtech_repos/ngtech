/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//***************************************************************************
#include "MathLib.h"
#include "NonCopyable.h"
//***************************************************************************

namespace NGTech {
	class OGL_StorageBuffer final
	{
	public:
		OGL_StorageBuffer();

		~OGL_StorageBuffer();

		void GenerateBuffer();

		void Bind();

		void UnBind();

		void* Map();

		void UnMap();

		void FillBuffer(unsigned int _size, void* ptr, const TypeDraw&_drawType);

		void FillBufferWB(unsigned int _size, void* ptr, const TypeDraw&_drawType);

		void FillBufferZeroWB(unsigned int _size, const TypeDraw&_drawType);

		void FillBufferZero(unsigned int _size, const TypeDraw&_drawType);

		void UpdateWB(void*& _shader_data);

		void Update(void*& _shader_data);

		void BindBufferBase(int _id);

		void UnBindBufferBase(int _id);
	private:
		bool CheckBuffer();

	private:
		unsigned int stbID;
		void *savedPtr;
	};
}