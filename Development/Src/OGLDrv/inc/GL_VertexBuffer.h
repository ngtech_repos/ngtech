/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//***************************************************************************
#include "MathLib.h"
#include "NonCopyable.h"
//***************************************************************************

namespace NGTech {
	//---------------------------------------------------------------------------
	//Desc: Vertex buffer object class
	//---------------------------------------------------------------------------
	class OGL_VertexBuffer final :public I_VBManager, Noncopyable
	{
	public:
		OGL_VertexBuffer();
		virtual ~OGL_VertexBuffer();

		/**
		Fabric Method
		*/
		static OGL_VertexBuffer*Create(void *_data, int numElements, int elemSize, DataType dataType, const TypeDraw& _drawType);

		// ������������� ��������� VBO ��� �������
		virtual void Bind() const;
		virtual void UnBind() const;
		virtual void BindIndex(unsigned int idx) const;
		virtual void UnbindIndex(unsigned int idx) const;
		virtual void Allocate(const void *_data, size_t _size, const TypeDraw& _usage);

		virtual void FillBuffer(size_t offset);

		ENGINE_INLINE unsigned int GetID() { return glID; }
	public:
		void _Create();
		size_t _size;
		unsigned int num_indices, buffer_size, frameNumber, buffer_offset;
		GLsync buffer_sync[3];
	private:
		void DeleteBuffers();
		void *_ResizeBuffer(locked_data, int _offset = 0, void** _data = nullptr);
		void DeleteBuffer(locked_data);
	private:
		void*data = nullptr;
		unsigned int glID = 0;
		unsigned int numElements = 0;
		unsigned int elementSize = 0;

		unsigned int dataType = 0;
		unsigned int drawType = 0;
	};
}