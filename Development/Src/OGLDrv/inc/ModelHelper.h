/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include "GL_VertexBuffer.h"
#include "GL_IndexBuffer.h"

namespace NGTech
{
	class ModelHelper final :public I_ModelHelper
	{
	public:
		ModelHelper();
		virtual ~ModelHelper();

		virtual void LoadToRender(void* _vertices, unsigned int* _indices, unsigned int sizeofVertex, unsigned int _numVertices, unsigned int _numIndices) override;

		// For Static meshes
		// �������� ������ �� ���������
		virtual void Draw() const override;
		// �������� ������ �� ���������, ��������� ����������
		virtual void DrawInstanced(unsigned int instance_count, unsigned int base_instance) const override;

		// For Animated meshes
		// ���������� ���������� ������ � ������� � �������� �� �� ���������
		virtual void UpdateAndDraw(unsigned int _offset) const override;
		// ���������� ���������� ������ � ������� � �������� �� �� ���������, � ������������
		virtual void UpdateAndDrawInstanced(unsigned int _offset, unsigned int instance_count, unsigned int base_instance) const override;

		virtual OGL_VertexBuffer * GetVB() const override;
		virtual OGL_IndexBuffer * GetIB() const override;
	private:
		// For Static meshes
		void Util_DrawSubsetInstanced(unsigned int instance_count, unsigned int base_instance) const;

		void _CreateVAO();
		void _BindVAO() const;
		void _UnBindVAO() const;
		void _GenerateVBO(unsigned int sizeofVertex);
	private:
		//!@todo �������� ��������� ModelData
		void* vertices = nullptr;
		unsigned int *indices = nullptr;

		OGL_VertexBuffer *vertexbuffer = nullptr;
		OGL_IndexBuffer *indexbuffer = nullptr;

		unsigned int sizeofVertex = 0;
		unsigned int vertexlayout = 0;

		unsigned int numVertices = 0;
		unsigned int numIndices = 0;
	};
}