/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

namespace NGTech
{
	enum OpenGLDeclUsage
	{
		GLDECLUSAGE_POSITION = 0,
		GLDECLUSAGE_BLENDWEIGHT,
		GLDECLUSAGE_BLENDINDICES,
		GLDECLUSAGE_NORMAL,
		GLDECLUSAGE_PSIZE,
		GLDECLUSAGE_TEXCOORD,
		GLDECLUSAGE_TANGENT,
		GLDECLUSAGE_BINORMAL,
		GLDECLUSAGE_TESSFACTOR,
		GLDECLUSAGE_POSITIONT,
		GLDECLUSAGE_COLOR,
		GLDECLUSAGE_FOG,
		GLDECLUSAGE_DEPTH,
		GLDECLUSAGE_SAMPLE,
		GLDECLUSAGE_INSTANCE
	};

	bool checkGLError(const char* file, int32_t line);

#ifndef CHECK_GL_ERROR
#define CHECK_GL_ERROR() ASSERT(checkGLError(__FILE__, __LINE__))
#endif
}