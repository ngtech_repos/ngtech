/* Copyright (C) 2009-2017, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//***************************************************************************
#include "RenderContextBase.h"
//***************************************************************************

struct GLFWwindow;

namespace NGTech {

	class OGLRenderContextGLFW : public RenderContextBase {
	public:

		OGLRenderContextGLFW(uint32_t redBits, uint32_t greenBits, uint32_t blueBits, uint32_t alphaBits, uint32_t depthBits, uint32_t stencilBits, uint32_t msaaSamples);

		virtual ~OGLRenderContextGLFW();

		void setWindow(void* _window);

		bool bindContext();

		bool unbindContext();

		bool swap();

		bool setSwapInterval(uint32_t interval);

		uint32_t width();

		uint32_t height();

		void* getGLProcAddress(const char* procname);

		bool isExtensionSupported(const char* ext);

		void setConfiguration(uint32_t redBits, uint32_t greenBits, uint32_t blueBits, uint32_t alphaBits, uint32_t depthBits, uint32_t stencilBits, uint32_t msaaSamples);

		virtual void* getCurrentPlatformContext() override;

		virtual void* getCurrentPlatformDisplay() override;

	protected:
		GLFWwindow* mWindow = nullptr;
	};
}