/* Copyright (C) 2009-2017, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//***************************************************************************
#include "RenderContextBase.h"
//***************************************************************************

namespace NGTech {

	struct RenderContextBase {
		virtual ~RenderContextBase() {}

		/// Get platform-specific context.
		/// This function is for use in special circumstances where the WGL, EGL, GLX, etc
		/// context is required by the application.  Most applications should avoid this function.
		/// \return the platform-specific context handle, cast to void* or nullptr if not supported
		virtual void* getCurrentPlatformContext() { return nullptr; }

		/// Get platform-specific display.
		/// This function is for use in special circumstances where the WGL, EGL, GLX, etc
		/// display is required by the application.  Most applications should avoid this function.
		/// \return the platform-specific display handle, cast to void* or nullptr if not supported
		virtual void* getCurrentPlatformDisplay() { return nullptr; }

		/// Force context reset.
		/// Optional per-platform function to request that the GL context be
		/// shut down and restarted on demand.  Used to test an app's implementation
		/// of the initRendering/shutdownRendering sequence
		/// \return true if the feature is supported and the context has been reset,
		/// false if not supported or could not be completed
		virtual bool requestResetContext() { return false; }
	};
}