/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include "../../Platform/inc/platformdetect.h"

#if  PLATFORM_OS == PLATFORM_OS_WINDOWS
#ifndef RENDER_API
#ifdef RENDER_EXPORTS
#define RENDER_API CROSSPLATFORM_EXPORT_FROM_DLL
#else
#define RENDER_API CROSSPLATFORM_IMPORT_FROM_DLL
#endif
#endif

#else
#ifndef RENDER_API
#define RENDER_API
#endif
#endif