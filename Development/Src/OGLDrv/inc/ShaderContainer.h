/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

namespace NGTech
{
	/**
	Used for contains shader texts
	\and for manipulations with versions
	*/
	struct ShaderContainer
	{
		/**
		*/
		ShaderContainer();
		/**
		*/
		~ShaderContainer();

		/**
		*/
		void AddDefines(const String &_name, const String&_defines);

		/**
		*/
		void Clear();

		std::vector<String>& GetVResult() {
			return vResult;
		}

		std::vector<String>& GetFResult() {
			return fResult;
		}

		std::vector<String>& GetGResult() {
			return gResult;
		}

		std::vector<String>& GetTesResult() {
			return tesResult;
		}

		std::vector<String>& GetTcsResult() {
			return tcsResult;
		}

		std::vector<String>& GetCsResult() {
			return csResult;
		}
	private:
		/**
		*/
		void Determinate();

		/**
		*/
		void _PushVersion();

	private:

		/**
		Containers for shader strings
		Avalible only from GLShader
		*/
		VectorString vResult;
		std::vector<String> fResult;
		std::vector<String> gResult;
		std::vector<String> tesResult;
		std::vector<String> tcsResult;
		std::vector<String> csResult;
		friend class GLShader;
		String stringVersion;
	};
}