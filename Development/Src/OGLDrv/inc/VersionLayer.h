/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

namespace NGTech {
	struct API_Type {
		enum Enum {
			GLES, ///< OpenGL ES
			GL ///< OpenGL
		};
	};

	enum class OcclusionQueryMode
	{
		/*Req OpenGL3.3 or ARB_occlusion_query2*/
		OQ_ANY_SAMPLES_PASSED,
		/*Req OpenGL4.3 or ARB_ES3_compatibility. May be faster, but at the cost of more more false positives*/
		OQ_ANY_SAMPLES_PASSED_CONSERVATIVE,
		OQ_NUM
	};

	enum class DrawElementsFunction
	{
		DrawElementsInstanced,/*glDrawElementsInstanced GL3 and OpenGLES3*/
		DrawElementsInstancedBaseInstance,/*glDrawElementsInstancedBaseInstance 4.2 and later. Only OpenGL*/
		//MultiDrawArraysIndirect,/*glMultiDrawArraysIndirect*/ // TODO:RENDER: NOT IMPEMENTED YET
		NUM_DRAW
	};

	enum class DrawArraysFunction
	{
		DrawArrays,/*glDrawArrays. OpenGL2 and ES2*/
		DrawArraysInstanced,/*glDrawArraysInstanced GL3 and OpenGLES3*/
		DrawArraysInstancedBaseInstance,/*glDrawElementsInstancedBaseInstance 4.2 and later. Only OpenGL*/
		NUM_DRAW
	};

	// Default value is OpenGL 3.3
	struct OpenGLVersionBase
	{
		OpenGLVersionBase(API_Type::Enum _api, uint32_t _majVersion, uint32_t _minVersion = 0)
			:m_api(_api)
			, m_majVersion(_majVersion)
			, m_minVersion(_minVersion)
			, m_oqMode(OcclusionQueryMode::OQ_ANY_SAMPLES_PASSED)
			, m_drawElementsMode(DrawElementsFunction::DrawElementsInstanced)
			, m_drawArraysMode(DrawArraysFunction::DrawArraysInstanced)
			, m_SupportedMDI(false)
			, m_SupportedComputeShader(false)
			, m_glslVersionString("#version 150 core\r\n")
			, m_glslExtensionsString("")
			, m_glslPrecisionOfType("")
		{}

		OpenGLVersionBase() :m_api(API_Type::GL)
			, m_majVersion(3)
			, m_minVersion(3)
			, m_oqMode(OcclusionQueryMode::OQ_ANY_SAMPLES_PASSED)
			, m_drawElementsMode(DrawElementsFunction::DrawElementsInstanced)
			, m_drawArraysMode(DrawArraysFunction::DrawArraysInstanced)
			, m_SupportedMDI(false)
			, m_SupportedComputeShader(false)
			, m_glslVersionString("#version 150 core\r\n")
			, m_glslExtensionsString("")
			, m_glslPrecisionOfType("")
		{}

		///@{
		/// Compare two APIs and versions.
		/// OpenGL ES is considered to be "less than" OpenGL
		/// If the APIs match, then the major and minor versions are compared
		bool operator<(const OpenGLVersionBase &rhs) const {
			if (m_api < rhs.m_api) return true;
			if (m_api > rhs.m_api) return false;

			if (m_majVersion < rhs.m_majVersion) return true;
			if (m_majVersion > rhs.m_majVersion) return false;

			if (m_minVersion < rhs.m_minVersion) return true;
			return false;
		}

		bool operator<=(const OpenGLVersionBase &rhs) const {
			if (m_api < rhs.m_api) return true;
			if (m_api > rhs.m_api) return false;

			if (m_majVersion < rhs.m_majVersion) return true;
			if (m_majVersion > rhs.m_majVersion) return false;

			if (m_minVersion <= rhs.m_minVersion) return true;
			return false;
		}

		bool operator>(const OpenGLVersionBase &rhs) const {
			if (m_api > rhs.m_api) return true;
			if (m_api < rhs.m_api) return false;

			if (m_majVersion > rhs.m_majVersion) return true;
			if (m_majVersion < rhs.m_majVersion) return false;

			if (m_minVersion > rhs.m_minVersion) return true;
			return false;
		}

		bool operator>=(const OpenGLVersionBase &rhs) const {
			if (m_api > rhs.m_api) return true;
			if (m_api < rhs.m_api) return false;

			if (m_majVersion > rhs.m_majVersion) return true;
			if (m_majVersion < rhs.m_majVersion) return false;

			if (m_minVersion >= rhs.m_minVersion) return true;
			return false;
		}

		bool operator==(const OpenGLVersionBase &rhs) const {
			return (m_api == rhs.m_api) && (m_majVersion == rhs.m_majVersion) &&
				(m_minVersion == rhs.m_minVersion);
		}

		bool operator!=(const OpenGLVersionBase &rhs) const {
			return (m_api != rhs.m_api) || (m_majVersion != rhs.m_majVersion) ||
				(m_minVersion != rhs.m_minVersion);
		}

		/*Functions for calling in render classes*/

		unsigned int GetOQMode();

		void RunDrawElementsFunction(DrawMode _mode, void *_indices, unsigned int _indexCount,
			unsigned int _instance_count,
			unsigned int _base_instance);

		void RunDrawArraysFunction(int vertexCount,
			unsigned int instance_count,
			unsigned int base_instance, const GLint first);

		void AllocateBuffer(const void * _data, size_t size, unsigned int drawType/*Already converted to OpenGL*/, unsigned int _bufferType /*Already converted to OpenGL*/, unsigned int glID);
		void FillBuffer(unsigned int glID, size_t offset, size_t size, void* data);

		bool ValidateComputeShaderExtension();
	private:
		/*Only for subversions. for overloading*/
		ENGINE_INLINE virtual OcclusionQueryMode _GetOcclusionQueryMode() {
			return OcclusionQueryMode::OQ_ANY_SAMPLES_PASSED;
		}

		ENGINE_INLINE virtual DrawElementsFunction _GetDrawElementsFunction() {
			return DrawElementsFunction::DrawElementsInstanced;
		}

		ENGINE_INLINE virtual DrawArraysFunction _GetDrawArraysFunction() {
			return DrawArraysFunction::DrawArrays;
		}

	public:
		///@}

		API_Type::Enum m_api; ///< The high-level API
		uint32_t m_majVersion; ///< The major version (X.0)
		uint32_t m_minVersion; ///< The minor version (0.Y)

		String m_glslVersionString; ///< #version bla-bla core
		String m_glslExtensionsString; ///< #extension bla-bla
		String m_glslPrecisionOfType; ///< #precision bla-bla

		OcclusionQueryMode m_oqMode; ///< Selected Oq. culling mode
		DrawElementsFunction m_drawElementsMode;  ///< Selected draw element mode
		DrawArraysFunction m_drawArraysMode;  ///< Selected draw arrays mode

		bool m_SupportedMDI; ///< IsSupported MultiDrawIndirect
		bool m_SupportedComputeShader; ///< Is Supported Compute Shader(Native)
	};

	// ==================================== OPENGL ====================================
	struct OpenGL42 :public OpenGLVersionBase
	{
		OpenGL42() :OpenGLVersionBase(API_Type::GL, 4, 2) {
			m_glslVersionString = "#version 420 core\r\n";
			m_glslExtensionsString = "";
			m_glslPrecisionOfType = "";

			m_SupportedComputeShader = false;
		}

		ENGINE_INLINE virtual OcclusionQueryMode _GetOcclusionQueryMode() override {
			return OcclusionQueryMode::OQ_ANY_SAMPLES_PASSED;
		}

		ENGINE_INLINE virtual DrawElementsFunction _GetDrawElementsFunction() override {
			return DrawElementsFunction::DrawElementsInstancedBaseInstance;
		}
		ENGINE_INLINE virtual DrawArraysFunction _GetDrawArraysFunction() override {
			return DrawArraysFunction::DrawArraysInstancedBaseInstance;
		}
	};

	struct OpenGL43 :public OpenGLVersionBase
	{
		OpenGL43() :OpenGLVersionBase(API_Type::GL, 4, 3) {
			m_glslVersionString = "#version 430 core\r\n";
			m_glslExtensionsString = "";
			m_glslPrecisionOfType = "";

			m_SupportedComputeShader = true;
		}

		ENGINE_INLINE virtual OcclusionQueryMode _GetOcclusionQueryMode() override {
			return OcclusionQueryMode::OQ_ANY_SAMPLES_PASSED_CONSERVATIVE;
		}
		ENGINE_INLINE virtual DrawElementsFunction _GetDrawElementsFunction() override {
			return DrawElementsFunction::DrawElementsInstancedBaseInstance;
		}
		ENGINE_INLINE virtual DrawArraysFunction _GetDrawArraysFunction() override {
			return DrawArraysFunction::DrawArraysInstancedBaseInstance;
		}
	};

	struct OpenGL44 :public OpenGLVersionBase
	{
		OpenGL44() :OpenGLVersionBase(API_Type::GL, 4, 4) {
			m_glslVersionString = "#version 440 core\r\n";
			m_glslExtensionsString = "";
			m_glslPrecisionOfType = "";

			m_SupportedComputeShader = true;
		}

		ENGINE_INLINE virtual OcclusionQueryMode _GetOcclusionQueryMode() override {
			return OcclusionQueryMode::OQ_ANY_SAMPLES_PASSED_CONSERVATIVE;
		}
		ENGINE_INLINE virtual DrawElementsFunction _GetDrawElementsFunction() override {
			return DrawElementsFunction::DrawElementsInstancedBaseInstance;
		}
		ENGINE_INLINE virtual DrawArraysFunction _GetDrawArraysFunction() override {
			return DrawArraysFunction::DrawArraysInstancedBaseInstance;
		}
	};

	struct OpenGL45 :public OpenGLVersionBase
	{
		OpenGL45() :OpenGLVersionBase(API_Type::GL, 4, 5) {
			m_glslVersionString = "#version 450 core\r\n";
			m_glslExtensionsString = "";
			m_glslPrecisionOfType = "";

			m_SupportedComputeShader = true;
		}

		ENGINE_INLINE virtual OcclusionQueryMode _GetOcclusionQueryMode() override {
			return OcclusionQueryMode::OQ_ANY_SAMPLES_PASSED_CONSERVATIVE;
		}
		ENGINE_INLINE virtual DrawElementsFunction _GetDrawElementsFunction() override {
			return DrawElementsFunction::DrawElementsInstancedBaseInstance;
		}
		ENGINE_INLINE virtual DrawArraysFunction _GetDrawArraysFunction() override {
			return DrawArraysFunction::DrawArraysInstancedBaseInstance;
		}
	};

	struct OpenGL46 :public OpenGLVersionBase
	{
		OpenGL46() :OpenGLVersionBase(API_Type::GL, 4, 6) {
			m_glslVersionString = "#version 460 core\r\n";
			m_glslExtensionsString = "";
			m_glslPrecisionOfType = "";

			m_SupportedComputeShader = true;
		}

		ENGINE_INLINE virtual OcclusionQueryMode _GetOcclusionQueryMode() override {
			return OcclusionQueryMode::OQ_ANY_SAMPLES_PASSED_CONSERVATIVE;
		}
		ENGINE_INLINE virtual DrawElementsFunction _GetDrawElementsFunction() override {
			return DrawElementsFunction::DrawElementsInstancedBaseInstance;
		}
		ENGINE_INLINE virtual DrawArraysFunction _GetDrawArraysFunction() override {
			return DrawArraysFunction::DrawArraysInstancedBaseInstance;
		}
	};

	// ==================================== OPENGL ES ===================================
	struct OpenGL_ES31 :public OpenGLVersionBase
	{
		OpenGL_ES31() :OpenGLVersionBase(API_Type::GLES, 3, 1) {
			m_glslVersionString = "#version 310 es\r\n";

			m_glslExtensionsString = "#extension GL_EXT_shader_io_blocks : enable\r\n";
			m_glslExtensionsString += "#extension GL_OES_shader_io_blocks  : enable\r\n";

			m_glslPrecisionOfType += "precision highp float;\r\n";
			m_glslPrecisionOfType += "precision highp int;\r\n";
			m_glslPrecisionOfType += "precision lowp sampler2D;\r\n";
			m_glslPrecisionOfType += "precision lowp samplerCube;\r\n";

			m_SupportedComputeShader = false;
		}

		ENGINE_INLINE virtual OcclusionQueryMode _GetOcclusionQueryMode() override {
			return OcclusionQueryMode::OQ_ANY_SAMPLES_PASSED_CONSERVATIVE;
		}
		ENGINE_INLINE virtual DrawElementsFunction _GetDrawElementsFunction() override {
			return DrawElementsFunction::DrawElementsInstanced;
		}
		ENGINE_INLINE virtual DrawArraysFunction _GetDrawArraysFunction() override {
			return DrawArraysFunction::DrawArraysInstanced;
		}
	};
}