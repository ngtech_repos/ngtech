/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "RenderPrivate.h"

namespace NGTech
{
	unsigned int map_Format_Src[20] =
	{
		0,
		GL_RGB,//1-GLFMT_R8G8B8
		GL_RGBA,//2-GLFMT_A8R8G8B8
		GL_RGBA,//3
		GL_DEPTH_STENCIL,//4
		GL_DEPTH_COMPONENT,//5
		GL_RGB,//6-GLFMT_B16G16R16
		GL_RGBA,//7-GLFMT_A16B16G16R16
		GL_RED,//8
		GL_RG,//9
		GL_RGB,//10-GLFMT_B16G16R16F
		GL_RGBA,//11-GLFMT_A16B16G16R16F
		GL_RED,//12
		GL_RG,//13
		GL_RGB,//14-GLFMT_B32G32R32F
		GL_RGBA,//15-GLFMT_A32B32G32R32F
		GL_RGB,//16-GLFMT_B32G32R32F
		GL_RGBA,//17-GLFMT_A32B32G32R32F
#if (!IS_OS_ANDROID && !IS_OS_IOS)
		GL_COMPRESSED_RGBA_S3TC_DXT1_EXT, //18-DXT1
		GL_COMPRESSED_RGBA_S3TC_DXT5_EXT //19-DXT5
#else
		0,0
#endif
	};

	int map_Format_Internal[20] =
	{
		0,
		GL_RGB8,//1-GLFMT_R8G8B8
		GL_RGBA8,//2-GLFMT_A8R8G8B8
		GL_SRGB8_ALPHA8,//3
		GL_DEPTH_STENCIL,//4
		GL_DEPTH_COMPONENT,//5
		#if !IS_OS_ANDROID
		GL_RGB16,//6-GLFMT_B16G16R16
		GL_RGBA16,//7-GLFMT_A16B16G16R16
		#else
		GL_RGB16F,//6-GLFMT_B16G16R16
		GL_RGBA16F,//7-GLFMT_A16B16G16R16
		#endif
		GL_R16F,//8
		GL_RG16F,//9
		GL_RGB16F,//10-GLFMT_B16G16R16F
		GL_RGBA16F,//11-GLFMT_A16B16G16R16F
		GL_R32F,//12
		GL_RG32F,//13
		GL_RGB32F,//14-GLFMT_B32G32R32F
		GL_RGBA32F,//15-GLFMT_A32B32G32R32F
		GL_SRGB,//16-GLFMT_SRGB16
#if !IS_OS_ANDROID
		GL_SRGB_ALPHA,//17-GLFMT_SRGBA16
#else
		GL_RGBA32F,//17-GLFMT_SRGBA16
#endif
		GL_RGBA, // 18-DXT1
		GL_RGBA // 19-DXT5
	};

	unsigned int map_Format_Type[20] =
	{
		0,
		GL_UNSIGNED_BYTE,//1-GLFMT_R8G8B8
#if !IS_OS_ANDROID
		GL_UNSIGNED_INT_8_8_8_8_REV,//2 -GLFMT_A8R8G8B8
#else
		GL_UNSIGNED_BYTE,//2 -GLFMT_A8R8G8B8
#endif
		GL_UNSIGNED_BYTE,//3
		GL_UNSIGNED_INT_24_8,//4
		GL_FLOAT,//5
		GL_UNSIGNED_INT,//6-GLFMT_B16G16R16
		GL_UNSIGNED_INT,//7-GLFMT_A16B16G16R16
		GL_HALF_FLOAT,//8
		GL_HALF_FLOAT,//9
		GL_HALF_FLOAT,//10-GLFMT_B16G16R16F
		GL_HALF_FLOAT,//11-GLFMT_A16B16G16R16F
		GL_FLOAT,//12
		GL_FLOAT,//13
		GL_FLOAT,//14-GLFMT_B32G32R32F
		GL_FLOAT,//15-GLFMT_A32B32G32R32F
		GL_UNSIGNED_BYTE,//16-GLFMT_SRGB16
		GL_UNSIGNED_BYTE,//17-GLFMT_SRGBA16
		GL_UNSIGNED_BYTE, // 18-DXT1
		GL_FLOAT // 19-DXT5
	};

	unsigned int map_MipMapModes[6] = {
		0,
		GL_NEAREST,
		GL_LINEAR,/*similiat what ANISOTROPIC*/
		GL_NEAREST_MIPMAP_NEAREST,
		GL_LINEAR_MIPMAP_NEAREST,
		GL_LINEAR_MIPMAP_LINEAR
	};

	unsigned int map_TypeDraw[5] = {
		0,
		GL_STATIC_DRAW,
		GL_STREAM_DRAW,
		GL_DYNAMIC_DRAW,
		GL_DYNAMIC_COPY
	};
}