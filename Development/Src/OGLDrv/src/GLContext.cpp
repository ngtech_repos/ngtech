#include "RenderPrivate.h"

#if !IS_OS_ANDROID
//***************************************************
#include "../../../Externals/glfw/include/GLFW/glfw3.h"
#include "../../../Externals/glfw/include/GLFW/glfw3native.h"
//***************************************************
#endif

#include "OGLRenderContextWINAPI.h"
#include "OGLRenderContextGLFW.h"

namespace NGTech
{
	OGLRenderContextWINAPI::OGLRenderContextWINAPI(I_Window* _window) :window(_window)
	{
	}

	bool OGLRenderContextWINAPI::Initialise()
	{
		Debug("[OGLDrv] GLSystem::createContext - creating WINAPI starting");
		Debug("[Render]GLSystem::_createOldContext");

		ASSERT(window, "Invalid pointer on window");

		static PIXELFORMATDESCRIPTOR pfd =
		{
			sizeof(PIXELFORMATDESCRIPTOR),
			1,											// Version Number
			PFD_DRAW_TO_WINDOW |						// Format Must Support Window
			PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
			PFD_DOUBLEBUFFER,							// Must Support Double Buffering
			PFD_TYPE_RGBA,								// Request An RGBA Format
			window->m_Vars.bpp,							// Select Our Color Depth
			0, 0, 0, 0, 0, 0,							// Color Bits Ignored
			window->m_Vars.alphaBits,						// Alpha Buffer
			0,											// Shift Bit Ignored
			0,											// No Accumulation Buffer
			0, 0, 0, 0,									// Accumulation Bits Ignored
			window->m_Vars.zdepth,						// Z-Buffer (Depth Buffer)
			window->m_Vars.bpp,							// Stencil Buffer
			0,											// No Auxiliary Buffer
			PFD_MAIN_PLANE,								// Main Drawing Layer
			0,											// Reserved
			0, 0, 0										// Layer Masks Ignored
		};

		if (!(window->m_Vars.hDC = GetDC((HWND)window->m_Vars.hWnd))) {
			Error("WindowSystem::initialise() error: can't create a GL device context", true);
			return false;
		}

		if (!(window->m_Vars.pixelFormat = ChoosePixelFormat((HDC)window->m_Vars.hDC, &pfd))) {
			Error("WindowSystem::initialise() error: can't find a suitable pixel format", true);
			return false;
		}

		if (!(SetPixelFormat((HDC)window->m_Vars.hDC, window->m_Vars.pixelFormat, &pfd))) {
			Error("WindowSystem::initialise() error: can't set the pixel format", true);
			return false;
		}

		if (!(window->m_Vars.hRC = wglCreateContext((HDC)window->m_Vars.hDC))) {
			Error("WindowSystem::initialise() error: can't create a GL rendering context", true);
			return false;
		}

		if (!wglMakeCurrent((HDC)window->m_Vars.hDC, (HGLRC)window->m_Vars.hRC)) {
			Error("WindowSystem::initialise() error: can't activate the GL rendering context", true);
			return false;
		}

		CHECK_GL_ERROR();

		Debug("[OGLDrv] GLSystem::createContext - WinAPI created");
		return true;
	}

	OGLRenderContextWINAPI::~OGLRenderContextWINAPI()
	{
	}

	OGLRenderContextGLFW::OGLRenderContextGLFW(uint32_t redBits, uint32_t greenBits, uint32_t blueBits, uint32_t alphaBits, uint32_t depthBits, uint32_t stencilBits, uint32_t msaaSamples)
	{
		mWindow = nullptr;
		// Hack - we can't query most of this back from GLFW, so we assume it all "took"
		//mConfig = config;
		glfwWindowHint(GLFW_RED_BITS, redBits);
		glfwWindowHint(GLFW_GREEN_BITS, greenBits);
		glfwWindowHint(GLFW_BLUE_BITS, blueBits);
		glfwWindowHint(GLFW_ALPHA_BITS, alphaBits);
		glfwWindowHint(GLFW_DEPTH_BITS, depthBits);
		glfwWindowHint(GLFW_STENCIL_BITS, stencilBits);
		glfwWindowHint(GLFW_SAMPLES, msaaSamples);

#ifdef HEAVY_DEBUG
		glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
#endif
	}

	OGLRenderContextGLFW::~OGLRenderContextGLFW()
	{
	}

	void OGLRenderContextGLFW::setWindow(void* _window)
	{
		mWindow = (GLFWwindow*)_window;
		ASSERT(mWindow, "Invalid pointer");
	}

	bool OGLRenderContextGLFW::bindContext()
	{
		ASSERT(mWindow, "Invalid pointer");
		glfwMakeContextCurrent(mWindow);
		CHECK_GL_ERROR();
		return true;
	}

	bool OGLRenderContextGLFW::unbindContext()
	{
		CHECK_GL_ERROR();
		glfwMakeContextCurrent(NULL);
		CHECK_GL_ERROR();

		return true;
	}

	bool OGLRenderContextGLFW::swap()
	{
		glfwSwapBuffers(mWindow);
		return true;
	}

	bool OGLRenderContextGLFW::setSwapInterval(uint32_t interval)
	{
		CHECK_GL_ERROR();
		glfwSwapInterval(interval);
		CHECK_GL_ERROR();

		return false;
	}

	uint32_t OGLRenderContextGLFW::width()
	{
		ASSERT(mWindow, "Invalid pointer");
		int32_t w, h;
		glfwGetFramebufferSize(mWindow, &w, &h);
		CHECK_GL_ERROR();
		return w;
	}

	uint32_t OGLRenderContextGLFW::height()
	{
		ASSERT(mWindow, "Invalid pointer");
		int32_t w, h;
		glfwGetFramebufferSize(mWindow, &w, &h);
		CHECK_GL_ERROR();
		return h;
	}

	void* OGLRenderContextGLFW::getGLProcAddress(const char* procname)
	{
		return glfwGetProcAddress(procname);
	}

	bool OGLRenderContextGLFW::isExtensionSupported(const char* ext)
	{
		return glfwExtensionSupported(ext) ? true : false;
	}

	void OGLRenderContextGLFW::setConfiguration(uint32_t redBits, uint32_t greenBits, uint32_t blueBits, uint32_t alphaBits, uint32_t depthBits, uint32_t stencilBits, uint32_t msaaSamples)
	{
		ASSERT(mWindow, "Invalid pointer");

		int32_t major = glfwGetWindowAttrib(mWindow, GLFW_CONTEXT_VERSION_MAJOR);
		int32_t minor = glfwGetWindowAttrib(mWindow, GLFW_CONTEXT_VERSION_MINOR);
		bool isOpenGLES = glfwGetWindowAttrib(mWindow, GLFW_CONTEXT_CREATION_API) == GLFW_EGL_CONTEXT_API;

		// �������� ������� ������ ������, �� ������ ���������
		OpenGLVersionManager::getInstance().ChangeMode(major, minor, isOpenGLES);

		glGetIntegerv(GL_RED_BITS, (GLint*)&redBits);
		glGetIntegerv(GL_GREEN_BITS, (GLint*)&greenBits);
		glGetIntegerv(GL_BLUE_BITS, (GLint*)&blueBits);
		glGetIntegerv(GL_ALPHA_BITS, (GLint*)&alphaBits);
		glGetIntegerv(GL_DEPTH_BITS, (GLint*)&depthBits);
		glGetIntegerv(GL_STENCIL_BITS, (GLint*)&stencilBits);

		bindContext();

		// initialize extensions framework
		GLenum err = glewInit();
		if (GLEW_OK != err)
		{
			// Problem: glewInit failed, something is seriously wrong.
			//!@todo would be nice to use dialog here too....
			String ErrS = "Error: ";
			ErrS += (char*)glewGetErrorString(err);
			Error(ErrS, true);
		}
		LogPrintf("Using GLEW %s\n", glewGetString(GLEW_VERSION));
	}

	void* OGLRenderContextGLFW::getCurrentPlatformContext()
	{
#ifdef IS_OS_WINDOWS
		return (void*)wglGetCurrentContext();
#else
		return (void*)glXGetCurrentContext();
#endif
	}

	void* OGLRenderContextGLFW::getCurrentPlatformDisplay()
	{
#ifdef IS_OS_WINDOWS
		return (void*)wglGetCurrentDC();
#else
		return (void*)glXGetCurrentDrawable();
#endif
	}
}