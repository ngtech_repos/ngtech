/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "RenderPrivate.h"
//***************************************************************************
#include "../../Platform/inc/platformdetect.h"
//***************************************************************************
#include "GLSystem.h"
//***************************************************************************

namespace NGTech {
	/**
	*/
#if PLATFORM_OS == PLATFORM_OS_WINDOWS
	static void APIENTRY ReportGLError(
#else
	static void ReportGLError(
#endif
		GLenum source,
		GLenum type,
		GLuint id,
		GLenum severity,
		GLsizei length,
		const GLchar *message,
		const void *userdata)
	{
#if !IS_OS_ANDROID
		char debugSource[32] = { 0 };
		switch (source)
		{
		case GL_DEBUG_SOURCE_API:
			strcpy(debugSource, "OpenGL");
			break;
		case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
			strcpy(debugSource, "Windows");
			break;
		case GL_DEBUG_SOURCE_SHADER_COMPILER:
			strcpy(debugSource, "Shader Compiler");
			break;
		case GL_DEBUG_SOURCE_THIRD_PARTY:
			strcpy(debugSource, "Third Party");
			break;
		case GL_DEBUG_SOURCE_APPLICATION:
			strcpy(debugSource, "Application");
			break;
		case GL_DEBUG_SOURCE_OTHER:
			strcpy(debugSource, "Other");
			break;
		}

		char debugType[32] = { 0 };
		switch (type)
		{
		case GL_DEBUG_TYPE_ERROR:
			strcpy(debugType, "Error");
			break;
		case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
			strcpy(debugType, "Deprecated behavior");
			break;
		case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
			strcpy(debugType, "Undefined behavior");
			break;
		case GL_DEBUG_TYPE_PORTABILITY:
			strcpy(debugType, "Portability");
			break;
		case GL_DEBUG_TYPE_PERFORMANCE:
			strcpy(debugType, "Performance");
			break;
		case GL_DEBUG_TYPE_OTHER:
			strcpy(debugType, "Message");
			break;
		case GL_DEBUG_TYPE_MARKER:
			strcpy(debugType, "Marker");
			break;
		case GL_DEBUG_TYPE_PUSH_GROUP:
			strcpy(debugType, "Push group");
			break;
		case GL_DEBUG_TYPE_POP_GROUP:
			strcpy(debugType, "Pop group");
			break;
		}

		char severityType[32] = { 0 };
		int Errormode = 0;

		switch (severity)
		{
		case GL_DEBUG_SEVERITY_HIGH:
			strcpy(severityType, "[OGL-Error]");
			Errormode = 2;
			break;
		case GL_DEBUG_SEVERITY_MEDIUM:
			strcpy(severityType, "[OGL-Warning]");
			Errormode = 1;
			break;
		case GL_DEBUG_SEVERITY_LOW:
			strcpy(severityType, "[OGL-Info]");
			Errormode = 0;
			break;
		}

		if ((id != 131076) && (id != 131184) && (id != 131185) && (id != 131186) && (id != 131188) && (id != 131204) && (id != 131218))
		{
			char buffer[4096];
			sprintf(buffer, "%s %s: %s %i: %s\n", severityType, debugSource, debugType, id, message); //-V111

			switch (Errormode)
			{
			case 0:
				DebugM(buffer);
				break;
			case 1:
				Warning(buffer);
				break;
			case 2:
				ErrorWrite(buffer);
				break;
			}
		}
#endif
	}

	/**
	*/
	void GLSystem::EnableDebugOutput()
	{
		CHECK_GL_ERROR();
#if !IS_OS_ANDROID
#ifdef _ENGINE_DEBUG_
		glEnable(GL_DEBUG_OUTPUT);
		if (glDebugMessageCallback) {
			Debug("Enabling GL debug output");
			glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
			glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_TRUE);
			glDebugMessageCallback(ReportGLError, nullptr);
			GLuint unusedIds = 0;
			glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, &unusedIds, true);
			debugOutputEnabled = true;
		}
		else
			Debug("glDebugMessageCallback not available");
#else
		glDisable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS); // necessary, since otherwise NV OpenGL driver (340.52) crashes
#endif
#endif
		CHECK_GL_ERROR();
	}

	bool checkGLError(const char* file, int32_t line)
	{
		GLint error = glGetError();
		if (error)
		{
			const char* errorString = 0;
			switch (error)
			{
			case GL_INVALID_ENUM: errorString = "GL_INVALID_ENUM"; break;
			case GL_INVALID_FRAMEBUFFER_OPERATION: errorString = "GL_INVALID_FRAMEBUFFER_OPERATION"; break;
			case GL_INVALID_VALUE: errorString = "GL_INVALID_VALUE"; break;
			case GL_INVALID_OPERATION: errorString = "GL_INVALID_OPERATION"; break;
			case GL_STACK_OVERFLOW: errorString = "GL_STACK_OVERFLOW"; break;
			case GL_STACK_UNDERFLOW: errorString = "GL_STACK_UNDERFLOW"; break;
			case GL_OUT_OF_MEMORY: errorString = "GL_OUT_OF_MEMORY"; break;
			default: errorString = "unknown OpenGL error"; break;
			}

			ErrorWrite("GL error: %s, line %d: %s\n", file, line, errorString); //-V111
			error = 0; // nice place to hang a breakpoint in compiler... :)
			return false;
		}
		return true;
	}
}