/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "RenderPrivate.h"
//***************************************************************************
#include "GLExtensions.h"
//***************************************************************************

namespace NGTech {
	/**
	*/
	bool HardwareRenderFeatureManager::IsExtSupported(const char*name) {
#if !IS_OS_ANDROID
		return glewIsExtensionSupported(name);
#else
		return false;
#endif
	}

	/**
	*/
	void HardwareRenderFeatureManager::InitExtensions(I_RenderLowLevel* _sys) {
#if !IS_OS_ANDROID
		glewExperimental = GL_TRUE;
		if (glewInit() != GLEW_OK)
			Error("GLExtensions::initExtensions()", true);
#endif
		CHECK_GL_ERROR();

		DeterminateRenderMode(_sys);
	}

	/**
	*/
	void HardwareRenderFeatureManager::DeterminateRenderMode(I_RenderLowLevel* _sys)
	{
#if !IS_OS_ANDROID
		if (GLEW_VERSION_4_6)
		{
			LogPrintf("[GLExtensions::DeterminateRenderMode] OpenGL4.6 is supported");
			OpenGLVersionManager::getInstance().m_SeletectedProfile = OpenGL46();
		}
		if (GLEW_VERSION_4_5)
		{
			LogPrintf("[GLExtensions::DeterminateRenderMode] OpenGL4.5 is supported");
			OpenGLVersionManager::getInstance().m_SeletectedProfile = OpenGL45();
		}
		else if (GLEW_VERSION_4_4)
		{
			LogPrintf("[GLExtensions::DeterminateRenderMode] OpenGL4.4 is supported");
			OpenGLVersionManager::getInstance().m_SeletectedProfile = OpenGL44();
		}
		else if (GLEW_VERSION_4_3)
		{
			LogPrintf("[GLExtensions::DeterminateRenderMode] OpenGL4.3 is supported");
			OpenGLVersionManager::getInstance().m_SeletectedProfile = OpenGL43();
		}
		else if (GLEW_VERSION_4_2)
		{
			LogPrintf("[GLExtensions::DeterminateRenderMode] OpenGL4.2 is supported");
			OpenGLVersionManager::getInstance().m_SeletectedProfile = OpenGL42();
		}
		else
		{
			Error("[Critical Error]Your drivers for video card or hardware is very old. Try update it. Minimal requirements is needed support OpenGL 4.3.", true);
		}
#else
		OpenGLVersionManager::getInstance().m_SeletectedProfile = OpenGL_ES31();
#endif
	}
}