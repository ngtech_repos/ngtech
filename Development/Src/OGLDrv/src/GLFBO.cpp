/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "RenderPrivate.h"
//***************************************************************************
#include "GLSystem.h"
#include "GLFBO.h"
//***************************************************************************

namespace NGTech {
	/**
	*/
	GLFBO *GLFBO::Create(unsigned int width, unsigned int height) {
		GLFBO * fbo = new GLFBO();

		fbo->mFBOSize = Vec2(width, height);

		return fbo;
	}

	/**
	*/
	GLFBO::GLFBO()
		:fboid((GLuint)-1)
	{
		CHECK_GL_ERROR();
		glGenFramebuffers(1, &fboid);
		CHECK_GL_ERROR();
	}

	/**
	*/
	GLFBO::~GLFBO() {
		if (_IsInvalid())
			return;

		for (unsigned int i = 0; i < 8; ++i)
		{
			if (rendertargets[i].id != 0)
			{
				if (rendertargets[i].type == 0)
				{
					glDeleteRenderbuffers(1, &rendertargets[i].id);
					CHECK_GL_ERROR();
				}
				else
				{
					glDeleteTextures(1, &rendertargets[i].id);
					CHECK_GL_ERROR();
				}
			}
		}

		if (depthstencil.id != 0)
		{
			if (depthstencil.type == 0)
			{
				glDeleteRenderbuffers(1, &depthstencil.id);
				CHECK_GL_ERROR();
			}
			else
			{
				glDeleteTextures(1, &depthstencil.id);
				CHECK_GL_ERROR();
			}
		}

		glDeleteFramebuffers(1, &fboid);
	}

	bool GLFBO::_IsInvalid() {
		if (fboid == -1)
		{
			Warning("Invalid FBO");
			return true;
		}
		return false;
	}

	/**
	*/
	void GLFBO::Set()
	{
		if (_IsInvalid())
			return;

		GLenum buffs[8];
		GLsizei count = 0;

		for (unsigned int i = 0; i < 8; ++i)
		{
			if (rendertargets[i].id != 0)
			{
				buffs[i] = GL_COLOR_ATTACHMENT0 + i;
				count = i;
			}
			else
			{
				buffs[i] = GL_NONE;
			}
		}

		// ���������� ���������� �����, ��� ��� ����� ����� ������������� ������ GL_INVALID_OPERATION
		//GL_INVALID_OPERATION is generated if framebuffer is not zero or the name of a framebuffer previously returned from a call to glGenFramebuffers.
		Unset();
		CHECK_GL_ERROR();

		GLint curBuff;
		glGetIntegerv(GL_FRAMEBUFFER_BINDING, &curBuff);

		if (curBuff != static_cast<GLint>(fboid))
			glBindFramebuffer(GL_FRAMEBUFFER, fboid);

		CHECK_GL_ERROR();

		// �������� ��� ��������
		for (unsigned int i = 0; i < count; ++i)
		{
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, this->GetColorAttachment(i), 0);
			CHECK_GL_ERROR();
		}

		// � �������� �������
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, this->GetDepthAttachment(), 0);
		CHECK_GL_ERROR();

		if (count > 0) {
			glDrawBuffers(count + 1, buffs);
			CHECK_GL_ERROR();
		}

		float clearColorZero[4] = { Math::ZEROFLOAT, Math::ZEROFLOAT, Math::ZEROFLOAT, Math::ZEROFLOAT };
		for (unsigned int i = 0; i < count; i++)
		{
			glClearBufferfv(GL_COLOR, i, clearColorZero);
			CHECK_GL_ERROR();
		}

		GetRender()->SetViewport(0, 0, mFBOSize.x, mFBOSize.y);
	}

	/**
	*/
	void GLFBO::Unset() {
		CHECK_GL_ERROR();

		GLint curBuff;
		glGetIntegerv(GL_FRAMEBUFFER_BINDING, &curBuff);

		if (curBuff != 0)
			glBindFramebuffer(GL_FRAMEBUFFER, 0);

		CHECK_GL_ERROR();

		glDrawBuffer(GL_BACK);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLFBO::Clear() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLFBO::ClearDepthOnly() {
		glClear(GL_DEPTH_BUFFER_BIT);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLFBO::ClearColorOnly() {
		glClear(GL_COLOR_BUFFER_BIT);
		CHECK_GL_ERROR();
	}

	void GLFBO::ClearDepthAndStencil()
	{
		glClear(GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		CHECK_GL_ERROR();
	}

	/**
	*/
	bool GLFBO::Validate()
	{
		GLint curBuff;
		glGetIntegerv(GL_FRAMEBUFFER_BINDING, &curBuff);
		if (curBuff != static_cast<GLint>(fboid))
			glBindFramebuffer(GL_FRAMEBUFFER, fboid);

		GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

		switch (status)
		{
		case GL_FRAMEBUFFER_COMPLETE:
#ifdef HEAVY_DEBUG
			Debug("OpenGLFrameRenderBuffer::Validate(): Complete attachment !");
#endif
			break;

		case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
			Warning("OpenGLFrameRenderBuffer::Validate(): GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT!");

			glGetIntegerv(GL_FRAMEBUFFER_BINDING, &curBuff);

			if (curBuff != 0)
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

			CHECK_GL_ERROR();
			break;

		case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT:
			Warning("OpenGLFrameRenderBuffer::Validate(): GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS!");

			glGetIntegerv(GL_FRAMEBUFFER_BINDING, &curBuff);

			if (curBuff != 0)
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

			CHECK_GL_ERROR();
			break;

		case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
			Warning("OpenGLFrameRenderBuffer::Validate(): GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT!");

			glGetIntegerv(GL_FRAMEBUFFER_BINDING, &curBuff);

			if (curBuff != 0)
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
			CHECK_GL_ERROR();
			break;

		case GL_FRAMEBUFFER_UNSUPPORTED:
			CHECK_GL_ERROR();
			Error("GLFBO::set() error: framebuffer unsupported", true);
			break;

		default:
			glGetIntegerv(GL_FRAMEBUFFER_BINDING, &curBuff);

			if (curBuff != 0)
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
			CHECK_GL_ERROR();
			Warning("OpenGLFrameRenderBuffer::Validate(): Unknown error!");
			break;
		}

		CHECK_GL_ERROR();
		glGetIntegerv(GL_FRAMEBUFFER_BINDING, &curBuff);

		if (curBuff != 0)
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

		return (status == GL_FRAMEBUFFER_COMPLETE);
	}

	/**
	*/
	bool GLFBO::AttachColorTexture(unsigned int _index) {
		if (_IsInvalid())
			return false;

		return AttachColorTexture(_index, Format::GLFMT_A16B16G16R16F);
	}

	/**
	*/
	bool GLFBO::AttachColorTexture(unsigned int _index, const Format& format) {
		if (_IsInvalid())
			return false;

		return _AttachTexture(GL_COLOR_ATTACHMENT0 + _index, format);
	}

	/**
	*/
	bool GLFBO::AttachDepthTexture() {
		if (_IsInvalid())
			return false;

		return _AttachTexture(GL_DEPTH_ATTACHMENT, Format::GLFMT_D32F);
	}

	/**
	*/
	void GLFBO::BindColorTexture(unsigned int tex_unit) {
		if (_IsInvalid())
			return;

		glActiveTexture(GL_TEXTURE0 + tex_unit);
		CHECK_GL_ERROR();
		glBindTexture(GL_TEXTURE_2D, this->GetColorAttachment(tex_unit));
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLFBO::BindColorTexture(unsigned int _active, unsigned int tex_unit) {
		if (_IsInvalid())
			return;

		glActiveTexture(GL_TEXTURE0 + _active);
		CHECK_GL_ERROR();
		glBindTexture(GL_TEXTURE_2D, this->GetColorAttachment(tex_unit));
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLFBO::BindDepthTexture()
	{
		if (_IsInvalid())
			return;

		glBindTexture(GL_TEXTURE_2D, this->GetDepthAttachment());
		CHECK_GL_ERROR();
	}

	void GLFBO::BindDepthTexture(unsigned int tex_unit) {
		if (_IsInvalid())
			return;

		glActiveTexture(GL_TEXTURE0 + tex_unit);
		CHECK_GL_ERROR();
		glBindTexture(GL_TEXTURE_2D, this->GetDepthAttachment());
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLFBO::ReattachColor(unsigned int _target, int face, int level) {
		if (_IsInvalid())
			return;

		auto target = GL_COLOR_ATTACHMENT0 + _target;
		_Reattach(target, face, level);
	}

	/**
	*/
	void GLFBO::ReattachColor(unsigned int _target, int level) {
		if (_IsInvalid())
			return;

		auto target = GL_COLOR_ATTACHMENT0 + _target;
		_Reattach(target, level);
	}

	/**
	*/
	void GLFBO::ReattachDepth(int face, int level) {
		if (_IsInvalid())
			return;

		_Reattach(GL_DEPTH_ATTACHMENT, face, level);
	}

	/**
	*/
	void GLFBO::ReattachDepth(int level) {
		if (_IsInvalid())
			return;

		_Reattach(GL_DEPTH_ATTACHMENT, level);
	}

	/**
	*/
	void GLFBO::ReattachDepthStencil(int face, int level)
	{
		_Reattach(GL_DEPTH_STENCIL_ATTACHMENT, face, level);
	}

	/**
	*/
	void GLFBO::ReattachDepthStencil(int level)
	{
		_Reattach(GL_DEPTH_STENCIL_ATTACHMENT, level);
	}

	/**
	*/
	ENGINE_INLINE void GLFBO::_Reattach(unsigned int target, int face, int level) {
		_Reattach(target, level);
	}

	/**
	*/
	void GLFBO::_Reattach(unsigned int target, int level)
	{
		if (_IsInvalid())
		{
			ErrorWrite("[%s] invalid", __FUNCTION__);
			return;
		}

		Attachment* attach = 0;

		if (target == GL_DEPTH_ATTACHMENT || target == GL_DEPTH_STENCIL_ATTACHMENT)
			attach = &depthstencil;
		else
			attach = &rendertargets[target - GL_COLOR_ATTACHMENT0];

		if (attach->type != 1)
		{
			Error("[GLFBO::_Reattach] Already attached to this target!", true);
			return;
		}

		GLint curBuff;
		glGetIntegerv(GL_FRAMEBUFFER_BINDING, &curBuff);
		if (curBuff != static_cast<GLint>(fboid))
			glBindFramebuffer(GL_FRAMEBUFFER, fboid);

		CHECK_GL_ERROR();
		glFramebufferTexture2D(GL_FRAMEBUFFER, target, GL_TEXTURE_2D, attach->id, level);
		CHECK_GL_ERROR();
	}

	/**
	*/
	bool GLFBO::IsAlreadyBound() {
		CHECK_GL_ERROR();
		return IsAlreadyBindedForWrite(fboid);
	}

	/**
	*/
	bool GLFBO::IsAlreadyBindedForRead(int _check) {
		int drawFboId = -3;
		glGetIntegerv(GL_READ_FRAMEBUFFER_BINDING, &drawFboId);
		CHECK_GL_ERROR();
		return drawFboId == _check;
	}

	/**
	*/
	bool GLFBO::IsAlreadyBindedForWrite(int _check) {
		int drawFboId = 0;
		glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &drawFboId);
		CHECK_GL_ERROR();
		return drawFboId == _check;
	}

	/**
	*/
	void GLFBO::UnBindActiveTexture(unsigned int tex_unit) {
		glActiveTexture(GL_TEXTURE0 + tex_unit);
		CHECK_GL_ERROR();
		glBindTexture(GL_TEXTURE_2D, 0);
		CHECK_GL_ERROR();
	}

	/**
	*/
	bool GLFBO::_AttachRenderbuffer(unsigned int _target, const Format& _format)
	{
		Attachment* attach = 0;

		GLenum target = (GLenum)_target;
		if (target == GL_DEPTH_ATTACHMENT || target == GL_DEPTH_STENCIL_ATTACHMENT)
			attach = &depthstencil;
		else if (target >= GL_COLOR_ATTACHMENT0 && target < GL_COLOR_ATTACHMENT8)
			attach = &rendertargets[target - GL_COLOR_ATTACHMENT0];
		else
		{
			Error("[GLFBO::_AttachRenderbuffer] Target is invalid!", true);
			return false;
		}

		if (attach->id != 0)
		{
			Error("[GLFBO::_AttachRenderbuffer] Already attached to this target!", true);
			return false;
		}

		glGenRenderbuffers(1, &attach->id);
		CHECK_GL_ERROR();

		GLint curBuff;
		glGetIntegerv(GL_FRAMEBUFFER_BINDING, &curBuff);
		if (curBuff != static_cast<GLint>(fboid))
			glBindFramebuffer(GL_FRAMEBUFFER, fboid);
		CHECK_GL_ERROR();

		glBindRenderbuffer(GL_RENDERBUFFER, attach->id);
		CHECK_GL_ERROR();

		size_t format = static_cast<size_t>(_format);

		glRenderbufferStorage(GL_RENDERBUFFER, map_Format_Internal[format], mFBOSize.x, mFBOSize.y);
		CHECK_GL_ERROR();
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, target, GL_RENDERBUFFER, attach->id);
		CHECK_GL_ERROR();

		glBindRenderbuffer(GL_RENDERBUFFER, 0);
		CHECK_GL_ERROR();

		glGetIntegerv(GL_FRAMEBUFFER_BINDING, &curBuff);
		if (curBuff != 0)
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		CHECK_GL_ERROR();

		attach->type = 0;

		return true;
	}

	/**
	*/
	bool GLFBO::_AttachTexture(unsigned int target, const Format& _format, const Filter& _filter)
	{
#ifdef HEAVY_DEBUG
		DebugM("%s %s", __FUNCTION__, "Started");
#endif
		Attachment* attach = 0;

		if (target == GL_DEPTH_ATTACHMENT || target == GL_DEPTH_STENCIL_ATTACHMENT)
			attach = &depthstencil;
		else if (target >= GL_COLOR_ATTACHMENT0 && target < GL_COLOR_ATTACHMENT8)
			attach = &rendertargets[target - GL_COLOR_ATTACHMENT0];
		else
		{
			Error("[GLFBO::AttachColorTexture] Target is invalid!", true);
			return false;
		}

#ifdef HEAVY_DEBUG
		DebugM("%s %s", __FUNCTION__, "Correct targets");
#endif

		if (attach->id != 0)
		{
			Error("[GLFBO::AttachColorTexture] Already attached to this target!", true);
			return false;
		}

#ifdef HEAVY_DEBUG
		DebugM("%s %s", __FUNCTION__, "Attach id !=0 - succesfull");
#endif

		glGenTextures(1, &attach->id);
		CHECK_GL_ERROR();

		GLint curBuff;
		glGetIntegerv(GL_FRAMEBUFFER_BINDING, &curBuff);
		if (curBuff != static_cast<GLint>(fboid))
			glBindFramebuffer(GL_FRAMEBUFFER, fboid);
		CHECK_GL_ERROR();

		glBindTexture(GL_TEXTURE_2D, attach->id);
		CHECK_GL_ERROR();

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
		CHECK_GL_ERROR();

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		CHECK_GL_ERROR();

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		CHECK_GL_ERROR();

		auto filter = static_cast<size_t>(_filter);
		auto format = static_cast<size_t>(_format);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, map_MipMapModes[filter]);
		CHECK_GL_ERROR();

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, map_MipMapModes[filter]);
		CHECK_GL_ERROR();

		glTexImage2D(GL_TEXTURE_2D, 0, map_Format_Internal[format], mFBOSize.x, mFBOSize.y, 0, map_Format_Src[format], map_Format_Type[format], 0);
		CHECK_GL_ERROR();

		glFramebufferTexture2D(GL_FRAMEBUFFER, target, GL_TEXTURE_2D, attach->id, 0);
		CHECK_GL_ERROR();

#ifdef HEAVY_DEBUG
		DebugM("%s %s", __FUNCTION__, "Attached texture in framebuffer. Now Disable it");
#endif

		glBindTexture(GL_TEXTURE_2D, 0);
		CHECK_GL_ERROR();

		glGetIntegerv(GL_FRAMEBUFFER_BINDING, &curBuff);
		if (curBuff != 0)
			glBindFramebuffer(GL_FRAMEBUFFER, 0);

		CHECK_GL_ERROR();

		attach->type = 1;

#ifdef HEAVY_DEBUG
		DebugM("%s %s", __FUNCTION__, "now validation");
#endif

		return Validate();
	}

	void GLFBO::Resolve(I_RenderTarget* to, unsigned int bitmask)
	{
		GLint curBuff;
		glGetIntegerv(GL_FRAMEBUFFER_BINDING, &curBuff);
		if (curBuff != static_cast<GLint>(fboid))
			glBindFramebuffer(GL_READ_FRAMEBUFFER, fboid);
		CHECK_GL_ERROR();

		// if to == NULL then resolve to screen
		if (to) {
			glGetIntegerv(GL_FRAMEBUFFER_BINDING, &curBuff);
			if (curBuff != static_cast<GLint>(to->GetUID()))
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, to->GetUID());
			CHECK_GL_ERROR();
		}
		else
		{
			glGetIntegerv(GL_FRAMEBUFFER_BINDING, &curBuff);
			if (curBuff != 0)
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
			CHECK_GL_ERROR();
		}

		if (bitmask & GL_COLOR_BUFFER_BIT)
		{
			glReadBuffer(GL_COLOR_ATTACHMENT0);
			CHECK_GL_ERROR();
			glDrawBuffer((to == 0) ? GL_BACK : GL_COLOR_ATTACHMENT0);
			CHECK_GL_ERROR();
		}

		GLenum filter = ((bitmask & (GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT)) ? GL_NEAREST : GL_LINEAR);

		if (to)
		{
			glBlitFramebuffer(0, 0, mFBOSize.x, mFBOSize.y, 0, 0, to->GetSize().x, to->GetSize().y, bitmask, filter);
			CHECK_GL_ERROR();
		}
		else {
			glBlitFramebuffer(0, 0, mFBOSize.x, mFBOSize.y, 0, 0, mFBOSize.x, mFBOSize.y, bitmask, filter);
			CHECK_GL_ERROR();
		}

		glGetIntegerv(GL_FRAMEBUFFER_BINDING, &curBuff);
		if (curBuff != 0)
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		CHECK_GL_ERROR();

		if (bitmask & GL_COLOR_BUFFER_BIT)
		{
			glReadBuffer(GL_BACK);
			CHECK_GL_ERROR();
			glDrawBuffer(GL_BACK);
			CHECK_GL_ERROR();
		}
	}

	void GLFBO::ReadBuffer(unsigned int _texIndex) {
		glReadBuffer(GL_COLOR_ATTACHMENT0 + _texIndex);
	}

	void GLFBO::WriteBuffer(unsigned int _texIndex) {
		glDrawBuffer(GL_COLOR_ATTACHMENT0 + _texIndex);
		//!@todo Not implemented see http://docs.gl/gl4/glDrawBuffer
	}
}