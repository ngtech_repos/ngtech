/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "RenderPrivate.h"
//***************************************************************************
#include "GLSystem.h"
#include "GLOcclusionQuery.h"
//***************************************************************************
#include "GLExtensions.h"
//***************************************************************************

namespace NGTech {
	// Used in debugging
#define NOT_ALLOW_NOT_SUPPORTED_FUNCS 1

	/*
	*/
	GLQuery::GLQuery(GLuint _type) : type(_type) {
		query_id = 0;
		result = 1;
	}

	GLQuery::~GLQuery() {
		if (query_id && glIsQuery(query_id)) glDeleteQueries(1, &query_id);
	}

	/*
	*/
	void GLQuery::Clear() {
		result = 1;
	}

	void GLQuery::Destroy() {
		query_id = 0;
		result = 1;
	}

	/*
	*/
	void GLQuery::BeginQuery() {
		CHECK_GL_ERROR();
#if NOT_ALLOW_NOT_SUPPORTED_FUNCS
		ASSERT(glGenQueries, "glGenQueries not supported");
#endif
#if NOT_ALLOW_NOT_SUPPORTED_FUNCS
		ASSERT(glBeginQuery, "glBeginQuery not supported");
#endif

		if (query_id == 0)
		{
			glGenQueries(1, &query_id);
			CHECK_GL_ERROR();
		}

		glBeginQuery(type, query_id);
		CHECK_GL_ERROR();
	}

	void GLQuery::EndQuery() {
		CHECK_GL_ERROR();

#if NOT_ALLOW_NOT_SUPPORTED_FUNCS
		ASSERT(glEndQuery, "glEndQuery not supported");
#endif
		if (!glEndQuery)
			return;

		glEndQuery(type);
		CHECK_GL_ERROR();
	}

	/*
	*/
	bool GLQuery::IsCompleted() {
		if (query_id == 0) return 1;
		GLuint available = 0;
		glGetQueryObjectuiv(query_id, GL_QUERY_RESULT_AVAILABLE, &available);
		return (available != 0);
	}

	unsigned int GLQuery::GetResult() {
		if (query_id == 0) return 1;
		GLuint available = 0;
		glGetQueryObjectuiv(query_id, GL_QUERY_RESULT_AVAILABLE, &available);
		if (available) glGetQueryObjectuiv(query_id, GL_QUERY_RESULT, &result);
		return result;
	}

	long long GLQuery::GetResultLong() {
		if (query_id == 0) return 1;
		GLuint available = 0;
		GLint64 result_long = result;
		glGetQueryObjectuiv(query_id, GL_QUERY_RESULT_AVAILABLE, &available);
		if (available) glGetQueryObjecti64v(query_id, GL_QUERY_RESULT, &result_long);
		return (long long)result_long;
	}

	/*
	*/
	GLuint GLQuery::GetTypeQuery() const {
		return type;
	}

	/**
	*/
	GLOcclusionQuery::GLOcclusionQuery()
	{
		query = GLQuery(OpenGLVersionManager::getInstance().m_SeletectedProfile.GetOQMode());
	}

	/**
	*/
	void GLOcclusionQuery::BeginQuery() {
		query.BeginQuery();
	}

	/**
	*/
	void GLOcclusionQuery::EndQuery() {
		query.EndQuery();
	}

	/**
	*/
	void GLOcclusionQuery::BeginConditionalRender()
	{
		CHECK_GL_ERROR();
#if !IS_OS_ANDROID

#if NOT_ALLOW_NOT_SUPPORTED_FUNCS
		ASSERT(glBeginConditionalRender, "glBeginConditionalRender not supported");
#endif

		if (query.GetQueryID() != 0 && glBeginConditionalRender)
			glBeginConditionalRender(query.GetQueryID(), GL_QUERY_NO_WAIT);
#else
		DebugM("[%] Not available on mobile", __FUNCTION__);
#endif
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLOcclusionQuery::EndConditionalRender()
	{
		CHECK_GL_ERROR();
#if !IS_OS_ANDROID
#if NOT_ALLOW_NOT_SUPPORTED_FUNCS
		ASSERT(glEndConditionalRender, "glEndConditionalRender not supported");
#endif

		if (query.GetQueryID() != 0 && glEndConditionalRender)
			glEndConditionalRender();
#else
		DebugM("[%] Not available on mobile", __FUNCTION__);
#endif
		CHECK_GL_ERROR();
	}

	/**
	*/
	bool GLOcclusionQuery::IsCompleted()
	{
		return query.IsCompleted();
	}
}