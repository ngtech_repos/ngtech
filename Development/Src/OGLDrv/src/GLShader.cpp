/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "RenderPrivate.h"
//***************************************************************************
#include <sstream>
#include <iostream>
//***************************************************************************
#include "GLSystem.h"
#include "GLShader.h"
#include "FileHelper.h"
//***************************************************************************
//#define _DISABLE_SHADER_CACHE 1
/*
WIP:�������� ����������� �������� ��������
��� �� ���� ������� ������ � ���������� � ��������� �����
http://steps3d.narod.ru/tutorials/separate-shader-objects.html
��� ���� ������� glCreateShaderProgramv  ��� ������ �������� ��������� ���-�� ����
https://code.google.com/p/steps-framework/source/browse/#svn%2Fsrc
https://www.opengl.org/sdk/docs/man/html/glCreateShaderProgram.xhtml
*/
//#define RENDER_DEV 0

#include "ShaderInlines.inl"

namespace NGTech
{
	/**
	Loading shader cache util - loading shader cache from cache or spriv
	*/
	static ENGINE_INLINE bool _LoadShaderCacheUtil(const char* _path, GLShader*_shader)
	{
#ifndef _DISABLE_SHADER_CACHE
		CHECK_GL_ERROR();

		GLint Size = 0;
		std::vector<unsigned char> Data;

		return _CP_LoadBinaryShader_CacheUtil(_path, Size, Data, _shader);
#endif

		return false;
	}

	/**
	*/
	GLShader *GLShader::Create(const String &path, const String &_defines, bool allowSaveShaderCache) {
		GLShader *shader = new GLShader(path.c_str());

		if (shader->_CreateShader(path, _defines, "", allowSaveShaderCache))
		{
			return shader;
		}
		else
		{
			SAFE_DELETE(shader);
			return nullptr;
		}
	}

	/**
	*/
	GLShader::GLShader()
	{
		this->vs = NULL;
		this->fs = NULL;
		this->cs = NULL;
		this->gs = NULL;
		this->tcs = NULL;
		this->tes = NULL;
		this->program = NULL;

		this->bLoaded = false;
		this->bLinked = false;
		this->allowShaderCache = false;

		this->mShaderId = 0;
		this->floatcap = 0;
		this->floatsize = 0;
		this->intcap = 0;
		this->intsize = 0;
		this->PipelineName = 0;

		this->floatvalues = nullptr;
		this->intvalues = nullptr;
		this->contaner = std::make_shared<ShaderContainer>();
	}

	GLShader::~GLShader() {
		CHECK_GL_ERROR();
		this->Disable();
		CHECK_GL_ERROR();

		SAFE_rESET(contaner);
		SAFE_DELETE_ARRAY(floatvalues);
		SAFE_DELETE_ARRAY(intvalues);

		_Clean();
	}

	void GLShader::_Clean()
	{
#define DELETE_ACTION(x) if (x) 	{	CHECK_GL_ERROR(); glDeleteShader(x); CHECK_GL_ERROR();  }
#define DELETE_AND_DEATTACH(x) if (x) 	{	if(x>0) { CHECK_GL_ERROR(); glDetachShader(this->program, x); DELETE_ACTION(x); CHECK_GL_ERROR();	} }

		DELETE_AND_DEATTACH(vs);
		DELETE_AND_DEATTACH(fs);
		DELETE_AND_DEATTACH(gs);
		DELETE_AND_DEATTACH(cs);
		DELETE_AND_DEATTACH(tcs);
		DELETE_AND_DEATTACH(tes);
		/*��������� ��������� �� � ����, �� �����������, ������� ������� ���*/
		_DeleteProgramAndPipeline(program, PipelineName);

#undef DELETE_AND_DEATTACH
#undef DELETE_ACTION
	}

	/**
	*/
	void GLShader::_DeleteProgramAndPipeline(GLuint pr, GLuint pipe)
	{
		CHECK_GL_ERROR();
		this->Disable();
		CHECK_GL_ERROR();
		if (pr > 0) {
			CHECK_GL_ERROR();
			glDeleteProgram(pr);
			CHECK_GL_ERROR();
		}
		if (pipe > 0) {
			CHECK_GL_ERROR();
			glDeleteProgramPipelines(1, &pipe);
			CHECK_GL_ERROR();
		}
	}

	/**
	*/
	void GLShader::Recompile(const String& _defines, const String& _scenedefines)
	{
		// ���� ��� �������� ��������� ���
		if (this->allowShaderCache)
			_CP_DeleteShaderCacheUtil(*this);

		//!@todo ������ ��������������

		// ��������� ������ ID
		auto oldprogram = this->program;
		auto oldPipe = this->PipelineName;
		// �������� ����� ����������
		if (this->_CreateShader(this->GetPath(), _defines, _scenedefines, this->allowShaderCache))
		{
			/*
			��������� ����� �� �����-������� ������ ������
			��������� ��������� �� � ����, �� �����������, ������� ������� ���
			*/
			_DeleteProgramAndPipeline(oldprogram, oldPipe);
		}
	}

	/**
	*/
	void GLShader::Enable() {
		CHECK_GL_ERROR();
		if (program != 0)
		{
			glUseProgram(program);
			CHECK_GL_ERROR();
		}
		else
			ErrorWrite("[GLShader::Enable] program id = 0");

		if (PipelineName != 0)
		{
			glBindProgramPipeline(PipelineName);
			CHECK_GL_ERROR();
		}
		else
			ErrorWrite("[GLShader::Enable] PipelineName id = 0");

		CommitChanges();
	}

	void GLShader::Disable() {
		CHECK_GL_ERROR();
		glUseProgram(NULL);
		CHECK_GL_ERROR();
		glBindProgramPipeline(0);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLShader::SendMat4(const String &name, const Mat4 &value) {
		if (name.empty())
			return;

		Uniform test;
		strcpy(test.Name, name.c_str());

		size_t id = uniforms.find(test);

		if (id < uniforms.size())
		{
			const Uniform& uni = uniforms[id];
			float* reg = (floatvalues + uni.StartRegister * 4);

			memcpy(reg, value.e, uni.RegisterCount * 4 * sizeof(float));
			uni.Changed = true;
		}
	}

	void GLShader::SendVec4(const String &name, const Vec4 &value)
	{
		if (name.empty())
			return;

		Uniform test;
		strcpy(test.Name, name.c_str());

		size_t id = uniforms.find(test);

		if (id < uniforms.size())
		{
			const Uniform& uni = uniforms[id];
			float* reg = (floatvalues + uni.StartRegister * 4);

			memcpy(reg, value, uni.RegisterCount * 4 * sizeof(float));
			uni.Changed = true;
		}
	}

	void GLShader::SendVec3(const String &name, const Vec3 &value)
	{
		if (name.empty())
			return;

		SendVec4(name, value);
	}

	void GLShader::SendVec2(const String &name, const Vec2 &value) {
		if (name.empty())
			return;

		SendVec4(name, value);
	}

	void GLShader::SendFloat(const String &name, float value) {
		if (name.empty())
			return;

		Uniform test;
		strcpy(test.Name, name.c_str());

		size_t id = uniforms.find(test);

		if (id < uniforms.size())
		{
			const Uniform& uni = uniforms[id];
			float* reg = (floatvalues + uni.StartRegister * 4);

			reg[0] = value;
			uni.Changed = true;
		}
	}

	/**
	*/
	void GLShader::_AddUniform(const char* name, GLuint location, GLuint count, GLenum type)
	{
		Uniform uni;

		if (strlen(name) >= sizeof(uni.Name))
			throw 1;

		strcpy(uni.Name, name);

		if (type == GL_FLOAT_MAT4)
			count = 4;

		uni.Type = type;
		uni.RegisterCount = count;
		uni.Location = location;
		uni.Changed = true;

		if (type == GL_FLOAT || (type >= GL_FLOAT_VEC2 && type <= GL_FLOAT_VEC4) || type == GL_FLOAT_MAT4)
		{
			uni.StartRegister = floatsize;

			if (floatsize + count > floatcap)
			{
				unsigned int newcap = std::max(floatsize + count, floatsize + 8);

				floatvalues = (float*)realloc(floatvalues, newcap * 4 * sizeof(float));
				floatcap = newcap;
			}

			float* reg = (floatvalues + uni.StartRegister * 4);
			memset(reg, 0, uni.RegisterCount * 4 * sizeof(float));

			floatsize += count;
		}
		else if (type == GL_INT || (type >= GL_INT_VEC2 && type <= GL_INT_VEC4) || type == GL_SAMPLER_2D || type == GL_IMAGE_2D
			|| type == GL_SAMPLER_CUBE
			|| type == GL_SAMPLER_2D_SHADOW
			|| type == GL_SAMPLER_3D
			|| type == GL_UNSIGNED_INT_SAMPLER_2D || type == GL_UNSIGNED_INT_SAMPLER_3D || type == GL_UNSIGNED_INT_SAMPLER_CUBE
#if !IS_OS_ANDROID
			|| type == GL_SAMPLER_2D_RECT
			|| type == GL_SAMPLER_1D_SHADOW
			|| type == GL_UNSIGNED_INT_SAMPLER_1D
			|| type == GL_UNSIGNED_INT_SAMPLER_1D_ARRAY
#endif
			|| type == GL_UNSIGNED_INT_SAMPLER_2D_ARRAY
			)		// samplers will work as int
		{
			uni.StartRegister = intsize;

			if (intsize + count > intcap)
			{
				unsigned int newcap = std::max(intsize + count, intsize + 8);

				intvalues = (int*)realloc(intvalues, newcap * 4 * sizeof(int));
				intcap = newcap;
			}

			int* reg = (intvalues + uni.StartRegister * 4);
			memset(reg, 0, uni.RegisterCount * 4 * sizeof(int));

			intsize += count;
		}
		else
		{
			// Take type and convert him to HEX, and see in gl.h
			Error(StringHelper::StickString("[GLShader::_AddUniform] Unknown type %i. Not supported currently", type), true);
			// not handled
			throw 1;
		}
		uniforms.insert(uni);
	}

	void GLShader::_QueryUniforms(GLuint _newProgram)
	{
		CHECK_GL_ERROR();
		GLint		count;
		GLenum		type;
		GLsizei		length;
		GLint		size, loc;
		GLchar		uniname[256];

		if (_newProgram == 0)
			return;

		memset(uniname, 0, sizeof(uniname));
		uniforms.clear();

		CHECK_GL_ERROR();
		glGetProgramiv(_newProgram, GL_ACTIVE_UNIFORMS, &count);
		CHECK_GL_ERROR();

		// uniforms
		for (int i = 0; i < count; ++i)
		{
			memset(uniname, 0, sizeof(uniname));

			CHECK_GL_ERROR();
			glGetActiveUniform(_newProgram, i, 256, &length, &size, &type, uniname);
			CHECK_GL_ERROR();
			loc = glGetUniformLocation(_newProgram, uniname);
			CHECK_GL_ERROR();

			if (loc == -1)
				continue;

			_AddUniform(uniname, loc, size, type);
		}
	}

	/**
	*/
	void GLShader::CommitChanges()
	{
		CHECK_GL_ERROR();

		for (size_t i = 0; i < uniforms.size(); ++i)
		{
			const Uniform& uni = uniforms[i];
			float* floatdata = (floatvalues + uni.StartRegister * 4);
			int* intdata = (intvalues + uni.StartRegister * 4);

			if (!uni.Changed)
				continue;

			uni.Changed = false;

			switch (uni.Type)
			{
			case GL_FLOAT:
				CHECK_GL_ERROR();
				glUniform1fv(uni.Location, uni.RegisterCount, floatdata);
				CHECK_GL_ERROR();
				break;

			case GL_FLOAT_VEC2:
				CHECK_GL_ERROR();
				glUniform2fv(uni.Location, uni.RegisterCount, floatdata);
				CHECK_GL_ERROR();
				break;

			case GL_FLOAT_VEC3:
				CHECK_GL_ERROR();
				glUniform3fv(uni.Location, uni.RegisterCount, floatdata);
				CHECK_GL_ERROR();
				break;

			case GL_FLOAT_VEC4:
				CHECK_GL_ERROR();
				glUniform4fv(uni.Location, uni.RegisterCount, floatdata);
				CHECK_GL_ERROR();
				break;

			case GL_FLOAT_MAT4:
				CHECK_GL_ERROR();
				glUniformMatrix4fv(uni.Location, uni.RegisterCount / 4, false, floatdata);
				CHECK_GL_ERROR();
				break;

			case GL_INT:
			case GL_SAMPLER_2D:
			case GL_SAMPLER_3D:
			case GL_SAMPLER_CUBE:
			case GL_SAMPLER_2D_SHADOW:
#if !IS_OS_ANDROID
			case GL_SAMPLER_1D_SHADOW:
			case GL_SAMPLER_2D_RECT:
#endif
				CHECK_GL_ERROR();
				glUniform1i(uni.Location, intdata[0]);
				CHECK_GL_ERROR();
				break;

			default:
				break;
			}
		}
	}

	/**
	*/
	void GLShader::SendInt(const String &name, int value) {
		Uniform test;
		strcpy(test.Name, name.c_str());

		size_t id = uniforms.find(test);

		if (id < uniforms.size())
		{
			const Uniform& uni = uniforms[id];
			int* reg = (intvalues + uni.StartRegister * 4);

			reg[0] = value;
			uni.Changed = true;
		}
	}

	/**
	*/
	bool GLShader::_CreateShader(const String &_path, const String &_shaderSpecificDefs, const String &_sceneSpecificDefs, bool _allowShaderCache)
	{
		CHECK_GL_ERROR();
		unsigned int newPipelineName = 0;
		glGenProgramPipelines(1, &newPipelineName);
		CHECK_GL_ERROR();

#ifndef _DISABLE_SHADER_CACHE
		bool m_success = false;

		this->allowShaderCache = _allowShaderCache;

		if (_allowShaderCache) {
			m_success = _LoadShaderCacheUtil(_path.c_str(), this);
			LogPrintf("Validation shader binary is %i", m_success);
		}

		if (m_success == false) {
			//!@todo ����� ������� ��������� SPIRV - ���� ������ ������, ��� � ��� �������� ��������
			CP_ParseShaderFileFinal(*GetCvars(), _path, _shaderSpecificDefs, _sceneSpecificDefs, _allowShaderCache, newPipelineName, this->contaner, *this);
		}
		else
		{
			this->PipelineName = newPipelineName;
			this->SetPath(_path);
			this->defines = _shaderSpecificDefs + _sceneSpecificDefs;
		}
#endif

		return true;
	}

	/**
	*/
	void GLShader::AddDefines(const String &_defines) {
		ASSERT(contaner, "[GLShader::AddDefines] Not exist contaner. For shader: %s", this->GetPath().c_str());

		if (!contaner)
			return;

		this->defines = _defines;
		contaner->AddDefines(this->GetPath(), this->defines);
	}

	bool GLShader::CheckLinked(GLuint _programId, GLuint _PipelineName, const char* _path, bool _showError)
	{
		int linked = 0;
		CHECK_GL_ERROR();
		glGetProgramiv(_programId, GL_LINK_STATUS, &linked);
		CHECK_GL_ERROR();

		if (!linked)
		{
			char sInfoLog[1024];
			char sFinalMessage[1536];
			int iLogLength;

			CHECK_GL_ERROR();
			// ������� ��� ���������� (������ ���)
			glGetProgramInfoLog(_programId, 1024, &iLogLength, sInfoLog);
			CHECK_GL_ERROR();

			sprintf(sFinalMessage, "Error! Shader file %s wasn't linked! The compiler returned: %s ", _path, sInfoLog);

			if ((iLogLength > 0) && _showError)
				Error(sFinalMessage, false);

			CHECK_GL_ERROR();
			glDeleteProgram(_programId);
			CHECK_GL_ERROR();

			bLinked = false;
			return false;
		}
		bLinked = true;

		CHECK_GL_ERROR();
		glUseProgramStages(_PipelineName, GL_VERTEX_SHADER_BIT | GL_TESS_CONTROL_SHADER_BIT | GL_TESS_EVALUATION_SHADER_BIT | GL_GEOMETRY_SHADER_BIT | GL_FRAGMENT_SHADER_BIT, _programId);
		CHECK_GL_ERROR();

		_QueryUniforms(_programId);

		return true;
	}

	bool GLShader::CheckLinkedAndBind(GLuint _programId, GLuint _PipelineName, const char*_path, bool _showError)
	{
		_BindAttributes(_programId);
		return CheckLinked(_programId, _PipelineName, _path, _showError);
	}

	bool GLShader::SaveToShaderCache(const char* _path)
	{
		if (this->allowShaderCache == false)
			return true;

#ifndef _DISABLE_SHADER_CACHE
		Debug(__FUNCTION__);

		if (program == 0 || !bLinked)
		{
			Debug("[GLShader::_saveCache] shadercache not saved, check fragment: program == 0 || !bLinked");
			return false;
		}

		if (glGetProgramBinary == NULL || glProgramBinary == NULL || glProgramParameteri == NULL)
		{
			Debug("[GLShader::_saveCache] shadercache not saved, check fragment: glGetProgramBinary == NULL || glProgramBinary == NULL || glProgramParameteri == NULL");
			return false;
		}

		GLint Size;
		GLenum Format(0);

		CHECK_GL_ERROR();
		glGetProgramiv(this->program, GL_PROGRAM_BINARY_LENGTH, &Size);
		CHECK_GL_ERROR();

		if (Size < 1)
		{
			Debug("[GLShader::_saveCache] shadercache not saved, check fragment: Size < 1");
			return false;
		}

		std::vector<unsigned char> Data(Size);

		CHECK_GL_ERROR();
		glGetProgramBinary(this->program, Size, nullptr, &Format, &Data[0]);
		CHECK_GL_ERROR();

		if (Data.empty())
		{
			Debug("[GLShader::_saveCache] shadercache not saved, check fragment: Size < 1");
			return false;
		}

		return saveBinary(_CP_CreateShaderCacheDirectoryUtil(_path), Data);
#else
		return true;
#endif
	}

	/**
	*/
	GLuint GLShader::operator [](const String& attribute) {
		return _attributeList[attribute];
	}

	GLuint GLShader::operator [](const char* attribute) {
		return _attributeList[attribute];
	}

	void GLShader::_BindAttributes(GLuint _programId)
	{
		if (_programId == 0)
			return;

		GLint count;
		GLenum type;
		GLint size, loc;
		GLchar attribname[256];

		CHECK_GL_ERROR();
		glGetProgramiv(_programId, GL_ACTIVE_ATTRIBUTES, &count);
		CHECK_GL_ERROR();

		_attributeList["my_Position"] = GLDECLUSAGE_POSITION;
		_attributeList["my_Normal"] = GLDECLUSAGE_NORMAL;
		_attributeList["my_Tangent"] = GLDECLUSAGE_TANGENT;
		_attributeList["my_Binormal"] = GLDECLUSAGE_BINORMAL;
		_attributeList["my_Color"] = GLDECLUSAGE_COLOR;
		_attributeList["my_Texcoord0"] = GLDECLUSAGE_TEXCOORD;
		_attributeList["my_Texcoord1"] = GLDECLUSAGE_TEXCOORD + 10;
		_attributeList["my_Texcoord2"] = GLDECLUSAGE_TEXCOORD + 11;
		_attributeList["my_Texcoord3"] = GLDECLUSAGE_TEXCOORD + 12;
		_attributeList["my_Texcoord4"] = GLDECLUSAGE_TEXCOORD + 13;
		_attributeList["my_Texcoord5"] = GLDECLUSAGE_TEXCOORD + 14;
		_attributeList["my_Texcoord6"] = GLDECLUSAGE_TEXCOORD + 15;
		_attributeList["my_Texcoord7"] = GLDECLUSAGE_TEXCOORD + 16;

		//TODO:������� � Utils
		for (int i = 0; i < count; ++i)
		{
			memset(attribname, 0, sizeof(attribname));

			glGetActiveAttrib(_programId, i, 256, NULL, &size, &type, attribname);
			loc = glGetAttribLocation(_programId, attribname);

			auto it = _attributeList.find(String(attribname));

			if (loc == -1 || it == _attributeList.end())
			{
				Warning("could not find uniform \"%s\" in program %d", attribname, _programId);
			}
			else
				glBindAttribLocation(_programId, it->second, attribname);
		}

		// bind fragment shader output
		GLint numrts;

		CHECK_GL_ERROR();
		glGetIntegerv(GL_MAX_DRAW_BUFFERS, &numrts);
		CHECK_GL_ERROR();
#if !IS_OS_ANDROID
		/*CHECK_GL_ERROR();
		glBindFragDataLocation(_programId, 0, "my_FragColor0");
		CHECK_GL_ERROR();

		if (numrts > 1)
		{
			CHECK_GL_ERROR();
			glBindFragDataLocation(_programId, 1, "my_FragColor1");
			CHECK_GL_ERROR();
		}

		if (numrts > 2) {
			CHECK_GL_ERROR();
			glBindFragDataLocation(_programId, 2, "my_FragColor2");
			CHECK_GL_ERROR();
		}

		if (numrts > 3)
		{
			CHECK_GL_ERROR();
			glBindFragDataLocation(_programId, 3, "my_FragColor3");
			CHECK_GL_ERROR();
		}*/

		for (unsigned int i = 0; i < numrts; ++i)
		{
			CHECK_GL_ERROR();
			glBindFragDataLocation(_programId, i, ("my_FragColor" + std::to_string(i)).c_str());
			CHECK_GL_ERROR();
		}
#endif

		CHECK_GL_ERROR();
		// Necessary to retrieve shader program binary
		glProgramParameteri(_programId, GL_PROGRAM_BINARY_RETRIEVABLE_HINT, GL_TRUE);
		CHECK_GL_ERROR();
		// relink
		glLinkProgram(_programId);
		CHECK_GL_ERROR();
	}

	void GLShader::DispatchCompute(unsigned int num_groups_x, unsigned int num_groups_y, unsigned int num_groups_z)
	{
		CHECK_GL_ERROR();
		glDispatchCompute(num_groups_x, num_groups_y, num_groups_z);
		CHECK_GL_ERROR();
	}
}