/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "RenderPrivate.h"
//***************************************************************************
#include "RenderQueueWrapper.h"
//***************************************************************************
#include "GLSystem.h"
#include "GLTexture.h"
#include "OGL_ScreenQuad.h"
#include "OGLRenderContextGLFW.h"
#include "OGLRenderContextWINAPI.h"
//***************************************************************************

namespace NGTech {
	/**
	*/
	static ENGINE_INLINE bool _RequireExtension(const char*name, bool _fatal)
	{
		if (!HardwareRenderFeatureManager::IsExtSupported(name))
		{
			if (_fatal)
				PlatformError((String("GLSystem::requireExtension() error: your video card does not support ") + name).c_str());
			else
				Warning("GLSystem::requireExtension() error: your video card does not support %s", name);

			return false;
		}

		return true;
	}

	/**
	*/
	GLSystem::GLSystem(CoreManager*_engine)
		:engine(_engine), m_WeakCvars(engine->cvars), polygon_cull(CullFace::CULL_NONE), polygon_front(CullType::CULL_NONE),
		depth_func(CompareType::COMP_NONE),
		debugOutputEnabled(false)
	{}

	/**
	*/
	static ENGINE_INLINE void ManageVSync(CoreManager*engine, bool v)
	{
		ASSERT(engine, "Not exist Engine");
		ASSERT(engine->iWindow, "Not exist Window");

		engine->iWindow->ManageVSync(v);
	}

	/**
	*/
	void GLSystem::_RenderSetDefaults()
	{
#ifdef RENDER_DEV
		CHECK_GL_ERROR();
		glClearColor(Math::ZEROFLOAT, 0.125f, 0.3f, Math::ONEFLOAT);
		CHECK_GL_ERROR();
#else
		CHECK_GL_ERROR();
		//clear display
		glClearColor(Math::ZEROFLOAT, Math::ZEROFLOAT, Math::ZEROFLOAT, Math::ZEROFLOAT);
		CHECK_GL_ERROR();
#endif
		CHECK_GL_ERROR();
#if !IS_OS_ANDROID
		glClearDepth(1.0);
#else
		glClearDepthf(1.0);
#endif
		CHECK_GL_ERROR();

		//enable depth testing and culling
		glEnable(GL_CULL_FACE);
		CHECK_GL_ERROR();
		glCullFace(GL_BACK);
		CHECK_GL_ERROR();

		glDepthFunc(GL_LEQUAL);
		CHECK_GL_ERROR();
		glEnable(GL_DEPTH_TEST);
		CHECK_GL_ERROR();
#if !IS_OS_ANDROID
		glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
		CHECK_GL_ERROR();
#endif
		//Texture Params
		glPixelStorei(GL_PACK_ALIGNMENT, 1);
		CHECK_GL_ERROR();
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLSystem::Initialise()
	{
		if (!_CreateContext(engine->iWindow))
		{
			Error("[GLSystem] initialize-Failed Creation OpenGL Context", true);
			return;
		}
		Log::WriteHeader("-- GLSystem --");
		LogPrintf("Vendor:%s", _GetVendor());
		LogPrintf("Renderer:%s", _GetRenderer());
		LogPrintf("Version:%s", _GetVersion());
		LogPrintf("GLSL Version:%s", _GetGLSLVersion());

		defAniso = Aniso::ANISO_X0;
		// ��������� ��������� ���������� �������� - �������� ����������
		defFilter = Filter::LINEAR_MIPMAP_LINEAR;

		auto _window = GetWindow();
		ASSERT(_window, "Invalid pointer");

		if (_window == NULL)
			return;

		HardwareRenderFeatureManager::InitExtensions(this);
		CHECK_GL_ERROR();

		Reshape(_window->GetWidth(), _window->GetHeight(), matrixes.matProj);
		CHECK_GL_ERROR();
		_RenderSetDefaults();
		CHECK_GL_ERROR();

		ManageVSync(engine, false);
		CHECK_GL_ERROR();
		EnableMultiSample(false);
		CHECK_GL_ERROR();

#ifdef _ENGINE_DEBUG_
		_RequireExtension("GL_ARB_debug_output", false);
		// for opengles these extensions are only available
		_RequireExtension("GL_KHR_debug", false);

		EnableDebugOutput();
#endif
	}

	/**
	*/
	void GLSystem::FlushGPU()
	{
		CHECK_GL_ERROR();
		glFlush();
		CHECK_GL_ERROR();
		glFinish();
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLSystem::EnableWireframeMode()
	{
#if !IS_OS_ANDROID
		CHECK_GL_ERROR();
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		CHECK_GL_ERROR();
#endif
	}

	/**
	*/
	void GLSystem::DisableWireframeMode()
	{
#if !IS_OS_ANDROID
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
#endif
	}

	/**
	*/
	void GLSystem::DisableDebugOutput()
	{
#ifdef _ENGINE_DEBUG_
		Debug("Disabling GL debug output");
		CHECK_GL_ERROR();
		glDisable(GL_DEBUG_OUTPUT);
		CHECK_GL_ERROR();
		glDebugMessageCallback(nullptr, nullptr);
		CHECK_GL_ERROR();
		debugOutputEnabled = false;
#endif
	}

	/**
	*/
	void GLSystem::EnableMultiSample(bool v) {
		CHECK_GL_ERROR();

		if (v)
		{
			glEnable(GL_MULTISAMPLE);
			CHECK_GL_ERROR();
		}
		else
		{
			glDisable(GL_MULTISAMPLE);
			CHECK_GL_ERROR();
		}
	}

	/**
	*/
	void GLSystem::EnableVSync(bool _v) {
		ManageVSync(engine, _v);
	}

	/**
	*/
	GLSystem::~GLSystem() {
	}

	/**
	*/
	const char * GLSystem::_GetVendor() {
		return (const char *)glGetString(GL_VENDOR);
	}

	/**
	*/
	const char * GLSystem::_GetRenderer() {
		return (const char *)glGetString(GL_RENDERER);
	}

	/**
	*/
	const char * GLSystem::_GetVersion() {
		return (const char *)glGetString(GL_VERSION);
	}

	/**
	*/
	const char* GLSystem::_GetGLSLVersion() {
		return (const char*)glGetString(GL_SHADING_LANGUAGE_VERSION);
	}

	/**
	*/
	void GLSystem::Reshape(uint32_t width, uint32_t height, Mat4&proj) {
		// Prevent a divide by zero, when window is too short
		// (you cant make a window of zero width).
		if (height == 0)
			height = 1;

		// Set the viewport to be the entire window
		SetViewport(0, 0, width, height);

		// Reset Matrix
		proj.Identity();

		auto cvars = m_WeakCvars.lock();
		ASSERT(cvars, "Cvars is expired");

		// Set the correct perspective.
		proj = proj * Mat4::perspective(cvars->cl_fov, (TimeDelta)width / (TimeDelta)height, 1, 500);
		matrixes.matMVP.Identity();
	}

	/**
	*/
	void GLSystem::Reshape(uint32_t width, uint32_t height) {
		this->Reshape(width, height, matrixes.matProj);
	}

	/**
	*/
	void GLSystem::GetViewport(int *viewport) {
		CHECK_GL_ERROR();
		glGetIntegerv(GL_VIEWPORT, viewport);
		CHECK_GL_ERROR();
	}

	/*
	*/
	void GLSystem::ClearColor(Vec3 color) {
		CHECK_GL_ERROR();
		glClearColor(color.x, color.y, color.z, 1.0);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLSystem::ColorMask(bool r, bool g, bool b, bool a) {
		CHECK_GL_ERROR();
		glColorMask(r, g, b, a);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLSystem::Clear(RenderBuffer buffer) {
		static const GLuint buffer_modes[] = {
			0,/*INVALID*/
			GL_COLOR_BUFFER_BIT,/*COLOR_BUFFER*/
			GL_DEPTH_BUFFER_BIT,/*DEPTH_BUFFER*/
			GL_STENCIL_BUFFER_BIT,/*STENCIL_BUFFER*/
			GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT,/*COLOR_AND_DEPTH*/
			GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT/*ALL_BUFFERS*/
		};
		if (buffer == RenderBuffer::INVALID || buffer == RenderBuffer::NUM_BUFFERS)
		{
			Debug("[GLSystem::Clear] Ivalid call param in Clear method");
			return;
		}

		auto reqClear = buffer_modes[static_cast<unsigned int>(buffer)];
#ifdef RENDER_DEV
		DebugM("[GLSystem::Clear] Called with buffer: %i", reqClear);
#endif

		CHECK_GL_ERROR();
		glClear(reqClear);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLSystem::SetViewport(unsigned int w, unsigned int h) {
		SetViewport(0, 0, w, h);
	}

	/**
	*/
	void GLSystem::SetViewport(unsigned int x, unsigned int y, unsigned int w, unsigned int h) {
		mCurrentViewport = Vec4(x, y, w, h);
		CHECK_GL_ERROR();
		glViewport(x, y, w, h);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLSystem::SetPolygonFront(CullType front)
	{
		CHECK_GL_ERROR();
		ASSERT(front >= CullType::CULL_NONE && front < CullType::NUM_FRONT_MODES, "GLSystem::SetPolygonFront(): bad polygon front");

		static const GLuint front_modes[] = {
			0,
			GL_CCW,
			GL_CW,
			GL_FRONT
		};

		if (front == CullType::CULL_FRONT) {
			if (polygon_front == CullType::CULL_CCW)
			{
				glFrontFace(front_modes[static_cast<unsigned int>(CullType::CULL_CW)]);
				CHECK_GL_ERROR();
				polygon_front = CullType::CULL_CW;
			}
			else if (polygon_front == CullType::CULL_CW)
			{
				glFrontFace(front_modes[static_cast<unsigned int>(CullType::CULL_CCW)]);
				CHECK_GL_ERROR();
				polygon_front = CullType::CULL_CCW;
			}
		}
		else if (polygon_front != front)
		{
			glFrontFace(front_modes[static_cast<unsigned int>(front)]);
			CHECK_GL_ERROR();
			polygon_front = front;
		}
	}

	/**
	*/
	void GLSystem::SetPolygonCull(CullFace cull)
	{
		CHECK_GL_ERROR();
		ASSERT(cull >= CullFace::CULL_NONE && cull < CullFace::NUM_CULL_MODES, "GLSystem::SetPolygonCull(): bad polygon cull");

		static const GLuint cull_modes[] = {
			0,
			GL_FRONT,
			GL_BACK,
		};

		if (polygon_cull != cull)
		{
			if (cull == CullFace::CULL_NONE)
			{
				glDisable(GL_CULL_FACE);
				glCullFace(GL_BACK);
				CHECK_GL_ERROR();
			}
			else
			{
				glCullFace(cull_modes[static_cast<unsigned int>(cull)]);
				CHECK_GL_ERROR();
				if (polygon_cull == CullFace::CULL_NONE) glEnable(GL_CULL_FACE);
				CHECK_GL_ERROR();
			}
			polygon_cull = cull;
		}
	}

	/**
	*/
	void GLSystem::EnableCulling(CullType type) {
		glEnable(GL_CULL_FACE);
		SetPolygonFront(type);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLSystem::EnableCulling(CullFace face) {
		glEnable(GL_CULL_FACE);
		SetPolygonCull(face);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLSystem::EnableCulling() {
		glEnable(GL_CULL_FACE);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLSystem::DisableCulling() {
		glDisable(GL_CULL_FACE);
		polygon_cull = CullFace::CULL_NONE;
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLSystem::DepthFunc(CompareType _type) {
		static const GLuint modes[] = {
			0,/*NEVER*/0x0209, /*LESS*/ 0x0201,/*EQUAL*/ 0x0202,
			/*LEQUAL*/ 0x0203, /*GREATER*/ 0x0204,/*NOTEQUAL*/ 0x0205,
			/*GEQUAL*/ 0x0206, /*ALWAYS*/ 0x0207
		};

		if (_type == CompareType::COMP_NONE)
		{
			glDisable(GL_DEPTH_TEST);
			glDepthFunc(GL_LESS);
			CHECK_GL_ERROR();
		}
		else
		{
			auto index = static_cast<unsigned int>(_type);
			glDepthFunc(modes[index]);
			CHECK_GL_ERROR();
			if (depth_func == CompareType::COMP_NONE) glEnable(GL_DEPTH_TEST);
			CHECK_GL_ERROR();
		}

		depth_func = _type;
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLSystem::EnableDepth(CompareType _type) {
		CHECK_GL_ERROR();

		this->EnableDepth();

		static const GLuint modes[] = {
			0, /*NEVER*/0x0209, /*LESS*/ 0x0201,/*EQUAL*/ 0x0202,
			/*LEQUAL*/ 0x0203, /*GREATER*/ 0x0204,/*NOTEQUAL*/ 0x0205,
			/*GEQUAL*/ 0x0206, /*ALWAYS*/ 0x0207
		};

		auto index = static_cast<unsigned int>(_type);
		glDepthFunc(modes[index]);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLSystem::EnableDepth() {
		CHECK_GL_ERROR();
		glEnable(GL_DEPTH_TEST);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLSystem::DisableDepth() {
		CHECK_GL_ERROR();
		glDisable(GL_DEPTH_TEST);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLSystem::DisableStencilTest() {
		CHECK_GL_ERROR();
		glDisable(GL_STENCIL_TEST);
		CHECK_GL_ERROR();
	}

	void GLSystem::EnableStencilTest() {
		CHECK_GL_ERROR();
		glEnable(GL_STENCIL_TEST);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLSystem::DepthMask(bool mask) {
		CHECK_GL_ERROR();
		glDepthMask(mask);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLSystem::PolygonOffsetFill(float a, float b) {
		CHECK_GL_ERROR();
		glPolygonOffset(a, b);
		CHECK_GL_ERROR();
	}

	/***
	*/
	void GLSystem::EnablePolygonOffsetFill(float a, float b) {
		CHECK_GL_ERROR();
		glEnable(GL_POLYGON_OFFSET_FILL);
		glPolygonOffset(a, b);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLSystem::EnablePolygonOffsetFill() {
		CHECK_GL_ERROR();
		glEnable(GL_POLYGON_OFFSET_FILL);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLSystem::DisablePolygonOffsetFill() {
		CHECK_GL_ERROR();
		glDisable(GL_POLYGON_OFFSET_FILL);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLSystem::EnableBlending() {
		CHECK_GL_ERROR();
		glEnable(GL_BLEND);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLSystem::BlendFunc(BlendParam _Src, BlendParam _Dst) {
		CHECK_GL_ERROR();
		static const GLuint BlendModes[] = {
			1, 0, 0x0300, 0x0306, 0x0302, 0x0304,
			0x0301, 0x0307, 0x0303, 0x0305
		};

		auto _srcIndex = static_cast<unsigned int>(_Src);
		auto _dstIndex = static_cast<unsigned int>(_Dst);

		glBlendFunc(BlendModes[_srcIndex], BlendModes[_dstIndex]);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLSystem::EnableBlending(BlendParam src, BlendParam dst)
	{
		CHECK_GL_ERROR();
		glEnable(GL_BLEND);
		BlendFunc(src, dst);
	}

	/**
	*/
	void GLSystem::DisableBlending() {
		glDisable(GL_BLEND);
	}

	/**
	*/
	void GLSystem::ClipPlane(const Vec4 &plain, int plainNum) {
#if !IS_OS_ANDROID
		double eq[4] = { (double)plain[0], (double)plain[1], (double)plain[2], (double)plain[3] };

		CHECK_GL_ERROR();
		glClipPlane(GL_CLIP_PLANE0 + plainNum, eq);
		CHECK_GL_ERROR();
#endif
	}

	/**
	*/
	void GLSystem::EnableClipPlane(int plainNum) {
#if !IS_OS_ANDROID
		CHECK_GL_ERROR();
		glEnable(GL_CLIP_PLANE0 + plainNum);
		CHECK_GL_ERROR();
#endif
	}

	/**
	*/
	void GLSystem::EnableClipPlane(const Vec4 &plain, int plainNum) {
#if !IS_OS_ANDROID
		double eq[4] = { (double)plain[0], (double)plain[1], (double)plain[2], (double)plain[3] };

		CHECK_GL_ERROR();
		glClipPlane(GL_CLIP_PLANE0 + plainNum, eq);
		CHECK_GL_ERROR();
		glEnable(GL_CLIP_PLANE0 + plainNum);
		CHECK_GL_ERROR();
#endif
	}

	/**
	*/
	void GLSystem::DisableClipPlane(int plainNum) {
#if !IS_OS_ANDROID
		CHECK_GL_ERROR();
		glDisable(GL_CLIP_PLANE0 + plainNum);
		CHECK_GL_ERROR();
#endif
	}

	/**
	*/
	I_Drawable* GLSystem::CreateReact() {
		return new OGL_ScreenQuad();
	}

	/**
	*/
	void GLSystem::_DrawElements(DrawMode _dmMode, void *_indices, unsigned int _indexCount,
		unsigned int _instance_count,
		unsigned int _base_instance)
	{
		m_APIVersionLayer.m_SeletectedProfile.RunDrawElementsFunction(_dmMode, _indices, _indexCount, _instance_count, _base_instance);

#ifndef DROP_EDITOR
		GetStatistic()->RecordDrawCalls();
#endif
	}

	/**
	*/
	void GLSystem::_DrawArrays(DrawMode _dmMode, unsigned int _vertexCount,
		unsigned int _instance_count,
		unsigned int _base_instance)
	{
		//!@todo Replace first element
		const GLint first = 0;

		m_APIVersionLayer.m_SeletectedProfile.RunDrawArraysFunction(_vertexCount, _instance_count, _base_instance, first);

#ifndef DROP_EDITOR
		GetStatistic()->RecordDrawCalls();
#endif
	}

	bool GLSystem::_CreateContext(I_Window* _window)
	{
		Debug(__FUNCTION__);

		ASSERT(_window, "Invalid pointer on Window");

		if (_window == nullptr)
			return false;

#ifndef DROP_EDITOR
		if (engine && engine->mIsEditor)
		{
			auto context = std::make_shared<OGLRenderContextWINAPI>(_window);

			bool result = context->Initialise();
			ASSERT(result, "Failed creation context");

			m_RenderContext = context;
			ASSERT(m_RenderContext, "m_RenderContext is null");

			return result;
		}
		else
#endif
			Debug("[OGLDrv] GLSystem::createContext - creating GLFW starting");
		{
			//Debug("Implemented in {WindowSystemGLFW::Initialise} because crossplatform");
			// TODO:Replace params: on CVARS VALUES
			auto context = std::make_shared<OGLRenderContextGLFW>(8, 8, 8, _window->m_Vars.alphaBits, _window->m_Vars.zdepth, _window->m_Vars.bpp, 0);
			// For GLFW version is contains GLFWwindow pointer
			context->setWindow(_window->m_Vars.hDC);

			m_RenderContext = context;

			Debug("[OGLDrv] GLSystem::createContext - GLFW created");

			return true;
		}
		return false;
	}

	/**
	*/
	void GLSystem::EndFrame()
	{
		this->FlushGPU();

		ASSERT(engine, "Engine not exist");
		ASSERT(engine->iWindow, "Engine->iWindow not exist"); //-V595

#if !IS_OS_ANDROID
#ifndef DROP_EDITOR
		if (engine && engine->mIsEditor)
			SwapBuffers((HDC)engine->iWindow->m_Vars.hDC);
		else
#endif
#endif
			engine->iWindow->SwapBuffers();
	}

	void GLSystem::BeginFrame() {
		this->Clear(RenderBuffer::ALL_BUFFERS);
	}

	/**
	*/
	bool GLSystem::IsComputeSupported() {
		return m_APIVersionLayer.m_SeletectedProfile.ValidateComputeShaderExtension();
	}

	void GLSystem::ClearDepthStencilColor()
	{
		CHECK_GL_ERROR();
#if (!IS_OS_ANDROID && !IS_OS_IOS)
		glClearDepth(Math::ONEFLOAT);
		CHECK_GL_ERROR();
#else
		glClearDepthf(Math::ONEFLOAT);
		CHECK_GL_ERROR();
#endif
		CHECK_GL_ERROR();
		glClearStencil(0);
		CHECK_GL_ERROR();
		glStencilMask(0xFF);
		CHECK_GL_ERROR();

		this->Clear(RenderBuffer::ALL_BUFFERS);
	}

	void GLSystem::ClearDepthColor()
	{
		this->Clear(RenderBuffer::COLOR_AND_DEPTH);
	}

	void GLSystem::ClearDepthOnly()
	{
		this->Clear(RenderBuffer::DEPTH_BUFFER);
	}

	void GLSystem::ClearColorOnly()
	{
		this->Clear(RenderBuffer::COLOR_BUFFER);
	}

	void GLSystem::ProcessQueue() {
		ASSERT(RenderQueue, "Invalid pointer on RenderQueue");

		CommandForDraw task;

		if (RenderQueue->obj.size_approx() == 0)
		{
			//Debug("Queue empty");
			return;
		}

		ASSERT(RenderQueue->obj.try_dequeue(task), "Empty Render queue");

		//DebugM("Process command with name:%s", task.name.c_str());

		if (task.m_Type == CommandForDraw::Type::ELEMENTS)
		{
			_DrawElements(task.m_Mode, task.m_Indices, task.m_IndexCount, task.m_InstanceCount, task.m_BaseInstance);
			//DebugM("Process command ELEMENT with name:%s, pointer: %p, instance_count: %i, base_instance: %i", task.name.c_str(), task.indices, task.instance_count, task.base_instance);
		}
		else if (task.m_Type == CommandForDraw::Type::ARRAYS)
		{
			_DrawArrays(task.m_Mode, task.m_VertexCount, task.m_InstanceCount, task.m_BaseInstance);
			//DebugM("Process command ARRAY with name:%s, pointer: %p, instance_count: %i, base_instance: %i", task.name.c_str(), task.indices, task.instance_count, task.base_instance);
		}
	}
}