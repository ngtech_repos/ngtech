/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "RenderPrivate.h"
//***************************************************************************
#include "GLSystem.h"
#include "GL_VertexBuffer.h"
#include "GLTexture.h"
//***************************************************************************

namespace NGTech {
	/**
	*/
	GLOcclusionQuery* GLSystem::GetOQ() { return new GLOcclusionQuery(); }

	/**
	*/
	GLTexture* GLSystem::TextureCreate(const std::shared_ptr<TextureParamsTransfer>&_obj)
	{
		// TODO: add check on default resource

		ASSERT(_obj, "Invalid pointer on _obj");
		if (!_obj) return nullptr;

		switch (_obj->m_Type)
		{
		case TextureParamsTransfer::Type::TEXTURE_2D:
			return TextureCreate2DUtil(_obj->path, _obj);
			break;

		case TextureParamsTransfer::Type::TEXTURE_3D:
			ASSERT(false, "Not implemented yet");
			return nullptr;
			break;

		case TextureParamsTransfer::Type::TEXTURE_CUBE:
			return TextureCreateCubeFromFileUtil(_obj->path);
			break;

		default:
			ASSERT(false, "Invalid texture type");
			return nullptr;
		}

		return nullptr;	
	}

	/**
	*/
	GLTexture* GLSystem::TextureCreate2DUtil(const String &path, const std::shared_ptr<TextureParamsTransfer>&textP) { return GLTexture::Create2d(path, textP); }

	/**
	*/
	GLTexture* GLSystem::TextureCreateCubeUtil(const String &path) { return GLTexture::CreateCube(path); }

	/**
	*/
	GLTexture* GLSystem::TextureCreate2DUtil(uint32_t width, uint32_t height, const Format& format, const String&path) { return GLTexture::Create2d(width, height, format, path); }

	/**
	*/
	GLTexture* GLSystem::TextureCreate3DUtil(uint32_t width, uint32_t height, uint32_t depth, const Format& format, const String&path) { return GLTexture::Create3d(width, height, depth, format, path); }

	/**
	*/
	GLTexture* GLSystem::TextureCreateCubeUtil(uint32_t width, uint32_t height, const Format& format, const String&path) { return GLTexture::CreateCube(width, height, format, path); }

	GLTexture* GLSystem::TextureCreateCubeFromFileUtil(const String&path) {
		return GLTexture::GLCreateCubeTextureFromFile(path);
	}

	/**
	*/
	GLShader  *GLSystem::ShaderCreate(const String &path, const String &defines, bool allowShaderCache) { return GLShader::Create(path, defines, allowShaderCache); }

	/**
	*/
	GLFBO*GLSystem::CreateFBO(unsigned int x, unsigned int y) { return GLFBO::Create(x, y); }

	/**
	*/
	OGL_IndexBuffer *GLSystem::CreateIBO(void *data, uint32_t numElements, DataType dataType)
	{
		auto ptr = OGL_IndexBuffer::Create(data, numElements, dataType);
		return ptr;
	}

	/**
	*/
	OGL_VertexBuffer *GLSystem::CreateVBO(void *data, uint32_t numElements, uint32_t elemSize, DataType dataType, TypeDraw drawType)
	{
		auto ptr = OGL_VertexBuffer::Create(data, numElements, elemSize, dataType, drawType);
		return ptr;
	}

	/**
	*/
	I_ModelHelper*GLSystem::GetModelHelper() { return new ModelHelper(); }
}