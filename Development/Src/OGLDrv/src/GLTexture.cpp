/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "RenderPrivate.h"
//***************************************************************************
#include "GLExtensions.h"
#include "GLTexture.h"
#include "GLCommons.h"
//***************************************************************************
//see http://www.slideshare.net/Mark_Kilgard/opengl-45-update-for-nvidia-gpus?qid=0ec7626a-1ec3-4178-aefb-2636607cf26e&v=qf1&b=&from_search=1

namespace NGTech
{
	/**
	*/
	GLTexture *GLTexture::Create2d(const String &path, const std::shared_ptr<TextureParamsTransfer>& textP) {
		ImageData *image = Image::Load(path);
		ASSERT(image, "Failed loading Image %s", path.c_str());

		if (!image) {
			ErrorWrite("Failed loading Image %s", path.c_str());
			return nullptr;
		}

		// textP Process
		if (textP)
		{
			if (textP->generateNormal) {
				Image::GenerateNormalMap(image, textP->normalLevel);
			}
		}

		void *addr = image->data.data();
		void **data = &addr;

		//!@todo ��������� ��������� ��������
		Format format = Format::GLFMT_UNKNOWN;

		if (image->Format == ILFormats::RGB) {
			format = Format::GLFMT_R8G8B8;
		}
		else if (image->Format == ILFormats::RGBA) {
			format = Format::GLFMT_A8R8G8B8;
		}

		GLTexture *texture = Create(image->Width, image->Height, image->Depth,
			TextureTarget::TEXTURE_2D, format, data, path);

		delete image;

		return texture;
	}

	[[deprecated]]
	GLTexture *GLTexture::CreateCube(const String &path) {
		ASSERT(!path.empty(), "Empty path");
		if (path.empty()) return nullptr;

		ImageData *image[6];
		const char *suffix[] = { "px", "nx", "py", "ny", "pz", "nz" };

		for (unsigned int i = 0; i < 6; i++) {
			char dst[1024];
			sprintf(dst, path.c_str(), suffix[i]);
			image[i] = Image::Load(path);
		}
		GLubyte *data[6];
		for (int i = 0; i < 6; i++)
			data[i] = image[i]->data.data();

		//!@todo ��������� ��������� ��������
		Format format = Format::GLFMT_UNKNOWN;
		if (image[0]->Format == ILFormats::RGB)
			format = Format::GLFMT_R8G8B8;
		else if (image[0]->Format == ILFormats::RGBA)
			format = Format::GLFMT_A8R8G8B8;

		GLTexture *texture = Create(image[0]->Width, image[0]->Height, image[0]->Depth,
			TextureTarget::TEXTURE_CUBE, format, (void**)data, path);

		for (unsigned int i = 0; i < 6; i++)
			delete image[i];

		return texture;
	}

	GLTexture* GLTexture::GLCreateCubeTextureFromFile(const String &path)
	{
		Ref<GLTexture> ptr = new GLTexture(path);
		ASSERT(ptr->m_texData, "Invalid pointer on m_texData");
		ptr->m_texData->Target = GL_TEXTURE_CUBE_MAP;

		DDS_Image_Info info;

		if (!LoadFromDDS(path.c_str(), &info))
		{
			ErrorWrite("Error: Could not load texture %s !", path.c_str());
			SAFE_DELETE(ptr);
			return nullptr;
		}

		CHECK_GL_ERROR();
		glGenTextures(1, &ptr->m_texData->glID);
		CHECK_GL_ERROR();
		glBindTexture(GL_TEXTURE_CUBE_MAP, ptr->m_texData->glID);

		CHECK_GL_ERROR();
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		CHECK_GL_ERROR();
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		CHECK_GL_ERROR();
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		if (info.MipLevels > 1)
		{
			CHECK_GL_ERROR();
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			CHECK_GL_ERROR();
		}
		else
		{
			CHECK_GL_ERROR();
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			CHECK_GL_ERROR();
		}

		GLsizei pow2s = ddsShared::NextPow2(info.Width);
		GLsizei facesize;

		if (info.Format == Format::GLFMT_DXT1 || info.Format == Format::GLFMT_DXT5)
		{
			// compressed
			GLsizei size;
			GLsizei offset = 0;

			for (int i = 0; i < 6; ++i)
			{
				for (unsigned int j = 0; j < info.MipLevels; ++j)
				{
					size = Math::Max(1, pow2s >> j);
					facesize = ddsShared::GetCompressedLevelSize(info.Width, info.Height, j, info.Format);

					auto format = static_cast<unsigned int>(info.Format);
					glCompressedTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, j, map_Format_Internal[format],
						size, size, 0, facesize, (char*)info.Data + offset);

					offset += facesize;
				}
			}
		}
		else
		{
			// uncompressed
			GLsizei size;
			GLsizei offset = 0;
			GLsizei bytes = 4;

			if (info.Format == Format::GLFMT_A16B16G16R16F)
				bytes = 8;

			for (unsigned int i = 0; i < 6; ++i)
			{
				for (unsigned int j = 0; j < info.MipLevels; ++j)
				{
					size = Math::Max(1, pow2s >> j);
					facesize = size * size * bytes;

					auto format = static_cast<unsigned int>(info.Format);

					CHECK_GL_ERROR();
					glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, j, map_Format_Internal[format], size, size, 0,
						map_Format_Src[format], map_Format_Type[format], (char*)info.Data + offset);
					CHECK_GL_ERROR();

					offset += facesize;
				}
			}
		}
		if (info.Data)
			free(info.Data);

		bool _allfine = checkGLError(__FILE__, __LINE__);
		if (_allfine == false)
		{
			auto data = ptr->GetTextureData();
			glDeleteTextures(1, &data->glID);
			data->glID = 0;

			ErrorWrite("Error: Could not create texture %s !", path);
		}
		else
			LogPrintf("Created cube texture:%s %i", path.c_str(), info.Width, " x %i", info.Height);

		return ptr;
	}

	/**
	*/
	GLTexture *GLTexture::Create2d(Image *image, const String &path)
	{
		void *addr = image->GetData();
		void **data = &addr;

		Format format = Format::GLFMT_UNKNOWN;

		//!@todo ��������� ��������� ��������
		if (image->GetFormat() == ILFormats::RGB) {
			format = Format::GLFMT_R8G8B8;
		}
		else if (image->GetFormat() == ILFormats::RGBA) {
			format = Format::GLFMT_A8R8G8B8;
		}

		GLTexture *texture = Create(image->GetWidth(), image->GetHeight(), image->GetDepth(),
			TextureTarget::TEXTURE_2D, format, data, path);

		return texture;
	}

	/**
	*/
	GLTexture *GLTexture::Create3d(Image *image, const String &path)
	{
		void *addr = image->GetData();
		void **data = &addr;

		//!@todo ��������� ��������� ��������
		Format format = Format::GLFMT_UNKNOWN;
		if (image->GetFormat() == ILFormats::RGB) {
			format = Format::GLFMT_R8G8B8;
		}
		else if (image->GetFormat() == ILFormats::RGBA) {
			format = Format::GLFMT_A8R8G8B8;
		}

		GLTexture *texture = Create(image->GetWidth(), image->GetHeight(), image->GetDepth(),
			TextureTarget::TEXTURE_3D, format, data, path);

		return texture;
	}

	/**
	*/
	GLTexture *GLTexture::CreateCube(Image **image, const String &path)
	{
		GLubyte *data[6];
		for (unsigned int i = 0; i < 6; i++) {
			data[i] = image[i]->GetData();
		}

		//!@todo ��������� ��������� ��������
		Format format = Format::GLFMT_UNKNOWN;
		if (image[0]->GetFormat() == ILFormats::RGB) {
			format = Format::GLFMT_R8G8B8;
		}
		else if (image[0]->GetFormat() == ILFormats::RGBA) {
			format = Format::GLFMT_A8R8G8B8;
		}

		GLTexture *texture = Create(image[0]->GetWidth(), image[0]->GetHeight(), image[0]->GetDepth(),
			TextureTarget::TEXTURE_CUBE, format, (void**)data, path);

		return texture;
	}

	/**
	*/
	GLTexture *GLTexture::Create2d(unsigned int width, unsigned int height, Format format, const String&path)
	{
		GLubyte *data[1];
		data[0] = nullptr;

		return Create(width, height, 1, TextureTarget::TEXTURE_2D, format, (void**)data, path);
	}

	/**
	*/
	GLTexture *GLTexture::Create3d(unsigned int width, unsigned int height, unsigned int depth, Format format, const String&path)
	{
		GLubyte *data[1];
		data[0] = nullptr;

		return Create(width, height, depth, TextureTarget::TEXTURE_3D, format, (void**)data, path);
	}

	/**
	*/
	GLTexture *GLTexture::CreateCube(unsigned int width, unsigned int height, Format format, const String&path) {
		GLubyte *data[6];
		for (unsigned int i = 0; i < 6; i++) {
			data[i] = nullptr;
		}

		return Create(width, height, 1, TextureTarget::TEXTURE_CUBE, format, (void**)data, path);
	}

	/**
	*/
	GLTexture::~GLTexture() {
		ASSERT(m_texData, "m_texData is nullptr");

		CHECK_GL_ERROR();
		glDeleteTextures(1, &m_texData->glID);
		CHECK_GL_ERROR();

		SAFE_DELETE(m_texData);
	}

	/**
	*/
	void GLTexture::SetWrap(WrapType _wrap)
	{
		MT::ScopedGuard guard(mutex);

		ASSERT(m_texData, "Invalid pointer on m_texData");

		switch (_wrap)
		{
		case WrapType::ZERO:
			m_texData->Wrap = 0;
			break;
		case WrapType::REPEAT:
			m_texData->Wrap = GL_REPEAT;
			break;
		case WrapType::CLAMP:
			m_texData->Wrap = GL_CLAMP;
			break;
		case WrapType::CLAMP_TO_EDGE:
			m_texData->Wrap = GL_CLAMP_TO_EDGE;
			break;
		default:
			ASSERT("Invalid value of wraptype");
			return;
			break;
		}

		CHECK_GL_ERROR();
		//IN OGL4.5 replaced on glTextureParameteri
		glTexParameteri(m_texData->Target, GL_TEXTURE_WRAP_S, m_texData->Wrap);
		CHECK_GL_ERROR();
		glTexParameteri(m_texData->Target, GL_TEXTURE_WRAP_T, m_texData->Wrap);
		CHECK_GL_ERROR();
		glTexParameteri(m_texData->Target, GL_TEXTURE_WRAP_R, m_texData->Wrap);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLTexture::_SetWrap(unsigned int _wrap)
	{
		MT::ScopedGuard guard(mutex);

		switch (_wrap)
		{
		case 0:
			SetWrap(WrapType::ZERO);
			break;

		case GL_REPEAT:
			SetWrap(WrapType::REPEAT);
			break;

		case GL_CLAMP:
			SetWrap(WrapType::CLAMP);
			break;

		case GL_CLAMP_TO_EDGE:
			SetWrap(WrapType::CLAMP_TO_EDGE);
			break;

		default:
			ASSERT(false, "Invalid value %i", _wrap);
			break;
		}
	}

	/**
	*/
	void GLTexture::SetFilter(Filter filter)
	{
		MT::ScopedGuard guard(mutex);

		ASSERT(m_texData, "invalid pointer on m_texData");

		if (filter == Filter::NEAREST)
		{
			m_texData->magFilter = GL_NEAREST;
			m_texData->minFilter = GL_NEAREST;
			CHECK_GL_ERROR();
			glTexParameteri(m_texData->Target, GL_TEXTURE_MAG_FILTER, m_texData->magFilter);
			CHECK_GL_ERROR();
			glTexParameteri(m_texData->Target, GL_TEXTURE_MIN_FILTER, m_texData->minFilter);
			CHECK_GL_ERROR();
		}
		else if (filter == Filter::LINEAR)
		{
			m_texData->magFilter = GL_LINEAR;
			m_texData->minFilter = GL_LINEAR;
			CHECK_GL_ERROR();
			glTexParameteri(m_texData->Target, GL_TEXTURE_MAG_FILTER, m_texData->magFilter);
			CHECK_GL_ERROR();
			glTexParameteri(m_texData->Target, GL_TEXTURE_MIN_FILTER, m_texData->minFilter);
			CHECK_GL_ERROR();
		}
		else if (filter == Filter::NEAREST_MIPMAP_NEAREST)
		{
			m_texData->magFilter = GL_NEAREST;
			m_texData->minFilter = GL_NEAREST_MIPMAP_NEAREST;
			CHECK_GL_ERROR();
			glTexParameteri(m_texData->Target, GL_TEXTURE_MAG_FILTER, m_texData->magFilter);
			CHECK_GL_ERROR();
			glTexParameteri(m_texData->Target, GL_TEXTURE_MIN_FILTER, m_texData->minFilter);
			CHECK_GL_ERROR();
			glGenerateMipmap(m_texData->Target);
			CHECK_GL_ERROR();
		}
		else if (filter == Filter::LINEAR_MIPMAP_NEAREST)
		{
			m_texData->magFilter = GL_LINEAR;
			m_texData->minFilter = GL_LINEAR_MIPMAP_NEAREST;
			CHECK_GL_ERROR();
			glTexParameteri(m_texData->Target, GL_TEXTURE_MAG_FILTER, m_texData->magFilter);
			CHECK_GL_ERROR();
			glTexParameteri(m_texData->Target, GL_TEXTURE_MIN_FILTER, m_texData->minFilter);
			CHECK_GL_ERROR();
			glGenerateMipmap(m_texData->Target);
			CHECK_GL_ERROR();
		}
		else if (filter == Filter::LINEAR_MIPMAP_LINEAR)
		{
			m_texData->magFilter = GL_LINEAR;
			m_texData->minFilter = GL_LINEAR_MIPMAP_LINEAR;
			CHECK_GL_ERROR();
			glTexParameteri(m_texData->Target, GL_TEXTURE_MAG_FILTER, m_texData->magFilter);
			CHECK_GL_ERROR();
			glTexParameteri(m_texData->Target, GL_TEXTURE_MIN_FILTER, m_texData->minFilter);
			CHECK_GL_ERROR();
			glGenerateMipmap(m_texData->Target);
			CHECK_GL_ERROR();
		}
	}

	/**
	*/
	void GLTexture::SetAniso(Aniso _aniso)
	{
		MT::ScopedGuard guard(mutex);
		CHECK_GL_ERROR();

		if ((_aniso != Aniso::ANISO_X0) && (_aniso != Aniso::ANISO_NUM))
		{
			m_texData->aniso = _aniso;
			auto _anisoValue = static_cast<unsigned int>(_aniso);
			glTextureParameterfEXT(m_texData->glID, m_texData->Target, GL_TEXTURE_MAX_ANISOTROPY_EXT, _anisoValue);
		}
		else {
			glTextureParameterfEXT(m_texData->glID, m_texData->Target, GL_TEXTURE_MAX_ANISOTROPY_EXT, 1.0f);
		}
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLTexture::Set()
	{
		MT::ScopedGuard guard(mutex);

		ASSERT(m_texData, "invalid pointer on m_texData");

		CHECK_GL_ERROR();
		glBindTexture(m_texData->Target, m_texData->glID);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLTexture::UnSet()
	{
		MT::ScopedGuard guard(mutex);

		ASSERT(m_texData, "invalid pointer on m_texData");

		CHECK_GL_ERROR();
		glBindTexture(m_texData->Target, 0);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLTexture::Set(unsigned int tex_unit)
	{
		MT::ScopedGuard guard(mutex);
		ASSERT(m_texData, "Invalid pointer on m_texData");

		CHECK_GL_ERROR();

		glActiveTexture(GL_TEXTURE0 + tex_unit);
		CHECK_GL_ERROR();

		glBindTexture(m_texData->Target, m_texData->glID);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLTexture::Unset(unsigned int tex_unit)
	{
		MT::ScopedGuard guard(mutex);

		ASSERT(m_texData, "Invalid pointer on m_texData");
		CHECK_GL_ERROR();

		glActiveTexture(GL_TEXTURE0 + tex_unit);
		CHECK_GL_ERROR();

		glBindTexture(m_texData->Target, 0);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLTexture::Copy(int face)
	{
		MT::ScopedGuard guard(mutex);

		ASSERT(m_texData, "Invalid pointer on m_texData");

		CHECK_GL_ERROR();

		glActiveTexture(m_texData->Target);
		CHECK_GL_ERROR();

		glBindTexture(m_texData->Target, m_texData->glID);

		CHECK_GL_ERROR();
		if (face < 0)
		{
			glCopyTexSubImage2D(m_texData->Target, 0, 0, 0, 0, 0, m_texData->Width, m_texData->Height);
			CHECK_GL_ERROR();
		}
		else
		{
			glCopyTexSubImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + face, 0, 0, 0, 0, 0, m_texData->Width, m_texData->Height);
			CHECK_GL_ERROR();
		}
		CHECK_GL_ERROR();

		glBindTexture(m_texData->Target, 0);
		CHECK_GL_ERROR();
		glActiveTexture(GL_TEXTURE0);
		CHECK_GL_ERROR();
	}

	/**
	*/
	GLTexture *GLTexture::Create(unsigned int width, unsigned int height, unsigned int depth, TextureTarget target, Format format, void **data, const String& path)
	{
		static const GLuint mTargets[] =
		{
			/*ZERO*/0,
			/*TEXTURE_2D*/GL_TEXTURE_2D,
			/*TEXTURE_3D*/ GL_TEXTURE_3D,
			/*TEXTURE_CUBE*/GL_TEXTURE_CUBE_MAP
		};

		GLTexture *texture = new GLTexture(path);
		ASSERT(texture->m_texData, "Invalid pointer on m_texData");

		CHECK_GL_ERROR();

		texture->m_texData->selectedTarget = target;

		texture->m_texData->Width = width;
		texture->m_texData->Height = height;
		texture->m_texData->Depth = depth;

		texture->m_texData->Wrap = GL_REPEAT;
		texture->m_texData->Target = mTargets[static_cast<unsigned int>(target)];

		CHECK_GL_ERROR();
		texture->_ActivateFormat(format);

		CHECK_GL_ERROR();
		glGenTextures(1, &texture->m_texData->glID);
		CHECK_GL_ERROR();
		glBindTexture(texture->m_texData->Target, texture->m_texData->glID);//IN DSA is not needed
		CHECK_GL_ERROR();

		texture->_SetWrap(texture->m_texData->Wrap);

		CHECK_GL_ERROR();

		texture->SetAniso(GetRender()->defAniso);

		CHECK_GL_ERROR();

		Filter filter = GetRender()->defFilter;
		texture->_ActivateTarget(data);// LOD=0
		texture->SetFilter(filter);
		glBindTexture(texture->m_texData->Target, 0);//IN DSA is not needed

		return texture;
	}

	/**
	*/
	void GLTexture::_ActivateTarget(void **data)
	{
		MT::ScopedGuard guard(mutex);
		ASSERT(m_texData, "Invalid pointer on m_texData");

		CHECK_GL_ERROR();

		if ((this->m_texData->Target == GL_TEXTURE_1D) || (this->m_texData->Target == GL_TEXTURE_1D_ARRAY))
		{
			glTexImage1D(this->m_texData->Target, 0, this->m_texData->internalFormat, this->m_texData->Width, 0, this->m_texData->srcFormat, this->m_texData->dataType, data[0]);
			CHECK_GL_ERROR();
		}
		else if ((this->m_texData->Target == GL_TEXTURE_2D) || (this->m_texData->Target == GL_TEXTURE_2D_ARRAY))
		{
			glTexImage2D(this->m_texData->Target, 0, this->m_texData->internalFormat, this->m_texData->Width, this->m_texData->Height, 0, this->m_texData->srcFormat, this->m_texData->dataType, data[0]);
			CHECK_GL_ERROR();
		}
		else if ((this->m_texData->Target == GL_TEXTURE_3D) || (this->m_texData->Target == GL_TEXTURE_CUBE_MAP_ARRAY))
		{
			glTexImage3D(this->m_texData->Target, 0, this->m_texData->internalFormat, this->m_texData->Width, this->m_texData->Height, this->m_texData->Depth, 0, this->m_texData->srcFormat, this->m_texData->dataType, data[0]);
			CHECK_GL_ERROR();
		}
		else if (this->m_texData->Target == GL_TEXTURE_CUBE_MAP)
		{
			for (unsigned int i = 0; i < 6; i++) {
				glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, this->m_texData->internalFormat, this->m_texData->Width, this->m_texData->Height, 0, this->m_texData->srcFormat, this->m_texData->dataType, data[i]);
				CHECK_GL_ERROR();
			}
		}
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLTexture::_ActivateFormat(Format _format)
	{
		MT::ScopedGuard guard(mutex);
		ASSERT(m_texData, "Invalid pointer on m_texData");

		auto format = static_cast<unsigned int>(_format);

		this->m_texData->srcFormat = map_Format_Src[format];
		this->m_texData->internalFormat = map_Format_Internal[format];
		this->m_texData->dataType = map_Format_Type[format];
	}

	/**
	*/
	void GLTexture::SetMinMipLevel(unsigned int level)
	{
		MT::ScopedGuard guard(mutex);
		ASSERT(m_texData, "Invalid pointer on m_texData");

		m_texData->_minMipLevel = level;

		CHECK_GL_ERROR();
		glTexParameteri(m_texData->Target, GL_TEXTURE_BASE_LEVEL, level);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void GLTexture::SetMaxMipLevel(unsigned int level)
	{
		MT::ScopedGuard guard(mutex);
		ASSERT(m_texData, "Invalid pointer on m_texData");

		m_texData->_maxMipLevel = level;

		CHECK_GL_ERROR();
		glTexParameteri(m_texData->Target, GL_TEXTURE_MAX_LEVEL, level);
		CHECK_GL_ERROR();
	}
}