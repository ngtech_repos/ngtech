/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "RenderPrivate.h"

#include "GLSystem.h"
#include "ModelHelper.h"

namespace NGTech
{
	ModelHelper::ModelHelper()
	{
	}

	ModelHelper::~ModelHelper()
	{
		CHECK_GL_ERROR();
		if (vertexlayout != 0)
		{
			glDeleteVertexArrays(1, &vertexlayout);
			vertexlayout = 0;
		}
		CHECK_GL_ERROR();
	}

	void ModelHelper::Draw() const {
		DrawInstanced(1, 0);
	}

	void ModelHelper::DrawInstanced(unsigned int instance_count, unsigned int base_instance) const {
		ASSERT(vertices, "Invalid pointer on vertices");
		ASSERT(indices, "Invalid pointer on indices");

		ASSERT(vertexbuffer, "Invalid pointer on vertexbuffer");
		ASSERT(indexbuffer, "Invalid pointer on indexbuffer");

		// sizeofVertex == 0 true
		ASSERT(sizeofVertex != 0, "Invalid sizeofVertex");
		// vertexlayout == 0 true
		ASSERT(vertexlayout != 0, "Invalid vertexlayout");
		// numVertices == 0 true
		ASSERT(numVertices != 0, "Invalid numVertices");
		// numIndices == 0 true
		ASSERT(numIndices != 0, "Invalid numIndices");

		_BindVAO();

		Util_DrawSubsetInstanced(instance_count, base_instance);

		_UnBindVAO();
	}

	void ModelHelper::Util_DrawSubsetInstanced(unsigned int instance_count, unsigned int base_instance) const
	{
		auto render = GetRender();
		ASSERT(render, "Invalid pointer");

		if (!render)
			return;

		render->DrawCommand({ CommandForDraw::Type::ELEMENTS, indices, numIndices, numVertices, instance_count, base_instance, "Util_DrawSubsetInstanced" });

		//!@todo ������� ��������� ��� ����, ��� �� ��������� ������ �� �������� ����
		render->ProcessQueue();
	}

	void ModelHelper::_CreateVAO()
	{
		CHECK_GL_ERROR();
		glGenVertexArrays(1, &this->vertexlayout);
		CHECK_GL_ERROR();
	}

	void ModelHelper::LoadToRender(void* _vertices, unsigned int* _indices, unsigned int _sizeofVertex, unsigned int _numVertices, unsigned int _numIndices)
	{
		ASSERT(_vertices, "[Failed loading to render] Invalid pointer on vertices");
		ASSERT(_indices, "[Failed loading to render] Invalid pointer on indices");

		// sizeofVertex == 0 true
		ASSERT(_sizeofVertex != 0, "[Failed loading to render] Invalid sizeofVertex");
		// numVertices == 0 true
		ASSERT(_numVertices != 0, "[Failed loading to render] Invalid numVertices");
		// numIndices == 0 true
		ASSERT(_numIndices != 0, "[Failed loading to render] Invalid numIndices");

		numVertices = _numVertices;
		numIndices = _numIndices;
		vertices = _vertices;
		indices = _indices;
		sizeofVertex = _sizeofVertex;

		//���������� ������ � VAO
		_GenerateVBO(sizeofVertex);
		_CreateVAO();

		ASSERT(vertexbuffer, "Invalid pointer on vertexbuffer");
		ASSERT(vertexbuffer, "Invalid pointer on indexbuffer");

		vertexbuffer->Bind();
		indexbuffer->Bind();

		_BindVAO();

		CHECK_GL_ERROR();
		// register all components for locations
		// 0 -> position
		glVertexAttribPointer(GLDECLUSAGE_POSITION,
			3, 			// number of values per vertex //Nick:��� ��� ���� 3 ������ ����
			GL_FLOAT,
			GL_FALSE,	// normalized
			_sizeofVertex, 			// stride (offset to next vertex data)
			(const GLvoid*)NULL);
		CHECK_GL_ERROR();

		// 5 -> texture coordinates
		glVertexAttribPointer(GLDECLUSAGE_TEXCOORD,
			2, 			// number of values per vertex
			GL_FLOAT,
			GL_FALSE,	// normalized
			_sizeofVertex, 			// stride (offset to next vertex data)
			(const GLvoid*) sizeof(Vec3));
		CHECK_GL_ERROR();

		// 3 -> normal
		glVertexAttribPointer(GLDECLUSAGE_NORMAL,
			3, 			// number of values per vertex
			GL_FLOAT,
			GL_FALSE,	// normalized
			_sizeofVertex, 			// stride (offset to next vertex data)
			(const GLvoid*)(sizeof(Vec3) + sizeof(Vec2)));
		CHECK_GL_ERROR();

		// 6 -> tangent
		glVertexAttribPointer(GLDECLUSAGE_TANGENT,
			3, 			// number of values per vertex
			GL_FLOAT,
			GL_FALSE,	// normalized
			_sizeofVertex, 			// stride (offset to next vertex data)
			(const GLvoid*)(2 * sizeof(Vec3) + sizeof(Vec2)));
		CHECK_GL_ERROR();

		// 7 -> binormal
		glVertexAttribPointer(GLDECLUSAGE_BINORMAL,
			3, 			// number of values per vertex
			GL_FLOAT,
			GL_FALSE,	// normalized
			_sizeofVertex, 	// stride (offset to next vertex data)
			(const GLvoid*)(3 * sizeof(Vec3) + sizeof(Vec2)));
		CHECK_GL_ERROR();

		glEnableVertexAttribArray(GLDECLUSAGE_POSITION);
		CHECK_GL_ERROR();
		glEnableVertexAttribArray(GLDECLUSAGE_TEXCOORD);
		CHECK_GL_ERROR();
		glEnableVertexAttribArray(GLDECLUSAGE_NORMAL);
		CHECK_GL_ERROR();
		glEnableVertexAttribArray(GLDECLUSAGE_TANGENT);
		CHECK_GL_ERROR();
		glEnableVertexAttribArray(GLDECLUSAGE_BINORMAL);
		CHECK_GL_ERROR();

		/*
		����� ������� ������
		String vertex_source =
		"#version 330\n"
		"uniform mat4 ViewProjection;\n" // the projection matrix uniform
		"layout(location = 0) in vec4 vposition;\n"
		"layout(location = 1) in vec4 vcolor;\n"
		"layout(location = GLDECLUSAGE_INSTANCE) in vec3 voffset;\n" // the per instance offset
		"out vec4 fcolor;\n"
		"void main() {\n"
		"   fcolor = vcolor;\n"
		"   gl_Position = ViewProjection*(vposition + vec4(voffset, 0));\n"
		"}\n";
		*/

		// INSTANCING SUPPORT - ���������� ������ �������� �����������
		// � ������� ��������� layout(location = GLDECLUSAGE_INSTANCE) in vec3 voffset;\n" // the per instance offset
		{
			// ��������� ���������� ��� INSTANSING
			// 2nd arg = 1 means updated attribute per instance, not per vertex
			// ���� https://www.khronos.org/registry/OpenGL/extensions/ARB/ARB_instanced_arrays.txt
			// ������ �� �������� �����: https://github.com/progschj/OpenGL-Examples/blob/master/06instancing1.cpp

			// a attrib divisor of 1 means that attribute 2 will advance once
			// every instance (0 would mean once per vertex)
			// 2nd arg = 1 means updated attribute per instance, not per vertex
			// ���� �� ����� ��� ������������ ������ ��� DRAW ARRAYs
			// set up generic attrib pointers
			glEnableVertexAttribArray(GLDECLUSAGE_INSTANCE);
			CHECK_GL_ERROR();
			glVertexAttribPointer(GLDECLUSAGE_INSTANCE, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (char*)0 + 0 * sizeof(GLfloat));
			CHECK_GL_ERROR();

			// a attrib divisor of 1 means that attribute 2 will advance once
			// every instance (0 would mean once per vertex)
			glVertexAttribDivisor(GLDECLUSAGE_INSTANCE, 1);
			CHECK_GL_ERROR();
		}

		_UnBindVAO();

		vertexbuffer->UnBind();

		indexbuffer->UnBind();
	}

	void ModelHelper::_BindVAO() const
	{
		// vertexlayout == 0 true
		ASSERT(vertexlayout != 0, "Invalid vertexlayout");

		CHECK_GL_ERROR();
		glBindVertexArray(vertexlayout);
		CHECK_GL_ERROR();
	}

	void ModelHelper::_UnBindVAO() const
	{
		CHECK_GL_ERROR();
		glBindVertexArray(0);
		CHECK_GL_ERROR();
	}

	void ModelHelper::_GenerateVBO(unsigned int _sizeofVertex)
	{
		GLSystem*render = (GLSystem*)GetRender();
		ASSERT(render, "Invalid pointer");

		vertexbuffer = render->CreateVBO(vertices, numVertices, _sizeofVertex, DataType::DT_FLOAT, TypeDraw::TD_STATIC_DRAW);
		ASSERT(vertexbuffer, "VB invalid pointer");
		indexbuffer = render->CreateIBO(indices, numIndices, DataType::DT_UNSIGNED_INT);
		ASSERT(indexbuffer, "IB invalid pointer");
	}

	OGL_VertexBuffer * ModelHelper::GetVB() const
	{
		ASSERT(vertexbuffer, "Invalid pointer");
		return vertexbuffer;
	}

	OGL_IndexBuffer * ModelHelper::GetIB() const
	{
		ASSERT(indexbuffer, "Invalid pointer");
		return indexbuffer;
	}

	void ModelHelper::UpdateAndDraw(unsigned int _offset) const {
		UpdateAndDrawInstanced(_offset, 1, 0);
	}

	void ModelHelper::UpdateAndDrawInstanced(unsigned int _offset, unsigned int instance_count, unsigned int base_instance) const
	{
		ASSERT(vertices, "Invalid pointer on vertices");
		ASSERT(indices, "Invalid pointer on indices");

		ASSERT(vertexbuffer, "Invalid pointer on vertexbuffer");
		ASSERT(indexbuffer, "Invalid pointer on indexbuffer");

		// sizeofVertex == 0 true
		ASSERT(sizeofVertex != 0, "Invalid sizeofVertex");
		// vertexlayout == 0 true
		ASSERT(vertexlayout != 0, "Invalid vertexlayout");
		// numVertices == 0 true
		ASSERT(numVertices != 0, "Invalid numVertices");
		// numIndices == 0 true
		ASSERT(numIndices != 0, "Invalid numIndices");

		_BindVAO();

		{
			vertexbuffer->FillBuffer(_offset);
			indexbuffer->FillBuffer(_offset);

			Util_DrawSubsetInstanced(instance_count, base_instance);
		}

		_UnBindVAO();
	}
}