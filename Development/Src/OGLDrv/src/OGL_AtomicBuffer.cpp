/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "RenderPrivate.h"

//W.I.P :work started 02.07.15

//***************************************************************************
#include "GLSystem.h"
//***************************************************************************

namespace NGTech
{
	OGL_AtomicBuffer::OGL_AtomicBuffer()
	{
		CHECK_GL_ERROR();
		stbID = 0;
		savedPtr = nullptr;
		GenerateBuffer();
	}

	OGL_AtomicBuffer::~OGL_AtomicBuffer()
	{
		CHECK_GL_ERROR();
		if (stbID)
			glDeleteBuffers(1, &stbID);
		CHECK_GL_ERROR();
		stbID = 0;
	}

	void OGL_AtomicBuffer::GenerateBuffer()
	{
		CHECK_GL_ERROR();
		glGenBuffers(1, &stbID);
	}

	void OGL_AtomicBuffer::Bind()
	{
		CHECK_GL_ERROR();
		if (!CheckBuffer())
			return;

		glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, stbID);
		CHECK_GL_ERROR();
	}

	void OGL_AtomicBuffer::UnBind()
	{
		CHECK_GL_ERROR();
		if (!CheckBuffer())
			return;

		glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, 0);
		CHECK_GL_ERROR();
	}

	void* OGL_AtomicBuffer::Map()
	{
#if !IS_OS_ANDROID
		CHECK_GL_ERROR();
		if (!CheckBuffer())
			return nullptr;

		void* ptr = glMapBuffer(GL_ATOMIC_COUNTER_BUFFER, GL_WRITE_ONLY);
		CHECK_GL_ERROR();
		return ptr;
#else
		return nullptr;
		//glMapBufferRange(GL_ELEMENT_ARRAY_BUFFER, 0, sizeof(int) * num_indices, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT);
#endif
	}

	void OGL_AtomicBuffer::UnMap()
	{
		CHECK_GL_ERROR();
		if (stbID == 0)
			return;

		CHECK_GL_ERROR();
		glUnmapBuffer(GL_ATOMIC_COUNTER_BUFFER);
		CHECK_GL_ERROR();
	}

	void OGL_AtomicBuffer::FillBuffer(unsigned int _size, void* ptr, const TypeDraw&_drawType)
	{
		CHECK_GL_ERROR();
		if ((!CheckBuffer()) || (!ptr))
			return;

		if (savedPtr == ptr)
		{
			Update(ptr);
			return;
		}
		savedPtr = ptr;

		auto index = static_cast<unsigned int>(_drawType);
		CHECK_GL_ERROR();
		glBufferData(GL_ATOMIC_COUNTER_BUFFER, _size, &ptr, map_TypeDraw[index]);
		CHECK_GL_ERROR();
	}

	void OGL_AtomicBuffer::FillBufferWB(unsigned int _size, void* ptr, const TypeDraw&_drawType)
	{
		CHECK_GL_ERROR();
		if ((!CheckBuffer()) || (!ptr))
			return;

		if (savedPtr == ptr)
		{
			UpdateWB(ptr);
			return;
		}
		savedPtr = ptr;
		Bind();

		auto index = static_cast<unsigned int>(_drawType);
		CHECK_GL_ERROR();
		glBufferData(GL_ATOMIC_COUNTER_BUFFER, _size, &ptr, map_TypeDraw[index]);
		CHECK_GL_ERROR();
		UnBind();
	}

	void OGL_AtomicBuffer::FillBufferZeroWB(unsigned int _size, const TypeDraw&_drawType)
	{
		CHECK_GL_ERROR();
		if ((!CheckBuffer()))
			return;

		Bind();

		auto index = static_cast<unsigned int>(_drawType);
		CHECK_GL_ERROR();
		glBufferData(GL_ATOMIC_COUNTER_BUFFER, _size, 0, map_TypeDraw[index]);
		CHECK_GL_ERROR();
		UnBind();
	}

	void OGL_AtomicBuffer::UpdateWB(void*& _shader_data)
	{
#if !IS_OS_ANDROID
		CHECK_GL_ERROR();
		if (!CheckBuffer())
			return;

		glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, stbID);
		CHECK_GL_ERROR();
		GLvoid* p = glMapBuffer(GL_ATOMIC_COUNTER_BUFFER, GL_WRITE_ONLY);
		CHECK_GL_ERROR();
		memcpy(p, &_shader_data, sizeof(_shader_data));
		glUnmapBuffer(GL_ATOMIC_COUNTER_BUFFER);
		CHECK_GL_ERROR();
#endif
	}

	void OGL_AtomicBuffer::Update(void*& _shader_data)
	{
#if !IS_OS_ANDROID
		CHECK_GL_ERROR();
		if ((!CheckBuffer()) || (!_shader_data))
			return;

		CHECK_GL_ERROR();
		GLvoid* p = glMapBuffer(GL_ATOMIC_COUNTER_BUFFER, GL_WRITE_ONLY);
		CHECK_GL_ERROR();
		memcpy(p, &_shader_data, sizeof(_shader_data));
#endif
	}

	void OGL_AtomicBuffer::BindBufferBase(int _id)
	{
		CHECK_GL_ERROR();
		glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, _id, stbID);
		CHECK_GL_ERROR();
	}

	void OGL_AtomicBuffer::UnBindBufferBase(int _id)
	{
		CHECK_GL_ERROR();
		glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, _id, 0);
		CHECK_GL_ERROR();
	}

	bool OGL_AtomicBuffer::CheckBuffer()
	{
		CHECK_GL_ERROR();
		if (stbID == 0)
		{
			Error("OGL_AtomicRenderBuffer::CheckBuffer()", true);
			return false;
		}
		return true;
	}
}