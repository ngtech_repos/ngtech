/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "RenderPrivate.h"

//***************************************************************************
#include <algorithm>
//***************************************************************************
#include "GLSystem.h"
#include "GL_IndexBuffer.h"
#include "GLSynch.h"
//***************************************************************************

namespace NGTech {
	/**
	*/
	OGL_IndexBuffer::OGL_IndexBuffer()
	{
		CHECK_GL_ERROR();
		data = nullptr;
		numElements = 0;
		frameNumber = 0;
		glID = 0;
		_size = 0;
		buffer_offset = 0;
		num_indices = 0;
		_Create();
	}

	/**
	*/
	void OGL_IndexBuffer::DeleteBuffers()
	{
		CHECK_GL_ERROR();
		if (buffer_sync[0] && glIsSync(buffer_sync[0]))
		{
			CHECK_GL_ERROR();
			glDeleteSync(buffer_sync[0]);
			CHECK_GL_ERROR();
		}
		if (buffer_sync[1] && glIsSync(buffer_sync[1]))
		{
			CHECK_GL_ERROR();
			glDeleteSync(buffer_sync[1]);
			CHECK_GL_ERROR();
		}
		if (buffer_sync[2] && glIsSync(buffer_sync[2]))
		{
			CHECK_GL_ERROR();
			glDeleteSync(buffer_sync[2]);
			CHECK_GL_ERROR();
		}

		data_locked.ptr = nullptr;
		buffer_sync[0] = NULL;
		buffer_sync[1] = NULL;
		buffer_sync[2] = NULL;

		glDeleteBuffers(1, &glID);
		CHECK_GL_ERROR();
	}
	/**
	*/
	OGL_IndexBuffer::~OGL_IndexBuffer() {
		CHECK_GL_ERROR();
		DeleteBuffers();
	}

	/**
	*/
	OGL_IndexBuffer *OGL_IndexBuffer::Create(void *_data, int numElements, DataType dataType) {
		CHECK_GL_ERROR();
		OGL_IndexBuffer *ibo = new OGL_IndexBuffer();

		ibo->numElements = numElements;
		ibo->data = _data;

		if (dataType == DataType::DT_UNSIGNED_INT)
			ibo->dataType = GL_UNSIGNED_INT;
		else if (dataType == DataType::DT_UNSIGNED_SHORT)
			ibo->dataType = GL_UNSIGNED_SHORT;

		if (numElements <= 65536) {
			ibo->dataType = GL_UNSIGNED_SHORT;
			ibo->elementSize = sizeof(unsigned short);
			ibo->Allocate(ibo->data, sizeof(unsigned short) * ibo->numElements, TypeDraw::TD_STATIC_DRAW);
		}
		else {
			ibo->dataType = GL_UNSIGNED_INT;
			ibo->elementSize = sizeof(int);
			ibo->Allocate(ibo->data, sizeof(int) * ibo->numElements, TypeDraw::TD_STATIC_DRAW);
		}

		return ibo;
	}

	/**
	*/
	void OGL_IndexBuffer::_Create() {
		CHECK_GL_ERROR();
		glGenBuffers(1, &glID);
	}

	/**
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glID);
	*/
	void OGL_IndexBuffer::Bind() const
	{
		CHECK_GL_ERROR();
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glID);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void OGL_IndexBuffer::UnBind() const
	{
		CHECK_GL_ERROR();
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void OGL_IndexBuffer::BindIndex(unsigned int idx) const
	{
		CHECK_GL_ERROR();
		glBindBufferBase(GL_ELEMENT_ARRAY_BUFFER, idx, glID);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void OGL_IndexBuffer::UnbindIndex(unsigned int idx) const
	{
		CHECK_GL_ERROR();
		glBindBufferBase(GL_ELEMENT_ARRAY_BUFFER, idx, 0);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void OGL_IndexBuffer::Allocate(const void *_data, size_t size, const TypeDraw& _drawType)
	{
		CHECK_GL_ERROR();
		data = (void*)_data;
		_size = size;

#ifndef DROP_EDITOR
		GetStatistic()->RecordMemoryWrite(size);
#endif
		auto index = static_cast<unsigned int>(_drawType);

		drawType = map_TypeDraw[index];

		OpenGLVersionManager::getInstance().m_SeletectedProfile.AllocateBuffer(_data, size, drawType, GL_ELEMENT_ARRAY_BUFFER, glID);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void OGL_IndexBuffer::FillBuffer(size_t offset) {
		CHECK_GL_ERROR();

#ifndef DROP_EDITOR
		GetStatistic()->RecordMemoryWrite(elementSize * numElements);
#endif
		OpenGLVersionManager::getInstance().m_SeletectedProfile.FillBuffer(glID, offset, elementSize * numElements, data);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void* OGL_IndexBuffer::_ResizeBuffer(locked_data indexdata_locked, int offset, void** _ptr)
	{
		CHECK_GL_ERROR();
		// update vertices buffer
		buffer_offset += buffer_size;
		//!@todo BUGFIX: ������� ������ ������� �������(��������� �� ���������)
		buffer_size = sizeof(_ptr);//Nick?

		auto window = GetWindow();
		// per-frame buffers
		if (frameNumber != window->m_Vars.frameNumber) {
			frameNumber = window->m_Vars.frameNumber;
			numElements = buffer_size;
		}
		else {
			numElements += buffer_size;
		}

		if (num_indices < numElements * 3)
		{
			num_indices = std::max(num_indices * 2, numElements * 3);

			// synchronization
			//Nick:disabled 5.01.16
			//GLSynch::waitSync(buffer_sync);

			Bind();

#if !IS_OS_ANDROID
			GLint index_flags = GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT | GL_MAP_UNSYNCHRONIZED_BIT/*Nick:added GL_MAP_UNSYNCHRONIZED_BIT 5.01.16*/;
			CHECK_GL_ERROR();
			glBufferStorage(GL_ELEMENT_ARRAY_BUFFER, this->elementSize, NULL, index_flags | GL_DYNAMIC_STORAGE_BIT);
			CHECK_GL_ERROR();
#else
			GLint index_flags = GL_MAP_WRITE_BIT;
#endif
			CHECK_GL_ERROR();
			indexdata_locked.ptr = glMapBufferRange(GL_ELEMENT_ARRAY_BUFFER, offset, this->elementSize, index_flags);
			CHECK_GL_ERROR();
			indexdata_locked.flags = index_flags;

			if (!indexdata_locked.ptr)
			{
				ErrorWrite("[%s] can't map buffer", __FUNCTION__);
				return nullptr;
			}

			(*_ptr) = indexdata_locked.ptr;

			// update vertices vao
			//update_vertex_array();
		}

		// synchronization
		//Nick:disabled 5.01.16
		//GLSynch::waitSync(buffer_sync, buffer_offset, buffer_size, num_indices);

		// copy vertices
		//Math::memcpy(_data.ptr + vertex_offset, vertex.get(), vertex_flush);

		return indexdata_locked.ptr;
	}

	/**
	*/
	void OGL_IndexBuffer::DeleteBuffer(locked_data _data)
	{
		CHECK_GL_ERROR();
		if (_data.ptr && glID != 0)
		{
			glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
			CHECK_GL_ERROR();
			_data.ptr = 0;
		}
	}
}