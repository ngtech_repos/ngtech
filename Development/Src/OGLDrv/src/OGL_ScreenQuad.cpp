/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "RenderPrivate.h"

#include "OGL_ScreenQuad.h"

/*
TODO:Render::OGL_ScreenQuad: ������� ������������ � ������ ��������,������������
https://code.google.com/p/steps-framework/source/browse/src/ScreenQuad.cpp
*/

namespace NGTech
{
	OGL_ScreenQuad::OGL_ScreenQuad()
	{
	}

	void OGL_ScreenQuad::_Init()
	{
		vertexbuffer = 0;
		vertexlayout = 0;

		CHECK_GL_ERROR();
		glGenBuffers(1, &vertexbuffer);
		CHECK_GL_ERROR();
		glGenVertexArrays(1, &vertexlayout);
		CHECK_GL_ERROR();

		float quadVertices[] =
		{
			// Positions        // Texture Coords
			-Math::ONEFLOAT, Math::ONEFLOAT, Math::ZEROFLOAT, Math::ZEROFLOAT, Math::ONEFLOAT,
			-Math::ONEFLOAT, -Math::ONEFLOAT, Math::ZEROFLOAT, Math::ZEROFLOAT, Math::ZEROFLOAT,
			Math::ONEFLOAT, Math::ONEFLOAT, Math::ZEROFLOAT, Math::ONEFLOAT, Math::ONEFLOAT,
			Math::ONEFLOAT, -Math::ONEFLOAT, Math::ZEROFLOAT, Math::ONEFLOAT, Math::ZEROFLOAT,
		};

		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		CHECK_GL_ERROR();
		glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
		CHECK_GL_ERROR();

		glBindVertexArray(vertexlayout);
		CHECK_GL_ERROR();
		{
			glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
			CHECK_GL_ERROR();

			glEnableVertexAttribArray(GLDECLUSAGE_POSITION);
			CHECK_GL_ERROR();
			glVertexAttribPointer(GLDECLUSAGE_POSITION, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
			CHECK_GL_ERROR();

			glEnableVertexAttribArray(GLDECLUSAGE_TEXCOORD);
			CHECK_GL_ERROR();
			glVertexAttribPointer(GLDECLUSAGE_TEXCOORD, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (GLvoid*)(3 * sizeof(GLfloat)));
			CHECK_GL_ERROR();
		}
		glBindVertexArray(0);
		CHECK_GL_ERROR();

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		CHECK_GL_ERROR();

		m_Inited = true;
	}

	OGL_ScreenQuad::~OGL_ScreenQuad()
	{
		CHECK_GL_ERROR();
		if (vertexlayout)
		{
			glDeleteVertexArrays(1, &vertexlayout);
			CHECK_GL_ERROR();
		}

		if (vertexbuffer)
		{
			glDeleteBuffers(1, &vertexbuffer);
			CHECK_GL_ERROR();
		}
	}

	void OGL_ScreenQuad::Draw()
	{
		if (m_Inited == false)
			_Init();

		CHECK_GL_ERROR();
		if (vertexlayout == 0)
		{
			Error("vertexlayout == 0", false);
			return;
		}

		glBindVertexArray(vertexlayout);

		CHECK_GL_ERROR();
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		CHECK_GL_ERROR();

		glBindVertexArray(0);
		CHECK_GL_ERROR();

#ifndef DROP_EDITOR
		GetStatistic()->RecordDrawCalls();
#endif
	}
}