/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "RenderPrivate.h"

//W.I.P :work started 02.07.15

//***************************************************************************
#include "GLSystem.h"
//***************************************************************************

namespace NGTech
{
	OGL_StorageBuffer::OGL_StorageBuffer()
	{
		CHECK_GL_ERROR();
		stbID = 0;
		savedPtr = nullptr;
		GenerateBuffer();
		CHECK_GL_ERROR();
	}

	OGL_StorageBuffer::~OGL_StorageBuffer()
	{
		CHECK_GL_ERROR();
		if (stbID)
		{
			glDeleteBuffers(1, &stbID);
			CHECK_GL_ERROR();
		}
		stbID = 0;
	}

	void OGL_StorageBuffer::GenerateBuffer()
	{
		CHECK_GL_ERROR();
		glGenBuffers(1, &stbID);
		CHECK_GL_ERROR();
	}

	void OGL_StorageBuffer::Bind()
	{
		CHECK_GL_ERROR();
		if (!CheckBuffer())
			return;

		glBindBuffer(GL_SHADER_STORAGE_BUFFER, stbID);
		CHECK_GL_ERROR();
	}

	void OGL_StorageBuffer::UnBind()
	{
		CHECK_GL_ERROR();
		if (!CheckBuffer())
			return;

		glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
		CHECK_GL_ERROR();
	}

	void* OGL_StorageBuffer::Map()
	{
#if !IS_OS_ANDROID
		if (!CheckBuffer())
			return nullptr;

		CHECK_GL_ERROR();
		return glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_WRITE);
#else
		return nullptr;
#endif
	}

	void OGL_StorageBuffer::UnMap()
	{
		CHECK_GL_ERROR();
		if (stbID == 0)
			return;

		CHECK_GL_ERROR();
		glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
		CHECK_GL_ERROR();
	}

	void OGL_StorageBuffer::FillBuffer(unsigned int _size, void* ptr, const TypeDraw&_drawType)
	{
		ASSERT(ptr, "Invalid pointer passed");
		if ((!CheckBuffer()) || (!ptr))
			return;

		if (savedPtr == ptr)
		{
			Debug("[OGL_StorageRenderBuffer::FillBufferWB()] Updates SSBO");
			Update(ptr);
			return;
		}
		savedPtr = ptr;

		auto index = static_cast<unsigned int>(_drawType);
		CHECK_GL_ERROR();
		glBufferData(GL_SHADER_STORAGE_BUFFER, _size, &ptr, map_TypeDraw[index]);
		CHECK_GL_ERROR();
	}

	void OGL_StorageBuffer::FillBufferWB(unsigned int _size, void* ptr, const TypeDraw&_drawType)
	{
		ASSERT(ptr, "Invalid pointer passed");
		if ((!CheckBuffer()) || (!ptr))
			return;

		CHECK_GL_ERROR();
		if (savedPtr == ptr)
		{
			Debug("[OGL_StorageRenderBuffer::FillBufferWB()] Updates SSBO");
			UpdateWB(ptr);
			return;
		}
		savedPtr = ptr;
		Bind();

		auto index = static_cast<unsigned int>(_drawType);
		glBufferData(GL_SHADER_STORAGE_BUFFER, _size, &ptr, map_TypeDraw[index]);
		CHECK_GL_ERROR();
		UnBind();
	}

	void OGL_StorageBuffer::FillBufferZeroWB(unsigned int _size, const TypeDraw&_drawType)
	{
		if ((!CheckBuffer()))
			return;

		Bind();

		auto index = static_cast<unsigned int>(_drawType);
		CHECK_GL_ERROR();
		glBufferData(GL_SHADER_STORAGE_BUFFER, _size, 0, map_TypeDraw[index]);
		CHECK_GL_ERROR();
		UnBind();
	}

	void OGL_StorageBuffer::FillBufferZero(unsigned int _size, const TypeDraw&_drawType)
	{
		if ((!CheckBuffer()))
			return;

		auto index = static_cast<unsigned int>(_drawType);
		CHECK_GL_ERROR();
		glBufferData(GL_SHADER_STORAGE_BUFFER, _size, 0, map_TypeDraw[index]);
		CHECK_GL_ERROR();
	}

	void OGL_StorageBuffer::UpdateWB(void*& _shader_data)
	{
#if !IS_OS_ANDROID
		if (!CheckBuffer())
			return;

		CHECK_GL_ERROR();
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, stbID);
		CHECK_GL_ERROR();
		GLvoid* p = glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_WRITE_ONLY);
		CHECK_GL_ERROR();
		memcpy(p, &_shader_data, sizeof(_shader_data));
		CHECK_GL_ERROR();
		glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
		CHECK_GL_ERROR();
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
		CHECK_GL_ERROR();
#endif
	}

	void OGL_StorageBuffer::Update(void*& _shader_data)
	{
#if !IS_OS_ANDROID
		if ((!CheckBuffer()) || (!_shader_data))
			return;

		CHECK_GL_ERROR();
		GLvoid* p = glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_WRITE_ONLY);
		CHECK_GL_ERROR();
		memcpy(p, &_shader_data, sizeof(_shader_data));
#endif
	}

	void OGL_StorageBuffer::BindBufferBase(int _id)
	{
		CHECK_GL_ERROR();
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, _id, stbID);
		CHECK_GL_ERROR();
	}

	void OGL_StorageBuffer::UnBindBufferBase(int _id)
	{
		CHECK_GL_ERROR();
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, _id, 0);
		CHECK_GL_ERROR();
	}

	bool OGL_StorageBuffer::CheckBuffer()
	{
		CHECK_GL_ERROR();
		if (stbID == 0)
		{
			Error("OGL_StorageRenderBuffer::CheckBuffer()", true);
			return false;
		}
		return true;
	}
}