/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "RenderPrivate.h"
//***************************************************************************
#include <algorithm>
//***************************************************************************
#include "GLSystem.h"
#include "GL_VertexBuffer.h"
#include "GLSynch.h"
//***************************************************************************

namespace NGTech {
	/**
	*/
	OGL_VertexBuffer::OGL_VertexBuffer()
	{
		CHECK_GL_ERROR();
		data = nullptr;
		numElements = 0;
		frameNumber = 0;
		glID = 0;
		_size = 0;
		buffer_offset = 0;
		num_indices = 0;
		_Create();
	}

	/**
	*/
	void OGL_VertexBuffer::DeleteBuffers()
	{
		CHECK_GL_ERROR();
		if (buffer_sync[0] && glIsSync(buffer_sync[0]))
		{
			CHECK_GL_ERROR();
			glDeleteSync(buffer_sync[0]);
			CHECK_GL_ERROR();
		}

		if (buffer_sync[1] && glIsSync(buffer_sync[1]))
		{
			CHECK_GL_ERROR();
			glDeleteSync(buffer_sync[1]);
			CHECK_GL_ERROR();
		}
		if (buffer_sync[2] && glIsSync(buffer_sync[2]))
		{
			CHECK_GL_ERROR();
			glDeleteSync(buffer_sync[2]);
			CHECK_GL_ERROR();
		}

		data_locked.ptr = nullptr;
		buffer_sync[0] = NULL;
		buffer_sync[1] = NULL;
		buffer_sync[2] = NULL;

		glDeleteBuffers(1, &glID);
		CHECK_GL_ERROR();
	}
	/**
	*/
	OGL_VertexBuffer::~OGL_VertexBuffer() {
		CHECK_GL_ERROR();
		DeleteBuffers();
		CHECK_GL_ERROR();
	}

	/**
	*/
	OGL_VertexBuffer *OGL_VertexBuffer::Create(void *_data, int numElements, int elemSize, DataType dataType, const TypeDraw& _drawType)
	{
		CHECK_GL_ERROR();
		OGL_VertexBuffer *vbo = new OGL_VertexBuffer();

		vbo->numElements = numElements;
		vbo->elementSize = elemSize;
		vbo->data = _data;

#if !IS_OS_ANDROID
		if (dataType == DataType::DT_FLOAT) {
			vbo->dataType = GL_FLOAT;
		}
		else if (dataType == DataType::DT_DOUBLE) {
			vbo->dataType = GL_DOUBLE;
		}
#else
		vbo->dataType = GL_FLOAT;
#endif
		auto index = static_cast<unsigned int>(_drawType);

		vbo->drawType = map_TypeDraw[index];

		vbo->Allocate(vbo->data, vbo->elementSize * vbo->numElements, _drawType);

		return vbo;
	}

	/**
	*/
	void OGL_VertexBuffer::_Create() {
		CHECK_GL_ERROR();
		glGenBuffers(1, &glID);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void OGL_VertexBuffer::Bind() const
	{
		CHECK_GL_ERROR();
		glBindBuffer(GL_ARRAY_BUFFER, glID);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void OGL_VertexBuffer::UnBind() const
	{
		CHECK_GL_ERROR();
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void OGL_VertexBuffer::BindIndex(unsigned int idx) const
	{
		CHECK_GL_ERROR();
		glBindBufferBase(GL_ARRAY_BUFFER, idx, glID);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void OGL_VertexBuffer::UnbindIndex(unsigned int idx) const
	{
		CHECK_GL_ERROR();
		glBindBufferBase(GL_ARRAY_BUFFER, idx, 0);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void OGL_VertexBuffer::Allocate(const void *_data, size_t size, const TypeDraw& _drawType)
	{
		CHECK_GL_ERROR();
		data = (void*)_data;
		_size = size;

#ifndef DROP_EDITOR
		GetStatistic()->RecordMemoryWrite(size);
#endif

		auto index = static_cast<unsigned int>(_drawType);
		drawType = map_TypeDraw[index];

		OpenGLVersionManager::getInstance().m_SeletectedProfile.AllocateBuffer(_data, size, drawType, GL_ARRAY_BUFFER, glID);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void OGL_VertexBuffer::FillBuffer(size_t offset) {
		CHECK_GL_ERROR();

#ifndef DROP_EDITOR
		GetStatistic()->RecordMemoryWrite(elementSize * numElements);
#endif
		OpenGLVersionManager::getInstance().m_SeletectedProfile.FillBuffer(glID, offset, elementSize * numElements, data);
		CHECK_GL_ERROR();
	}

	/**
	*/
	void* OGL_VertexBuffer::_ResizeBuffer(locked_data _data, int offset, void** _ptr)
	{
		CHECK_GL_ERROR();

		// update vertices buffer
		buffer_offset += buffer_size;
		buffer_size = sizeof(_ptr);//Nick?

		auto window = GetWindow();
		ASSERT(window, "Invalid pointer on window");

		// per-frame buffers
		if (frameNumber != window->m_Vars.frameNumber) {
			frameNumber = window->m_Vars.frameNumber;
			numElements = buffer_size;
		}
		else {
			numElements += buffer_size;
		}

		if (num_indices < numElements * 3)
		{
			num_indices = std::max(num_indices * 2, numElements * 3);

			// synchronization
			//Nick:disabled 5.01.16
			//GLSynch::waitSync(buffer_sync);

			Bind();

			// RB: removed GL_MAP_INVALIDATE_RANGE_BIT as it breaks with an optimization in the NVidia WHQL drivers >= 344.11
#if !IS_OS_ANDROID
			GLint index_flags = GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT | GL_MAP_UNSYNCHRONIZED_BIT/*Nick:added GL_MAP_UNSYNCHRONIZED_BIT 5.01.16*/;
			CHECK_GL_ERROR();
			glBufferStorage(GL_ARRAY_BUFFER, this->elementSize, NULL, index_flags | GL_DYNAMIC_STORAGE_BIT);
			CHECK_GL_ERROR();
#else
			GLint index_flags = GL_MAP_WRITE_BIT;
#endif
			CHECK_GL_ERROR();
			_data.ptr = glMapBufferRange(GL_ARRAY_BUFFER, offset, this->elementSize, index_flags);
			CHECK_GL_ERROR();
			_data.flags = index_flags;
			buffer_offset = 0;

			if (_data.ptr == nullptr)
			{
				Error("GLExt::render(): can't map buffer\n", true);
				return NULL;
			}

			if (_ptr)
				(*_ptr) = _data.ptr;

			//!@todo CLean this
			// update vertices vao
			//update_vertex_array();
		}

		// TODO:RENDER Implement update
		// synchronization
		//Nick:disabled 5.01.16
		//GLSynch::waitSync(buffer_sync, buffer_offset, buffer_size, num_indices);

		// copy vertices
		//Math::memcpy(_data.ptr + vertex_offset, vertex.get(), vertex_flush);

		return _data.ptr;
	}

	/**
	*/
	void OGL_VertexBuffer::DeleteBuffer(locked_data _data)
	{
		CHECK_GL_ERROR();
		if (_data.ptr && glID != 0)
		{
			CHECK_GL_ERROR();
			glUnmapBuffer(GL_ARRAY_BUFFER);
			CHECK_GL_ERROR();
			_data.ptr = 0;
		}
	}
}