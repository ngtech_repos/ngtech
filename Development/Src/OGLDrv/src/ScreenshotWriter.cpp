/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "RenderPrivate.h"

#include "../../Common/StringHelper.h"

#include "GLSystem.h"
#include "FileHelper.h"

namespace NGTech
{
	/**
	*/
	void GLSystem::WriteScreenshot(const char* _path) const
	{
		size_t unused = 42;

		unsigned int wx = mCurrentViewport.x;
		unsigned int wy = mCurrentViewport.y;
		unsigned int ww = mCurrentViewport.z;
		unsigned int wh = mCurrentViewport.w;

		unsigned char* output = new unsigned char[(ww - wx) * (wh - wy) * 3 * sizeof(char)];

		CHECK_GL_ERROR();
		glReadBuffer(GL_FRONT);
		CHECK_GL_ERROR();
		glReadPixels(wx, wy, ww, wh, GL_RGB, GL_UNSIGNED_BYTE, output);
		CHECK_GL_ERROR();

		unsigned char tgaHeader[12] = { 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		unsigned char header[6];
		unsigned char bits = 0;
		int colorMode = 0;
		unsigned char tempColors = 0;

		String originalPath(_path);

		// Nick:Note: ��� �������� �������� Antender'��, ����� ��������� ���
		// ��������������� ��, � �������� � ������ ����� � ��� ������� �� ����� �����������
		// ����� ������ ������ ������. ����������� ������ � #36
		String actualPath(originalPath);
		if (FileUtil::FileExist(actualPath))
		{
			int fileCount = 0;
			do
			{
				actualPath = (originalPath + ".bak" + StringHelper::to_string(fileCount) + ".tga");
			} while (FileUtil::FileExist(actualPath));
		}

		FILE* file = fopen(actualPath.c_str(), "wb");
		if (!file) {
			SAFE_DELETE_ARRAY(output);
			ErrorWrite("Cannot create screen shot file: %s", actualPath.c_str());
			return;
		}
		originalPath.clear();
		actualPath.clear();

		colorMode = 3;
		bits = 24;
		header[0] = (int)(ww - wx) % 256;
		header[1] = (int)(ww - wx) / 256;
		header[2] = (int)(wh - wy) % 256;
		header[3] = (int)(wh - wy) / 256;
		header[4] = bits;
		header[5] = 0;
		unused = fwrite(tgaHeader, sizeof(tgaHeader), 1, file);
		unused = fwrite(header, sizeof(header), 1, file);
		for (size_t i = 0; i < (ww - wx) * (wh - wy) * colorMode; i += colorMode) {
			tempColors = output[i];
			output[i] = output[i + 2];
			output[i + 2] = tempColors;
		}
		unused = fwrite(output, (ww - wx) * (wh - wy) * colorMode, 1, file);
		SAFE_DELETE_ARRAY(output);
		fclose(file);
	}
}