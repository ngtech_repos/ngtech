/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "RenderPrivate.h"
//***************************************************************************
#include "ShaderContainer.h"

namespace NGTech
{
	/**
	*/
	ShaderContainer::ShaderContainer()
	{
		stringVersion.clear();
		vResult.clear();
		fResult.clear();
		gResult.clear();
		tesResult.clear();
		tcsResult.clear();
		csResult.clear();

		_PushVersion();
	}

	/**
	*/
	ShaderContainer::~ShaderContainer()
	{
		Clear();
	}

	/**
	*/
	void ShaderContainer::Clear()
	{
		stringVersion.clear();
		vResult.clear();
		fResult.clear();
		gResult.clear();
		tesResult.clear();
		tcsResult.clear();
		csResult.clear();
	}

	/**
	*/
	void ShaderContainer::AddDefines(const String &_name, const String&_defines)
	{
		vResult.push_back(_defines);
		fResult.push_back(_defines);
		gResult.push_back(_defines);
		tesResult.push_back(_defines);
		tcsResult.push_back(_defines);
		csResult.push_back(_defines);
#ifdef RENDER_DEV
		DebugM("Shader %s compiled with defines: %s", _name, _defines.c_str());
#endif
	}

	/**
	*/
	void ShaderContainer::Determinate()
	{
		auto _profile = OpenGLVersionManager::getInstance().GetSelectedProfile();
		stringVersion = _profile.m_glslVersionString + "\n" + _profile.m_glslExtensionsString + "\n" + _profile.m_glslPrecisionOfType + "\n";

#ifdef RENDER_DEV
		Debug(str.c_str());
#endif
	}

	/**
	*/
	void ShaderContainer::_PushVersion()
	{
		Determinate();
		vResult.push_back(stringVersion);
		fResult.push_back(stringVersion);
		gResult.push_back(stringVersion);
		tesResult.push_back(stringVersion);
		tcsResult.push_back(stringVersion);
		csResult.push_back(stringVersion);
	}
}