#pragma once

// Inline for default shader pipeline - without SPIRV
// CP - Classic Pipeline

namespace NGTech
{
	/**
	Creation shader cache user directory
	*/
	static ENGINE_INLINE const char* _CP_CreateShaderCacheDirectoryUtil(const char * _file)
	{
		static String exeDir = GetExecutableDirPath();
#if PLATFORM_MEMORY_ADDRESS_SPACE == PLATFORM_MEMORY_ADDRESS_SPACE_32BIT
		static const char* dir = "/../userData/ShaderCache.32/";
#else
		static const char* dir = "/../userData/ShaderCache.64/";
#endif
		FileUtil::CreateDirectoryIfNotExist(exeDir + dir);

		static String mFilename;
		mFilename.clear();
		mFilename = exeDir + dir;
		mFilename += _file;
		mFilename += ".bin";

		return mFilename.c_str();
	}

	/**
	������� ���� ����������������� �������
	*/
	static ENGINE_INLINE bool _CP_DeleteShaderCacheUtil(GLShader&_shader)
	{
#ifndef _DISABLE_SHADER_CACHE
		static String exeDir = GetExecutableDirPath();
#if PLATFORM_MEMORY_ADDRESS_SPACE == PLATFORM_MEMORY_ADDRESS_SPACE_32BIT
		static const char* dir = "/../userData/ShaderCache.32/";
#else
		static const char* dir = "/../userData/ShaderCache.64/";
#endif

		// ���� ������ ���� ��� �� ��������� ������������� ���������� ����
		if (_shader.GetPath().empty() || (_shader.IsAllowShaderCache() == false))
			return false;

		if (!deleteBinary((exeDir + dir + _shader.GetPath() + ".bin").c_str()))
		{
			DebugM("[Shader::Recompile] Failed deletion shader %s%s cache", _shader.GetPath().c_str(), ".bin");
			return false;
		}
#endif

		return true;
	}

	/*
	��������� ������ �� ���������� ���� (��������� ��� - �� ���������� ������ � ��������� ��� ����-��� ��� ������ ������ � �������������. ���������� ��� ���������� �������������)
	���������� �� ��������� SpirV (OpenGL 4.6) - 4.x
	*/
	static ENGINE_INLINE bool _CP_LoadBinaryShader_CacheUtil(const char *_path, GLint& _size, std::vector<unsigned char>& _to, GLShader* _shader)
	{
		if (loadBinary(_CP_CreateShaderCacheDirectoryUtil(_path), _to, _size))
		{
			DebugM("loading shader binary,for %s, is successfully", _path);

			GLint formats = 0;
			CHECK_GL_ERROR();
			glGetIntegerv(GL_NUM_PROGRAM_BINARY_FORMATS, &formats);
			CHECK_GL_ERROR();
			GLint binaryFormats = 0;
			glGetIntegerv(GL_PROGRAM_BINARY_FORMATS, &binaryFormats);
			CHECK_GL_ERROR();

			auto _program = glCreateProgram();

			_shader->SetProgramID(_program);

			CHECK_GL_ERROR();
			glProgramBinary(_program, binaryFormats, &_to[0], _size);
			CHECK_GL_ERROR();

			_to.clear();

			return _shader->CheckLinked(_program, _shader->GetPipelineNameID(), _path, true);
		}

		return false;
	}

	/*TODO:Bugfix: is very shit code*/
	static ENGINE_INLINE bool _CP_ParseSFMemoryUtil(String sCode, const char* sDirectory, std::vector<String> & vResult)
	{
		try
		{
			auto pos = sCode.find("#include");
			if (pos != String::npos)
			{
				size_t start = sCode.find('\"', pos) + 1;
				size_t end = sCode.find('\"', start);

				String incfile(sCode.substr(start, end - start));

				VFileStream file(incfile, FILE_MODE::READ_TEXT, false);

				if (!file.IsValid())
					return false;

				String incsource;
				incsource = file.LoadFile();

				if (!incsource.empty())
				{
					sCode.replace(pos, end - pos + 1, incsource);
					/*DebugM(sCode.c_str());*/

					return _CP_ParseSFMemoryUtil(sCode, sDirectory, vResult);
				}
				sCode.replace(pos, end - pos + 1, "");
			}
			vResult.push_back(sCode);

#ifdef HEAVY_DEBUG
			DebugM(sCode.c_str());
#endif
		}
		catch (std::exception&e)
		{
			Warning(e.what());
		}

		return true;
	}

	static ENGINE_INLINE bool _CP_CheckCompileUtil(unsigned int _shaderId, const char*_path, GLShader&_shader)
	{
		int compiled = 0;
		CHECK_GL_ERROR();
		glGetShaderiv(_shaderId, GL_COMPILE_STATUS, &compiled);
		CHECK_GL_ERROR();

		if (compiled == GL_FALSE)
		{
			char sInfoLog[1024];
			char sFinalMessage[1536];
			int iLogLength;
			CHECK_GL_ERROR();
			glGetShaderInfoLog(_shaderId, 1024, &iLogLength, sInfoLog);
			CHECK_GL_ERROR();
			sprintf(sFinalMessage, "Error! Shader file %s wasn't compiled! The compiler returned: %s ", _path, sInfoLog);

			Error(sFinalMessage, false);

			CHECK_GL_ERROR();
			glDeleteShader(_shaderId);
			CHECK_GL_ERROR();

			_shader.SetLoaded(false);
			return false;
		}

		return true;
	}

	static ENGINE_INLINE bool CP_LoadShaderFromMemory_CodeUtil(const std::vector<String> &_sLines, String ShaderPath, int gl_shaderTypeID, GLuint &_out, GLShader&_shader)
	{
		const char** sProgram = new const char*[_sLines.size()];

		for (auto i = 0; i < _sLines.size(); i++)
		{
			sProgram[i] = _sLines[i].c_str();
		}

		CHECK_GL_ERROR();
		_out = glCreateShader(gl_shaderTypeID);
		CHECK_GL_ERROR();

		glShaderSource(_out, _sLines.size(), sProgram, NULL);
		CHECK_GL_ERROR();
		glCompileShader(_out);
		CHECK_GL_ERROR();

		SAFE_DELETE_ARRAY(sProgram);
		_shader.SetLoaded(true);

		return _CP_CheckCompileUtil(_out, ShaderPath.c_str(), _shader);
	}

	static ENGINE_INLINE void CP_CreateProgram(const char* _path, GLuint _newPipelineName, GLShader& _shader)
	{
		//create
		CHECK_GL_ERROR();
		auto newProgram = glCreateProgram();
		CHECK_GL_ERROR();
		if (newProgram == 0)
		{
			ASSERT(false, "glCreateProgram==0");
			return;
		}

		CHECK_GL_ERROR();
		glProgramParameteri(newProgram, GL_PROGRAM_SEPARABLE, GL_TRUE);
		CHECK_GL_ERROR();
#define ATTACH_ACTION(x) if (x) glAttachShader(newProgram, x);
		ATTACH_ACTION(_shader.GetShader_VS_ID());
		ATTACH_ACTION(_shader.GetShader_FS_ID());
		ATTACH_ACTION(_shader.GetShader_GS_ID());
		ATTACH_ACTION(_shader.GetShader_CS_ID());
		ATTACH_ACTION(_shader.GetShader_TES_ID());
		ATTACH_ACTION(_shader.GetShader_TCS_ID());
#undef ATTACH_ACTION

		if (_shader.CheckLinkedAndBind(newProgram, _newPipelineName, _path, true) == false)
		{
			Error("Failed Link and Bind", false);
			return;
		}

		// Set New Value as active
		_shader.SetPipelineNameID(_newPipelineName);
		_shader.SetProgramID(newProgram);
	}

	/*
	������ ���� ������������ ������� �� ��� ������� � ������������� ��.
	*/
	static ENGINE_INLINE void CP_ParseShaderFileFinal(const CVARManager& _cvars, const String &_path, const String &_shaderSpecificDefs, const String &_sceneSpecificDefs, bool _allowShaderCacheForThis, GLuint _newPipelineName, std::shared_ptr<ShaderContainer>& _toContainer, GLShader& _shader)
	{
		const bool r_ShadersAllowCacheGlobal = _cvars.r_ShadersAllowCache;

		if (_allowShaderCacheForThis && r_ShadersAllowCacheGlobal)
			LogPrintf("Shader Cache for shader: %s is not valid, or not found", _path.c_str());

		VFileStream mFile(_path, FILE_MODE::READ_TEXT, false, true);
		if (!mFile.IsValid())
		{
			Error("[GLShader::_ParseShaderFileUtil] Invalid file, check him,or check FS system", true);
			return;
		}

		String line;
		line.clear();

		std::shared_ptr<ShaderContainer> m_newContainer(new ShaderContainer());
		_toContainer.swap(m_newContainer);

		// Parsing Shader file
		{
			// �������� ����������� ��������
			while (!mFile.IsEof())
			{
				line = mFile.GetLine();

				_shader.AddDefines(_shaderSpecificDefs + _sceneSpecificDefs);

				//find GLSL vertex shader
				if (line == "[GLSL_VERTEX_SHADER]")
				{
#ifdef _ENGINE_DEBUG_
					DebugM("[GLSL_VERTEX_SHADER] %s", _path.c_str());
#endif
					while (!mFile.IsEof()) {
						line = mFile.GetLine();
						if (line == "[GLSL_FRAGMENT_SHADER]") break;
						else if (line == "[GLSL_COMPUTE_SHADER]") break;
						else if (line == "[GLSL_TESSELATION_CONTROL_SHADER]") break;
						else if (line == "[GLSL_TESSELATION_EVAL_SHADER]") break;
						else if (line == "[GLSL_GEOMETRY_SHADER]") break;

						_CP_ParseSFMemoryUtil(line + " \n", Sys_GetSubDirectory(_path).c_str(), _toContainer->GetVResult());
					}

					GLuint tmpID = -1;
					CP_LoadShaderFromMemory_CodeUtil(_toContainer->GetVResult(), _path, GL_VERTEX_SHADER, tmpID, _shader);

					_shader.SetShader_VS_ID(tmpID);
				}

				//find GLSL fragment shader
				if (line == "[GLSL_FRAGMENT_SHADER]")
				{
#ifdef _ENGINE_DEBUG_
					DebugM("[GLSL_FRAGMENT_SHADER] %s", _path.c_str());
#endif
					while (!mFile.IsEof()) {
						line = mFile.GetLine();
						if (line == "[GLSL_VERTEX_SHADER]") break;
						else if (line == "[GLSL_COMPUTE_SHADER]") break;
						else if (line == "[GLSL_TESSELATION_CONTROL_SHADER]") break;
						else if (line == "[GLSL_TESSELATION_EVAL_SHADER]") break;
						else if (line == "[GLSL_GEOMETRY_SHADER]") break;

						_CP_ParseSFMemoryUtil(line + " \n", Sys_GetSubDirectory(_path).c_str(), _toContainer->GetFResult());
					}

					GLuint tmpID = -1;
					CP_LoadShaderFromMemory_CodeUtil(_toContainer->GetFResult(), _path, GL_FRAGMENT_SHADER, tmpID, _shader);

					_shader.SetShader_FS_ID(tmpID);
				}

				//find GLSL GEOMETRY SHADER
				if (line == "[GLSL_GEOMETRY_SHADER]")
				{
#ifdef _ENGINE_DEBUG_
					DebugM("[GLSL_GEOMETRY_SHADER] %s", _path.c_str());
#endif
					while (!mFile.IsEof()) {
						line = mFile.GetLine();
						if (line == "[GLSL_FRAGMENT_SHADER]") break;
						else if (line == "[GLSL_COMPUTE_SHADER]") break;
						else if (line == "[GLSL_TESSELATION_CONTROL_SHADER]") break;
						else if (line == "[GLSL_TESSELATION_EVAL_SHADER]") break;
						else if (line == "[GLSL_VERTEX_SHADER]") break;

						_CP_ParseSFMemoryUtil(line + " \n", Sys_GetSubDirectory(_path).c_str(), _toContainer->GetGResult());
					}

					GLuint tmpID = -1;
					CP_LoadShaderFromMemory_CodeUtil(_toContainer->GetGResult(), _path, GL_GEOMETRY_SHADER, tmpID, _shader);

					_shader.SetShader_GS_ID(tmpID);
				}

				//find GLSL TESSELATION EVAL SHADER
				if (line == "[GLSL_TESSELATION_EVAL_SHADER]")
				{
#ifdef _ENGINE_DEBUG_
					DebugM("[GLSL_TESSELATION_EVAL_SHADER] %s", _path.c_str());
#endif
					while (!mFile.IsEof()) {
						line = mFile.GetLine();
						if (line == "[GLSL_FRAGMENT_SHADER]") break;
						else if (line == "[GLSL_COMPUTE_SHADER]") break;
						else if (line == "[GLSL_TESSELATION_CONTROL_SHADER]") break;
						else if (line == "[GLSL_VERTEX_SHADER]") break;
						else if (line == "[GLSL_GEOMETRY_SHADER]") break;

						_CP_ParseSFMemoryUtil(line + " \n", Sys_GetSubDirectory(_path).c_str(), _toContainer->GetTesResult());
					}

					GLuint tmpID = -1;
					CP_LoadShaderFromMemory_CodeUtil(_toContainer->GetTesResult(), _path, GL_TESS_EVALUATION_SHADER, tmpID, _shader);

					_shader.SetShader_TES_ID(tmpID);
				}

				//find GLSL TESSELATION CONTROL SHADER
				if (line == "[GLSL_TESSELATION_CONTROL_SHADER]")
				{
#ifdef _ENGINE_DEBUG_
					DebugM("[GLSL_TESSELATION_CONTROL_SHADER] %s", _path.c_str());
#endif
					while (!mFile.IsEof()) {
						line = mFile.GetLine();
						if (line == "[GLSL_FRAGMENT_SHADER]") break;
						else if (line == "[GLSL_COMPUTE_SHADER]") break;
						else if (line == "[GLSL_TESSELATION_EVAL_SHADER]") break;
						else if (line == "[GLSL_VERTEX_SHADER]") break;
						else if (line == "[GLSL_GEOMETRY_SHADER]") break;
						Warning("[GLSL_TESSELATION_CONTROL_SHADER]");

						_CP_ParseSFMemoryUtil(line + " \n", Sys_GetSubDirectory(_path).c_str(), _toContainer->GetTcsResult());
					}

					GLuint tmpID = -1;
					CP_LoadShaderFromMemory_CodeUtil(_toContainer->GetTcsResult(), _path, GL_TESS_CONTROL_SHADER, tmpID, _shader);

					_shader.SetShader_TCS_ID(tmpID);
				}

				//find GLSL COMPUTE SHADER
				if (line == "[GLSL_COMPUTE_SHADER]")
				{
#ifdef _ENGINE_DEBUG_
					DebugM("[GLSL_COMPUTE_SHADER] %s", _path.c_str());
#endif
					while (!mFile.IsEof()) {
						line = mFile.GetLine();
						if (line == "[GLSL_FRAGMENT_SHADER]") break;
						else if (line == "[GLSL_TESSELATION_CONTROL_SHADER]") break;
						else if (line == "[GLSL_TESSELATION_EVAL_SHADER]") break;
						else if (line == "[GLSL_VERTEX_SHADER]") break;
						else if (line == "[GLSL_GEOMETRY_SHADER]") break;

						_CP_ParseSFMemoryUtil(line + " \n", Sys_GetSubDirectory(_path).c_str(), _toContainer->GetCsResult());
					}

					GLuint tmpID = -1;
					CP_LoadShaderFromMemory_CodeUtil(_toContainer->GetCsResult(), _path, GL_COMPUTE_SHADER, tmpID, _shader);

					_shader.SetShader_CS_ID(tmpID);
				}
			}
		}

		CP_CreateProgram(_path.c_str(), _newPipelineName, _shader);

		if (_allowShaderCacheForThis && r_ShadersAllowCacheGlobal)
		{
			DebugM("[GLShader::_ParseShaderFileUtil] Save shader cache for %s, %i, %i", _shader.GetPath().c_str(), _allowShaderCacheForThis, r_ShadersAllowCacheGlobal);
			if (_shader.SaveToShaderCache(_path.c_str()) == false)
			{
				Warning("Failed Saving Compiled cache: %s", _path.c_str());
			}
		}

		// cleaning
#define DELETE_ACTION(x) if (x) 	{	CHECK_GL_ERROR(); glDeleteShader(x); CHECK_GL_ERROR();  }

		//DELETE_ACTION(_shader.GetShader_VS_ID());
		//DELETE_ACTION(_shader.GetShader_FS_ID());
		//DELETE_ACTION(_shader.GetShader_CS_ID());
		//DELETE_ACTION(_shader.GetShader_TCS_ID());
		//DELETE_ACTION(_shader.GetShader_TES_ID());

#undef DELETE_ACTION
	}
}