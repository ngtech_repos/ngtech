#include "RenderPrivate.h"

#include "VersionLayer.h"

namespace NGTech {
	static ENGINE_INLINE unsigned int ConvertOQModeFromEngineToOpenGL(OcclusionQueryMode oqMode)
	{
		static const unsigned int avaliblemodes[] = { GL_ANY_SAMPLES_PASSED, GL_ANY_SAMPLES_PASSED_CONSERVATIVE };
		auto selectedMode = static_cast<unsigned int>(oqMode);
		return avaliblemodes[selectedMode];
	}

	static ENGINE_INLINE void _AllocateOGLBufferDefault(unsigned int glID, size_t size, const void * _data, unsigned int _drawType, unsigned int _type)/*GL_ARRAY_BUFFER*/
	{
#ifdef RENDER_DEV
		Debug("_AllocateOGLBufferDefault Default way");
#endif
		{
			// BIND
			{
				CHECK_GL_ERROR();
				glBindBuffer(_type, glID);
				CHECK_GL_ERROR();
			}
			{
				CHECK_GL_ERROR();
				glBufferData(_type, size, _data, _drawType);
				CHECK_GL_ERROR();
			}
			// UNBIND
			{
				CHECK_GL_ERROR();
				glBindBuffer(_type, 0);
				CHECK_GL_ERROR();
			}
		}
	}

	static ENGINE_INLINE void _AllocateOGLBufferOpenGL45AndLater(unsigned int glID, size_t size, const void * _data, unsigned int _drawType, unsigned int _type) {
		// THIS function is universal
		if (glNamedBufferDataEXT)
		{
#ifdef RENDER_DEV
			Debug("OpenGL 4.5 way _AllocateOGLBufferOpenGL45AndLater");
#endif

			CHECK_GL_ERROR();
			glNamedBufferDataEXT(glID, size, _data, _drawType);
			CHECK_GL_ERROR();
		}
		else
		{
#ifdef RENDER_DEV
			Debug("_AllocateOGLBufferOpenGL45AndLater Default way");
#endif

			_AllocateOGLBufferDefault(glID, size, _data, _drawType, _type);
		}
	}

	static ENGINE_INLINE void _FillBufferDefaultWay(unsigned int glID, size_t offset, size_t size, void* data)
	{
		if (glBufferSubData)
		{
#ifdef RENDER_DEV
			Debug("[_FillBufferDefaultWay] Default way filling buffer");
#endif

			CHECK_GL_ERROR();
			glBufferSubData(glID, offset, size, data);
			CHECK_GL_ERROR();
		}
		else
		{
			ASSERT(glBufferSubData, "Not exist glBufferSubData");
		}
	}

	static ENGINE_INLINE void _FillBufferOpenGL45(unsigned int glID, size_t offset, size_t size, void* data)
	{
		if (glNamedBufferSubDataEXT)
		{
#ifdef RENDER_DEV
			Debug("OpenGL 4.5 way filling buffer");
#endif

			CHECK_GL_ERROR();
			glNamedBufferSubDataEXT(glID, offset, size, data);
			CHECK_GL_ERROR();
		}
		else
		{
#if NOT_ALLOW_NOT_SUPPORTED_FUNCS
			ASSERT(glNamedBufferSubDataEXT && "Not exist glNamedBufferSubDataEXT");
#endif
			_FillBufferDefaultWay(glID, offset, size, data);
		}
	}

	unsigned int OpenGLVersionBase::GetOQMode()
	{
		return ConvertOQModeFromEngineToOpenGL(this->_GetOcclusionQueryMode());
	}

	void OpenGLVersionBase::RunDrawElementsFunction(DrawMode _mode, void *_indices, unsigned int _indexCount,
		unsigned int _instance_count,
		unsigned int _base_instance)
	{
		static const GLuint drawmodes[] = { GL_TRIANGLES, GL_PATCHES, GL_LINES, GL_POINTS, 0 };
		auto cast = static_cast<unsigned int>(_mode);
		ASSERT(_mode != DrawMode::DM_COUNT, "Invalid DrawMode");

		switch (this->m_drawElementsMode)
		{
		case DrawElementsFunction::DrawElementsInstanced:
			glDrawElementsInstanced(drawmodes[cast], _indexCount, GL_UNSIGNED_INT, _indices, _instance_count);
			CHECK_GL_ERROR();
			break;

		case DrawElementsFunction::DrawElementsInstancedBaseInstance:
			//!@todo render: not impemented intancing
			//!@todo Instancing ������ �� ��������������, ������ ����� ���������� ������ instances, ������ � ���� ������ � ���������� ��������� id ��� base_instance
			//!@todo index_type-��������
			//glDrawElementsInstancedBaseInstance(GL_TRIANGLES, indexCount, index_type, indices, instance_count, base_instance);

			if (glDrawElementsInstancedBaseInstance)
			{
				glDrawElementsInstancedBaseInstance(drawmodes[cast], _indexCount, GL_UNSIGNED_INT, _indices, _instance_count, _base_instance);
				CHECK_GL_ERROR();
			}
			else
			{
				Debug("Function not found. Draw in compatible mode");
				this->m_drawElementsMode = DrawElementsFunction::DrawElementsInstanced;
				RunDrawElementsFunction(_mode, _indices, _indexCount, _instance_count, _base_instance);
			}

			break;

			//!@todo render: not impemented MDI
		//case DrawElementsFunction::MultiDrawArraysIndirect:
		//	//glMultiDrawArraysIndirect(GL_TRIANGLES, NULL, MultiDrawIndirect::NUM_DRAWS, 0);
		//	//!@todo render: not impemented MDI
		//	break;

		default:
			ASSERT("Invalid m_drawElementsMode in " && __FUNCTION__);
			break;
		}
	}

	void OpenGLVersionBase::RunDrawArraysFunction(int vertexCount, unsigned int instance_count, unsigned int base_instance, const GLint first)
	{
		switch (this->m_drawArraysMode)
		{
		case DrawArraysFunction::DrawArrays:
			glDrawArrays(GL_TRIANGLES, first, vertexCount);

			break;

		case DrawArraysFunction::DrawArraysInstanced:

			if (glDrawArraysInstanced)
				glDrawArraysInstanced(GL_TRIANGLES, first, vertexCount, instance_count);
			else
			{
				Debug("Function not found. Draw in compatible mode");
				this->m_drawArraysMode = DrawArraysFunction::DrawArrays;
				RunDrawArraysFunction(vertexCount, instance_count, base_instance, first);
			}

			break;

		case DrawArraysFunction::DrawArraysInstancedBaseInstance:
			if (glDrawArraysInstancedBaseInstance)
			{
				glDrawArraysInstancedBaseInstance(GL_TRIANGLES, first, vertexCount, instance_count, base_instance);
				CHECK_GL_ERROR();
			}
			else
			{
				Debug("Function not found. Draw in compatible mode");
				this->m_drawArraysMode = DrawArraysFunction::DrawArraysInstanced;
				RunDrawArraysFunction(vertexCount, instance_count, base_instance, first);
			}

			break;

		default:
			ASSERT("Invalid m_drawArraysMode in " && __FUNCTION__);
			break;
		}
	}

	void OpenGLVersionBase::AllocateBuffer(const void * _data, size_t size, unsigned int _drawType/*Already converted to OpenGL*/, unsigned int _bufferType/*Already converted to OpenGL*/, unsigned int glID)
	{
		if (m_majVersion >= 4 && m_minVersion >= 5) //-V112
			_AllocateOGLBufferOpenGL45AndLater(glID, size, _data, _drawType, _bufferType);
		else
			_AllocateOGLBufferDefault(glID, size, _data, _drawType, _bufferType);
	}

	void OpenGLVersionBase::FillBuffer(unsigned int glID, size_t offset, size_t size, void* data)
	{
		if (m_majVersion >= 4 && m_minVersion >= 5) //-V112
		{
			_FillBufferOpenGL45(glID, offset, size, data);
		}
		else
		{
			_FillBufferDefaultWay(glID, offset, size, data);
		}
	}

	bool OpenGLVersionBase::ValidateComputeShaderExtension()
	{
		if (m_SupportedComputeShader || HardwareRenderFeatureManager::IsExtSupported("GL_ARB_compute_shader"))
			return true;

		Warning("[OpenGLVersionBase::ValidateComputeShaderExtension()] Compute shaders is not supported :( ");

		return false;
	}
}