/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#ifndef MEMORY_ALLOCATOR_H
#define MEMORY_ALLOCATOR_H

#include "memoryallocators/MemoryAllocator.h"

namespace NGTech
{
	ATTRIBUTE_ALIGNED16(struct) ALIGNED_AT16
	{
		void *operator new (size_t size) { return SSEMemoryAllocatorUtil::Allocate(size); }
		void *operator new[](size_t size) { return SSEMemoryAllocatorUtil::Allocate(size); }

		void operator delete (void *p) { SSEMemoryAllocatorUtil::DeAllocate(p); }
		void operator delete(void *ptr, size_t) { SSEMemoryAllocatorUtil::DeAllocate(ptr); }
		void operator delete[](void *ptr, size_t) { SSEMemoryAllocatorUtil::DeAllocate(ptr); }
	};
}

#endif //MEMORY_ALLOCATOR_H