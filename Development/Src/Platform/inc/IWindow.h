/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/

#pragma once

//***************************************************
#include <string>
#include "I_Input.h"
//***************************************************

namespace NGTech
{
	struct WindowCallbacks {
		virtual ~WindowCallbacks() {}
		virtual void ResizeWindow(uint32_t _w, uint32_t _h) {}
		virtual void ScriptUpdate() {}
	};

	/**/
	struct I_Window :public RefCount
	{
		I_Window() {
			this->AddRef();
		}

		/**
		*/
		virtual ~I_Window() {}

		/**
		*/
		virtual void Initialise(void*_hwnd) { ASSERT(NULL, "FUNCTION MUST BE OVERLOADED"); }
		virtual void Release() { delete (I_Window *)this; }
		/**
		*/
		virtual bool CreateWindowImpl() {
			ASSERT(NULL, "FUNCTION MUST BE OVERLOADED");
			return false;
		}
		/**
		*/
		virtual void SetTitle(const String &title) {
			ASSERT(NULL, "FUNCTION MUST BE OVERLOADED");
		}
		/**
		*/
		virtual void Update() {
			ASSERT(NULL, "FUNCTION MUST BE OVERLOADED");
		}
		/**
		Gets screen width
		\return screen width
		*/
		ENGINE_INLINE virtual uint32_t GetWidth() { return m_Vars.width; }
		/**
		Gets screen height
		\return screen height
		*/
		ENGINE_INLINE virtual uint32_t GetHeight() { return m_Vars.height; }
		/**
		Checks if mouse was moved
		\return true if moved
		*/
		ENGINE_INLINE virtual bool IsMouseMoved() { return m_Input.mousing; }
		/**
		Gets mouse X coordinate
		\return X coordinate
		*/
		ENGINE_INLINE virtual int GetMouseX() { return m_Input.mouseX; }
		/**
		Gets mouse Y coordinate
		\return Y coordinate
		*/
		ENGINE_INLINE virtual int GetMouseY() { return m_Input.mouseY; }
		/**
		Gets mouse delta X coordinate
		\return delta X coordinate
		*/
		ENGINE_INLINE virtual int GetMouseDX() { return m_Input.mouseX - m_Input.oldMouseX; }
		/**
		Gets mouse delta Y coordinate
		\return delta Y coordinate
		*/
		ENGINE_INLINE virtual int GetMouseDY() { return m_Input.mouseY - m_Input.oldMouseY; }
		/**
		*/
		ENGINE_INLINE virtual void ToggleShowCursor() { ShowCursor(!m_Input.cursorVisible); }
		/**
		*/
		ENGINE_INLINE virtual bool IsCursorVisible() { return m_Input.cursorVisible; }
		/**
		*/
		ENGINE_INLINE virtual void ToggleGrabMouse() { m_Input.mouseGrabbed = !m_Input.mouseGrabbed; }
		/**
		*/
		ENGINE_INLINE virtual bool IsMouseGrabbed() { return m_Input.mouseGrabbed; }
		virtual void GrabMouse(bool _v) { m_Input.mouseGrabbed = _v; }
		/**
		*/
		ENGINE_INLINE virtual void SetMousePos(int x, int y) { ASSERT(NULL, "FUNCTION MUST BE OVERLOADED"); }
		/**
		*/
		ENGINE_INLINE virtual void ShowCursor(bool show) { ASSERT(NULL, "FUNCTION MUST BE OVERLOADED"); }
		/**
		*/
		virtual bool IsMouseButtonPressed(int mb) {
			ASSERT(NULL, "FUNCTION MUST BE OVERLOADED");
			return false;
		}
		/**
		*/
		virtual bool WasMouseButtonPressed(int mb) {
			ASSERT(NULL, "FUNCTION MUST BE OVERLOADED");
			return false;
		}
		/**
		*/
		virtual bool WasMouseButtonReleased(int mb) {
			ASSERT(NULL, "FUNCTION MUST BE OVERLOADED");
			return false;
		}

		/**
		*/
		virtual bool IsKeyPressed(const char* key) {
			ASSERT(NULL, "FUNCTION MUST BE OVERLOADED");
			return false;
		}

		virtual bool IsKeyDown(const char* key) {
			ASSERT(NULL, "FUNCTION MUST BE OVERLOADED");
			return false;
		}

		virtual bool IsKeyUp(const char* key) {
			ASSERT(NULL, "FUNCTION MUST BE OVERLOADED");
			return false;
		}

		/**
		*/
		virtual void SetKeyDown(int _value) {
			ASSERT(NULL, "FUNCTION MUST BE OVERLOADED");
		}

		virtual void SetKeyUp(int _value) {
			ASSERT(NULL, "FUNCTION MUST BE OVERLOADED");
		}

		/**
		*/
		virtual void ShowOSCursor(bool _value) {
			ASSERT(NULL, "FUNCTION MUST BE OVERLOADED");
		}

		/**
		*/
		virtual const int Input_GetKeyValueByChar(const char*  _p) {
			ASSERT(NULL, "FUNCTION MUST BE OVERLOADED");
			return 0;
		}
		/**
		*/
		virtual const char* Input_GetKeyValueByInt(int _p) {
			ASSERT(NULL, "FUNCTION MUST BE OVERLOADED");
			return NULL;
		}
		/**
		*/
		virtual void SwapBuffers() {
			ASSERT(NULL, "FUNCTION MUST BE OVERLOADED");
		}
		/**
		*/
		virtual void ManageVSync(bool _v) {
			ASSERT(NULL, "FUNCTION MUST BE OVERLOADED");
		}
		/**
		*/
		virtual bool IsWindowFocused() {
			ASSERT(NULL, "FUNCTION MUST BE OVERLOADED");
			return false;
		}

	public:
		I_Input			m_Input;
		WindowCallbacks m_Callback;
		void*			m_engine = nullptr;

		struct PlatformDependVars
		{
			uint32_t width = 0;
			uint32_t height = 0;
			//!@todo DEPRECATED-����� �� ������������, �� �� ������� ����� ���� ���������������� ����� - � ������ ����� �� ���������
			uint32_t frameNumber = 0;
			uint32_t pixelFormat = 0;

			int mx = 0;
			int my = 0;
			int alphaBits = 8;
			/*Default bits per pixel*/
			int bpp = 32; //-V112
			int zdepth = 0;

			bool isExternalHwnd = false;
			bool fullscreen = false;
			bool withoutBorder = false;

			void* hWnd = nullptr;
			void* hInstance = nullptr;
			// a handle to the device context for the Window
			void* hDC = nullptr;
			// handle to the rendering context.
			void* hRC = nullptr;
		};
		PlatformDependVars m_Vars;
	};
}