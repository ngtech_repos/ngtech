#pragma once

namespace NGTech
{
	/**/
	struct I_Input
	{
		I_Input()
			:mouseGrabbed(false),
			mousing(false),
			cursorVisible(false),
			mouseX(0),
			mouseY(0),
			oldMouseX(0),
			oldMouseY(0)
		{
			for (int i = 0; i < I_Input::MOUSE_NUMS; i++)
			{
				this->mouseButtons[i] = false;
				this->oldMouseButtons[i] = false;
			}

			for (int i = 0; i < I_Input::KEY_NUMS; i++)
			{
				this->keys[i] = false;
				this->oldKeys[i] = false;
			}
		}
		/**
		Mouse buttons enum
		*/
		enum MouseButton {
			MOUSE_LEFT_BUTTON = 0,
			MOUSE_RIGHT_BUTTON = 1,
			MOUSE_RIGHT_MIDLLE = 2,
			MOUSE_NUMS
		};

		/**
		Keyboard keys enum
		*/
		enum KeyButtons {
			KEY_ESC = /*GLFW_KEY_ESCAPE*/256,
			KEY_SPACE = /*GLFW_KEY_SPACE*/32,
			KEY_MINUS = /*GLFW_KEY_MINUS*/45,
			KEY_RETURN = /*GLFW_KEY_BACKSPACE*/259,
			KEY_LEFT_BRACKET = /*GLFW_KEY_LEFT_BRACKET*/91,
			KEY_RIGHT_BRACKET = /*GLFW_KEY_RIGHT_BRACKET*/93,
			KEY_EQUAL = /*GLFW_KEY_EQUAL*/61,
			KEY_BACKSLASH = /*GLFW_KEY_BACKSLASH*/92,
			KEY_SEMICOLON = /*GLFW_KEY_SEMICOLON*/59,
			KEY_APOSTROPHE = /*GLFW_KEY_APOSTROPHE*/39,
			KEY_GRAVE_ACCENT = /*GLFW_KEY_GRAVE_ACCENT*/96,

			KEY_UP = /*GLFW_KEY_UP*/265,
			KEY_DOWN = /*GLFW_KEY_DOWN*/264,
			KEY_LEFT = /*GLFW_KEY_LEFT*/263,
			KEY_RIGHT = /*GLFW_KEY_RIGHT*/262,

			KEY_0 = /*GLFW_KEY_KP_0*/320,
			KEY_1,
			KEY_2, KEY_3,
			KEY_4, KEY_5,
			KEY_6, KEY_7,
			KEY_8, KEY_9,

			KEY_A = 65,
			KEY_B,
			KEY_C, KEY_D,
			KEY_E, KEY_F,
			KEY_G, KEY_H,
			KEY_I, KEY_J,
			KEY_K, KEY_L,
			KEY_M, KEY_N,
			KEY_O, KEY_P,
			KEY_Q, KEY_R,
			KEY_S, KEY_T,
			KEY_U, KEY_V,
			KEY_W, KEY_X,
			KEY_Y, KEY_Z,

			KEY_F1 = /*GLFW_KEY_F1*/290,
			KEY_F2,
			KEY_F3, KEY_F4,
			KEY_F5, KEY_F6,
			KEY_F7, KEY_F8,
			KEY_F9, KEY_F10,
			KEY_F11, KEY_F12,
			KEY_F13, KEY_F14,
			KEY_F15, KEY_F16,
			KEY_F17, KEY_F18,
			KEY_F19, KEY_F20,
			KEY_F21, KEY_F22,
			KEY_F23, KEY_F24,
			KEY_F25,

			KEY_NUMS = 348
		};

		bool mouseButtons[MOUSE_NUMS];
		bool oldMouseButtons[MOUSE_NUMS];

		bool keys[KEY_NUMS];
		bool oldKeys[KEY_NUMS];

		int mouseX, mouseY, oldMouseX, oldMouseY;
		bool mousing, cursorVisible, mouseGrabbed;
	};
	}