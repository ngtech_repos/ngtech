/* Trick to allow multiple inheritance of objects
* inheriting shared_from_this.
* cf. https://stackoverflow.com/a/12793989/587407
* cf. https://stackoverflow.com/questions/16082785/use-of-enable-shared-from-this-with-multiple-inheritance
* Example:
class A: public inheritable_enable_shared_from_this<A>
{
public:
void foo1()
{
auto ptr = shared_from_this();
}
};

class B: public inheritable_enable_shared_from_this<B>
{
public:
void foo2()
{
auto ptr = shared_from_this();
}
};

class C: public inheritable_enable_shared_from_this<C>
{
public:
void foo3()
{
auto ptr = shared_from_this();
}
};

class D: public A, public B, public C
{
public:
void foo()
{
auto ptr = A::downcasted_shared_from_this<D>();
}
};
*/

#pragma once

#include <memory>

namespace NGTech
{
	/* First a common base class
	* of course, one should always virtually inherit from it.
	*/
	class MultipleInheritableEnableSharedFromThis : public std::enable_shared_from_this<MultipleInheritableEnableSharedFromThis>
	{
	public:
		virtual ~MultipleInheritableEnableSharedFromThis()
		{}
	};

	template <class T>
	class inheritable_enable_shared_from_this : virtual public MultipleInheritableEnableSharedFromThis
	{
	public:
		std::shared_ptr<T> shared_from_this() {
			return std::dynamic_pointer_cast<T>(MultipleInheritableEnableSharedFromThis::shared_from_this());
		}
		/* Utility method to easily downcast.
		* Useful when a child doesn't inherit directly from enable_shared_from_this
		* but wants to use the feature.
		*/
		template <class Down>
		std::shared_ptr<Down> downcasted_shared_from_this() {
			return std::dynamic_pointer_cast<Down>(MultipleInheritableEnableSharedFromThis::shared_from_this());
		}
	};
}