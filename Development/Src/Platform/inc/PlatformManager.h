/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

namespace NGTech
{
	/**
	*/
	class PlatformManager
	{
	public:
		PlatformManager();
		~PlatformManager();
		/**
		*/
		bool InitPlatformCode();

		void RunTask(const ThreadTask&, unsigned int _count);

		template<class T>
		void RunTask(T _task, unsigned int _count)
		{
			if (!scheduler) {
				PlatformError("Invalid scheduler pointer", __FUNCTION__, true);
				return;
			}

			scheduler->RunAsync(_task->GetGroup(), _task, _count);
		}

		template<class T>
		void WaitTask(T _task, unsigned int _count)
		{
			if (!scheduler) {
				PlatformError("Invalid scheduler pointer", __FUNCTION__, true);
				return;
			}

			scheduler->WaitGroup(_task->GetGroup(), _count);
		}

		void WaitTask(const ThreadTask&, unsigned int _time = 100);

		void ReleaseGroup(const ThreadTask&_task);

		void WaitAllTasks(unsigned long _time);

		void UpdatePlatform();

		void StopJobs();

		ENGINE_INLINE const std::shared_ptr<MT::TaskScheduler>& GetScheduler() const {
			return scheduler;
		}
	private:
		bool _InitScheduler();
	private:
		/**
		*/
		std::shared_ptr<MT::TaskScheduler> scheduler;
	};
}