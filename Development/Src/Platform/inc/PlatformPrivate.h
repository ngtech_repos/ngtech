/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

// Build Defines
#include "../../BuildDefines.h"

// Compiler Utils
#include "platformdetect.h"

#include "Types.h"
// System Utils
#include "SystemUtils.h"
#include "Timer.h"
// Headers
#include "STLHeaders.h"
// Profiler
#include "Profiler.h"
// Multithread
#include "Scheduler/MTScheduler.h"
#include "Scheduler/MTTaskGroup.h"
#include "Scheduler/ThreadTask.h"
// All will avalible there
#include "PlatformManager.h"
#include "STLUtils.h"
// DbgTools
#include "dbToolsDefines.h"
#include "whereami.h"
#include "dbgtools/debugger.h"
#include "dbgtools/assert.h"
#include "dbgtools/callstack.h"
#include "dbgtools/fpe_ctrl.h"
#include "dbgtools/static_assert.h"
#include "dbgtools/static_assert_counter.h"

#include "types/pointers.hpp"
// LogFunctions forward declarations
#include "LogFunctions.h"
// Window utils
#include "IWindow.h"