/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#ifndef PLATFORM_PROFILER_H
#define PLATFORM_PROFILER_H

#pragma once

#if (MT_INSTRUMENTED_BUILD) && (MT_ENABLE_BROFILER_SUPPORT)
#include "../../../Tools/Brofiler/BrofilerCore/Brofiler.h"

#define BROFILER_NEXT_FRAME() Brofiler::NextFrame();   \
                              BROFILER_EVENT("Frame") \

#else
#define PROFILE
#define BROFILER_INLINE_EVENT(NAME, CODE) { CODE; }
#define BROFILER_CATEGORY(NAME, COLOR)
#define BROFILER_FRAME(NAME)
#define BROFILER_THREAD(FRAME_NAME)
#define BROFILER_NEXT_FRAME()
#endif

namespace MT
{
	class IProfilerEventListener;
}

MT::IProfilerEventListener* GetProfiler();

#endif