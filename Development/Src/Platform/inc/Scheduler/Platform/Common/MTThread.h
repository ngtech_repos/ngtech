#pragma once

#define MT_CPUCORE_ANY (0xffffffff)

#include "MTAtomic.h"

namespace MT
{
	namespace ThreadPriority
	{
		enum Type
		{
			HIGH = 0,
			DEFAULT = 1,
			LOW = 2
		};
	}

	class ThreadBase
	{
	protected:
		void * funcData;
		TThreadEntryPoint func;
	public:

		MT_NOCOPYABLE(ThreadBase);

		ThreadBase()
			: funcData(nullptr)
			, func(nullptr)
		{
		}
	};
}
