/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include "AlignedBaseClasses.h"

namespace NGTech
{
	ATTRIBUTE_ALIGNED16(struct) ThreadTask:public ALIGNED_AT16
	{
		MT_DECLARE_TASK(ThreadTask, MT::StackRequirements::STANDARD, MT::TaskPriority::NORMAL, MT::Color::Blue);

		ThreadTask() {
			SetGroup(MT::TaskGroup::Default());
		}

		virtual ~ThreadTask() {}

		virtual void Do(MT::FiberContext& ctx)
		{
		}
		virtual void Wait(MT::FiberContext& ctx)
		{
		}

		ENGINE_INLINE const MT::TaskGroup &GetGroup() const {
			return group;
		}

		ENGINE_INLINE void SetGroup(MT::TaskGroup _group) {
			group = _group;
		}
	protected:
		MT::TaskGroup group;
	};
}