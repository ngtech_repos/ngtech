/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#ifndef STL_UTILS_H
#define STL_UTILS_H 1

#pragma once

// for inline and forceinline
#include "platformdetect.h"

// Needed for std::find
#include <algorithm>

// std::map for cmpMaps
#include <map>
// for format
#include <stdarg.h>
// for vector
#include <vector>

namespace NGTech
{
	//---------------------------------------------------------------------------
	//Desc: String
	//---------------------------------------------------------------------------
	typedef std::string String;
	typedef std::stringstream Stringstream;
	//---------------------------------------------------------------------------

	//===============================================
	// Existing element in stl::vector
	//===============================================
	template <typename T>
	static ENGINE_INLINE const bool Contains(std::vector<T>& Vec, const T& Element)
	{
		if (std::find(Vec.begin(), Vec.end(), Element) != Vec.end())
			return true;

		return false;
	}

	//===============================================
	// Compare maps
	// see https://stackoverflow.com/questions/8473009/how-to-efficiently-compare-two-maps-of-strings-in-c
	// see http://rsdn.org/forum/cpp/6518087.flat
	//===============================================

	template <typename Map>
	static ENGINE_INLINE bool key_compare(Map const &lhs, Map const &rhs) {
		return lhs.size() == rhs.size()
			&& std::equal(lhs.begin(), lhs.end(),
				rhs.begin()
				/*, Pair_First_Equal()*/
			); // predicate instance
	}

	// Without std::equal
	template <typename T>
	static ENGINE_INLINE bool _CompareMaps(const T& l, const T& r)
	{
		// same types, proceed to compare maps here

		if (l.size() != r.size())
			return false;  // differing sizes, they are not the same

		typename T::const_iterator i, j;
		for (i = l.begin(), j = r.begin(); i != l.end(); ++i, ++j)
		{
			if (*i != *j)
				return false;
		}

		return true;
	}

	template <typename Map>
	struct cmpMaps {
		template <typename Map>
		ENGINE_INLINE bool operator()(Map const &lhs, Map const &rhs) const {
			// No predicate needed because there is operator== for pairs already.
			return key_compare <Map>(lhs, rhs);
		}
	};

	//===============================================
	// Search in std::map
	// see https://stackoverflow.com/questions/1939953/how-to-find-if-a-given-key-exists-in-a-c-stdmap
	//===============================================
	template <typename T, typename Key>
	static ENGINE_INLINE bool key_exists(const T& container, const Key& key)
	{
		return (container.find(key) != std::end(container));
	}

	/*
	// And use it like this:

	std::map<int, int> some_map;
	find_and_execute(some_map, 1,
	[](int key, int value){ std::cout << "key " << key << " found, value: " << value << std::endl; },
	[](int key){ std::cout << "key " << key << " not found" << std::endl; });
	*/
	template <typename T, typename Key, typename FoundFunction, typename NotFoundFunction>
	static ENGINE_INLINE void find_and_execute(const T& container, const Key& key, FoundFunction found_function, NotFoundFunction not_found_function)
	{
		auto& it = container.find(key);
		if (it != std::end(container))
		{
			found_function(key, it->second);
		}
		else
		{
			not_found_function(key);
		}
	}

	//===============================================
	// �������� ������� �� ������, ��� ������ ����������� ����� ������ �������
	// Note: ������������ � SceneManager, ��� �������� ���� Light. ���������� � ����������� ������� ������� ��� �������
	// ��� ������ ������ ���� ��� ������ �����������, �.� ����� ����� ��������
	//===============================================
	template <class T, class P>
	static ENGINE_INLINE void _DeleteAllObjectsFromListWithoutCallingDesCtorForObj(T& _objL, P& _ptr)
	{
		if (!_ptr)
			return;

		auto it = std::find(_objL.begin(), _objL.end(), _ptr);
		if (it != _objL.end())
		{
			_objL.erase(it);
		}
	}

	//===============================================
	// ������� ��� �������������� ������
	// https://habrahabr.ru/post/318962/
	//===============================================
	String StringFormat(const char *fmt, ...);


	//===============================================
	// ������� ������� ���� ������ ���������� � ���������� �� �����
	// @todo ��� �� ������� �������� ��� ����� ���� �� ������������������. �������� �� binary_search
	//===============================================
	template <class T1, class T2>
	static ENGINE_INLINE T1 SearchFirstElementByNameInContainerUtil(T2 _cont, const String & _name) {
		for (const auto&it : _cont) {
			if (!it) continue;
			if (it->GetNameForChange() == _name) {
				return it;
			}
		}
		return nullptr;
	}
}
#endif