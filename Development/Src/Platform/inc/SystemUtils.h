/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

#include "StlUtils.h"

namespace NGTech
{
	const char *Sys_EXEPath();
	void PlatformError(const char* desc, const char* header = "NGTech error", bool fatal = false);
	void Sys_ReLaunch(void * data, const unsigned int dataSize);
	String Sys_GetSubDirectory(String sFile);
}