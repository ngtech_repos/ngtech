#pragma once

#if IS_OS_WINDOWS
//***************************************************
#include "IWindow.h"
//***************************************************************************
#include "String.h"
//***************************************************

namespace NGTech {
#ifndef DROP_EDITOR
	class CVARManager;
	//---------------------------------------------------------------------------
	//Desc: Engine`s main window and input system. Created one time
	//---------------------------------------------------------------------------
	struct WindowSystem :public I_Window {
		/**
		Mouse buttons enum
		*/
		enum MouseButton {
			MOUSE_LEFT_BUTTON = 0,
			MOUSE_RIGHT_BUTTON = 1,
			MOUSE_RIGHT_MIDLLE = 2//������� ���
		};
		/**
		Keyboard keys enum
		*/
		// see http://www.mods.com.au/budapi_docs/Virtual%20Key%20Codes.htm
		// and Platform\src\glfw\win32_window.c
		enum Key {
			KEY_ESC = VK_ESCAPE,
			KEY_SPACE = VK_SPACE,
			KEY_MINUS = VK_OEM_MINUS,
			KEY_RETURN = VK_BACK,
			KEY_LEFT_BRACKET = 0x1A,
			KEY_RIGHT_BRACKET = 0x1B,
			KEY_EQUAL = 0x0D,
			KEY_BACKSLASH = 0xDC/*VK_BACKSLASH*/,
			KEY_SEMICOLON = 0x27/*VK_SEMICOLON*/,
			KEY_APOSTROPHE = 0x28/*VK_APOSTROPHE*/,
			KEY_GRAVE_ACCENT = 0x29,

			KEY_UP = VK_UP,
			KEY_DOWN = VK_DOWN,
			KEY_LEFT = VK_LEFT,
			KEY_RIGHT = VK_RIGHT,

			KEY_0 = VK_NUMPAD0,
			KEY_1,
			KEY_2, KEY_3,
			KEY_4, KEY_5,
			KEY_6, KEY_7,
			KEY_8, KEY_9,

			KEY_A = 65,
			KEY_B,
			KEY_C, KEY_D,
			KEY_E, KEY_F,
			KEY_G, KEY_H,
			KEY_I, KEY_J,
			KEY_K, KEY_L,
			KEY_M, KEY_N,
			KEY_O, KEY_P,
			KEY_Q, KEY_R,
			KEY_S, KEY_T,
			KEY_U, KEY_V,
			KEY_W, KEY_X,
			KEY_Y, KEY_Z,

			KEY_F1 = VK_F1, KEY_F2,
			KEY_F3, KEY_F4,
			KEY_F5, KEY_F6,
			KEY_F7, KEY_F8,
			KEY_F9, KEY_F10,
			KEY_F11, KEY_F12,
			KEY_F13, KEY_F14,
			KEY_F15, KEY_F16,
			KEY_F17, KEY_F18,
			KEY_F19, KEY_F20,
			KEY_F21, KEY_F22,
			KEY_F23, KEY_F24,
			KEY_F25, KEY_F26
		};
	public:
		/**
		*/
		explicit WindowSystem(void*_m_engine, const PlatformDependVars&_vars, const WindowCallbacks& _callback);
		/**
		*/
		virtual ~WindowSystem();
		/**
		*/
		virtual void Initialise(void*_hwnd) override;

		virtual bool CreateWindowImpl() override { return true; }
		/**
		*/
		virtual void SetTitle(const String &title) override;
		/**
		*/
		virtual void Update() override;
		/**
		*/
		virtual void SetMousePos(int x, int y) override;
		virtual void GrabMouse(bool grab) override;
		/**
		*/
		virtual void ShowCursor(bool show) override;
		/**
		*/
		virtual bool IsMouseButtonPressed(int mb) override;
		/**
		*/
		virtual bool WasMouseButtonPressed(int mb) override;
		/**
		*/
		virtual bool WasMouseButtonReleased(int mb) override;
		/**
		*/
		virtual bool IsKeyPressed(const char* key) override;
		/**
		*/
		virtual bool IsKeyDown(const char* key) override;
		/**
		*/
		virtual bool IsKeyUp(const char* key) override;
		/**
		*/
		virtual void ShowOSCursor(bool _value) override;
		/**
		*/
		virtual const int Input_GetKeyValueByChar(const char*  _p) override;
		/**
		*/
		virtual const char* Input_GetKeyValueByInt(int _p) override;

		/**
		*/
		virtual void ManageVSync(bool _v) override;

		/**
		*/
		virtual void SetKeyDown(int _value) override;

		/**
		*/
		virtual void SetKeyUp(int _value) override;
	};
#endif
}
#endif