/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#pragma once

//***************************************************
#include "../../../Externals/glfw/include/GLFW/glfw3.h"
//***************************************************
#include "IWindow.h"
//***************************************************

namespace NGTech {
	//class ScriptUpdateJob;

	/**
	Engine`s main window and input system. Created one time
	*/
	class WindowSystemGLFW :public I_Window
	{
	public:
		/**
		*/
		WindowSystemGLFW(void*_m_engine, const PlatformDependVars&_vars, const WindowCallbacks& _callback);

		/**
		Creates new WindowSystem
		\param width screen width
		\param height screen height
		\param bpp screen bpp
		\param zDepth ZBuffer depth
		\param fullscreen flag
		\param windowTitle window title string
		*/
		virtual void Initialise(void*) override;

		virtual bool CreateWindowImpl() override;

		/**
		Destroys WindowSystem
		*/
		virtual ~WindowSystemGLFW();

		/**
		Sets window title
		\param title title text
		*/
		virtual void SetTitle(const String &title) override;

		/**
		Swaps app`s back and front buffers
		*/
		virtual void SwapBuffers() override;

		/**
		Updates WindowSystem and processes events
		*/
		virtual void Update() override;

		/**
		Sets mouse position
		\param x mouse x
		\param y mouse y
		*/
		virtual void SetMousePos(int x, int y) override;

		/**
		Show/hide cursor
		\param show show cursor if true
		*/
		virtual void ShowCursor(bool show) override;

		/**
		Toggle show/hide cursor
		*/
		ENGINE_INLINE virtual void ToggleShowCursor() override { ShowCursor(!m_Input.cursorVisible); }

		/**
		Checks if cursor is visible
		\return true if visible
		*/
		ENGINE_INLINE bool isCursorVisible() { return m_Input.cursorVisible; }

		/**
		Grab/release cursor
		\param grab grab cursor if true
		*/
		virtual void GrabMouse(bool grab) override;

		/**
		Toggle grab/release cursor
		*/
		ENGINE_INLINE virtual void ToggleGrabMouse() override { GrabMouse(!m_Input.mouseGrabbed); }

		/**
		Checks if cursor is grabbed
		\return true if grabbed
		*/
		ENGINE_INLINE bool isMouseGrabbed() { return m_Input.mouseGrabbed; }

		/**
		Checks if the mouse button is pressed
		\param mb mouse button id
		\return true if pressed
		*/
		virtual bool IsMouseButtonPressed(int mb) override;

		/**
		Checks if the mouse button was pressed in previous frame and now it is released
		\param mb mouse button id
		\return true if was pressed
		*/
		virtual bool WasMouseButtonPressed(int mb) override;

		/**
		Checks if the mouse button was released in previous frame and now it is pressed
		\param mb mouse button id
		\return true if was pressed
		*/
		virtual bool WasMouseButtonReleased(int mb) override;

		/**
		Checks if the key is pressed
		\param key key id
		\return true if pressed
		*/
		virtual bool IsKeyPressed(const char*  key) override;

		/**
		Checks if the key was pressed in previous frame and now it is released
		\param key key id
		\return true if was pressed
		*/
		virtual bool IsKeyDown(const char*  key) override;

		/**
		Checks if the key was released in previous frame and now it is pressed
		\param key key id
		\return true if was released
		*/
		virtual bool IsKeyUp(const char*  key) override;

		/**
		*/
		ENGINE_INLINE virtual void SetKeyDown(int _value) override {
			m_Input.keys[_value] = true;
		}

		/**
		*/
		ENGINE_INLINE virtual void SetKeyUp(int _value) override {
			m_Input.keys[_value] = false;
		}

		/**
		*/
		virtual const int Input_GetKeyValueByChar(const char*  _p) override;

		/**
		*/
		virtual const char* Input_GetKeyValueByInt(int _p) override;

		/**
		*/
		virtual void ShowOSCursor(bool _value) override;

		/**
		*/
		virtual void ManageVSync(bool _v) override;

		/**
		*/
		virtual bool IsWindowFocused() override;
	private:
		void _CreateContextAndWindow();
		bool _CreateContext(int major, int minor, bool specific);
		void _CleanResources();

		/**
		Callbacks
		*/
		void setInputCallbacksGLFW(GLFWwindow *window);
		static void window_size_callback(GLFWwindow* window, int width, int height);
		static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
		static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
		static void cursor_position_callback(GLFWwindow* window, double mx, double my);
	private:
		GLFWwindow * window = nullptr;

		// Scriptable
		/*std::shared_ptr<ScriptUpdateJob> m_updateJob;*/
	};
}