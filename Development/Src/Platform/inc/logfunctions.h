//***************************************************************************
// Moved in different header for using with Managed code
//***************************************************************************
#pragma once

namespace NGTech {
#ifdef _ENGINE_DEBUG_
#define Debug(text) DebugF(text, __FILE__, __LINE__)
#else
#define Debug(text)
#endif
	
// Realisation will in core.dll
#ifndef CORE_EXPORTS
#define CORE_API CROSSPLATFORM_IMPORT_FROM_DLL
#else
#define CORE_API CROSSPLATFORM_EXPORT_FROM_DLL
#endif

	void CORE_API DebugF(const String& text, const char* _file, int _line);
	void CORE_API Warning(const char *fmt, ...);
	void CORE_API DebugM(const char *fmt, ...);
	void CORE_API LogPrintf(const char *fmt, ...);
	void CORE_API ErrorWrite(const char *fmt, ...);
	void CORE_API Error(const String& text, bool _fatal);
};