/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#ifndef MACRO_STRINGS_H
#define MACRO_STRINGS_H

// MACRO_STRING_CONCAT macro
#if defined(MACRO_STRING_CONCAT) || defined(MACRO_STRING_CONCAT_HELPER)
#	error please do not define MACRO_STRING_CONCAT or MACRO_STRING_CONCAT_HELPER macros
#endif // #if defined(MACRO_STRING_CONCAT) || defined(MACRO_STRING_CONCAT_HELPER)

#define STRING_CONCAT_HELPER(a,b)	a##b
#define STRING_CONCAT(a,b)			STRING_CONCAT_HELPER(a,b)

// MACRO__MAKE_STRING macro
#if defined(MACRO_MAKE_STRING) || defined(MACRO_MAKE_STRING_HELPER)
#	error please do not define MACRO_MAKE_STRING or MACRO_MAKE_STRING_HELPER macros
#endif // #if defined(MAKE_STRING) || defined(MAKE_STRING_HELPER)

#define MACRO_MAKE_STRING_HELPER(a)		#a
#define MACRO_MAKE_STRING(a)			MACRO_MAKE_STRING_HELPER(a)

#if defined(IS_OS_WINDOWS) && !defined(DROP_EDITOR)

/// Use to set class name, as below
#define _CLASS_NAME_BASE(T) public: ENGINE_INLINE virtual const char* ClassName(){ return #T; }
#define _CLASS_NAME_OVERRIDE(T) public: ENGINE_INLINE virtual const char* ClassName() override { return #T; }

/// Use to set class name, as below
#define _CLASS_NAME_REFLECTION_BASE(T) public: static ENGINE_INLINE const char* ClassName_Reflection(){ return #T; }
#define _CLASS_CREATOR_REFLECTION_BASE(T) public: static ENGINE_INLINE T* Creator() { auto ptr = new T(); return ptr; }

#define _CLASS_CHECKWHATWASMODIFICATED_BASE(T) public: virtual void EditorCheckWhatWasModificated() {}
#define _CLASS_CHECKWHATWASMODIFICATED_OVERRIDE(T) public: virtual void EditorCheckWhatWasModificated() override;

#define _CLASS_INIT_EDITORVARS_BASE(T) virtual void _InitEditorVars();
#define _CLASS_INIT_EDITORVARS_OVERRIDE(T) virtual void _InitEditorVars() override;

#define CLASS_INIT_EDTOR_VARS() _InitEditorVars()
#define CLASS_REINIT_EDITOR_VARS() this->ReInitEditorVars()

#define _CLASS_REINIT_EDITOR_VARS_BASE(T) virtual void ReInitEditorVars() {}
#define _CLASS_REINIT_EDITOR_VARS_OVERRIDE(T) virtual void ReInitEditorVars() override;

#define _CLASS_DO_REFLECTABLE_BASE(T) _CLASS_NAME_BASE(T) _CLASS_NAME_REFLECTION_BASE(T) _CLASS_CREATOR_REFLECTION_BASE(T)
#define _CLASS_DO_REFLECTABLE_OVERRIDE(T) _CLASS_NAME_OVERRIDE(T) _CLASS_NAME_REFLECTION_BASE(T) _CLASS_CREATOR_REFLECTION_BASE(T)

#define _CLASS_ADD_EDITOR_VARS_BASE(T)
#define _CLASS_ADD_EDITOR_VARS_OVERRIDE(T) virtual void _AddEditorVars() override;

#define _CLASS_ADD_EDITOR_VARS_TO_BASE(T)
#define _CLASS_ADD_EDITOR_VARS_TO_OVERRIDE(T) virtual void AddEditorVarsTo(std::vector<EditorVar>&_to) override;

#define CLASS_AVALIBLE_IN_EDITOR_BASE(T) _CLASS_CHECKWHATWASMODIFICATED_BASE(T) _CLASS_INIT_EDITORVARS_BASE(T) _CLASS_DO_REFLECTABLE_BASE(T) _CLASS_ADD_EDITOR_VARS_BASE(T) _CLASS_ADD_EDITOR_VARS_TO_BASE(T) _CLASS_REINIT_EDITOR_VARS_BASE(T)
#define CLASS_AVALIBLE_IN_EDITOR(T) _CLASS_CHECKWHATWASMODIFICATED_OVERRIDE(T) _CLASS_INIT_EDITORVARS_OVERRIDE(T) _CLASS_DO_REFLECTABLE_OVERRIDE(T) _CLASS_ADD_EDITOR_VARS_OVERRIDE(T) _CLASS_ADD_EDITOR_VARS_TO_OVERRIDE(T) _CLASS_REINIT_EDITOR_VARS_OVERRIDE(T)

#else

#define CLASS_AVALIBLE_IN_EDITOR_BASE(T)
#define CLASS_AVALIBLE_IN_EDITOR(T)
#define CLASS_INIT_EDTOR_VARS()
#define CLASS_REINIT_EDITOR_VARS()

#endif

#endif // #ifndef MACRO_STRINGS_H