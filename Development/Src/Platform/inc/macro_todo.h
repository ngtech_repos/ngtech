/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#ifndef MACRO_TODO_H
#define MACRO_TODO_H

#if defined(TODO) || defined(_TODO_HELPER) || defined(TODO_HELPER)
#	error do not define TODO _TODO_HELPER TODO_HELPER macro
#endif // #ifdef TODO

#define _TODO_HELPER(message_to_show) #message_to_show
#define TODO_HELPER(message_to_show) _TODO_HELPER(message_to_show)

#if PLATFORM_COMPILER == PLATFORM_COMPILER_MSVC

#define TODO(message_to_show) __pragma(message("TODO: "_TODO_HELPER(message_to_show) " :: " __FILE__ "@"TODO_HELPER(__LINE__)))
#else
#define DO_PRAGMA(x) _Pragma (#x)

#define TODO(message_to_show)
#endif

#if defined(GCC_VERSION) && ( GCC_VERSION < 4 )
// to disable unknown pragma warning message
#	pragma warning( disable : ??? )
#endif // #if defined(GCC_VERSION) && ( GCC_VERSION < 4.5 )

#endif // #ifndef MACRO_TODO_H