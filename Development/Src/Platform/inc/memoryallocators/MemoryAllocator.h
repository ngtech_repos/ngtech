#ifndef CALLOCATOR_H
#define CALLOCATOR_H

#include "BaseAllocator.h"

namespace NGTech
{
	using namespace allocator;

	struct SSEMemoryAllocatorUtil
	{
		// SSE - 16
		static void* Allocate(size_t size, const std::size_t alignment = 16);
		static void DeAllocate(void* _ptr);
	};

	class MemoryAllocator : public allocator::BaseAllocator {
	public:
		MemoryAllocator();

		virtual ~MemoryAllocator();

		virtual void* Allocate(const std::size_t size, const std::size_t alignment = 0) override;

		virtual void Free(void* ptr) override;

		virtual void Init() override;
	};

	class SSEMemoryAllocator : public MemoryAllocator
	{
		SSEMemoryAllocator() : MemoryAllocator()
		{}

		virtual ~SSEMemoryAllocator()
		{}

		// For SSE - 16
		virtual void* Allocate(const std::size_t size, const std::size_t alignment = 16) override;
		virtual void Free(void* ptr) override;
	};

	namespace allocator
	{
		template <class T> T* allocateNew(BaseAllocator& allocator)
		{
			return new (allocator.allocate(sizeof(T), __alignof(T))) T;
		}

		template <class T> T* allocateNew(BaseAllocator& allocator, const T& t)
		{
			return new (allocator.allocate(sizeof(T), __alignof(T))) T(t);
		}

		template<class T> void deallocateDelete(BaseAllocator& allocator, T& object)
		{
			object.~T();
			allocator.deallocate(&object);
		}

		template<class T> T* allocateArray(BaseAllocator& allocator, size_t length)
		{
			ASSERT(length != 0);

			auto headerSize = sizeof(size_t) / sizeof(T);

			if (sizeof(size_t) % sizeof(T) > 0)
				headerSize += 1;

			//Allocate extra space to store array length in the bytes before the array
			T* p = ((T*)allocator.allocate(sizeof(T)*(length + headerSize), __alignof(T))) + headerSize;

			*(((size_t*)p) - 1) = length;

			for (size_t i = 0; i < length; i++)
				new (&p[i]) T;

			return p;
		}

		template<class T> void deallocateArray(BaseAllocator& allocator, T* array)
		{
			ASSERT(array != nullptr);

			size_t length = *(((size_t*)array) - 1);

			for (size_t i = 0; i < length; i++)
				array[i].~T();

			//Calculate how much extra memory was allocated to store the length before the array
			auto headerSize = sizeof(size_t) / sizeof(T);

			if (sizeof(size_t) % sizeof(T) > 0)
				headerSize += 1;

			allocator.deallocate(array - headerSize);
		}
	};
}

#endif /* CALLOCATOR_H */