/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#ifndef PLATFORM_DETECT_H
#define PLATFORM_DETECT_H

#include <stdint.h>

///////////////////////////////////////////////////////////////////////////////////////////////////
// OS definition

#define PLATFORM_OS_WINDOWS     1
#define PLATFORM_OS_LINUX       2
#define PLATFORM_OS_MACOSX      3
#define PLATFORM_OS_ANDROID     4
#define PLATFORM_OS_IOS			5

//!@todo ��������� ������ __MINGW__ - ����� ��� ����� ������ ���, �.� ��� ���� Windows �������, ������ ������ ������������������ ����������
#if (defined( __WIN32__ ) || defined( _WIN32 ) || defined( __WIN64__ ) || defined( _WIN64 ) || defined( WIN32 )) && (!defined(__ANDROID_API__)) && (!defined(__MINGW__)) && (!defined(__LINUX__))
#   define IS_OS_WINDOWS    1
#   define IS_OS_LINUX      0
#   define IS_OS_ANDROID    0
#   define IS_OS_MACOSX     0
#   define IS_OS_IOS		0

#   define PLATFORM_OS      PLATFORM_OS_WINDOWS
//#   pragma message("Platform OS is Windows.")
#elif defined(__LINUX__) && (!defined(__ANDROID_API__) ) || defined( LINUX )
#   define IS_OS_WINDOWS    0
#   define IS_OS_LINUX      1
#   define IS_OS_ANDROID    0
#   define IS_OS_MACOSX     0
#   define PLATFORM_OS      PLATFORM_OS_LINUX
//#   pragma message("Platform OS is Linux.")
#elif ( defined(__APPLE__) && defined(__MACH__) )  || defined( MACOSX )
#   define IS_OS_WINDOWS    0
#   define IS_OS_LINUX      0
#   define IS_OS_ANDROID    0
#   define IS_OS_MACOSX     1
#   define PLATFORM_OS      PLATFORM_OS_MACOSX
//#   pragma message("Platform OS is MacOSX.")
#elif ( defined(__ANDROID_API__) || defined(__ANDROID__ ) || defined( ANDROID ))
#   define IS_OS_WINDOWS		0
#   define IS_OS_LINUX			0
#   define IS_OS_ANDROID		1
#   define IS_OS_MACOSX			0
#	define NGTECH_STATIC_LIBS	1
#   define PLATFORM_OS      PLATFORM_OS_ANDROID
//#   pragma message("Platform OS is Android.")
#else
#   error "This platform is not supported."
#endif

#if ((IS_OS_ANDROID==1) || (IS_OS_LINUX==1) || (IS_OS_MACOSX==1))
#	define NGTECH_STATIC_LIBS	1
#endif

#define PLATFORM_COMPILER_MSVC    1
#define PLATFORM_COMPILER_GCC     2
#define PLATFORM_COMPILER_CLANG   3
#define PLATFORM_COMPILER_INTEL   4

#if defined( _MSC_VER )
#   define PLATFORM_COMPILER            PLATFORM_COMPILER_MSVC
#   define PLATFORM_COMPILER_VERSION    _MSC_VER
#   define IS_COMPILER_MSVC       1
#   define IS_COMPILER_GCC        0
#   define IS_COMPILER_CLANG      0
#   define IS_COMPILER_INTEL      0
#   define ENGINE_INLINE		  __forceinline
//#   pragma message("Platform Compiler is Microsoft Visual C++.")
#	pragma comment(lib,"winmm.lib")
#elif defined( __clang__ )
#   define PLATFORM_COMPILER            PLATFORM_COMPILER_CLANG
#   define PLATFORM_COMPILER_VERSION    (__clang_major__ + __clang_minor__ + __clang_patchlevel__)
#   define IS_COMPILER_MSVC       0
#   define IS_COMPILER_GCC        0
#   define IS_COMPILER_CLANG      1
#   define IS_COMPILER_INTEL      0
#   define ENGINE_INLINE		  inline
//#   pragma message("Platform Compiler is CLANG.")
#elif defined(__GNUC__) || defined(__GNUG__)
#   define PLATFORM_COMPILER            PLATFORM_COMPILER_GCC
#   define PLATFORM_COMPILER_VERSION    (__GNUC__ * 10000 + __GNUC_MINOR__ * 100)
#   define IS_COMPILER_MSVC       0
#   define IS_COMPILER_GCC        1
#   define IS_COMPILER_CLANG      0
#   define IS_COMPILER_INTEL      0
#   define ENGINE_INLINE		  inline
//#   pragma message("Platform Compiler is GCC.")
#else
#   error "This compiler is not supported."
#endif

#define PLATFORM_MEMORY_ADDRESS_SPACE_32BIT  1
#define PLATFORM_MEMORY_ADDRESS_SPACE_64BIT  2

#if defined(__x86_64__) || defined(_M_X64) || defined(__powerpc64__) || defined(__LP64__)
#   define IS_PLATFORM_64BIT                1
#   define IS_PLATFORM_32BIT                0
#   define PLATFORM_MEMORY_ADDRESS_SPACE    PLATFORM_MEMORY_ADDRESS_SPACE_64BIT
//#   pragma message("Using 64bit memory address space.")
#else
#   define IS_PLATFORM_64BIT                0
#   define IS_PLATFORM_32BIT                1
#   define PLATFORM_MEMORY_ADDRESS_SPACE    PLATFORM_MEMORY_ADDRESS_SPACE_32BIT
//#   pragma message("Using 32bit memory address space.")
#endif

/*
*/
#if _MSC_VER < 1900
#if !defined(snprintf)
#define snprintf sprintf_s
#endif

#if !defined(vnsprintf)
#define vnsprintf vsprintf_s
#endif
#endif  /* _MSC_VER */

#if (!defined(__MINGW__)) && (PLATFORM_COMPILER == PLATFORM_COMPILER_GCC || PLATFORM_COMPILER == PLATFORM_COMPILER_CLANG)
#define _strnicmp strncasecmp
#define _stricmp strcasecmp
#define stricmp strcasecmp
#endif

#if !defined(CROSSPLATFORM_EXPORT_FROM_DLL) || !defined(CROSSPLATFORM_IMPORT_FROM_DLL)
#if (PLATFORM_COMPILER == PLATFORM_COMPILER_GCC || PLATFORM_COMPILER == PLATFORM_COMPILER_CLANG || PLATFORM_COMPILER == PLATFORM_COMPILER_INTEL)
#define CROSSPLATFORM_EXPORT_FROM_DLL __attribute__((visibility("default")))
#define CROSSPLATFORM_IMPORT_FROM_DLL

#elif ((PLATFORM_COMPILER == PLATFORM_COMPILER_MSVC) && (IS_OS_WINDOWS))
#define CROSSPLATFORM_EXPORT_FROM_DLL __declspec(dllexport)
#define CROSSPLATFORM_IMPORT_FROM_DLL __declspec(dllimport)
#endif

#else

#define CROSSPLATFORM_EXPORT_FROM_DLL
#define CROSSPLATFORM_IMPORT_FROM_DLL
#pragma warning Unknown dynamic link import/export semantics.

#endif

#include "../../../Externals/ngtech_gnu/gnuadditions.h"

///////////////////////////////////////////////////////////////////////////////////////////////////
/*
*/
#define ALIGNED4(VALUE) (((size_t)(VALUE) + 3) & ~3)
#define ALIGNED8(VALUE) (((size_t)(VALUE) + 7) & ~7)
#define ALIGNED16(VALUE) (((size_t)(VALUE) + 15) & ~15)
#define ALIGNED128(VALUE) (((size_t)(VALUE) + 127) & ~127)
#define IS_ALIGNED4(VALUE) (((size_t)(VALUE) & 3) == 0)
#define IS_ALIGNED8(VALUE) (((size_t)(VALUE) & 7) == 0)
#define IS_ALIGNED16(VALUE) (((size_t)(VALUE) & 15) == 0)
#define IS_ALIGNED128(VALUE) (((size_t)(VALUE) & 127) == 0)
#define ASSERT_ALIGNED4(VALUE) ASSERT(IS_ALIGNED4(VALUE))
#define ASSERT_ALIGNED8(VALUE) ASSERT(IS_ALIGNED8(VALUE))
#define ASSERT_ALIGNED16(VALUE) ASSERT(IS_ALIGNED16(VALUE))
#define ASSERT_ALIGNED128(VALUE) ASSERT(IS_ALIGNED128(VALUE))

/*
*/
#if PLATFORM_OS == PLATFORM_OS_WINDOWS
#define ATTRIBUTE_ALIGNED4(NAME) __declspec(align(4)) NAME
#define ATTRIBUTE_ALIGNED8(NAME) __declspec(align(8)) NAME
#define ATTRIBUTE_ALIGNED16(NAME) __declspec(align(16)) NAME
#define ATTRIBUTE_ALIGNED128(NAME) __declspec(align(128)) NAME
#else
#define ATTRIBUTE_ALIGNED4(NAME) NAME __attribute__ ((aligned(4)))
#define ATTRIBUTE_ALIGNED8(NAME) NAME __attribute__ ((aligned(8)))
#define ATTRIBUTE_ALIGNED16(NAME) NAME __attribute__ ((aligned(16)))
#define ATTRIBUTE_ALIGNED128(NAME) NAME __attribute__ ((aligned(128)))
#endif
///////////////////////////////////////////////////////////////////////////////////////////////////

#define	_NAME_SEPARATOR_			"|"
#define	_THREADID_NAME_SEPARATOR_	"@"

#ifdef _DEBUG
#define UTIL_SAFE_RELEASE_DEBUG_HELPER(p, adp,  _file, _line) Warning("Remove Ref: ObjectName: %s , File: %s, Line: %i RefCount %i, Additional Params: %s", MACRO_MAKE_STRING(p), _file, _line, p->RefCounter(), adp)
#else
#define UTIL_SAFE_RELEASE_DEBUG_HELPER(p, adp,  _file, _line)
#endif

#define UTIL_SAFE_RELEASE_HELPER(p, adp,  _file, _line) { if ( (p) ) { UTIL_SAFE_RELEASE_DEBUG_HELPER(p,adp,_file, _line); { (p)->Release(); (p) = nullptr; } } }

#define SAFE_RELEASE(p) {UTIL_SAFE_RELEASE_HELPER(p, "", __FILE__,__LINE__)}
#define SAFE_RELEASE_ADV(p, adv) {UTIL_SAFE_RELEASE_HELPER(p, adv, __FILE__,__LINE__)}
#define SAFE_rELEASE(p) { if ( (p) ) { (p)->release(); } }
#define SAFE_rESET(p) { if ( (p) ) { (p).reset(); (p) = nullptr; } }
#define SAFE_DELETE(a) if( (a) != NULL ) delete (a); (a) = nullptr;
#define SAFE_DELETE_FROM_VECTOR(_vec, _ptr) { _vec.erase(std::remove(_vec.begin(), _vec.end(),_ptr), _vec.end()); }
#define SAFE_DESTROY(p) { if ( (p) ) { (p)->Destroy(); (p) = nullptr; } }
#define SAFE_DESTROY_FROM_SCENE(p) { if ( (p) ) { (p)->DestroyFromScene(); (p) = nullptr; } }
#define SAFE_DELETE_ARRAY(p) { delete [] p; }

#define MAX_FILENAME_SIZE				256
#define	MAX_STRING_CHARS_LENGTH			1024
#define MAX_PRINT_MSG					16384		// buffer size for our various printf routines

#include "macro_strings.h"
#include "macro_todo.h"

#ifndef MACRO_UNUSED
#define MACRO_UNUSED(x) (void)(x)
#endif

#if PLATFORM_COMPILER == PLATFORM_COMPILER_MSVC
#pragma warning(disable:4091)	// Need for hiding warning on winSDK. dbghelp.h
#pragma warning(disable:4316 )	// Object in heap, can't be corrected on 64
#pragma warning(disable:4714 )	// Function 'function' marked as __forceinline not inlined
#pragma warning(disable:4324)	// 'struct_name' : structure was padded due to __declspec(align())
#pragma warning(disable:4201)	// 'union{struct_name}' : union without name
#endif

#ifndef NOMINMAX
#define NOMINMAX
#endif // !NOMINMAX

// USE_SINGLE_PRECISION
#if USE_SINGLE_PRECISION
typedef float TimeDelta;
#else
typedef double TimeDelta;
#endif

#endif