// (C) SUMA 1996
// some classes for simple referencing and auto deleting dynamic objects

#ifdef _MSC_VER
#pragma once
#endif

#ifndef _POINTERS_HPP
#define _POINTERS_HPP

// ComRef class to automate reference counting for IUnknown objects

#include "Containers/typeOpts.hpp"
//#include <Es/Framework/debugLog.hpp>
//#include <Es/Memory/normalNew.hpp>

#if defined _CPPRTTI
#include "typeinfo.h"
#endif

namespace NGTech
{
	//! This template class provides a smart pointers for COM objects.
	/*!
	  Reference counting automation. This template class provides a reference
	  substitution for smart pointer operations.
	  \sa Ref, RefCount, SRef
	*/
	template<class Type>
	class ComRef
	{
	private:
		//! Reference to the COM object.
		Type *_ref;
	public:
		//! Default constructor.
		ComRef() { _ref = NULL; }
		//! Constructor.
		/*!
		  Initialization through temporary pointer. Be carefull using this...
		  \param ptr Pointer to COM object to assign.
		*/
		ComRef(Type *ptr) { _ref = ptr; }
		//! Copying of pointer. Present pointer will be released.
		/*!
		  Note that this method will not call the AddRef() method. It is beeing
		  expected the user will call it by himself, therefore using this method
		  you must be VERY careful.
		  \param source Pointer to COM object to assign.
		*/
		void operator = (Type *source) { Free(); _ref = source; }
		//! Determining wheter pointer is not null.
	  /*!
		\return TRUE in case pointer is not null, FALSE otherwise.
	  */
		bool NotNull() const { return _ref != NULL; }
		//! Determining wheter pointer is null.
	  /*!
		\return TRUE in case pointer is null, FALSE otherwise.
	  */
		bool IsNull() const { return _ref == NULL; }
		//! Copy constructor.
		/*!
		  \param source Source pointer to copy.
		*/
		ComRef(const ComRef &source) { _ref = source._ref; if (_ref) _ref->AddRef(); }
		//! Copying of pointer. Present pointer will be released.
		/*!
		  \param sRef Source pointer to assign.
		*/
		void operator = (const ComRef &sRef)
		{
			Type *source = sRef._ref;
			if (source) source->AddRef();
			Free();
			_ref = source;
		}
		//! Releasing of pointer to COM object.
		/*!
		  \return Resulting value of the reference count.
		*/
		int Free() { int ret = 0; if (_ref) ret = _ref->Release(), _ref = NULL; return ret; }
		//! Destructor.
		ENGINE_INLINE ~ComRef() { Free(); }
		//! Retrieving the pointer.
		/*!
		  \return Pointer to the COM object.
		*/
		ENGINE_INLINE Type *operator -> () const { return _ref; }
		//! Retrieving the pointer.
		/*!
		  \return Pointer to the COM object.
		*/
		ENGINE_INLINE Type *GetRef() const { return _ref; }
		//! Retrieving the pointer.
		/*!
		  \return Pointer to the COM object.
		*/
		ENGINE_INLINE operator Type *() const { return _ref; } // casting
		//! Initialization of the COM object.
		/*!
		  This method clears present pointer and returns pointer to this pointer
		  and expects user will initialize it properly by himself (f.i. it's up to
		  him to call the AddRef() method), therefore you must be VERY careful to
		  use this method.
		  \return Pointer to pointer to new COM object.
		*/
		Type **Init() { Free(); return &_ref; } // initialization will be performed outside
		ClassIsMovableZeroed(ComRef);
	};

	// SRef class automatically deallocated when destructed (like Ref with count==1)

	template<class Type>
	class InitPtr
	{
	private:
		mutable Type *_ref;
		// we use mutable because Visual C++ 5.0 has bug
		// - sometime const / non-const reference is messed up in templates

	public:
		InitPtr() { _ref = NULL; }
		InitPtr(Type *source) { _ref = source; }
		void operator = (Type *source) { _ref = source; }

		bool NotNull() const { return _ref != NULL; }
		bool IsNull() const { return _ref == NULL; }
		bool operator ! () const { return _ref == NULL; }
		//operator bool() const {return _ref!=NULL;}

		Type *operator -> () const { return _ref; }
		operator Type *() const { return _ref; } // casting

		ClassIsSimpleZeroed(InitPtr)
	};

	/// provide delete for a normal pointer
	template <class Type>
	struct SRefTraits
	{
		static void Delete(Type *ptr) { delete ptr; }
	};

	/// provide delete for an array pointer
	template <class Type>
	struct SRefArrayTraits
	{
		static void Delete(Type *ptr) { delete[] ptr; }
	};

	//! This template class provides a little pointer superstructure.
	/*!
	  Whereas using Ref structure more pointers can share one object, this structure is
	  suitable just for one pointer - one object relation. Anyway you can be sure that
	  with destroying the pointer the object itself will be also destroyed.
	  \sa Ref
	*/
	template<class Type>
	class SRef
	{
	protected:
		//! Reference to the object.
		/*!
			Note that we use the mutable keyword because Visual C++ 5.0 has a bug
			- sometimes const/non-const reference is messed up in templates.
		*/
		mutable Type *_ref;
	public:
		//! Default constructor
		ENGINE_INLINE SRef() { _ref = NULL; }
		//! Using this constructor you can determine the object to point to.
		/*!
		  \param source Pointer to the object to be assigned.
		*/
		ENGINE_INLINE SRef(Type *source)
		{
			_ref = source;
		}
		//! Assigning of an object. The previous content will be deleted.
		void operator = (Type *source)
		{
			if (_ref == source) return;
			Free();
			_ref = source;
		}
		//! Copy constructor
		/*!
		  \param sRef Reference to the source pointer. The source will be invalid then.
		*/
		SRef(const SRef &source)
		{
			_ref = source._ref;
			source._ref = NULL; // old is invalidated, valid is only new copy
		}
		//! Assigning of pointers. The one from the right side will be invalid then.
		void operator = (const SRef &source)
		{
			if (source._ref != _ref)
			{
				Free();
				_ref = source._ref;
				source._ref = NULL; // old is invalidated, valid is only new copy
			}
		}
		//! Determining whether pointer is not null.
	  /*!
		\return TRUE in case pointer is not null, FALSE otherwise.
	  */
		ENGINE_INLINE bool NotNull() const { return _ref != NULL; }
		//! Determining whether pointer is null.
	  /*!
		\return TRUE in case pointer is null, FALSE otherwise.
	  */
		ENGINE_INLINE bool IsNull() const { return _ref == NULL; }
		//! Determining whether pointer is null.
		ENGINE_INLINE bool operator ! () const { return _ref == NULL; }
		//! Destructor will delete the object itself
		ENGINE_INLINE ~SRef() { Free(); }
		//! This method will delete the object.
		void Free() { if (_ref) delete _ref, _ref = NULL; }
		//! This operator is suitable for accessing object members. It has the same meaning like standard -> operator.
		ENGINE_INLINE Type *operator -> () const { return _ref; }
		//! Operator for type casting.
		ENGINE_INLINE operator Type *() const { return _ref; }

		ClassIsMovableZeroed(SRef);
	};

	template<class Type>
	class APtr
	{
	private:
		mutable Type *_ref;
		// we use mutable because Visual C++ 5.0 has bug
		// - sometime const / non-const reference is messed up in templates

		void Alloc(int n)
		{
#if ALLOC_DEBUGGER && defined _CPPRTTI
			_ref = new(FileLine(typeid(Type).name()), sizeof(Type)) Type[n];
#else
			_ref = new Type[n];
#endif
		}
	public:
		APtr() { _ref = NULL; }
		explicit APtr(int n)
		{
			Alloc(n);
		}

		APtr(const APtr &source)
		{
			_ref = source._ref;
			source._ref = NULL; // old is invalidated, valid is only new copy
		}
		void operator = (const APtr &source)
		{
			if (source._ref != _ref)
			{
				Free();
				_ref = source._ref;
				source._ref = NULL; // old is invalidated, valid is only new copy
			}
		}

		void Realloc(int n)
		{
			Free();
			Alloc(n);
		}
		ENGINE_INLINE bool NotNull() const { return _ref != NULL; }
		ENGINE_INLINE bool IsNull() const { return _ref == NULL; }
		ENGINE_INLINE bool operator ! () const { return _ref == NULL; }

		~APtr() { if (_ref) delete[] _ref; }
		void Free() { if (_ref) delete[] _ref, _ref = NULL; }
		ENGINE_INLINE operator Type *() const { return _ref; } // casting

		ClassIsMovableZeroed(APtr);
	};

	//! This class as well as Ref template class are designed to provide smart pointer operations.
	/*!
	  Using this classes you need not take care of releasing structures you have allocated.
	  There is a reference count attribute inside RefCount class (_count) which helps us to determine wheter there still exists some valid pointer to it.
	  In case there is not we will delete whole object without fear.

	  Example of use:
	  <BR>...
	  <BR>class MyObject : public RefCount {
	  <BR>public:
	  <BR>  int a;
	  <BR>};
	  <BR>MyObject MO; // This is allocation on the stack - advantage of our Ref* classes is not seen here.
	  <BR>Ref<MyObject> pMO = new MyObject;
	  <BR>pMO->a = 666;
	  <BR>Ref<MyObject> pMO2 = pMO;
	  <BR>...
	  <BR>pMO2->Free(); // In case we are sure we don't need it anymore, we can remove a reference.
	  <BR>pMO->Free(); // At this point the object itself will be destroyed
	  <BR>...

	  Note that at the end of the scope where pMO* is defined, the object will be automatically released.
	  Another note that you MUST use the new keyword when allocating the space for the object.
	  Otherwisely constructor would not be called and _count would not be initialized.
	  \sa Ref
	*/
	class RefCount
	{
	private:
		//! Number of references to this object.
		mutable int _count;
	public:
		//! Default constructor.
		RefCount() { _count = 0; }
		//! Copy constructor (_count attribute will not be copied).
		/*!
		  \param src Reference to source object.
		*/
		RefCount(const RefCount &src) { _count = 0; }
		//! Copying of object (_count attribute will not be copied).
		void operator =(const RefCount &src) { _count = 0; }
		//! Destructor
		virtual ~RefCount() {} // use destructor of derived class
		//! Adding of reference to this object.
		/*!
		  \return Number of references.
		*/
		int AddRef() const
		{
			return ++_count;
		}
		//! Releasing of reference to this object.
		/*!
		  \return Number of references.
		*/
		int Release() const
		{
			int ret = --_count;
			if (ret == 0) delete (RefCount *)this;
			return ret;
		}

		template <class Tcallback, class Tcontainer, class Tobject>
		int ReleaseWithCallback(Tcallback callback, Tcontainer _con, Tobject obj) const
		{
			int ret = --_count;

			// Run action
			callback.Action(obj, _con);

			if (ret == 0) delete (RefCount *)this;

			return ret;
		}

		//! Determines number of references to this object.
		/*!
		  \return Number of references.
		*/
		int RefCounter() const { return _count; }
		//! get memory used by this object
		//! not including memory used to hold this object itself,
		//! which is covered by sizeof
		virtual double GetMemoryUsed() const { return 0; }
	};

	//! This template class provides a smart pointers.
	/*!
	  This template class provides a reference substitution for smart pointer operations.
	  See documentation of RefCount class for more information.
	  \sa RefCount, SRef, ComRef
	*/
	template<class Type>
	class Ref
	{
	protected:
		//! Reference to the object.
		Type *_ref;
	public:
		//! Default constructor.
		ENGINE_INLINE Ref() { _ref = NULL; }
		// Using this constructor you can get the existing reference from source parameter.
		/*!
		  \param source Source pointer to an object.
		*/
		Ref(Type *source)
		{
			if (source) source->AddRef();
			_ref = source;
		}
		//! Copying of pointer.
		void operator = (Type *source)
		{
			if (source) source->AddRef();
			if (_ref) _ref->Release();
			_ref = source;
		}
		// Copy constructor.
		/*!
		  \param sRef Reference to the source pointer.
		*/
		Ref(const Ref &sRef)
		{
			Type *source = sRef._ref;
			if (source) source->AddRef();
			_ref = source;
		}
		//! Copying of pointer.
		void operator = (const Ref &sRef)
		{
			Type *source = sRef._ref;
			if (source) source->AddRef();
			if (_ref) _ref->Release();
			_ref = source;
		}
		//! Determining wheter pointer is not null.
	  /*!
		\return TRUE in case pointer is not null, FALSE otherwise.
	  */
		ENGINE_INLINE bool NotNull() const { return _ref != NULL; }
		//! Determining wheter pointer is null.
	  /*!
		\return TRUE in case pointer is null, FALSE otherwise.
	  */
		ENGINE_INLINE bool IsNull() const { return _ref == NULL; }
		//! Destructor - heart of automatic releasing.
		ENGINE_INLINE ~Ref() { Free(); }
		//! This method will release the object (or at least decrements it's reference counter).
		/*!
		  However you are not forced to use it. Object will release itself automatically.
		*/
		void Free() { if (_ref) _ref->Release(), _ref = NULL; }
		//! This method will return standard pointer to the object.
		/*!
		  \return Standard pointer to the object.
		*/
		ENGINE_INLINE Type *GetRef() const { return _ref; }
		//! This method will set reference to the object.
		/*!
		  You must use this function with infernal caution. For instance if you set it carelessly to NULL,
		  you can be sure that the previous object will never be deleted.
		  \param ref New pointer to the object.
		*/
		ENGINE_INLINE void SetRef(Type *ref) { _ref = ref; }
		//! This operator is suitable for accessing object members. It has the same meaning like standard -> operator.
		ENGINE_INLINE Type *operator -> () const { return _ref; }
		//! Operator for type casting.
		ENGINE_INLINE operator Type *() const { return _ref; }

		//! calculate memory used
		double GetMemoryUsed() const
		{
			// for each owner only appropriate part of shared object is considered
			return double(sizeof(Type) + _ref->GetMemoryUsed()) / _ref->RefCounter();
		}
		ClassIsMovableZeroed(Ref);
	};

	//! Segment of alternative NULL addresses: 0 to MaxNull-1
#define MaxNull 0x0004

#define DNull(x) ((unsigned)(x) < MaxNull)

/*!
  The same as Ref<> but uses alternative NULL addresses (0-3) and
  re-initializes reference after destruction..
  \patch 27.3.2002 Pepca (alternative NULL values, DNull())
*/
	template < class Type >
	class RefD
	{
	protected:
		//! Reference to the object.
		Type *_ref;
	public:
		//! Default constructor.
		ENGINE_INLINE RefD()
		{
			_ref = NULL;
		}
		// Using this constructor you can get the existing reference from source parameter.
		/*!
		  \param source Source pointer to an object.
		*/
		RefD(Type *source)
		{
			if (!DNull(source)) source->AddRef();
			_ref = source;
		}
		/*! Copy constructor.
			\param sRef Reference to the source pointer.
		*/
		RefD(const RefD &sRef)
		{
			Type *source = sRef._ref;
			if (!DNull(source)) source->AddRef();
			_ref = source;
		}
		/*!
			Cross-constructor from Ref<>.
		*/
		RefD(const Ref<Type> &sRef)
		{
			Type *source = sRef.GetRef();
			if (!DNull(source)) source->AddRef();
			_ref = source;
		}
		//! Copying of pointer.
		void operator = (Type *source)
		{
			if (!DNull(source)) source->AddRef();
			if (!DNull(_ref)) _ref->Release();
			_ref = source;
		}
		//! Copying of pointer.
		void operator = (const RefD &sRef)
		{
			Type *source = sRef._ref;
			if (!DNull(source)) source->AddRef();
			if (!DNull(_ref)) _ref->Release();
			_ref = source;
		}
		//! Comparison with pointer.
		ENGINE_INLINE bool operator == (const Type *source) const
		{
			return _ref == source;
		}
		//! Comparison with pointer.
		ENGINE_INLINE bool operator != (const Type *source) const
		{
			return _ref != source;
		}
		//! Comparison with Ref<>.
		ENGINE_INLINE bool operator == (const Ref<Type> &sRef) const
		{
			return _ref == sRef.GetRef();
		}
		//! Comparison with Ref<>.
		ENGINE_INLINE bool operator != (const Ref<Type> &sRef) const
		{
			return _ref != sRef.GetRef();
		}
		//! Comparison with RefD<>.
		ENGINE_INLINE bool operator == (const RefD<Type> &sRef) const
		{
			return _ref == sRef._ref;
		}
		//! Comparison with RefD<>.
		ENGINE_INLINE bool operator != (const RefD<Type> &sRef) const
		{
			return _ref != sRef._ref;
		}
		//! bool typecast.
		ENGINE_INLINE operator bool() const
		{
			return !DNull(_ref);
		}
		//! bool typecast.
		ENGINE_INLINE bool operator ! () const
		{
			return DNull(_ref);
		}
		//! This operator is suitable for accessing object members. It has the same meaning like standard -> operator.
		ENGINE_INLINE Type *operator -> () const
		{
			return _ref;
		}
		//! Operator for type casting.
		ENGINE_INLINE operator Type *() const
		{
			return _ref;
		}
		//! This method will release the object (or at least decrements it's reference counter).
		/*!
		  However you are not forced to use it. Object will release itself automatically.
		*/
		void Free()
		{
			if (!DNull(_ref)) {
				_ref->Release();
				_ref = NULL;
			}
		}
		//! Destructor - heart of automatic releasing.
		~RefD()
		{
			Free();
		}
		/*!
		  \return TRUE in case pointer is not null, FALSE otherwise.
		*/
		ENGINE_INLINE bool NotNull() const
		{
			return !DNull(_ref);
		}
		//! Determining wheter pointer is null.
		/*!
		  \return TRUE in case pointer is null, FALSE otherwise.
		*/
		ENGINE_INLINE bool IsNull() const
		{
			return DNull(_ref);
		}
		//! This method will return standard pointer to the object.
		/*!
		  \return Standard pointer to the object.
		*/
		ENGINE_INLINE Type *GetRef() const
		{
			return _ref;
		}
		ClassIsMovableZeroed(RefD);
	};

	template<class Type>
	class RefN
	{
	public:
		static RefCount _nil;
#define TypeNull ((Type *)&_nil)

	private:
		Type *_ref;

	public:
		ENGINE_INLINE RefN() { _ref = TypeNull; }

		RefN(Type *source)
		{
			if (!source)
			{
				// TODO: avoid assigning NULL
				source = TypeNull;
			}
			source->AddRef();
			_ref = source;
		}
		void operator = (Type *source)
		{
			if (!source)
			{
				// TODO: avoid assigning NULL
				source = TypeNull;
			}
			source->AddRef();
			_ref->Release();
			_ref = source;
		}
		RefN(const RefN &sRef)
		{
			Type *source = sRef._ref;
			source->AddRef();
			_ref = source;
		}
		void operator = (const RefN &sRef)
		{
			Type *source = sRef._ref;
			source->AddRef();
			_ref->Release();
			_ref = source;
		}

		ENGINE_INLINE bool NotNull() const { return _ref != TypeNull; }
		ENGINE_INLINE bool IsNull() const { return _ref == TypeNull; }

		~RefN() { if (_ref) _ref->Release(); }
		void Free() { operator =(TypeNull); }

		// note: may return TypeNull
		ENGINE_INLINE Type *GetRef() const { return _ref; }
		ENGINE_INLINE void SetRef(Type *ref) { _ref = ref; } // note: use with caution

		ENGINE_INLINE Type *operator -> () const { return _ref; }
		ENGINE_INLINE operator Type *() const { return _ref; } // casting

		ClassIsMovableZeroed(RefN);
	};

	template <class Type>
	RefCount RefN<Type>::_nil;

	/*
	// TODO: use RefN instead of Ref
	#define Ref RefN
	*/

	// Temp class to automate deleting of dynamic objects

	//! This class provides an array structure with deleting dynamic object automation.
	/*!
	  It is sort of similar to AutoArray class, however, it is not so advanced.
	  \sa AutoArray
	*/
	template <class Type>
	class Temp
	{
		typedef ConstructTraits<Type> CTraits;
	private:
		//! Pointer to data.
		Type *_obj;
		//! Number of items in the array.
		int _n;
		//! Allocation of space for n items, implicit constructors are not called.
		/*!
		  \param n Number of items to allocate space for.
		*/
		void DoAlloc(int n)
		{
#if ALLOC_DEBUGGER && defined _CPPRTTI
			char *mem = new(FileLine(typeid(Type).name()), sizeof(Type)) char[n * sizeof(Type)];
#else
			char *mem = new char[n * sizeof(Type)];
#endif
			_obj = (Type *)mem;
			_n = n;
		}
		//! Allocation of space for n items, implicit constructors are called.
		/*!
		  \param n Number of items to allocate space for.
		*/
		void DoConstruct(int n)
		{
			DoAlloc(n);
			CTraits::ConstructArray(_obj, _n);
		}
		//! Deallocation of allocated space, destructors are called.
		void DoDelete()
		{
			if (_obj)
			{
				CTraits::DestructArray(_obj, _n);
				delete[](char *)_obj;
				_obj = NULL;
			}
		}
	public:
		//! Default constructor.
		Temp() { _obj = NULL, _n = 0; }
		//! Using this constructor you can allocate space for n items. Implicit constructors of those items are called.
		/*!
		  \param n Number of items to allocate space for.
		*/
		explicit Temp(int n) { DoConstruct(n); }
		//! Using this constructor you can create an array with n items.
		//! For each item will be called copy constructor with correspond src item.
		/*!
		  \param src Source array.
		  \param n Number of items to allocate space for.
		*/
		Temp(const Type *src, int n)
		{
			DoAlloc(n);
			CTraits::CopyConstruct(_obj, src, _n);
		}
		//! Assigning of an array. Array will be copied, the previous one will be deleted.
		//! For each item will be called copy constructor with correspond src item.
		void operator = (const Temp &src)
		{
			DoDelete();
			DoAlloc(src.Size());
			CTraits::CopyConstruct(_obj, src.Data(), src.Size());
		}
		//! Copy constructor. Array will be copied, the previous one will be deleted.
		//! For each item will be called copy constructor with correspond src item.
		/*!
		  \param src Source array.
		*/
		Temp(const Temp &src)
		{
			DoAlloc(src.Size());
			CTraits::CopyConstruct(_obj, src.Data(), src.Size());
		}
		//! This method will export data of this object.
		/*!
		  The data pointer of this object will be cancelled. It is up to caller of this method to delete data.
		  \return Pointer to data
		*/
		Type *Export() { Type *ret = _obj; _obj = NULL; return ret; }
		//! Determining wheter pointer is not null.
	  /*!
		\return TRUE in case pointer is not null, FALSE otherwise.
	  */
		ENGINE_INLINE bool NotNull() const { return _obj != NULL; }
		//! Determining wheter array is null.
	  /*!
		\return TRUE in case array is null, FALSE otherwise.
	  */
		ENGINE_INLINE bool IsNull() const { return _obj == NULL; }
		//! Determining wheter array is null.
		ENGINE_INLINE bool operator ! () const { return _obj == NULL; }
		//! Destructor will delete the array itself
		ENGINE_INLINE ~Temp() { DoDelete(); }
		//! This method will delete the array.
		ENGINE_INLINE void Free() { DoDelete(); }
		//! Operator for type casting.
		ENGINE_INLINE operator Type *() { return _obj; }
		//! Operator for type casting.
		ENGINE_INLINE operator const Type *() const { return _obj; }
		//! This method returns pointer to array.
		/*!
		  \return Pointer to array.
		*/
		ENGINE_INLINE Type *Data() { return _obj; }
		//! This method returns pointer to array.
		/*!
		  \return Pointer to array.
		*/
		ENGINE_INLINE const Type *Data() const { return _obj; }
		//! Returns number of items in the array.
		/*!
		  \return Number of items in the array.
		*/
		ENGINE_INLINE int Size() const { return _n; }
		//! Setting of specified item.
		/*!
		  Returned item can be modified.
		  \param i Index to desirable item.
		  \return Desirable item
		*/
		Type &Set(int i)
		{
			AssertDebug(i >= 0 && i < _n);
			return _obj[i];
		}
		//! Getting of specified item.
		/*!
		  Returned item cannot be modified.
		  \param i Index to desirable item.
		  \return Desirable item.
		*/
		const Type &Get(int i) const
		{
			AssertDebug(i >= 0 && i < _n);
			return _obj[i];
		}
		//! Defined operator [] for modifying a value.
		Type &operator [] (int i) { return Set(i); }
		//! Defined operator [] for reading a value.
		const Type &operator [] (int i) const { return Get(i); }
		//! Reallocating of the array to n items. The previous array will be deleted.
		/*!
		  \param n Number of items in the new array.
		*/
		void Realloc(int n)
		{
			DoDelete();
			DoConstruct(n);
		}
		//! Reallocating of the array to n items. The previous array will be deleted.
		//! The new array will be initialized by copy constructor with particular src parameter.
		/*!
		  \param src Source array.
		  \param n Number of items in the new array.
		*/
		void Realloc(const Type *src, int n)
		{
			DoDelete();
			DoAlloc(n);
			CTraits::CopyConstruct(_obj, src, _n);
		}

		ClassIsMovable(Temp);
	};

	template <class Type>
	class Auto
	{
		typedef ConstructTraits<Type> CTraits;

	private:
		Type *_obj;
		int _n;

	private: // No copy
		void operator = (const Auto &src);
		Auto(const Auto &src);

	public:
		Auto(int n, void *mem)
		{
			_obj = (Type *)mem;
			_n = n;
			CTraits::ConstructArray(_obj, _n);
		}
		~Auto()
		{
			CTraits::DestructArray(_obj, _n);
		}

		operator Type *() { return _obj; } // casting
		operator const Type *() const { return _obj; } // casting

		Type *Data() { return _obj; }
		const Type *Data() const { return _obj; }
		int Size() const { return _n; }
	};

#define AutoAuto(Type,var,n) Auto<Type> var(n,_alloca(sizeof(Type)*n))

	// class to automate buffer deallocating and size information
	template <class Type>
	class Buffer : public RefCount
	{
		typedef ConstructTraits<Type> CTraits;

	private:
		int _length;
		Type *_buffer;

	protected:
		void DoConstruct() { _buffer = NULL, _length = 0; }
		void DoConstruct(int i)
		{
#if ALLOC_DEBUGGER && defined _CPPRTTI
			_buffer = new(FileLine(typeid(Type).name()), sizeof(Type)) Type[i];
#else
			_buffer = new Type[i];
#endif
			_length = i;
		}

		void DoConstruct(const Type *src, int len)
		{
			DoConstruct(len);
			for (int i = 0; i < len; i++) _buffer[i] = src[i];
		}

		void DoConstruct(const Buffer &src, int from = 0, int to = -1)
		{
			if (src._buffer)
			{
				if (to < 0) to = src._length;
				DoConstruct(src._buffer + from, to - from);
			}
			else
			{
				_buffer = NULL, _length = 0;
			}
		}

	public:
		void Delete()
		{
			if (_buffer) delete[] _buffer;
			_buffer = NULL;
			_length = 0;
		}
		Buffer() { DoConstruct(); }
		Buffer(int len) { DoConstruct(len); }
		Buffer(const Type *src, int len) { DoConstruct(src, len); }
		Buffer(const Buffer &src, int from = 0, int to = -1) { DoConstruct(src, from, to); }
		void Init() { Delete(); DoConstruct(); }
		void Init(int len) { Delete(); DoConstruct(len); }
		void Init(const Type *src, int len) { Delete(); DoConstruct(src, len); }
		void Realloc(int len) {}
		void Resize(int len)
		{
			if (_length != len) Delete(), DoConstruct(len);
		}
		void Init(const Buffer &src, int from = 0, int to = -1)
		{
			Delete();
			DoConstruct(src, from, to);
		}
		~Buffer() { Delete(); }

		void operator = (const Buffer &src)
		{
			Delete();
			DoConstruct(src);
		}

		ENGINE_INLINE int Length() const { return _length; }
		ENGINE_INLINE int Size() const { return _length; }
		ENGINE_INLINE Type *Data() { return _buffer; }
		ENGINE_INLINE const Type *Data() const { return _buffer; }
		ENGINE_INLINE operator Type *() { return _buffer; } // casting
		ENGINE_INLINE operator const Type *() const { return _buffer; } // casting

		const Type &operator [](int i) const { return _buffer[i]; }
		Type &operator [](int i) { return _buffer[i]; }

		ClassIsMovable(Buffer);
	};
}

//#include <Es/Memory/debugNew.hpp>

#endif