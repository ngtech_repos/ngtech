/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "PlatformPrivate.h"

namespace NGTech
{
	/**
	*/
	PlatformManager::PlatformManager()
	{}

	/**
	*/
	PlatformManager::~PlatformManager()
	{
		scheduler.reset();
	}

	/**
	*/
	bool PlatformManager::_InitScheduler()
	{
		try
		{
			scheduler = std::make_shared<MT::TaskScheduler>();
#ifdef MT_INSTRUMENTED_BUILD
			scheduler->SetProfilerEventListener(GetProfiler());
#endif
		}
		catch (std::exception&e)
		{
			PlatformError(e.what(), __FUNCTION__, true);
			return false;
		}

		return true;
	}

	/**
	*/
	void PlatformManager::UpdatePlatform()
	{
		ASSERT(scheduler, "Invalid scheduler pointer");
		if (!scheduler) {
			PlatformError("Invalid scheduler pointer %s", __FUNCTION__, true);
			return;
		}
	}

	/**
	*/
	void PlatformManager::RunTask(const ThreadTask& _task, unsigned int _count)
	{
		ASSERT(scheduler, "Invalid scheduler pointer");
		if (!scheduler) {
			PlatformError("Invalid scheduler pointer %s", __FUNCTION__, true);
			return;
		}

		scheduler->RunAsync(_task.GetGroup(), &_task, _count);
		scheduler->WaitGroup(_task.GetGroup(), _count);
	}

	/**
	*/
	void PlatformManager::WaitTask(const ThreadTask& _task, unsigned int _time)
	{
		ASSERT(scheduler, "Invalid scheduler pointer");
		if (!scheduler) {
			PlatformError("Invalid scheduler pointer %s", __FUNCTION__, true);
			return;
		}

		scheduler->WaitGroup(_task.GetGroup(), _time);
	}

	/**
	*/
	void PlatformManager::WaitAllTasks(unsigned long _m)
	{
		ASSERT(scheduler, "Invalid scheduler pointer");
		if (!scheduler) {
			PlatformError("Invalid scheduler pointer %s", __FUNCTION__, true);
			return;
		}

		scheduler->WaitAll(_m);
	}

	/**
	*/
	void PlatformManager::ReleaseGroup(const ThreadTask&_task)
	{
		ASSERT(scheduler, "Invalid scheduler pointer");
		if (!scheduler) {
			PlatformError("Invalid scheduler pointer %s", __FUNCTION__, true);
			return;
		}

		scheduler->ReleaseGroup(_task.GetGroup());
	}

	void PlatformManager::StopJobs()
	{
		ASSERT(scheduler, "Invalid scheduler pointer");
		if (!scheduler) {
			PlatformError("Invalid scheduler pointer %s", __FUNCTION__, true);
			return;
		}

		SAFE_rESET(scheduler);
	}

	bool PlatformManager::InitPlatformCode()
	{
		bool res = _InitScheduler();
		ASSERT(res, "Failed InitPlatformCode");

		return res;
	}
}