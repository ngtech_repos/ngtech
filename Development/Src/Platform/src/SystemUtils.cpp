/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "PlatformPrivate.h"

#if PLATFORM_OS == PLATFORM_OS_WINDOWS
#include <Windows.h>
#elif PLATFORM_OS == PLATFORM_OS_ANDROID
#include <android/log.h>
#define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, PROJECT_TITLE, __VA_ARGS__))
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, PROJECT_TITLE, __VA_ARGS__))
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, PROJECT_TITLE, __VA_ARGS__))
#define LOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR, PROJECT_TITLE, __VA_ARGS__))
#define LOGEF(...) ((void)__android_log_print(ANDROID_LOG_FATAL, PROJECT_TITLE, __VA_ARGS__))
#endif

#include <string>
#include <iostream>

namespace NGTech
{
	const char *Sys_EXEPath()
	{
#if PLATFORM_OS==PLATFORM_OS_WINDOWS
		static char exe[MAX_FILENAME_SIZE];
		memset(exe, 0, sizeof(exe));
		GetModuleFileNameA(NULL, exe, sizeof(exe) - 1);
#elif PLATFORM_OS==PLATFORM_OS_LINUX
		char exe[1024];
		memset(exe, 0, sizeof(exe));
		if (readlink("/proc/self/exe", exe, sizeof(exe)) != -1)
			exe[sizeof(buf) - 1] = '\0';
#elif (PLATFORM_OS==PLATFORM_OS_MACOSX)||(PLATFORM_OS==PLATFORM_OS_IOS)
		char exe[1024];
		memset(exe, 0, sizeof(exe));
		unsigned int size = sizeof(exe);
		if (_NSGetExecutablePath(exe, &size) != -1)
			exe[sizeof(buf) - 1] = '\0';
#elif (PLATFORM_OS==PLATFORM_OS_ANDROID)
		auto exe = String("").c_str();
#endif
		return exe;
	}

	void PlatformError(const char* desc, const char* header, bool fatal)
	{
#if PLATFORM_OS == PLATFORM_OS_WINDOWS
		MessageBoxA(0, desc, header, 0);
#elif PLATFORM_OS == PLATFORM_OS_ANDROID
		if (fatal)
			LOGEF(header, desc);
		else
			LOGE(header, desc);
#else
		std::cout << header << desc.c_str() << "\n";
#endif
		if (fatal)
			exit(0);
	}

	String Sys_GetSubDirectory(String sFile)
	{
		String sDirectory;
		int slashIndex = -1;
		for (int i = (int)sFile.size() - 1; i >= 0; i--)
		{
			if (sFile[i] == '\\' || sFile[i] == '/')
			{
				slashIndex = i;
				break;
			}
		}

		sDirectory = sFile.substr(0, slashIndex + 1);
		return sDirectory;
	}
}