/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "PlatformPrivate.h"

#include "../../Common/StringHelper.h"

#if PLATFORM_OS == PLATFORM_OS_WINDOWS
#include <Windows.h>
#include <string>

namespace NGTech
{
	void Sys_ReLaunch(void * data, const unsigned int dataSize) {
		char szPathOrig[MAX_PRINT_MSG];
		STARTUPINFOA si;
		PROCESS_INFORMATION	pi;

		ZeroMemory(&si, sizeof(si));
		si.cb = sizeof(si);

		strcpy(szPathOrig, StringHelper::StickString("\"%s\" %s", Sys_EXEPath(), (const char *)data).c_str());

		if (!CreateProcessA(NULL, szPathOrig, NULL, NULL, 0, 0, NULL, NULL, &si, &pi)) {
			MessageBoxA(0, StringHelper::StickString("Could not start process: '%s' ", szPathOrig).c_str(), "Error", 0);
			return;
		}
	}
}
#endif