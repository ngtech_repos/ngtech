/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "PlatformPrivate.h"

// see:https://github.com/4ian/GD/blob/master/GDCpp/GDCpp/Runtime/TimeManager.cpp
#if PLATFORM_COMPILER == PLATFORM_COMPILER_MSVC || (IS_OS_WINDOWS && PLATFORM_COMPILER == PLATFORM_COMPILER_INTEL)
#	include <windows.h>
#	include <Mmsystem.h>
#	pragma comment(lib, "winmm.lib")
#else
#	include <sys/time.h>

#if ((IS_OS_MACOSX || IS_OS_IOS) || (IS_OS_ANDROID && __ANDROID_API__ > 21))
#	include <mach/mach.h>
#	include <mach/mach_time.h>
#endif

#endif

namespace NGTech
{
	Timer::Timer() :
		mTimeStart(0),
		mLastTime(0),
		mLastFrame(0),
		timeScale(1.0),
		needTimebase(true)
	{
		this->Reset();
	}

	void Timer::Reset()
	{
		this->_Init();
	}

	uint64_t Timer::GetMilliseconds()
	{
		return GetCurrentMilliseconds() - mTimeStart;
	}

	uint64_t Timer::GetCurrentMilliseconds()
	{
#if PLATFORM_COMPILER == PLATFORM_COMPILER_MSVC || (IS_OS_WINDOWS && PLATFORM_COMPILER == PLATFORM_COMPILER_INTEL)
		/*
		We do this because clock() is not affected by timeBeginPeriod on Win32.
		QueryPerformanceCounter is a little overkill for the amount of precision that
		I consider acceptable. If someone submits a patch that replaces this code
		with QueryPerformanceCounter, I wouldn't complain. Until then, timeGetTime
		gets the results I'm after. -EMS

		See: http://www.geisswerks.com/ryan/FAQS/timing.html
		And: http://support.microsoft.com/default.aspx?scid=KB;EN-US;Q274323&
		*/
		return timeGetTime();
#else
		// TODO:Timer: Nick:was,but needly test on Unix
		// ANDROID API 23 - it's android 6.0
#if ((IS_OS_MACOSX || IS_OS_IOS || IS_OS_LINUX) || (IS_OS_ANDROID && __ANDROID_API__ > 21))
		// Must exist on IOS, on android only on Android 6.0 and later
		return mach_absolute_time();
#else
		struct timeval now;
		gettimeofday(&now, NULL);
		return (now.tv_sec) * 1000 + (now.tv_usec) / 1000;
#endif
#endif
	}

	void Timer::_Init()
	{
		if (needTimebase)
		{
			double _frequencyTmp = 0.0;
			double _timeScaleTmp = 0.0;

			needTimebase = false;
#if PLATFORM_COMPILER == PLATFORM_COMPILER_MSVC || (IS_OS_WINDOWS && PLATFORM_COMPILER == PLATFORM_COMPILER_INTEL)
			LARGE_INTEGER waitTime;
			QueryPerformanceFrequency(&waitTime);

			_frequencyTmp = waitTime.QuadPart;
			_timeScaleTmp = 1.0 / _frequencyTmp;
#else
#if ((IS_OS_MACOSX || IS_OS_IOS || IS_OS_LINUX) || (IS_OS_ANDROID && __ANDROID_API__ > 21))
			mach_timebase_info_data_t timebase;
			mach_timebase_info(&timebase);

			_frequencyTmp = timebase.denom / 1000000000.0;
			_timeScaleTmp = (double)timebase.numer / _frequencyTmp;
#else
			struct timespec ts;
			clock_gettime(CLOCK_MONOTONIC, &ts);
			_timeScaleTmp = (double)ts.tv_sec + (double)ts.tv_nsec / 1000000000.0;
#endif
#endif
			SetTimeScale(_timeScaleTmp);
		}

		mTimeStart = GetCurrentMilliseconds();
		mLastTime = mTimeStart;
		mLastFrame = mTimeStart;
	}

	TimeDelta Timer::GetTimeScale() {
		return timeScale;
	}

	void Timer::SetTimeScale(TimeDelta s) {
		if (s < 0.0) {
			s = 1.0;
		}
		timeScale = s;
	}

	TimeDelta Timer::GetElapsedSec()
	{
		return GetElapsed()*timeScale;
	}

	TimeDelta Timer::GetElapsed()
	{
		auto mNowTime = GetMilliseconds();
		auto mTimeDiff = mNowTime - mLastTime;
		mLastTime = mNowTime;

		return mTimeDiff;
	}

	TimeDelta Timer::Frametime()
	{
		uint64_t currentTime = GetCurrentMilliseconds();

		double rval = (currentTime - mLastFrame) * timeScale;
		mLastFrame = currentTime;

		return rval;
	}

	void Timer::UpdateFrametime()
	{
		uint64_t currentTime = GetCurrentMilliseconds();
		mLastFrame = currentTime;
	}

	void Timer::UpdateElapsed()
	{
		auto mNowTime = GetMilliseconds();
		mLastTime = mNowTime;
	}
} // namespace NGTech