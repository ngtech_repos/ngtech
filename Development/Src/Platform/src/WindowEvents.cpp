/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//***************************************************
#include "PlatformPrivate.h"
//***************************************************
#if !IS_OS_ANDROID
#include <stdlib.h>
#include <iostream>
//***************************************************
#include "../../../Externals/glfw/include/GLFW/glfw3.h"
//***************************************************
#include "../inc/I_Input.h"
#include "../inc/WindowSystem_GLFW.h"
//***************************************************
#include "LogFunctions.h"
//***************************************************

namespace NGTech {
	/*
	*/
	static ENGINE_INLINE const char* get_button_name(int button)
	{
		switch (button)
		{
		case GLFW_MOUSE_BUTTON_LEFT:
			return "left";
		case GLFW_MOUSE_BUTTON_RIGHT:
			return "right";
		case GLFW_MOUSE_BUTTON_MIDDLE:
			return "middle";
		}

		return NULL;
	}

	/**
	*/
	void WindowSystemGLFW::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
	{
		auto windowSystem = static_cast<WindowSystemGLFW*>(glfwGetWindowUserPointer(window));
		ASSERT(windowSystem, "Invalid pointer on windowSystem");

		if (!windowSystem) return;

		/*
		*/
		switch (action) {
		case GLFW_PRESS:
		{
			/*
			*/
			windowSystem->m_Input.keys[key] = true;

			break;
		}

		case GLFW_RELEASE:
		{
			/*
			*/
			windowSystem->m_Input.keys[key] = false;

			break;
		}
		}
	}

	/**
	*/
	void WindowSystemGLFW::mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
	{
		auto windowSystem = static_cast<WindowSystemGLFW*>(glfwGetWindowUserPointer(window));
		ASSERT(windowSystem, "Invalid pointer on windowSystem");

		if (!windowSystem) return;

		switch (action) {
		case GLFW_PRESS:
		{
			windowSystem->m_Input.mouseButtons[button] = true;
			break;
		}

		case GLFW_RELEASE:
		{
			windowSystem->m_Input.mouseButtons[button] = false;
			break;
		}
		}
	}

	/**
	*/
	void WindowSystemGLFW::cursor_position_callback(GLFWwindow* window, double mx, double my)
	{
		auto windowSystem = static_cast<WindowSystemGLFW*>(glfwGetWindowUserPointer(window));
		ASSERT(windowSystem, "Invalid pointer on windowSystem");

		if (!windowSystem) return;

		if ((windowSystem->m_Input.oldMouseX == windowSystem->m_Input.mouseX) && (windowSystem->m_Input.oldMouseY == windowSystem->m_Input.mouseY))
			windowSystem->m_Input.mousing = false;
		else
			windowSystem->m_Input.mousing = true;

		windowSystem->m_Input.oldMouseX = windowSystem->m_Input.mouseX;
		windowSystem->m_Input.oldMouseY = windowSystem->m_Input.mouseY;

		windowSystem->m_Input.mouseX = mx;
		windowSystem->m_Input.mouseY = my;
	}

	/**
	*/
	void WindowSystemGLFW::window_size_callback(GLFWwindow* window, int _width, int _height) {
		auto windowSystem = static_cast<WindowSystemGLFW*>(glfwGetWindowUserPointer(window));

		ASSERT(windowSystem, "Invalid pointer on windowSystem");

		if (!windowSystem)
			return;

		windowSystem->m_Callback.ResizeWindow(_width, _height);
	}

	/**
	*/
	static ENGINE_INLINE void CloseGLFWWindow(GLFWwindow* _window) {
		ASSERT(_window, "Invalid pointer on _window");
		glfwWindowShouldClose(_window);
	}
}
#endif