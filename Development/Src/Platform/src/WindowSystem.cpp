/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "PlatformPrivate.h"

#if IS_OS_WINDOWS
//***************************************************
#include "WindowSystem.h"
//***************************************************
#include "LogFunctions.h"
//***************************************************

namespace NGTech {
#ifndef DROP_EDITOR
#pragma comment(lib, "opengl32.lib")

	/**
	*/
	WindowSystem*_gWindowSystem = nullptr;

	static LRESULT ENGINE_INLINE CALLBACK wndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		switch (uMsg)
		{
		case WM_CLOSE:
		{
			PostQuitMessage(0);
			//exit(0);
			break;
		}

		case WM_KEYDOWN:
		{
			if (_gWindowSystem)
				_gWindowSystem->m_Input.keys[wParam] = true;

			break;
		}

		case WM_KEYUP:
		{
			if (_gWindowSystem)
				_gWindowSystem->m_Input.keys[wParam] = false;

			break;
		}

		case WM_MOUSEMOVE:
		{
			if (_gWindowSystem) {
				_gWindowSystem->m_Vars.mx = LOWORD(lParam);
				_gWindowSystem->m_Vars.my = HIWORD(lParam);
				_gWindowSystem->m_Input.mousing = true;

				break;
			}
		}

		case WM_LBUTTONDOWN:
		{
			if (_gWindowSystem)
				_gWindowSystem->m_Input.mouseButtons[0] = true;

			break;
		}

		case WM_LBUTTONUP:
		{
			if (_gWindowSystem)
				_gWindowSystem->m_Input.mouseButtons[0] = false;

			break;
		}

		case WM_RBUTTONDOWN:
		{
			if (_gWindowSystem)
				_gWindowSystem->m_Input.mouseButtons[1] = true;

			break;
		}

		case WM_RBUTTONUP:
		{
			if (_gWindowSystem)
				_gWindowSystem->m_Input.mouseButtons[1] = false;

			break;
		}

		case WM_SIZE:
		{
			uint32_t w = LOWORD(lParam);
			uint32_t h = HIWORD(lParam);

			if (_gWindowSystem)
				_gWindowSystem->m_Callback.ResizeWindow(w, h);

			break;
		}

		case WM_DISPLAYCHANGE:
		{
			uint32_t w = LOWORD(lParam);
			uint32_t h = HIWORD(lParam);

			if (_gWindowSystem)
				_gWindowSystem->m_Callback.ResizeWindow(w, h);

			break;
		}

		case WM_SIZING:
		{
			uint32_t w = LOWORD(lParam);
			uint32_t h = HIWORD(lParam);

			if (_gWindowSystem)
				_gWindowSystem->m_Callback.ResizeWindow(w, h);

			break;
		}

		default:
			return DefWindowProc(hWnd, uMsg, wParam, lParam);
		}
		return 0;
	}

	static ENGINE_INLINE void _GetMonitorResolution(HWND hwnd, uint32_t &monitor_width, uint32_t &monitor_height)
	{
		ASSERT(hwnd, "Invalid pointer");

		HMONITOR monitor = MonitorFromWindow(hwnd, MONITOR_DEFAULTTONEAREST);
		MONITORINFO info;
		info.cbSize = sizeof(MONITORINFO);
		GetMonitorInfo(monitor, &info);
		monitor_width = info.rcMonitor.right - info.rcMonitor.left;
		monitor_height = info.rcMonitor.bottom - info.rcMonitor.top;
	}

	/**
	*/

	/**
	*/
	WindowSystem::WindowSystem(void*_m_engine, const PlatformDependVars&_vars, const WindowCallbacks& _callback)
	{
		LogPrintf("-- WindowSystem --");

		/*	this->m_Vars.mx = 0;
			this->m_Vars.my = 0;
			this->m_Vars.width = _cvars->r_width;
			this->m_Vars.height = _cvars->r_height;
			this->m_Vars.fullscreen = _cvars->r_fullscreen;
			this->m_Vars.withoutBorder = _cvars->w_withoutBorder;
			this->m_Vars.hWnd = nullptr;
			this->m_Vars.bpp = _cvars->r_bpp;
			this->m_Vars.zdepth = _cvars->r_zdepth;
			this->m_Vars.isExternalHwnd = false;

			this->m_Input.mouseGrabbed = false;*/

		m_engine = _m_engine;
		m_Callback = _callback;
		m_Vars = _vars;

		_gWindowSystem = this;
	}

	void ShowWindowWrapper(HWND hWnd)
	{
		ShowWindow(hWnd, SW_SHOW);
		SetForegroundWindow(hWnd);
		SetFocus(hWnd);
	}

	/**
	*/
	void WindowSystem::Initialise(void* _hwnd) {
		m_Vars.isExternalHwnd = _hwnd != NULL;

		if (m_Vars.isExternalHwnd == false)
			ShowOSCursor(false);

		if (_hwnd == 0)
		{
			WNDCLASSA wc;
			DWORD dwExStyle;
			DWORD dwStyle;
			RECT windowRect;

			windowRect.left = 0;
			windowRect.right = this->m_Vars.width;
			windowRect.top = 0;
			windowRect.bottom = this->m_Vars.height;

			this->m_Vars.hInstance = GetModuleHandle(NULL);
			wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
			wc.lpfnWndProc = (WNDPROC)wndProc;
			wc.cbClsExtra = 0;
			wc.cbWndExtra = 0;
			wc.hInstance = (HINSTANCE)this->m_Vars.hInstance;
			wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
			wc.hCursor = LoadCursor(NULL, IDC_ARROW);
			wc.hbrBackground = NULL;
			wc.lpszMenuName = NULL;
			wc.lpszClassName = "OpenGL";

			if (!RegisterClassA(&wc)) {
				Error("WindowSystem::initialise() error: failed to register the window class", true);
				return;
			}

			if (this->m_Vars.fullscreen) {
				DEVMODE dmScreenSettings;
				memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));

				dmScreenSettings.dmSize = sizeof(dmScreenSettings);
				dmScreenSettings.dmPelsWidth = this->m_Vars.width;
				dmScreenSettings.dmPelsHeight = this->m_Vars.height;
				dmScreenSettings.dmBitsPerPel = this->m_Vars.bpp;
				dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

				if (ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL) {
					this->m_Vars.fullscreen = false;
				}
			}

			if (this->m_Vars.fullscreen) {
				dwExStyle = WS_EX_APPWINDOW;
				dwStyle = WS_POPUP;
			}
			else {
				dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
				dwStyle = WS_OVERLAPPEDWINDOW;
			}

			AdjustWindowRectEx(&windowRect, dwStyle, FALSE, dwExStyle);

			if (!(this->m_Vars.hWnd = CreateWindowExA(dwExStyle,			// Extended Style For The Window
				"OpenGL",							// Class Name
				PROJECT_TITLE,						// Window Title
				dwStyle |							// Defined Window Style
				WS_CLIPSIBLINGS |					// Required Window Style
				WS_CLIPCHILDREN,					// Required Window Style
				0, 0,								// Window Position
				windowRect.right - windowRect.left,	// Calculate Window Width
				windowRect.bottom - windowRect.top,	// Calculate Window Height
				NULL,								// No Parent Window
				NULL,								// No Menu
				(HINSTANCE)this->m_Vars.hInstance,							// Instance
				NULL)))								// Dont Pass Anything To WM_CREATE
			{
				Error("WindowSystem::initialise() error: window creation error", true);
				return;
			}
		}
		else
			this->m_Vars.hWnd = _hwnd;

		auto _hwndConv = (HWND)this->m_Vars.hWnd;

		if (!m_Vars.isExternalHwnd) {
			ShowWindowWrapper(_hwndConv);
		}
		else
			_GetMonitorResolution(_hwndConv, this->m_Vars.width, this->m_Vars.height);
	}

	WindowSystem::~WindowSystem() {
		if (m_Vars.fullscreen) {
			ShowCursor(TRUE);
		}

		if (m_Vars.hDC) {
			ReleaseDC((HWND)m_Vars.hWnd, (HDC)m_Vars.hDC);
			m_Vars.hDC = NULL;
		}

		if (m_Vars.hWnd) {
			DestroyWindow((HWND)m_Vars.hWnd);
			m_Vars.hWnd = NULL;
		}

		UnregisterClassA("OpenGL", (HINSTANCE)m_Vars.hInstance);
		m_Vars.hInstance = NULL;
	}

	void WindowSystem::SetTitle(const String &title) {
		SetWindowTextA((HWND)m_Vars.hWnd, title.c_str());
	}

	void WindowSystem::Update() {
		memcpy(m_Input.oldKeys, m_Input.keys, sizeof(m_Input.keys));
		memcpy(m_Input.oldMouseButtons, m_Input.mouseButtons, sizeof(m_Input.mouseButtons));
		m_Input.mousing = false;

		MSG	msg;
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		if (m_Input.mouseGrabbed) {
			m_Input.oldMouseX = m_Input.mouseX;
			m_Input.oldMouseY = m_Input.mouseY;
			m_Input.mouseX += m_Vars.mx - m_Vars.width / 2;
			m_Input.mouseY += m_Vars.my - m_Vars.height / 2;
			SetMousePos(m_Vars.width / 2, m_Vars.height / 2);
		}
		else {
			m_Input.oldMouseX = m_Input.mouseX;
			m_Input.oldMouseY = m_Input.mouseY;
			m_Input.mouseX = m_Vars.mx;
			m_Input.mouseY = m_Vars.my;
		}
	}

	bool WindowSystem::IsKeyPressed(const char* _key) {
		const int key = Input_GetKeyValueByChar(_key);
		return m_Input.keys[key];
	}

	bool WindowSystem::IsKeyDown(const char* _key) {
		const int key = Input_GetKeyValueByChar(_key);
		return (m_Input.keys[key] && !m_Input.oldKeys[key]);
	}

	bool WindowSystem::IsKeyUp(const char* _key) {
		const int key = Input_GetKeyValueByChar(_key);
		return (!m_Input.keys[key] && m_Input.oldKeys[key]);
	}

	bool WindowSystem::IsMouseButtonPressed(int mb) {
		return m_Input.mouseButtons[mb];
	}

	bool WindowSystem::WasMouseButtonPressed(int mb) {
		return (m_Input.mouseButtons[mb] && !m_Input.oldMouseButtons[mb]);
	}

	bool WindowSystem::WasMouseButtonReleased(int mb) {
		return (!m_Input.mouseButtons[mb] && m_Input.oldMouseButtons[mb]);
	}

	void WindowSystem::ShowCursor(bool show) {
		m_Input.cursorVisible = show;
	}

	void WindowSystem::SetMousePos(int x, int y) {
		POINT pt;
		pt.x = x;
		pt.y = y;
		ClientToScreen((HWND)m_Vars.hWnd, &pt);
		SetCursorPos(pt.x, pt.y);
	}

	void WindowSystem::GrabMouse(bool grab) {
		m_Input.mouseX = m_Input.oldMouseX = m_Vars.width / 2;
		m_Input.mouseY = m_Input.oldMouseY = m_Vars.height / 2;

		if (grab) { SetMousePos(m_Vars.width / 2, m_Vars.height / 2); }
		ShowCursor(!grab);
		m_Input.mouseGrabbed = grab;
	}

	void WindowSystem::ShowOSCursor(bool _value) {
		::ShowCursor(_value);
	}

	void WindowSystem::ManageVSync(bool _v) {
		//!@todo Implement vsync
	}

	/**
	DEPRECATED
	*/
	const int WindowSystem::Input_GetKeyValueByChar(const char*  _p)
	{
		if (stricmp(_p, "A") == 0)
			return KEY_A;
		else if (stricmp(_p, "B") == 0)
			return KEY_B;
		else if (stricmp(_p, "C") == 0)
			return KEY_C;
		else if (stricmp(_p, "D") == 0)
			return KEY_D;
		else if (stricmp(_p, "E") == 0)
			return KEY_E;
		else if (stricmp(_p, "F") == 0)
			return KEY_F;
		else if (stricmp(_p, "G") == 0)
			return KEY_G;
		else if (stricmp(_p, "H") == 0)
			return KEY_H;
		else if (stricmp(_p, "I") == 0)
			return KEY_I;
		else if (stricmp(_p, "J") == 0)
			return KEY_J;
		else if (stricmp(_p, "K") == 0)
			return KEY_K;
		else if (stricmp(_p, "L") == 0)
			return KEY_L;
		else if (stricmp(_p, "M") == 0)
			return KEY_M;
		else if (stricmp(_p, "N") == 0)
			return KEY_N;
		else if (stricmp(_p, "O") == 0)
			return KEY_O;
		else if (stricmp(_p, "P") == 0)
			return KEY_P;
		else if (stricmp(_p, "Q") == 0)
			return KEY_Q;
		else if (stricmp(_p, "R") == 0)
			return KEY_R;
		else if (stricmp(_p, "S") == 0)
			return KEY_S;
		else if (stricmp(_p, "T") == 0)
			return KEY_T;
		else if (stricmp(_p, "U") == 0)
			return KEY_U;
		else if (stricmp(_p, "V") == 0)
			return KEY_V;
		else if (stricmp(_p, "W") == 0)
			return KEY_W;
		else if (stricmp(_p, "X") == 0)
			return KEY_X;
		else if (stricmp(_p, "Y") == 0)
			return KEY_Y;
		else if (stricmp(_p, "Z") == 0)
			return KEY_Z;

		else if (_p[0] == '0')
			return KEY_0;
		else if (_p[0] == '1')
			return KEY_1;
		else if (_p[0] == '2')
			return KEY_2;
		else if (_p[0] == '3')
			return KEY_3;
		else if (_p[0] == '4')
			return KEY_4;
		else if (_p[0] == '5')
			return KEY_5;
		else if (_p[0] == '6')
			return KEY_6;
		else if (_p[0] == '7')
			return KEY_7;
		else if (_p[0] == '8')
			return KEY_8;
		else if (_p[0] == '9')
			return KEY_9;

		else if ((stricmp(_p, "ESC")) == 0)
			return KEY_ESC;
		else if ((stricmp(_p, "EQUAL")) == 0)
			return KEY_EQUAL;

		else if ((stricmp(_p, "UP")) == 0)
			return KEY_UP;
		else if ((stricmp(_p, "DONW")) == 0)
			return KEY_DOWN;
		else if ((stricmp(_p, "LEFT")) == 0)
			return KEY_LEFT;
		else if ((stricmp(_p, "RIGHT")) == 0)
			return KEY_RIGHT;

		else if ((stricmp(_p, "F1")) == 0)
			return KEY_F1;
		else if ((stricmp(_p, "F2")) == 0)
			return KEY_F2;
		else if ((stricmp(_p, "F3")) == 0)
			return KEY_F3;
		else if ((stricmp(_p, "F4")) == 0)
			return KEY_F4;
		else if ((stricmp(_p, "F5")) == 0)
			return KEY_F5;
		else if ((stricmp(_p, "F6")) == 0)
			return KEY_F6;
		else if ((stricmp(_p, "F7")) == 0)
			return KEY_F7;
		else if ((stricmp(_p, "F8")) == 0)
			return KEY_F8;
		else if ((stricmp(_p, "F9")) == 0)
			return KEY_F9;
		else if ((stricmp(_p, "F10")) == 0)
			return KEY_F10;
		else if ((stricmp(_p, "F11")) == 0)
			return KEY_F11;
		else if ((stricmp(_p, "F12")) == 0)
			return KEY_F12;

		return 0;
	}

	/**
	DEPRECATED
	*/
	const char* WindowSystem::Input_GetKeyValueByInt(int _p)
	{
		if (_p == KEY_A)
			return "A";
		else if (_p == KEY_B)
			return "B";
		else if (_p == KEY_C)
			return "C";
		else if (_p == KEY_D)
			return "D";
		else if (_p == KEY_E)
			return "E";
		else if (_p == KEY_F)
			return "F";
		else if (_p == KEY_G)
			return "G";
		else if (_p == KEY_H)
			return "H";
		else if (_p == KEY_I)
			return "I";
		else if (_p == KEY_J)
			return "J";
		else if (_p == KEY_K)
			return "K";
		else if (_p == KEY_L)
			return "L";
		else if (_p == KEY_M)
			return "M";
		else if (_p == KEY_N)
			return "N";
		else if (_p == KEY_O)
			return "O";
		else if (_p == KEY_P)
			return "P";
		else if (_p == KEY_Q)
			return "Q";
		else if (_p == KEY_R)
			return "R";
		else if (_p == KEY_S)
			return "S";
		else if (_p == KEY_T)
			return "T";
		else if (_p == KEY_U)
			return "U";
		else if (_p == KEY_V)
			return "V";
		else if (_p == KEY_W)
			return "W";
		else if (_p == KEY_X)
			return "X";
		else if (_p == KEY_Y)
			return "Y";
		else if (_p == KEY_Z)
			return "Z";

		else if (_p == KEY_0)
			return "0";
		else if (_p == KEY_1)
			return "1";
		else if (_p == KEY_2)
			return "2";
		else if (_p == KEY_3)
			return "3";
		else if (_p == KEY_4)
			return "4";
		else if (_p == KEY_5)
			return "5";
		else if (_p == KEY_6)
			return "6";
		else if (_p == KEY_7)
			return "7";
		else if (_p == KEY_8)
			return "8";
		else if (_p == KEY_9)
			return "9";

		else if (_p == KEY_ESC)
			return "ESC";
		else if (_p == KEY_SPACE)
			return "SPACE";
		else if (_p == KEY_UP)
			return "UP";
		else if (_p == KEY_DOWN)
			return "DOWN";
		else if (_p == KEY_LEFT)
			return "LEFT";
		else if (_p == KEY_RIGHT)
			return "RIGHT";

		else if (_p == KEY_F1)
			return "F1";
		else if (_p == KEY_F2)
			return "F2";
		else if (_p == KEY_F3)
			return "F3";
		else if (_p == KEY_F4)
			return "F4";
		else if (_p == KEY_F5)
			return "F5";
		else if (_p == KEY_F6)
			return "F6";
		else if (_p == KEY_F7)
			return "F7";
		else if (_p == KEY_F8)
			return "F8";
		else if (_p == KEY_F9)
			return "F9";
		else if (_p == KEY_F10)
			return "F10";
		else if (_p == KEY_F11)
			return "F11";
		else if (_p == KEY_F12)
			return "F12";

		return "NULL";
	}

	/**
	TODO:ADD Check
	*/
	void WindowSystem::SetKeyDown(int _value) {
		m_Input.keys[_value] = true;
	}

	/**
	TODO:ADD Check
	*/
	void WindowSystem::SetKeyUp(int _value) {
		m_Input.keys[_value] = false;
	}
#endif
}
#endif