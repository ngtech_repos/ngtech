/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
//***************************************************
#include "PlatformPrivate.h"

#if !IS_OS_ANDROID
//***************************************************
#include "../../../Externals/glfw/include/GLFW/glfw3.h"
#include "../../../Externals/glfw/include/GLFW/glfw3native.h"
//***************************************************
#include "../inc/WindowSystem_GLFW.h"
#include "../inc/WindowEvents.h"
//***************************************************
//#include "ScriptUpdateJob.h"
//***************************************************
#include "LogFunctions.h"
//***************************************************

namespace NGTech {
	/**
	*/
	static ENGINE_INLINE const char* get_key_name(int key)
	{
		switch (key)
		{
			// Printable keys
		case GLFW_KEY_A:            return "A";
		case GLFW_KEY_B:            return "B";
		case GLFW_KEY_C:            return "C";
		case GLFW_KEY_D:            return "D";
		case GLFW_KEY_E:            return "E";
		case GLFW_KEY_F:            return "F";
		case GLFW_KEY_G:            return "G";
		case GLFW_KEY_H:            return "H";
		case GLFW_KEY_I:            return "I";
		case GLFW_KEY_J:            return "J";
		case GLFW_KEY_K:            return "K";
		case GLFW_KEY_L:            return "L";
		case GLFW_KEY_M:            return "M";
		case GLFW_KEY_N:            return "N";
		case GLFW_KEY_O:            return "O";
		case GLFW_KEY_P:            return "P";
		case GLFW_KEY_Q:            return "Q";
		case GLFW_KEY_R:            return "R";
		case GLFW_KEY_S:            return "S";
		case GLFW_KEY_T:            return "T";
		case GLFW_KEY_U:            return "U";
		case GLFW_KEY_V:            return "V";
		case GLFW_KEY_W:            return "W";
		case GLFW_KEY_X:            return "X";
		case GLFW_KEY_Y:            return "Y";
		case GLFW_KEY_Z:            return "Z";
		case GLFW_KEY_1:            return "1";
		case GLFW_KEY_2:            return "2";
		case GLFW_KEY_3:            return "3";
		case GLFW_KEY_4:            return "4";
		case GLFW_KEY_5:            return "5";
		case GLFW_KEY_6:            return "6";
		case GLFW_KEY_7:            return "7";
		case GLFW_KEY_8:            return "8";
		case GLFW_KEY_9:            return "9";
		case GLFW_KEY_0:            return "0";
		case GLFW_KEY_SPACE:        return "SPACE";
		case GLFW_KEY_MINUS:        return "MINUS";
		case GLFW_KEY_EQUAL:		return "EQUAL";
		case GLFW_KEY_LEFT_BRACKET: return "LEFT BRACKET";
		case GLFW_KEY_RIGHT_BRACKET: return "RIGHT BRACKET";
		case GLFW_KEY_BACKSLASH:    return "BACKSLASH";
		case GLFW_KEY_SEMICOLON:    return "SEMICOLON";
		case GLFW_KEY_APOSTROPHE:   return "APOSTROPHE";
		case GLFW_KEY_GRAVE_ACCENT: return "GRAVE ACCENT";
		case GLFW_KEY_COMMA:        return "COMMA";
		case GLFW_KEY_PERIOD:       return "PERIOD";
		case GLFW_KEY_SLASH:        return "SLASH";
		case GLFW_KEY_WORLD_1:      return "WORLD 1";
		case GLFW_KEY_WORLD_2:      return "WORLD 2";

			// Function keys
		case GLFW_KEY_ESCAPE:       return "ESC";
		case GLFW_KEY_F1:           return "F1";
		case GLFW_KEY_F2:           return "F2";
		case GLFW_KEY_F3:           return "F3";
		case GLFW_KEY_F4:           return "F4";
		case GLFW_KEY_F5:           return "F5";
		case GLFW_KEY_F6:           return "F6";
		case GLFW_KEY_F7:           return "F7";
		case GLFW_KEY_F8:           return "F8";
		case GLFW_KEY_F9:           return "F9";
		case GLFW_KEY_F10:          return "F10";
		case GLFW_KEY_F11:          return "F11";
		case GLFW_KEY_F12:          return "F12";
		case GLFW_KEY_F13:          return "F13";
		case GLFW_KEY_F14:          return "F14";
		case GLFW_KEY_F15:          return "F15";
		case GLFW_KEY_F16:          return "F16";
		case GLFW_KEY_F17:          return "F17";
		case GLFW_KEY_F18:          return "F18";
		case GLFW_KEY_F19:          return "F19";
		case GLFW_KEY_F20:          return "F20";
		case GLFW_KEY_F21:          return "F21";
		case GLFW_KEY_F22:          return "F22";
		case GLFW_KEY_F23:          return "F23";
		case GLFW_KEY_F24:          return "F24";
		case GLFW_KEY_F25:          return "F25";
		case GLFW_KEY_UP:           return "UP";
		case GLFW_KEY_DOWN:         return "DOWN";
		case GLFW_KEY_LEFT:         return "LEFT";
		case GLFW_KEY_RIGHT:        return "RIGHT";
		case GLFW_KEY_LEFT_SHIFT:   return "LEFT SHIFT";
		case GLFW_KEY_RIGHT_SHIFT:  return "RIGHT SHIFT";
		case GLFW_KEY_LEFT_CONTROL: return "LEFT CONTROL";
		case GLFW_KEY_RIGHT_CONTROL: return "RIGHT CONTROL";
		case GLFW_KEY_LEFT_ALT:     return "LEFT ALT";
		case GLFW_KEY_RIGHT_ALT:    return "RIGHT ALT";
		case GLFW_KEY_TAB:          return "TAB";
		case GLFW_KEY_ENTER:        return "ENTER";
		case GLFW_KEY_BACKSPACE:    return "BACKSPACE";
		case GLFW_KEY_INSERT:       return "INSERT";
		case GLFW_KEY_DELETE:       return "DELETE";
		case GLFW_KEY_PAGE_UP:      return "PAGE UP";
		case GLFW_KEY_PAGE_DOWN:    return "PAGE DOWN";
		case GLFW_KEY_HOME:         return "HOME";
		case GLFW_KEY_END:          return "END";
		case GLFW_KEY_KP_0:         return "KEYPAD 0";
		case GLFW_KEY_KP_1:         return "KEYPAD 1";
		case GLFW_KEY_KP_2:         return "KEYPAD 2";
		case GLFW_KEY_KP_3:         return "KEYPAD 3";
		case GLFW_KEY_KP_4:         return "KEYPAD 4";
		case GLFW_KEY_KP_5:         return "KEYPAD 5";
		case GLFW_KEY_KP_6:         return "KEYPAD 6";
		case GLFW_KEY_KP_7:         return "KEYPAD 7";
		case GLFW_KEY_KP_8:         return "KEYPAD 8";
		case GLFW_KEY_KP_9:         return "KEYPAD 9";
		case GLFW_KEY_KP_DIVIDE:    return "KEYPAD DIVIDE";
		case GLFW_KEY_KP_MULTIPLY:  return "KEYPAD MULTPLY";
		case GLFW_KEY_KP_SUBTRACT:  return "KEYPAD SUBTRACT";
		case GLFW_KEY_KP_ADD:       return "KEYPAD ADD";
		case GLFW_KEY_KP_DECIMAL:   return "KEYPAD DECIMAL";
		case GLFW_KEY_KP_EQUAL:     return "KEYPAD EQUAL";
		case GLFW_KEY_KP_ENTER:     return "KEYPAD ENTER";
		case GLFW_KEY_PRINT_SCREEN: return "PRINT SCREEN";
		case GLFW_KEY_NUM_LOCK:     return "NUM LOCK";
		case GLFW_KEY_CAPS_LOCK:    return "CAPS LOCK";
		case GLFW_KEY_SCROLL_LOCK:  return "SCROLL LOCK";
		case GLFW_KEY_PAUSE:        return "PAUSE";
		case GLFW_KEY_LEFT_SUPER:   return "LEFT SUPER";
		case GLFW_KEY_RIGHT_SUPER:  return "RIGHT SUPER";
		case GLFW_KEY_MENU:         return "MENU";
		case GLFW_KEY_UNKNOWN:      return "UNKNOWN";

		default:                    return NULL;
		}
	}

	/**
	*/
	static ENGINE_INLINE const int get_key_name(const char* _p)
	{
		if (stricmp(_p, "A") == 0)
			return I_Input::KEY_A;
		else if (stricmp(_p, "B") == 0)
			return I_Input::KEY_B;
		else if (stricmp(_p, "C") == 0)
			return I_Input::KEY_C;
		else if (stricmp(_p, "D") == 0)
			return I_Input::KEY_D;
		else if (stricmp(_p, "E") == 0)
			return I_Input::KEY_E;
		else if (stricmp(_p, "F") == 0)
			return I_Input::KEY_F;
		else if (stricmp(_p, "G") == 0)
			return I_Input::KEY_G;
		else if (stricmp(_p, "H") == 0)
			return I_Input::KEY_H;
		else if (stricmp(_p, "I") == 0)
			return I_Input::KEY_I;
		else if (stricmp(_p, "J") == 0)
			return I_Input::KEY_J;
		else if (stricmp(_p, "K") == 0)
			return I_Input::KEY_K;
		else if (stricmp(_p, "L") == 0)
			return I_Input::KEY_L;
		else if (stricmp(_p, "M") == 0)
			return I_Input::KEY_M;
		else if (stricmp(_p, "N") == 0)
			return I_Input::KEY_N;
		else if (stricmp(_p, "O") == 0)
			return I_Input::KEY_O;
		else if (stricmp(_p, "P") == 0)
			return I_Input::KEY_P;
		else if (stricmp(_p, "Q") == 0)
			return I_Input::KEY_Q;
		else if (stricmp(_p, "R") == 0)
			return I_Input::KEY_R;
		else if (stricmp(_p, "S") == 0)
			return I_Input::KEY_S;
		else if (stricmp(_p, "T") == 0)
			return I_Input::KEY_T;
		else if (stricmp(_p, "U") == 0)
			return I_Input::KEY_U;
		else if (stricmp(_p, "V") == 0)
			return I_Input::KEY_V;
		else if (stricmp(_p, "W") == 0)
			return I_Input::KEY_W;
		else if (stricmp(_p, "X") == 0)
			return I_Input::KEY_X;
		else if (stricmp(_p, "Y") == 0)
			return I_Input::KEY_Y;
		else if (stricmp(_p, "Z") == 0)
			return I_Input::KEY_Z;

		else if (_p[0] == '0')
			return I_Input::KEY_0;
		else if (_p[0] == '1')
			return I_Input::KEY_1;
		else if (_p[0] == '2')
			return I_Input::KEY_2;
		else if (_p[0] == '3')
			return I_Input::KEY_3;
		else if (_p[0] == '4')
			return I_Input::KEY_4;
		else if (_p[0] == '5')
			return I_Input::KEY_5;
		else if (_p[0] == '6')
			return I_Input::KEY_6;
		else if (_p[0] == '7')
			return I_Input::KEY_7;
		else if (_p[0] == '8')
			return I_Input::KEY_8;
		else if (_p[0] == '9')
			return I_Input::KEY_9;

		else if ((stricmp(_p, "SPACE")) == 0)
			return I_Input::KEY_SPACE;

		else if ((stricmp(_p, "MINUS")) == 0)
			return I_Input::KEY_MINUS;
		else if ((stricmp(_p, "LEFT BRACKET")) == 0)
			return I_Input::KEY_LEFT_BRACKET;
		else if ((stricmp(_p, "RIGHT BRACKET")) == 0)
			return I_Input::KEY_RIGHT_BRACKET;
		else if ((stricmp(_p, "BACKSLASH")) == 0)
			return I_Input::KEY_BACKSLASH;
		else if ((stricmp(_p, "SEMICOLON")) == 0)
			return I_Input::KEY_SEMICOLON;
		else if ((stricmp(_p, "APOSTROPHE")) == 0)
			return I_Input::KEY_APOSTROPHE;
		else if ((stricmp(_p, "GRAVE ACCENT")) == 0)
			return I_Input::KEY_GRAVE_ACCENT;
		/*else if ((stricmp(_p, "COMMA")) == 0)
			return KEY_COMMA;
			else if ((stricmp(_p, "PERIOD")) == 0)
			return KEY_PERIOD;
			else if ((stricmp(_p, "SLASH")) == 0)
			return KEY_SLASH;
			else if ((stricmp(_p, "WORLD 1")) == 0)
			return KEY_WORLD_1;
			else if ((stricmp(_p, "WORLD 2")) == 0)
			return KEY_WORLD_2;*/

		else if ((stricmp(_p, "F1")) == 0)
			return I_Input::KEY_F1;
		else if ((stricmp(_p, "F2")) == 0)
			return I_Input::KEY_F2;
		else if ((stricmp(_p, "F3")) == 0)
			return I_Input::KEY_F3;
		else if ((stricmp(_p, "F4")) == 0)
			return I_Input::KEY_F4;
		else if ((stricmp(_p, "F5")) == 0)
			return I_Input::KEY_F5;
		else if ((stricmp(_p, "F6")) == 0)
			return I_Input::KEY_F6;
		else if ((stricmp(_p, "F7")) == 0)
			return I_Input::KEY_F7;
		else if ((stricmp(_p, "F8")) == 0)
			return I_Input::KEY_F8;
		else if ((stricmp(_p, "F9")) == 0)
			return I_Input::KEY_F9;
		else if ((stricmp(_p, "F10")) == 0)
			return I_Input::KEY_F10;
		else if ((stricmp(_p, "F11")) == 0)
			return I_Input::KEY_F11;
		else if ((stricmp(_p, "F12")) == 0)
			return I_Input::KEY_F12;
		else if ((stricmp(_p, "F13")) == 0)
			return I_Input::KEY_F13;
		else if ((stricmp(_p, "F14")) == 0)
			return I_Input::KEY_F14;
		else if ((stricmp(_p, "F15")) == 0)
			return I_Input::KEY_F15;
		else if ((stricmp(_p, "F16")) == 0)
			return I_Input::KEY_F16;
		else if ((stricmp(_p, "F17")) == 0)
			return I_Input::KEY_F17;
		else if ((stricmp(_p, "F18")) == 0)
			return I_Input::KEY_F18;
		else if ((stricmp(_p, "F19")) == 0)
			return I_Input::KEY_F19;
		else if ((stricmp(_p, "F20")) == 0)
			return I_Input::KEY_F20;
		else if ((stricmp(_p, "F21")) == 0)
			return I_Input::KEY_F21;
		else if ((stricmp(_p, "F22")) == 0)
			return I_Input::KEY_F22;
		else if ((stricmp(_p, "F23")) == 0)
			return I_Input::KEY_F23;
		else if ((stricmp(_p, "F24")) == 0)
			return I_Input::KEY_F24;
		else if ((stricmp(_p, "F25")) == 0)
			return I_Input::KEY_F25;

#if 0
		case GLFW_KEY_LEFT_SHIFT:   return "LEFT SHIFT";
		case GLFW_KEY_RIGHT_SHIFT:  return "RIGHT SHIFT";
		case GLFW_KEY_LEFT_CONTROL: return "LEFT CONTROL";
		case GLFW_KEY_RIGHT_CONTROL: return "RIGHT CONTROL";
		case GLFW_KEY_LEFT_ALT:     return "LEFT ALT";
		case GLFW_KEY_RIGHT_ALT:    return "RIGHT ALT";
		case GLFW_KEY_TAB:          return "TAB";
		case GLFW_KEY_ENTER:        return "ENTER";
		case GLFW_KEY_BACKSPACE:    return "BACKSPACE";
		case GLFW_KEY_INSERT:       return "INSERT";
		case GLFW_KEY_DELETE:       return "DELETE";
		case GLFW_KEY_PAGE_UP:      return "PAGE UP";
		case GLFW_KEY_PAGE_DOWN:    return "PAGE DOWN";
		case GLFW_KEY_HOME:         return "HOME";
		case GLFW_KEY_END:          return "END";
		case GLFW_KEY_KP_0:         return "KEYPAD 0";
		case GLFW_KEY_KP_1:         return "KEYPAD 1";
		case GLFW_KEY_KP_2:         return "KEYPAD 2";
		case GLFW_KEY_KP_3:         return "KEYPAD 3";
		case GLFW_KEY_KP_4:         return "KEYPAD 4";
		case GLFW_KEY_KP_5:         return "KEYPAD 5";
		case GLFW_KEY_KP_6:         return "KEYPAD 6";
		case GLFW_KEY_KP_7:         return "KEYPAD 7";
		case GLFW_KEY_KP_8:         return "KEYPAD 8";
		case GLFW_KEY_KP_9:         return "KEYPAD 9";
		case GLFW_KEY_KP_DIVIDE:    return "KEYPAD DIVIDE";
		case GLFW_KEY_KP_MULTIPLY:  return "KEYPAD MULTPLY";
		case GLFW_KEY_KP_SUBTRACT:  return "KEYPAD SUBTRACT";
		case GLFW_KEY_KP_ADD:       return "KEYPAD ADD";
		case GLFW_KEY_KP_DECIMAL:   return "KEYPAD DECIMAL";
		case GLFW_KEY_KP_EQUAL:     return "KEYPAD EQUAL";
		case GLFW_KEY_KP_ENTER:     return "KEYPAD ENTER";
		case GLFW_KEY_PRINT_SCREEN: return "PRINT SCREEN";
		case GLFW_KEY_NUM_LOCK:     return "NUM LOCK";
		case GLFW_KEY_CAPS_LOCK:    return "CAPS LOCK";
		case GLFW_KEY_SCROLL_LOCK:  return "SCROLL LOCK";
		case GLFW_KEY_PAUSE:        return "PAUSE";
		case GLFW_KEY_LEFT_SUPER:   return "LEFT SUPER";
		case GLFW_KEY_RIGHT_SUPER:  return "RIGHT SUPER";
		case GLFW_KEY_MENU:         return "MENU";
		case GLFW_KEY_UNKNOWN:      return "UNKNOWN";
#endif

		else if ((stricmp(_p, "ESC")) == 0)
			return I_Input::KEY_ESC;
		else if ((stricmp(_p, "EQUAL")) == 0)
			return I_Input::KEY_EQUAL;

		else if ((stricmp(_p, "UP")) == 0)
			return I_Input::KEY_UP;
		else if ((stricmp(_p, "DONW")) == 0)
			return I_Input::KEY_DOWN;
		else if ((stricmp(_p, "LEFT")) == 0)
			return I_Input::KEY_LEFT;
		else if ((stricmp(_p, "RIGHT")) == 0)
			return I_Input::KEY_RIGHT;

		return 0;
	}

	/*
	*/
	static ENGINE_INLINE void _get_resolution(uint32_t &window_width, uint32_t &window_height)
	{
		const GLFWvidmode * mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		ASSERT(mode, "[glfwGetVideoMode] Invalid pointer on mode");

		window_width = mode->width;
		window_height = mode->height;
	}

	/**
	*/
	static ENGINE_INLINE void error_callback(int error, const char* description) {
		Warning(description, error);
	}

	/**
	*/
	WindowSystemGLFW::WindowSystemGLFW(void*_m_engine, const PlatformDependVars&_vars, const WindowCallbacks& _callback)
	{
		m_engine = _m_engine;
		m_Vars = _vars;
		m_Callback = _callback;

		LogPrintf("-- WindowSystem --");
	}

	/*
	*/
	void WindowSystemGLFW::setInputCallbacksGLFW(GLFWwindow *_window)
	{
		glfwSetKeyCallback(_window, key_callback);
		glfwSetMouseButtonCallback(_window, mouse_button_callback);
		glfwSetCursorPosCallback(_window, cursor_position_callback);

		// TODO:[WindowSystemGLFW::callback] Implement this
		//glfwSetScrollCallback(_window, scroll);

		glfwSetWindowSizeCallback(_window, window_size_callback);
		glfwSetWindowCloseCallback(_window, window_close_callback);
		glfwSetFramebufferSizeCallback(_window, window_size_callback);

		// TODO:[WindowSystemGLFW::callback] Implement this
		//glfwSetWindowFocusCallback(_window, focus);
	}

	/*
	*/
	void WindowSystemGLFW::Initialise(void*)
	{
		Debug("WindowSystem::Initialize()");

		//m_updateJob = std::make_shared<ScriptUpdateJob>(ScriptUpdateJob::UPDATE_MODE::UPDATE_INPUT);

		_CreateContextAndWindow();

		ShowOSCursor(false);

		glfwMakeContextCurrent(window);
	}

	void WindowSystemGLFW::_CleanResources()
	{
		// FIXME: Crash on release build
		//!@todo Crash on release build

		//m_updateJob.reset();

		//if (window)
		//	glfwDestroyWindow(window);

		//glfwTerminate();
	}

	/*
	*/
	bool WindowSystemGLFW::CreateWindowImpl()
	{
		Debug("WindowSystem::CreateWindowImpl() - PreInitialize");

		glfwSetErrorCallback(error_callback);

		if (!glfwInit())
		{
			glfwTerminate();
			Error("[WindowSystemGLFW::Initialise] Failed initializing GLFW window {glfwInit}", true);
			return false;
		}

		GLFWmonitor *mMonitor = nullptr;
		if (m_Vars.fullscreen)
		{
			mMonitor = glfwGetPrimaryMonitor();

			const auto mode = glfwGetVideoMode(mMonitor);

			glfwWindowHint(GLFW_RED_BITS, mode->redBits);
			glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
			glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
			glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

			//!@todo Only for debug
			glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);

			m_Vars.width = mode->width;
			m_Vars.height = mode->height;
		}

		if (!m_Vars.fullscreen && m_Vars.withoutBorder)
			glfwWindowHint(GLFW_DECORATED, GL_FALSE);

#ifdef HEAVY_DEBUG
		glfwWindowHint(GLFW_FLOATING, GL_FALSE);
#endif

		if ((m_Vars.width <= 1) || (m_Vars.height <= 1))
		{
			m_Vars.width = 1920;
			m_Vars.height = 1080;
		}

		//!@todo Replace Define PROJECT_TITLE on m_Vars.ProjectTitle
		window = glfwCreateWindow(m_Vars.width, m_Vars.height, PROJECT_TITLE, mMonitor, NULL);
		if (!window)
		{
			Error("[WindowSystemGLFW::CreateWindow] Failed to open GLFW window", true);
			glfwTerminate();
			return false;
		}

		// Will used in callbacks(e.g Focus_Callback)
		glfwSetWindowUserPointer(window, this);

		// Set callback functions
		setInputCallbacksGLFW(window);

		// It's will used in render, will contain GLFWWindow pointer
		m_Vars.hDC = window;

		return true;
	}

	/**
	*/
	void WindowSystemGLFW::_CreateContextAndWindow()
	{
		static const uint32_t OPENGLMAXMMINORVERSION = 6;
		static const uint32_t OPENGLMAJORVERSION = 4;

		for (uint32_t m = OPENGLMAXMMINORVERSION; m >= 0; m--) //-V654
		{
			if (_CreateContext(OPENGLMAJORVERSION, m, true))
				return;
		}

		for (uint32_t m = 3; m >= 0; m--) //-V779
		{
			if (_CreateContext(3, m, true))
				return;
		}

		// Create any context - driver selection
		ASSERT(!_CreateContext(0, 0, false), "[WindowSystemGLFW::Initialise] Failed initializing any GLFW window {glfwCreateWindow}. Try update video drivers");
	}

	/**
	TODO: Engine refactoring: move to OGLDRV,this deprecated
	*/
	bool WindowSystemGLFW::_CreateContext(int major, int minor, bool specific)
	{
		//Request Specific Version
		// #38 bufgix: on AMD videocards is not works for any context >3.1
		// so on Windows we will use Maximal Driver version

		//!@todo Engine refactoring: move to OGLDRV
//#if PLATFORM_OS !=PLATFORM_OS_WINDOWS
		if (specific)
		{
			LogPrintf("[%s] OpenGL Context requested: %i, %i", __FUNCTION__, major, minor);
			glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, major);
			glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, minor);
			glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		}
		else
			Warning("[%s] OpenGL Context requested any compatible");
		//#endif

		return true;
	}

	/**
	*/
	bool WindowSystemGLFW::IsWindowFocused()
	{
		ASSERT(window, "Not exist pointer");
		if (!window)
			return false;

		return (glfwGetWindowAttrib(window, GLFW_FOCUSED));
	}

	/**
	*/
	WindowSystemGLFW::~WindowSystemGLFW() {
		_CleanResources();
	}

	/*
	*/
	void WindowSystemGLFW::SetTitle(const String &title) {
		ASSERT(window, "Invalid pointer");
		if (window)
			glfwSetWindowTitle(window, title.c_str());
	}

	/*
	*/
	void WindowSystemGLFW::Update() {
		BROFILER_CATEGORY("UpdateInput", Brofiler::Color::Orange);
		if (glfwWindowShouldClose(window))
			return;

		m_Callback.ScriptUpdate();

		memcpy(m_Input.oldKeys, m_Input.keys, sizeof(m_Input.keys));
		memcpy(m_Input.oldMouseButtons, m_Input.mouseButtons, sizeof(m_Input.mouseButtons));

		glfwPollEvents();

		// update frame
		if (++m_Vars.frameNumber >= 0x7fffffff) m_Vars.frameNumber = 0;
	}

	/**
	*/
	bool WindowSystemGLFW::IsKeyPressed(const char* _key) {
		const int key = Input_GetKeyValueByChar(_key);
		return m_Input.keys[key];
	}

	/**
	*/
	bool WindowSystemGLFW::IsKeyDown(const char* _key) {
		const int key = Input_GetKeyValueByChar(_key);
		return (m_Input.keys[key] && !m_Input.oldKeys[key]);
	}

	/**
	*/
	bool WindowSystemGLFW::IsKeyUp(const char* _key) {
		const int key = Input_GetKeyValueByChar(_key);
		return (!m_Input.keys[key] && m_Input.oldKeys[key]);
	}

	/**
	*/
	bool WindowSystemGLFW::IsMouseButtonPressed(int mb) {
		return m_Input.mouseButtons[mb];
	}

	/**
	*/
	bool WindowSystemGLFW::WasMouseButtonPressed(int mb) {
		return (m_Input.mouseButtons[mb] && !m_Input.oldMouseButtons[mb]);
	}

	/**
	*/
	bool WindowSystemGLFW::WasMouseButtonReleased(int mb) {
		return (!m_Input.mouseButtons[mb] && m_Input.oldMouseButtons[mb]);
	}

	/**
	*/
	void WindowSystemGLFW::ShowCursor(bool show) {
		m_Input.cursorVisible = show;
	}

	/**
	*/
	void WindowSystemGLFW::ShowOSCursor(bool show) {
		int value = GLFW_CURSOR_NORMAL;

		if (show == 1)
			value = GLFW_CURSOR_NORMAL;
		else
			value = GLFW_CURSOR_DISABLED;

		glfwSetInputMode(window, GLFW_CURSOR, value);
	}

	/**
	*/
	void WindowSystemGLFW::SetMousePos(int x, int y) {
		glfwSetCursorPos(window, x, y);
	}

	/**
	*/
	void WindowSystemGLFW::GrabMouse(bool grab) {
		ShowCursor(!grab);
		m_Input.mouseGrabbed = grab;
	}

	/**
	*/
	void WindowSystemGLFW::SwapBuffers() {
		ASSERT(window, "Invalid pointer");
		glfwSwapBuffers(window);
	}

	/**
	*/
	const int WindowSystemGLFW::Input_GetKeyValueByChar(const char*  _p) {
		return get_key_name(_p);
	}

	/**
	*/
	const char* WindowSystemGLFW::Input_GetKeyValueByInt(int _p) {
		return get_key_name(_p);
	}

	/**
	*/
	void WindowSystemGLFW::ManageVSync(bool _v) {
		glfwSwapInterval(_v);
	}
}
#endif