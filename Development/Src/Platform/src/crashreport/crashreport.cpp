#include "PlatformPrivate.h"

#if IS_OS_WINDOWS
//***************************************************************************
#include "crashreport/windows/handler/exception_handler.h"
#include "crashreport/windows/crash_generation/client_info.h"
#include "crashreport/windows/crash_generation/crash_generation_server.h"
//***************************************************************************
#include "dbgtools/callstack.h"
//***************************************************************************
#endif

namespace NGTech
{
#if IS_OS_WINDOWS
	static ExceptionHandler* handler = NULL;
	static CrashGenerationServer* crash_server = NULL;
	const wchar_t kPipeName[] = L"/./NGTechCrashService";
	const std::wstring dump_path = L"./../logs/";
#endif

	void CrashServerStart() {
#if IS_OS_WINDOWS
		// Do not create another instance of the server.
		if (crash_server) {
			return;
		}

		if (_waccess(dump_path.c_str(), 00) == -1)
			_wmkdir(dump_path.c_str());

		crash_server = new CrashGenerationServer(kPipeName,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			true,
			&dump_path);

		if (!crash_server->Start()) {
			delete crash_server;
			crash_server = NULL;
		}
#endif
	}

	void CrashServerStop() {
#if IS_OS_WINDOWS
		delete crash_server;
		crash_server = NULL;
#endif
	}

	VectorString GetCallStackToString()
	{
		VectorString strings;
#if IS_OS_WINDOWS
		void* addresses[256];
		int i;
		int num_addresses = callstack(0, addresses, 256);

		callstack_symbol_t symbols[256];
		char  symbols_buffer[1024];
		num_addresses = callstack_symbols(addresses, symbols, num_addresses, symbols_buffer, 1024);

		strings.push_back("================================================================================== \n");
		strings.push_back("CALLSTACK: \n");

		for (i = 0; i < num_addresses; ++i)
		{
			String str = "[" + std::to_string(i) + "] " + symbols[i].function;
			str += " ";
			str += symbols[i].file;
			str += " " + std::to_string(symbols[i].line) + "\n";

			strings.push_back(str);
		}
		strings.push_back("================================================================================== \n");

#else
		strings.clear();
#endif
		return strings;
	}

	void GetCallStackToString(String& str)
	{
		auto stack = GetCallStackToString();
		for (const auto &it : stack)
			str += it;
	}

#if IS_OS_WINDOWS
	bool ShowDumpResults(const wchar_t* _dump_path,
		const wchar_t* minidump_id,
		void* context,
		EXCEPTION_POINTERS* exinfo,
		MDRawAssertionInfo* assertion,
		bool succeeded)
	{
		String ex = "General Protection Fault!";
		String desc("Critical Error: \n");
		GetCallStackToString(desc);

#ifdef _NDEBUG
		PlatformError(desc.c_str(), ex.c_str(), true);
#else
		PlatformError(desc.c_str(), ex.c_str(), false);
#endif

		return succeeded;
	}
#endif

	bool InitMiniDump()
	{
#if IS_OS_WINDOWS

#ifndef _DEBUG
		CrashServerStart();
		// This is needed for CRT to not show dialog for invalid param
		// failures and instead let the code handle it.
		_CrtSetReportMode(_CRT_ASSERT, 0);

		handler = new ExceptionHandler(dump_path,
			NULL,
			ShowDumpResults,
			NULL,
			ExceptionHandler::HANDLER_ALL,
			MiniDumpNormal,
			kPipeName,
			NULL);
		if (!handler)
			return false;
#endif

#endif

		return true;
	}
}