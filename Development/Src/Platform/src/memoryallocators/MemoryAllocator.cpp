/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "PlatformPrivate.h"

#include "memoryallocators/MemoryAllocator.h"
#include <stdlib.h>     /* malloc, free */

using namespace NGTech;
using namespace allocator;

MemoryAllocator::MemoryAllocator()
	: BaseAllocator(0) {
}

void MemoryAllocator::Init() {
}

MemoryAllocator::~MemoryAllocator() {
}

void* MemoryAllocator::Allocate(const std::size_t size, const std::size_t alignment) {
	return malloc(size);
}

void MemoryAllocator::Free(void* ptr) {
	free(ptr);
}