/* Copyright (C) 2009-2019, NG Games Ltd. All rights reserved.
*
* This file is part of the NGTech (http://nggames.com/).
*
* Your use and or redistribution of this software in source and / or
* binary form, with or without modification, is subject to: (i) your
* ongoing acceptance of and compliance with the terms and conditions of
* the NGTech License Agreement; and (ii) your inclusion of this notice
* in any version of this software that you use or redistribute.
* A copy of the NGTech License Agreement is available by contacting
* NG Games Ltd. at http://nggames.com/
*/
#include "PlatformPrivate.h"

#include "memoryallocators/MemoryAllocator.h"

namespace NGTech
{
	void* SSEMemoryAllocatorUtil::Allocate(size_t size, const std::size_t  alignment) {
		void* p = nullptr;
#if (defined(__SSE__) || defined(_M_IX86) || defined(_M_X64))
		p = _mm_malloc(size, alignment);
#else
		if (posix_memalign(&p, size, alignment) != 0)
		{
			p = nullptr;
		}
#endif
		return p;
	}

	void SSEMemoryAllocatorUtil::DeAllocate(void* ptr) {
#if (defined(__SSE__) || defined(_M_IX86) || defined(_M_X64))
		_mm_free(ptr);
#else
		free(ptr);
#endif
	}

	void* SSEMemoryAllocator::Allocate(const std::size_t size, const std::size_t alignment) {
		return SSEMemoryAllocatorUtil::Allocate(size, alignment);
	}

	void SSEMemoryAllocator::Free(void* ptr) {
		SSEMemoryAllocatorUtil::DeAllocate(ptr);
	}
}