# NGTech Engine

# PROJECT DISCONTINUED
Unfortunately I'm no longer maintaining this project - I've ended university. It's code must be refactored, for actual state to 2020 year. 

I will rewrite it for support of multithreading, with OpenGL single threaded design, new entity system, modern render pipeline (is actual problems). 


I recommend everyone to use bgfx instead or urho3d/unity3d/UE - it's stable, reliable and has excellent documentation.

I thank everyone who was involved in developing 3dparty this little library.


# Platforms
  - Windows as main platform(with editor)
  - Linux (Runtime only). Tested on Ubuntu 14 and CentOS 5
  - Android (Runtime only). Tested on HTC Nexus 7

Not tested:

  - MacOS X - this is system has only opengl4.1, we have target on 4.3 and later. Many render features will be unavailable

### Building for source
* Install VS with C++/C#/Mobile developing
* Clone repo with all submodules
* Build PhysX libraries for your target platform

For Editor

* Install DevExpress 17.1.3
* Open solution in Src
* Build in target platform

For Runtime
* Install VS compilers.
* Build

Or

* Open CMake
* Push to "'Platform' build"
* Build

### Tech
* OpenGL 4/OpenGL ES 3.1 Render (with resource loading using fibers)
* PhysX 4.0 for physics (in dedicated thread). Rigid Body and Character controller. Physics materials, callbacks.
* OpenAL Soft for sound (in dedicated thread) with EFX filters
* Lua/C++ for game scripting with caching
* New Resource manager with fibers (WIP)
* Fiber task scheduler (with Green Threads support)
* Own project building system using CMake (Building by one button) from editor with packaging(with running compilers)
* Version layer for older devices(emulation). (from opengl 4.5 to 3.3)
* PBR lighting (without IBL)
* Hybrid render(Fully Deferred Shading and Forward+)
* Parallax Occlusion Mapping
* Steep Parallax Mapping
* Parallax Mapping
* FXAA
* Gamma correction and tone-mapping (7 options)
* Editor written on DevExpress
* Virtual File System.
* Profiler support (in web browser or brofiler)
* BreakPad for minidump generation
* Procedural generation of material and audio graph (**Early WIP**)
* MyGUI as main GUI system (**removed as deprecated. See older branches. Will replaced on own**)
* Scaleform(3.3 and 4.1) support - (**Not public**)  
* Own window system (GLFW for PC Runtime, and own on Android runtime. Partially replaced on SDL). 
* Own Serialization 
* Own Reflection System for editor 
* CMake as secondary build system
* JSON for serialization
* Animation system (per-vertex)

### Installation
Clone repo with all submodules

### Plugins

| Plugin | Directory |
| ------ | ------ |
| 3D Studio Max 2014(NGGF export, NGGF Skinned) | [Development/ ContentTools/NGMax/] |

### Development

Want to contribute? Great!
Just contact with me.

### Todos

 - Write Tests
 - Implement all features


### Older Screenshots
Editor
![A screenshot](https://pp.userapi.com/c637421/v637421287/28d4/hdTCiXdqvME.jpg "Screenshot")
![A screenshot](https://pp.userapi.com/c637421/v637421287/28b6/F9b7YMHLk8o.jpg "Screenshot")

License
----

Proprietary License - contact with me

**Just connect with me**